//
//  TSFeedBackViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/24/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSFeedBackViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *feedBackTableView;


@end
