//
//  TSPortionsHomeViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 22/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSPortionsHomeViewController.h"

#define ViewAllPortions @"View All"
#define CreatedToday @"Created Today"

@interface TSPortionsHomeViewController ()
    
@end

@implementation TSPortionsHomeViewController
{
    NSArray *allPortionsArray;
    NSArray *createdTodayArray;
    
    NSString *title;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_PORTIONHOME parameter:[NSMutableDictionary new]];
    
    [self.tableView setTableFooterView:[UIView new]];
    
    [TSSingleton layerDrawForView:self.collectionView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    [self categorizePortions];
    
    [self fetchPortionsListServiceCall];
    
    title = SINGLETON_INSTANCE.barTitle;
}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_PORTIONHOME parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = title;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Button Actions

-(IBAction)createPortionButtonTapped:(id)sender{
    
    [TSAnalytics logEvent:ANALYTICS_PORTION_CLICKED_CREATE_INHOME parameter:[NSMutableDictionary new]];
    
    if ([SINGLETON_INSTANCE checkForActiveInternet]) {
        
        TSCreatePortionViewController *createPortionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBCreatePortion"];
        
        [self.navigationController pushViewController:createPortionVC animated:YES];
        
    }else{
        
        [TSSingleton showAlertWithMessage:TEXT_NOINTERNET];
    }
}

#pragma mark - Collection view Datasource & Delegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 3;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"collectionViewCell";
    
    UICollectionViewCell *collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UIImageView *placeholderImgVw = (UIImageView *)[collectionViewCell viewWithTag:1000];
    
    UILabel *textLabel = (UILabel *)[collectionViewCell viewWithTag:1001];
    
    UILabel *countLabel = (UILabel *)[collectionViewCell viewWithTag:1002];
    
    [TSSingleton layerDrawForView:collectionViewCell.contentView position:LAYER_LEFT color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:collectionViewCell.contentView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    switch (indexPath.row) {
            
        case 0:
            placeholderImgVw.image = [UIImage imageNamed:@"approved"];
            textLabel.text = @"Approved";
            countLabel.textColor = COLOR_ACCEPT_CREATEDTODAY;
            countLabel.text = [self retrieveCountOfAssignmentTypeWithID:StatusTypeApproved];
            break;
            
        case 1:
            placeholderImgVw.image = [UIImage imageNamed:@"pending"];
            textLabel.text = @"Pending";
            countLabel.text = [self retrieveCountOfAssignmentTypeWithID:StatusTypePending];
            countLabel.textColor = COLOR_PENDING_VIEWALL;
            break;
            
        case 2:
            placeholderImgVw.image = [UIImage imageNamed:@"declined"];
            textLabel.text = @"Declined";
            countLabel.text = [self retrieveCountOfAssignmentTypeWithID:StatusTypeDeclined];
            countLabel.textColor = COLOR_DECLINED_DUESTODAY;
            break;
            
        default:
            break;
    }
    
    collectionViewCell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    collectionViewCell.layer.borderWidth = 0.35f;
    
    return collectionViewCell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    UILabel *countLbl = (UILabel *)[cell viewWithTag:1002];
    
    if ([countLbl.text intValue]>0) {
        
        TSPortionsListTableViewController *portionListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBPortionList"];
        
        NSArray *portionListArr = [NSArray new];
        
        switch (indexPath.row) {
                
            case 0:
                portionListArr = [self retrieveArrayOfAssignmentTypeWithID:StatusTypeApproved];
                
                [portionListVC setListType:ANALYTICS_VALUE_APPROVED];
                
                [TSAnalytics logEvent:ANALYTICS_PORTION_CLICKED_VIEW_PORTION parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_APPROVED,ANALYTICS_PARAM_VIEWTYPE, nil]];
                
                SINGLETON_INSTANCE.barTitle = TEXT_APPROVED;
                break;
                
            case 1:
                portionListArr = [self retrieveArrayOfAssignmentTypeWithID:StatusTypePending];
                
                [portionListVC setListType:ANALYTICS_VALUE_PENDING];
                
                [TSAnalytics logEvent:ANALYTICS_PORTION_CLICKED_VIEW_PORTION parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_PENDING,ANALYTICS_PARAM_VIEWTYPE, nil]];
                
                SINGLETON_INSTANCE.barTitle = TEXT_PENDING;
                break;
                
            case 2:
                portionListArr = [self retrieveArrayOfAssignmentTypeWithID:StatusTypeDeclined];
                
                [portionListVC setListType:ANALYTICS_VALUE_DECLINED];
                
                [TSAnalytics logEvent:ANALYTICS_PORTION_CLICKED_VIEW_PORTION parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_DECLINED,ANALYTICS_PARAM_VIEWTYPE, nil]];
                
                SINGLETON_INSTANCE.barTitle = TEXT_DECLINED;
                break;
                
            default:
                break;
        }
        
        [portionListVC setPortionsArray:portionListArr];
        
        if(portionListArr.count > 0)
            [self.navigationController pushViewController:portionListVC animated:YES];
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
   //return CGSizeMake(DeviceWidth/3, 130);
    float width = DeviceWidth/3;
    float height = 130;//width*1.214;
    
    return CGSizeMake(width, height);
}

#pragma mark - Tableview Datasource & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    UIImageView *placeHolderImgVw = (UIImageView *)[cell viewWithTag:100];
    
    UILabel *subMenuLabel = (UILabel *)[cell viewWithTag:101];
    
    UILabel *countLabel = (UILabel *)[cell viewWithTag:102];
    
    if (indexPath.row == 0) {
        
        subMenuLabel.text = @"View All";
        
        placeHolderImgVw.image = [UIImage imageNamed:@"portions_overall"];
        
        countLabel.text = [NSString stringWithFormat:@"%ld", (long)[allPortionsArray count]];
        
        countLabel.textColor = [UIColor colorWithRed:88.0/255.0 green:116.0/255.0 blue:165/255.0 alpha:1.0];
    }
    else{
        
        subMenuLabel.text = @"Created Today";
        
        placeHolderImgVw.image = [UIImage imageNamed:@"portions_today"];
        
        countLabel.text = [NSString stringWithFormat:@"%ld", (long)[createdTodayArray count]];
        
        countLabel.textColor = COLOR_ACCEPT_CREATEDTODAY;
    }
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TSPortionsListTableViewController *portionListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBPortionList"];
    
    SINGLETON_INSTANCE.barTitle = title;
    
    if (indexPath.row == 0){
        
        NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:NO];
        
        allPortionsArray = [allPortionsArray sortedArrayUsingDescriptors:@[dateDescriptor]];
        
        portionListVC.portionsArray = allPortionsArray;
        
        portionListVC.listType = ANALYTICS_VALUE_VIEWALL;
        
        [TSAnalytics logEvent:ANALYTICS_PORTION_CLICKED_VIEW_PORTION parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_VIEWALL,ANALYTICS_PARAM_VIEWTYPE, nil]];
        
        if(allPortionsArray.count > 0)
            [self.navigationController pushViewController:portionListVC animated:YES];
    }else{
        NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:NO];
        
        createdTodayArray = [createdTodayArray sortedArrayUsingDescriptors:@[dateDescriptor]];
        
        portionListVC.portionsArray = createdTodayArray;
        
        portionListVC.listType = ANALYTICS_VALUE_CREATEDTODAY;
        
        [TSAnalytics logEvent:ANALYTICS_PORTION_CLICKED_VIEW_PORTION parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_CREATEDTODAY,ANALYTICS_PARAM_VIEWTYPE, nil]];
        
        if(createdTodayArray.count > 0)
            [self.navigationController pushViewController:portionListVC animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 75;
}

#pragma mark - Methods

-(void) categorizePortions{
    
    NSPredicate *allPortionsPred = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@ AND isForApprovals == 0",SINGLETON_INSTANCE.staffTransId];
    
    allPortionsArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Portions" withPredicate:allPortionsPred];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSPredicate *createdTodayPred = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@ AND portionDate MATCHES %@ AND isForApprovals == 0",SINGLETON_INSTANCE.staffTransId,[SINGLETON_INSTANCE stringFromDateWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:[NSDate date]]];
        
        createdTodayArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Portions" withPredicate:createdTodayPred];
        
        [self.tableView reloadData];
        
        [self.collectionView reloadData];
    });
}

// Method to get Portions Type Counts such as Approved, Declined, Pending
-(NSString *)retrieveCountOfAssignmentTypeWithID:(NSInteger)typeID{
    
    if (allPortionsArray.count>0 ){
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"approvedStatus == %d",typeID];
        
        NSArray *filteredArray = [allPortionsArray filteredArrayUsingPredicate:predicate];
        
        return [NSString stringWithFormat:@"%lu",(unsigned long)filteredArray.count];
    }
    else
        return @"0";
}

// Method to Get Array of Portions with Type
-(NSArray *)retrieveArrayOfAssignmentTypeWithID:(NSInteger)typeID{
    
    if (allPortionsArray.count>0){
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"approvedStatus == %d",typeID];
        
        NSArray *filteredArray = [allPortionsArray filteredArrayUsingPredicate:predicate];
        
        NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:NO];
        
        filteredArray = [filteredArray sortedArrayUsingDescriptors:@[dateDescriptor]];

        
        return filteredArray;
    }
    else
        return nil;
}

#pragma mark - Fetch Portions List API Call

// Service Call to fetch Portions list
-(void) fetchPortionsListServiceCall{
    
    NSString *urlString = @"MastersMServices/PortionsService.svc/FetchPortionsByStaff";
    
    NSDictionary *param = @{@"guidStaffTransID":SINGLETON_INSTANCE.staffTransId};
    
    BOOL showIndicator = NO;
    
    if(allPortionsArray.count == 0)
        showIndicator = YES;
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:showIndicator onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if([responseDictionary[@"FetchPortionsByStaffResult"][@"ResponseCode"] intValue] == 1) {
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                    [[Portions instance]storePortionsForStaffObj:SINGLETON_INSTANCE.staff withDict:responseDictionary isForApprovals:NO];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self categorizePortions];
                        
                    });
                    
                });
            }
        }
    }];
}
@end