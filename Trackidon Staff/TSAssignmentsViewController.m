//
//  TSAssignmentsViewController.m
//  Trackidon Staff
//
//  Created by ORGware on 23/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSAssignmentsViewController.h"

#define ViewAllAssignments @"View All"
#define DuesToday @"Dues Today"
#define CreatedToday @"Created Today"

@interface TSAssignmentsViewController (){

    NSMutableDictionary *listDict;
    
    NSString *title;
    
    NSArray *allAssignmentsArray;
}
@end

@implementation TSAssignmentsViewController

#pragma mark - View lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_ASSIGNMENTOPENED parameter:[NSMutableDictionary new]];
    
    listDict = [NSMutableDictionary new];
    
    [self.tableView setTableFooterView:[UIView new]];
    
    [TSSingleton layerDrawForView:self.collectionView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    [self categorizeAssignments];
    
    [self fetchAssignmentsListServiceCall];
    
    NSLog(@"%@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
    
    title = SINGLETON_INSTANCE.barTitle;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_ASSIGNMENTHOME parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = title;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Button Actions

- (IBAction)createAssignmentsBtnAction:(id)sender {
    
    if ([SINGLETON_INSTANCE checkForActiveInternet]) {
        
        [SINGLETON_INSTANCE fetchFilterAndReceipientDetailsWithMenuId:SINGLETON_INSTANCE.currentMenuId showIndicator:YES onCompletion:nil];
        
        [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_CREATE_INHOME parameter:[NSMutableDictionary new]];

        
        TSCreateAssignmentViewController *createAssignmentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBCreateAssignment"];
        
        [self.navigationController pushViewController:createAssignmentVC animated:YES];
    }
    else{
        
        [TSSingleton showAlertWithMessage:TEXT_NOINTERNET];
    }
    
}

#pragma mark - Collection view Datasource & Delegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 3;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"collectionViewCell";
    
    UICollectionViewCell *collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UIImageView *placeholderImgVw = (UIImageView *)[collectionViewCell viewWithTag:1000];
    
    UILabel *textLabel = (UILabel *)[collectionViewCell viewWithTag:1001];
    
    UILabel *countLabel = (UILabel *)[collectionViewCell viewWithTag:1002];
    
    [TSSingleton layerDrawForView:collectionViewCell.contentView position:LAYER_LEFT color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:collectionViewCell.contentView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    switch (indexPath.row) {
            
        case 0:
            placeholderImgVw.image = [UIImage imageNamed:@"approved"];
            textLabel.text = @"Approved";
            countLabel.textColor = COLOR_ACCEPT_CREATEDTODAY;
            countLabel.text = [self retrieveCountOfAssignmentTypeWithID:StatusTypeApproved];
            break;
            
        case 1:
            placeholderImgVw.image = [UIImage imageNamed:@"pending"];
            textLabel.text = @"Pending";
            countLabel.text = [self retrieveCountOfAssignmentTypeWithID:StatusTypePending];
            countLabel.textColor = COLOR_PENDING_VIEWALL;
            break;
            
        case 2:
            placeholderImgVw.image = [UIImage imageNamed:@"declined"];
            textLabel.text = @"Declined";
            countLabel.text = [self retrieveCountOfAssignmentTypeWithID:StatusTypeDeclined];
            countLabel.textColor = COLOR_DECLINED_DUESTODAY;
            break;
            
        default:
            break;
    }
    
    collectionViewCell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    collectionViewCell.layer.borderWidth = 0.35f;
    
    return collectionViewCell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    UILabel *countLbl = (UILabel *)[cell viewWithTag:1002];
    
    if ([countLbl.text intValue]>0) {
        
        TSAssignmentListTableViewController *assignmentListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAssignmentList"];
        
        NSArray *assignmentListArr = [NSArray new];
        
        switch (indexPath.row) {
                
            case 0:
                assignmentListArr = [self retrieveArrayOfAssignmentTypeWithID:StatusTypeApproved];
                
                [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_VIEW_ASSIGNMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_APPROVED,ANALYTICS_PARAM_VIEWTYPE, nil]];
                
                SINGLETON_INSTANCE.barTitle = TEXT_APPROVED;
                
                break;
                
            case 1:
                assignmentListArr = [self retrieveArrayOfAssignmentTypeWithID:StatusTypePending];
                [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_VIEW_ASSIGNMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_PENDING,ANALYTICS_PARAM_VIEWTYPE, nil]];
                SINGLETON_INSTANCE.barTitle = TEXT_PENDING;
                break;
                
            case 2:
                assignmentListArr = [self retrieveArrayOfAssignmentTypeWithID:StatusTypeDeclined];
                [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_VIEW_ASSIGNMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_DECLINED,ANALYTICS_PARAM_VIEWTYPE, nil]];
                SINGLETON_INSTANCE.barTitle = TEXT_DECLINED;
                break;
                
            default:
                break;
        }
        
        
        
        [assignmentListVC setAssignmentsArray:assignmentListArr];
        
        [self.navigationController pushViewController:assignmentListVC animated:YES];
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
 
    //return CGSizeMake(DeviceWidth/3, 130);
    float width = DeviceWidth/3;
    float height = 130;//width*1.214;
    
    return CGSizeMake(width, height);
}

#pragma mark - Tableview Datasource & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return listDict.allKeys.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    UIImageView *placeHolderImgVw = (UIImageView *)[cell viewWithTag:100];
    
    UILabel *subMenuLabel = (UILabel *)[cell viewWithTag:101];

    UILabel *countLabel = (UILabel *)[cell viewWithTag:102];
    
//    NSString *subMenuTitle = [[listDict allKeys] objectAtIndex:indexPath.row];
//    
//    subMenuLabel.text = subMenuTitle;
//    
//    NSString *imageName;
//    
//    if ([subMenuTitle isEqualToString:ViewAllAssignments]) {
//        
//        imageName = @"assignment_overall";
//        
//        countLabel.textColor = COLOR_PENDING_VIEWALL;
//    }
//    else if ([subMenuTitle isEqualToString:DuesToday]){
//        
//        imageName = @"assignment_dues";
//        
//        countLabel.textColor = COLOR_DECLINED_DUESTODAY;
//    }
//    else{
//        
//        imageName = @"assignment_created";
//        
//        countLabel.textColor = COLOR_ACCEPT_CREATEDTODAY;
//    }
//    
//    placeHolderImgVw.image = [UIImage imageNamed:imageName];
//    
//    countLabel.text = [NSString stringWithFormat:@"%ld", (long)[[listDict objectForKey:subMenuTitle] count]];

    switch (indexPath.row) {
        
        case 0:
            
            subMenuLabel.text = ViewAllAssignments;
            placeHolderImgVw.image = [UIImage imageNamed: @"assignment_overall"];;
            countLabel.text = [NSString stringWithFormat:@"%ld", (long)[[listDict objectForKey:ViewAllAssignments] count]];
            countLabel.textColor = [UIColor colorWithRed:103.0/255.0 green:199.0/255.0 blue:226.0/255.0 alpha:1.0];
            
            break;
            
        case 1:
            
            subMenuLabel.text = CreatedToday;
            placeHolderImgVw.image = [UIImage imageNamed:@"assignment_created"];
            countLabel.text = [NSString stringWithFormat:@"%ld", (long)[[listDict objectForKey:CreatedToday] count]];
            countLabel.textColor = COLOR_PENDING_VIEWALL;
            break;
            
        case 2:
            
            subMenuLabel.text = DuesToday;
            placeHolderImgVw.image = [UIImage imageNamed:@"assignment_dues"];
            countLabel.text = [NSString stringWithFormat:@"%ld", (long)[[listDict objectForKey:DuesToday] count]];
            countLabel.textColor = COLOR_DECLINED_DUESTODAY;
            break;
            
        default:
            break;
    }
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SINGLETON_INSTANCE.barTitle = title;
    
//    NSString *subMenuTitle = [[listDict allKeys] objectAtIndex:indexPath.row];
//
//    [assignmentListVC setAssignmentsArray:[listDict objectForKey:subMenuTitle]];
    
    NSArray *assignmentsArray = [NSArray new];
    
    

    switch (indexPath.row) {
            
        case 0:
            
            assignmentsArray = [listDict objectForKey:ViewAllAssignments];
            
            [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_VIEW_ASSIGNMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_VIEWALL,ANALYTICS_PARAM_VIEWTYPE, nil]];
            
            break;
            
        case 1:
            assignmentsArray = [listDict objectForKey:CreatedToday];
            [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_VIEW_ASSIGNMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_CREATEDTODAY,ANALYTICS_PARAM_VIEWTYPE, nil]];
            break;
            
        case 2:
            assignmentsArray = [listDict objectForKey:DuesToday];
            [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_VIEW_ASSIGNMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_DUESTODAY,ANALYTICS_PARAM_VIEWTYPE, nil]];

            break;
            
        default:
            break;
    }
    
    
    if (assignmentsArray.count>0) {
        
        NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:NO];
        
        assignmentsArray = [assignmentsArray sortedArrayUsingDescriptors:@[dateDescriptor]];
        
        TSAssignmentListTableViewController *assignmentListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAssignmentList"];
        
        [assignmentListVC setAssignmentsArray:assignmentsArray];
        
        [self.navigationController pushViewController:assignmentListVC animated:YES];
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 75;
}

#pragma mark - Methods

-(void) categorizeAssignments{
    
    NSPredicate *allAssignmentsPred = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@",SINGLETON_INSTANCE.staffTransId];
    
    allAssignmentsArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Assignments" withPredicate:allAssignmentsPred];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSPredicate *createdTodayPred = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@ AND assignmentDate == %@",SINGLETON_INSTANCE.staffTransId,[SINGLETON_INSTANCE dateFromDate:[NSDate date] withFormate:DATE_FORMATE_SERVICE_RESPONSE]];
        
        NSArray *createdTodayArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Assignments" withPredicate:createdTodayPred];
        
        NSPredicate *duesTodayPred = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@ AND dueDate == %@",SINGLETON_INSTANCE.staffTransId,[SINGLETON_INSTANCE dateFromDate:[NSDate date] withFormate:DATE_FORMATE_SERVICE_RESPONSE]];
        
        NSArray *duesTodayArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Assignments" withPredicate:duesTodayPred];
        
        [listDict removeAllObjects];
        
        
    //    if (allAssignmentsArray.count>0) {
        
            [listDict setObject:allAssignmentsArray forKey:ViewAllAssignments];
    //    }
        
    //    if (createdTodayArray.count>0) {
        
            [listDict setObject:createdTodayArray forKey:CreatedToday];
    //    }
        
    //    if (duesTodayArray.count>0) {
        
            [listDict setObject:duesTodayArray forKey:DuesToday];
    //    }
        
        if (listDict.allKeys.count>0) {
            
            [self.tableView setHidden:NO];
        }
        else{
            
            [self.tableView setHidden:YES];
        }
        
        [self.tableView reloadData];
        
        [self.collectionView reloadData];
        
    });
    
}

// Method to get Assignment Type Counts such as Approved, Declined, Pending
-(NSString *)retrieveCountOfAssignmentTypeWithID:(NSInteger)typeID{
    
    if (listDict.allKeys.count>0 && [listDict objectForKey:ViewAllAssignments]){
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"approvedStatus == %d",typeID];
        
        NSArray *filteredArray = [[listDict objectForKey:ViewAllAssignments] filteredArrayUsingPredicate:predicate];
        
        return [NSString stringWithFormat:@"%lu",(unsigned long)filteredArray.count];
    }
    else
        return @"0";
}

// Method to Get Array of assignments with Type
-(NSArray *)retrieveArrayOfAssignmentTypeWithID:(NSInteger)typeID{
    
    if (listDict.allKeys.count>0 && [listDict objectForKey:ViewAllAssignments]){
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"approvedStatus == %d",typeID];
        
        NSArray *filteredArray = [[listDict objectForKey:ViewAllAssignments] filteredArrayUsingPredicate:predicate];
        
        NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:NO];
        
        filteredArray = [filteredArray sortedArrayUsingDescriptors:@[dateDescriptor]];
        
        return filteredArray;
    }
    else
        return nil;
}

#pragma mark - Fetch Assignments List API Call

// Service Call to fetch Assignments list
-(void) fetchAssignmentsListServiceCall{
    
    NSString *urlString = @"MastersMServices/AssignmentService.svc/FetchAssignmentsByStaff";
    
    NSDictionary *param = @{@"guidStaffTransID":SINGLETON_INSTANCE.staffTransId};
    
    BOOL showIndicator = NO;
    
    if(allAssignmentsArray.count == 0)
        showIndicator = YES;
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:showIndicator onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            NSLog(@"%@",responseDictionary);
            if([responseDictionary[@"FetchAssignmentsByStaffResult"][@"ResponseCode"] intValue] == 1) {
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                    [[Assignments instance]storeAssignmentsDataForStaff:SINGLETON_INSTANCE.staff withDict:responseDictionary andIsForApprovals:NO];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self categorizeAssignments];
                        
                    });
                });
            }
        }
    }];
}
@end