//
//  TSEventsApprovalsViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 02/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TSEventsApprovalsViewController;

@interface TSEventsApprovalsViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *eventsTableView;
@property (weak, nonatomic) IBOutlet UIView *selectionView;

// Button Actions
- (IBAction)approvedButtonTapped:(id)sender;
- (IBAction)declineButtonTapped:(id)sender;

@end

@interface ApprovalsModel : NSObject

@property(nonatomic,strong) NSString *transID;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,assign) NSInteger approvalStatus;
@property(nonatomic,assign) BOOL isSelected;

@end