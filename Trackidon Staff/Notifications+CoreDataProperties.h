//
//  Notifications+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 28/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Notifications.h"

NS_ASSUME_NONNULL_BEGIN

@interface Notifications (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *acadamicTransId;
@property (nullable, nonatomic, retain) NSString *approveByTransId;
@property (nullable, nonatomic, retain) NSString *approvedByName;
@property (nullable, nonatomic, retain) NSDate *approvedDate;
@property (nullable, nonatomic, retain) NSNumber *approvedStatus;
@property (nullable, nonatomic, retain) NSString *assignedList;
@property (nullable, nonatomic, retain) NSString *boardName;
@property (nullable, nonatomic, retain) NSString *boardTransID;
@property (nullable, nonatomic, retain) NSNumber *isForApprovals;
@property (nullable, nonatomic, retain) NSDate *modifiedDate;
@property (nullable, nonatomic, retain) NSString *notificationAttachmentUrl;
@property (nullable, nonatomic, retain) NSDate *notificationDate;
@property (nullable, nonatomic, retain) NSString *notificationDescription;
@property (nullable, nonatomic, retain) NSNumber *notificationGroupType;
@property (nullable, nonatomic, retain) NSNumber *notificationIsHasAttachment;
@property (nullable, nonatomic, retain) NSNumber *notificationPriority;
@property (nullable, nonatomic, retain) NSString *notificationTitle;
@property (nullable, nonatomic, retain) NSString *notificationTransId;
@property (nullable, nonatomic, retain) NSString *schoolTransId;
@property (nullable, nonatomic, retain) NSString *staffTransId;
@property (nullable, nonatomic, retain) NSString *userCreatedName;
@property (nullable, nonatomic, retain) NSString *userCreatedTransId;
@property (nullable, nonatomic, retain) NSString *userModifiedName;
@property (nullable, nonatomic, retain) NSString *userModifiedTransId;
@property (nullable, nonatomic, retain) StaffDetails *staff;

@end

NS_ASSUME_NONNULL_END
