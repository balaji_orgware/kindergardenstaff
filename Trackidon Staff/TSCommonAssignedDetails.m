//
//  TSCommonAssignedDetails.m
//  Trackidon Staff
//
//  Created by Elango on 27/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSCommonAssignedDetails.h"

@implementation AssigneeModel

@end

@implementation TSCommonAssignedDetails

+ (instancetype)instance
{
    static TSCommonAssignedDetails *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (&onceToken, ^{
        
        kSharedInstance = [[TSCommonAssignedDetails alloc]init];
        
    });
    
    return kSharedInstance;
}

- (NSString *)getAssignedDetailsWithArray:(NSArray *)assignedArr withGroupType:(NSString *)groupType{
    
    @try {
        
        NSString *assignedDetails = @"";
        
        NSMutableArray *assigneedDetailsArr = [NSMutableArray new];
        
        if ([assignedArr count] > 0 ) {
            
            for (NSDictionary *assignedDict in assignedArr)
            {
            //for (int i = 0; i < assignedArr.count; i++) {
                
                //NSDictionary *assignedDict = [assignedArr objectAtIndex:i];
                
                AssigneeModel *assignee = [[AssigneeModel alloc] init];
                
                NSString *boardName = assignedDict[@"BoardName"] != [NSNull null] ? assignedDict[@"BoardName"] : @"";
                
                NSString *groupName = assignedDict[@"GroupName"] != [NSNull null] ? assignedDict[@"GroupName"] : @"";
                
                NSString *className = assignedDict[@"ClassName"] != [NSNull null] ? assignedDict[@"ClassName"] : @"";
                
                NSString *sectionName = assignedDict[@"SectionName"] != [NSNull null] ? assignedDict[@"SectionName"] : @"";
                
                NSString *studentName = assignedDict[@"StudentName"] != [NSNull null] ? assignedDict[@"StudentName"] : @"";
                
                NSString *staffName = assignedDict[@"StaffName"] != [NSNull null] ? assignedDict[@"StaffName"] : @"";
                
                BOOL isStaff = [assignedDict[@"IsStaff"] integerValue] == 1 ? YES : NO ;
                
                
                if (groupName.length > 0) {//Particular Group
                    
                    assignedDetails = [NSString stringWithFormat:@"%@ - %@ (Group)",boardName,groupName];
                    
                    assignee.assigneeType = [NSNumber numberWithInteger:ASSIGN_TYPE_GROUP];
                    
                    assignee.assigneeName = assignedDetails;
                    
                } else if (studentName.length > 0) //Particular Student
                {
                    
                    assignedDetails = [NSString stringWithFormat:@"%@ - %@ - %@ - %@",boardName,studentName,className,sectionName];
                    
                    assignee.assigneeType = [NSNumber numberWithInteger:ASSIGN_TYPE_STUDENT];
                    
                    assignee.assigneeName = assignedDetails;
                    
                }else if (![sectionName isEqualToString:@""]) //Particular Section
                {
                    
                    assignedDetails = [NSString stringWithFormat:@"%@ - %@ - %@",boardName,className,sectionName];
                    
                    assignee.assigneeType = [NSNumber numberWithInteger:ASSIGN_TYPE_SECTION];
                    
                    assignee.assigneeName = assignedDetails;
                    
                }else if (![className isEqualToString:@""] && [sectionName isEqualToString:@""]) //Particular Class
                {
                    
                    NSRange range = [className rangeOfString:@" "];
                    
                    assignedDetails =[NSString stringWithFormat:@"%@ - %@ (All Section)",boardName,[className substringToIndex:range.location]];
                    
                    assignee.assigneeType = [NSNumber numberWithInteger:ASSIGN_TYPE_CLASS];
                    
                    assignee.assigneeName = assignedDetails;
                    
                    
                }else{//No Known Assigne
                    
                    assignedDetails = @"NA";
                    
                    assignee.assigneeType = [NSNumber numberWithInteger:ASSIGN_TYPE_UNKNOWN];
                    
                    assignee.assigneeName = assignedDetails;
                }
                
                if (isStaff && staffName.length >0) {//Staff
                    
                    if (boardName.length >0)
                        assignedDetails =[NSString stringWithFormat:@"%@ - %@ (Staff)",boardName ,staffName];
                    else
                        assignedDetails = [NSString stringWithFormat:@"%@ (Staff)",staffName];
                    
                    assignee.assigneeType = [NSNumber numberWithInteger:ASSIGN_TYPE_STAFF];
                    
                    assignee.assigneeName = assignedDetails;
                    
                } else if ([[NSString stringWithFormat:@"%@",groupType] isEqualToString:RECEPIENT_TYPE_ENTIRESCHOOL] || [[NSString stringWithFormat:@"%@",groupType] isEqualToString:RECEPIENT_TYPE_ALLSTUDENT]){//All School or All Student
                    
                    if ([[NSString stringWithFormat:@"%@",groupType] isEqualToString:RECEPIENT_TYPE_ENTIRESCHOOL]){
                        
                        assignedDetails =[NSString stringWithFormat:@"%@ - Entire School",boardName];
                        
                        assignee.assigneeType = [NSNumber numberWithInteger:ASSIGN_TYPE_ENTIRESCHOOL];
                        
                        assignee.assigneeName = assignedDetails;
                        
                    }else {
                        
                        assignedDetails =[NSString stringWithFormat:@"%@ - All Student",boardName];
                        
                        assignee.assigneeType = [NSNumber numberWithInteger:ASSIGN_TYPE_ALLSTUDENT];
                        
                        assignee.assigneeName = assignedDetails;
                    }
                }
                
                [assigneedDetailsArr addObject:assignee];
                
                assignee = nil;
            }
            
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"assigneeType" ascending:YES];
            
            assigneedDetailsArr = [[assigneedDetailsArr sortedArrayUsingDescriptors:@[descriptor]] mutableCopy];
            
            return [[assigneedDetailsArr valueForKey:@"assigneeName"] componentsJoinedByString:ASSIGNEE_SAPRATOR];
        
        }
        
        return nil;
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    }
}

@end
