//
//  TSHandlerClass.h
//  Trackidon Staff
//
//  Created by ORGware on 17/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import <MBProgressHUD.h>
#import "TSHeaders.pch"
#import "TSConstants.h"
#import "Reachability.h"

typedef void(^completionHandler) (BOOL success, NSDictionary *responseDictionary);

typedef void (^downloadHandler)(BOOL success,NSURL *filePath);

@interface TSHandlerClass : NSObject <NSURLSessionTaskDelegate>

+(instancetype) sharedHandler;

-(void) postRequestWithURL:(NSString *)urlString baseURL:(NSString *)baseURL param:(NSDictionary *)params currentView:(UIView *)view showIndicator:(BOOL)showIndicator onCompletion:(completionHandler)handler;

//-(void) postRequestWithURLSession:(NSString *)urlString baseURL:(NSString *)baseURL param:(NSDictionary *)params currentView:(UIView *)view showIndicator:(BOOL)showIndicator onCompletion:(completionHandler)handler;

@property (nonatomic,strong) NSURL *attachmentUrl;

//@property (nonatomic,retain) MBProgressHUD *hud;

@property (nonatomic,strong) NSURLSession *urlSession;

- (void)downloadAttachmentWithURL:(NSString *)attachmentURL currentView:(UIView *)view withHandler:(downloadHandler)downloadHandler;

@end
