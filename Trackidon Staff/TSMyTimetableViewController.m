//
//  TSMyTimetableViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 23/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSMyTimetableViewController.h"

@interface TSMyTimetableViewController (){

    NSMutableArray *timeArray,*timeTableDataArray;
    
    NSString *selectedDay;
    
    UIBarButtonItem *updateButton;
    
    NSString *dayName;
}
@end

@implementation TSMyTimetableViewController

#pragma mark - View lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    timeTableDataArray = [NSMutableArray new];
    
    [self setSelectedDateAsToday];
    
    timeArray = [[NSMutableArray alloc] initWithObjects:@"08.30 AM - 09.15 AM",@"09.15 AM - 10.00 AM",@"10.15 AM - 11.00 AM",@"11.00 AM - 11.45 AM",@"01.00 PM - 01.45 PM",@"01.45 PM - 02.30 PM",@"02.45 PM - 03.30 PM",@"03.30 PM - 04.15 PM",@"04.15 PM - 05.00 PM", nil];
    
    updateButton = [[UIBarButtonItem alloc]initWithTitle:@"Update" style:UIBarButtonItemStyleDone target:self action:@selector(updateButtonAction)];
    
    [self.timeTableTableView setTableFooterView:[UIView new]];
    
    [self categorizeTimeTable];
    
    [self fetchTimeTableServiceCall];

}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_MYTIMETABLE parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    [self categorizeTimeTable];
    
    if (SINGLETON_INSTANCE.timetableNeedsUpdate) {
        
        self.navigationItem.rightBarButtonItem = updateButton;
    }
    else{
        
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"My Timetable";
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

#pragma mark - Button Actions

-(void) updateButtonAction{
    
    Timetable *timeTableObj = [self getPeriodsForDay:selectedDay];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    [param setValue:SINGLETON_INSTANCE.academicTransId forKey:@"guidAcadamicTransID"];
    
    [param setValue:SINGLETON_INSTANCE.selectedSchoolTransId forKey:@"guidSchoolTransID"];
    
    [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidTeacherTransID"];
    
    [param setValue:timeTableObj.dayTransID forKey:@"guidDayTransID"];
    
    [param setValue:timeTableObj.periodOneClassTransID forKey:@"guidIPeriodClass"];
    
    [param setValue:timeTableObj.periodOneSecTransID forKey:@"guidIPeriodSection"];
    
    [param setValue:timeTableObj.periodOneTransID forKey:@"guidIPeriod"];
    
    
    [param setValue:timeTableObj.periodTwoClassTransID forKey:@"guidIIPeriodClass"];
    
    [param setValue:timeTableObj.periodTwoSecTransID forKey:@"guidIIPeriodSection"];
    
    [param setValue:timeTableObj.periodTwoTransID forKey:@"guidIIPeriod"];
    
    
    [param setValue:timeTableObj.periodThreeClassTransID forKey:@"guidIIIPeriodClass"];
    
    [param setValue:timeTableObj.periodThreeSecTransID forKey:@"guidIIIPeriodSection"];
    
    [param setValue:timeTableObj.periodThreeTransID forKey:@"guidIIIPeriod"];
    
    
    [param setValue:timeTableObj.periodFourClassTransID forKey:@"guidIVPeriodClass"];
    
    [param setValue:timeTableObj.periodFourSecTransID forKey:@"guidIVPeriodSection"];
    
    [param setValue:timeTableObj.periodFourTransID forKey:@"guidIVPeriod"];
    
    
    [param setValue:timeTableObj.periodFiveClassTransID forKey:@"guidVPeriodClass"];
    
    [param setValue:timeTableObj.periodFiveSecTransID forKey:@"guidVPeriodSection"];
    
    [param setValue:timeTableObj.periodFiveTransID forKey:@"guidVPeriod"];
    
    
    [param setValue:timeTableObj.periodSixClassTransID forKey:@"guidVIPeriodClass"];
    
    [param setValue:timeTableObj.periodSixSecTransID forKey:@"guidVIPeriodSection"];
    
    [param setValue:timeTableObj.periodSixTransID forKey:@"guidVIPeriod"];
    
    
    [param setValue:timeTableObj.periodSevenClassTransID forKey:@"guidVIIPeriodClass"];
    
    [param setValue:timeTableObj.periodSevenSecTransID forKey:@"guidVIIPeriodSection"];
    
    [param setValue:timeTableObj.periodSevenTransID forKey:@"guidVIIPeriod"];
    
    
    [param setValue:timeTableObj.periodEightClassTransID forKey:@"guidVIIIPeriodClass"];
    
    [param setValue:timeTableObj.periodEightSecTransID forKey:@"guidVIIIPeriodSection"];
    
    [param setValue:timeTableObj.periodEightTransID forKey:@"guidVIIIPeriod"];
    
    
    [param setValue:timeTableObj.specialClassClassTransID forKey:@"guidSpecialClassClass"];
    
    [param setValue:timeTableObj.specialClassSecTransID forKey:@"guidSpecialClassSection"];
    
    [param setValue:timeTableObj.specialClassTransID forKey:@"guidSpecialClass"];
    
    [param setValue:@"10" forKey:@"Period"];

    [self updateTimeTableServiceCall:param];
}

- (IBAction)dayButtonTapped:(id)sender {
    
    if (self.navigationItem.rightBarButtonItem == nil) {
   
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        
        [sender setSelected:NO];
        
        [self setNormalTextColorsToAllButtons];
        
        [sender setTitleColor:APPTHEME_COLOR forState:UIControlStateNormal];
        
        switch ([sender tag]) {
                
            case 10:
                
                selectedDay = @"MON";
                
                dayName = @"Monday";
                
                break;
                
            case 11:
                
                selectedDay = @"TUE";
                
                dayName = @"Tuesday";
                
                break;
                
            case 12:
                
                selectedDay = @"WED";
                
                dayName = @"Wednesday";
                
                break;
                
                
            case 13:
                
                selectedDay = @"THU";
                
                dayName = @"Thursday";
                
                break;
                
            case 14:
                
                selectedDay = @"FRI";
                
                dayName = @"Friday";
                
                break;
                
            case 15:
                
                selectedDay = @"SAT";
                
                dayName = @"Saturday";
                
                break;
                
            default:
                break;
        }
        
        NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
        
        [analyticsDict setValue:dayName forKey:ANALYTICS_PARAM_DAY];
        
        [analyticsDict setValue:ANALYTICS_VALUE_MYTIMETABLE forKey:ANALYTICS_PARAM_TYPE];
        
        [TSAnalytics logEvent:ANALYTICS_TIMETABLE_CLICKED_DAY parameter:analyticsDict];
        
        Timetable *timeTableObj = [self getPeriodsForDay:selectedDay];
        
        if (timeTableObj !=nil){
            
            [_timeTableTableView setHidden:NO];
            
            [_timeTableTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
            
            [_timeTableTableView reloadData];
        }
        else{
            
            [_timeTableTableView setHidden:YES];
        }
        
    }
    else{
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Unsaved changes" message:@"Do you want to update the changes ?" delegate:self cancelButtonTitle:@"Discard" otherButtonTitles:@"Update",nil];
        
        [alertView setTag:100];
        
        [alertView show];
    }
    
}

#pragma mark - AlertView Delegates

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 100) {
        
        if (buttonIndex) {
            
            [self updateButtonAction];
        }
        else{
            
            self.navigationItem.rightBarButtonItem = nil;
            
            [self fetchTimeTableServiceCall];
        }
    }
    
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

#pragma mark - Tableview Datasource & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    Timetable *timeTableObj = [self getPeriodsForDay:selectedDay];

    if (timeTableObj == nil) {
        
        [self.timeTableTableView setHidden:YES];
    }
    else{
        
        [self.timeTableTableView setHidden:NO];
    }
    
    return 9;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sCellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UILabel *rowCountLabel = (UILabel *)[cell viewWithTag:101];
    
    UILabel *subjectNameLabel = (UILabel *)[cell viewWithTag:102];
    
    UILabel *durationLabel = (UILabel *)[cell viewWithTag:103];
    
    UIImageView *notesImgVw = (UIImageView *)[cell viewWithTag:104];
    
    Timetable *timeTableObj = [self getPeriodsForDay:selectedDay];
    
    if (timeTableObj !=nil) {
        
        switch (indexPath.row) {
                
            case 0:
                
                subjectNameLabel.text = timeTableObj.periodOne;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:255 andBlue:137 andGreen:139];
                break;
                
            case 1:
                
                subjectNameLabel.text = timeTableObj.periodTwo;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:255 andBlue:186 andGreen:139];
                break;
                
            case 2:
                
                subjectNameLabel.text = timeTableObj.periodThree;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:103 andBlue:131 andGreen:175];
                break;
                
            case 3:
                
                subjectNameLabel.text = timeTableObj.periodFour;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:112 andBlue:210 andGreen:178];
                break;
                
            case 4:
                
                subjectNameLabel.text = timeTableObj.periodFive;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:68 andBlue:214 andGreen:251];
                break;
                
            case 5:
                
                subjectNameLabel.text = timeTableObj.periodSix;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:107 andBlue:101 andGreen:186];
                break;
                
            case 6:
                
                subjectNameLabel.text = timeTableObj.periodSeven;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:163 andBlue:51 andGreen:183];
                break;
                
            case 7:
                
                subjectNameLabel.text = timeTableObj.periodEight;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:0 andBlue:152 andGreen:94];
                break;
                
            case 8:
                
                subjectNameLabel.text = timeTableObj.specialClass;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:72 andBlue:72 andGreen:72];
                break;
                
            default:
                break;
        }
    }else{
        
        subjectNameLabel.text = @"No Data Found";
    }
    
    rowCountLabel.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    
    if (indexPath.row <timeArray.count-1)
        durationLabel.text = [timeArray objectAtIndex:indexPath.row];
    
    else
        durationLabel.text = @"Special Class";
    
    
    rowCountLabel.layer.cornerRadius = rowCountLabel.frame.size.width/2;
    
    rowCountLabel.layer.masksToBounds = YES;
    
    [notesImgVw setHidden:YES];
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [TSAnalytics logEvent:ANALYTICS_TIMETABLE_CLICKED_CHANGEPERIOD parameter:[NSMutableDictionary new]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UINavigationController *navCtrl = [self.storyboard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
    
    TSCreateTimeTableViewController *createTimetableVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBCreateTimetable"];
    
    Timetable *timeTableObj = [self getPeriodsForDay:selectedDay];
    
    if(timeTableObj != nil){
        
        [createTimetableVC setTimetableObj:timeTableObj];
        
        [createTimetableVC setSelectedDay:selectedDay];
        
        [createTimetableVC setSelectedPeriod:[NSString stringWithFormat:@"%ld",indexPath.row+1]];

        [navCtrl setViewControllers:@[createTimetableVC]];
        
        [self.navigationController presentViewController:navCtrl animated:YES completion:nil];
    }
}

#pragma mark - Methods

-(UIColor *)getUIColorFromRGB:(int)red andBlue:(int)blue andGreen:(int)green{
    
    return [UIColor colorWithRed:red/255.0 green:blue/255.0 blue:green/255.0 alpha:1.0];
}

// Method to filter Particular day record from DB
-(Timetable *)getPeriodsForDay:(NSString *)daySelected{
    
    NSPredicate *periodCountPred = [NSPredicate predicateWithFormat:@"day MATCHES %@",daySelected];
    
    NSArray *filteredArray = [timeTableDataArray filteredArrayUsingPredicate:periodCountPred];
    
    if (filteredArray.count>0)
        return [filteredArray objectAtIndex:0];
    else
        return nil;
}

// Method to change Button Colors for all day buttons
-(void) setNormalTextColorsToAllButtons{
    
    for (id isBtn in self.daysView.subviews) {
        
        if ([isBtn isKindOfClass:[UIButton class]]) {
            
            UIButton *daysBtn = (UIButton *)isBtn;
            
            if ([daysBtn tag] != 0) {
                
                [daysBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                
                [TSSingleton layerDrawForView:daysBtn position:LAYER_TOP color:[UIColor lightGrayColor]];
                
                //[TPSingleton layerDrawForView:daysBtn position:LAYER_RIGHT color:[UIColor lightGrayColor]];
            }
        }
    }
}

// Fetch Data from DB
-(void) categorizeTimeTable{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@",SINGLETON_INSTANCE.staffTransId];
    
    [timeTableDataArray removeAllObjects];
    
    timeTableDataArray = [NSMutableArray arrayWithArray:[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Timetable" withPredicate:predicate]];
    
    if (timeTableDataArray.count>0) {
        
        [_daysView setHidden:NO];
        
        [_timeTableTableView setHidden:NO];
    }
    else{
        
        [_daysView setHidden:YES];
        
        [_timeTableTableView setHidden:YES];
    }
    
    [self.timeTableTableView reloadData];
}

// Set Selected Date as Today date. Change Day Button Text colors
-(void) setSelectedDateAsToday{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [calendar components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
    
    UIButton *button;
    
    switch ([components weekday]) {
            
        case 2:
            
            selectedDay = @"MON";
            
            dayName = @"Monday";
            
            button = (UIButton *)[self.view viewWithTag:10];
            
            break;
            
        case 3:
            
            selectedDay = @"TUE";
            
            dayName = @"Tuesday";
            
            button = (UIButton *)[self.view viewWithTag:11];
            
            break;
            
        case 4:
            
            selectedDay = @"WED";
            
            dayName = @"Wednesday";
            
            button = (UIButton *)[self.view viewWithTag:12];
            
            break;
            
        case 5:
            
            selectedDay = @"THU";
            
            dayName = @"Thursday";
            
            button = (UIButton *)[self.view viewWithTag:13];
            
            break;
            
        case 6:
            
            selectedDay = @"FRI";
            
            dayName = @"Friday";
            
            button = (UIButton *)[self.view viewWithTag:14];
            
            break;
            
        case 7:
            
            selectedDay = @"SAT";
            
            dayName = @"Saturday";
            
            button = (UIButton *)[self.view viewWithTag:15];
            
            break;
            
        default:
            
            selectedDay = @"MON";
            
            dayName = @"Monday";
            
            button = (UIButton *)[self.view viewWithTag:10];
            
            break;
    }
    
    [button setTitleColor:APPTHEME_COLOR forState:UIControlStateNormal];
    
    [self.timeTableTableView reloadData];
}

#pragma mark - Service Calls

-(void) fetchTimeTableServiceCall{
    
    NSString *urlString = @"MastersMServices/StaffTimeTableService.svc/FetchTimeTableByStaffTransID";
    
    NSDictionary *param = @{@"guidStaffTransID":SINGLETON_INSTANCE.staffTransId};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:NO onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
        if([responseDictionary[@"FetchTimeTableByStaffTransIDResult"][@"ResponseCode"] intValue] ==1){
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [[Timetable instance]storeTimeTableForStaff:SINGLETON_INSTANCE.staff withDict:responseDictionary];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self setNormalTextColorsToAllButtons];
                    
                    [self setSelectedDateAsToday];
                    
                    [self categorizeTimeTable];
                    
                });
            });
            }
        }
    }];
}

-(void)updateTimeTableServiceCall :(NSDictionary *)param{
    
    NSString *urlString = @"MastersMServices/StaffTimeTableService.svc/UpdateStaffTimeTable";
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            SINGLETON_INSTANCE.timetableNeedsUpdate = NO;
            
            if ([responseDictionary[@"UpdateStaffTimeTableResult"][@"ResponseCode"] intValue] == 1) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:dayName forKey:ANALYTICS_PARAM_DAY];
                
                [analyticsDict setValue:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_TIMETABLE_CLICKED_UPDATE parameter:analyticsDict];
                
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"UpdateStaffTimeTableResult"][@"ResponseMessage"]];
                
                self.navigationItem.rightBarButtonItem = nil;
                
                [self setNormalTextColorsToAllButtons];
                
                [self setSelectedDateAsToday];
                
                [self categorizeTimeTable];
            }
            else{
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:dayName forKey:ANALYTICS_PARAM_DAY];
                
                [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_TIMETABLE_CLICKED_UPDATE parameter:analyticsDict];
            }
        }
        else{
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            [analyticsDict setValue:dayName forKey:ANALYTICS_PARAM_DAY];
            
            [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
            
            [TSAnalytics logEvent:ANALYTICS_TIMETABLE_CLICKED_UPDATE parameter:analyticsDict];
        }
    }];
}
@end