//
//  TSAssignmentApprovalDescViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 05/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Assignments;

@interface TSAssignmentApprovalDescViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *assignmentTitleLbl;

@property (weak, nonatomic) IBOutlet UILabel *subjectLbl;

@property (weak, nonatomic) IBOutlet UILabel *typeLbl;

@property (weak, nonatomic) IBOutlet UIView *topView;

@property (weak, nonatomic) IBOutlet UIButton *attachmentButton;

@property (weak, nonatomic) IBOutlet UILabel *assignedTeacherLabel;

@property (weak, nonatomic) IBOutlet UIView *dateView;

@property (weak, nonatomic) IBOutlet UILabel *assignedDateLabel;

@property (weak, nonatomic) IBOutlet UILabel *dueDateLabel;

@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

@property (weak, nonatomic) IBOutlet UIView *assignmentCountView;

@property (weak, nonatomic) IBOutlet UILabel *assignmentRemainingCountLbl;

@property (weak, nonatomic) IBOutlet UILabel *assignedLbl;

@property (weak, nonatomic) IBOutlet UILabel *dueLbl;

@property (weak, nonatomic) IBOutlet UITextField *assignedToTxtFld;

@property (weak, nonatomic) IBOutlet UIView *assignedToView;

@property (nonatomic,strong) Assignments *assignmentObj;

@end
