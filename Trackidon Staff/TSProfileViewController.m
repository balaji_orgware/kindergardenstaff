//
//  TSProfileViewController.m
//  Trackidon Staff
//
//  Created by Elango on 17/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSProfileViewController.h"

@interface TSProfileViewController ()<AttachmentPikerDelegate>
{
    NSData *imageData;
    
    NSString *encodedString;
}
@end

@implementation TSProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [TSAnalytics logEvent:ANALYTICS_PROFILE_OPENED parameter:[NSMutableDictionary new]];
    
    UITapGestureRecognizer *staffImageViewTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(staffImgViewTapped)];
    
    _staffImgView.userInteractionEnabled=YES;
    
    _staffImgView.layer.cornerRadius = _staffImgView.frame.size.height/2;
    
    _staffImgView.layer.masksToBounds = YES;
    
    [_staffImgView addGestureRecognizer:staffImageViewTap];
    
    [self fetchStaffDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_PROFILEHOME parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Profile";
}

-(void) fetchStaffDetails{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId MATCHES %@",SINGLETON_INSTANCE.staffTransId];
    
    NSArray *staffDetailArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"StaffDetails" withPredicate:predicate];
    
    if (staffDetailArray.count>0) {
        
        StaffDetails *staffObj = [staffDetailArray objectAtIndex:0];
        
        _nameTxtLbl.text = staffObj.name;
        
        _phoneTextLbl.text = [staffObj.mobileNo isEqualToString:@""]?@"-":staffObj.mobileNo;
        
        _emailTxtLbl.text = [staffObj.emailId isEqualToString:@""]?@"-":staffObj.emailId;
        
        _staffImgView.image = [UIImage imageWithData:staffObj.profileImageData];
        
        if(_staffImgView.image == nil)
            _staffImgView.image = [UIImage imageNamed:@"no_image"];
    }
}


-(void)staffImgViewTapped{
    
    [TSAnalytics logEvent:ANALYTICS_PROFILE_CHENGEDPROFILEIMAGE parameter:[NSMutableDictionary new]];
    
//    [TSAttachmentPicker instance].delegate = self;
//    
//    [[TSAttachmentPicker instance] showAttachmentPickerForView:self frame:_staffImgView.frame];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose Photo", nil];
    
    if (IS_IPAD) {
        
        [actionSheet showFromRect:[_staffImgView frame] inView:self.view animated:YES];
        
    }else{
        
        [actionSheet showInView:self.view];
        
    }

    
}

#pragma mark - UIActionSheet Delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex==0) {
        
        NSLog(@"Take a Photo");
        
        NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
        
        [analyticsDict setValue:ANALYTICS_VALUE_CAMERA forKey:ANALYTICS_PARAM_SOURCE];
        
        [TSAnalytics logEvent:ANALYTICS_PROFILE_CLICKEDIMAGEOPTION parameter:analyticsDict];
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        
        picker.delegate = self;
        
        picker.allowsEditing = YES;
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self presentViewController:picker animated:YES completion:NULL];
        }];
    }
    else if(buttonIndex ==1){
        
        NSLog(@"choose from library");
        
        NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
        
        [analyticsDict setValue:ANALYTICS_VALUE_GALLERY forKey:ANALYTICS_PARAM_SOURCE];
        
        [TSAnalytics logEvent:ANALYTICS_PROFILE_CLICKEDIMAGEOPTION parameter:analyticsDict];
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        
        picker.delegate = self;
        
        picker.allowsEditing = YES;
        
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self presentViewController:picker animated:YES completion:NULL];
        }];
    }
    else{
        
        NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
        
        [analyticsDict setValue:ANALYTICS_VALUE_CANCEL forKey:ANALYTICS_PARAM_SOURCE];
        
        [TSAnalytics logEvent:ANALYTICS_PROFILE_CLICKEDIMAGEOPTION parameter:analyticsDict];
        
    }
}


//#pragma mark - Attachment picker Delegate
//
//- (void)selectedAttachmentImageData:(NSData *)imageData imageName:(NSString *)imageName isHasAttachment:(BOOL)isHasAttachment{
//    
//    UIImage *image = [UIImage imageWithData:imageData];
//    
//    _staffImgView.image = image;
//}
//
//- (void)selectedAttachmentImageName:(NSString *)imageName{
//    
//}

#pragma mark - ImagePicker Delegate

//***** Image Selection and update parent profile service call *****//

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Finish");
    
    _staffImgView.image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    
    imageData = UIImageJPEGRepresentation(_staffImgView.image, 0.0f);
    
    encodedString =  [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidTransID"];
    
    //[param setValue:@"2298ebb4-7192-4494-b19a-f29cf5fff82d" forKey:@"guidTransID"];
    
    [param setValue:[@"data:image/jpeg;base64,"stringByAppendingString:[encodedString stringByReplacingOccurrencesOfString:@"\n" withString:@""]] forKey:@"strProfileImage"];
    
    [param setValue:@"9" forKey:@"TypeID"]; //28 student image type id
    
    // 9 For parent
    NSLog(@"param....%@",param);
    
    [self updateStaffProfileImage:@"MastersMServices/AccountsMService.svc/UpdateProfilePhoto" params:param];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


#pragma mark - Service call

-(void)updateStaffProfileImage:(NSString *)url params:(NSMutableDictionary *)param{
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:url baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
      
        if (success) {
            
            NSLog(@"%@",responseDictionary);
            
            if ([responseDictionary[@"UpdateProfilePhotoResult"][@"ResponseCode"] boolValue]) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [analyticsDict setValue:responseDictionary[@"UpdateProfilePhotoResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                
                [TSAnalytics logEvent:ANALYTICS_PROFILE_UPLOADIMAGE parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"UpdateProfilePhotoResult"][@"ResponseMessage"]];
                
                [[StaffDetails instance]updateStaffImageWithData:imageData parentTransId:SINGLETON_INSTANCE.staffTransId];
                
            }
            else{
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [analyticsDict setValue:responseDictionary[@"UpdateProfilePhotoResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                
                [TSAnalytics logEvent:ANALYTICS_PROFILE_UPLOADIMAGE parameter:analyticsDict];
            }

            
        }else{
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
            
            [analyticsDict setValue:ANALYTICS_VALUE_SERVICEFAILURE_RESPONSEMSG forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
            
            [TSAnalytics logEvent:ANALYTICS_PROFILE_UPLOADIMAGE parameter:analyticsDict];
             
            [TSSingleton requestTimeOutErrorAlert];
        }
        
    }];
    
}

@end
