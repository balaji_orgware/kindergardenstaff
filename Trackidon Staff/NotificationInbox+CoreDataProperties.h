//
//  NotificationInbox+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 28/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "NotificationInbox.h"

NS_ASSUME_NONNULL_BEGIN

@interface NotificationInbox (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *assignedList;
@property (nullable, nonatomic, retain) NSString *createdBy;
@property (nullable, nonatomic, retain) NSNumber *isHasAttachment;
@property (nullable, nonatomic, retain) NSDate *modifiedDate;
@property (nullable, nonatomic, retain) NSString *notificationAttachment;
@property (nullable, nonatomic, retain) NSDate *notificationDate;
@property (nullable, nonatomic, retain) NSString *notificationDescription;
@property (nullable, nonatomic, retain) NSString *notificationTitle;
@property (nullable, nonatomic, retain) NSNumber *priority;
@property (nullable, nonatomic, retain) NSString *staffTransID;
@property (nullable, nonatomic, retain) NSString *transID;
@property (nullable, nonatomic, retain) StaffDetails *staff;

@end

NS_ASSUME_NONNULL_END
