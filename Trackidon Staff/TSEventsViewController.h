//
//  TSEventsViewController.h
//  Trackidon Staff
//
//  Created by Elango on 19/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSEventsViewController : UIViewController

@property (nonatomic,retain) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic,assign) NSInteger listType;

@property (weak, nonatomic) IBOutlet UITableView *eventsTblVw;

@property (nonatomic,strong) NSArray *eventsArray;


@end
