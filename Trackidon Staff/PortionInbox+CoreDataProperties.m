//
//  PortionInbox+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 28/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PortionInbox+CoreDataProperties.h"

@implementation PortionInbox (CoreDataProperties)

@dynamic approvedByName;
@dynamic assignedList;
@dynamic createdBy;
@dynamic isHasAttachment;
@dynamic modifiedDate;
@dynamic portionsAttachment;
@dynamic portionsDate;
@dynamic portionsDescription;
@dynamic portionsTitle;
@dynamic staffTransID;
@dynamic subjectName;
@dynamic transID;
@dynamic staff;

@end
