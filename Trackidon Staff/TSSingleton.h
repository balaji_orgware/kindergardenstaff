//
//  TSSingleton.h
//  Trackidon Staff
//
//  Created by ORGware on 16/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@class StaffDetails;

typedef void (^onBoardFetchCompletion) (BOOL isSuccess);
typedef void (^onReceiverFetchCompletion) (BOOL isSuccess);
typedef void (^onGroupListFetchCompletion) (BOOL isSuccess);
typedef void (^onStaffListFetchCompletion) (BOOL isSuccess);
typedef void (^onFilterAndReceipientFetchCompletion) (BOOL isSuccess);


@interface TSSingleton : NSObject 

@property (nonatomic, strong) NSMutableArray *moduleMenuArr;

@property (nonatomic, strong) NSMutableArray *communicationMenuArr;

@property (nonatomic,strong) NSString *barTitle;

@property (nonatomic, strong) NSMutableArray *moreMenuArr;

@property (nonatomic, strong) NSMutableArray *inboxMenuArr;

@property (nonatomic, strong) NSMutableArray *approvalMenuArr;

@property (nonatomic,assign) BOOL timetableNeedsUpdate;

@property (nonatomic, strong) NSString *academicTransId;

@property (nonatomic, strong) NSString *staffTransId;

@property (nonatomic, strong) NSString *selectedSchoolTransId;

@property (nonatomic, strong) NSString *selectedSchoolName;

@property (nonatomic, strong) NSString *currentMenuId;

@property (nonatomic, assign) BOOL isPushNotification;

@property (nonatomic, strong) NSString *pushNotificationCommTransID;

@property (nonatomic, strong) StaffDetails *staff;

@property (nonatomic,assign) BOOL needRefreshHealthRecords;

typedef NS_ENUM(NSInteger,LAYER_SIDES){
    LAYER_TOP = 1,
    LAYER_BOTTOM,
    LAYER_LEFT,
    LAYER_RIGHT,
    LAYER_ALL
};

typedef NS_ENUM(NSInteger,MENU){
    MENU_COMMUNICATION = 1,
    MENU_PROFILE = 2,
    MENU_LIVE_TRACK = 3,
    MENU_SETTINGS = 4,
    MENU_MORE = 5,
    MENU_ALERTS = 6,
    MENU_INBOX = 7,
    MENU_APPROVALS = 8,
    MENU_ASSIGNMENTS = 9,
    MENU_EVENTS = 10,
    MENU_ATTENDANCE = 11,
    MENU_EXAMS = 12,
    MENU_NOTIFICATIONS = 13,
    MENU_HOLIDAY = 14,
    MENU_PORTIONS = 15,
    MENU_LIBRARY = 16,
    MENU_TIME_TABLE = 17,
    MENU_RESULTS = 18,
    MENU_FEES = 19,
    MENU_HOSTEL = 20,
    MENU_CHANGE_PASSWORD = 21,
    MENU_ABOUT_US = 22,
    MENU_USER_MANUAL = 23,
    MENU_FEEDBACK = 24,
    MENU_LOGOUT = 25,
    MENU_INBOX_NOTIFICATION = 26,
    MENU_INBOX_EVENTS = 27,
    MENU_ASSIGNMENT_APPROVALS = 28,
    MENU_EVENT_APPROVALS = 29,
    MENU_NOTIFICATION_APPROVALS = 30,
    MENU_SUPPORT = 31,
    MENU_INBOX_PORTION =33,
    MENU_PORTION_APPROVALS = 32,
    MENU_HEALTHDETAILS = 34,
    MENU_TEMPERATURE = 35,
    MENU_CHANGE_SCHOOL = 1000,
    MENU_NOTIFICATION_CC = 100,
    MENU_EVENT_CC = 101,
    MENU_ASSIGNMENT_CC = 102
};

typedef NS_ENUM(NSInteger,ATTENDANCE_STATUS)
{
    ATTENDANCE_PRESENT = 0,
    ATTENDANCE_ABSENT,
    ATTENDANCE_LATE,
    ATTENDANCE_NOTAPPLICABLE
};

typedef NS_ENUM(NSInteger,StatusType) {
    
    StatusTypePending = 0,
    StatusTypeApproved,
    StatusTypeDeclined
};

typedef NS_ENUM(NSInteger,LIST_TYPE)
{
    LIST_TYPE_ACCEPTED = 0,
    LIST_TYPE_PENDING,
    LIST_TYPE_DECLINED,
    LIST_TYPE_OVERALL,
    LIST_TYPE_CREATED_TODAY
};

typedef NS_ENUM(NSInteger,ASSIGN_TYPE)
{
    ASSIGN_TYPE_ENTIRESCHOOL = 0,
    ASSIGN_TYPE_ALLSTUDENT,
    ASSIGN_TYPE_STAFF,
    ASSIGN_TYPE_GROUP,
    ASSIGN_TYPE_CLASS,
    ASSIGN_TYPE_SECTION,
    ASSIGN_TYPE_STUDENT,
    ASSIGN_TYPE_UNKNOWN,
};

typedef NS_ENUM(NSInteger,APPROVAL_TYPE){
    
    APPROVAL_TYPE_APPROVE = 1,
    APPROVAL_TYPE_DECLINE
};

typedef NS_ENUM(NSInteger,TypeID) {
    
    TypeID_Subject = 15,
    TypeID_AssignmentType = 31
};


//Instance

+(instancetype) singletonSharedInstance;

+(void) showAlertWithMessage:(NSString *)message;

+(void) requestTimeOutErrorAlert;


// Class methods

// Adding easing animation to view
-(void) addEaseAnimationToView: (UIView *)view;

// Method to detect active internet connection.
-(BOOL) checkForActiveInternet;

// Method to show Alert view
-(void) showAlertMessageWithTitle:(NSString *)title andMessage:(NSString *)message;// classInstance:(id)instance;

// Method to draw Border layer for the view
//-(void) drawLayerForView:(UIView *)view inPosition:(NSString *)position color:(UIColor *)color;


- (NSString *)convertTimeToTweleveHousrFomateFromString:(NSString *)timeString;

// Method to change date with given format and returns as NSDate
- (NSDate *) dateFromStringWithFormate:(NSString *)formate withDate:(NSString *)date;

// Method to change date with given format and returns as NSString
- (NSString *) stringFromDateWithFormate:(NSString *)formate withDate:(NSDate *)date;

- (NSString *)dateLblTextWithFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate;

- (NSDate *)dateFromDate:(NSDate *)date withFormate:(NSString *)formate;

-(NSString *)changeDateFormatFromService:(NSString *)dateString withFormat:(NSString *)dateFormat;

// instance methods

// Method to get attributed string from Date string
-(NSMutableAttributedString *) getDateAttributedStringForDateString:(NSString *)dateString inLabel:(UILabel *)label;

// Method to validate proper email address
-(BOOL) isValidEmailAddress:(NSString *)email;

// Method to validate proper mobile no
-(BOOL) isValidMobileNo:(NSString *)mobile;

// Method to check whether string is null/empty or not
-(BOOL) isEmpty :(NSString *)string;

// Method to check null or not. If null, returns empty string
-(NSString *) nullValueCheck:(NSString *)string;

// Method to add Right View to TextField Right
-(void)addRightViewToTextField:(UITextField *)textField withImageName:(NSString *)imageName;

// Method to add Right Drop down arrow to the textfield Right
-(void) addDropDownArrowToTextField:(UITextField *)textField;

// Method to add Left inset view for the textfield
-(void) addLeftInsetToTextField:(UITextField *)textField;

// Core Data Common Methods

- (void)storeContextInMainThread;

// Method to Delete all objects in an entity
- (void)deleteAllObjectsInEntity:(NSString *)nameEntity;

// Method to Delete particular objects in an entity
-(void)deleteSelectedObjectsInEntityWithEntityName:(NSString *)entityName withPredicate:(NSPredicate *)predicate withContext:(NSManagedObjectContext *)context;

// Method to retrieve all objects in an entity
- (NSArray *)retrieveAllObjectsInEntity:(NSString *)entityName;

// Method to retrieve particular row from an entity
- (NSArray *)retrieveSelectedObjectsInEntityWithEntityName:(NSString *)entityName withPredicate:(NSPredicate *)predicate;

-(BOOL)reachabilityCheck;

- (void)addDoneButtonForTextView:(UITextView *)textView;

- (void)addDoneButtonForTextField:(UITextField *)textField;

+(void)layerDrawForView:(UIView *)view position:(LAYER_SIDES)position color:(UIColor *) color;

+(void)removeAllLayersForView:(UIView *)view;

+(NSString *)daysBetweenFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate;

//+(NSString *) dateCompareFromServiceDate:(NSDate *)serviceFromDate toDate:(NSDate *)serviceToDate;

+(NSString *) dateCompareFromDate:(NSDate *)serviceFromDate toDate:(NSDate *)serviceToDate;

- (NSString *)stringFromDate:(NSDate *)date withFormate:(NSString *)formate;

-(NSDate *)dateFromString:(NSString *)dateString withFormate:(NSString *)dateFormat;

+(NSDate *) dateFromString:(NSString *)dateStr dateFormat:(NSString *)dateFormat;

- (void)createMenu;

//Sevice Call

- (void)fetchBoardAndClassDetailsServiceCallWithSchoolTransId:(NSString *)schoolTransId showIndicator:(BOOL)showIndicator onCompletion:(onBoardFetchCompletion)completionHandler;

- (void)fetchStaffListBySchoolTransId:(NSString *)schoolTransId showIndicator:(BOOL)showIndicator onCompletion:(onStaffListFetchCompletion)completionHandler;

- (void)fetchReceiverDetailsWithSchoolTransId:(NSString *)schoolTransId showIndicator:(BOOL)showIndicator onCompletion:(onReceiverFetchCompletion)completionHandler;

- (void)fetchGroupListBySchoolTransId:(NSString *)schoolTransId showIndicator:(BOOL)showIndicator onCompletion:(onGroupListFetchCompletion)completionHandler;

- (void)fetchFilterAndReceipientDetailsWithMenuId:(NSString *)menuId showIndicator:(BOOL)showIndicator onCompletion:(onFilterAndReceipientFetchCompletion)completionHandler;

//PushCount

- (void)updatePushCountServiceCall;

- (void)fetchPushCountServiceCall;

@end