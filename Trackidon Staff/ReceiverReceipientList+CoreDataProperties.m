//
//  ReceiverReceipientList+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 02/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ReceiverReceipientList+CoreDataProperties.h"

@implementation ReceiverReceipientList (CoreDataProperties)

@dynamic menuId;
@dynamic receipientDescription;
@dynamic recepientTypeId;
@dynamic isSelected;
@dynamic staff;

@end
