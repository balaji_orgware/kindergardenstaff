//
//  TSCommonListViewController.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 20/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSCommonListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *studentNameVw;
@property (weak, nonatomic) IBOutlet UILabel *studentNameLbl;

@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@property (weak, nonatomic) IBOutlet UIView *updateButtonVw;
@property (weak, nonatomic) IBOutlet UIButton *updateButton;

@property (nonatomic,strong) StudentModel *studentObj;
@property (nonatomic,assign) NSInteger moduleType;
@property (nonatomic,strong) NSArray *contentArray;
@property (nonatomic,strong) NSDictionary *generalInfoDict;

@end
