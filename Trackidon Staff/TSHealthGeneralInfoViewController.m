//
//  TSHealthGeneralInfoViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 15/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSHealthGeneralInfoViewController.h"

#define TEXT_EDIT @"EDIT"
#define TEXT_UPDATE @"UPDATE"
#define Health_Service_DF @"dd-MMMM-yyyy"

@interface TSHealthGeneralInfoViewController (){
    
    BOOL isEditing, isRaisedTableView;
    
    NSArray *nameListArray,*imageListArray,*bloodGroupArr,*skinColorArr,*hairColorArr;
    
    TSSelectionViewController *selectionVC;
    
    CGRect viewRect, currentVisibleRect;
    
    UIView *currentResponder;
}
@end

@implementation TSHealthGeneralInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    viewRect = self.view.frame;
    
    isEditing = NO;
    
    isRaisedTableView = NO;
    
    if (_generalInfoModelObj == nil) {
        
        _generalInfoModelObj = [[GeneralInfoModel alloc]init];
    }
    
    nameListArray = @[@"Blood Group",@"Skin Color",@"Hair Color",@"Mole 1",@"Mole 2",@"Food Allergy"];
    imageListArray = @[@"blood",@"skin_medium_brown",@"hair_black",@"mole",@"mole",@"food_allergy"];
    bloodGroupArr = @[@"A +ve",@"A -ve",@"B +ve",@"B -ve",@"O +ve",@"O -ve",@"AB +ve",@"AB -ve"];
    hairColorArr = @[@"Black",@"Blonde",@"Red",@"White",@"Brown"];
    skinColorArr = @[@"Black",@"Brown",@"Ivory",@"Medium brown",@"Olive"];

    _studentNameLbl.text = _studentObj.studentName;
    
    [TSSingleton layerDrawForView:_studentNameVw position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationItem.title = @"General Information";
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Helper Methods

-(void) keyboardShown:(NSNotification *)notification{
 
    NSDictionary* info = [notification userInfo];
    
    CGSize currentKeyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    CGRect textFieldFrame;

    textFieldFrame = CGRectMake(currentResponder.frame.origin.x, [_tableView rowHeight]*([currentResponder.accessibilityHint integerValue]+1) ,currentResponder.frame.size.width, currentResponder.frame.size.height);
    
    CGPoint textFieldOrigin = CGPointMake(currentResponder.frame.origin.x, currentResponder.frame.origin.y+textFieldFrame.origin.y);
    textFieldOrigin.y = textFieldOrigin.y + currentResponder.frame.size.height;
    CGFloat textFieldHeight = currentResponder.frame.size.height;
    
    CGRect visibleRect = viewRect;
    visibleRect.size.height -= currentKeyboardSize.height + textFieldHeight;
    currentVisibleRect = visibleRect;
    
    if (!CGRectContainsPoint(visibleRect, textFieldOrigin))
    {
        isRaisedTableView = YES;
        
        CGPoint scrollPoint = CGPointMake(0.0, textFieldOrigin.y - visibleRect.size.height  + textFieldHeight);
        
        [_tableView setContentOffset:scrollPoint animated:YES];
    }
    else
        isRaisedTableView = NO;
}

-(void) keyboardHide:(NSNotification *)notification{
    
    if (isRaisedTableView) {
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[currentResponder.accessibilityHint integerValue] inSection:0];
        
        [_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

#pragma mark - Selection View Controller

-(void) showSelectionViewControllerWithTextField:(UITextField *)textField withSelectedValue:(NSString *)selected{
    
    if (!selectionVC) {
        
        selectionVC = [[TSSelectionViewController alloc]initWithNibName:@"TSSelectionViewController" bundle:nil];
        selectionVC.delegate = self;
        
        NSMutableDictionary *contentDict = [NSMutableDictionary new];
        
        if ([textField.accessibilityHint integerValue] == 0) {
            
            for (NSString *bloodGroup in bloodGroupArr) {
                [contentDict setValue:@"" forKey:bloodGroup];
            }
        }
        else if ([textField.accessibilityHint integerValue] == 1){
            
            for (NSString *skinColor in skinColorArr) {
                [contentDict setValue:@"" forKey:skinColor];
            }
        }
        else if ([textField.accessibilityHint integerValue] == 2){
         
            for (NSString *hairColor in hairColorArr) {
                [contentDict setValue:@"" forKey:hairColor];
            }
        }
        
        if (contentDict.allKeys.count >0) {
            
            [selectionVC setContentDict:contentDict];
            [selectionVC setSelectedValue:selected];
            [selectionVC showSelectionViewController:self forTextField:textField];
        }
        else{
            selectionVC = nil;
        }
    }
}

#pragma mark - Selection View Delegates

-(void)selectedValueFromDict:(NSDictionary *)contentDict withSelectedValue:(NSString *)selectedValue andTransID:(NSString *)transID forTextField:(UITextField *)textField{
    
    switch ([textField.accessibilityHint integerValue]) {
        case 0:
            _generalInfoModelObj.bloodGroup = selectedValue;
            break;
            
        case 1:
            _generalInfoModelObj.skinColor = selectedValue;
            break;
            
        case 2:
            _generalInfoModelObj.hairColor = selectedValue;
            break;
            
        default:
            break;
    }

    [_tableView reloadData];
    selectionVC = nil;
}

-(void)backViewTappedOn:(UIViewController *)viewController{
    
    selectionVC = nil;
}

#pragma mark - Button Actions

- (IBAction)editButtonTapped:(id)sender {

    if ([_editButton.titleLabel.text isEqualToString:TEXT_EDIT]) {
        
        [_editButton setTitle:TEXT_UPDATE forState:UIControlStateNormal];
        isEditing = YES;
    }
    else if ([_editButton.titleLabel.text isEqualToString:TEXT_UPDATE]){
        
        if (_generalInfoModelObj.identityMarkOne.length == 0) {
            [TSSingleton showAlertWithMessage:@"Please enter Identification Mark one"];
            return;
        }
        else if (_generalInfoModelObj.identityMarkTwo.length == 0){
            [TSSingleton showAlertWithMessage:@"Please enter Identification Mark two"];
            return;
        }
        else if (_generalInfoModelObj.foodAllergy.length == 0){
            [TSSingleton showAlertWithMessage:@"Please enter Food Allery info"];
            return;
        }
        else if (_generalInfoModelObj.identityMarkOne.length>150){
            
            [TSSingleton showAlertWithMessage:@"Identity Mark 1 Text length should be less than 150 characters"];
            return;
        }
        else if (_generalInfoModelObj.identityMarkTwo.length>150){
            
            [TSSingleton showAlertWithMessage:@"Identity Mark 2 Text length should be less than 150 characters"];
            return;
        }
        else if (_generalInfoModelObj.foodAllergy.length >500){
            
            [TSSingleton showAlertWithMessage:@"Food Allergy Text length should be less than 500 characters"];
            return;
        }
        
        [self updateGeneralInfoDetailsAPICall];
        
    }
    
    [_tableView reloadData];
}

#pragma mark - TableView Datasource & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return nameListArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
    
    UILabel *menuNameLbl = (UILabel *)[cell viewWithTag:101];
    
    UITextField *valueTxtFld = (UITextField *)[cell viewWithTag:102];
    
    menuNameLbl.text = [nameListArray objectAtIndex:indexPath.row];
    
    imageView.image = [UIImage imageNamed:[imageListArray objectAtIndex:indexPath.row]];
    
    valueTxtFld.delegate = self;
    valueTxtFld.accessibilityHint = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    
    switch (indexPath.row) {
        case 0:
            valueTxtFld.text = _generalInfoModelObj.bloodGroup;
            break;
            
        case 1:
        {
            valueTxtFld.text = _generalInfoModelObj.skinColor;
            
            if ([valueTxtFld.text isEqualToString:@"Black"]) {
                imageView.image = [UIImage imageNamed:@"skin_black"];
            }
            else if ([valueTxtFld.text isEqualToString:@"Brown"]){
                imageView.image = [UIImage imageNamed:@"skin_brown"];
            }
            else if ([valueTxtFld.text isEqualToString:@"Ivory"]){
                imageView.image = [UIImage imageNamed:@"skin_ivory"];
            }
            else if ([valueTxtFld.text isEqualToString:@"Medium brown"]){
                imageView.image = [UIImage imageNamed:@"skin_medium_brown"];
            }
            else{
                imageView.image = [UIImage imageNamed:@"skin_olive"];
            }
        }
        break;
            
        case 2:
        {
            valueTxtFld.text = _generalInfoModelObj.hairColor;
            
            hairColorArr = @[@"Black",@"Blonde",@"Red",@"White",@"Brown"];

            if ([valueTxtFld.text isEqualToString:@"Black"]) {
                imageView.image = [UIImage imageNamed:@"hair_black"];
            }
            else if ([valueTxtFld.text isEqualToString:@"Blonde"]){
                imageView.image = [UIImage imageNamed:@"hair_blonde"];
            }
            else if ([valueTxtFld.text isEqualToString:@"Red"]){
                imageView.image = [UIImage imageNamed:@"hair_red"];
            }
            else if ([valueTxtFld.text isEqualToString:@"White"]){
                imageView.image = [UIImage imageNamed:@"hair_white"];
            }
            else{
                imageView.image = [UIImage imageNamed:@"skin_brown"];
            }
        }
            break;
            
        case 3:
            valueTxtFld.text = _generalInfoModelObj.identityMarkOne;
            break;
            
        case 4:
            valueTxtFld.text = _generalInfoModelObj.identityMarkTwo;
            break;
            
        case 5:
            valueTxtFld.text = _generalInfoModelObj.foodAllergy;
            break;
            
        default:
            valueTxtFld.text = @"";
            break;
    }
    
    if (isEditing)
        valueTxtFld.userInteractionEnabled = YES;
    else
        valueTxtFld.userInteractionEnabled = NO;
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (isEditing) {
        
        if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) {
            
            UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
            UITextField *textField = (UITextField *)[selectedCell viewWithTag:102];
            textField.accessibilityHint = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self textFieldShouldBeginEditing:textField];
        }
    }
}

#pragma mark - Textfield Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if ([textField.accessibilityHint integerValue] == 3 || [textField.accessibilityHint integerValue] == 4 || [textField.accessibilityHint integerValue] == 5) {
        
        currentResponder = textField;
    
        return YES;
    }
    else{
        
        [self showSelectionViewControllerWithTextField:textField withSelectedValue:textField.text];
        
        return NO;
    }
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    textField.text = [textField.text capitalizedString];
    
    switch ([textField.accessibilityHint integerValue]) {
        case 3:
            _generalInfoModelObj.identityMarkOne = textField.text;
            break;
            
        case 4:
            _generalInfoModelObj.identityMarkTwo = textField.text;
            break;
            
        case 5:
            _generalInfoModelObj.foodAllergy = textField.text;
            break;
            
        default:
            break;
    }
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    [_tableView setContentOffset:CGPointZero animated:YES];
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Service Call

-(void) updateGeneralInfoDetailsAPICall{
    
    NSString *urlString = @"HealthDetails/Services/HealthCardService.svc/UpdateHealthCardInfo";
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    [dict setValue:_studentObj.studentTransID forKey:@"StudentTransID"];
    [dict setValue:_generalInfoModelObj.bloodGroup forKey:@"BloodGroup"];
    [dict setValue:_generalInfoModelObj.skinColor forKey:@"SkinColour"];
    [dict setValue:_generalInfoModelObj.hairColor forKey:@"HairColour"];
    [dict setValue:_generalInfoModelObj.identityMarkOne forKey:@"IdentityMark1"];
    [dict setValue:_generalInfoModelObj.identityMarkTwo forKey:@"IdentityMark2"];
    [dict setValue:SINGLETON_INSTANCE.staffTransId forKey:@"ExaminerID"];
    [dict setValue:[SINGLETON_INSTANCE stringFromDate:[NSDate date] withFormate:Health_Service_DF] forKey:@"ExaminedDate"];
    [dict setValue:SINGLETON_INSTANCE.staff.mobileNo forKey:@"Mobileno"];
    [dict setValue:[_generalInfoModelObj.hcTransID isEqualToString:EMPTY_TRANSID] ? EMPTY_TRANSID : _generalInfoModelObj.hcTransID forKey:@"HCTransID"];
    [dict setValue:SINGLETON_INSTANCE.staffTransId forKey:@"UserTransID"];
    [dict setValue:@"" forKey:@"Remarks"];
    [dict setValue:@"" forKey:@"AddlnRemarks"];
    
    NSDictionary *genDict = @{@"GeneralInfo":dict};
    
    NSDictionary *param = @{@"ObjUpdateHealthCard":genDict};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"UpdateHealthCardInfoResult"][@"ResponseCode"] integerValue] == 1) {
             
                [TSSingleton showAlertWithMessage:responseDictionary[@"UpdateHealthCardInfoResult"][@"ResponseMessage"]];
                
                [_editButton setTitle:TEXT_EDIT forState:UIControlStateNormal];
                isEditing = NO;
                
                SINGLETON_INSTANCE.needRefreshHealthRecords = YES;
                
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }];
}

@end