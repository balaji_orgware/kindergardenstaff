//
//  Menu+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 16/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Menu.h"

NS_ASSUME_NONNULL_BEGIN

@interface Menu (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *isHasSubMenu;
@property (nullable, nonatomic, retain) NSNumber *isModule;
@property (nullable, nonatomic, retain) NSNumber *isShow;
@property (nullable, nonatomic, retain) NSString *menuId;
@property (nullable, nonatomic, retain) NSString *menuName;
@property (nullable, nonatomic, retain) NSString *menuPriority;
@property (nullable, nonatomic, retain) NSString *menuTransId;
@property (nullable, nonatomic, retain) NSString *moduleId;
@property (nullable, nonatomic, retain) NSString *moduleName;
@property (nullable, nonatomic, retain) NSString *modulePriority;
@property (nullable, nonatomic, retain) NSString *moduleTransId;
@property (nullable, nonatomic, retain) NSString *schoolTransId;
@property (nullable, nonatomic, retain) School *school;

@end

NS_ASSUME_NONNULL_END
