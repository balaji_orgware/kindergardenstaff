//
//  TSNotificationApprovalsViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 06/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSNotificationApprovalsViewController.h"

@interface TSNotificationApprovalsViewController (){
    
    NSMutableArray *notificationArray,*notificationModelArray;
    
    UIBarButtonItem *selectAllBtn;
    
    UILongPressGestureRecognizer *longPressGesture;
    
    TSSelectionViewController *selectionVC;
    
    NSArray *attachmentImgArray;

}
@end

@implementation TSNotificationApprovalsViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    attachmentImgArray = @[@"one",@"two",@"three",@"four",@"five"];
    
    notificationArray = [NSMutableArray new];
    
    notificationModelArray = [NSMutableArray new];
    
    selectAllBtn = [[UIBarButtonItem alloc]initWithTitle:@"Select All" style:UIBarButtonItemStyleDone target:self action:@selector(selectAllAction)];
    
    longPressGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressGestureAction:)];
    
    longPressGesture.delegate = self;
    
    [self.notificationTableView addGestureRecognizer:longPressGesture];
    
    [self.notificationTableView setTableFooterView:[UIView new]];
    
    self.selectionView.frame = CGRectMake(self.selectionView.frame.origin.x, DeviceHeight+64, self.selectionView.frame.size.width, self.selectionView.frame.size.height);
    
    [self categorizeNotifications];
    
    [TSAnalytics logEvent:ANALYTICS_NOTIFAPPROVALS_OPENED_APPROVALS parameter:[NSMutableDictionary new]];
}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_APPROVALNOTIFICATION_LIST parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@ Log",SINGLETON_INSTANCE.barTitle];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

#pragma mark - Button Actions

- (IBAction)approvedButtonTapped:(id)sender {
    
    NSPredicate *approvePredicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *approvedArray = [notificationModelArray filteredArrayUsingPredicate:approvePredicate];
    
    if (approvedArray.count>0) {
        
        UIAlertView *approveAlertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:[NSString stringWithFormat:@"Do you want to Approve these %ld Notifications?",(unsigned long)approvedArray.count] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        
        approveAlertView.tag = APPROVAL_TYPE_APPROVE;
        
        approveAlertView.delegate = self;
        
        [approveAlertView show];
    }
}

- (IBAction)declineButtonTapped:(id)sender {
    
    NSPredicate *declinePredicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *declinedArray = [notificationModelArray filteredArrayUsingPredicate:declinePredicate];
    
    if (declinedArray.count>0) {
        
        UIAlertView *declineAlertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:[NSString stringWithFormat:@"Do you want to Decline these %ld Notifications?",(unsigned long)declinedArray.count] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        
        declineAlertView.tag = APPROVAL_TYPE_DECLINE;
        
        declineAlertView.delegate = self;
        
        [declineAlertView show];
    }
}

#pragma mark - Alert View Delegates

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == APPROVAL_TYPE_APPROVE) {
        
        if (buttonIndex) {
            
            NSPredicate *approvePredicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *approvedArray = [notificationModelArray filteredArrayUsingPredicate:approvePredicate];
            
            NSMutableArray *notificationsTransIDArr = [NSMutableArray new];
            
            for (ApprovalsModel *modelObj in approvedArray) {
                
                [notificationsTransIDArr addObject:modelObj.transID];
            }
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidUserTransID"];
            
            [param setValue:[NSString stringWithFormat:@"%ld",(long)APPROVAL_TYPE_APPROVE] forKey:@"intApprovedStatus"];
            
            [param setValue:notificationsTransIDArr forKey:@"guidNotificationTransID"];
            
            NSDictionary *updateParam = @{@"ApproveSchoolNotification":param};
            
            [self updateNotificationsWithParam:updateParam withApprovalType:APPROVAL_TYPE_APPROVE];
        }
    }
    else if (alertView.tag == APPROVAL_TYPE_DECLINE){
        
        if (buttonIndex) {
            
            NSPredicate *declinePredicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *declinedArray = [notificationModelArray filteredArrayUsingPredicate:declinePredicate];
            
            NSMutableArray *notificationsTransIDArr = [NSMutableArray new];
            
            for (ApprovalsModel *modelObj in declinedArray) {
                
                [notificationsTransIDArr addObject:modelObj.transID];
            }
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidUserTransID"];
            
            [param setValue:[NSString stringWithFormat:@"%ld",(long)APPROVAL_TYPE_DECLINE] forKey:@"intApprovedStatus"];
            
            [param setValue:notificationsTransIDArr forKey:@"guidNotificationTransID"];
            
            NSDictionary *updateParam = @{@"ApproveSchoolNotification":param};
            
            [self updateNotificationsWithParam:updateParam withApprovalType:APPROVAL_TYPE_DECLINE];
        }
    }
    
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}


#pragma mark - Helper methods

// Select All Button Action
-(void) selectAllAction{
    
    for (ApprovalsModel *modelObj in notificationModelArray) {
        
        modelObj.isSelected = YES;
    }
    
    self.navigationItem.title = [NSString stringWithFormat:@"%ld   Selected",(unsigned long)notificationModelArray.count];
    
    [self.notificationTableView reloadData];
}

// Method to Show/hide Selection View
-(void) animateSelectionView{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *filteredArray = [notificationModelArray filteredArrayUsingPredicate:predicate];
    
    if (filteredArray.count>0) {
        
        NSLog(@"%@",self.selectionView);
        
        if(self.selectionView.frame.origin.y >= DeviceHeight){
            
            [UIView animateWithDuration:0.2f animations:^{
                
                self.notificationTableView.frame = CGRectMake(self.notificationTableView.frame.origin.x, self.notificationTableView.frame.origin.y, self.notificationTableView.frame.size.width, DeviceHeight - 64 - self.selectionView.frame.size.height);
                
                self.selectionView.frame = CGRectMake(self.selectionView.frame.origin.x, self.notificationTableView.frame.origin.y+self.notificationTableView.frame.size.height, self.selectionView.frame.size.width, self.selectionView.frame.size.height);
            }];
        }
        
        self.navigationItem.rightBarButtonItem = selectAllBtn;
        
        self.navigationItem.title = [NSString stringWithFormat:@"%ld   Selected",(unsigned long)filteredArray.count];
    }
    else{
        
        self.navigationItem.rightBarButtonItem = nil;
        
        self.navigationItem.title = @"Notification Approval";

        if (self.selectionView.frame.origin.y < DeviceHeight) {
            
            [UIView animateWithDuration:0.2f animations:^{
                
                self.notificationTableView.frame = CGRectMake(self.notificationTableView.frame.origin.x, self.notificationTableView.frame.origin.y, self.notificationTableView.frame.size.width, DeviceHeight-64);
                
                self.selectionView.frame = CGRectMake(self.selectionView.frame.origin.x, DeviceHeight, self.selectionView.frame.size.width, self.selectionView.frame.size.height);
            }];
        }
    }
}

// Long press Gesture Action
-(void)longPressGestureAction:(UILongPressGestureRecognizer *)sender{
    
    CGPoint touchPoint = [sender locationOfTouch:0 inView:self.notificationTableView];
    
    NSIndexPath *indexPath = [_notificationTableView indexPathForRowAtPoint:touchPoint];
    
    if (indexPath != nil && sender.state == UIGestureRecognizerStateBegan) {
        
        ApprovalsModel *modelObj = [notificationModelArray objectAtIndex:indexPath.row];
        
        modelObj.isSelected = !modelObj.isSelected;
        
        [self.notificationTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        [self animateSelectionView];
    }
    
}

// Post Notification Method to Update Tableview from TSEventApprovalDescViewController
-(void) updateTableView{
    
    [self categorizeNotifications];
    
//    [self fetchUnapprovedNotificationsAPICallWithIndicator:NO];
}

// Method to Clear Approve/Decline Selection View
-(void) clearSelectionView{
    
    [UIView animateWithDuration:0.2f animations:^{
        
        self.navigationItem.title = @"Notification Approval";
        
        self.navigationItem.rightBarButtonItem = nil;
        
        self.notificationTableView.frame = CGRectMake(self.notificationTableView.frame.origin.x, self.notificationTableView.frame.origin.y, self.notificationTableView.frame.size.width, DeviceHeight-64);
        
        self.selectionView.frame = CGRectMake(self.selectionView.frame.origin.x, DeviceHeight, self.selectionView.frame.size.width, self.selectionView.frame.size.height);
        
        self.navigationItem.rightBarButtonItem = nil;
    }];
}
/*
#pragma mark - Selection View

-(void) showSelectionViewWithSelectedText:(NSString *)selected andTextField:(UITextField *)textField{
    
    if (selectionVC == nil) {
        
        selectionVC = [[TSSelectionViewController alloc]initWithNibName:@"TSSelectionViewController" bundle:nil];
        
        selectionVC.delegate = self;
        
        NSMutableDictionary *contentDict = [NSMutableDictionary new];
        
        for (Board *board in boardArray) {
            
            [contentDict setValue:board.boardTransId forKey:board.boardName];
        }
        
        [selectionVC setContentDict:contentDict];
        
        [selectionVC setSelectedValue:selected.length>0?selected:@""];
        
        [selectionVC showSelectionViewController:self forTextField:textField];
    }
    else{
        
        selectionVC = nil;
    }
}

// MARK: Selection Callback Delegate

-(void)selectedValueFromDict:(NSDictionary *)contentDict withSelectedValue:(NSString *)selectedValue andTransID:(NSString *)transID forTextField:(UITextField *)textField{
    
    selectedBoardTransID = transID;
    
    [self clearSelectionView];
    
    [self categorizeNotifications];
    
    [self fetchUnapprovedNotificationsAPICallWithIndicator:YES];
    
    selectionVC = nil;
}

-(void)backViewTappedOn:(UIViewController *)viewController{
    
    selectionVC = nil;
}
 */

#pragma mark - Tableview Datasources & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return notificationArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sCellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier];
    
    UILabel *notifTitleLabel = (UILabel *)[cell viewWithTag:100];
    
    UILabel *notifStatusLabel = (UILabel *)[cell viewWithTag:101];
    
    UILabel *notifDateLabel = (UILabel *)[cell viewWithTag:102];
    
    UIImageView *attachmentImgVw = (UIImageView *)[cell viewWithTag:103];
    
    UILabel *notifBoardLabel = (UILabel *)[cell viewWithTag:104];

    Notifications *notificationObj = [notificationArray objectAtIndex:indexPath.row];
    
    notifTitleLabel.text = [notificationObj.notificationTitle capitalizedString];
    
    notifDateLabel.text = [SINGLETON_INSTANCE stringFromDate:notificationObj.notificationDate withFormate:DATE_FORMATE_MNTH_DAY_YR];
    
    notifStatusLabel.text = [NSString stringWithFormat:@"Created by %@",notificationObj.userCreatedName];
    
    notifBoardLabel.text = notificationObj.boardName;
    
    ApprovalsModel *modelObj = [notificationModelArray objectAtIndex:indexPath.row];
    
    if (!modelObj.isSelected) {
        
        [cell setBackgroundColor:[UIColor whiteColor]];
    }
    else{
        
        [cell setBackgroundColor:[UIColor lightGrayColor]];
    }
    if ([notificationObj.notificationIsHasAttachment intValue]==1) {
        
        attachmentImgVw.image = [UIImage imageNamed:[attachmentImgArray objectAtIndex:indexPath.row%5]];
        
        attachmentImgVw.hidden = NO;
    }
    else{
        
        attachmentImgVw.hidden = YES;
    }

    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [TSAnalytics logEvent:ANALYTICS_NOTIFAPPROVALS_CLICKED_APPROVALSINLIST parameter:[NSMutableDictionary new]];

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateTableView) name:@"updateTableView" object:nil];
    
    TSNotificationApprovalDescViewController *notificationApprovalDescVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBNotificationApprovalDesc"];
    
    [notificationApprovalDescVC setNotification:[notificationArray objectAtIndex:indexPath.row]];
    
    [self.navigationController pushViewController:notificationApprovalDescVC animated:YES];
}
#pragma mark - Categorize Events

-(void) categorizeNotifications{
    
    [notificationArray removeAllObjects];
    
    [notificationModelArray removeAllObjects];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId MATCHES %@ AND isForApprovals == 1",SINGLETON_INSTANCE.staffTransId];
    
    notificationArray = [NSMutableArray arrayWithArray:[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Notifications" withPredicate:predicate]];
    
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:NO];
    
    notificationArray = [[notificationArray sortedArrayUsingDescriptors:@[dateDescriptor]] mutableCopy];
    
    for (Notifications *notificationObj in notificationArray) {
        
        ApprovalsModel *modelObj = [ApprovalsModel new];
        modelObj.title = notificationObj.notificationTitle;
        modelObj.transID = notificationObj.notificationTransId;
        modelObj.isSelected = NO;
        modelObj.approvalStatus = [notificationObj.approvedStatus integerValue];
        
        [notificationModelArray addObject:modelObj];
    }
    
    if(notificationArray.count>0){
        
        [self.notificationTableView setHidden:NO];
    }
    else{
        
        [self.notificationTableView setHidden:YES];
    }
    
    [self.notificationTableView reloadData];
}
/*
#pragma mark - Service Calls

// Fetch Unapproved Events Service Call
-(void) fetchUnapprovedNotificationsAPICallWithIndicator:(BOOL)animated{
    
    NSString *urlString = @"MastersMServices/SchoolNotificationsService.svc/FetchUnApprovedSchoolNotification";
    
    NSDictionary *params = @{@"guidBoardTransID":selectedBoardTransID};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:animated onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchUnApprovedSchoolNotificationResult"][@"ResponseCode"] intValue] == 1) {
                
                [[Notifications instance]saveNotificationsWithResponseDict:responseDictionary withBoardTransID:selectedBoardTransID andIsForApprovals:YES];
                
                [self categorizeNotifications];
            }
        }
    }];
}
*/

// Update Events Service Call
-(void) updateNotificationsWithParam:(NSDictionary *)param withApprovalType:(NSInteger)approvalType{
    
    NSString *urlString = @"MastersMServices/SchoolNotificationsService.svc/MobileApproveSchoolNotification";
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"MobileApproveSchoolNotificationResult"][@"ResponseCode"] integerValue] == 1) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                if (approvalType == APPROVAL_TYPE_APPROVE) {
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                else{
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                
                [TSAnalytics logEvent:ANALYTICS_NOTIFAPPROVALS_CLICKED_BULKAPPROVALS parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"MobileApproveSchoolNotificationResult"][@"ResponseMessage"]];
                
                NSArray *notifTransIDArr = param[@"ApproveSchoolNotification"][@"guidNotificationTransID"];
                
                for (NSString *transID in notifTransIDArr) {
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId MATCHES %@ AND isForApprovals == 1 AND notificationTransId MATCHES %@",SINGLETON_INSTANCE.staffTransId,transID];
                    
                    [SINGLETON_INSTANCE deleteSelectedObjectsInEntityWithEntityName:@"Notifications" withPredicate:predicate withContext:[APPDELEGATE_INSTANCE managedObjectContext]];
                }
                
                [notificationModelArray removeAllObjects];
                
                [notificationArray removeAllObjects];
                
                [self clearSelectionView];
                
                [self categorizeNotifications];
            }
            else{
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                if (approvalType == APPROVAL_TYPE_APPROVE) {
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                else{
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                
                [TSAnalytics logEvent:ANALYTICS_NOTIFAPPROVALS_CLICKED_BULKAPPROVALS parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
            }
        }
        else{
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
            
            if (approvalType == APPROVAL_TYPE_APPROVE) {
                
                [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
            }
            else{
                
                [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
            }
            
            [TSAnalytics logEvent:ANALYTICS_NOTIFAPPROVALS_CLICKED_BULKAPPROVALS parameter:analyticsDict];
        }
    }];
}

@end