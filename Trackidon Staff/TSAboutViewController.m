//
//  TSAboutViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/23/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSAboutViewController.h"

@interface TSAboutViewController ()

@end

@implementation TSAboutViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [TSAnalytics logEvent:ANALYTICS_SUPPORT_OPENED parameter:[NSMutableDictionary new]];
    
    UITapGestureRecognizer *supportEmailLblTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(supportEmailTapped)];
    
    [_supportEmailLbl addGestureRecognizer:supportEmailLblTap];
    
    _supportEmailLbl.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *websiteLblTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(websiteTapped)];
    
    [_webUrlLbl addGestureRecognizer:websiteLblTap];
    
    _webUrlLbl.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *contact1LblTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contact1Tapped)];
    
    [_contactNo1Lbl addGestureRecognizer:contact1LblTap];
    
    _contactNo1Lbl.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *contact2LblTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contact2Tapped)];
    
    [_contactNo2Lbl addGestureRecognizer:contact2LblTap];
    
    _contactNo2Lbl.userInteractionEnabled = YES;
    
    for (UIView *view in self.navigationController.navigationBar.subviews)
    {
        for (UIView *view2 in view.subviews)
        {
            if ([view2 isKindOfClass:[UIImageView class]])
            {
                [view2 removeFromSuperview];
                break;
            }
        }
        break;
    }
    
    if (IS_IPHONE_4) {
        
        [self.aboutScrollView setContentSize:CGSizeMake(DEVICE_SIZE.width, DEVICE_SIZE.height +200)];
        
    } else if (IS_IPHONE_5)
    {
        [self.aboutScrollView setContentSize:CGSizeMake(DEVICE_SIZE.width, DEVICE_SIZE.height + 50)];
        
    } else if(IS_IPAD){
        
    }
    
    self.navigationView.backgroundColor = APPTHEME_COLOR;
    
    
    /*if (IS_IPHONE_4) {
     [self.aboutScrollView setContentSize:CGSizeMake(DEVICE_SIZE.width, DEVICE_SIZE.height +230)];
     
     }
     else if (IS_IPHONE_5){
     [self.aboutScrollView setContentSize:CGSizeMake(DEVICE_SIZE.width, DEVICE_SIZE.height +130)];
     
     }
     else if (IS_IPHONE_6){
     [self.aboutScrollView setContentSize:CGSizeMake(DEVICE_SIZE.width, DEVICE_SIZE.height +50)];
     }
     else {
     [self.aboutScrollView setContentSize:CGSizeMake(DEVICE_SIZE.width, DEVICE_SIZE.height)];
     }*/
    
    self.navigationItem.title = @"Support";
    
    self.navigationController.navigationBar.topItem.title = @"";
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_SUPPORT parameter:[NSMutableDictionary new]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void) contact1Tapped{
    
    [TSAnalytics logEvent:ANALYTICS_SUPPORT_CLICKEDPHONE parameter:[NSMutableDictionary new]];
    
    [self callContactWithNo:[_contactNo1Lbl.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
    
}

-(void) contact2Tapped{
    
    [TSAnalytics logEvent:ANALYTICS_SUPPORT_CLICKEDPHONE parameter:[NSMutableDictionary new]];
    
    [self callContactWithNo:[_contactNo2Lbl.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
}

-(void) websiteTapped{
    
    [TSAnalytics logEvent:ANALYTICS_SUPPORT_CLICKEDWEB parameter:[NSMutableDictionary new]];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.trackidon.com"]];
    
}

-(void) supportEmailTapped{
    
    [TSAnalytics logEvent:ANALYTICS_SUPPORT_CLICKEDMAIL parameter:[NSMutableDictionary new]];
    
    MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc]init];
    
    if ([MFMailComposeViewController canSendMail]) {
        
        mailComposer.mailComposeDelegate = self;
        
        //[mailComposer.navigationBar setBarTintColor:APPTHEME_COLOR];
        
        //[mailComposer.navigationBar setTintColor:[UIColor blackColor]];
        
        [mailComposer.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor blackColor]}];
        
        [mailComposer setToRecipients:[NSArray arrayWithObjects:@"support@trackidon.com", nil]];
        
        [mailComposer setSubject:[NSString stringWithFormat:@"Report from %@",[USERDEFAULTS objectForKey:USERNAME]]];
        
        [self presentViewController:mailComposer animated:YES completion:nil];
        
    }
    
}


-(void) callContactWithNo:(NSString *)number{
    
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",number]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
        
    } else{
        
        [TSSingleton showAlertWithMessage:@"Call facility is not available!!!"];
        
    }
    
}

#pragma mark - Mail Composer Delegate

-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    if (result == MFMailComposeResultSent){
        
        NSLog(@"\n\n Email Sent");
        
    }
    
    if([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)])
        [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
