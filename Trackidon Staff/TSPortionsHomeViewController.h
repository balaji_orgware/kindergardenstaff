//
//  TSPortionsHomeViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 22/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSPortionsHomeViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDataSource,UITableViewDelegate>

// Declarations

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *createPortionVw;


//Button Actions

- (IBAction)createPortionButtonTapped:(id)sender;

@end
