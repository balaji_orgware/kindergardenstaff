//
//  TSEventInboxViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/2/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSEventInboxViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *eventsTableView;

@property (nonatomic, assign) int listType;
@property (nonatomic,retain) NSFetchedResultsController *fetchedResultsController;

@end
