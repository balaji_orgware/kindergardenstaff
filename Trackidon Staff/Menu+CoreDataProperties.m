//
//  Menu+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 16/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Menu+CoreDataProperties.h"

@implementation Menu (CoreDataProperties)

@dynamic isHasSubMenu;
@dynamic isModule;
@dynamic isShow;
@dynamic menuId;
@dynamic menuName;
@dynamic menuPriority;
@dynamic menuTransId;
@dynamic moduleId;
@dynamic moduleName;
@dynamic modulePriority;
@dynamic moduleTransId;
@dynamic schoolTransId;
@dynamic school;

@end
