//
//  TSMyTimetableViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 23/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,SelectedDay) {
    
    SelectedDayMonday = 1,
    SelectedDayTuesday,
    SelectedDayWednesday,
    SelectedDayThursday,
    SelectedDayFriday,
    SelectedDaySaturday
};

@interface TSMyTimetableViewController : UIViewController<UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *timeTableTableView;

@property (weak, nonatomic) IBOutlet UIView *daysView;

- (IBAction)dayButtonTapped:(id)sender;

@end
