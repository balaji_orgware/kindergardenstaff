//
//  TSCreateGeneralCheckupDentalViewController.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 11/08/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSCreateGeneralCheckupDentalViewController : UIViewController <UITextFieldDelegate,UITextViewDelegate,UIAlertViewDelegate,datePickerDelegate>

// Examine Date View
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *examineDateView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

// Examined By View
@property (weak, nonatomic) IBOutlet UIView *examinedByView;
@property (weak, nonatomic) IBOutlet UITextField *examinedByTxtFld;

// General Checkup View
@property (weak, nonatomic) IBOutlet UIView *generalCheckupView;
@property (weak, nonatomic) IBOutlet UIView *neckInfoView;
@property (weak, nonatomic) IBOutlet UITextField *neckInfoTxtFld;
@property (weak, nonatomic) IBOutlet UIView *handsInfoView;
@property (weak, nonatomic) IBOutlet UITextField *handsInfoTxtFld;
@property (weak, nonatomic) IBOutlet UIView *legInfoView;
@property (weak, nonatomic) IBOutlet UITextField *legInfoTxtFld;
@property (weak, nonatomic) IBOutlet UIView *abdomenInfoView;
@property (weak, nonatomic) IBOutlet UITextField *abdomenInfoTxtFld;
@property (weak, nonatomic) IBOutlet UIView *respiratoryInfoView;
@property (weak, nonatomic) IBOutlet UITextField *respiratoryInfoTxtFld;
@property (weak, nonatomic) IBOutlet UIView *anemiaInfoView;
@property (weak, nonatomic) IBOutlet UITextField *anemiaInfoTxtFld;
@property (weak, nonatomic) IBOutlet UIView *allergyInfoView;
@property (weak, nonatomic) IBOutlet UITextField *allergyInfoTxtFld;
@property (weak, nonatomic) IBOutlet UIView *nervesystemInfoView;
@property (weak, nonatomic) IBOutlet UITextField *nerveSystemInfoTxtFld;
@property (weak, nonatomic) IBOutlet UIView *cardioVascularInfoView;
@property (weak, nonatomic) IBOutlet UITextField *cardioVascularInfoTxtFld;


// Dental View
@property (weak, nonatomic) IBOutlet UIView *dentalView;
@property (weak, nonatomic) IBOutlet UIView *intralOralInfoView;
@property (weak, nonatomic) IBOutlet UITextField *intraOralTxtFld;
@property (weak, nonatomic) IBOutlet UIView *extraOralInfoView;
@property (weak, nonatomic) IBOutlet UITextField *extraOralTxtFld;
@property (weak, nonatomic) IBOutlet UIView *tickView;
@property (weak, nonatomic) IBOutlet UIView *toothCavityView;
@property (weak, nonatomic) IBOutlet UIImageView *toothCavityTickImgVw;
@property (weak, nonatomic) IBOutlet UIView *plaqueView;
@property (weak, nonatomic) IBOutlet UIImageView *plaqueTickImgView;
@property (weak, nonatomic) IBOutlet UIView *gumBleedingView;
@property (weak, nonatomic) IBOutlet UIImageView *gumBleedingTickImgVw;
@property (weak, nonatomic) IBOutlet UIView *gumInflammationView;
@property (weak, nonatomic) IBOutlet UIImageView *gumInflammationTickImgVw;
@property (weak, nonatomic) IBOutlet UIView *stainsInfoView;
@property (weak, nonatomic) IBOutlet UIImageView *stainsTickImgVw;
@property (weak, nonatomic) IBOutlet UIView *tarterInfoView;
@property (weak, nonatomic) IBOutlet UIImageView *tarterTickImgVw;
@property (weak, nonatomic) IBOutlet UIView *badbreathInfoView;
@property (weak, nonatomic) IBOutlet UIImageView *badbreathTickImgVw;
@property (weak, nonatomic) IBOutlet UIView *softTissueInfoView;
@property (weak, nonatomic) IBOutlet UIImageView *softTissueTickImgVw;

@property (weak, nonatomic) IBOutlet UITextView *remarksTxtVw;

// Done view
@property (weak, nonatomic) IBOutlet UIView *doneView;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

- (IBAction)doneButtonTapped:(id)sender;

@property (nonatomic,strong) NSDictionary *generalInfoDict;
@property (nonatomic,assign) NSInteger healthType;

@end


