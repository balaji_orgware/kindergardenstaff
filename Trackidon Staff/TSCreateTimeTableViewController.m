//
//  TSCreateTimeTableViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 24/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSCreateTimeTableViewController.h"

@interface TSCreateTimeTableViewController (){
    
    BOOL isLeisureHour;
    
    Board *boardObj;
    
    NSArray *boardArray, *subjectsArray;
    
    NSString *selectedBoardTransID,*selectedClassTransID,*selectedSectionTransID, *selectedSubjectTransID;
    
    NSString *selectedBoard, *selectedClass, *selectedSection, *selectedSubject;
    
    TSSelectionViewController *selectionView;
}

@end

@implementation TSCreateTimeTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    isLeisureHour = NO;
    
    boardArray = [NSArray new];
    
    _dayNameLabel.text = _selectedDay;
    
    if ([_selectedDay isEqualToString:@"MON"]) {
        
        _dayNameLabel.text = @"Monday";
    }
    else if ([_selectedDay isEqualToString:@"TUE"]){
        
        _dayNameLabel.text = @"Tuesday";
    }
    else if([_selectedDay isEqualToString:@"WED"]){
        
        _dayNameLabel.text = @"Wednesday";
    }
    else if ([_selectedDay isEqualToString:@"THU"]){
        
        _dayNameLabel.text = @"Thursday";
    }
    else if ([_selectedDay isEqualToString:@"FRI"]){
        
        _dayNameLabel.text = @"Friday";
    }
    else{
        
        _dayNameLabel.text = @"Saturday";
    }
    
    
    switch ([_selectedPeriod intValue]) {
            
        case 1:
            
            _periodNameLabel.text = @"FIRST PERIOD";
            
            break;
            
        case 2:
            
            _periodNameLabel.text = @"SECOND PERIOD";
            
            break;
            
        case 3:
            
            _periodNameLabel.text = @"THIRD PERIOD";
            
            break;
            
        case 4:
            
            _periodNameLabel.text = @"FOURTH PERIOD";
            
            break;
            
        case 5:
            
            _periodNameLabel.text = @"FIFTH PERIOD";
            
            break;
            
        case 6:
            
            _periodNameLabel.text = @"SIXTH PERIOD";
            
            break;
            
        case 7:
            
            _periodNameLabel.text = @"SEVENTH PERIOD";
            
            break;
            
        case 8:
            
            _periodNameLabel.text = @"EIGHTH PERIOD";
            
            break;
            
        case 9:
            
            _periodNameLabel.text = @"SPECIAL CLASS";
            
            break;
            
        default:
            break;
    }
    
    if (IS_IPHONE_4) {
        
        _scrollView.contentSize = CGSizeMake(DeviceWidth, _selectSubjectTxtFld.frame.origin.y+_selectSubjectTxtFld.frame.size.height+10);
    }
    
    _tickImgVw.center = CGPointMake(DeviceWidth -50,_leisureButton.center.y);
    
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_selectClassTxtFld];
    
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_selectSectionTxtFld];
    
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_selectSubjectTxtFld];
    
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_selectBoardTxtFld];
    
    [TSSingleton layerDrawForView:_headerView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    [_tickImgVw setImage:[UIImage imageNamed:@"unapproved"]];
    
    NSPredicate *subjPred = [NSPredicate predicateWithFormat:@"typeID == %d",TypeID_Subject];
    
    subjectsArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"CommonTypeDetails" withPredicate:subjPred];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"schoolTransId MATCHES %@",SINGLETON_INSTANCE.selectedSchoolTransId];
    
    boardArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Board" withPredicate:pred];
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveButtonAction)];
    
    self.navigationItem.rightBarButtonItem = saveButton;
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonAction)];
    
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    self.navigationItem.title = @"Assign class and subject";
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_TIMETABLECREATE parameter:[NSMutableDictionary new]];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

#pragma mark - Button actions

-(void) saveButtonAction{
    
    SINGLETON_INSTANCE.timetableNeedsUpdate = YES;

    if (!isLeisureHour) {
        
        if (_selectBoardTxtFld.text.length == 0){
            
            [TSSingleton showAlertWithMessage:@"Choose your Board"];
            return;
        }
        else if (_selectClassTxtFld.text.length == 0){
            
            [TSSingleton showAlertWithMessage:@"Choose your Class"];
            return;
        }
        else if (_selectSectionTxtFld.text.length == 0){
            
            [TSSingleton showAlertWithMessage:@"Choose your Section"];
            return;
        }
        else if (_selectSubjectTxtFld.text.length == 0){
            
            [TSSingleton showAlertWithMessage:@"Choose your Subject"];
            return;
        }
        
        [self changePeriod:_selectedPeriod ofDay:_selectedDay];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else{
        
        [self changePeriod:_selectedPeriod ofDay:_selectedDay];

        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void) cancelButtonAction{
    
    SINGLETON_INSTANCE.timetableNeedsUpdate = NO;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)leisureButtonTapped:(id)sender {
    
    if ([_tickImgVw.image isEqual:[UIImage imageNamed:@"approved"]]) {
        
        _tickImgVw.image = [UIImage imageNamed:@"unapproved"];
        
        isLeisureHour = NO;
    }
    else{
        
        _tickImgVw.image = [UIImage imageNamed:@"approved"];
        
        _selectBoardTxtFld.text = @"";
        _selectClassTxtFld.text = @"";
        _selectSectionTxtFld.text = @"";
        _selectSubjectTxtFld.text = @"";
        
        selectedBoardTransID = @"00000000-0000-0000-0000-000000000000";
        
        selectedSubjectTransID = @"00000000-0000-0000-0000-000000000000";
        
        selectedSectionTransID = @"00000000-0000-0000-0000-000000000000";
        
        selectedClassTransID = @"00000000-0000-0000-0000-000000000000";
        
        selectedBoard = @"Empty";
        
        selectedClass = @"Empty";
        
        selectedSection = @"Empty";
        
        selectedSubject = @"Empty";
        
        isLeisureHour = YES;
    }
}

#pragma mark - Methods

-(void) showSelectionViewWithTextfield:(UITextField *)textField{
    
    if (!selectionView) {
        
        selectionView = [[TSSelectionViewController alloc]initWithNibName:@"TSSelectionViewController" bundle:nil];
        
        [selectionView setDelegate:self];
        
        NSMutableDictionary *contentDict = [NSMutableDictionary new];
        
        if (textField == _selectBoardTxtFld) {
        
            for (Board *board in boardArray) {
                
                [contentDict setObject:board.boardTransId forKey:board.boardName];
            }
        }
        else if (textField == _selectClassTxtFld){
            
            NSPredicate *filterPred = [NSPredicate predicateWithFormat:@"boardTransId MATCHES %@",selectedBoardTransID];
            
            NSArray *filteredArray = [NSMutableArray arrayWithArray: [boardArray filteredArrayUsingPredicate:filterPred]];
            
            boardObj = [filteredArray objectAtIndex:0];
            
            for (Classes *classObj in boardObj.classes) {
                
                [contentDict setObject:classObj.classTransId forKey:classObj.classStandardName];
            }
        }
        else if (textField == _selectSectionTxtFld){
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"classTransId MATCHES %@",selectedClassTransID];
            
            NSSet *filteredSet = [boardObj.classes filteredSetUsingPredicate:predicate];
            
            Classes *classObj = [[filteredSet allObjects] objectAtIndex:0];
            
            for (Section *secObj in classObj.section) {
                
                [contentDict setObject:secObj.sectionTransId forKey:secObj.sectionName];
            }
        }
        else if (textField == _selectSubjectTxtFld){
            
            for (CommonTypeDetails *obj in subjectsArray) {
                
                [contentDict setValue:obj.transID forKey:obj.name];
            }
        }
        
        if (contentDict.count>0) {
                    
            selectionView.selectedValue = textField.text.length>0 ? textField.text : @"";
            
            [selectionView setContentDict:contentDict];
            
            [selectionView showSelectionViewController:self forTextField:textField];
        }
        else{
            
            if (textField == _selectBoardTxtFld) {
                
                [TSSingleton showAlertWithMessage:@"No Boards to select"];
            }
            else if (textField == _selectClassTxtFld){
                
                [TSSingleton showAlertWithMessage:@"No Classes to select"];
            }
            else if (textField == _selectSectionTxtFld){
                
                [TSSingleton showAlertWithMessage:@"No Sections to select"];
            }
            else if (textField == _selectSubjectTxtFld){
                
                [TSSingleton showAlertWithMessage:@"No Subject to select"];
            }
            
            [selectionView removeSelectionViewController:self];
            
            selectionView = nil;
        }
    }
}

// Remove SelectionVC from Parent view controller
-(void) removeSelectionView{
    
    [selectionView removeSelectionViewController:self];
    
    selectionView = nil;
}

#pragma mark - Selection Callback Delegate

-(void)selectedValueFromDict:(NSDictionary *)contentDict withSelectedValue:(NSString *)selectedValue andTransID:(NSString *)transID forTextField:(UITextField *)textField{
    
    if (textField == _selectBoardTxtFld) {
        
        selectedBoardTransID = transID;
        
        selectedBoard = selectedValue;
    }
    else if (textField == _selectClassTxtFld){
        
        selectedClassTransID = transID;
        
        selectedClass = selectedValue;
    }
    else if (textField == _selectSectionTxtFld){
        
        selectedSectionTransID = transID;
        
        selectedSection = selectedValue;
    }
    else if (textField == _selectSubjectTxtFld){
        
        selectedSubjectTransID = transID;
        
        selectedSubject = selectedValue;
    }
    selectionView = nil;
}

-(void)backViewTappedOn:(UIViewController *)viewController{
    
    selectionView = nil;
}

#pragma mark - Textfield Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    _tickImgVw.image = [UIImage imageNamed:@"unapproved"];
    
    isLeisureHour = NO;
    
    if (textField == _selectBoardTxtFld) {
        
        _selectClassTxtFld.text = @"";
        
        _selectSectionTxtFld.text = @"";
        
        [self showSelectionViewWithTextfield:textField];
    }
    else if (textField == _selectClassTxtFld){
        
        if (_selectBoardTxtFld.text.length>0) {
            
            _selectSectionTxtFld.text = @"";
            
            [self showSelectionViewWithTextfield:textField];
        }
    }
    else if(textField == _selectSectionTxtFld){
        
        if (_selectClassTxtFld.text.length>0) {
            
            [self showSelectionViewWithTextfield:textField];
        }
    }
    else if (textField == _selectSubjectTxtFld){
        
        if (_selectSectionTxtFld.text.length >0) {
            
            [self showSelectionViewWithTextfield:textField];
        }
    }
    
    return NO;
}


#pragma mark Methods

-(void)changePeriod:(NSString *)period ofDay:(NSString *)day{
    
    NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
    
    NSError *error = nil;
    
    Timetable *obj = _timetableObj;
    
    switch ([period intValue]) {
            
        case 1:
            
            [obj setPeriodOneTransID:selectedSubjectTransID];
            
            [obj setPeriodOneClassTransID:selectedClassTransID];
            
            [obj setPeriodOneSecTransID: selectedSectionTransID];
            
            [obj setPeriodOne:selectedSubject];
            
            [obj setPeriodOneClass:selectedClass];
            
            [obj setPeriodOneSection:selectedSection];
            
            break;
            
        case 2:
            
            [obj setPeriodTwoTransID:selectedSubjectTransID];
            
            [obj setPeriodTwoClassTransID:selectedClassTransID];
            
            [obj setPeriodTwoSecTransID: selectedSectionTransID];
            
            [obj setPeriodTwo:selectedSubject];
            
            [obj setPeriodTwoClass:selectedClass];
            
            [obj setPeriodTwoSection:selectedSection];
            
            break;
            
        case 3:
            
             [obj setPeriodThreeTransID:selectedSubjectTransID];
             
             [obj setPeriodThreeClassTransID:selectedClassTransID];
             
             [obj setPeriodThreeSecTransID: selectedSectionTransID];
             
             [obj setPeriodThree:selectedSubject];
             
             [obj setPeriodThreeClass:selectedClass];
             
             [obj setPeriodThreeSection:selectedSection];
            
            break;
            
        case 4:
            
            [obj setPeriodFourTransID:selectedSubjectTransID];
            
            [obj setPeriodFourClassTransID:selectedClassTransID];
            
            [obj setPeriodFourSecTransID: selectedSectionTransID];
            
            [obj setPeriodFour:selectedSubject];
            
            [obj setPeriodFourClass:selectedClass];
            
            [obj setPeriodFourSection:selectedSection];
            
            break;
            
        case 5:
            
            [obj setPeriodFiveTransID:selectedSubjectTransID];
            
            [obj setPeriodFiveClassTransID:selectedClassTransID];
            
            [obj setPeriodFiveSecTransID: selectedSectionTransID];
            
            [obj setPeriodFive:selectedSubject];
            
            [obj setPeriodFiveClass:selectedClass];
            
            [obj setPeriodFiveSection:selectedSection];
            
            break;
            
        case 6:
            
            [obj setPeriodSixTransID:selectedSubjectTransID];
            
            [obj setPeriodSixClassTransID:selectedClassTransID];
            
            [obj setPeriodSixSecTransID: selectedSectionTransID];
            
            [obj setPeriodSix:selectedSubject];
            
            [obj setPeriodSixClass:selectedClass];
            
            [obj setPeriodSixSection:selectedSection];
            
            break;
            
        case 7:
            
            [obj setPeriodSevenTransID:selectedSubjectTransID];
            
            [obj setPeriodSevenClassTransID:selectedClassTransID];
            
            [obj setPeriodSevenSecTransID: selectedSectionTransID];
            
            [obj setPeriodSeven:selectedSubject];
            
            [obj setPeriodSevenClass:selectedClass];
            
            [obj setPeriodSevenSection:selectedSection];
            
            break;
            
        case 8:
            
            [obj setPeriodEightTransID:selectedSubjectTransID];
            
            [obj setPeriodEightClassTransID:selectedClassTransID];
            
            [obj setPeriodEightSecTransID: selectedSectionTransID];
            
            [obj setPeriodEight:selectedSubject];
            
            [obj setPeriodEightClass:selectedClass];
            
            [obj setPeriodEightSection:selectedSection];
            
            break;
            
        case 9:
            
            [obj setSpecialClassTransID:selectedSubjectTransID];
            
            [obj setSpecialClassClassTransID:selectedClassTransID];
            
            [obj setSpecialClassSecTransID: selectedSectionTransID];
            
            [obj setSpecialClass:selectedSubject];
            
            [obj setSpecialClassClass:selectedClass];
            
            [obj setSpecialClassSec:selectedSection];
            
            break;
            
        default:
            break;
    }
    
    if (![context save:&error]) {
        
        NSLog(@"Error during saving records");
    }
}

//#pragma mark - Service Call
//
//-(void) fetchSubjectsServiceCall{
//    
//    NSString *urlString = @"MastersMServices/MastersMService.svc";
//    
//    NSDictionary *param = @{@"intTypeID":@"15"};
//    
//    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:NO onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
//       
//        if (success) {
//            
//            if ([responseDictionary[@"FetchMastersbyTypeResult"][@"ResponseCode"] intValue] == 1) {
//                
//                
//            }
//        }
//        
//    }];
//}

@end