//
//  Holidays.m
//  Trackidon Staff
//
//  Created by Elango on 23/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "Holidays.h"
#import "StaffDetails.h"

@implementation Holidays

// Method to get static instance
+(instancetype) instance{
    
    static Holidays *holidaysObj = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        holidaysObj = [[Holidays alloc]init];
    });
    
    return holidaysObj;
}


-(void)storeHolidaysForStaff:(StaffDetails *)staffObj withDict:(NSDictionary *)dictionary{
    
    NSError *error = nil;
    
    //NSManagedObjectContext *context = [[TSAppDelegate instance] managedObjectContext];
    
    NSManagedObjectContext *mainContext = [APPDELEGATE_INSTANCE managedObjectContext];
    
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    
    [context setParentContext:mainContext];
        
    NSManagedObjectID *objectId = [SINGLETON_INSTANCE.staff objectID];
    
    StaffDetails *tmpStaff = [context objectWithID:objectId];

    
    for (NSDictionary *tmpDict in dictionary[@"FetchAllByEventsResult"][@"EventsMClass"]) {
        
        NSDictionary *dict = [tmpDict dictionaryByReplacingNullsWithBlanks];
        
        
        Holidays *holidaysObj = [self isHolidayAlreadyExistsWithTransID:dict[@"TransID"] andStaffObj:staffObj context:context];
        
        if (holidaysObj == nil) {
            
            holidaysObj = [NSEntityDescription insertNewObjectForEntityForName:@"Holidays" inManagedObjectContext:context];
        }
        
        holidaysObj.academicTransID = dict[@"AcadamicTransID"];
        holidaysObj.approvedByDate = dict[@"ApprovedDate"];
        holidaysObj.approvedByName = dict[@"ApprovedByName"];
        holidaysObj.approvedByTransID = dict[@"ApprovedByName"];
        holidaysObj.approvedStatus = [NSString stringWithFormat:@"%@",dict[@"ApprovedStatus"]];
        holidaysObj.attachment = dict[@"Attachment"];
        holidaysObj.classSection = dict[@"ClassSection"];
        holidaysObj.dateTimeModified = dict[@"DatetimeModified"];
        holidaysObj.holidayDescription = dict[@"Description"];
        holidaysObj.holidayTime = dict[@"EventTime"];
        holidaysObj.holidayTitle = dict[@"EventTitle"];
        holidaysObj.holidayTypeID = [NSString stringWithFormat:@"%@",dict[@"EventTypeID"]];
        holidaysObj.fromDate = dict[@"FromDate"];
        holidaysObj.isAttachment = dict[@"IsAttachment"];
        holidaysObj.schoolTransID = dict[@"SchoolTransID"];
        holidaysObj.toDate = dict[@"ToDate"];
        holidaysObj.transID = dict[@"TransID"];
        holidaysObj.userCreatedTransID = dict[@"UserCreated"];
        holidaysObj.userCreatedName = dict[@"UserCreatedName"];
        holidaysObj.userModifiedName = dict[@"UserModifiedName"];
        holidaysObj.userModifiedTransID = dict[@"UserModified"];
        holidaysObj.userTransID = dict[@"UserTransID"];
        holidaysObj.venue = dict[@"Venue"];
        
        holidaysObj.staffTransID = staffObj.staffTransId;
        
        holidaysObj.staff = tmpStaff;
        
        if (![context save:&error]) {
            
            NSLog(@"Error during saving holidays");
        }
        
        [SINGLETON_INSTANCE storeContextInMainThread];
    }
}


// Method to check whether Holiday is already exists or not
-(Holidays *)isHolidayAlreadyExistsWithTransID:(NSString *)transID andStaffObj:(StaffDetails *)staffObj context:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Holidays" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"staffTransID contains[cd] %@ AND transID MATCHES %@",staffObj.staffTransId,transID];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
}

@end
