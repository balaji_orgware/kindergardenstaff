//
//  ReceiverFilterList+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 02/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ReceiverFilterList.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReceiverFilterList (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *filterDescription;
@property (nullable, nonatomic, retain) NSString *filterTypeId;
@property (nullable, nonatomic, retain) NSString *menuId;
@property (nullable, nonatomic, retain) NSNumber *isSelected;
@property (nullable, nonatomic, retain) StaffDetails *staff;

@end

NS_ASSUME_NONNULL_END
