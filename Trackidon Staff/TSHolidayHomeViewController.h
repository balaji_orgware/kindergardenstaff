//
//  TSHolidayHomeViewController.h
//  Trackidon Staff
//
//  Created by Elango on 23/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JTCalendar.h"

typedef NS_ENUM(NSInteger,HolidayType) {
    
    HolidayTypePublicHoliday = 24,
    HolidayTypeSchoolHoliday
};

typedef NS_ENUM(NSInteger,HolidayReturnType) {
    
    HolidayReturnTypeNone = 0,
    HolidayReturnTypePublicHoliday,
    HolidayReturnTypeSchoolHoliday
};


@interface TSHolidayHomeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;

@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;

@property (weak, nonatomic) IBOutlet UITableView *menuTableView;

@property (weak, nonatomic) IBOutlet UIView *listView;

@property (nonatomic,strong) JTCalendarManager *calendarManager;

@property (nonatomic,strong) NSDate *dateSelected;

@property (weak, nonatomic) IBOutlet UIButton *prevMnthBtn;
@property (weak, nonatomic) IBOutlet UIButton *nxtMnthBtn;

- (IBAction)prevMonthButtonTapped:(id)sender;
- (IBAction)nextMonthButtonTapped:(id)sender;

@end
