//
//  Portions.m
//  Trackidon Staff
//
//  Created by Balaji on 22/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "Portions.h"

@implementation Portions

// Get Static Instance
+(instancetype) instance{
    
    static Portions *portionObj = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        portionObj = [[Portions alloc]init];
    });
    
    return portionObj;
}

// Method to store Portions Data to Core Data
-(void) storePortionsForStaffObj:(StaffDetails *)staffObj withDict:(NSDictionary *)dictionary isForApprovals:(BOOL)isForApprovals{
    
    @try {
        
        NSManagedObjectContext *mainContext = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSArray *responseArray = [NSArray new];
        
        NSPredicate *predicate;
        
        if (isForApprovals) {
            
            responseArray = dictionary[@"FetchUnApprovedCommunicationResult"][@"UnApprovalClass"][@"PortionsClass"];
            
            predicate = [NSPredicate predicateWithFormat:@"isForApprovals == 1"];
            
        }
        else{
            
            responseArray = dictionary[@"FetchPortionsByStaffResult"][@"PortionsClass"];
            
            predicate = [NSPredicate predicateWithFormat:@"isForApprovals == 0"];
        }
        
        [SINGLETON_INSTANCE deleteSelectedObjectsInEntityWithEntityName:@"Portions" withPredicate:predicate withContext:mainContext];
        
        NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        
        [context setParentContext:mainContext];
        
        NSManagedObjectID *objectId = [staffObj objectID];
        
        StaffDetails *tmpStaff = [context objectWithID:objectId];
        
        for (NSDictionary *dictionary in responseArray) {
            
            NSDictionary *dict = [dictionary dictionaryByReplacingNullsWithBlanks];
            
            Portions *portionObj;
            
            portionObj = [NSEntityDescription insertNewObjectForEntityForName:@"Portions" inManagedObjectContext:context];
            
            portionObj.academicTransID = dict[@"AcadamicTransID"];
            portionObj.academicYear = dict[@"AcadamicYear"];
            portionObj.approvedByName = dict[@"ApprovedByName"];
            portionObj.approvedByTransID = dict[@"ApprovedBy"];
            portionObj.approvedDate = dict[@"ApprovedDate"];
            portionObj.attachment = dict[@"Attachment"];
            portionObj.approvedStatus = dict[@"ApprovedStatus"];
            portionObj.createdByName = dict[@"CreatedBy"];
            portionObj.isAttachment = dict[@"IsAttachment"];
            portionObj.modifiedByName = dict[@"ModifiedBy"];
            portionObj.portionDate = dict[@"PortionsDate"];
            portionObj.portionDescription = dict[@"PortionsDescription"];
            portionObj.portionTitle = dict[@"PortionsTitle"];
            portionObj.priority = dict[@"Priority"];
            portionObj.schoolName = dict[@"SchoolName"];
            portionObj.schoolTransID = dict[@"SchoolTransID"];
            portionObj.transID = dict[@"TransID"];
            portionObj.modifiedDate = [SINGLETON_INSTANCE dateFromString:dict[@"ModifiedDate"] withFormate:DATE_FORMATE_MODIFIED_DATE];
            
            portionObj.staffTransID = staffObj.staffTransId;
            portionObj.staff = tmpStaff;
            portionObj.subjectTitle = dict[@"SubjectName"];
            
            portionObj.boardName = dict[@"BoardName"];
            
            portionObj.isForApprovals = [NSNumber numberWithBool:isForApprovals];
            
            NSString *assigneeList = [[TSCommonAssignedDetails instance] getAssignedDetailsWithArray:dict[@"PortionsDetails"] withGroupType:dict[@"GroupType"]];
            
            portionObj.assignedList = assigneeList;

            //[[AssignedDetails instance]saveAssignedDetailsWithDict:dict[@"PortionsDetails"] withGroupType:dict[@"GroupType"] transId:portionObj.transID forObject:portionObj objectType:OBJECT_TYPE_PORTIONS context:context];
        }
        
        NSError *error;
        
        if (![context save:&error]) {
            
            NSLog(@"Failed to save Portions");
        }
        
        [SINGLETON_INSTANCE storeContextInMainThread];
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
    }
}

// Method to check whether portion data already exist or not
-(Portions *) isPortionAlreadyExistsWithTransID:(NSString *)transID andStaffObj:(StaffDetails *)staffObj withContext:(NSManagedObjectContext *)context isForApprovals:(BOOL)isForApprovals{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Portions" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred;
    
    if (isForApprovals) {
        
        pred = [NSPredicate predicateWithFormat:@"transID MATCHES %@ AND staffTransID MATCHES %@ AND isForApprovals == 1",transID,staffObj.staffTransId];
    }
    else{
        
        pred = [NSPredicate predicateWithFormat:@"transID MATCHES %@ AND staffTransID MATCHES %@ AND isForApprovals == 0",transID,staffObj.staffTransId];
    }
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
}


// Method to store push notification portion
-(void) storePushNotificationPortionWithDict:(NSDictionary *)dictionary{
    
    @try {
        
        NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSError *error = nil;
        
        NSDictionary *dict = [dictionary[@"FetchPortionsByTransIDResult"][@"PortionsClass"] dictionaryByReplacingNullsWithBlanks];
        
        Portions *portionObj = [self isPortionAlreadyExistsWithTransID:dict[@"TransID"] andStaffObj:SINGLETON_INSTANCE.staff withContext:context isForApprovals:NO];
        
        if (portionObj == nil) {
            
            portionObj = [NSEntityDescription insertNewObjectForEntityForName:@"Portions" inManagedObjectContext:context];
        }
        
        portionObj.academicTransID = dict[@"AcadamicTransID"];
        portionObj.academicYear = dict[@"AcadamicYear"];
        portionObj.approvedByName = dict[@"ApprovedByName"];
        portionObj.approvedByTransID = dict[@"ApprovedBy"];
        portionObj.approvedDate = dict[@"ApprovedDate"];
        portionObj.attachment = dict[@"Attachment"];
        portionObj.approvedStatus = dict[@"ApprovedStatus"];
        portionObj.createdByName = dict[@"CreatedBy"];
        portionObj.isAttachment = dict[@"IsAttachment"];
        portionObj.modifiedByName = dict[@"ModifiedBy"];
        portionObj.portionDate = dict[@"PortionsDate"];
        portionObj.portionDescription = dict[@"PortionsDescription"];
        portionObj.portionTitle = dict[@"PortionsTitle"];
        portionObj.priority = dict[@"Priority"];
        portionObj.schoolName = dict[@"SchoolName"];
        portionObj.schoolTransID = dict[@"SchoolTransID"];
        portionObj.transID = dict[@"TransID"];
        portionObj.modifiedDate = [SINGLETON_INSTANCE dateFromString:dict[@"ModifiedDate"] withFormate:DATE_FORMATE_MODIFIED_DATE];
        
        portionObj.staffTransID = SINGLETON_INSTANCE.staffTransId;
        portionObj.staff = SINGLETON_INSTANCE.staff;
        portionObj.subjectTitle = dict[@"SubjectName"];
        
        portionObj.boardName = dict[@"BoardName"];
        
        portionObj.isForApprovals = [NSNumber numberWithBool:NO];
        
        NSString *assigneeList = [[TSCommonAssignedDetails instance] getAssignedDetailsWithArray:dict[@"PortionsDetails"] withGroupType:dict[@"GroupType"]];
        
        portionObj.assignedList = assigneeList;

        
        if (![context save:&error]) {
            
            NSLog(@"Whoops couldn't save");
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}
@end