//
//  TSHandlerClass.m
//  Trackidon Staff
//
//  Created by ORGware on 17/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSHandlerClass.h"

@implementation TSHandlerClass
{
    MBProgressHUD *hud;
}


+(instancetype) sharedHandler{
    
    static TSHandlerClass *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        kSharedInstance = [[self alloc]init];
        
    });
    
    return kSharedInstance;
}


-(void) postRequestWithURL:(NSString *)urlString baseURL:(NSString *)baseURL param:(NSDictionary *)params currentView:(UIView *)view showIndicator:(BOOL)showIndicator onCompletion:(completionHandler)handler{
    
    if (showIndicator && view != nil){
        
        [hud hide:YES];
    
        hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        
        //[hud setAnimationType:MBProgressHUDAnimationZoomIn];
        
        [hud show:YES];
    }else {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    }
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithBaseURL:[NSURL URLWithString:baseURL]];
    
    AFSecurityPolicy *securityPolicy = [[AFSecurityPolicy alloc]init];
    
    [securityPolicy setAllowInvalidCertificates:YES];
    
    [manager setSecurityPolicy:securityPolicy];

    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    if ([SINGLETON_INSTANCE checkForActiveInternet]) {
        
        [manager POST:urlString parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSDictionary *dictionary = [[NSDictionary alloc]init];
            
            dictionary = (NSDictionary *)responseObject;
            
            /*NSMutableDictionary *analyticsDict = [NSMutableDictionary new];
            
            [analyticsDict setObject:[NSString stringWithFormat:@"%@%@",baseURL,urlString] forKey:ANALYTICS_PARAM_URL];
            
            [analyticsDict setObject:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_RESPONSE];
            
            [analyticsDict setObject:@"No Error" forKey:ANALYTICS_PARAM_ERRORCODE];
            
            [analyticsDict setObject:@"No Error" forKey:ANALYTICS_PARAM_ERROR];
            
            [TSAnalytics logEvent:ANALYTICS_WEBSERVICE_SERVICECALL parameter:analyticsDict];*/
            
            if (showIndicator) {
                
                [hud hide:YES];
                
                [hud removeFromSuperview];
                
            }else {
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            }
            
            handler(YES,dictionary);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            if(showIndicator){
                
                //[SINGLETON_INSTANCE showAlertMessageWithTitle:nil andMessage:TEXT_FAILURERESPONSE];
                [TSSingleton requestTimeOutErrorAlert];
                
                [hud hide:YES];
                
                [hud removeFromSuperview];
                
            }else {
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            }
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary new];
            
            [analyticsDict setObject:[NSString stringWithFormat:@"%@%@",baseURL,urlString] forKey:ANALYTICS_PARAM_URL];
            
            [analyticsDict setObject:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_RESPONSE];
            
            [analyticsDict setObject:[NSString stringWithFormat:@"%ld",(long)error.code] forKey:ANALYTICS_PARAM_ERRORCODE];
            
            [analyticsDict setObject:error.localizedDescription forKey:ANALYTICS_PARAM_ERROR];
            
            [TSAnalytics logEvent:ANALYTICS_WEBSERVICE_SERVICECALL parameter:analyticsDict];
            
            handler(NO,nil);
        }];
    }
    else{
        
        if(showIndicator){
            
            [TSSingleton showAlertWithMessage:TEXT_NOINTERNET];
            
            //[SINGLETON_INSTANCE showAlertMessageWithTitle:nil andMessage:TEXT_NOINTERNET];
            
            [hud hide:YES];
            
            [hud removeFromSuperview];
            
        }else {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
        
        handler(NO,nil);
    }
}

/*
-(void)postRequestWithURLSession:(NSString *)urlString baseURL:(NSString *)baseURL param:(NSDictionary *)params currentView:(UIView *)view showIndicator:(BOOL)showIndicator onCompletion:(completionHandler)handler{

    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSError *error = nil;
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    
    NSURL *url = [NSURL URLWithString:urlString relativeToURL:[NSURL URLWithString:baseURL]];
    
    NSMutableURLRequest *urlReq = [NSMutableURLRequest requestWithURL:url];
    
    [urlReq addValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    
    [urlReq setHTTPMethod:@"POST"];
    
    [urlReq setHTTPBody:[NSJSONSerialization dataWithJSONObject:params options:0 error:&error]];
    
    NSURLSessionDataTask *task = [urlSession dataTaskWithRequest:urlReq completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
     
        if (!error) {
            
            NSDictionary *jsondict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            handler(YES,jsondict);
        }
    }];
    
    [task resume];
}*/


-(void)downloadAttachmentWithURL:(NSString *)attachmentURL currentView:(UIView *)view withHandler:(downloadHandler)downloadHandler{
    
    if ([SINGLETON_INSTANCE reachabilityCheck]) {
        
        hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        
        [hud setMode:MBProgressHUDModeIndeterminate];
        
        [hud setLabelText:@"Downloading"];
        
        _attachmentUrl = [NSURL URLWithString:attachmentURL];
        
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:_attachmentUrl                                                  cachePolicy:NSURLRequestReloadIgnoringLocalCacheData                 timeoutInterval:60.0];
        
        AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
        
        NSURLSessionDownloadTask *downloadTask = [sessionManager downloadTaskWithRequest:urlRequest progress:^(NSProgress * _Nonnull downloadProgress) {
            
            hud.progress = downloadProgress.fractionCompleted;
            
        } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString  *filePathUrl = [NSString stringWithFormat:@"%@/%@", documentsDirectory,[_attachmentUrl lastPathComponent]];
            
            NSURL *destFilePath = [NSURL fileURLWithPath:filePathUrl];
            
            return destFilePath;
            
        } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
            
            if (!error) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary new];
                
                [analyticsDict setObject:attachmentURL forKey:ANALYTICS_PARAM_URL];
                
                [analyticsDict setObject:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_RESPONSE];
                
                [analyticsDict setObject:@"No Error" forKey:ANALYTICS_PARAM_ERRORCODE];
                
                [analyticsDict setObject:@"No Error" forKey:ANALYTICS_PARAM_ERROR];
                
                [TSAnalytics logEvent:ANALYTICS_DOWNLOADATTACHMENT_REQUEST parameter:analyticsDict];
                
                [hud hide:YES];
                
                [hud removeFromSuperview];

                downloadHandler(YES,filePath);
                
            }
            else{
                
                [hud hide:YES];
                
                [hud removeFromSuperview];
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary new];
                
                [analyticsDict setObject:attachmentURL forKey:ANALYTICS_PARAM_URL];
                
                [analyticsDict setObject:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_RESPONSE];
                
                [analyticsDict setObject:[NSString stringWithFormat:@"%ld",(long)error.code] forKey:ANALYTICS_PARAM_ERRORCODE];
                
                [analyticsDict setObject:error.localizedDescription forKey:ANALYTICS_PARAM_ERROR];
                
                downloadHandler(NO,[NSURL new]);
            }
        }];
        
        [downloadTask resume];
        
        
        } else {
        
        [TSSingleton showAlertWithMessage:TEXT_NOINTERNET];
        
    }
}



@end
