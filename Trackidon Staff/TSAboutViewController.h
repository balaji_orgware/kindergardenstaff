//
//  TSAboutViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/23/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface TSAboutViewController : UIViewController<UIScrollViewDelegate,MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *navigationView;

@property (weak, nonatomic) IBOutlet UIScrollView *aboutScrollView;
@property (weak, nonatomic) IBOutlet UIView *contactInfoVw;
@property (weak, nonatomic) IBOutlet UITextView *aboutTxtVw;

@property (weak, nonatomic) IBOutlet UILabel *supportEmailLbl;
@property (weak, nonatomic) IBOutlet UILabel *webUrlLbl;

@property (weak, nonatomic) IBOutlet UILabel *contactNo1Lbl;

@property (weak, nonatomic) IBOutlet UILabel *contactNo2Lbl;

@end
