//
//  TSAnalytics.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 5/9/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSAnalytics.h"

@implementation TSAnalytics

+(void)logEvent:(NSString *)event parameter:(NSMutableDictionary *)parameter{
    
    @try {
        
        [parameter setObject:[self reachabilityCheck] forKey:ANALYTICS_PARAM_NETWORK_STATUS];
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isLogin"]) {
            
            [parameter setObject:SINGLETON_INSTANCE.staff.inChargeClassName forKey:ANALYTICS_PARAM_CLASS_NAME];
            
            [parameter setObject:[USERDEFAULTS valueForKey:@"schoolName"] forKey:ANALYTICS_PARAM_SCHOOL_NAME];
            
            [Localytics setValue:[USERDEFAULTS valueForKey:@"schoolName"]
             forProfileAttribute:ANALYTICS_PARAM_SCHOOL_NAME withScope:LLProfileScopeApplication];
            
        }
        
    } @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    }
    
    [self sendEvent:event withParams:parameter];
}


+(void)logScreen:(NSString *)screen parameter:(NSMutableDictionary *)parameter{
    
    
    /*@try {
     
     } @catch (NSException *exception) {
     
     NSLog(@"%@",exception);
     
     }*/
    
    [self sendScreen:screen withParams:parameter];
    
}

+ (void)sendEvent:(NSString *)event withParams:(NSMutableDictionary *)parameter{
    
    //Localytics
    
    if(!DEBUGGING)
        [Localytics tagEvent:event attributes:parameter];
    
}

+ (void)sendScreen:(NSString *)screen withParams:(NSMutableDictionary *)parameter{
    
    //Localytics
    if(!DEBUGGING)
        [Localytics tagScreen:screen];
}

// Reachability check for Internet Connection
+ (NSString *)reachabilityCheck{
    
    NSString *status;
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {
        
        status = ANALYTICS_VALUE_OFFLINE;
        
    }else{
        status = ANALYTICS_VALUE_ONLINE;
    }
    
    return status;
}



@end
