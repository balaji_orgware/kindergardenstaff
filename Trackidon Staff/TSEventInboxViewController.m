//
//  TSEventInboxViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/2/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSEventInboxViewController.h"

@interface TSEventInboxViewController ()
{
    NSArray *eventArr,*attachmentImgArray;
}
@end

@implementation TSEventInboxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [TSAnalytics logEvent:ANALYTICS_INBOXEVENT_OPENED parameter:[NSMutableDictionary new]];
    
    attachmentImgArray = @[@"one",@"two",@"three",@"four",@"five"];
    
    [_eventsTableView setTableFooterView:[UIView new]];


    [self fetchEvents];
    
    [self fetchEventInbox];
}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_INBOXEVENT parameter:[NSMutableDictionary new]];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Event Log";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Tableview Delegate & Datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return eventArr.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    UILabel *titleLbl = (UILabel *)[cell viewWithTag:101];
    
    UILabel *createdByLbl = (UILabel *)[cell viewWithTag:102];
    
    UILabel *dateLbl = (UILabel *)[cell viewWithTag:103];
    
    UIImageView *attachmentImgVw = (UIImageView *)[cell viewWithTag:104];
    
    EventInbox *eventInboxObj = [eventArr objectAtIndex:indexPath.row];
    
    titleLbl.text = eventInboxObj.eventTitle;
    
    createdByLbl.text = [NSString stringWithFormat:@"Created By : %@",eventInboxObj.userCreatedName];
    
    dateLbl.text = [ SINGLETON_INSTANCE dateLblTextWithFromDate:eventInboxObj.fromDate toDate:eventInboxObj.toDate];
    
    if ([eventInboxObj.isHasAttachment intValue] == 1) {
        
        NSString *attachmentImgName;
        
        switch (indexPath.row%5) {
                
            case 0:
                attachmentImgName = @"one";
                break;
                
            case 1:
                attachmentImgName = @"two";
                break;
                
            case 2:
                attachmentImgName = @"three";
                break;
                
            case 3:
                attachmentImgName = @"four";
                break;
                
            case 4:
                attachmentImgName = @"five";
                break;
                
            default:
                attachmentImgName = @"one";
                break;
        }
        attachmentImgVw.image = [UIImage imageNamed:attachmentImgName];
        
        [attachmentImgVw setHidden:NO];
    }
    else{
        
        [attachmentImgVw setHidden:YES];
    }

    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [TSAnalytics logEvent:ANALYTICS_INBOXEVENT_CLICKEDINLIST parameter:[NSMutableDictionary new]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TSEventsDescriptionViewController *eventDesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBEventsDescription"];
    
    id eventInbox = (EventInbox *)[eventArr objectAtIndex:indexPath.row];
    
    eventDesVC.descriptionType = EventsInboxString;
    
    eventDesVC.commonObj = eventInbox;
    
    [self.navigationController pushViewController:eventDesVC animated:YES];
}

-(void)fetchEventInbox{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    NSString *url = @"MastersMServices/SchoolNotificationsService.svc/FetchSchoolStaffNotificationsByStaffID";
    
    [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidStaffTransID"];
    
    BOOL animated;
    
    if (eventArr.count == 0)
        animated = YES;
    
    else
        animated = NO;
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:url baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:animated onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchSchoolStaffNotificationsByStaffIDResult"][@"ResponseCode"]intValue]==1) {
                
                NSLog(@"%@",responseDictionary);

                if (responseDictionary[@"FetchSchoolStaffNotificationsByStaffIDResult"][@"EventsMClass"] != [NSNull null]) {
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                        [[EventInbox instance]saveEventInboxWithResponseDict:responseDictionary];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self fetchEvents];
                            
                        });
                    });
                }
            }
        }
    }];
}

- (void)fetchEvents{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransID CONTAINS[cd] %@",SINGLETON_INSTANCE.staffTransId];
    
    eventArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"EventInbox" withPredicate:predicate];
    
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:NO];
    
    eventArr = [[eventArr sortedArrayUsingDescriptors:@[dateDescriptor]] mutableCopy];
    
    if (eventArr.count>0) {
        
        [self.eventsTableView setHidden:NO];
    }
    else{
        
        [self.eventsTableView setHidden:YES];
    }
    
    [self.eventsTableView reloadData];
    
}

@end
