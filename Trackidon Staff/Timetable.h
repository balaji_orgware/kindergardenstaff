//
//  Timetable.h
//  Trackidon Staff
//
//  Created by Balaji on 23/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StaffDetails;

NS_ASSUME_NONNULL_BEGIN

@interface Timetable : NSManagedObject

// Method to Get Static Instance
+(instancetype)instance;

// Method to Store Timetable to core data
-(void)storeTimeTableForStaff:(StaffDetails *)staffObj withDict:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END

#import "Timetable+CoreDataProperties.h"
