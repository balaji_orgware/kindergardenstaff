//
//  Board+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 14/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Board+CoreDataProperties.h"

@implementation Board (CoreDataProperties)

@dynamic boardName;
@dynamic boardTransId;
@dynamic isViewableByStaff;
@dynamic schoolName;
@dynamic schoolTransId;
@dynamic classes;
@dynamic school;

@end
