//
//  Events+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 28/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Events+CoreDataProperties.h"

@implementation Events (CoreDataProperties)

@dynamic acadamicTransId;
@dynamic approvedByName;
@dynamic approvedByTransId;
@dynamic approvedDate;
@dynamic approvedStatus;
@dynamic assignedList;
@dynamic boardName;
@dynamic boardTransID;
@dynamic eventAttachmentUrl;
@dynamic eventDescription;
@dynamic eventFromDate;
@dynamic eventGroupType;
@dynamic eventIsHasAttachment;
@dynamic eventTime;
@dynamic eventTitle;
@dynamic eventToDate;
@dynamic eventTransId;
@dynamic eventTypeId;
@dynamic eventVenue;
@dynamic isForApprovals;
@dynamic modifiedDate;
@dynamic schoolTransId;
@dynamic secondLangTransID;
@dynamic staffTransId;
@dynamic userCreatedName;
@dynamic userCreatedTransId;
@dynamic userModifiedName;
@dynamic userModifiedTransId;
@dynamic staff;

@end
