//
//  Trust.h
//  Trackidon Staff
//
//  Created by Elango on 16/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Trust : NSManagedObject

+ (instancetype)instance;

- (void)saveTrustWithResponseDict:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END

#import "Trust+CoreDataProperties.h"
