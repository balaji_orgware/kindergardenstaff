//
//  TSMyClassTimetableViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 15/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSMyClassTimetableViewController.h"

@interface TSMyClassTimetableViewController (){
    
    NSMutableArray *timeArray,*timeTableDataArray;
    
    NSString *selectedDay;

}
@end

@implementation TSMyClassTimetableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setSelectedDateAsToday];
    
    timeTableDataArray = [NSMutableArray new];
    
    timeArray = [[NSMutableArray alloc] initWithObjects:@"08.30 AM - 09.15 AM",@"09.15 AM - 10.00 AM",@"10.15 AM - 11.00 AM",@"11.00 AM - 11.45 AM",@"01.00 PM - 01.45 PM",@"01.45 PM - 02.30 PM",@"02.45 PM - 03.30 PM",@"03.30 PM - 04.15 PM",@"04.15 PM - 05.00 PM", nil];

    [self.timeTableTableView setTableFooterView:[UIView new]];
    
    [self categorizeTimeTable];
    
    [self fetchClassTimeTableServiceCall];
}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_MYCLASSTIMETABLE parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"My Class Timetable";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button Actions

- (IBAction)dayButtonTapped:(id)sender {

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [sender setSelected:NO];
    
    [self setNormalTextColorsToAllButtons];
    
    [sender setTitleColor:APPTHEME_COLOR forState:UIControlStateNormal];
    
    NSString *dayName;
    
    switch ([sender tag]) {
            
        case 10:
            
            selectedDay = @"MON";
            
            dayName = @"Monday";
            
            break;
            
        case 11:
            
            selectedDay = @"TUE";
            
            dayName = @"Tuesday";
            
            break;
            
        case 12:
            
            selectedDay = @"WED";
            
            dayName = @"Wednesday";
            
            break;
            
            
        case 13:
            
            selectedDay = @"THU";
            
            dayName = @"Thursday";
            
            break;
            
        case 14:
            
            selectedDay = @"FRI";
            
            dayName = @"Friday";
            
            break;
            
        case 15:
            
            selectedDay = @"SAT";
            
            dayName = @"Saturday";
            
            break;
            
        default:
            break;
    }
    
    NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
    
    [analyticsDict setValue:dayName forKey:ANALYTICS_PARAM_DAY];
    
    [analyticsDict setValue:ANALYTICS_VALUE_MYCLASSTIMETABLE forKey:ANALYTICS_PARAM_TYPE];
    
    [TSAnalytics logEvent:ANALYTICS_TIMETABLE_CLICKED_DAY parameter:analyticsDict];
    
    Timetable *timeTableObj = [self getPeriodsForDay:selectedDay];
    
    if (timeTableObj !=nil){
        
        [_timeTableTableView setHidden:NO];
        
        [_timeTableTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
        [_timeTableTableView reloadData];
    }
    else{
        
        [_timeTableTableView setHidden:YES];
    }
}


#pragma mark - Helper Methods

-(UIColor *)getUIColorFromRGB:(int)red andBlue:(int)blue andGreen:(int)green{
    
    return [UIColor colorWithRed:red/255.0 green:blue/255.0 blue:green/255.0 alpha:1.0];
}

// Method to filter Particular day record from DB
-(Timetable *)getPeriodsForDay:(NSString *)daySelected{
    
    NSPredicate *periodCountPred = [NSPredicate predicateWithFormat:@"day MATCHES %@",daySelected];
    
    NSArray *filteredArray = [timeTableDataArray filteredArrayUsingPredicate:periodCountPred];
    
    if (filteredArray.count>0)
        return [filteredArray objectAtIndex:0];
    else
        return nil;
}


// Method to change Button Colors for all day buttons
-(void) setNormalTextColorsToAllButtons{
    
    for (id isBtn in self.daysView.subviews) {
        
        if ([isBtn isKindOfClass:[UIButton class]]) {
            
            UIButton *daysBtn = (UIButton *)isBtn;
            
            if ([daysBtn tag] != 0) {
                
                [daysBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                
                [TSSingleton layerDrawForView:daysBtn position:LAYER_TOP color:[UIColor lightGrayColor]];
                
                //[TPSingleton layerDrawForView:daysBtn position:LAYER_RIGHT color:[UIColor lightGrayColor]];
            }
        }
    }
}

// Fetch Data from DB
-(void) categorizeTimeTable{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@",SINGLETON_INSTANCE.staffTransId];
    
    [timeTableDataArray removeAllObjects];
    
    timeTableDataArray = [NSMutableArray arrayWithArray:[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Timetable" withPredicate:predicate]];
    
    if (timeTableDataArray.count>0) {
        
        [_daysView setHidden:NO];
        
        [_timeTableTableView setHidden:NO];
    }
    else{
        
        [_daysView setHidden:YES];
        
        [_timeTableTableView setHidden:YES];
    }
    
    [self.timeTableTableView reloadData];
}

// Set Selected Date as Today date. Change Day Button Text colors
-(void) setSelectedDateAsToday{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [calendar components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
    
    UIButton *button;
    
    switch ([components weekday]) {
            
        case 2:
            
            selectedDay = @"MON";
            
            button = (UIButton *)[self.view viewWithTag:10];
            
            break;
            
        case 3:
            
            selectedDay = @"TUE";
            
            button = (UIButton *)[self.view viewWithTag:11];
            
            break;
            
        case 4:
            
            selectedDay = @"WED";
            
            button = (UIButton *)[self.view viewWithTag:12];
            
            break;
            
        case 5:
            
            selectedDay = @"THU";
            
            button = (UIButton *)[self.view viewWithTag:13];
            
            break;
            
        case 6:
            
            selectedDay = @"FRI";
            
            button = (UIButton *)[self.view viewWithTag:14];
            
            break;
            
        case 7:
            
            selectedDay = @"SAT";
            
            button = (UIButton *)[self.view viewWithTag:15];
            
            break;
            
        default:
            
            selectedDay = @"MON";
            
            button = (UIButton *)[self.view viewWithTag:10];
            
            break;
    }
    
    [button setTitleColor:APPTHEME_COLOR forState:UIControlStateNormal];
    
    [self.timeTableTableView reloadData];
}


#pragma mark - TableView Datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    Timetable *timeTableObj = [self getPeriodsForDay:selectedDay];

    if (timeTableObj == nil) {
        
        [self.timeTableTableView setHidden:YES];
    }
    else{
        
        [self.timeTableTableView setHidden:NO];
    }
    
    return 9;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sCellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UILabel *rowCountLabel = (UILabel *)[cell viewWithTag:101];
    
    UILabel *subjectNameLabel = (UILabel *)[cell viewWithTag:102];
    
    UILabel *durationLabel = (UILabel *)[cell viewWithTag:103];
    
    UIImageView *notesImgVw = (UIImageView *)[cell viewWithTag:104];
    
    Timetable *timeTableObj = [self getPeriodsForDay:selectedDay];
    
    if (timeTableObj !=nil) {
        
        switch (indexPath.row) {
                
            case 0:
                
                subjectNameLabel.text = timeTableObj.periodOne;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:255 andBlue:137 andGreen:139];
                break;
                
            case 1:
                
                subjectNameLabel.text = timeTableObj.periodTwo;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:255 andBlue:186 andGreen:139];
                break;
                
            case 2:
                
                subjectNameLabel.text = timeTableObj.periodThree;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:103 andBlue:131 andGreen:175];
                break;
                
            case 3:
                
                subjectNameLabel.text = timeTableObj.periodFour;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:112 andBlue:210 andGreen:178];
                break;
                
            case 4:
                
                subjectNameLabel.text = timeTableObj.periodFive;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:68 andBlue:214 andGreen:251];
                break;
                
            case 5:
                
                subjectNameLabel.text = timeTableObj.periodSix;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:107 andBlue:101 andGreen:186];
                break;
                
            case 6:
                
                subjectNameLabel.text = timeTableObj.periodSeven;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:163 andBlue:51 andGreen:183];
                break;
                
            case 7:
                
                subjectNameLabel.text = timeTableObj.periodEight;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:0 andBlue:152 andGreen:94];
                break;
                
            case 8:
                
                subjectNameLabel.text = timeTableObj.specialClass;
                rowCountLabel.backgroundColor = [self getUIColorFromRGB:72 andBlue:72 andGreen:72];
                break;
                
            default:
                break;
        }
    }else{
        
        subjectNameLabel.text = @"No Data Found";
    }
    
    rowCountLabel.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    
    if (indexPath.row <timeArray.count-1)
        durationLabel.text = [timeArray objectAtIndex:indexPath.row];
    
    else
        durationLabel.text = @"Special Class";
    
    
    rowCountLabel.layer.cornerRadius = rowCountLabel.frame.size.width/2;
    
    rowCountLabel.layer.masksToBounds = YES;
    
    [notesImgVw setHidden:YES];
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

#pragma mark - Service Call

-(void) fetchClassTimeTableServiceCall{
    
    NSString *urlString = @"MastersMServices/TimeTableService.svc/FetchTimetableByClassTransID";
    
    StaffDetails *staff = [SINGLETON_INSTANCE staff];
    
    NSDictionary *params = @{@"guidSchoolTransID":SINGLETON_INSTANCE.selectedSchoolTransId,@"guidAcdamicTransID":SINGLETON_INSTANCE.academicTransId,@"guidClassTransID":staff.inChargeClassTransId,@"guidSectionTransID":staff.inChargeSectionTransId};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:NO onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchTimetableByClassTransIDResult"][@"ResponseCode"] intValue] == 1) {
                
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                    
//                    [[Timetable instance]storeTimeTableForStaff:SINGLETON_INSTANCE.staff withDict:responseDictionary];
//                    
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        
//                        [self setNormalTextColorsToAllButtons];
//                        
//                        [self setSelectedDateAsToday];
//                        
//                        [self categorizeTimeTable];
//                        
//                    });
//                });
            }
        }
    }];
}


@end