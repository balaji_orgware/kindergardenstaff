//
//  TSSettingsTableViewController.m
//  Trackidon Staff
//
//  Created by ORGware on 18/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSSettingsTableViewController.h"

@interface TSSettingsTableViewController ()
{
    NSMutableArray *settingsModelArray;
}
@end

@implementation SettingsModel

@end

@implementation TSSettingsTableViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.tableView.hidden = YES;
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];

    //[self.tableView setTableFooterView:[UIView new]];
    
    self.navigationItem.title = @"Settings";
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    [param setValue:[SINGLETON_INSTANCE staffTransId] forKey:@"guidParentTransID"];
    
    [self fetchSettingsByStaffTransIDWithURL:@"MastersMServices/NotificationSetttingService.svc/FetchNotificationSettingbyParentTransID" params:param];
    
    NSLog(@"%@",param);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UIImageView *iconImageView = (UIImageView *)[cell viewWithTag:200];
    
    UILabel *nameLbl = (UILabel *)[cell viewWithTag:201];
    
    UIButton *onOffBtn = (UIButton *)[cell viewWithTag:202];
    
    [onOffBtn addTarget:self action:@selector(onOffBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    SettingsModel *modelObj = [settingsModelArray objectAtIndex:0];
    
    switch ([indexPath row]) {
            
        case 0:
            
            [iconImageView setImage:[UIImage imageNamed:@"email"]];
            
            [nameLbl setText:@"Email notification"];
            
            if([modelObj.emailAlert boolValue]){
                
                [onOffBtn setSelected:YES];
                
                [onOffBtn setImage:[UIImage imageNamed:@"on"] forState:UIControlStateSelected];
                
            }else{
                
                [onOffBtn setSelected:NO];
                
                [onOffBtn setImage:[UIImage imageNamed:@"off"] forState:UIControlStateNormal];
                
                
            }
            
            break;
            
        case 1:
            
            [iconImageView setImage:[UIImage imageNamed:@"push"]];
            
            [nameLbl setText:@"Push notification"];
            
            if([modelObj.pushNotificationAlert boolValue]){
                
                [onOffBtn setSelected:YES];
                
                [onOffBtn setImage:[UIImage imageNamed:@"on"] forState:UIControlStateSelected];
                
            }else{
                
                [onOffBtn setSelected:NO];
                
                [onOffBtn setImage:[UIImage imageNamed:@"off"] forState:UIControlStateNormal];
                
                
            }
            
            break;
            
        default:
            break;
    }

    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    UIButton *onOffBtn = (UIButton *)[cell viewWithTag:202];
    
    SettingsModel *modelObj = [settingsModelArray objectAtIndex:0];
    
    switch ([indexPath row]) {
        case 0:
            
            [modelObj setEmailAlert:[modelObj.emailAlert boolValue]? @"0":@"1"];
            
            break;
            
        case 1:
            
            [modelObj setPushNotificationAlert:[modelObj.pushNotificationAlert boolValue]? @"0":@"1"];
            
            break;
            
        default:
            break;
    }
    
    [self buttonStateChanged:onOffBtn];
    
    [self updateBtnClick:nil];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - ButtonAction

-(void)onOffBtnAction:(id)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    
    NSIndexPath *clickedButtonIndexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    NSLog(@"%ld",(long)[clickedButtonIndexPath row]);
    
    SettingsModel *modelObj = [settingsModelArray objectAtIndex:0];
    
    switch ([clickedButtonIndexPath row]) {
        case 0:
            
            [modelObj setEmailAlert:[modelObj.emailAlert boolValue]? @"0":@"1"];
            
            break;
            
        case 1:
            
            [modelObj setPushNotificationAlert:[modelObj.pushNotificationAlert boolValue]? @"0":@"1"];
            
            break;
            
        default:
            break;
    }
    
    [self buttonStateChanged:sender];
    
    [self updateBtnClick:nil];
    
}

#pragma mark - Methods

-(void) buttonStateChanged:(UIButton *)button{
    
    if ([button isSelected]) {
        
        [button setSelected:NO];
        
        [button setImage:[UIImage imageNamed:@"off"] forState:UIControlStateNormal];
        
    }else{
        
        [button setSelected:YES];
        
        [button setImage:[UIImage imageNamed:@"on"] forState:UIControlStateSelected];
        
    }
    
}

-(void) updateBtnClick:(id)sender{
    
    NSLog(@"%@",sender);
    
    SettingsModel *modelObj = [settingsModelArray objectAtIndex:0];
    
    NSLog(@"%@",modelObj.pushNotificationAlert);
    
    NSLog(@"%@",modelObj.emailAlert);
    
    NSLog(@"%@",modelObj.smsAlert);
    
    NSLog(@"%@",modelObj.proximityAlert);
    
    NSMutableDictionary *param = [NSMutableDictionary new];
    
    [param setValue:modelObj.notificationTransID forKey:@"guidTransID"];
    
    [param setValue:[SINGLETON_INSTANCE staffTransId] forKey:@"guidParentTransID"];
    
    [param setValue:[modelObj.pushNotificationAlert  boolValue]?@"true" : @"false" forKey:@"bolPushNotificationAlert"];
    
    [param setValue:[modelObj.emailAlert  boolValue]?@"true" : @"false"  forKey:@"bolEmailAlert"];
    
    [param setValue:@"true" forKey:@"bolSMSAlert"];
    
    [param setValue:@"true" forKey:@"bolproximityAlert"];
    
    [param setValue:@"0" forKey:@"intproximityDisTance"];
    
    NSLog(@"%@",param);
    
    [self updateSettingsByStaffTransIDWithURL:@"MastersMServices/NotificationSetttingService.svc/UpdateFetchNotificationSetting" params:param];
    
    
}

#pragma mark - Service Call

-(void)updateSettingsByStaffTransIDWithURL:(NSString *)url params:(NSMutableDictionary *)param{
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:url baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            NSLog(@"%@",responseDictionary);
            
            if ([responseDictionary[@"UpdateFetchNotificationSettingResult"][@"ResponseCode"] boolValue]) {
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"UpdateFetchNotificationSettingResult"][@"ResponseMessage"]];
                
            }else{
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"UpdateFetchNotificationSettingResult"][@"ResponseMessage"]];
                
            }
        }
        else{
            NSLog(@"error");
            [TSSingleton requestTimeOutErrorAlert];

        }
    }];
    
}

-(void)fetchSettingsByStaffTransIDWithURL:(NSString *)url params:(NSMutableDictionary *)param{
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:url baseURL:[USERDEFAULTS objectForKey:BASE_URL]  param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
//            NSLog(@"%@",responseDictionary);
            
            if ([responseDictionary[@"FetchNotificationSettingbyParentTransIDResult"][@"ResponseCode"] boolValue]) {
                
                settingsModelArray = [[NSMutableArray alloc] init];
                
                if (responseDictionary[@"FetchNotificationSettingbyParentTransIDResult"][@"NotificationSettingsData"] != [NSNull null]) {
                    
                    SettingsModel *obj = [[SettingsModel alloc] init];
                    
                    [obj setPushNotificationAlert:responseDictionary[@"FetchNotificationSettingbyParentTransIDResult"][@"NotificationSettingsData"][@"PushNotificationAlert"]];
                    
                    [obj setProximityAlert:responseDictionary[@"FetchNotificationSettingbyParentTransIDResult"][@"NotificationSettingsData"][@"proximityAlert"]];
                    
                    [obj setSmsAlert:responseDictionary[@"FetchNotificationSettingbyParentTransIDResult"][@"NotificationSettingsData"][@"SMSAlert"]];
                    
                    [obj setEmailAlert:responseDictionary[@"FetchNotificationSettingbyParentTransIDResult"][@"NotificationSettingsData"][@"EmailAlert"]];
                    
                    [obj setNotificationTransID:responseDictionary[@"FetchNotificationSettingbyParentTransIDResult"][@"NotificationSettingsData"][@"TransID"]];
                    
                    [settingsModelArray addObject:obj];
                    
                    obj = nil;
                    
                }
                //_settingsTableView.hidden = NO;
                
                self.tableView.hidden = NO;
                
                [self.tableView reloadData];
                
                //[_settingsTableView reloadData];
                
            }else{
                
               // [_settingsTableView setHidden:YES];
                
                self.tableView.hidden = YES;
                
                
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"FetchNotificationSettingbyParentTransIDResult"][@"ResponseMessage"]];
            }
            
            
        }else{
//            NSLog(@"Error");
            [TSSingleton requestTimeOutErrorAlert];

        }
    }];
    
}

@end
