//
//  TSCreatePortionViewController.h
//  Trackidon Staff
//
//  Created by Elango on 07/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSCreatePortionViewController : UIViewController<SelectionCallBackDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollVw;
@property (weak, nonatomic) IBOutlet UITextField *titleTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *attachementTxtFld;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTxtVw;
@property (weak, nonatomic) IBOutlet UIView *receiverSelectionVw;
@property (weak, nonatomic) IBOutlet UITextField *subjectTxtFld;

@end
