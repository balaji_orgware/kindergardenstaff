//
//  TSTemperatureHistoryViewController.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 13/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PNChart.h"

typedef NS_ENUM(NSInteger,CURRENT_VIEW)
{
    DAY_VIEW = 1,
    WEEKLY_VIEW,
    MONTHLY_VIEW,
};

@interface TSTemperatureHistoryViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollVw;
@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;

@property (weak, nonatomic) IBOutlet UIView *contentVw;

@property (weak, nonatomic) IBOutlet UIView *studentNameVw;
@property (weak, nonatomic) IBOutlet UILabel *studentNameLbl;


@property (weak, nonatomic) IBOutlet UIButton *dayBtn;
@property (weak, nonatomic) IBOutlet UIButton *weeakBtn;
@property (weak, nonatomic) IBOutlet UIButton *monthBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

@property (weak, nonatomic) IBOutlet UIView *lineVw;
@property (weak, nonatomic) IBOutlet UIView *calanderVw;

@property (strong, nonatomic) JTCalendarManager *calendarManager;

@property (weak, nonatomic) IBOutlet PNBarChart *dayBarChart;
@property (weak, nonatomic) IBOutlet UIView *dayVw;
@property (weak, nonatomic) IBOutlet UIView *dayRecordVw;
@property (weak, nonatomic) IBOutlet UILabel *dayForenoonTempLbl;
@property (weak, nonatomic) IBOutlet UILabel *dayAfternoonTempLbl;
@property (weak, nonatomic) IBOutlet UIImageView *dayForenoonTempImgVw;
@property (weak, nonatomic) IBOutlet UIImageView *dayAfternoonTempImg;

@property (weak, nonatomic) IBOutlet UIView *weeakMnthVw;
@property (weak, nonatomic) IBOutlet UITableView *tblVw;

@property (weak, nonatomic) IBOutlet UIView *foorenoonLegendVw;
@property (weak, nonatomic) IBOutlet UIView *afternoonLgendVw;

@property (weak, nonatomic) IBOutlet PNLineChart *lineChart;

@property (nonatomic,strong) StudentModel *studentObj;

@end