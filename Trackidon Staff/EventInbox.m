//
//  EventInbox.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/4/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "EventInbox.h"
#import "StaffDetails.h"

@implementation EventInbox

+(instancetype)instance{
    
    static EventInbox *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (&onceToken, ^{
        
        kSharedInstance = [[EventInbox alloc]init];
        
    });
    
    return kSharedInstance;
}

-(void)saveEventInboxWithResponseDict:(NSDictionary *)dict{

    NSLog(@"%@",dict);

    @try {
        
        [SINGLETON_INSTANCE deleteAllObjectsInEntity:@"EventInbox"];
        
        NSManagedObjectContext *mainContext = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        
        [context setParentContext:mainContext];
                
        NSManagedObjectID *objectId = [SINGLETON_INSTANCE.staff objectID];
        
        StaffDetails *tmpStaff = [context objectWithID:objectId];
        
        
        for (NSDictionary *tempEventInbox in dict[@"FetchSchoolStaffNotificationsByStaffIDResult"][@"EventsMClass"]) {
            
            NSDictionary *eventDict = [tempEventInbox dictionaryByReplacingNullsWithBlanks];
            
            EventInbox *eventInbox ;//= [self checkAvailabilityWithTransId:eventDict[@"TransID"] context:context];
            
            //if (eventInbox == nil) {
                
                eventInbox = [NSEntityDescription insertNewObjectForEntityForName:@"EventInbox" inManagedObjectContext:context];
            //}
            
                eventInbox.eventTitle = eventDict[@"EventTitle"];
                eventInbox.eventVenue = eventDict[@"Venue"];
                eventInbox.eventTime = eventDict[@"EventTime"];
                eventInbox.userCreatedName = eventDict[@"UserCreatedName"];
                eventInbox.eventDescription = eventDict[@"Description"];
                eventInbox.fromDate = [SINGLETON_INSTANCE dateFromString:eventDict[@"FromDate"] withFormate:DATE_FORMATE_SERVICE_RESPONSE];
                eventInbox.toDate = [SINGLETON_INSTANCE dateFromString:eventDict[@"ToDate"] withFormate:DATE_FORMATE_SERVICE_RESPONSE];
                eventInbox.isHasAttachment = [NSNumber numberWithInteger:[eventDict[@"IsAttachment"]integerValue]];
            eventInbox.transID = eventDict[@"TransID"];
            eventInbox.staffTransID = SINGLETON_INSTANCE.staffTransId;
            eventInbox.eventAttachment = eventDict[@"Attachment"];
            eventInbox.modifiedDate = [SINGLETON_INSTANCE dateFromString:eventDict[@"ModifiedDate"] withFormate:DATE_FORMATE_MODIFIED_DATE];
            eventInbox.staff = tmpStaff;
            
            NSString *assigneeList = [[TSCommonAssignedDetails instance] getAssignedDetailsWithArray:eventDict[@"SelectEventsMDetails"] withGroupType:eventDict[@"GroupType"]];
            
            eventInbox.assignedList = assigneeList;
            
            //[[AssignedDetails instance] saveAssignedDetailsWithDict:eventDict[@"SelectEventsMDetails"] withGroupType:eventDict[@"GroupType"] transId:eventInbox.transID forObject:eventInbox objectType:OBJECT_TYPE_EVENTINBOX context:context];

        }
        
        NSError *error;
        
        if (![context save:&error]) {
            
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        
        [SINGLETON_INSTANCE storeContextInMainThread];

    } @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
    }

}

-(EventInbox *)checkAvailabilityWithTransId:(NSString *)transId context:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"EventInbox" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transID contains[cd] %@", transId];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }

}

-(void) savePushNotificationEventWithResponseDict:(NSDictionary *)dictionary{
    
    @try {
        
        NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSError *error = nil;
        
        NSDictionary *eventDict = [dictionary[@"FetchEventByTransIDResult"][@"EventsMClass"]dictionaryByReplacingNullsWithBlanks];
        
        EventInbox *eventInbox = [self checkAvailabilityWithTransId:eventDict[@"TransID"] context:context];
        
        if (eventInbox == nil) {
            
            eventInbox = [NSEntityDescription insertNewObjectForEntityForName:@"EventInbox" inManagedObjectContext:context];
        }
        
        eventInbox.eventTitle = eventDict[@"EventTitle"];
        eventInbox.eventVenue = eventDict[@"Venue"];
        eventInbox.eventTime = eventDict[@"EventTime"];
        eventInbox.userCreatedName = eventDict[@"UserCreatedName"];
        eventInbox.eventDescription = eventDict[@"Description"];
        eventInbox.fromDate = [SINGLETON_INSTANCE dateFromString:eventDict[@"FromDate"] withFormate:DATE_FORMATE_SERVICE_RESPONSE];
        eventInbox.toDate = [SINGLETON_INSTANCE dateFromString:eventDict[@"ToDate"] withFormate:DATE_FORMATE_SERVICE_RESPONSE];
        eventInbox.isHasAttachment = [NSNumber numberWithInteger:[eventDict[@"IsAttachment"]integerValue]];
        eventInbox.transID = eventDict[@"TransID"];
        eventInbox.staffTransID = SINGLETON_INSTANCE.staffTransId;
        eventInbox.eventAttachment = eventDict[@"Attachment"];
        eventInbox.modifiedDate = [SINGLETON_INSTANCE dateFromString:eventDict[@"ModifiedDate"] withFormate:DATE_FORMATE_MODIFIED_DATE];
        eventInbox.staff = SINGLETON_INSTANCE.staff;
        
        NSString *assigneeList = [[TSCommonAssignedDetails instance] getAssignedDetailsWithArray:eventDict[@"SelectEventsMDetails"] withGroupType:eventDict[@"GroupType"]];
        
        eventInbox.assignedList = assigneeList;
        
        if (![context save:&error]) {
            
            NSLog(@"Whoops Couldnt save records.");
        }
        
    } @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    } @finally {
        
        
    }
}
@end
