//
//  TSAttendanceTableViewController.h
//  Trackidon Staff
//
//  Created by ORGware on 25/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSAttendanceTableViewController : UITableViewController<SelectionCallBackDelegate>

@end
