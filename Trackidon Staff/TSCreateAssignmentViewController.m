//
//  TSCreateAssignmentViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 07/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSCreateAssignmentViewController.h"

@interface TSCreateAssignmentViewController (){
    
    CGRect viewRect, currentVisibleRect;
    
    NSArray *subjectsArray,*subjectTypeArray;
    
    NSDate *dueDate;
    
    NSString *selectedSubjectTransID, *selectedSubjectTypeTransID, *attachmentEncodedString;
    
    UIView *currentResponder;
    
    TSSelectionViewController *selectionVC;
}

@end

@implementation TSCreateAssignmentViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];

    {
        for (id obj in _scrollView.subviews) {
            
            if ([obj isKindOfClass:[UITextField class]]) {
                
                UITextField *textField = (UITextField *)obj;
                
                [TSSingleton layerDrawForView:textField position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
                
                [SINGLETON_INSTANCE addLeftInsetToTextField:textField];
            }
        }
        
        [TSSingleton layerDrawForView:_addAttachmentTxtFld position:LAYER_TOP color:[UIColor lightGrayColor]];
        
        [SINGLETON_INSTANCE addDoneButtonForTextView:_descriptionTxtVw];
        
        [SINGLETON_INSTANCE addDropDownArrowToTextField:_subjectNameTxtFld];
        [SINGLETON_INSTANCE addDropDownArrowToTextField:_subjectTypeTxtFld];
        [SINGLETON_INSTANCE addRightViewToTextField:_dueDateTxtFld withImageName:CALENDAR_IMGNAME];
        [SINGLETON_INSTANCE addRightViewToTextField:_addAttachmentTxtFld withImageName:ATTACHMENTPIN_IMGNAME];
        
        _descriptionTxtVw.text = DESCRIPTION_TEXT;
        _descriptionTxtVw.textColor = APP_PLACEHOLDER_COLOR;
        _descriptionTxtVw.font = [UIFont fontWithName:APP_FONT size:16];
        _descriptionTxtVw.textContainerInset =
        UIEdgeInsetsMake(15,5,10,15); // top, left, bottom, right
        
        viewRect = self.view.frame;
        
    }
    
    [_scrollView setContentSize:CGSizeMake(DeviceWidth,_addAttachmentTxtFld.frame.origin.y+_addAttachmentTxtFld.frame.size.height)];
    
    
    NSPredicate *subjPred = [NSPredicate predicateWithFormat:@"typeID == %d",TypeID_Subject];
    
    subjectsArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"CommonTypeDetails" withPredicate:subjPred];
    
    NSPredicate *subjTypePred = [NSPredicate predicateWithFormat:@"typeID == %d",TypeID_AssignmentType];
    
    subjectTypeArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"CommonTypeDetails" withPredicate:subjTypePred];
}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_ASSIGNMENTCREATE parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Create Assignment";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

#pragma mark - Button Actions

- (IBAction)assignedToButtonTapped:(id)sender {
    
    if (_subjectNameTxtFld.text.length == 0) {
        
        [TSSingleton showAlertWithMessage:@"Please select subject"];
        
        return;
    }
    else if (_assignmentTitleTxtFld.text.length ==0){
        
        [TSSingleton showAlertWithMessage:@"Assignment Title cannot be empty!!!"];
        
        return;
    }
    else if (_assignmentTitleTxtFld.text.length> MAX_TITLE_LENGTH){
        
        //[TSSingleton showAlertWithMessage:@"Assignment Title is too long. Choose Title shorter than this"];
        [TSSingleton showAlertWithMessage:TEXT_TITLE_LENGTH_EXCEED];
        
        return;
    }
    else if (_subjectTypeTxtFld.text.length == 0){
        
        [TSSingleton showAlertWithMessage:@"Please select subject type"];
        
        return;
    }
    else if (_dueDateTxtFld.text.length == 0){
        
        [TSSingleton showAlertWithMessage:@"Please Select due date"];
        
        return;
    }
    else if (_descriptionTxtVw.text.length == 0 || [_descriptionTxtVw.text isEqualToString:@"Description"]){
        
        [TSSingleton showAlertWithMessage:@"Please enter Assignment description"];
        
        return;
    }
    
    NSMutableDictionary *param = [NSMutableDictionary new];
    NSMutableDictionary *analyticsDict = [NSMutableDictionary new];

    
    [param setValue:selectedSubjectTransID forKey:@"SubjectTransID"];
    [param setValue:selectedSubjectTypeTransID forKey:@"AssignmentType"];
    [param setValue:[SINGLETON_INSTANCE stringFromDate:[NSDate date] withFormate:DATE_FORMATE_SERVICE_CALL] forKey:@"AssignmentDate"];
    [param setValue:[SINGLETON_INSTANCE stringFromDate:dueDate withFormate:DATE_FORMATE_SERVICE_CALL] forKey:@"DueDate"];
    [param setValue:_assignmentTitleTxtFld.text forKey:@"AssignmentTitle"];
    [param setValue:_descriptionTxtVw.text forKey:@"AssignmentDescription"];
    
    if (_addAttachmentTxtFld.text.length >0 ) {
        
        [analyticsDict setValue:ANALYTICS_VALUE_YES forKey:ANALYTICS_PARAM_ISHASATTACHMENT];

        [param setValue:@"true" forKey:@"IsAttachment"];
        
        [param setValue:[@"data:image/jpeg;base64,"stringByAppendingString:[attachmentEncodedString stringByReplacingOccurrencesOfString:@"\n" withString:@""]] forKey:@"Attachment"];
    }
    else{
        
        [analyticsDict setValue:ANALYTICS_VALUE_NO forKey:ANALYTICS_PARAM_ISHASATTACHMENT];
        
        [param setValue:@"false" forKey:@"IsAttachment"];
    }
    
    [analyticsDict setValue:_subjectNameTxtFld.text forKey:ANALYTICS_PARAM_SUBJECT_NAME];
    
    [analyticsDict setValue:_subjectTypeTxtFld.text forKey:ANALYTICS_PARAM_SUBJECT_TYPE];
    
    [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_ASSIGN_RECEIVER parameter:analyticsDict];
    
    
    TSReceiverSelectionViewController *receiverSelectionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBReceiverSelection"];
    
    receiverSelectionVC.menuId = SINGLETON_INSTANCE.currentMenuId;
    
    receiverSelectionVC.commonDict = param;
    
    receiverSelectionVC.communicationType = OBJECT_TYPE_ASSIGNMENT;
    
    [self.navigationController pushViewController:receiverSelectionVC animated:YES];
}

#pragma mark - Date Picker Delegate Method

-(void)selectedDate:(NSDate *)date{
    
    dueDate = date;
    
    _dueDateTxtFld.text = [SINGLETON_INSTANCE stringFromDate:date withFormate:DATE_FORMATE_DAY_MNTH_YR];
}

#pragma mark - Touches Began Methods

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
}

#pragma mark - Selection View Methods

-(void) showSelectionViewWithSelectedText:(NSString *)selected andTextField:(UITextField *)textField{
    
    if (selectionVC == nil) {
        
        selectionVC = [[TSSelectionViewController alloc]initWithNibName:@"TSSelectionViewController" bundle:nil];
        
        selectionVC.delegate = self;
        
        NSMutableDictionary *contentDict = [NSMutableDictionary new];
        
        if (textField == _subjectNameTxtFld) {
            
            for (CommonTypeDetails *obj in subjectsArray) {
                
                [contentDict setValue:obj.transID forKey:obj.name];
            }
            
        }
        else if (textField == _subjectTypeTxtFld){
            
            for (CommonTypeDetails *obj in subjectTypeArray) {
                
                [contentDict setValue:obj.transID forKey:obj.name];
            }
        }
        
        if (contentDict.allKeys.count>0) {
            
            [selectionVC setContentDict:contentDict];
            
            [selectionVC setSelectedValue:selected.length>0?selected:@""];
            
            [selectionVC showSelectionViewController:self forTextField:textField];
        }
        else{
            
            selectionVC = nil;
        }
        
    }
    else{
        
        selectionVC = nil;
    }
}

// MARK: Selection Callback Delegate

-(void)selectedValueFromDict:(NSDictionary *)contentDict withSelectedValue:(NSString *)selectedValue andTransID:(NSString *)transID forTextField:(UITextField *)textField{
    
    if (textField == _subjectNameTxtFld) {
        
        selectedSubjectTransID = transID;
    }
    else if (textField == _subjectTypeTxtFld){
        
        selectedSubjectTypeTransID = transID;
    }
    
    selectionVC = nil;
}

-(void)backViewTappedOn:(UIViewController *)viewController{
    
    selectionVC = nil;
}

#pragma mark - Attachment Delegate

- (void) selectedAttachmentImageData:(NSData *)imageData imageName:(NSString *)imageName isHasAttachment:(BOOL)isHasAttachment
{
    if (isHasAttachment) {
        
        NSLog(@"%@",imageData);
        
        attachmentEncodedString =  [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        
        NSLog(@"%@",attachmentEncodedString);
    }
}

- (void)selectedAttachmentImageName:(NSString *)imageName{
    
    if (imageName != nil) {
        
        _addAttachmentTxtFld.text = imageName;
    }else {
        _addAttachmentTxtFld.text = @"Photo";
    }
}

#pragma mark - TextField Delegates

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{

    currentResponder = textField;
    
    if (textField == _assignmentTitleTxtFld) {
        
        return YES;
    }
    else
    {
        if (textField == _subjectNameTxtFld || textField == _subjectTypeTxtFld) {
            
            [self showSelectionViewWithSelectedText:textField.text andTextField:textField];
        }
        else if (textField == _dueDateTxtFld){
            
            TSDatePickerViewController *datePickerVC = [[TSDatePickerViewController alloc]initWithNibName:@"TSDatePickerViewController" bundle:nil];
            
            [datePickerVC showInViewController:self calendarType:CALENDAR_TYPE_DATE withMinDate:[NSDate date] withMaxDate:nil];
            
            datePickerVC.delegate = self;
        }
        else if (textField == _addAttachmentTxtFld){
            
            [TSAttachmentPicker instance].delegate = self;
            
            [[TSAttachmentPicker instance] showAttachmentPickerForView:self frame:textField.frame];
        }
        
//        textField.text = @"";
        
        return NO;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - TextView Delegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    currentResponder = textView;
    
    if ([textView.text isEqualToString:DESCRIPTION_TEXT]) {
        
        textView.text = @"";
        textView.textColor = APP_BLACK_COLOR;
        textView.font = [UIFont fontWithName:APP_FONT size:14];
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
        textView.text = DESCRIPTION_TEXT;
        textView.textColor = APP_PLACEHOLDER_COLOR;
        textView.font = [UIFont fontWithName:APP_FONT size:17];
    }
    
    CGFloat fixedWidth = _descriptionTxtVw.frame.size.width;
    CGSize newSize = [_descriptionTxtVw sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = _descriptionTxtVw.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    
    if (newFrame.size.height > 50) {
        _descriptionTxtVw.frame = newFrame;
    }
    else {
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), 50);
        _descriptionTxtVw.frame = newFrame;
    }
    
    [self reArrangeControls];
    
    //ScrollView Fix
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_scrollView setContentSize:CGSizeMake(DeviceWidth, _addAttachmentTxtFld.frame.origin.y+_addAttachmentTxtFld.frame.size.height)];
    });
    
}

- (void)textViewDidChange:(UITextView *)textView{
    
    [self reArrangeControls];
}

- (void)reArrangeControls{
    
    _addAttachmentTxtFld.frame = CGRectMake(_addAttachmentTxtFld.frame.origin.x,_descriptionTxtVw.frame.origin.y + _descriptionTxtVw.frame.size.height, _addAttachmentTxtFld.frame.size.width, _addAttachmentTxtFld.frame.size.height);
    
    [TSSingleton layerDrawForView:_addAttachmentTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:_addAttachmentTxtFld position:LAYER_TOP color:[UIColor lightGrayColor]];
}

#pragma mark - Keyboard Hide

- (void)keyboardWasShown:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    
    CGSize currentKeyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    CGPoint textFieldOrigin = currentResponder.frame.origin;
    textFieldOrigin.y = textFieldOrigin.y + currentResponder.frame.size.height;
    CGFloat textFieldHeight = currentResponder.frame.size.height;
    
    CGRect visibleRect = viewRect;
    visibleRect.size.height -= currentKeyboardSize.height + textFieldHeight;
    currentVisibleRect = visibleRect;
    
    if ([_descriptionTxtVw isFirstResponder]) {
        
        CGRect descRect = viewRect;
        
        descRect.size.height -= currentKeyboardSize.height;
        
        _descriptionTxtVw.frame = CGRectMake(_descriptionTxtVw.frame.origin.x, _descriptionTxtVw.frame.origin.y, _descriptionTxtVw.frame.size.width, descRect.size.height - 64 );
        
        
        [self reArrangeControls];
        
        CGPoint scrollPoint = CGPointMake(0.0, _descriptionTxtVw.frame.origin.y);
        
        [_scrollView setContentOffset:scrollPoint animated:YES];
        
        [_scrollView setScrollEnabled:NO];
        
    }
    else if (!CGRectContainsPoint(visibleRect, textFieldOrigin))
    {
        
        CGPoint scrollPoint = CGPointMake(0.0, textFieldOrigin.y - visibleRect.size.height  + textFieldHeight);
        
        [_scrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)notification
{
    [_scrollView setScrollEnabled:YES];
    
    [_scrollView setContentOffset:CGPointZero animated:YES];
}

@end