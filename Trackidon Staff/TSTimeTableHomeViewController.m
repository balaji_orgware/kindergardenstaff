//
//  TSTimeTableHomeViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 15/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSTimeTableHomeViewController.h"

@interface TSTimeTableHomeViewController ()

@end

@implementation TSTimeTableHomeViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [TSAnalytics logEvent:ANALYTICS_TIMETABLE_TIMETABLEOPENED parameter:[NSMutableDictionary new]];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_TIMETABLEHOME parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Timetable";
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sCellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier];
    
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:101];
    
    UIImageView *iconImageView = (UIImageView *)[cell viewWithTag:100];
    
    switch (indexPath.row) {
        
        case 0:
            
            nameLabel.text = @"My Timetable";
            iconImageView.image = [UIImage imageNamed:@"my_timetable"];
            break;
        
        case 1:
            
            nameLabel.text = @"My Class Timetable";
            iconImageView.image = [UIImage imageNamed:@"class_timetable"];
            break;
            
        default:
            break;
    }
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
            
        case 0:
        {
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            [analyticsDict setValue:ANALYTICS_VALUE_MYTIMETABLE forKey:ANALYTICS_PARAM_TYPE];
            
            [TSAnalytics logEvent:ANALYTICS_TIMETABLE_CLICKED_TIMETABLETYPE parameter:analyticsDict];
            
            TSMyTimetableViewController *timetableVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBMyTimetable"];
            
            [self.navigationController pushViewController:timetableVC animated:YES];

            break;
        }
            
        case 1:
        {
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            [analyticsDict setValue:ANALYTICS_VALUE_MYCLASSTIMETABLE forKey:ANALYTICS_PARAM_TYPE];
            
            [TSAnalytics logEvent:ANALYTICS_TIMETABLE_CLICKED_TIMETABLETYPE parameter:analyticsDict];
            
            StaffDetails *staff = [SINGLETON_INSTANCE staff];
            
            if (![staff.inChargeClassTransId isEqualToString:@"00000000-0000-0000-0000-000000000000"] && ![staff.inChargeSectionTransId isEqualToString:@"00000000-0000-0000-0000-000000000000"]) {
                
                TSMyClassTimetableViewController *myClassTimeTableVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBMyClassTimeTable"];
                
                [self.navigationController pushViewController:myClassTimeTableVC animated:YES];
            }
            else{
                
                [TSSingleton showAlertWithMessage:@"You don't have any assigned class"];
                
//                TSMyClassTimetableViewController *myClassTimeTableVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBMyClassTimeTable"];
//                
//                [self.navigationController pushViewController:myClassTimeTableVC animated:YES];
            }
            
            break;
        }
            
        default:
            break;
    }
}

@end
