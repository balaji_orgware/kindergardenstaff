//
//  TSAssignmentListTableViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 21/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSAssignmentListTableViewController : UITableViewController

@property (nonatomic,strong) NSArray *assignmentsArray;

@property (nonatomic, assign) int listType;
@property (nonatomic,retain) NSFetchedResultsController *fetchedResultsController;

@end
