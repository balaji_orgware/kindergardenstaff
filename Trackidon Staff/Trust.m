//
//  Trust.m
//  Trackidon Staff
//
//  Created by Elango on 16/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "Trust.h"

@implementation Trust

+ (instancetype)instance{
    
    static Trust *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (&onceToken, ^{
        
        kSharedInstance = [[Trust alloc]init];
        
    });
    
    return kSharedInstance;
}

- (void)saveTrustWithResponseDict:(NSDictionary *)dict{
    
    NSLog(@"%@",dict);
    
    @try {
        
        [SINGLETON_INSTANCE deleteAllObjectsInEntity:@"Trust"];
        
        NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSDictionary *schoolDict = dict[@"FetchSchoolsBySchoolCodeResult"][@"School"];
        
        schoolDict = [schoolDict dictionaryByReplacingNullsWithBlanks];
                
        Trust *trust;
        
        trust = [self checkAvailablityWithTrustTransId:schoolDict[@"TrustTransID"] context:context];
        
        if (trust == nil) {
            
            trust=[NSEntityDescription insertNewObjectForEntityForName:@"Trust" inManagedObjectContext:context];
        }
        
        trust.addressOne = schoolDict[@"Address1"];
        trust.addressTwo = schoolDict[@"Address2"];
        trust.addressThree = schoolDict[@"Address3"];
        trust.contactMail = schoolDict[@"ContactMail"];
        trust.contactPerson = schoolDict[@"ContactPerson"];
        trust.contactPhone = schoolDict[@"ContactPhone"];
        trust.contactFaxNo = schoolDict[@"FaxNo"];
        trust.latitude = schoolDict[@"Latitude"];
        trust.longtitude = schoolDict[@"Longtitude"];
        trust.baseUrl = schoolDict[@"MobileBaseUrl"];
        trust.pincode = schoolDict[@"Pincode"];
        trust.schoolCode = schoolDict[@"SchoolCode"];
        trust.schoolLogoUrl = schoolDict[@"SchoolLogo"];//Trust Logo
        trust.schoolName = schoolDict[@"SchoolName"];
        trust.schoolTransId = schoolDict[@"TransId"];
        trust.startDate = schoolDict[@"StartDate"];
        trust.trustName = schoolDict[@"TrustName"];
        trust.trustTransId = schoolDict[@"TrustTransID"];
        
        if (trust.schoolLogoUrl.length > 0 ) {
            
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:trust.schoolLogoUrl]];
            
            UIImage *image=[UIImage imageWithData:imageData];
            
            if (image != nil)
                trust.schoolLogoData = imageData;
            
        }
        
        
        NSError *error;
        
        if (![context save:&error]) {
            
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            
        }
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    }
    
}

-(Trust *)checkAvailablityWithTrustTransId:(NSString *)trustTransId context:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Trust" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"trustTransId contains[cd] %@", trustTransId];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
    
}

@end
