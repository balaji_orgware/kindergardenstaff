//
//  TSAssignmentsViewController.h
//  Trackidon Staff
//
//  Created by ORGware on 23/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TSAssignmentsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

// Declarations

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *createAssignmentVw;


//Button Actions

- (IBAction)createAssignmentsBtnAction:(id)sender;

@end
