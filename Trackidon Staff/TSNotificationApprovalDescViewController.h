//
//  TSNotificationApprovalDescViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 06/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Notifications;

@interface TSNotificationApprovalDescViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topView;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@property (weak, nonatomic) IBOutlet UILabel *boardLbl;

@property (weak, nonatomic) IBOutlet UIButton *attachmentButton;

@property (weak, nonatomic) IBOutlet UILabel *createdByLbl;

@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (weak, nonatomic) IBOutlet UIView *dateView;

@property (weak, nonatomic) IBOutlet UILabel *notificationPriorityTextLbl;

@property (weak, nonatomic) IBOutlet UIView *notificationPriorityView;

@property (weak, nonatomic) IBOutlet UILabel *fromDateLbl;
@property (weak, nonatomic) IBOutlet UILabel *createdOnTextLbl;

@property (weak, nonatomic) IBOutlet UIView *assignedToView;
@property (weak, nonatomic) IBOutlet UITextField *assignedToTxtFld;

@property (weak, nonatomic) Notifications *notification;

@end
