//
//  TSDescriptionViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 5/20/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSDescriptionViewController.h"

@interface TSDescriptionViewController ()

@end

@implementation TSDescriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.65f]];
    
    [UIView animateWithDuration:0.3f animations:^{
        
        [self.view setBackgroundColor:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f]];
        
        _descTxtView.alpha = 0.0;
        
        _descView.alpha = 0.0f;
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        
        [self removeFromParentViewController];
        
    }];
    
}


- (void)showInViewController:(UIViewController *)viewController descriptionString:(NSString *)text{
    
    NSLog(@"%@",text);
    
    [viewController addChildViewController:self];
    
    [viewController.view addSubview:self.view];
    
    self.view.frame = CGRectMake(0, 0, DEVICE_SIZE.width, DEVICE_SIZE.height);
    
    [self didMoveToParentViewController:viewController];
    
    NSEnumerator *frontToBackWindows = [UIApplication.sharedApplication.windows reverseObjectEnumerator];
    
    for (UIWindow *window in frontToBackWindows){
        
        BOOL windowOnMainScreen = window.screen == UIScreen.mainScreen;
        
        BOOL windowIsVisible = !window.hidden && window.alpha > 0;
        
        BOOL windowLevelNormal = window.windowLevel == UIWindowLevelNormal;
        
        if (windowOnMainScreen && windowIsVisible && windowLevelNormal) {
            
            [window addSubview:self.view];
            
            break;
        }
    }
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f]];
    
    _descTxtView.alpha = 0.0;
    
    _descView.alpha = 0.0f;
    
    [UIView animateWithDuration:0.3f animations:^{
        
        [self.view setBackgroundColor:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.65f]];
        
        _descTxtView.alpha = 1.0;
        
        _descView.alpha = 1.0f;
        
    } completion:^(BOOL finished) {
        
        
    }];
    
    [self arrangeViewsAndDescString:text];
    
}

-(void) arrangeViewsAndDescString:(NSString *)string{
    
    _descTxtView.text = string;
    
    [_descTxtView setFont:[UIFont fontWithName:APP_FONT size:17.0f]];
    
    if(IS_IPAD)
        [_descTxtView setFont:[UIFont fontWithName:APP_FONT size:22.0f]];
    
    _descView.frame = CGRectMake(self.view.frame.origin.x + 20, self.view.frame.origin.y + 110, self.view.frame.size.width - 40, self.view.frame.size.height - 220);
    
    _descTxtView.frame = CGRectMake(5, 5, _descView.frame.size.width - 10, _descView.frame.size.height - 10);
    
    _descView.layer.cornerRadius = 4.0f;
    
}

@end
