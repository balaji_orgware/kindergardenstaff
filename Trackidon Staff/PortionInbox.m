//
//  PortionInbox.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/21/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "PortionInbox.h"
#import "StaffDetails.h"

@implementation PortionInbox

+(instancetype)instance{
    
    static PortionInbox *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (&onceToken, ^{
        
        kSharedInstance = [[PortionInbox alloc]init];
        
    });
    
    return kSharedInstance;
}

-(void)saveProtionInboxWithResponseDict:(NSDictionary *)dict{
    
    NSLog(@"%@",dict);
    
    @try {
        
        [SINGLETON_INSTANCE deleteAllObjectsInEntity:@"PortionInbox"];
        
        NSManagedObjectContext *mainContext = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        
        [context setParentContext:mainContext];
                
        NSManagedObjectID *objectId = [SINGLETON_INSTANCE.staff objectID];
        
        StaffDetails *tmpStaff = [context objectWithID:objectId];
        
        for (NSDictionary *tempPortionInbox in dict[@"FetchSchoolStaffNotificationsByStaffIDResult"][@"PortionsClass"]) {
            
            NSDictionary *portionDict = [tempPortionInbox dictionaryByReplacingNullsWithBlanks];
            
            //PortionInbox *portionInbox = [self checkAvailabilityWithTransId:portionDict[@"TransID"] context:context];
            
            //if (portionInbox == nil) {
                
                PortionInbox *portionInbox = [NSEntityDescription insertNewObjectForEntityForName:@"PortionInbox" inManagedObjectContext:context];
            //}
            
            portionInbox.portionsTitle = portionDict[@"PortionsTitle"];
            portionInbox.portionsDate = [SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:portionDict[@"PortionsDate"]];
            portionInbox.portionsDescription = portionDict[@"PortionsDescription"];
            portionInbox.approvedByName = portionDict[@"ApprovedByName"];
            portionInbox.isHasAttachment = portionDict[@"IsAttachment"];
            portionInbox.portionsAttachment = portionDict[@"Attachment"];
            portionInbox.subjectName = portionDict[@"SubjectName"];
            portionInbox.transID = portionDict[@"TransID"];
            portionInbox.staffTransID = SINGLETON_INSTANCE.staffTransId;
            portionInbox.modifiedDate = [SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:portionDict[@"ModifiedDate"]];
            portionInbox.createdBy = portionDict[@"CreatedBy"];
            
            NSString *assigneeList = [[TSCommonAssignedDetails instance] getAssignedDetailsWithArray:portionDict[@"PortionsDetails"] withGroupType:portionDict[@"GroupType"]];
            
            portionInbox.assignedList = assigneeList;
            
            //[[AssignedDetails instance]saveAssignedDetailsWithDict:portionDict[@"PortionsDetails"] withGroupType:portionDict[@"GroupType"] transId:portionInbox.transID forObject:portionInbox objectType:OBJECT_TYPE_PORTIONINBOX context:context];
            
            portionInbox.staff = tmpStaff;
            
        }
        
        NSError *error;
        
        if (![context save:&error]) {
            
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        
        [SINGLETON_INSTANCE storeContextInMainThread];
        
        
    } @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
    } 
    
}

-(PortionInbox *)checkAvailabilityWithTransId:(NSString *)transId context:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PortionInbox" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transID contains[cd] %@", transId];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
    
}

- (void)savePushNotificationPortionInboxWithDict:(NSDictionary *)dictionary{
    
    @try {
        
        NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSError *error = nil;
        
        NSDictionary *portionDict = [dictionary[@"FetchPortionsByTransIDResult"][@"PortionsClass"] dictionaryByReplacingNullsWithBlanks];
        
        PortionInbox *portionInbox = [self checkAvailabilityWithTransId:portionDict[@"TransID"] context:context];
        
        if (portionInbox == nil) {
            
            portionInbox = [NSEntityDescription insertNewObjectForEntityForName:@"PortionInbox" inManagedObjectContext:context];
        }
        
        portionInbox.portionsTitle = portionDict[@"PortionsTitle"];
        portionInbox.portionsDate = [SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:portionDict[@"PortionsDate"]];
        portionInbox.portionsDescription = portionDict[@"PortionsDescription"];
        portionInbox.approvedByName = portionDict[@"ApprovedByName"];
        portionInbox.isHasAttachment = portionDict[@"IsAttachment"];
        portionInbox.portionsAttachment = portionDict[@"Attachment"];
        portionInbox.subjectName = portionDict[@"SubjectName"];
        portionInbox.transID = portionDict[@"TransID"];
        portionInbox.staffTransID = SINGLETON_INSTANCE.staffTransId;
        portionInbox.modifiedDate = [SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:portionDict[@"ModifiedDate"]];
        portionInbox.createdBy = portionDict[@"CreatedBy"];
        
        NSString *assigneeList = [[TSCommonAssignedDetails instance] getAssignedDetailsWithArray:portionDict[@"PortionsDetails"] withGroupType:portionDict[@"GroupType"]];
        
        portionInbox.assignedList = assigneeList;
        
        //[[AssignedDetails instance]saveAssignedDetailsWithDict:portionDict[@"PortionsDetails"] withGroupType:portionDict[@"GroupType"] transId:portionInbox.transID forObject:portionInbox objectType:OBJECT_TYPE_PORTIONINBOX context:context];
        
        portionInbox.staff = SINGLETON_INSTANCE.staff;
        
        if (![context save:&error]) {
            
            NSLog(@"Whoops could not save");
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}
@end
