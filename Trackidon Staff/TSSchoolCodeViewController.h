//
//  TPSchoolCodeViewController.h
//  Trackidon
//
//  Created by ORGMacMini2 on 3/10/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSSchoolCodeViewController.h"

@interface TSSchoolCodeViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *schoolCodeTxtFld;

@property (weak, nonatomic) IBOutlet UIButton *goBtn;
@property (weak, nonatomic) IBOutlet UIView *inputFieldsVw;
@property (weak, nonatomic) IBOutlet UIView *logoVw;

@property (weak, nonatomic) IBOutlet UIImageView *redView;

@end
