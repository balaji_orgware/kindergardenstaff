//
//  TSAssignmentListTableViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 21/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSAssignmentListTableViewController.h"

@interface TSAssignmentListTableViewController (){

    NSArray *attachmentImgArray;
}
@end

@implementation TSAssignmentListTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    attachmentImgArray = @[@"one",@"two",@"three",@"four",@"five"];
    
    [self.tableView setTableFooterView:[UIView new]];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_ASSIGNMENTLIST parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@ Log",SINGLETON_INSTANCE.barTitle];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.assignmentsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
    
    UILabel *approvedLabel = (UILabel *)[cell viewWithTag:101];
    
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:103];
    
    UIImageView *attachmentImgVw = (UIImageView *)[cell viewWithTag:102];
    
    Assignments *assignmentObj = [self.assignmentsArray objectAtIndex:indexPath.row];
    
    titleLabel.text = [assignmentObj.assignmentTitle capitalizedString];
    
    dateLabel.text = [SINGLETON_INSTANCE stringFromDate:assignmentObj.assignmentDate withFormate:DATE_FORMATE_MNTH_DAY_YR];
    
    if ([assignmentObj.approvedStatus intValue] == StatusTypeApproved) {
        
        approvedLabel.textColor = COLOR_ACCEPT_CREATEDTODAY;
        
        approvedLabel.text = [NSString stringWithFormat:@"Approved by %@",[assignmentObj.approvedByName uppercaseString]];
    }
    else if ([assignmentObj.approvedStatus intValue] == StatusTypeDeclined){
        
        approvedLabel.textColor = COLOR_DECLINED_DUESTODAY;
        
        approvedLabel.text = [NSString stringWithFormat:@"Declined by %@",[assignmentObj.approvedByName uppercaseString]];
    }
    else{
        
        approvedLabel.textColor = COLOR_PENDING_VIEWALL;
        
        approvedLabel.text = @"Pending";
    }
    
    if ([assignmentObj.isAttachment intValue]==1) {
             
        attachmentImgVw.hidden = NO;
        
        attachmentImgVw.image = [UIImage imageNamed:[attachmentImgArray objectAtIndex:indexPath.row % 5]];
    }
    else{
        
        attachmentImgVw.hidden = YES;
    }
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_ASSIGNMENT_IN_LIST parameter:[NSMutableDictionary new]];
    
    //[TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_ASSIGNMENT_IN_LIST parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_VIEWALL,ANALYTICS_PARAM_LISTTYPE, nil]];

    
    TSAssignmentDescriptionViewController *AssignmentDesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAssignmentDescription"];
    
    Assignments *assignmentObj = [self.assignmentsArray objectAtIndex:indexPath.row];

    AssignmentDesVC.assignmentObj = assignmentObj;

    [self.navigationController pushViewController:AssignmentDesVC animated:YES];
    
}

@end