//
//  Classes+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 04/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Classes.h"

NS_ASSUME_NONNULL_BEGIN

@interface Classes (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *boardTransId;
@property (nullable, nonatomic, retain) NSString *classStandardName;
@property (nullable, nonatomic, retain) NSString *classTransId;
@property (nullable, nonatomic, retain) NSString *schoolName;
@property (nullable, nonatomic, retain) NSString *schoolTransId;
@property (nullable, nonatomic, retain) NSString *boardName;
@property (nullable, nonatomic, retain) Board *board;
@property (nullable, nonatomic, retain) NSSet<Section *> *section;

@end

@interface Classes (CoreDataGeneratedAccessors)

- (void)addSectionObject:(Section *)value;
- (void)removeSectionObject:(Section *)value;
- (void)addSection:(NSSet<Section *> *)values;
- (void)removeSection:(NSSet<Section *> *)values;

@end

NS_ASSUME_NONNULL_END
