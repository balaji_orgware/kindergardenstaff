//
//  TSAttachmentPicker.m
//  Trackidon Staff
//
//  Created by Elango on 28/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSAttachmentPicker.h"
#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>

@implementation TSAttachmentPicker


+ (instancetype)instance{
    
    static TSAttachmentPicker *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        kSharedInstance = [[TSAttachmentPicker alloc]init];
        
    });
    
    return kSharedInstance;

    
}

#pragma mark - Attachment Picker

- (void)showAttachmentPickerForView:(UIViewController *)viewController frame:(CGRect)frame
{
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Choose Attachment" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery", nil];
    
    if (IS_IPAD) {
        
        [actionSheet showFromRect:frame inView:viewController.view animated:YES];
        
    } else {
        [actionSheet showInView:viewController.view];
    }
}

#pragma mark ActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex==0) {
        
        [self openCamera];
        
    } else if(buttonIndex ==1){
        
        [self openGallery];
        
    }
}

- (void) openCamera{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        [TSSingleton showAlertWithMessage:@"Your device does not have camera!"];
        
    } else {
        
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        
        if(status == AVAuthorizationStatusAuthorized) { // authorized
            
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
            
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            //imagePicker.allowsEditing = YES;

            imagePicker.delegate = self;
            
            imagePicker.navigationBar.barTintColor = APPTHEME_COLOR;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [[self getCurrentViewController] presentViewController:imagePicker animated:YES completion:NULL];
            });
            

            
        } else if (status == AVAuthorizationStatusNotDetermined){
            
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if(granted){
                    
                    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
                    
                    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    
                    //imagePicker.allowsEditing = YES;
                    
                    imagePicker.delegate = self;
                    
                    imagePicker.navigationBar.barTintColor = APPTHEME_COLOR;
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [[self getCurrentViewController] presentViewController:imagePicker animated:YES completion:NULL];
                    });
                
                    
                    NSLog(@"Granted access");
                    
                } else {
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                            //[SINGLETON_INSTANCE showAlertMessageWithTitle:@"Camera Access Denied By You" andMessage:@"Please enable camera access to TrakidonStaff in settings"];
                        
                        [TSSingleton showAlertWithMessage:@"Camera Access Denied By You"];
                    
                            NSLog(@"Not granted access");
                        
                    });
                    
                }
            }];
        }else { // denied
            
            [self showPermissionDeclinedAlert];
            
            //[SINGLETON_INSTANCE showAlertMessageWithTitle:@"Camera Access Denied By You" andMessage:@"Please enable camera access to TrakidonStaff in settings"];
        }
    }
}

- (void)openGallery{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        [TSSingleton showAlertWithMessage:@"Your device does not have any PhotoLibrary!"];
    } else {
        
        PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
        
        if (status == PHAuthorizationStatusAuthorized) {
            
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
            
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            //imagePicker.allowsEditing = YES;
            
            imagePicker.delegate = self;
            
            imagePicker.navigationBar.barTintColor = APPTHEME_COLOR;

            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [[self getCurrentViewController] presentViewController:imagePicker animated:YES completion:NULL];
                
            });
            
            
            
        } else if (status == PHAuthorizationStatusNotDetermined){
            
            if (!SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                
                [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                    
                    if (status == PHAuthorizationStatusAuthorized) {
                        
                        UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
                        
                        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                        
                        //imagePicker.allowsEditing = YES;
                        
                        imagePicker.delegate = self;
                        
                        imagePicker.navigationBar.barTintColor = APPTHEME_COLOR;

                        
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            
                            [[self getCurrentViewController] presentViewController:imagePicker animated:YES completion:NULL];
                            
                        });
                        
                    } else{
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            
                            //[SINGLETON_INSTANCE showAlertMessageWithTitle:@"Gallery Access Denied By You" andMessage:@"Please enable Gallery access to TrakidonStaff in settings"];
                            
                            [TSSingleton showAlertWithMessage:@"Gallery Access Denied By You"];
                            
                        });
                    }
                }];
            } else {// IOS <8.0
                
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
                
                imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                
                //imagePicker.allowsEditing = YES;
                
                imagePicker.delegate = self;
                
                imagePicker.navigationBar.barTintColor = APPTHEME_COLOR;

                
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     
                    [[self getCurrentViewController] presentViewController:imagePicker animated:YES completion:NULL];
                    
                });
            }
        }else { // denied
            
            [self showPermissionDeclinedAlert];
            
            //[SINGLETON_INSTANCE showAlertMessageWithTitle:@"Gallery Access Denied By You" andMessage:@"Please enable Gallery access to TrakidonStaff in settings"];
        }
    }
}

#pragma mark - ImagePicker Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    UIImage *choosenImage = info[UIImagePickerControllerOriginalImage];
    
    NSURL *imageURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    
    [self nameForSelectedImageFromRefranceUrl:imageURL];
    
    
    NSData *imageData = UIImageJPEGRepresentation(choosenImage, 0.5);
    
    float imageSize = imageData.length/1024.0f/1024.0f;
    
    NSLog(@"File size is : %.2f MB",imageSize);
    
    if (imageSize <= 16) {
        
        [[self delegate] selectedAttachmentImageData:imageData imageName:@"image" isHasAttachment:YES];
        
        [picker dismissViewControllerAnimated:YES completion:NULL];
        
    } else {
        
        [SINGLETON_INSTANCE showAlertMessageWithTitle:@"File Size Too Large" andMessage:@"Please select file size less than 16MB"];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [[self delegate] selectedAttachmentImageData:nil imageName:nil isHasAttachment:NO];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)nameForSelectedImageFromRefranceUrl:(NSURL *)imageURL{
    
    __block NSString *imageName;

    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
    {
        ALAssetRepresentation *representation = [myasset defaultRepresentation];
        NSString *fileName = [representation filename];
        NSLog(@"fileName : %@",fileName);
        
        imageName = [representation filename];
        
        [[self delegate] selectedAttachmentImageName:imageName];

    };
    
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    
    [assetslibrary assetForURL:imageURL
                   resultBlock:resultblock
                  failureBlock:nil];
    
}

#pragma mark - Methods

- (void)showPermissionDeclinedAlert
{
    if (!SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        
        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]]){
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Access Denied By You" message:@"Please enable Media access to TrakidonStaff in settings" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [alertController dismissViewControllerAnimated:YES completion:nil];
                
            }];
            
            UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                
                [alertController dismissViewControllerAnimated:NO completion:nil];
                
            }];
            
            [alertController addAction:cancelAction];
            
            [alertController addAction:settingsAction];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
                [[self getCurrentViewController] presentViewController:alertController animated:YES completion:nil];
                
            });

        }else {
            [SINGLETON_INSTANCE showAlertMessageWithTitle:@"Access Denied By You" andMessage:@"Please enable access to TrakidonStaff in settings"];
        }
        
    }else
    {
        [SINGLETON_INSTANCE showAlertMessageWithTitle:@"Access Denied By You" andMessage:@"Please enable  access to TrakidonStaff in settings"];
    }
    
}

-(id)getCurrentViewController
{
    id WindowRootVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    id currentViewController = [self findTopViewController:WindowRootVC];
    
    return currentViewController;
}

-(id)findTopViewController:(id)inController
{
    if ([inController isKindOfClass:[UITabBarController class]])
    {
        return [self findTopViewController:[inController selectedViewController]];
    }
    else if ([inController isKindOfClass:[UINavigationController class]])
    {
        return [self findTopViewController:[inController visibleViewController]];
    }
    else if ([inController isKindOfClass:[UIViewController class]])
    {
        return inController;
    }
    else
    {
        NSLog(@"Unhandled ViewController class : %@",inController);
        return nil;
    }
}



@end
