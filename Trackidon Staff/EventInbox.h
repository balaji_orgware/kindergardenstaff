//
//  EventInbox.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/4/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StaffDetails;

NS_ASSUME_NONNULL_BEGIN

@interface EventInbox : NSManagedObject

+ (instancetype)instance;

- (void)saveEventInboxWithResponseDict:(NSDictionary *)dict;

-(void) savePushNotificationEventWithResponseDict:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END

#import "EventInbox+CoreDataProperties.h"
