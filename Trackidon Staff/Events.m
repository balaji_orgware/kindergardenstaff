//
//  Events.m
//  Trackidon Staff
//
//  Created by Elango on 19/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "Events.h"

@implementation Events

+ (instancetype)instance{
    
    static Events *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (&onceToken, ^{
        
        kSharedInstance = [[Events alloc]init];
        
    });
    
    return kSharedInstance;
}

- (void)saveEventsWithResponseDict:(NSDictionary *)dict andIsForApprovals:(BOOL)isForApprovals{
    
    NSLog(@"%@",dict);
    
    @try {
        
        NSManagedObjectContext *mainContext = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSArray *responseArray = [NSArray new];
        
        NSPredicate *predicate;
        
        if (isForApprovals) {// Events Approvals
            
            responseArray = dict[@"FetchUnApprovedCommunicationResult"][@"UnApprovalClass"][@"EventsMClass"];
            
            predicate = [NSPredicate predicateWithFormat:@"isForApprovals == 1"];
            
        }
        else{ // Events
            
            responseArray = dict[@"FetchEventsMDetailsSelectByStaffTransIDResult"][@"EventsMClass"];
            
            predicate = [NSPredicate predicateWithFormat:@"isForApprovals == 0"];
        }
        
        [SINGLETON_INSTANCE deleteSelectedObjectsInEntityWithEntityName:@"Events" withPredicate:predicate withContext:mainContext];

        
        NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        
        [context setParentContext:mainContext];
        
        NSManagedObjectID *objectId = [SINGLETON_INSTANCE.staff objectID];
        
        StaffDetails *tmpStaff = [context objectWithID:objectId];

        for (NSDictionary *tmpEventDict in responseArray) {
            
            NSDictionary *eventDict = [tmpEventDict dictionaryByReplacingNullsWithBlanks];
            
            Events *event;
            
            //if(isForApprovals){
                
                event = [NSEntityDescription insertNewObjectForEntityForName:@"Events" inManagedObjectContext:context];
            /*}
            else{
             
                event = [self checkAvailabilityWithEventTransId:eventDict[@"TransID"] context:context withIsForApprovals:isForApprovals];
                
                if (event == nil) {
                    
                    event = [NSEntityDescription insertNewObjectForEntityForName:@"Events" inManagedObjectContext:context];
                }
            }*/
            
            event.acadamicTransId = eventDict[@"AcadamicTransID"];
            //event.approvedByTransId = eventDict[@"ApprovedBy"];
            event.approvedByName = eventDict[@"ApprovedByName"];
            //event.approvedDate = [SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:eventDict[@"ApprovedDate"]];
            event.approvedStatus = [NSNumber numberWithInteger:[eventDict[@"ApprovedStatus"]integerValue]];
            event.eventDescription = eventDict[@"Description"];
            event.eventTime = eventDict[@"EventTime"];
            event.eventTitle = eventDict[@"EventTitle"];
            event.eventAttachmentUrl = eventDict[@"Attachment"];
            event.eventTypeId = [NSNumber numberWithInteger:[eventDict[@"EventTypeID"]integerValue]];
            event.eventFromDate = [SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:eventDict[@"FromDate"]];
            event.eventGroupType = [NSNumber numberWithInteger:[eventDict[@"GroupType"]integerValue]];
            event.eventIsHasAttachment = [NSNumber numberWithInteger:[eventDict[@"IsAttachment"]integerValue]];
            event.schoolTransId = eventDict[@"SchoolTransID"];
            //event.secondLangTransID = eventDict[@"SecondLangTransID"];
            event.eventToDate = [SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:eventDict[@"ToDate"]];
            event.eventTransId = eventDict[@"TransID"];
            //event.userCreatedTransId = eventDict[@"UserCreated"];
            event.userCreatedName = eventDict[@"UserCreatedName"];
            //event.userModifiedTransId = eventDict[@"UserModified"];
            event.userModifiedName = eventDict[@"UserModifiedName"];
            event.eventVenue = eventDict[@"Venue"];
            event.staffTransId = SINGLETON_INSTANCE.staffTransId;
            event.boardName = eventDict[@"BoardName"];
//            event.boardTransID = boardTransID;
            event.isForApprovals = [NSNumber numberWithBool:isForApprovals];
            event.modifiedDate = [SINGLETON_INSTANCE dateFromString:eventDict[@"ModifiedDate"] withFormate:DATE_FORMATE_MODIFIED_DATE];
            
            event.staff = tmpStaff;
            
            NSString *assigneeList = [[TSCommonAssignedDetails instance] getAssignedDetailsWithArray:eventDict[@"SelectEventsMDetails"] withGroupType:eventDict[@"GroupType"]];
            
            event.assignedList = assigneeList;
            
            
           //[[AssignedDetails instance] saveAssignedDetailsWithDict:eventDict[@"SelectEventsMDetails"] withGroupType:eventDict[@"GroupType"] transId:event.eventTransId forObject:event objectType:OBJECT_TYPE_EVENT context:context];
            
        }
        
        NSError *error;

        if ([context hasChanges]) {
            if (![context save:&error]) {
                
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
        }
        
        [SINGLETON_INSTANCE storeContextInMainThread];
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    }
}

-(Events *)checkAvailabilityWithEventTransId:(NSString *)eventTransId context:(NSManagedObjectContext *)context withIsForApprovals:(BOOL)isForApprovals
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Events" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"eventTransId contains[cd] %@ AND isForApprovals == %@ AND staffTransId MATCHES %@", eventTransId,[NSNumber numberWithBool:isForApprovals],SINGLETON_INSTANCE.staffTransId];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
}

- (void)savePushNotificationEventsWithResponseDict:(NSDictionary *)dictionary{
    
    @try {
        
        NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSError *error = nil;
        
        NSDictionary *eventDict = [dictionary[@"FetchEventByTransIDResult"][@"EventsMClass"] dictionaryByReplacingNullsWithBlanks];
        
        Events *event = [self checkAvailabilityWithEventTransId:eventDict[@"TransID"] context:context withIsForApprovals:NO];
        
        if (event == nil) {
            
            event = [NSEntityDescription insertNewObjectForEntityForName:@"Events" inManagedObjectContext:context];
        }
        
        event.acadamicTransId = eventDict[@"AcadamicTransID"];
        //event.approvedByTransId = eventDict[@"ApprovedBy"];
        event.approvedByName = eventDict[@"ApprovedByName"];
        //event.approvedDate = [SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:eventDict[@"ApprovedDate"]];
        event.approvedStatus = [NSNumber numberWithInteger:[eventDict[@"ApprovedStatus"]integerValue]];
        event.eventDescription = eventDict[@"Description"];
        event.eventTime = eventDict[@"EventTime"];
        event.eventTitle = eventDict[@"EventTitle"];
        event.eventAttachmentUrl = eventDict[@"Attachment"];
        event.eventTypeId = [NSNumber numberWithInteger:[eventDict[@"EventTypeID"]integerValue]];
        event.eventFromDate = [SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:eventDict[@"FromDate"]];
        event.eventGroupType = [NSNumber numberWithInteger:[eventDict[@"GroupType"]integerValue]];
        event.eventIsHasAttachment = [NSNumber numberWithInteger:[eventDict[@"IsAttachment"]integerValue]];
        event.schoolTransId = eventDict[@"SchoolTransID"];
        //event.secondLangTransID = eventDict[@"SecondLangTransID"];
        event.eventToDate = [SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:eventDict[@"ToDate"]];
        event.eventTransId = eventDict[@"TransID"];
        //event.userCreatedTransId = eventDict[@"UserCreated"];
        event.userCreatedName = eventDict[@"UserCreatedName"];
        //event.userModifiedTransId = eventDict[@"UserModified"];
        event.userModifiedName = eventDict[@"UserModifiedName"];
        event.eventVenue = eventDict[@"Venue"];
        event.staffTransId = SINGLETON_INSTANCE.staffTransId;
        event.boardName = eventDict[@"BoardName"];
        event.isForApprovals = [NSNumber numberWithBool:NO];
        event.modifiedDate = [SINGLETON_INSTANCE dateFromString:eventDict[@"ModifiedDate"] withFormate:DATE_FORMATE_MODIFIED_DATE];
        
        event.staff = SINGLETON_INSTANCE.staff;
        
        NSString *assigneeList = [[TSCommonAssignedDetails instance] getAssignedDetailsWithArray:eventDict[@"SelectEventsMDetails"] withGroupType:eventDict[@"GroupType"]];
        
        event.assignedList = assigneeList;

        
        if (![context save:&error]) {
            
            NSLog(@"Oops ! Could not save ");
        }
        
    } @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    } @finally {
    
        
    }
}

@end