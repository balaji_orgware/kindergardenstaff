//
//  TSHolidayDescriptionViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/30/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSHolidayDescriptionViewController.h"

@interface TSHolidayDescriptionViewController ()

@end

@implementation TSHolidayDescriptionViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(descViewTapped)];
    
    [_descriptionTextView addGestureRecognizer:tapGesture];
    
    [TSSingleton layerDrawForView:_topView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:_dateView position:LAYER_TOP color:[UIColor lightGrayColor]];
    
    _fromDateLbl.layer.cornerRadius = _fromDateLbl.frame.size.width/2;
    
    _fromDateLbl.layer.masksToBounds = YES;
    
    _toDateLbl.layer.cornerRadius = _toDateLbl.frame.size.width/2;
    
    _toDateLbl.layer.masksToBounds = YES;
    
    //_progressView.center = CGPointMake(_fromDateLbl.center.x, _fromDateLbl.frame.origin.y);
    
    _progressView.frame = CGRectMake(_fromDateLbl.center.x +_fromDateLbl.frame.size.width/2, _fromDateLbl.center.y, _progressView.frame.size.width, _progressView.frame.size.height);
    
    _toDateLbl.frame = CGRectMake(_progressView.center.x +_progressView.frame.size.width/2, _fromDateLbl.frame.origin.y, _toDateLbl.frame.size.width, _toDateLbl.frame.size.height);
    
    _fromLbl.center = CGPointMake(_fromDateLbl.center.x, _fromDateLbl.frame.origin.y-10);
    
    _toLbl.center = CGPointMake(_toDateLbl.center.x, _toDateLbl.frame.origin.y-10);
    
    _holidaysObj = (Holidays *)self.holidaysObj;
    
    _fromDateLbl.attributedText = [SINGLETON_INSTANCE getDateAttributedStringForDateString:_holidaysObj.fromDate inLabel:_fromDateLbl];
    
    _toDateLbl.attributedText = [SINGLETON_INSTANCE getDateAttributedStringForDateString:_holidaysObj.toDate inLabel:_toDateLbl];
    
    _descriptionTextView.text = [_holidaysObj.holidayDescription capitalizedString];
    
    _titleLbl.text = [NSString stringWithFormat:@"Title : %@",_holidaysObj.holidayTitle];
    
    NSDate *holidayFromDate = [TSSingleton dateFromString:_holidaysObj.fromDate dateFormat:@"dd-MMM-yyyy"];
    
    NSDate *holidayToDate = [TSSingleton dateFromString:_holidaysObj.toDate dateFormat:@"dd-MMM-yyyy"];
    
    if ([holidayFromDate compare:holidayToDate]==NSOrderedSame) {
        
        // _remainingDaysCountLbl.text = [NSString stringWithFormat:@"%@  Day holiday",[TPSingleton daysBetweenFromDate:holidayFromDate toDate:holidayToDate]];
        
        _remainingCountLbl.text = [NSString stringWithFormat:@"%@",[TSSingleton daysBetweenFromDate:holidayFromDate toDate:holidayToDate]];
        
        _daysLeftLbl.text = @"Day Holiday";
    }
    else {
        //_remainingDaysCountLbl.text = [NSString stringWithFormat:@"%@  Days holiday",[TPSingleton daysBetweenFromDate:holidayFromDate toDate:holidayToDate]];
        
        _remainingCountLbl.text =[NSString stringWithFormat:@"%@",[TSSingleton daysBetweenFromDate:holidayFromDate toDate:holidayToDate]];
        
        _daysLeftLbl.text = @"Days Holiday";
        
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_HOLIDAYDESCRIPTION parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Holiday Detail";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) descViewTapped{
    
    [TSAnalytics logEvent:ANALYTICS_HOLIDAY_CLICKED_DESCRIPTION parameter:[NSMutableDictionary new]];
    
    TSDescriptionViewController *descVC = [[TSDescriptionViewController alloc] initWithNibName:@"TSDescriptionViewController" bundle:nil];
    
    [descVC showInViewController:self descriptionString:_descriptionTextView.text];
    
}

@end
