//
//  TSHealthTodayViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 25/06/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSHealthStudSelectionViewController.h"

@interface TSHealthStudSelectionViewController ()
{
    NSMutableArray *studentsListArray;
    
    NSArray *boardArray;
    
    NSString *selectedBoardTransID,*selectedClassTransID,*selectedSectionTransID;

    TSSelectionViewController *selectionVC;
}
@end

@implementation TSHealthStudSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    studentsListArray = [NSMutableArray new];
    
    NSPredicate *boardPredicate = [NSPredicate predicateWithFormat:@"schoolTransId contains[cd] %@ AND isViewableByStaff == 1",SINGLETON_INSTANCE.selectedSchoolTransId];
    
    boardArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Board" withPredicate:boardPredicate];
    
    if (boardArray.count == 1) {
        Board *boardObj = [boardArray objectAtIndex:0];
        
        _boardTxtFld.text = boardObj.boardName;
        selectedBoardTransID = boardObj.boardTransId;
        
        _selectionView.frame = CGRectMake(_selectionView.frame.origin.x, -_selectionView.frame.size.height/2, _selectionView.frame.size.width, _selectionView.frame.size.height);
        _studentsTableView.frame = CGRectMake(_studentsTableView.frame.origin.x, _selectionView.frame.origin.y+_selectionView.frame.size.height, _studentsTableView.frame.size.width, DeviceHeight - (_selectionView.frame.origin.y+_selectionView.frame.size.height));
    }
    
    [TSSingleton layerDrawForView:_boardTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_classTxtFld position:LAYER_RIGHT color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_selectionView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    [SINGLETON_INSTANCE addLeftInsetToTextField:_boardTxtFld];
    [SINGLETON_INSTANCE addLeftInsetToTextField:_classTxtFld];
    [SINGLETON_INSTANCE addLeftInsetToTextField:_sectionTxtFld];
    
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_boardTxtFld];
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_classTxtFld];
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_sectionTxtFld];
    
    SINGLETON_INSTANCE.needRefreshHealthRecords = NO;
    
    [_studentsTableView setTableFooterView:[UIView new]];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Health Today";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Tableview Datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return  studentsListArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    StudentModel *studentObj = [studentsListArray objectAtIndex:indexPath.row];
    
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:100];
    
    nameLabel.text = studentObj.studentName;
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

#pragma mark - Tableview Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TSHealthHomeViewController *healthHomeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBHealthHome"];
    
    [healthHomeVC setStudentObj:[studentsListArray objectAtIndex:indexPath.row]];
    
    [self.navigationController pushViewController:healthHomeVC animated:YES];
}

#pragma mark - Helper Methods

-(void)showSelectionViewControllerWithSelectedTextField:(UITextField *)textField withSelectedValue:(NSString *)selected{
    
    if (!selectionVC) {
        
        selectionVC = [[TSSelectionViewController alloc]initWithNibName:@"TSSelectionViewController" bundle:nil];
        
        selectionVC.delegate = self;
        
        NSMutableDictionary *contentDict = [NSMutableDictionary new];
        
        if (textField == _boardTxtFld) {
            
            for (Board *boardObj  in boardArray) {
                
                [contentDict setValue:boardObj.boardTransId forKey:boardObj.boardName];
            }
        }
        else if (textField == _classTxtFld){
            
            NSPredicate *classPredicate = [NSPredicate predicateWithFormat:@"boardTransId MATCHES %@",selectedBoardTransID];
            Board *boardObj = [boardArray filteredArrayUsingPredicate:classPredicate].count>0 ? [[boardArray filteredArrayUsingPredicate:classPredicate] objectAtIndex:0] : nil;

            for (Classes *classObj in boardObj.classes) {
                [contentDict setValue:classObj.classTransId forKey:classObj.classStandardName];
            }
        }
        else if (textField == _sectionTxtFld){
            
            NSPredicate *boardPredicate = [NSPredicate predicateWithFormat:@"boardTransId MATCHES %@",selectedBoardTransID];
            Board *boardObj = [boardArray filteredArrayUsingPredicate:boardPredicate].count>0 ? [[boardArray filteredArrayUsingPredicate:boardPredicate] objectAtIndex:0] : nil;
            
            NSPredicate *classPredicate = [NSPredicate predicateWithFormat:@"classTransId MATCHES %@",selectedClassTransID];
            
            Classes *classObj = [[boardObj.classes filteredSetUsingPredicate:classPredicate] allObjects].count >0 ? [[[boardObj.classes filteredSetUsingPredicate:classPredicate] allObjects] objectAtIndex:0] : nil;
            
            for (Section *secObj in classObj.section) {
                
                [contentDict setValue:secObj.sectionTransId forKey:secObj.sectionName];
            }
        }
        
        if (contentDict.allKeys.count >0) {
            
            [selectionVC setContentDict:contentDict];
            [selectionVC setSelectedValue:selected];
            
            [selectionVC showSelectionViewController:self forTextField:textField];
        }
        else{
            if (textField == _boardTxtFld) {
                
                [TSSingleton showAlertWithMessage:@"No boards to Choose"];
            }
            else if (textField == _classTxtFld){
                
                [TSSingleton showAlertWithMessage:@"No Classes to Choose"];
            }
            else if (textField == _sectionTxtFld){
                
                [TSSingleton showAlertWithMessage:@"No Sections to Choose"];
            }
            selectionVC = nil;
        }
    }
}

#pragma mark - SelectionCallBack Delegate

-(void)selectedValueFromDict:(NSDictionary *)contentDict withSelectedValue:(NSString *)selectedValue andTransID:(NSString *)transID forTextField:(UITextField *)textField{
    
    if (textField == _boardTxtFld) {
        
        selectedBoardTransID = transID;
    }
    else if (textField  == _classTxtFld){
        
        selectedClassTransID = transID;
    }
    else if(textField == _sectionTxtFld){
        
        selectedSectionTransID = transID;
        
        [self fetchStudentListAPICall];
    }
    selectionVC = nil;
}

-(void)backViewTappedOn:(UIViewController *)viewController{
    selectionVC = nil;
}

#pragma mark - TextField Delegates

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField == _boardTxtFld) {
        
        if (boardArray.count == 1) {
            
            return NO;
        }
        else{
            _classTxtFld.text = @"";
            _sectionTxtFld.text = @"";
            selectedClassTransID = nil;
            selectedSectionTransID = nil;
        }
    }
    else if (textField == _classTxtFld){
        
        if (_boardTxtFld.text.length == 0) {
            [TSSingleton showAlertWithMessage:@"Please Choose Board"];
            return  NO;
        }
        else{
            _sectionTxtFld.text = @"";
            selectedSectionTransID = nil;
        }
    }
    else if (textField == _sectionTxtFld){
        
        if (_boardTxtFld.text.length == 0) {
            [TSSingleton showAlertWithMessage:@"Please Choose Board"];
            return  NO;
        }
        else if (_classTxtFld.text.length == 0){
            [TSSingleton showAlertWithMessage:@"Please Choose Class"];
            return  NO;
        }
    }
    
    [studentsListArray removeAllObjects];
    
    [self.studentsTableView reloadData];
    
    [self showSelectionViewControllerWithSelectedTextField:textField withSelectedValue:textField.text];
    
    return NO;
}

#pragma mark - Service Call

-(void) fetchStudentListAPICall{
    
    NSString *urlString = @"MastersMServices/StudentsMService.svc/FetchStudentsByScectionList";
    
    NSDictionary *params = @{@"SectionList":@[selectedSectionTransID]};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchStudentsByScectionListResult"][@"ResponseCode"] integerValue] == 1) {
                
                [studentsListArray removeAllObjects];
                
                for (NSDictionary *dict in responseDictionary[@"FetchStudentsByScectionListResult"][@"StudentsMClass"]) {
                    
                    StudentModel *studentObj = [StudentModel new];
                    studentObj.studentName = dict[@"StudentName"];
                    studentObj.studentTransID = dict[@"TransID"];
                    
                    [studentsListArray addObject:studentObj];
                    
                    studentObj = nil;
                }
                
                if (studentsListArray.count >0) {
                    
                    [self.studentsTableView setHidden:NO];
                    [self.studentsTableView reloadData];
                }
                else{
                    [self.studentsTableView setHidden:YES];
                }
                
            }
            else{
                
                [self.studentsTableView setHidden:YES];
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"FetchStudentsByScectionListResult"][@"ResponseMessage"]];
            }
        }
        else{
            [self.studentsTableView setHidden:YES];
            
            [TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
        }
    }];
}
@end