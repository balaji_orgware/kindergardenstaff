//
//  TSBusTrackViewController.m
//  Trackidon Staff
//
//  Created by ORGware on 20/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSBusTrackViewController.h"

@interface TSBusTrackViewController ()
{
    BOOL isBusMarkerCreated;
    
    NSMutableArray *busDetailsArray,*busMarkersArray;
    
    NSString *selectedBus;
    
    TSSelectionViewController *selectionView;
}
@end

@implementation LiveTrackModel


@end

@implementation TSBusTrackViewController

#pragma mark - View lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    busDetailsArray = [NSMutableArray new];

    busMarkersArray = [NSMutableArray new];

    [SINGLETON_INSTANCE addDropDownArrowToTextField:_busSelectTxtFld];

    CLLocationCoordinate2D schoolLocation = CLLocationCoordinate2DMake([[USERDEFAULTS objectForKey:@"SchoolLatitude"] floatValue], [[USERDEFAULTS objectForKey:@"SchoolLongitude"] floatValue]);
    
    GMSMarker *schoolMarker = [GMSMarker new];
    schoolMarker.position = schoolLocation;
    schoolMarker.title = [USERDEFAULTS objectForKey:@"SchoolName"];
    schoolMarker.icon = [UIImage imageNamed:@"school"];
    schoolMarker.map = _mapView;
    
    
    GMSCameraPosition *cameraPosition = [GMSCameraPosition
                                         cameraWithTarget:schoolLocation zoom:12.0];
    _mapView.camera = cameraPosition;
    _mapView.myLocationEnabled = YES;
    _mapView.delegate = self;
    
    selectedBus = @"";
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationItem.title = @"Live Track";
    
    [self getBusLiveTrackDetails];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(getBusLiveTrackDetails) object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - GMS Map Delegate

-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    
    [self removeSelectionView];
}

-(void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
    
    [self removeSelectionView];
}

#pragma mark - Selection View Methods

// Show Selection View 
-(void) showSelectionViewWithSelected:(NSString *)selected forTextField:(UITextField *)textField{
    
    selectionView = [[TSSelectionViewController alloc]initWithNibName:@"TSSelectionViewController" bundle:nil];
    
    [selectionView setDelegate:self];
    
    NSMutableDictionary *contentDict = [NSMutableDictionary new];
    
    [contentDict setValue:@"" forKey:@"Select all"];
    
    for (LiveTrackModel *modelObj in busDetailsArray) {
        
        [contentDict setValue:modelObj.busNo forKey:[NSString stringWithFormat:@"%@ - %@",modelObj.busRouteName,modelObj.busNo]];
    }

    [selectionView setContentDict:contentDict];
    
    [selectionView setSelectedValue: selected.length>0 ? selected : @""];
    
    [selectionView showSelectionViewController:self forTextField:textField];
}

// Remove SelectionVC from Parent view controller
-(void) removeSelectionView{
    
//    [selectionView removeSelectionViewController:self];
    
    selectionView = nil;
}

#pragma mark - Selection View Delegate 

// Method to Get Selected Value from Common SelectionViewController
-(void)selectedValueFromArray:(NSArray *)contentArray withValue:(NSString *)string forTextField:(UITextField *)textField{
    
    _busSelectTxtFld.text = string;
    
    for (LiveTrackModel *modelObj in busDetailsArray) {
        
        if([string isEqualToString:@"Select All"]){
            
            selectedBus = @"";
            
            break;
        }
        else if ([string isEqualToString:[NSString stringWithFormat:@"%@ - %@",modelObj.busRouteName,modelObj.busNo]]) {
            
            selectedBus = modelObj.busNo;
            
            break;
        }
    }
    
    selectionView = nil;
    
    isBusMarkerCreated = NO;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(getBusLiveTrackDetails) object:nil];
    
    [self getBusLiveTrackDetails];
}

-(void)selectedValueFromDict:(NSDictionary *)contentDict withSelectedValue:(NSString *)selectedValue andTransID:(NSString *)transID forTextField:(UITextField *)textField{
    
    selectedBus = transID; // Here TransID has busNo.
    
    selectionView = nil;
    
    isBusMarkerCreated = NO;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(getBusLiveTrackDetails) object:nil];
    
    [self getBusLiveTrackDetails];
    
}

-(void)backViewTappedOn:(UIViewController *)viewController{
    
    selectionView = nil;
}

#pragma mark - Textfield Delegates

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{

    if (selectionView == nil) {
        
        [self showSelectionViewWithSelected:textField.text forTextField:textField];
    }
    
    return NO;
}

#pragma mark - Create Map Marks

// Method to create markers using Model objects
-(void) createAllBusMarkers:(NSArray *)array{
    
    for (int i=0;i<array.count;i++) {
        
        LiveTrackModel *modelObj = [array objectAtIndex:i];
        
        GMSMarker *marker = [GMSMarker new];
        
        marker.position = CLLocationCoordinate2DMake([modelObj.busLatitude floatValue], [modelObj.busLongitude floatValue]);
        marker.title = [NSString stringWithFormat:@"%d",i];
        marker.snippet = @"BusMarker";
        marker.icon = [UIImage imageNamed:@"bus"];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.map = _mapView;
        
        [busMarkersArray addObject:marker];
    }
}

// Method to create specific bus marker
-(void) createSpecificBusMarker:(NSArray *)array selectedBusNo:(NSString *)busNo{
    
    for (int i=0;i<array.count;i++) {
        
        LiveTrackModel *modelObj = [array objectAtIndex:i];
        
        if ([modelObj.busNo isEqualToString:busNo]) {
            
            GMSMarker *specificBusMarker = [GMSMarker new];
            
            specificBusMarker.position = CLLocationCoordinate2DMake([modelObj.busLatitude floatValue], [modelObj.busLongitude floatValue]);
            specificBusMarker.title = [NSString stringWithFormat:@"%d",i];
            specificBusMarker.snippet = @"busMarker";
            specificBusMarker.map = _mapView;
            specificBusMarker.icon = [UIImage imageNamed:@"bus_normal"];
            
            [busMarkersArray addObject:specificBusMarker];
        }
    }
}

// Remove all markers in map
-(void) removeAllMarkers{
    
    for (GMSMarker *marker in busMarkersArray) {
        
        marker.map = nil;
    }
    
    [busMarkersArray removeAllObjects];
}

// Showing all bus markers
-(void) selectAllBus:(NSArray *)array{
    
    if (!isBusMarkerCreated) {
        
        [self removeAllMarkers];
        
        [self createAllBusMarkers:array];
        
        isBusMarkerCreated = YES;
    }
    else{
        
        for (int i=0;i<array.count;i++) {
            
            LiveTrackModel *modelObj = [array objectAtIndex:i];
            
            GMSMarker *allBusMarker = (GMSMarker *)[busMarkersArray objectAtIndex:i];
            
            [CATransaction begin];
            
            [CATransaction setAnimationDuration:10.0];
            
            allBusMarker.position = CLLocationCoordinate2DMake([modelObj.busLatitude floatValue], [modelObj.busLongitude floatValue]);
            
            [CATransaction commit];
        }
    }
}

// Showing Specific bus
-(void) selectSpecificBus:(NSArray *)array andBusNo:(NSString *)busNo{
    
    if (!isBusMarkerCreated) {
        
        [self removeAllMarkers];
        
        [self createSpecificBusMarker:array selectedBusNo:busNo];
        
        isBusMarkerCreated = YES;
    }
    else{
        
        for (int i=0; i<array.count; i++) {
            
            LiveTrackModel *modelObj = [array objectAtIndex:i];
            
            if ([busNo isEqualToString:modelObj.busNo]) {
                
                GMSMarker *specificBusMarker = (GMSMarker *)[busMarkersArray objectAtIndex:0];
                
                [CATransaction begin];
                
                [CATransaction setAnimationDuration:10.0];
                
                GMSCameraUpdate *cameraUpdate = [GMSCameraUpdate setTarget:specificBusMarker.position zoom:_mapView.camera.zoom];
                
                [_mapView animateWithCameraUpdate:cameraUpdate];
                
                specificBusMarker.position = CLLocationCoordinate2DMake([modelObj.busLatitude floatValue], [modelObj.busLongitude floatValue]);
                
                [CATransaction commit];
            }
        }
    }
}

// Method to call api call with 10.sec interval
-(void) getBusLiveTrackDetails{
    
    [self fetchLiveTrackAPICall];
    
    [self performSelector:@selector(getBusLiveTrackDetails) withObject:nil afterDelay:10.0];
}

#pragma mark - Service API Call

// Fetch Live Track service call
-(void) fetchLiveTrackAPICall{
    
    NSString *urlString = @"MastersMServices/BusMasterService.svc/FetchBusMasters";
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:nil currentView:self.view showIndicator:NO onCompletion:^(BOOL success, NSDictionary *dictionary) {
        
        if (success) {
                        
            [busDetailsArray removeAllObjects];
            
            if ([dictionary[@"FetchBusMastersResult"][@"ResponseCode"] intValue] == 1) {
                
                NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                
                if (dictionary[@"FetchBusMastersResult"][@"BusMasterClass"] != [NSNull null]) {
                    
                    for (int i = 0; i < [dictionary[@"FetchBusMastersResult"][@"BusMasterClass"] count]; i++) {
                        
                        NSMutableDictionary *dict = [dictionary[@"FetchBusMastersResult"][@"BusMasterClass"] objectAtIndex:i];
                        
                        LiveTrackModel *obj = [[LiveTrackModel alloc] init];
                        
                        [obj setBusLatitude:(dict[@"BusLatitude"])!=[NSNull null] ? dict[@"BusLatitude"]: @"0.0"];
                        
                        [obj setBusLongitude:(dict[@"BusLongtitude"])!=[NSNull null] ? dict[@"BusLongtitude"]: @"0.0"];
                        
                        [obj setBusNo:[SINGLETON_INSTANCE nullValueCheck:dict[@"BusNo"]]];
                        
                        [obj setBusRouteName:[SINGLETON_INSTANCE nullValueCheck:dict[@"RouteName"]]];
                        
                        [obj setBusRouteTransID:dict[@"RouteTransID"]];
                        
                        [tempArray addObject:obj];
                        
                        obj = nil;
                    }
                }
                
                busDetailsArray = [NSMutableArray arrayWithArray:tempArray];
                
                if ([selectedBus isEqualToString:@""]) {
                    
                    [self selectAllBus:busDetailsArray];

                }
                else{
                    
                    [self selectSpecificBus:busDetailsArray andBusNo:selectedBus];
                }
            }
        }
    }];
}

@end