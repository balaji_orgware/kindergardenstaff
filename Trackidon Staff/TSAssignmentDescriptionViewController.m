//
//  TSAssignmentDescriptionViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/28/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSAssignmentDescriptionViewController.h"

@interface TSAssignmentDescriptionViewController ()
{
    Assignments *assignmentsObj;
    
    NSURL *attachmentURL;
    
    NSArray *assigneeArr;

    UITapGestureRecognizer *assignedViewTap;
}
@end

@implementation TSAssignmentDescriptionViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    /*NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transId MATCHES %@",_assignmentObj.transID];
    
    assigneeArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"AssignedDetails" withPredicate:predicate];
    
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"assigneeType" ascending:YES];
    
    NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
    
    assigneeArr = [assigneeArr sortedArrayUsingDescriptors:descriptors];*/
    
    assigneeArr = [_assignmentObj.assignedList componentsSeparatedByString:ASSIGNEE_SAPRATOR];
    
    [TSSingleton layerDrawForView:_topView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    _attachmentButton.frame = CGRectMake(DEVICE_SIZE.width - _attachmentButton.frame.size.width - 10, _topView.frame.origin.y + _topView.frame.size.height - _attachmentButton.frame.size.height/2 + 18, _attachmentButton.frame.size.height, _attachmentButton.frame.size.height);
    
    assignedViewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(assignedViewTapped)];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(descViewTapped)];
    
    [_descriptionTextView addGestureRecognizer:tapGesture];

    if (SINGLETON_INSTANCE.isPushNotification) {
        
        SINGLETON_INSTANCE.isPushNotification = NO;
        
        [self fetchPushNotificationAssignmentAPICall];
    }
    else{
        
        assignmentsObj = (Assignments *)self.assignmentObj;
        
        [self loadViewWithAssignment:assignmentsObj];
    }

    if (IS_IPHONE_4) {
        
        _dateView.frame = CGRectMake(0, DeviceHeight -75, DeviceWidth, 75);
        
        _assignedView.frame = CGRectMake(0, DeviceHeight-_dateView.frame.size.height-40, DeviceWidth, 40);
        
        if (assigneeArr.count == 0) {
            
            _descriptionTextView.frame = CGRectMake(self.descriptionTextView.frame.origin.x, self.descriptionTextView.frame.origin.y, self.descriptionTextView.frame.size.width, 120+_assignedView.frame.size.height);
        }
        else{
            
            _descriptionTextView.frame = CGRectMake(self.descriptionTextView.frame.origin.x, self.descriptionTextView.frame.origin.y, self.descriptionTextView.frame.size.width, 120);
        }
        
        _assignedDateLabel.frame = CGRectMake(_assignedDateLabel.frame.origin.x, 25, 45, 45);
        
        _dueDateLabel.frame = CGRectMake(_dueDateLabel.frame.origin.x, _assignedDateLabel.frame.origin.y, 45, 45);
        
        _assignedLbl.center = CGPointMake(_assignedDateLabel.center.x, 15);
        
        _dueLbl.center = CGPointMake(_dueDateLabel.center.x, 15);
        
        _progressView.frame = CGRectMake(_assignedDateLabel.frame.origin.x+_assignedDateLabel.frame.size.width, _assignedDateLabel.frame.origin.y+_assignedDateLabel.frame.size.height/2, _dueDateLabel.frame.origin.x -(_assignedDateLabel.frame.origin.x+_assignedDateLabel.frame.size.width), 2);
    }
    
    [TSSingleton layerDrawForView:_dateView position:LAYER_TOP color:[UIColor lightGrayColor]];
    
    _assignedDateLabel.layer.cornerRadius = _assignedDateLabel.frame.size.width/2;
    
    _assignedDateLabel.layer.masksToBounds = YES;
    
    _dueDateLabel.layer.cornerRadius = _dueDateLabel.frame.size.width/2;
    
    _dueDateLabel.layer.masksToBounds = YES;
    
    _progressView.frame = CGRectMake(_assignedDateLabel.center.x +_assignedDateLabel.frame.size.width/2, _assignedDateLabel.center.y, _progressView.frame.size.width, _progressView.frame.size.height);
    
    _dueDateLabel.frame = CGRectMake(_progressView.center.x +_progressView.frame.size.width/2, _assignedDateLabel.frame.origin.y, _dueDateLabel.frame.size.width, _dueDateLabel.frame.size.height);
    
    [TSSingleton layerDrawForView:_assignedView position:LAYER_TOP color:[UIColor lightGrayColor]];

//    _assignedView.center = CGPointMake(_dateView.center.x, _dateView.frame.origin.y-_assignedView.frame.size.height);
    
    _assignedLbl.center = CGPointMake(_assignedDateLabel.center.x, _assignedDateLabel.frame.origin.y-10);
    
    _dueLbl.center = CGPointMake(_dueDateLabel.center.x, _dueDateLabel.frame.origin.y-10);
}

#pragma mark - Helper methods

-(void) loadViewWithAssignment:(Assignments *)assignment{
    
    _assignedDateLabel.attributedText = [SINGLETON_INSTANCE getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:assignment.assignmentDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_assignedDateLabel];
    
    _dueDateLabel.attributedText = [SINGLETON_INSTANCE getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:assignment.dueDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_dueDateLabel];
    
    _descriptionTextView.text = [NSString stringWithFormat:@"\t%@",assignment.assignmentDescription];
    
    _assignmentRemainingCountLbl.attributedText = [self changeStringFormat:[TSSingleton dateCompareFromDate:assignment.assignmentDate toDate:assignment.dueDate]];
    
    //    if ([_assignmentRemainingCountLbl.text intValue]>1) {
    //
    //        _daysTextLabel.text = @"Days";
    //    }
    //    else{
    //
    //        _daysTextLabel.text = @"Day";
    //    }
    
    _assignedTeacherLabel.text = [NSString stringWithFormat:@"- by %@",assignment.approvedByName];
    
    _assingnmentTitleLbl.text = [NSString stringWithFormat:@"Title : %@",assignment.assignmentTitle];
    
    _subjectLbl.text = [NSString stringWithFormat:@"Subject : %@",assignment.subjectName];
    
    _typeLbl.text = [NSString stringWithFormat:@"Type : %@",assignment.assignmentTypeName];
    
    if ([assignment.isAttachment intValue] == 1) {
        
        [_attachmentButton setHidden:NO];
    }
    else{
        
        [_attachmentButton setHidden:YES];
    }
    
    _assignedView.hidden = NO;
    
    if (assigneeArr.count ==1) {
        
        _assignedTxtFld.text = [NSString stringWithFormat:@"%@ : %@",AssignmentAssigned,[assigneeArr objectAtIndex:0]];
        
    }else if (assigneeArr.count >1){
        
        [self.assignedView addGestureRecognizer:assignedViewTap];
        
        [SINGLETON_INSTANCE addRightViewToTextField:_assignedTxtFld withImageName:@"expand_arrow"];
    }
    else if (assigneeArr.count ==0){
        
        _assignedView.hidden = YES;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_ASSIGNMENTDESCRIPTION parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@ Detail",SINGLETON_INSTANCE.barTitle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)assignedViewTapped{
    
    AssignedListViewController *listVC = [[AssignedListViewController alloc]initWithNibName:@"AssignedListViewController" bundle:nil];
    
    [listVC setAssignedListArray:assigneeArr];
    
    [listVC setAssignedText:AssignmentAssigned];

    [listVC showInViewController:self];
    
}

- (IBAction)attachmentButtonTapped:(id)sender {
    
    NSString *attachmentURLString;
    
    if (assignmentsObj.attachment.length>0) {
        
        attachmentURL = [NSURL URLWithString:assignmentsObj.attachment];
        
        attachmentURLString = assignmentsObj.attachment;
        
        NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSString *attachmentFile = [documentsPath stringByAppendingPathComponent:[attachmentURL lastPathComponent]];
        
        NSLog(@"%@",attachmentFile);
        
        NSLog(@"%@",[attachmentURL lastPathComponent]);
        
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:attachmentFile];
        
        if (fileExists) {
            
            [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_ATTACHMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_OPENEDFROMLOCAL,ANALYTICS_PARAM_ATTACHMENTSTATUS, nil]];
            
            [self navigateToAttachmentViewControllerWithFileName:attachmentURL];
        }
        else{
            
            [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_ATTACHMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_DOWNLOADING,ANALYTICS_PARAM_ATTACHMENTSTATUS, nil]];
            
            [[TSHandlerClass sharedHandler]downloadAttachmentWithURL:attachmentURLString currentView:self.view withHandler:^(BOOL success, NSURL *filePath) {
                
                if (success) {
                    
                    [self navigateToAttachmentViewControllerWithFileName:filePath];
                }
                else{
                    
                    [TSSingleton showAlertWithMessage:@"Attachment might be have broken url"];
                }
            }];
        }
    }
    else{
        
        [TSSingleton showAlertWithMessage:TEXT_INVALID_ATTACHMENT];
    }
}

-(void) navigateToAttachmentViewControllerWithFileName:(NSURL *)fileURL{
    
    TSAttachmentViewController *attachmentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAttachment"];
    
    [attachmentVC setFileName:[fileURL lastPathComponent]];
    
    UINavigationController *navCtrl = [[UINavigationController alloc]initWithRootViewController:attachmentVC];
    
    navCtrl.navigationBar.translucent = NO;
    
    navCtrl.navigationBar.barTintColor = APPTHEME_COLOR;
    
    navCtrl.navigationBar.tintColor = [UIColor whiteColor];
    
    [navCtrl.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:APP_FONT size:18]}];
    
    [self.navigationController presentViewController:navCtrl animated:YES completion:nil];
}

- (IBAction)viewReceiverBtnClk:(id)sender {
    
    //[_assignedDetailsArray addObject:assignmentsObj];
    
    [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_VIEW_RECEIVER parameter:[NSMutableDictionary new]];
    
    if (assigneeArr.count > 0)
    {
        AssignedListViewController *listVC = [[AssignedListViewController alloc]initWithNibName:@"AssignedListViewController" bundle:nil];
        
        
        [listVC setAssignedListArray:assigneeArr];
        
        [listVC showInViewController:self];

    }
   
}

-(NSMutableAttributedString *) changeStringFormat:(NSString *)string{
    
    UIFont *daysCountFont = [UIFont fontWithName:APP_FONT size:40.0];
    
    NSDictionary *dateTextAttr = [NSDictionary dictionaryWithObject: daysCountFont forKey:NSFontAttributeName];
    
    NSMutableAttributedString *countAttrStr = [[NSMutableAttributedString alloc] initWithString:[[string componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""] attributes: dateTextAttr];
    
    UIFont *textFont = [UIFont fontWithName:APP_FONT size:13.0];
    
    NSDictionary *monthTextAttr = [NSDictionary dictionaryWithObject:textFont forKey:NSFontAttributeName];
    
    NSMutableAttributedString *textAttrStr = [[NSMutableAttributedString alloc]initWithString:[[string componentsSeparatedByCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]] componentsJoinedByString:@""] attributes:monthTextAttr];
    
    [countAttrStr appendAttributedString:[[NSAttributedString alloc]initWithString:@"\n"]];
    
    [countAttrStr appendAttributedString:textAttrStr];
    
    return countAttrStr;
}

#pragma mark - API Call

// Fetch Particular Assignment
-(void) fetchPushNotificationAssignmentAPICall{
    
    NSString *urlString = @"MastersMServices/AssignmentService.svc/FetchAssignmentsByTransID";
    
    NSDictionary *params = @{@"guidAssignmentTransID":SINGLETON_INSTANCE.pushNotificationCommTransID};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchAssignmentsByTransIDResult"][@"ResponseCode"] integerValue] == 1) {
                
                [[Assignments instance]storePushNotificationAssignmentWithDict:responseDictionary];
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transID MATCHES %@ AND  staffTransID MATCHES %@",SINGLETON_INSTANCE.pushNotificationCommTransID,SINGLETON_INSTANCE.staffTransId];
                
                NSArray *assignmentArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Assignments" withPredicate:predicate];
                
                if (assignmentArray.count>0) {
                    
                    Assignments *assignmentObj = [assignmentArray objectAtIndex:0];
                    
                    [self loadViewWithAssignment:assignmentObj];
                }
            }
            else{
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"FetchAssignmentsByTransIDResult"][@"ResponseMessage"]];
            }
        }
        else{
            
            [TSSingleton showAlertWithMessage:@"Timeout error. Please try again later"];
        }
    }];
}

-(void)descViewTapped{
    
    [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_DESCRIPTION parameter:[NSMutableDictionary new]];
    
    TSDescriptionViewController *desVC = [[TSDescriptionViewController alloc]initWithNibName:@"TSDescriptionViewController" bundle:nil];
    
    [desVC showInViewController:self descriptionString:_descriptionTextView.text];
    
}

@end
