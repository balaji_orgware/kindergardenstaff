//
//  Feedback+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/11/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Feedback.h"

NS_ASSUME_NONNULL_BEGIN

@interface Feedback (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *acadamicYearTransID;
@property (nullable, nonatomic, retain) NSString *classSection;
@property (nullable, nonatomic, retain) NSString *dateCreated;
@property (nullable, nonatomic, retain) NSString *feedbackDescription;
@property (nullable, nonatomic, retain) NSString *feedBackType;
@property (nullable, nonatomic, retain) NSString *feedBackTypeID;
@property (nullable, nonatomic, retain) NSString *parentMobileNo;
@property (nullable, nonatomic, retain) NSString *parentName;
@property (nullable, nonatomic, retain) NSString *studentName;
@property (nullable, nonatomic, retain) NSString *studentTransID;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *transID;
@property (nullable, nonatomic, retain) NSString *staffTransID;
@property (nullable, nonatomic, retain) StaffDetails *staff;

@end

NS_ASSUME_NONNULL_END
