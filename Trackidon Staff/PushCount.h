//
//  PushCount.h
//  Trackidon Staff
//
//  Created by Elango on 19/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StaffDetails;

NS_ASSUME_NONNULL_BEGIN

@interface PushCount : NSManagedObject

+ (instancetype)instance;

- (void)updatePushCountsFormenuId:(NSString *)menuId isForApproval:(BOOL)isForApproval;

- (void)savePushCountsWithDict:(NSDictionary *)dict;

-(void)saveUpdatedPushCountsWithDict:(NSDictionary *)dict isForApproval:(BOOL)isForApproval;

@end

NS_ASSUME_NONNULL_END

#import "PushCount+CoreDataProperties.h"
