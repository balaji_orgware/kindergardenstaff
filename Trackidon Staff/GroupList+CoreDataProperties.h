//
//  GroupList+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 08/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "GroupList.h"

NS_ASSUME_NONNULL_BEGIN

@interface GroupList (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *boardTransId;
@property (nullable, nonatomic, retain) NSString *boardName;
@property (nullable, nonatomic, retain) NSString *schoolTransId;
@property (nullable, nonatomic, retain) NSString *schoolName;
@property (nullable, nonatomic, retain) NSString *groupTransId;
@property (nullable, nonatomic, retain) NSString *groupName;
@property (nullable, nonatomic, retain) School *school;

@end

NS_ASSUME_NONNULL_END
