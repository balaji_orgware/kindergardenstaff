//
//  TSAttachmentViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/28/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSAttachmentViewController.h"

@interface TSAttachmentViewController ()

@end

@implementation TSAttachmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonAction)];
    
    self.navigationItem.rightBarButtonItem = doneButton;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask ,YES );
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:_fileName];
    
    _webView.scalesPageToFit = YES;
    
    NSURL *targetURL = [NSURL fileURLWithPath:filePath];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    
    [_webView loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_ATTACHMENTVIEWER parameter:[NSMutableDictionary new]];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Attachment";
}

-(void) doneButtonAction{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
