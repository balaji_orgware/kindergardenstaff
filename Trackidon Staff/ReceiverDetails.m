//
//  ReceiverDetails.m
//  Trackidon Staff
//
//  Created by Elango on 01/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "ReceiverDetails.h"
#import "StaffDetails.h"

@implementation ReceiverDetails

+ (instancetype)instance{
    
    static ReceiverDetails *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (&onceToken, ^{
        
        kSharedInstance = [[ReceiverDetails alloc]init];
        
    });
    
    return kSharedInstance;
}

- (void)saveReceiverDetailsWithResponseDict:(NSDictionary *)dict
{
    NSLog(@"%@",dict);
    
    @try {
        
        NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
        
        [SINGLETON_INSTANCE deleteAllObjectsInEntity:@"ReceiverDetails"];
        
        for (NSDictionary *tmpReceiverDict in dict[@"FetchCommonmasterResult"][@"MasterRefClass"]) {
            
            NSDictionary *receiverDict = [tmpReceiverDict dictionaryByReplacingNullsWithBlanks];
            
            ReceiverDetails *receiverDetail;
            
            //receiverDetail = [self checkAvailabilityWithReceiverTransId:receiverDict[@"TransID"] context:context];
            
            //if (receiverDetail == nil) {
                
                receiverDetail = [NSEntityDescription insertNewObjectForEntityForName:@"ReceiverDetails" inManagedObjectContext:context];
            //}
            
            receiverDetail.boardTransId = receiverDict[@"BoardTransID"];
            receiverDetail.boardName = receiverDict[@"BoardName"];
            receiverDetail.receiverTransId = receiverDict[@"TransID"];
            receiverDetail.receiverDescription = receiverDict[@"Description"];
            receiverDetail.receiverTypeId = [NSString stringWithFormat:@"%@", receiverDict[@"TypeID"]];
            receiverDetail.isSelected = [NSNumber numberWithBool:NO];
            receiverDetail.staff = SINGLETON_INSTANCE.staff;

        }
        
        NSError *error;
        
        if (![context save:&error]) {
            
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            
        }
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    }
}

-(ReceiverDetails *)checkAvailabilityWithReceiverTransId:(NSString *)receiverTransId context:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ReceiverDetails" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"receiverTransId contains[cd] %@", receiverTransId];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
    
}

@end