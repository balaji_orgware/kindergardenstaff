//
//  TSCreateGeneralCheckupDentalViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 11/08/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSCreateGeneralCheckupDentalViewController.h"

#define TEXT_Remarks @"Remarks"
#define IMG_CHECK [UIImage imageNamed:@"check"]
#define IMG_UNCHECK [UIImage imageNamed:@"uncheck"]

@interface TSCreateGeneralCheckupDentalViewController (){
    NSDate *selectedDate;
    CGRect viewRect, currentVisibleRect,scrollViewRect;
    UIView *currentResponder;
    CGRect textFldViewFrame;
    
    BOOL isRaisedScrollVw;
    
    BOOL isToothCavity,isPlaque,isGumBleeding,isGumInflammation,isStains,isTarter,isBadBreath,isSoftTissue;
}

@end

@implementation TSCreateGeneralCheckupDentalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    selectedDate = [NSDate date];
    
    isRaisedScrollVw = NO;
    
    _dateLabel.text = [SINGLETON_INSTANCE stringFromDate:selectedDate withFormate:DATE_FORMATE_DAY_MNTH_YR];
    
    [TSSingleton layerDrawForView:_examineDateView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_examinedByView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    UITapGestureRecognizer *examineDateTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(examineDateViewTapped)];
    [_examineDateView addGestureRecognizer:examineDateTap];
    
    if (_healthType == HealthType_GeneralCheckup) {
        
        [_generalCheckupView setHidden:NO];
        [_dentalView setHidden:YES];
        
        [TSSingleton layerDrawForView:_neckInfoView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        [TSSingleton layerDrawForView:_handsInfoView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        [TSSingleton layerDrawForView:_legInfoView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        [TSSingleton layerDrawForView:_abdomenInfoView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        [TSSingleton layerDrawForView:_respiratoryInfoView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        [TSSingleton layerDrawForView:_anemiaInfoView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        [TSSingleton layerDrawForView:_allergyInfoView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        [TSSingleton layerDrawForView:_cardioVascularInfoView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        [TSSingleton layerDrawForView:_nervesystemInfoView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        
        [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width, _generalCheckupView.frame.origin.y+_generalCheckupView.frame.size.height)];
    }
    else if(_healthType == HealthType_Dental){
        
        [_generalCheckupView setHidden:YES];
        [_dentalView setHidden:NO];

        isToothCavity = isPlaque = isGumBleeding = isGumInflammation = isStains = isTarter = isBadBreath = isSoftTissue = NO;

        [_toothCavityTickImgVw setImage:IMG_UNCHECK];
        [_plaqueTickImgView setImage:IMG_UNCHECK];
        [_gumBleedingTickImgVw setImage:IMG_UNCHECK];
        [_gumInflammationTickImgVw setImage:IMG_UNCHECK];
        [_stainsTickImgVw setImage:IMG_UNCHECK];
        [_tarterTickImgVw setImage:IMG_UNCHECK];
        [_badbreathTickImgVw setImage:IMG_UNCHECK];
        [_softTissueTickImgVw setImage:IMG_UNCHECK];
    
        [TSSingleton layerDrawForView:_intralOralInfoView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        [TSSingleton layerDrawForView:_extraOralInfoView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        [TSSingleton layerDrawForView:_tickView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        
        _remarksTxtVw.text = TEXT_Remarks;
        _remarksTxtVw.textColor = APP_PLACEHOLDER_COLOR;
        _remarksTxtVw.font = [UIFont fontWithName:APP_FONT size:16];
        _remarksTxtVw.textContainerInset = UIEdgeInsetsMake(15,10,10,15); // top, left, bottom, right
        
        [SINGLETON_INSTANCE addDoneButtonForTextView:_remarksTxtVw];
        
        [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width, _dentalView.frame.origin.y+_dentalView.frame.size.height)];
        
        UITapGestureRecognizer *toothCavityTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(toothCavityViewTapped)];
        [_toothCavityView addGestureRecognizer:toothCavityTap];
        
        UITapGestureRecognizer *plaqueTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(plaqueViewTapped)];
        [_plaqueView addGestureRecognizer:plaqueTap];
        
        UITapGestureRecognizer *gumBleedingTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gumBleedingViewTapped)];
        [_gumBleedingView addGestureRecognizer:gumBleedingTap];
        
        UITapGestureRecognizer *gumInflammationTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gumInflammationViewTapped)];
        [_gumInflammationView addGestureRecognizer:gumInflammationTap];
        
        UITapGestureRecognizer *stainsTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(stainsViewTapped)];
        [_stainsInfoView addGestureRecognizer:stainsTap];
        
        UITapGestureRecognizer *tarterTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tarterViewTapped)];
        [_tarterInfoView addGestureRecognizer:tarterTap];
        
        UITapGestureRecognizer *badbreathTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(badBreathViewTapped)];
        [_badbreathInfoView addGestureRecognizer:badbreathTap];
        
        UITapGestureRecognizer *softTissueTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(softTissueViewTapped)];
        [_softTissueInfoView addGestureRecognizer:softTissueTap];
    }
    
    viewRect = self.view.frame;
    scrollViewRect = _scrollView.frame;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if (_healthType == HealthType_Dental) {
        self.navigationItem.title = @"Dental Information";
    }
    else{
        self.navigationItem.title = @"General checkup";
    }
    self.navigationController.navigationBar.topItem.title = @"";
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Alert View Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1000) {
     
        if (buttonIndex) {
         
            NSDictionary *param;
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setValue:[_generalInfoDict objectForKey:@"HCTransID"] forKey:@"HCTransID"];
            [dict setValue:SINGLETON_INSTANCE.staffTransId forKey:@"ExaminerID"];
            [dict setValue:_examinedByTxtFld.text forKey:@"ExaminedBy"];
            [dict setValue:[SINGLETON_INSTANCE stringFromDate:selectedDate withFormate:@"dd-MMMM-yyyy"] forKey:@"ExaminedDate"];
            [dict setValue:SINGLETON_INSTANCE.staff.mobileNo forKey:@"Mobileno"];
            [dict setValue:SINGLETON_INSTANCE.staffTransId forKey:@"UserTransID"];
            [dict setValue:@"" forKey:@"AddlnRemarks"];

            if (_healthType == HealthType_GeneralCheckup) {
                
                [dict setValue:_neckInfoTxtFld.text forKey:@"NeckRemarks"];
                [dict setValue:_handsInfoTxtFld.text forKey:@"HandsRemarks"];
                [dict setValue:_legInfoTxtFld.text forKey:@"LegsRemarks"];
                [dict setValue:_abdomenInfoTxtFld.text forKey:@"Abdomen"];
                [dict setValue:_respiratoryInfoTxtFld.text forKey:@"Respiratary"];
                [dict setValue:_anemiaInfoTxtFld.text forKey:@"Anemia"];
                [dict setValue:_allergyInfoTxtFld.text forKey:@"Allergy"];
                [dict setValue:_nerveSystemInfoTxtFld.text forKey:@"NerveSystem"];
                [dict setValue:_cardioVascularInfoTxtFld.text forKey:@"CardioVascular"];
                
                param = @{@"GeneralCheckUp":dict};
            }
            else if(_healthType == HealthType_Dental){
        
                [dict setValue:_intraOralTxtFld.text forKey:@"IntraOral"];
                [dict setValue:_extraOralTxtFld.text forKey:@"ExtraOral"];
                [dict setValue:isToothCavity ? @"true":@"false" forKey:@"ToothCavity"];
                [dict setValue:isPlaque ? @"true":@"false" forKey:@"Plaque"];
                [dict setValue:isGumBleeding ? @"true":@"false" forKey:@"GumBleeding"];
                [dict setValue:isGumInflammation ? @"true":@"false" forKey:@"GumInflammation"];
                [dict setValue:isStains ? @"true":@"false" forKey:@"Stains"];
                [dict setValue:isTarter ? @"true":@"false" forKey:@"Tarter"];
                [dict setValue:isBadBreath ? @"true":@"false" forKey:@"BadBreath"];
                [dict setValue:isSoftTissue ? @"true":@"false" forKey:@"SoftTissue"];
                [dict setValue:_remarksTxtVw.text forKey:@"Remarks"];
                
                param = @{@"DentalInfo":dict};
            }
            
            [self updateServiceCall:param];
        }
        else{
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        }
    }
}

#pragma mark - Button Actions

- (IBAction)doneButtonTapped:(id)sender {
    
    if (_examinedByTxtFld.text.length == 0) {
        [TSSingleton showAlertWithMessage:@"Please enter Examiner Name"];
        return;
    }
    else{
        if (_healthType == HealthType_GeneralCheckup) {
          
            _neckInfoTxtFld.text = [_neckInfoTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            _handsInfoTxtFld.text = [_handsInfoTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            _legInfoTxtFld.text = [_legInfoTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            _abdomenInfoTxtFld.text = [_abdomenInfoTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            _respiratoryInfoTxtFld.text = [_respiratoryInfoTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            _anemiaInfoTxtFld.text = [_anemiaInfoTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            _allergyInfoTxtFld.text = [_allergyInfoTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            _nerveSystemInfoTxtFld.text = [_nerveSystemInfoTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            _cardioVascularInfoTxtFld.text = [_cardioVascularInfoTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            if (_neckInfoTxtFld.text.length == 0) {
                [TSSingleton showAlertWithMessage:@"Please enter Neck info"];
                return;
            }
            else if (_handsInfoTxtFld.text.length == 0){
                [TSSingleton showAlertWithMessage:@"Please enter Hands info"];
                return;
            }
            else if (_legInfoTxtFld.text.length == 0){
                [TSSingleton showAlertWithMessage:@"Please enter Leg info"];
                return;
            }
            else if (_abdomenInfoTxtFld.text.length == 0){
                [TSSingleton showAlertWithMessage:@"Please enter Abdomen info"];
                return;
            }
            else if (_respiratoryInfoTxtFld.text.length == 0){
                [TSSingleton showAlertWithMessage:@"Please enter Respiratory info"];
                return;
            }
            else if (_anemiaInfoTxtFld.text.length == 0){
                [TSSingleton showAlertWithMessage:@"Please enter Anemia info"];
                return;
            }
            else if (_allergyInfoTxtFld.text.length == 0){
                [TSSingleton showAlertWithMessage:@"Please enter Allergy info"];
                return;
            }
            else if (_nerveSystemInfoTxtFld.text.length == 0){
                [TSSingleton showAlertWithMessage:@"Please enter Nerve system info"];
                return;
            }
            else if (_cardioVascularInfoTxtFld.text.length == 0){
                [TSSingleton showAlertWithMessage:@"Please enter Cardio Vascular info"];
                return;
            }
            else if (_neckInfoTxtFld.text.length >500){
                [TSSingleton showAlertWithMessage:@"Neck Remarks should be less than 500 characters"];
                return;
            }
            else if (_handsInfoTxtFld.text.length >500){
                [TSSingleton showAlertWithMessage:@"Hands Remarks should be less than 500 characters"];
                return;
            }
            else if (_legInfoTxtFld.text.length >500){
                [TSSingleton showAlertWithMessage:@"Leg Remarks should be less than 500 characters"];
                return;
            }
            else if (_respiratoryInfoTxtFld.text.length >100){
                [TSSingleton showAlertWithMessage:@"Respiratory info should be less than 100 characters"];
                return;
            }
            else if (_anemiaInfoTxtFld.text.length >100){
                [TSSingleton showAlertWithMessage:@"Anemia info should be less than 100 characters"];
                return;
            }
            else if (_allergyInfoTxtFld.text.length >100){
                [TSSingleton showAlertWithMessage:@"Allergy info should be less than 100 characters"];
                return;
            }
            else if (_nerveSystemInfoTxtFld.text.length >100){
                [TSSingleton showAlertWithMessage:@"Nerve system info should be less than 100 characters"];
                return;
            }
            else if (_cardioVascularInfoTxtFld.text.length >100){
                [TSSingleton showAlertWithMessage:@"Cardio Vascular info should be less than 100 characters"];
                return;
            }
            else if (_abdomenInfoTxtFld.text.length >250){
                [TSSingleton showAlertWithMessage:@"Abdomen info should be less than 250 characters"];
                return;
            }
        }
        else if(_healthType == HealthType_Dental) {
            
            _intraOralTxtFld.text = [_intraOralTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            _extraOralTxtFld.text = [_extraOralTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            if (_intraOralTxtFld.text.length == 0) {
                [TSSingleton showAlertWithMessage:@"Please enter Intra Oral info"];
                return;
            }
            else if(_intraOralTxtFld.text.length >250){
                [TSSingleton showAlertWithMessage:@"Intra Oral Info should be less than 250 characters"];
                return;
            }
            else if (_extraOralTxtFld.text.length == 0){
                [TSSingleton showAlertWithMessage:@"Please enter Extra Oral info"];
                return;
            }
            else if (_extraOralTxtFld.text.length >250){
                [TSSingleton showAlertWithMessage:@"Extra Oral info should be less than 250 characters"];
                return;
            }
        }
    }

    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Are you sure want to update data ?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alertView setTag:1000];
    [alertView show];
}

-(void) examineDateViewTapped{
    
    TSDatePickerViewController *datePickerVC = [[TSDatePickerViewController alloc]initWithNibName:@"TSDatePickerViewController" bundle:nil];
    
    datePickerVC.delegate = self;
    
    [datePickerVC showInViewController:self calendarType:CALENDAR_TYPE_DATE withMinDate:nil withMaxDate:[NSDate date]];
}

-(void) toothCavityViewTapped{
    
    if (isToothCavity)
        [_toothCavityTickImgVw setImage:IMG_UNCHECK];
    else
        [_toothCavityTickImgVw setImage:IMG_CHECK];
    
    isToothCavity = !isToothCavity;
}

-(void) plaqueViewTapped{
    
    if (isPlaque)
        [_plaqueTickImgView setImage:IMG_UNCHECK];
    else
        [_plaqueTickImgView setImage:IMG_CHECK];
    
    isPlaque = !isPlaque;
}

-(void) gumBleedingViewTapped{
    
    if (isGumBleeding)
        [_gumBleedingTickImgVw setImage:IMG_UNCHECK];
    else
        [_gumBleedingTickImgVw setImage:IMG_CHECK];
    
    isGumBleeding = !isGumBleeding;
}

-(void) gumInflammationViewTapped{
    
    if (isGumInflammation)
        [_gumInflammationTickImgVw setImage:IMG_UNCHECK];
    else
        [_gumInflammationTickImgVw setImage:IMG_CHECK];
    
    isGumInflammation = !isGumInflammation;
}

-(void) stainsViewTapped{
    
    if (isStains)
        [_stainsTickImgVw setImage:IMG_UNCHECK];
    else
        [_stainsTickImgVw setImage:IMG_CHECK];
    
    isStains = !isStains;
}

-(void) tarterViewTapped{
    
    if (isTarter)
        [_tarterTickImgVw setImage:IMG_UNCHECK];
    else
        [_tarterTickImgVw setImage:IMG_CHECK];
    
    isTarter = !isTarter;
}

-(void) badBreathViewTapped{
    
    if (isBadBreath)
       [_badbreathTickImgVw setImage:IMG_UNCHECK];
    else
        [_badbreathTickImgVw setImage:IMG_CHECK];
    
    isBadBreath = !isBadBreath;
}

-(void) softTissueViewTapped{
    
    if (isSoftTissue)
        [_softTissueTickImgVw setImage:IMG_UNCHECK];
    else
        [_softTissueTickImgVw setImage:IMG_CHECK];
    
    isSoftTissue = !isSoftTissue;
}

#pragma mark - Helper Methods

-(void) keyboardShown:(NSNotification *)notification{
    
    NSDictionary* info = [notification userInfo];
    
    CGSize currentKeyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    if (_healthType == HealthType_GeneralCheckup) {
        
        if (currentResponder == _neckInfoTxtFld) {
            textFldViewFrame = _neckInfoView.frame;
        }
        else if (currentResponder == _handsInfoTxtFld){
            textFldViewFrame = _handsInfoView.frame;
        }
        else if (currentResponder == _legInfoTxtFld){
            textFldViewFrame = _legInfoView.frame;
        }
        else if (currentResponder == _abdomenInfoTxtFld){
            textFldViewFrame = _abdomenInfoView.frame;
        }
        else if (currentResponder == _respiratoryInfoTxtFld){
            textFldViewFrame = _respiratoryInfoView.frame;
        }
        else if (currentResponder == _anemiaInfoTxtFld){
            textFldViewFrame = _anemiaInfoView.frame;
        }
        else if (currentResponder == _allergyInfoTxtFld){
            textFldViewFrame = _allergyInfoView.frame;
        }
        else if (currentResponder == _nerveSystemInfoTxtFld){
            textFldViewFrame = _nervesystemInfoView.frame;
        }
        else if (currentResponder == _cardioVascularInfoTxtFld){
            textFldViewFrame = _cardioVascularInfoView.frame;
        }
    }
    else{
        
        if (currentResponder == _intraOralTxtFld) {
            textFldViewFrame = _intralOralInfoView.frame;
        }
        else if (currentResponder == _extraOralTxtFld){
            textFldViewFrame = _extraOralInfoView.frame;
        }
    }
    
    CGPoint textFieldOrigin = CGPointMake(currentResponder.frame.origin.x, currentResponder.frame.origin.y+textFldViewFrame.origin.y+_dentalView.frame.origin.y);
    textFieldOrigin.y = textFieldOrigin.y + currentResponder.frame.size.height;
    CGFloat textFieldHeight = currentResponder.frame.size.height;
    
    CGRect visibleRect = viewRect;
    visibleRect.size.height -= currentKeyboardSize.height + textFieldHeight;
    currentVisibleRect = visibleRect;
    
    isRaisedScrollVw = NO;
    
    if ([_remarksTxtVw isFirstResponder]) {
        
        CGRect descRect = viewRect;
        
        descRect.size.height -= currentKeyboardSize.height;
        
        _remarksTxtVw.frame = CGRectMake(_remarksTxtVw.frame.origin.x, _remarksTxtVw.frame.origin.y, _remarksTxtVw.frame.size.width, descRect.size.height - 64 );
        
        _dentalView.frame = CGRectMake(_dentalView.frame.origin.x, _dentalView.frame.origin.y, _dentalView.frame.size.width, _tickView.frame.origin.y+_tickView.frame.size.height+_remarksTxtVw.frame.size.height);
        
        _scrollView.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, _dentalView.frame.origin.y+_dentalView.frame.size.height);
        
        CGPoint scrollPoint = CGPointMake(0.0, _remarksTxtVw.frame.origin.y+_examineDateView.frame.size.height+_examinedByView.frame.size.height);
        
        [_scrollView setContentOffset:scrollPoint animated:YES];
        
        [_scrollView setScrollEnabled:NO];
    }
    else if (!CGRectContainsPoint(visibleRect, textFieldOrigin)){
        
        isRaisedScrollVw = YES;
        
        CGPoint scrollPoint = CGPointMake(0.0, textFieldOrigin.y - visibleRect.size.height  + textFieldHeight);
        
        [_scrollView setContentOffset:scrollPoint animated:YES];
    }
}

-(void) keyboardHide:(NSNotification *)notification{
    
    [_scrollView setScrollEnabled:YES];
    
//    if (isRaisedScrollVw)
//        [_scrollView setContentOffset:CGPointMake(_scrollView.contentOffset.x, currentResponder.frame.origin.y+textFldViewFrame.origin.y)];
//    else
        [_scrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - Date Picker Delegate

-(void)selectedDate:(NSDate *)date{
    
    selectedDate = date;
    
    _dateLabel.text = [SINGLETON_INSTANCE stringFromDate:selectedDate withFormate:DATE_FORMATE_DAY_MNTH_YR];
}

#pragma mark - TextField Delegates

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    currentResponder = textField;
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - TextView Delegates

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{

    currentResponder = textView;
    
    if ([textView.text isEqualToString:TEXT_Remarks]) {
        
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
        textView.font = [UIFont fontWithName:APP_FONT size:14];
    }
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {
        
        textView.text = TEXT_Remarks;
        textView.textColor = APP_PLACEHOLDER_COLOR;
        textView.font = [UIFont fontWithName:APP_FONT size:17];
    }
    
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    
    if (newFrame.size.height >50) {
        textView.frame = newFrame;
    }
    else{
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), 50);
        textView.frame = newFrame;
    }
    
    _dentalView.frame = CGRectMake(_dentalView.frame.origin.x, _dentalView.frame.origin.y, _dentalView.frame.size.width, _tickView.frame.origin.y+_tickView.frame.size.height+_remarksTxtVw.frame.size.height);
    
    [_scrollView setFrame:CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, DeviceHeight - _doneView.frame.size.height - 64)];

    [_scrollView setContentSize:CGSizeMake(DeviceWidth,_dentalView.frame.origin.y+_dentalView.frame.size.height)];
    
    [_scrollView setContentOffset:CGPointZero animated:YES];
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        _dentalView.frame = CGRectMake(_dentalView.frame.origin.x, _dentalView.frame.origin.y, _dentalView.frame.size.width, _tickView.frame.origin.y+_tickView.frame.size.height+_remarksTxtVw.frame.size.height);
//        [_scrollView setContentSize:CGSizeMake(DeviceWidth,_dentalView.frame.origin.y+_dentalView.frame.size.height)];
//    });
}

#pragma mark - Service Call

-(void) updateServiceCall : (NSDictionary *)param{
    
    NSString *urlString = @"HealthDetails/Services/HealthCardService.svc/UpdateHealthCardInfo";
    
    NSDictionary *params = @{@"ObjUpdateHealthCard":param};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"UpdateHealthCardInfoResult"][@"ResponseCode"] integerValue] == 1) {
                
                SINGLETON_INSTANCE.needRefreshHealthRecords = YES;
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"UpdateHealthCardInfoResult"][@"ResponseMessage"]];
                
                NSArray *viewControllers = [self.navigationController viewControllers];
                
                NSUInteger currentIndex = [viewControllers indexOfObject:self];
                
                [self.navigationController popToViewController:[viewControllers objectAtIndex:currentIndex-2] animated:YES];
            }
            else{
                SINGLETON_INSTANCE.needRefreshHealthRecords = NO;
            }
        }
        else{
            SINGLETON_INSTANCE.needRefreshHealthRecords = NO;
        }
    }];
}

@end