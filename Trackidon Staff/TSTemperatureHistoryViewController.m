//
//  TSTemperatureHistoryViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 13/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSTemperatureHistoryViewController.h"

#define COLOR_FORENOON  PNGreen
#define COLOR_AFTERNOON  PNBlue

@interface TSTemperatureHistoryViewController ()

@end

@implementation TSTemperatureHistoryViewController
{
    CURRENT_VIEW currentVw;
    
    NSMutableArray *temperetureRecordsArr,*studTempRecordsArr;
    
    NSDate *firstDateOfMonth;
    NSDate *firstDateOfWeek;
    NSDate *lastDateOfWeek;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [TSSingleton layerDrawForView:_calanderVw position:LAYER_TOP color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_calanderVw position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_studentNameVw position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    _calendarManager = [JTCalendarManager new];
    
    temperetureRecordsArr = [[NSMutableArray alloc] init];
    studTempRecordsArr = [NSMutableArray new];
    
    firstDateOfMonth = [_calendarManager.dateHelper firstDayOfMonth:[NSDate date]];
    
    firstDateOfWeek = [_calendarManager.dateHelper firstWeekDayOfWeek:[NSDate date]];
    
    lastDateOfWeek = [firstDateOfWeek dateByAddingTimeInterval:60*60*24*6];
    
    [self fetchTemperatureDetailsWithStartDate:[SINGLETON_INSTANCE stringFromDate:firstDateOfWeek withFormate:DATE_FORMATE_SERVICE_CALL] andEndDate:[SINGLETON_INSTANCE stringFromDate:lastDateOfWeek withFormate:DATE_FORMATE_SERVICE_CALL]];
    
    _studentNameLbl.text = _studentObj.studentName;
    
    if (IS_IPHONE_4)
        _noDataLbl.frame = CGRectMake(_noDataLbl.frame.origin.x, _noDataLbl.frame.origin.y + 50, _noDataLbl.frame.size.width, _noDataLbl.frame.size.height);
    
    // Swipe Left
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [_contentVw addGestureRecognizer:swipeleft];
    
    // SwipeRight
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [_contentVw addGestureRecognizer:swiperight];
    
    [self onDayViewTap:nil];
    
    [_foorenoonLegendVw setBackgroundColor:COLOR_FORENOON];
    [_afternoonLgendVw setBackgroundColor:COLOR_AFTERNOON];
    
    _foorenoonLegendVw.layer.cornerRadius = _foorenoonLegendVw.frame.size.height/2;
    _afternoonLgendVw.layer.cornerRadius = _afternoonLgendVw.frame.size.height/2;
    
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        for (UIView *view2 in view.subviews) {
            if ([view2 isKindOfClass:[UIImageView class]]) {
                [view2 removeFromSuperview];
            }
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Temperature History";
}

- (void)didReceiveMemoryWarning {    
    [super didReceiveMemoryWarning];
}

#pragma mark - Methods

- (IBAction)onDayViewTap:(id)sender {
    
    currentVw = DAY_VIEW;
    
    [self fetchDataForViewType:currentVw];
    
    [UIView animateWithDuration:0.2 animations:^{
        
        _lineVw.frame = CGRectMake(_dayBtn.frame.origin.x, _lineVw.frame.origin.y, _lineVw.frame.size.width, _lineVw.frame.size.height);
    }];
}

- (IBAction)onWeeaklyViewTap:(id)sender {
    
    currentVw = WEEKLY_VIEW;
    
    [self fetchDataForViewType:currentVw];
    
    [UIView animateWithDuration:0.2 animations:^{
        
        _lineVw.frame = CGRectMake(_weeakBtn.frame.origin.x, _lineVw.frame.origin.y, _lineVw.frame.size.width, _lineVw.frame.size.height);
        
    }];
    
}

- (IBAction)onMonthlyViewTap:(id)sender {
    
    currentVw = MONTHLY_VIEW;
    
    [self fetchDataForViewType:currentVw];
    
    [UIView animateWithDuration:0.2 animations:^{
        
        _lineVw.frame = CGRectMake(_monthBtn.frame.origin.x, _lineVw.frame.origin.y, _lineVw.frame.size.width, _lineVw.frame.size.height);
        
    }];
}

- (void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if (currentVw == DAY_VIEW)
        //Weekly
        [self onWeeaklyViewTap:nil];
    
    else if (currentVw == WEEKLY_VIEW)
        //Monthly
        [self onMonthlyViewTap:nil];
}

- (void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if (currentVw == MONTHLY_VIEW)
        //Weekly
        [self onWeeaklyViewTap:nil];
    
    else if (currentVw == WEEKLY_VIEW)
        //DayView
        [self onDayViewTap:nil];
}

- (void)fetchDataForViewType:(CURRENT_VIEW) viewType{
    
    [_scrollVw setContentOffset:CGPointZero];
    [_scrollVw setContentSize:CGSizeMake(DEVICE_SIZE.width,DEVICE_SIZE.height - 300)];
    
    if (viewType == DAY_VIEW) {     //Day View
        
        _dateLbl.text = [SINGLETON_INSTANCE stringFromDate:[NSDate date] withFormate:DATE_FORMATE_MNTH_DAY_YR];
        
        [temperetureRecordsArr removeAllObjects];
        
        NSPredicate *dayPred = [NSPredicate predicateWithFormat:@"takenDate == %@",[SINGLETON_INSTANCE dateFromDate:[NSDate date] withFormate:DATE_FORMATE_SERVICE_RESPONSE]];
        
        temperetureRecordsArr = [NSMutableArray arrayWithArray: [studTempRecordsArr filteredArrayUsingPredicate:dayPred]];
        
        NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                                  initWithKey:@"takenDate" ascending:YES];
        
        [temperetureRecordsArr sortUsingDescriptors:@[sort]];
        
        if (temperetureRecordsArr.count > 0) {
            
            _weeakMnthVw.hidden = YES;
            _dayVw.hidden = NO;
            
            static NSNumberFormatter *barChartFormatter;
            if (!barChartFormatter){
                barChartFormatter = [[NSNumberFormatter alloc] init];
                barChartFormatter.numberStyle = kCFNumberFormatterNoStyle;
                barChartFormatter.allowsFloats = YES;
                barChartFormatter.maximumFractionDigits = 0;
            }
            
            self.dayBarChart.backgroundColor = [UIColor clearColor];
            self.dayBarChart.yLabelFormatter = ^(CGFloat yValue){
                return [barChartFormatter stringFromNumber:[NSNumber numberWithFloat:yValue]];
            };
            
            self.dayBarChart.chartMarginLeft = 30.0;
            self.dayBarChart.chartMarginRight = 10.0;
            self.dayBarChart.chartMarginTop = 5.0;
            self.dayBarChart.chartMarginBottom = 10.0;
            self.dayBarChart.yChartLabelWidth = 20.0;
            self.dayBarChart.labelMarginTop = 5.0;
            self.dayBarChart.barWidth = 30.0;
            
            self.dayBarChart.showChartBorder = YES;
            self.dayBarChart.barBackgroundColor = [UIColor clearColor];
            
            //Label
            self.dayBarChart.labelFont = [UIFont fontWithName:APP_FONT size:14.0];
            self.dayBarChart.labelTextColor = APP_BLACK_COLOR;
            self.dayBarChart.yLabelSuffix = @"°";
            self.dayBarChart.yLabelSum = 5;
            
            [self.dayBarChart setXLabels:@[@"Forenoon",@"Afternoon"]];
            
            TemperatureStudModel *tempRecord = [temperetureRecordsArr lastObject];
            
            [self.dayBarChart setYValues:@[tempRecord.morningTemperature,tempRecord.eveningTemperature]];
            
            _dayForenoonTempLbl.text = tempRecord.morningTemperature;
            _dayAfternoonTempLbl.text = tempRecord.eveningTemperature;
            
            [self.dayBarChart setStrokeColors:@[COLOR_FORENOON,COLOR_AFTERNOON]];
            
            self.dayBarChart.isShowNumbers = NO;
            self.dayBarChart.isGradientShow = NO;
            
            [self.dayBarChart strokeChart];
            
            if (IS_IPHONE_4 || IS_IPHONE_5) {
                
                _dayRecordVw.frame = CGRectMake(_dayRecordVw.frame.origin.x, _dayBarChart.frame.origin.x + _dayBarChart.frame.size.height + 15, _dayRecordVw.frame.size.width, _dayRecordVw.frame.size.height);
                
            }
            
            [_scrollVw setContentSize:CGSizeMake(_dayRecordVw.frame.size.width, _dayRecordVw.frame.origin.y + _dayRecordVw.frame.size.height)];
            
        } else {
            _weeakMnthVw.hidden = YES;
            _dayVw.hidden = YES;
        }
        
    }else if (viewType == WEEKLY_VIEW) {    //Weekly View
        
        _dateLbl.text = [NSString stringWithFormat:@"%@ - %@",[SINGLETON_INSTANCE stringFromDate:firstDateOfWeek withFormate:@"MMMM dd"], [SINGLETON_INSTANCE stringFromDate:lastDateOfWeek withFormate:@"MMMM dd,YYYY"]];
        
        [temperetureRecordsArr removeAllObjects];
        
        NSPredicate *weeklyPred = [NSPredicate predicateWithFormat:@"takenDate >= %@ && takenDate <= %@",[SINGLETON_INSTANCE dateFromDate:firstDateOfWeek withFormate:DATE_FORMATE_SERVICE_RESPONSE],[SINGLETON_INSTANCE dateFromDate:lastDateOfWeek withFormate:DATE_FORMATE_SERVICE_RESPONSE]];
        
        temperetureRecordsArr = [NSMutableArray arrayWithArray: [studTempRecordsArr filteredArrayUsingPredicate:weeklyPred]];
        
        NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                                  initWithKey:@"takenDate" ascending:YES];
        
        [temperetureRecordsArr sortUsingDescriptors:@[sort]];
        
        if (temperetureRecordsArr.count > 0) {
            
            _weeakMnthVw.hidden = NO;
            _dayVw.hidden = YES;
            
            //Label
            _lineChart.xLabelFont = [UIFont fontWithName:APP_FONT size:12.0];
            _lineChart.xLabelColor = APP_BLACK_COLOR;
            _lineChart.yLabelFont = [UIFont fontWithName:APP_FONT size:14.0];
            _lineChart.yLabelColor = APP_BLACK_COLOR;
            _lineChart.yLabelNum = 5;
            _lineChart.yLabelHeight = 30;
            _lineChart.yUnit = @"°";
            
            //_lineChart.showYGridLines = YES;
            _lineChart.showCoordinateAxis = YES;
            _lineChart.axisColor = PNLightGrey;
            
            NSMutableArray * data01Array = [NSMutableArray new]; //= @[@60.1, @160.1, @126.4, @262.2, @186.2];
            NSMutableArray * data02Array = [NSMutableArray new]; //@[@20.1, @180.1, @26.4, @202.2, @126.2];
            NSMutableArray *xLblsArr = [NSMutableArray new];
            
            for (TemperatureStudModel *tempRecord in temperetureRecordsArr) {
                [data01Array addObject:
                 [tempRecord.morningTemperature stringByReplacingOccurrencesOfString:@"C" withString:@""]];
                [data02Array addObject:[tempRecord.eveningTemperature stringByReplacingOccurrencesOfString:@"C" withString:@""]];
                
                [xLblsArr addObject:[SINGLETON_INSTANCE stringFromDate:tempRecord.takenDate withFormate:@"EEE"]];
            }
            
            [_lineChart setXLabels:xLblsArr];
            
            // Line Chart No.1
            PNLineChartData *data01 = [PNLineChartData new];
            data01.color = COLOR_FORENOON;
            data01.itemCount = _lineChart.xLabels.count;
            data01.getData = ^(NSUInteger index) {
                CGFloat yValue = [data01Array[index] floatValue];
                return [PNLineChartDataItem dataItemWithY:yValue];
            };
            
            // Line Chart No 2
            PNLineChartData *data02 = [PNLineChartData new];
            data02.color = COLOR_AFTERNOON;
            data02.itemCount = _lineChart.xLabels.count;
            data02.getData = ^(NSUInteger index) {
                CGFloat yValue = [data02Array[index] floatValue];
                return [PNLineChartDataItem dataItemWithY:yValue];
            };
            
            data01.inflexionPointStyle = PNLineChartPointStyleCircle;
            data02.inflexionPointStyle = PNLineChartPointStyleCircle;
            
            
            _lineChart.chartData = @[data01, data02];
            [_lineChart strokeChart];
            //Tbl View Fix
            [temperetureRecordsArr addObject:@"Fix"];
            [_tblVw reloadData];
            
            data01Array = nil;
            data02Array = nil;
            xLblsArr = nil;
            
            _tblVw.frame = CGRectMake(_tblVw.frame.origin.x, _foorenoonLegendVw.frame.origin.y + _foorenoonLegendVw.frame.size.height + 15, _tblVw.frame.size.width, 50*temperetureRecordsArr.count);
            
            [_scrollVw setContentSize:CGSizeMake(_tblVw.frame.size.width, _tblVw.frame.origin.y + _tblVw.frame.size.height)];
            
        } else {
            
            _weeakMnthVw.hidden = YES;
            _dayVw.hidden = YES;
            
            
        }
    } else if (viewType == MONTHLY_VIEW){   //Monthly View
        
        _dateLbl.text = [SINGLETON_INSTANCE stringFromDate:[NSDate date] withFormate:@"MMMM"];
        
        [temperetureRecordsArr removeAllObjects];
        
        NSPredicate *monthlyPred = [NSPredicate predicateWithFormat:@"takenDate >= %@ && takenDate <= %@",[SINGLETON_INSTANCE dateFromDate:firstDateOfMonth withFormate:DATE_FORMATE_SERVICE_RESPONSE],[SINGLETON_INSTANCE dateFromDate:[NSDate date] withFormate:DATE_FORMATE_SERVICE_RESPONSE]];
        
        temperetureRecordsArr = [NSMutableArray arrayWithArray:[studTempRecordsArr filteredArrayUsingPredicate:monthlyPred]];
        
        NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                                  initWithKey:@"takenDate" ascending:YES];
        
        [temperetureRecordsArr sortUsingDescriptors:@[sort]];
        
        if (temperetureRecordsArr.count > 0) {
            
            _weeakMnthVw.hidden = NO;
            _dayVw.hidden = YES;
            
            NSMutableArray * data01Array = [NSMutableArray new]; //= @[@60.1, @160.1, @126.4, @262.2, @186.2];
            NSMutableArray * data02Array = [NSMutableArray new]; //@[@20.1, @180.1, @26.4, @202.2, @126.2];
            NSMutableArray *xLblsArr = [NSMutableArray new];
            
            for (TemperatureStudModel *tempRecord in temperetureRecordsArr) {
                [data01Array addObject:
                 [tempRecord.morningTemperature stringByReplacingOccurrencesOfString:@"C" withString:@""]];
                [data02Array addObject:[tempRecord.eveningTemperature stringByReplacingOccurrencesOfString:@"C" withString:@""]];
                
                [xLblsArr addObject:[SINGLETON_INSTANCE stringFromDate:tempRecord.takenDate withFormate:@"dd"]];
            }
            
            [_lineChart setXLabels:xLblsArr];
            
            //Label
            _lineChart.xLabelFont = [UIFont fontWithName:APP_FONT size:12.0];
            _lineChart.xLabelColor = APP_BLACK_COLOR;
            _lineChart.yLabelFont = [UIFont fontWithName:APP_FONT size:14.0];
            _lineChart.yLabelColor = APP_BLACK_COLOR;
            _lineChart.yLabelNum = 5;
            _lineChart.yLabelHeight = 30;
            _lineChart.yUnit = @"°";
            
            //_lineChart.showYGridLines = YES;
            _lineChart.showSmoothLines = NO;
            _lineChart.showCoordinateAxis = YES;
            _lineChart.axisColor = PNLightGrey;
            
            //[_lineChart setXLabels:@[@"SEP 1",@"SEP 2",@"SEP 3",@"SEP 4",@"SEP 5"]];
            
            // Line Chart No.1
            PNLineChartData *data01 = [PNLineChartData new];
            data01.color = COLOR_FORENOON;
            data01.itemCount = _lineChart.xLabels.count;
            data01.getData = ^(NSUInteger index) {
                CGFloat yValue = [data01Array[index] floatValue];
                return [PNLineChartDataItem dataItemWithY:yValue];
            };
            
            // Line Chart No.2
            PNLineChartData *data02 = [PNLineChartData new];
            data02.color = COLOR_AFTERNOON;
            data02.itemCount = _lineChart.xLabels.count;
            data02.getData = ^(NSUInteger index) {
                CGFloat yValue = [data02Array[index] floatValue];
                return [PNLineChartDataItem dataItemWithY:yValue];
            };
            
            _lineChart.chartData = @[data01, data02];
            [_lineChart strokeChart];
            //Tbl View Fix
            [temperetureRecordsArr addObject:@"Fix"];
            [_tblVw reloadData];
            
            data01Array = nil;
            data02Array = nil;
            xLblsArr = nil;
            
            
            _tblVw.frame = CGRectMake(_tblVw.frame.origin.x, _foorenoonLegendVw.frame.origin.y + _foorenoonLegendVw.frame.size.height + 15, _tblVw.frame.size.width, 50*temperetureRecordsArr.count);
            
            [_scrollVw setContentSize:CGSizeMake(_tblVw.frame.size.width, _tblVw.frame.origin.y + _tblVw.frame.size.height)];
            
        } else {
            
            _weeakMnthVw.hidden = YES;
            _dayVw.hidden = YES;
            
        }
    }
}

#pragma mark - Table view data sources & delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return temperetureRecordsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"%ld",(long)indexPath.row);
    
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [TSSingleton layerDrawForView:cell.contentView position:LAYER_TOP color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:cell.contentView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:cell.contentView position:LAYER_LEFT
                            color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:cell.contentView position:LAYER_RIGHT color:[UIColor lightGrayColor]];
    
    UILabel *temperatureDateLbl = (UILabel *)[cell viewWithTag:101];
    
    UILabel *forenoonTempLbl = (UILabel *)[cell viewWithTag:102];
    
    UILabel *afternoonTempLbl = (UILabel *)[cell viewWithTag:103];
    
    UIImageView *forenoonTmpImgVw = (UIImageView *)[cell viewWithTag:104];
    
    UIImageView *afternoonTmpImgVw = (UIImageView *)[cell viewWithTag:105];
    
    if (indexPath.row == 0){
        
        temperatureDateLbl.font = [UIFont fontWithName:APP_FONT size:15.0];
        forenoonTempLbl.font = [UIFont fontWithName:APP_FONT size:15.0];
        afternoonTempLbl.font = [UIFont fontWithName:APP_FONT size:15.0];
        
        temperatureDateLbl.text = @"Date";
        
        forenoonTempLbl.text = @"FN";
        
        afternoonTempLbl.text = @"AN";
        
        forenoonTmpImgVw.hidden = YES;
        afternoonTmpImgVw.hidden = YES;
        
    }
    
    if (indexPath.row != 0) {
        
        temperatureDateLbl.font = [UIFont fontWithName:APP_FONT size:13.0];
        forenoonTempLbl.font = [UIFont fontWithName:APP_FONT size:13.0];
        afternoonTempLbl.font = [UIFont fontWithName:APP_FONT size:13.0];
        
        TemperatureStudModel *tempRecord = [temperetureRecordsArr objectAtIndex:indexPath.row - 1];
        
        temperatureDateLbl.text = [SINGLETON_INSTANCE stringFromDate:tempRecord.takenDate withFormate:@"MMMM dd, yyyy, EEE"];
        
        forenoonTempLbl.text = tempRecord.morningTemperature;
        
        afternoonTempLbl.text = tempRecord.eveningTemperature;
        
        if ([forenoonTempLbl.text floatValue] > [afternoonTempLbl.text floatValue]) {
            forenoonTmpImgVw.hidden = NO;
            afternoonTmpImgVw.hidden = YES;
        } else {
            forenoonTmpImgVw.hidden = YES;
            afternoonTmpImgVw.hidden = NO;
        }
        
    }
    
    [TSSingleton layerDrawForView:afternoonTempLbl position:LAYER_LEFT color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:forenoonTempLbl position:LAYER_LEFT color:[UIColor lightGrayColor]];
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark Service Call

- (void)fetchTemperatureDetailsWithStartDate:(NSString *)startDate andEndDate:(NSString *)endDate{
    
    NSString *urlString = @"HealthDetails/Services/TemperatureService.svc/FetchTemperatureByID";
    
    StudentModel *studentMdlObj = _studentObj;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    [param setObject:studentMdlObj.studentTransID forKey:@"StudentByID"];
    
    [param setObject:startDate forKey:@"FromDate"];
    
    [param setObject:endDate forKey:@"ToDate"];
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if([responseDictionary[@"FetchTemperatureByIDResult"][@"ResponseCode"] intValue] == 1){
                
                [temperetureRecordsArr removeAllObjects];
                
                for (NSDictionary *tempDict in responseDictionary[@"FetchTemperatureByIDResult"][@"TemperatureSingle"]) {
                    
                    NSDictionary *temperatureDict = [tempDict dictionaryByReplacingNullsWithBlanks];

                    TemperatureStudModel *tempObj = [TemperatureStudModel new];
                    
                    tempObj.studentName = studentMdlObj.studentName;
                    
                    tempObj.studentTransID = studentMdlObj.studentTransID;
                    
                    tempObj.temperatureTransId = temperatureDict[@"TemperatureID"];
                    
                    tempObj.morningTemperature = temperatureDict[@"MorningTemperature"];
                    
                    tempObj.eveningTemperature = temperatureDict[@"EveningTemperature"];
                    
                    tempObj.createdDate = [SINGLETON_INSTANCE dateFromString:temperatureDict[@"DateTimeCreated"] withFormate:DATE_FORMATE_SERVICE_RESPONSE];
                    
                    tempObj.takenDate = [SINGLETON_INSTANCE dateFromString:temperatureDict[@"TakenDate"] withFormate:DATE_FORMATE_SERVICE_RESPONSE];
                    
                    tempObj.modifiedDate = [SINGLETON_INSTANCE dateFromString:temperatureDict[@"DateTimeModified"] withFormate:DATE_FORMATE_SERVICE_RESPONSE];
                    
                    [studTempRecordsArr addObject:tempObj];
                }
                
                [self fetchDataForViewType:currentVw];
                
            } else {
                //Service Response Failure
            }
        } else {
            //Service Call Failure
        }
    }];
    
}

@end