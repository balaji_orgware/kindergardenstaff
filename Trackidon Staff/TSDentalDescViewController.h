//
//  TSDentalDescViewController.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 22/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSDentalDescViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *backView;

@property (weak, nonatomic) IBOutlet UIView *descView;
@property (weak, nonatomic) IBOutlet UIView *dateView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *examinedByName;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *intraOralDescLbl;
@property (weak, nonatomic) IBOutlet UILabel *extraOralDescLbl;

@property (weak, nonatomic) IBOutlet UILabel *alsoHasLbl;
@property (weak, nonatomic) IBOutlet UITextView *alsoHasTxtVw;
@property (weak, nonatomic) IBOutlet UILabel *remarksLbl;
@property (weak, nonatomic) IBOutlet UITextView *remarksTxtVw;

-(void) showDescViewController:(UIViewController *)viewController withModelObj:(DentalInfoModel *)modelObj;

@end
