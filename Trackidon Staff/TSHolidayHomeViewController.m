//
//  TSHolidayHomeViewController.m
//  Trackidon Staff
//
//  Created by Elango on 23/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSHolidayHomeViewController.h"

@interface TSHolidayHomeViewController ()<UITableViewDataSource,UITableViewDelegate,JTCalendarDelegate>

@end

@implementation TSHolidayHomeViewController
{
    int unreadCount;
    
    NSMutableArray *wholeHolidaysArray;
    
    NSMutableDictionary *listDict;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    listDict = [NSMutableDictionary new];
    
    unreadCount = 0;
    
    [self.menuTableView setTableFooterView:[UIView new]];
    
    {
        _calendarManager = [JTCalendarManager new];
        
        _calendarManager.delegate = self;
        
        _calendarManager.settings.weekModeEnabled=NO;
        
        _calendarManager.settings.pageViewHaveWeekDaysView = YES;
        
        [_calendarManager setMenuView:_calendarMenuView];
        
        [_calendarManager setContentView:_calendarContentView];
        
        [_calendarManager setDate:[NSDate date]];
        
        _calendarManager.dateHelper.calendar.timeZone = [NSTimeZone systemTimeZone];
        
        _calendarManager.dateHelper.calendar.locale = [NSLocale localeWithLocaleIdentifier:@"en_IN"];
        
    }
       
    [self categorizeHolidays];
        
    [self retrieveHolidays];
    
    
    if (IS_IPHONE_4) {
        
        [_scrollView setContentSize:CGSizeMake(DEVICE_SIZE.width, 460)];
    }
    
    
    [_calendarMenuView bringSubviewToFront:_prevMnthBtn];
    [_calendarMenuView bringSubviewToFront:_nxtMnthBtn];
    
    [TSSingleton layerDrawForView:_calendarMenuView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:_listView position:LAYER_TOP color:[UIColor lightGrayColor]];
    
    [_calendarManager reload];
    
    [TSAnalytics logEvent:ANALYTICS_HOLIDAY_HOLIDAYOPENED parameter:[NSMutableDictionary new]];
}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_HOLIDAYHOME parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Holidays";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - JTCalendar Delegates

-(BOOL)calendar:(JTCalendarManager *)calendar canDisplayPageWithDate:(NSDate *)date{
    
    return YES;
}

- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView{
    
    dayView.circleView.hidden = YES;
    
    dayView.textLabel.textColor = [UIColor blackColor];
    
    if ([dayView isFromAnotherMonth]) {
        
        [dayView setHidden:YES];
    }
    else{
        
        [dayView setHidden:NO];
    }
    
    switch ([self haveHolidayForDate:dayView.date]) {
            
        case HolidayReturnTypeNone:
            
            break;
            
        case HolidayReturnTypePublicHoliday:
            
            dayView.circleView.hidden = NO;
            
            dayView.circleView.backgroundColor = HolidaysPublicBGcolor;
            
            dayView.textLabel.textColor = [UIColor whiteColor];
            
            break;
            
        case HolidayReturnTypeSchoolHoliday:
            
            dayView.circleView.hidden = NO;
            
            dayView.circleView.backgroundColor = HolidaysSchoolBGcolor;
            
            dayView.textLabel.textColor = [UIColor whiteColor];
            break;
            
        default:
            break;
    }
}

-(void)calendar:(JTCalendarManager *)calendar prepareMenuItemView:(UIView *)menuItemView date:(NSDate *)date{
    
    UILabel *label = (UILabel *)menuItemView;
    
    label.text = [SINGLETON_INSTANCE stringFromDateWithFormate:@"MMMM - YYYY" withDate:date];
    
    [label setFont:[UIFont fontWithName:APP_FONT size:17]];
    
    [label setTextColor:APP_BLACK_COLOR];
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView{
    
    if (!dayView.circleView.isHidden) {
        
        [TSAnalytics logEvent:ANALYTICS_HOLIDAY_CLICKED_HOLIDAY_IN_CALANDER parameter:[NSMutableDictionary new]];
        
        NSArray *holidaysArray = [self retrieveHolidayEntryForDate:dayView.date];
        
        if (holidaysArray.count>1) {
            
            TSHolidayListViewController *holidayListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBHolidayList"];
            
            holidayListVC.holidayGroupTitle = @"Holiday list";
            
            holidayListVC.holidaysListArray = holidaysArray;
            
            [self.navigationController pushViewController:holidayListVC animated:YES];
        }
        else{
            
            Holidays *holidayObj = [holidaysArray objectAtIndex:0];
            
            TSHolidayDescriptionViewController *holidayDesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBHolidayDescription"];
            
            holidayDesVC.holidaysObj = holidayObj;
            
            [self.navigationController pushViewController:holidayDesVC animated:YES];
        }
    }
}

#pragma mark - Tableview Datasources & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[listDict allKeys] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    UIView *colorView = [cell viewWithTag:100];
    
    UILabel *menuLabel = (UILabel *)[cell viewWithTag:101];
    
    UILabel *countLbl = (UILabel *)[cell viewWithTag:102];
    
    menuLabel.text = [[listDict allKeys] objectAtIndex:indexPath.row];
    
    countLbl.text = [NSString stringWithFormat:@"%lu",[[listDict objectForKey:menuLabel.text] count]];
    
    colorView.backgroundColor = [[[listDict allKeys] objectAtIndex:indexPath.row] isEqualToString:@"School holidays"] ? HolidaysSchoolBGcolor : HolidaysPublicBGcolor;
    
    countLbl.textColor = [[[listDict allKeys] objectAtIndex:indexPath.row] isEqualToString:@"School holidays"] ? HolidaysSchoolBGcolor : HolidaysPublicBGcolor;
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TSHolidayListViewController *holidayListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBHolidayList"];
    
    NSString *holidayGroupTitle = [[listDict allKeys] objectAtIndex:indexPath.row];
    
    if ([holidayGroupTitle isEqualToString:@"School holidays"]) {
        
        [TSAnalytics logEvent:ANALYTICS_HOLIDAY_CLICKED_SCHOOL_HOLIDAYS parameter:[NSMutableDictionary new]];
    }
    else{
        
        [TSAnalytics logEvent:ANALYTICS_HOLIDAY_CLICKED_PUBLIC_HOLIDAYS parameter:[NSMutableDictionary new]];
    }
    
    [holidayListVC setHolidayGroupTitle:holidayGroupTitle];
    
    
    [holidayListVC setHolidaysListArray:[listDict objectForKey:holidayGroupTitle]];
    
    if([[listDict objectForKey:holidayGroupTitle] count] > 0)
        [self.navigationController pushViewController:holidayListVC animated:YES];
}

#pragma mark - Methods

// Method to check whether the date having any holidays

-(NSInteger) haveHolidayForDate:(NSDate *)date{
    
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"fromDate MATCHES %@",[SINGLETON_INSTANCE stringFromDateWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:date]];
    
    NSArray *filteredArray = [wholeHolidaysArray filteredArrayUsingPredicate:filterPredicate];
    
    if (filteredArray.count>0) {
        
        Holidays *holidayObj = [filteredArray objectAtIndex:0];
        
        if ([holidayObj.holidayTypeID intValue] == HolidayTypePublicHoliday)
            return HolidayReturnTypePublicHoliday;
        
        else if([holidayObj.holidayTypeID intValue] == HolidayTypeSchoolHoliday)
            return HolidayReturnTypeSchoolHoliday;
        
        else
            return HolidayReturnTypeNone;
    }
    else
        return HolidayReturnTypeNone;
    
}

// Method to retrieve Holiday record from DB
-(NSArray *)retrieveHolidayEntryForDate:(NSDate *) date{
    
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"fromDate MATCHES %@",[SINGLETON_INSTANCE stringFromDateWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:date]];
    
    NSArray *filteredArray = [wholeHolidaysArray filteredArrayUsingPredicate:filterPredicate];
    
    return filteredArray;
}


-(void) categorizeHolidays{
    
    //NSLog(@"%@",[appDelegateInstance applicationDocumentsDirectory]);
    
    [wholeHolidaysArray removeAllObjects];
    
    [listDict removeAllObjects];
    
    NSPredicate *wholeHolidaysPred = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@",[SINGLETON_INSTANCE staff].staffTransId];
    
    wholeHolidaysArray = [NSMutableArray arrayWithArray:[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Holidays" withPredicate:wholeHolidaysPred]];
    
    NSPredicate *pubHolidayPred = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@ AND holidayTypeID contains[cd] %d",[SINGLETON_INSTANCE staff].staffTransId,HolidayTypePublicHoliday];
    
    NSArray *publicHolidaysArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Holidays" withPredicate:pubHolidayPred];
    
    NSPredicate *schoolHolidayPred = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@ AND holidayTypeID contains[cd] %d",[SINGLETON_INSTANCE staff].staffTransId,HolidayTypeSchoolHoliday];
    
    NSArray *schoolHolidaysArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Holidays" withPredicate:schoolHolidayPred];
    
    [listDict setObject:schoolHolidaysArray forKey:@"School holidays"];
    
    [listDict setObject:publicHolidaysArray forKey:@"Public holidays"];
    
    /*if (schoolHolidaysArray.count>0) {
        
        [listDict setObject:schoolHolidaysArray forKey:@"School holidays"];
    }
    
    if (publicHolidaysArray.count>0) {
        
        [listDict setObject:publicHolidaysArray forKey:@"Public holidays"];
    }
    
    if (listDict.allKeys.count>0) {
        
        [self.menuTableView setHidden:NO];
    }
    else{
        
        [self.menuTableView setHidden:YES];
    }*/
    
    [self.menuTableView reloadData];
    
    [_calendarManager reload];
}

#pragma mark - Service call

// Service Calls Hits
-(void) retrieveHolidays{
    
    [self fetchHolidaysServiceCallWithHolidayType:HolidayTypePublicHoliday forStaff:SINGLETON_INSTANCE.staff];
    
}

-(void) fetchHolidaysServiceCallWithHolidayType:(NSInteger)holidayType forStaff:(StaffDetails *)staff{
    
    NSString *urlString = @"MastersMServices/EventsMService.svc/FetchAllByEvents";
    
    NSMutableDictionary *param = [NSMutableDictionary new];
    
    [param setValue:[NSString stringWithFormat:@"24,25"] forKey:@"intEventTypeID"];
    
    //[param setValue:staff.staffTransId forKey:@"guidStaffTransID"];
    
    BOOL showIndicator = NO;
    
    if(wholeHolidaysArray.count == 0)
        showIndicator = YES;
    
    [[TSHandlerClass sharedHandler] postRequestWithURL:urlString baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:param currentView:self.view showIndicator:showIndicator onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if([responseDictionary[@"FetchAllByEventsResult"][@"ResponseCode"] intValue] == 1){
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                    [[Holidays instance] storeHolidaysForStaff:SINGLETON_INSTANCE.staff withDict:responseDictionary];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                         [self categorizeHolidays];
                        
                    });
                });
            }
        }
    }];
}

#pragma mark - Button Actions

- (IBAction)prevMonthButtonTapped:(id)sender {
    
    [TSAnalytics logEvent:ANALYTICS_HOLIDAY_CLICKED_PREV_MNTH_BTN parameter:[NSMutableDictionary new]];
    
    [_calendarManager.contentView loadPreviousPageWithAnimation];
}

- (IBAction)nextMonthButtonTapped:(id)sender {
    
    [TSAnalytics logEvent:ANALYTICS_HOLIDAY_CLICKED_NXT_MNTH_BTN parameter:[NSMutableDictionary new]];
    
    [_calendarManager.contentView loadNextPageWithAnimation];
}



@end
