//
//  Holidays.h
//  Trackidon Staff
//
//  Created by Elango on 23/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StaffDetails;

NS_ASSUME_NONNULL_BEGIN

@interface Holidays : NSManagedObject

+(instancetype)instance;

-(void)storeHolidaysForStaff: (StaffDetails*)staffObj withDict:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END

#import "Holidays+CoreDataProperties.h"
