//
//  TSForgotPasswordViewController.h
//  Trackidon Staff
//
//  Created by ORGware on 16/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSHeaders.pch"

@interface TSForgotPasswordViewController : UIViewController <UITextFieldDelegate>

// Declaration
@property (weak, nonatomic) IBOutlet UIView *forgotPwdContainerVw;
@property (weak, nonatomic) IBOutlet UIView *forgotPwdFieldsVw;
@property (weak, nonatomic) IBOutlet UITextField *usernameTxtFld;
@property (weak, nonatomic) IBOutlet UIView *logoView;
@property (weak, nonatomic) IBOutlet UIButton *getButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

//Method Declaration
- (IBAction)getButtonAction:(id)sender;
- (IBAction)backButtonAction:(id)sender;

@end
