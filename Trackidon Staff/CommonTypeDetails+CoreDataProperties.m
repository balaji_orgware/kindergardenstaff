//
//  CommonTypeDetails+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Balaji on 07/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CommonTypeDetails+CoreDataProperties.h"

@implementation CommonTypeDetails (CoreDataProperties)

@dynamic typeID;
@dynamic name;
@dynamic transID;

@end
