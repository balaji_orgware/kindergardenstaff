//
//  Assignments+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 28/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Assignments+CoreDataProperties.h"

@implementation Assignments (CoreDataProperties)

@dynamic academicTransID;
@dynamic academicYear;
@dynamic approvedByName;
@dynamic approvedByTransID;
@dynamic approvedDate;
@dynamic approvedStatus;
@dynamic assignedList;
@dynamic assignmentDate;
@dynamic assignmentDescription;
@dynamic assignmentTitle;
@dynamic assignmentTypeName;
@dynamic attachment;
@dynamic boardName;
@dynamic boardTransID;
@dynamic createdBy;
@dynamic dueDate;
@dynamic isAttachment;
@dynamic isForApprovals;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic schoolName;
@dynamic schoolTransID;
@dynamic staffTransID;
@dynamic subjectName;
@dynamic transID;
@dynamic staff;

@end
