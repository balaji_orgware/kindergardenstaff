//
//  TSApprovalsTableViewController.m
//  Trackidon Staff
//
//  Created by ORGware on 18/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSApprovalsTableViewController.h"

@interface TSApprovalsTableViewController (){
    
    NSArray *approvalArray;
}
@end

@implementation TSApprovalsTableViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    approvalArray = [NSArray new];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_APPROVALSHOME parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    [self.tableView setTableFooterView:[UIView new]];
    
    self.navigationItem.title = @"Approvals";
    
    [self fetchCommunicationApprovalsAPICall];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Helper Methods

-(void) fetchApprovalCounts{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isForApproval == 1"];
    
    approvalArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"PushCount" withPredicate:predicate];
}

-(NSString *) getCountForMenuID:(NSInteger)menuID{
 
    NSString *countValue;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"commTypeID MATCHES %@",[NSString stringWithFormat:@"%ld",(long)menuID]];
    
    NSArray *filteredArray = [approvalArray filteredArrayUsingPredicate:predicate];
    
    if (filteredArray.count >0) {
        
        PushCount *obj = [filteredArray objectAtIndex:0];
        
        countValue = [NSString stringWithFormat:@"%@",obj.unreadCount];
    }
    else{
        
        countValue = @"0";
    }
    
    return countValue;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [SINGLETON_INSTANCE approvalMenuArr].count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UIImageView *placeholderImgVw = (UIImageView *)[cell viewWithTag:500];
    
    UILabel *menuLabel = (UILabel *)[cell viewWithTag:501];
    
    UILabel *countLabel = (UILabel *)[cell viewWithTag:502];
    
    countLabel.layer.cornerRadius = countLabel.frame.size.width/2;
    
    countLabel.layer.masksToBounds = YES;
    
    NSDictionary *menuDict = [[SINGLETON_INSTANCE approvalMenuArr] objectAtIndex:indexPath.row];
    
    switch ([menuDict[@"MenuID"] integerValue]) {
            
        case MENU_ASSIGNMENT_APPROVALS:
            
            placeholderImgVw.image = [UIImage imageNamed:@"assignments"];
            
            countLabel.text = [self getCountForMenuID:MENU_ASSIGNMENT_APPROVALS];
            
//            countLabel.text = [USERDEFAULTS valueForKey:@"AssignmentsCount"] == nil ? @"0" : [NSString stringWithFormat:@"%@",[USERDEFAULTS valueForKey:@"AssignmentsCount"]];
            break;
            
        case MENU_EVENT_APPROVALS:
            
            placeholderImgVw.image = [UIImage imageNamed:@"events"];
            
            countLabel.text = [self getCountForMenuID:MENU_EVENT_APPROVALS];
            
//            countLabel.text = [USERDEFAULTS valueForKey:@"EventsCount"] == nil ? @"0" : [NSString stringWithFormat:@"%@",[USERDEFAULTS valueForKey:@"EventsCount"]];
            break;
            
        case MENU_NOTIFICATION_APPROVALS:
            
            placeholderImgVw.image = [UIImage imageNamed:@"notifications"];
            
            countLabel.text = [self getCountForMenuID:MENU_NOTIFICATION_APPROVALS];

//            countLabel.text = [USERDEFAULTS valueForKey:@"NotificationsCount"] == nil ? @"0" : [NSString stringWithFormat:@"%@",[USERDEFAULTS valueForKey:@"NotificationsCount"]];
            break;
            
        case MENU_PORTION_APPROVALS:
            
            placeholderImgVw.image = [UIImage imageNamed:@"portions"];
            
            countLabel.text = [self getCountForMenuID:MENU_PORTION_APPROVALS];

//            countLabel.text = [USERDEFAULTS valueForKey:@"PortionsCount"] == nil ? @"0" : [NSString stringWithFormat:@"%@",[USERDEFAULTS valueForKey:@"PortionsCount"]];
            break;
            
        default:
            break;
    }
    
    if ([countLabel.text integerValue]>0) {
        
        [countLabel setHidden:NO];
    }
    else{
        
        [countLabel setHidden:YES];
    }
    
    menuLabel.text = [menuDict[@"MenuName"] capitalizedString];
    
    menuLabel.font = [UIFont fontWithName:APP_FONT size:15];
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *menuDict = [[SINGLETON_INSTANCE approvalMenuArr] objectAtIndex:indexPath.row];
    
    SINGLETON_INSTANCE.barTitle = [menuDict[@"MenuName"] capitalizedString];
    
    switch ([menuDict[@"MenuID"] integerValue]) {
        
        case MENU_EVENT_APPROVALS:
        {
            [[PushCount instance]updatePushCountsFormenuId:[NSString stringWithFormat:@"%ld",(long)MENU_EVENT_APPROVALS]isForApproval:YES];
            
            TSEventsApprovalsViewController *eventApprovalsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBEventsApprovals"];
            
            [self.navigationController pushViewController:eventApprovalsVC animated:YES];
            
            break;
        }
            
        case MENU_ASSIGNMENT_APPROVALS:
        {
            [[PushCount instance]updatePushCountsFormenuId:[NSString stringWithFormat:@"%ld",(long)MENU_ASSIGNMENT_APPROVALS]isForApproval:YES];
            
            TSAssignmentApprovalViewController *assignmentApprovalsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAssignmentApprovals"];
            
            [self.navigationController pushViewController:assignmentApprovalsVC animated:YES];
            
            break;
        }
            
        case MENU_NOTIFICATION_APPROVALS:
        {
            [[PushCount instance]updatePushCountsFormenuId:[NSString stringWithFormat:@"%ld",(long)MENU_NOTIFICATION_APPROVALS]isForApproval:YES];

            TSNotificationApprovalDescViewController *notificationApprovalDescVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBNotificationApprovals"];
            
            [self.navigationController pushViewController:notificationApprovalDescVC animated:YES];
            
            break;
        }
            
        case MENU_PORTION_APPROVALS:
        {
            [[PushCount instance]updatePushCountsFormenuId:[NSString stringWithFormat:@"%ld",(long)MENU_PORTION_APPROVALS]isForApproval:YES];

            TSPortionsApprovalsViewController *portionApprovalsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBPortionsApprovals"];
            
            [self.navigationController pushViewController:portionApprovalsVC animated:YES];
            
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - Service Call

-(void) fetchCommunicationApprovalsAPICall{
    
    NSString *urlString = @"CommunicationPortal/Service/PushCountService.svc/FetchUnApprovedCommunication";
    
    NSDictionary *params = @{@"AccountTransID":SINGLETON_INSTANCE.staffTransId};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchUnApprovedCommunicationResult"][@"ResponseCode"] intValue] == 1) {
                
                    NSDictionary *dict = responseDictionary;
                
                    [[PushCount instance]saveUpdatedPushCountsWithDict:dict isForApproval:YES];
                
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                        NSArray *assignmentArray = [dict[@"FetchUnApprovedCommunicationResult"][@"UnApprovalClass"][@"AssignmentClass"] isEqual:[NSNull null]] ? [NSArray new]:dict[@"FetchUnApprovedCommunicationResult"][@"UnApprovalClass"][@"AssignmentClass"];
                        
                        if ([assignmentArray count]>0) {
                            
                            [[Assignments instance] storeAssignmentsDataForStaff:SINGLETON_INSTANCE.staff withDict:dict andIsForApprovals:YES];
                        }
                        
                        NSArray *eventsArray = [dict[@"FetchUnApprovedCommunicationResult"][@"UnApprovalClass"][@"EventsMClass"] isEqual:[NSNull null]] ? [NSArray new] : dict[@"FetchUnApprovedCommunicationResult"][@"UnApprovalClass"][@"EventsMClass"];
                        
                        if ([eventsArray count]>0) {
                            
                            [[Events instance]saveEventsWithResponseDict:dict andIsForApprovals:YES];
                        }
                        
                        NSArray *portionsArray = [dict[@"FetchUnApprovedCommunicationResult"][@"UnApprovalClass"][@"PortionsClass"] isEqual:[NSNull null]] ? [NSArray new] : dict[@"FetchUnApprovedCommunicationResult"][@"UnApprovalClass"][@"PortionsClass"];
                        
                        if ([portionsArray count]>0) {
                            
                            [[Portions instance] storePortionsForStaffObj:SINGLETON_INSTANCE.staff withDict:dict isForApprovals:YES];
                        }
                        
                        NSArray *notificationArray = [dict[@"FetchUnApprovedCommunicationResult"][@"UnApprovalClass"][@"SchoolNotificationClass"] isEqual:[NSNull null]]?[NSArray new] : dict[@"FetchUnApprovedCommunicationResult"][@"UnApprovalClass"][@"SchoolNotificationClass"];
                        
                        if ([notificationArray count]>0) {
                            
                            [[Notifications instance]saveNotificationsWithResponseDict:dict andIsForApprovals:YES];
                        }
                        
                        
                        /*NSString *assignmentCount = [NSString stringWithFormat:@"%@",dict[@"FetchUnApprovedCommunicationResult"][@"UnApprovalCount"][@"AssignmentCount"]];
                        
                        [USERDEFAULTS setValue:assignmentCount forKey:@"AssignmentsCount"];
                        
                        NSString *eventCount = [NSString stringWithFormat:@"%@",dict[@"FetchUnApprovedCommunicationResult"][@"UnApprovalCount"][@"EventsMCount"]];
                        
                        [USERDEFAULTS setValue:eventCount forKey:@"EventsCount"];
                        
                        NSString *portionCount = [NSString stringWithFormat:@"%@",dict[@"FetchUnApprovedCommunicationResult"][@"UnApprovalCount"][@"PortionsCount"]];
                        
                        [USERDEFAULTS setValue:portionCount forKey:@"PortionsCount"];
                        
                        NSString *notificationCount = [NSString stringWithFormat:@"%@",dict[@"FetchUnApprovedCommunicationResult"][@"UnApprovalCount"][@"SchoolNotificationCount"]];
                        
                        [USERDEFAULTS setValue:notificationCount forKey:@"NotificationsCount"];*/
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self fetchApprovalCounts];

                            [self.tableView reloadData];
                        });
                    });
                }
            }
        }];
}
@end