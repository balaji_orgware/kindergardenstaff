//
//  TSCreateEventViewController.m
//  Trackidon Staff
//
//  Created by Elango on 24/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

@implementation UITextField (custom)

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectMake(bounds.origin.x + 10, bounds.origin.y,
                      bounds.size.width - 20, bounds.size.height);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}
@end

#import "TSCreateEventViewController.h"

@interface TSCreateEventViewController ()<UITextFieldDelegate,UITextViewDelegate,datePickerDelegate,AttachmentPikerDelegate>

@end

@implementation TSCreateEventViewController
{
    BOOL isFromDateSelected;
    BOOL isToDateSelected;
    BOOL isHasEventAttachment;
    NSData *attachmentData;
    NSDate *fromDate;
    NSDate *toDate;
    UIView *currentResponder;
    CGRect viewRect;
    CGRect currentVisibleRect;
    NSMutableDictionary *eventDict;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [TSSingleton layerDrawForView:_titleTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_dateVw position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_timeTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_venueTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_attachementTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_attachementTxtFld position:LAYER_TOP color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_receiverSelectionVw position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    [SINGLETON_INSTANCE addDoneButtonForTextView:_descriptionTxtVw];
    
    _descriptionTxtVw.text = @"Description";
    _descriptionTxtVw.textColor = APP_PLACEHOLDER_COLOR;
    _descriptionTxtVw.font = [UIFont fontWithName:APP_FONT size:17];
    _descriptionTxtVw.textContainerInset =
    UIEdgeInsetsMake(15,5,10,15); // top, left, bottom, right
    
    viewRect = self.view.frame;
    
    [_scrollVw setContentSize:CGSizeMake(DeviceWidth, _attachementTxtFld.frame.origin.y + _attachementTxtFld.frame.size.height +60)];
    
    //_descriptionTxtVw.layer.borderColor = [UIColor greenColor].CGColor;
    //_descriptionTxtVw.layer.borderWidth = 0.5;
    
    
    eventDict = [NSMutableDictionary new];
    isHasEventAttachment = NO;
    
    [self setInitialDate];
    
}

- (void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_EVENTCREATE parameter:[NSMutableDictionary new]];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Create Event";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -

- (void)setInitialDate{
    
    //To Date
    
    NSArray *dateArr = [[SINGLETON_INSTANCE stringFromDateWithFormate:DATE_FORMATE_DAY_MNTH_YR withDate:[NSDate date]] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    _toDateDayLbl.text = dateArr[0];
    
    _toDateMnthYearLbl.text = [NSString stringWithFormat:@"%@ %@",dateArr[1],dateArr[2]];
    
    toDate = [NSDate date];
    
    //Frome Date
    
    _fromDateDayLbl.text = dateArr[0];
    
    _fromDateMnthYearLbl.text = [NSString stringWithFormat:@"%@ %@",dateArr[1],dateArr[2]];
    
    fromDate = [NSDate date];
    
}

#pragma mark DatePicker Delegate

- (void)selectedDate:(NSDate *)date{
    
    NSArray *dateArr = [[SINGLETON_INSTANCE stringFromDateWithFormate:DATE_FORMATE_DAY_MNTH_YR withDate:date] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSLog(@"%@",dateArr);
    
    if (isFromDateSelected) {
        
        isToDateSelected = NO;
        isFromDateSelected = NO;
        
        fromDate = date;
        
        _fromDateDayLbl.text = dateArr[0];
        
        _fromDateMnthYearLbl.text = [NSString stringWithFormat:@"%@ %@",dateArr[1],dateArr[2]];
        
        //Todate
        
        toDate = date;
        
        _toDateDayLbl.text = dateArr[0];
        
        _toDateMnthYearLbl.text = [NSString stringWithFormat:@"%@ %@",dateArr[1],dateArr[2]];
        
    } else if(isToDateSelected){
        
        isToDateSelected = NO;
        isFromDateSelected = NO;
        
        toDate = date;
        
        _toDateDayLbl.text = dateArr[0];
        
        _toDateMnthYearLbl.text = [NSString stringWithFormat:@"%@ %@",dateArr[1],dateArr[2]];
        
    } else {
        
        _timeTxtFld.text = [SINGLETON_INSTANCE stringFromDateWithFormate:@"hh:mm a" withDate:date];
    }
    
}

#pragma mark - Button Action

- (IBAction)onFromDateSelectionClk:(id)sender {
    
    isFromDateSelected = YES;
    isToDateSelected = NO;
    
    [self.view endEditing:YES];

    TSDatePickerViewController *datePickerVC = [[TSDatePickerViewController alloc]initWithNibName:@"TSDatePickerViewController" bundle:nil];
    
    [datePickerVC showInViewController:self calendarType:CALENDAR_TYPE_DATE withMinDate:[NSDate date] withMaxDate:nil];
    
    datePickerVC.delegate = self;
}

- (IBAction)todDateSelectionClk:(id)sender {
    
    isFromDateSelected = NO;
    isToDateSelected = YES;

    [self.view endEditing:YES];

    TSDatePickerViewController *datePickerVC = [[TSDatePickerViewController alloc]initWithNibName:@"TSDatePickerViewController" bundle:nil];
    
    [datePickerVC showInViewController:self calendarType:CALENDAR_TYPE_DATE withMinDate:fromDate withMaxDate:nil];
    
    datePickerVC.delegate = self;
    
}
- (IBAction)onSelectReceiverClk:(id)sender {
    
    if ([_titleTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
        
        [TSSingleton showAlertWithMessage:TEXT_EMPTY_TITLE];
        
    } else if (_titleTxtFld.text.length> MAX_TITLE_LENGTH){
        
        [TSSingleton showAlertWithMessage:TEXT_TITLE_LENGTH_EXCEED];
        
    }else if ([_timeTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)
    {
        [TSSingleton showAlertWithMessage:TEXT_EMPTY_TIME];
        
    }else if ([_venueTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)
    {
        [TSSingleton showAlertWithMessage:TEXT_EMPTY_VENUE];
        
    }else if ([_descriptionTxtVw.text isEqualToString:DESCRIPTION_TEXT])
    {
        [TSSingleton showAlertWithMessage:TEXT_EMPTY_DESCRIPTION];
        
    }else {
        
        [eventDict removeAllObjects];
        
        [eventDict setValue:@"26" forKey:@"EventTypeID"];
        
        [eventDict setValue:_titleTxtFld.text forKey:@"EventTitle"];
        
        [eventDict setValue:_descriptionTxtVw.text forKey:@"Description"];
        
        [eventDict setValue:_venueTxtFld.text forKey:@"Venue"];
        
        [eventDict setValue:[SINGLETON_INSTANCE stringFromDate:fromDate withFormate:DATE_FORMATE_SERVICE_CALL] forKey:@"FromDate"];
        
        [eventDict setValue:[SINGLETON_INSTANCE stringFromDate:toDate withFormate:DATE_FORMATE_SERVICE_CALL] forKey:@"ToDate"];
        
        [eventDict setValue:_timeTxtFld.text forKey:@"EventTime"];
        
        if (isHasEventAttachment) {
            
            [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_ASSIGN_RECEIVER parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_YES,ANALYTICS_PARAM_ISHASATTACHMENT, nil]];
            
            [eventDict setValue:@"true" forKey:@"IsAttachment"];
            
            [eventDict setValue:[@"data:image/jpeg;base64,"stringByAppendingString:[[attachmentData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength] stringByReplacingOccurrencesOfString:@"\n" withString:@""]] forKey:@"Attachment"];
            
        }else{
            
            [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_ASSIGN_RECEIVER parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_NO,ANALYTICS_PARAM_ISHASATTACHMENT, nil]];
            
            [eventDict setValue:@"false" forKey:@"IsAttachment"];
        
        }

        NSLog(@"%@",eventDict);

        
        TSReceiverSelectionViewController *receiverSelectionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBReceiverSelection"];
        
        receiverSelectionVC.menuId = SINGLETON_INSTANCE.currentMenuId;
        
        receiverSelectionVC.commonDict = eventDict;
        
        receiverSelectionVC.communicationType = OBJECT_TYPE_EVENT;
        
        [self.navigationController pushViewController:receiverSelectionVC animated:YES];
        
    }
}

#pragma mark - Attachment Delegate

- (void) selectedAttachmentImageData:(NSData *)imageData imageName:(NSString *)imageName isHasAttachment:(BOOL)isHasAttachment
{
    if (isHasAttachment) {
        isHasEventAttachment = YES;
        
        attachmentData = imageData;

        NSLog(@"%@",imageData);
    }
}

- (void)selectedAttachmentImageName:(NSString *)imageName{
    
    if (imageName != nil) {
        _attachementTxtFld.text = imageName;
    }else {
        _attachementTxtFld.text = @"Photo";
    }
}




#pragma mark - TextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    return YES;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    currentResponder = textField;
    
    if (textField == _timeTxtFld) {
        
        TSDatePickerViewController *datePickerVC = [[TSDatePickerViewController alloc]initWithNibName:@"TSDatePickerViewController" bundle:nil];
        
        [self.view endEditing:YES];
        
        if ([[SINGLETON_INSTANCE dateFromDate:fromDate withFormate:@"dd mm yyyy"] compare:[SINGLETON_INSTANCE dateFromDate:[NSDate date] withFormate:@"dd mm yyyy"]] == NSOrderedSame) {
            
            [datePickerVC showInViewController:self calendarType:CALENDAR_TYPE_TIME withMinDate:[NSDate date] withMaxDate:nil];
            
        } else {
        
            [datePickerVC showInViewController:self calendarType:CALENDAR_TYPE_TIME withMinDate:nil withMaxDate:nil];
        }
        
        datePickerVC.delegate = self;
        
        return NO;
        
    }else if(textField == _attachementTxtFld) {
        
        [self.view endEditing:YES];
        
        [TSAttachmentPicker instance].delegate = self;
        
        [[TSAttachmentPicker instance] showAttachmentPickerForView:self frame:textField.frame];
        
        return NO;

    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - TextView Delegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    currentResponder = textView;
    
    if ([textView.text isEqualToString:DESCRIPTION_TEXT]) {
        
        textView.text = @"";
        textView.textColor = APP_BLACK_COLOR;
        textView.font = [UIFont fontWithName:APP_FONT size:14];

        
    }
    
    return YES;
    
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
        textView.text = DESCRIPTION_TEXT;
        textView.textColor = APP_PLACEHOLDER_COLOR;
        textView.font = [UIFont fontWithName:APP_FONT size:17];
        
    }
    
    CGFloat fixedWidth = _descriptionTxtVw.frame.size.width;
    CGSize newSize = [_descriptionTxtVw sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = _descriptionTxtVw.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    
    if (newFrame.size.height > 50) {
        _descriptionTxtVw.frame = newFrame;
    }else {
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), 50);
        _descriptionTxtVw.frame = newFrame;
    }
    
    
    [self reArrangeControles];
    
    //ScrollView Fix
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_scrollVw setContentSize:CGSizeMake(DeviceWidth, _attachementTxtFld.frame.origin.y + _attachementTxtFld.frame.size.height + 60)];
    });
    
}

- (void)textViewDidChange:(UITextView *)textView{
    
    /*CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    
    if (newFrame.size.height > 50 && newFrame.size.height< currentVisibleRect.size.height ) {
        textView.frame = newFrame;
    }else {
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), 50);
        textView.frame = newFrame;
    }
    
    if (!CGRectContainsPoint(currentVisibleRect, textView.frame.origin))
    {
        
        CGPoint scrollPoint = CGPointMake(0.0, textView.frame.origin.y - currentVisibleRect.size.height  + textView.frame.size.height);
        
        [_scrollVw setContentOffset:scrollPoint animated:YES];
    }*/
    
    [self reArrangeControles];
    
}

- (void)reArrangeControles{
    
    _attachementTxtFld.frame = CGRectMake(_attachementTxtFld.frame.origin.x,_descriptionTxtVw.frame.origin.y + _descriptionTxtVw.frame.size.height, _attachementTxtFld.frame.size.width, _attachementTxtFld.frame.size.height);
    
    _attachmentPinImgVw.frame = CGRectMake(_attachmentPinImgVw.frame.origin.x,_attachementTxtFld.frame.origin.y + 15, _attachmentPinImgVw.frame.size.width, _attachmentPinImgVw.frame.size.height);
    
    //_receiverSelectionVw.frame = CGRectMake(_receiverSelectionVw.frame.origin.x,_attachementTxtFld.frame.origin.y + _attachementTxtFld.frame.size.height, _receiverSelectionVw.frame.size.width, _receiverSelectionVw.frame.size.height);
    
}


#pragma mark - Keyboard Hide

- (void)keyboardWasShown:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    CGSize currentKeyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    CGPoint textFieldOrigin = currentResponder.frame.origin;
    textFieldOrigin.y = textFieldOrigin.y + currentResponder.frame.size.height;
    CGFloat textFieldHeight = currentResponder.frame.size.height;
    
    CGRect visibleRect = viewRect;
    visibleRect.size.height -= currentKeyboardSize.height + textFieldHeight;
    currentVisibleRect = visibleRect;
    
    if ([_descriptionTxtVw isFirstResponder]) {
        
        CGRect descRect = viewRect;
        
        descRect.size.height -= currentKeyboardSize.height;
        
        _descriptionTxtVw.frame = CGRectMake(_descriptionTxtVw.frame.origin.x, _descriptionTxtVw.frame.origin.y, _descriptionTxtVw.frame.size.width, descRect.size.height - 64 );
        
        
        [self reArrangeControles];
        
        CGPoint scrollPoint = CGPointMake(0.0, _descriptionTxtVw.frame.origin.y);
        
        [_scrollVw setContentOffset:scrollPoint animated:YES];
        
        [_scrollVw setScrollEnabled:NO];
        
    }else if (!CGRectContainsPoint(visibleRect, textFieldOrigin))
    {
        
        CGPoint scrollPoint = CGPointMake(0.0, textFieldOrigin.y - visibleRect.size.height  + textFieldHeight);
        
        [_scrollVw setContentOffset:scrollPoint animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)notification
{
    [_scrollVw setScrollEnabled:YES];
    
    [_scrollVw setContentOffset:CGPointZero animated:YES];
}


@end
