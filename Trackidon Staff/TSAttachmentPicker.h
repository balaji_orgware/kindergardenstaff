//
//  TSAttachmentPicker.h
//  Trackidon Staff
//
//  Created by Elango on 28/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AttachmentPikerDelegate <NSObject>

@required

- (void) selectedAttachmentImageData:(NSData *)imageData imageName:(NSString *)imageName isHasAttachment:(BOOL)isHasAttachment;

- (void) selectedAttachmentImageName:(NSString *)imageName;

@end

@interface TSAttachmentPicker : NSObject <UIActionSheetDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic,strong) id <AttachmentPikerDelegate> delegate;

+(instancetype) instance;

-(id)getCurrentViewController;

- (void)showAttachmentPickerForView:(UIViewController *)viewController frame:(CGRect)frame;

@end
