//
//  TSAttendanceTodayViewController.m
//  Trackidon Staff
//
//  Created by ORGware on 25/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSAttendanceTodayViewController.h"

@interface TSAttendanceTodayViewController ()
{
    BOOL isTopScroll;
    
    float containerViewOriginY;
    
    Board *boardObj;
    
    NSArray *boardArray;
    
    NSMutableArray *studentsListArray,*attendanceUpdateArray;
    
    NSString *selectedBoardTransID,*selectedClassTransID,*selectedSectionTransID;
    
    TSSelectionViewController *selectionView;
}
@end

@implementation StudentModel

@end

@implementation TSAttendanceTodayViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    isTopScroll = YES;
    
    containerViewOriginY = self.containerView.frame.origin.y;
    
    studentsListArray = [NSMutableArray new];
    
    attendanceUpdateArray = [NSMutableArray new];
    
    _studentsTableView.frame = CGRectMake(_studentsTableView.frame.origin.x, _studentsTableView.frame.origin.y, DeviceWidth, _doneView.frame.origin.y - (_noOfStudentsView.frame.origin.y+_noOfStudentsView.frame.size.height));

    NSPredicate *boardPredicate = [NSPredicate predicateWithFormat:@"schoolTransId contains[cd] %@ AND isViewableByStaff == 1",SINGLETON_INSTANCE.selectedSchoolTransId];
    
    boardArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Board" withPredicate:boardPredicate];
    
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_boardTxtFld];
    
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_classTxtFld];
    
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_sectionTxtFld];
    
    [SINGLETON_INSTANCE addLeftInsetToTextField:_boardTxtFld];
    
    [SINGLETON_INSTANCE addLeftInsetToTextField:_classTxtFld];
    
    [SINGLETON_INSTANCE addLeftInsetToTextField:_sectionTxtFld];
    
    [TSSingleton layerDrawForView:_boardTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:_noOfStudentsView position:LAYER_TOP color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:_noOfStudentsView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:_classTxtFld position:LAYER_RIGHT color:[UIColor lightGrayColor]];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_ATTENDANCECREATE parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Create Attendance";
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

#pragma mark - Selection View Methods

// Show Selection View
-(void) showSelectionViewWithSelected:(NSString *)selected andTextField:(UITextField *)textField{
    
    if (!selectionView) {
        
        selectionView = [[TSSelectionViewController alloc]initWithNibName:@"TSSelectionViewController" bundle:nil];
        
        selectionView.delegate = self;
        
        NSMutableDictionary *contentDict = [NSMutableDictionary new];
        
        if (textField == _boardTxtFld) {
            
            for (Board *board in boardArray) {
                
                [contentDict setObject:board.boardTransId     forKey:board.boardName];
            }
        }
        else if (textField == _classTxtFld){
            
            NSPredicate *filterPred = [NSPredicate predicateWithFormat:@"boardTransId MATCHES %@",selectedBoardTransID];
            
            NSArray *filteredArray = [NSMutableArray arrayWithArray: [boardArray filteredArrayUsingPredicate:filterPred]];
            
            boardObj = [filteredArray objectAtIndex:0];
            
            for (Classes *classObj in boardObj.classes) {
                
                [contentDict setObject:classObj.classTransId forKey:classObj.classStandardName];
            }

        }
        else if(textField == _sectionTxtFld){
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"classTransId MATCHES %@",selectedClassTransID];
            
            NSSet *filteredSet = [boardObj.classes filteredSetUsingPredicate:predicate];
            
            Classes *classObj = [[filteredSet allObjects] objectAtIndex:0];
            
            for (Section *secObj in classObj.section) {
                
                [contentDict setObject:secObj.sectionTransId forKey:secObj.sectionName];
            }
        }
        
        if (contentDict.count>0) {
            
            [selectionView setContentDict:contentDict];
            
            selectionView.selectedValue = textField.text.length>0? textField.text:@"";
            
            [selectionView showSelectionViewController:self forTextField:textField];
        }
        else{
            
            if (textField == _boardTxtFld) {
                
                [TSSingleton showAlertWithMessage:@"No Boards to choose"];
            }
            else if (textField == _classTxtFld){
                
                [TSSingleton showAlertWithMessage:@"No Classes to choose"];
            }
            else if (textField == _sectionTxtFld){
                
                [TSSingleton showAlertWithMessage:@"No Sections to choose"];
            }
            
            [selectionView removeSelectionViewController:self];
            
            selectionView = nil;
        }
    }
}

// Remove SelectionVC from Parent view controller
-(void) removeSelectionView{
    
    [selectionView removeSelectionViewController:self];
    
    selectionView = nil;
}

#pragma mark - Selection Callback Delegate methods

-(void)selectedValueFromDict:(NSDictionary *)contentDict withSelectedValue:(NSString *)selectedTransID andTransID:(NSString *)transID forTextField:(UITextField *)textField{
    
    if (textField == _boardTxtFld) {
        
        selectedBoardTransID = transID;
    }
    else if (textField == _classTxtFld){
        
        selectedClassTransID = transID;
    }
    else if (textField == _sectionTxtFld){
        
        NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
        
        [analyticsDict setValue:_boardTxtFld.text forKey:ANALYTICS_PARAM_BOARDNAME];
        
        [analyticsDict setValue:_classTxtFld.text forKey:ANALYTICS_PARAM_CLASSNAME];
        
        [analyticsDict setValue:_sectionTxtFld.text forKey:ANALYTICS_PARAM_SECNAME];
        
        [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_CLICKED_CLASSSELECTION parameter:analyticsDict];
        
        selectedSectionTransID = transID;
        
        [self fetchTodayAttendanceRecordsAPICall];
    }
    
    selectionView = nil;
}

-(void)backViewTappedOn:(UIViewController *)viewController{
        
    selectionView = nil;
}

#pragma mark - Button Actions

// Done Button Tap Action
- (IBAction)doneButtonAction:(id)sender {
    
    if ([self.studentsTableView numberOfRowsInSection:0]>0)
    {
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Confirmation ?" message:@"Do you want to save ?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
        
        [alertView setDelegate:self];
        
        [alertView show];
    }
    else{
        
        [TSSingleton showAlertWithMessage:@"Nothing to update"];
    }
}

#pragma mark - Alert view Delegate methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        
        NSMutableDictionary *param = [NSMutableDictionary new];
        
        [param setValue:SINGLETON_INSTANCE.academicTransId forKey:@"AcadamicTransID"];
        
        [param setValue:SINGLETON_INSTANCE.selectedSchoolTransId forKey:@"SchoolTransID"];
        
        [param setValue:selectedClassTransID forKey:@"ClassTransID"];
        
        [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"UserTransID"];
        
        [param setValue:selectedBoardTransID forKey:@"BoardTransID"];
        
        [param setValue:attendanceUpdateArray forKey:@"AttendanceClass"];
        
        NSLog(@"%@",param);

        [self updateAttendanceDataWithParam:param];
    }
    
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

#pragma mark - Textfield Delegate methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    [attendanceUpdateArray removeAllObjects];
    
    [studentsListArray removeAllObjects];
    
    [self.studentsTableView reloadData];
    
    if(!selectionView){
        
        if (textField == _boardTxtFld) {
            
            _classTxtFld.text = @"";
            
            _sectionTxtFld.text = @"";
            
            [self showSelectionViewWithSelected:textField.text andTextField:textField];
            
//            _boardTxtFld.text = @"";

        }
        else if (textField == _classTxtFld) {
            
            if (_boardTxtFld.text.length>0) {
            
                _sectionTxtFld.text = @"";
                
                [self showSelectionViewWithSelected:textField.text andTextField:textField];
                
//                _classTxtFld.text = @"";
            }
        }
        else{
            
            if (_boardTxtFld.text.length>0 && _classTxtFld.text.length >0) {
                
                [self showSelectionViewWithSelected:textField.text andTextField:textField];
                
//                _sectionTxtFld.text = @"";
            }
        }
    }
    
    return NO;
}

#pragma mark - Scroll View Delegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView == _studentsTableView && [self.studentsTableView numberOfRowsInSection:0]>5) {
        
        if (scrollView.contentOffset.y>0) {
            
            if (isTopScroll) {
                
                CGRect newContainerFrame = self.containerView.frame;
                CGRect newStudentTableFrame = self.studentsTableView.frame;
                
                newContainerFrame.origin.x = self.containerView.frame.origin.x;
                newContainerFrame.origin.y = self.containerView.frame.origin.y - scrollView.contentOffset.y/2.0;
                [self.containerView setFrame:newContainerFrame];
                
                newStudentTableFrame.origin.x = self.studentsTableView.frame.origin.x;
                newStudentTableFrame.origin.y = self.studentsTableView.frame.origin.y-scrollView.contentOffset.y/2.0;
                newStudentTableFrame.size.height = DeviceHeight-self.doneView.frame.size.height-64-newStudentTableFrame.origin.y;

                [self.studentsTableView setFrame: newStudentTableFrame];
                
                if (newStudentTableFrame.origin.y < containerViewOriginY) {
                    
                    newStudentTableFrame.origin.y = containerViewOriginY;
                    newStudentTableFrame.origin.x = 0;
                    newStudentTableFrame.size.height = DeviceHeight - 64 - self.doneView.frame.size.height;
                    [self.studentsTableView setFrame:newStudentTableFrame];
                    isTopScroll = NO;
                }
            }
        }
        else if(scrollView.contentOffset.y<0){
            
//            if (!isTopScroll) {
            
                isTopScroll = YES;
            
                [UIView animateWithDuration:0.3 animations:^{
                
                [self.containerView setHidden:NO];
                    
                [self.containerView setFrame:CGRectMake(0, 0, DeviceWidth, self.containerView.frame.size.height)];
                    
                [self.studentsTableView setFrame:CGRectMake(0, self.containerView.frame.origin.y+self.containerView.frame.size.height, DeviceWidth, DeviceHeight -64 -self.doneView.frame.size.height - self.containerView.frame.size.height)];

                    
//                [self.studentsTableView setFrame:CGRectMake(0, self.containerView.frame.origin.y+self.containerView.frame.size.height, DeviceWidth, self.doneView.frame.origin.y - (self.containerView.frame.origin.y+self.containerView.frame.size.height))];
                }];
            
//            }
        }
    }
}

#pragma mark - Tableview Datasources & Delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    _totalStudentsCountLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)studentsListArray.count];
    
    return studentsListArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    UILabel *studentName = (UILabel *)[cell viewWithTag:100];
    
    UIImageView *statusImgVw = (UIImageView *)[cell viewWithTag:101];
    
    StudentModel *studentObj = [studentsListArray objectAtIndex:indexPath.row];
    
    studentName.text = studentObj.studentName;
    
    switch (studentObj.attendanceStatus) {
            
        case ATTENDANCE_PRESENT:
            
            statusImgVw.image = [UIImage imageNamed:@"present_toggle"];
            break;
            
        case ATTENDANCE_ABSENT:
            
            statusImgVw.image = [UIImage imageNamed:@"absent_toggle"];
            break;
            
        case ATTENDANCE_LATE:
            
            statusImgVw.image = [UIImage imageNamed:@"tardy_toggle"];
            break;
            
        default:
            break;
    }

    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    StudentModel *studentObj = [studentsListArray objectAtIndex:indexPath.row];
    
    NSMutableDictionary *paramDict = [attendanceUpdateArray objectAtIndex:indexPath.row];

    switch (studentObj.attendanceStatus) {
            
        case ATTENDANCE_PRESENT:
            
            studentObj.attendanceStatus = ATTENDANCE_ABSENT;
            
            [paramDict setValue:[NSString stringWithFormat:@"%ld",(long)ATTENDANCE_ABSENT] forKey:@"AttendanceStatus"];
            [paramDict setValue:@"0" forKey:@"Present"];
            [paramDict setValue:@"1" forKey:@"Absent"];
            [paramDict setValue:@"0" forKey:@"Tardy"];
            
            break;
            
        case ATTENDANCE_ABSENT:
            
            studentObj.attendanceStatus = ATTENDANCE_LATE;
            
            [paramDict setValue:[NSString stringWithFormat:@"%ld",(long)ATTENDANCE_LATE] forKey:@"AttendanceStatus"];
            [paramDict setValue:@"0" forKey:@"Present"];
            [paramDict setValue:@"0" forKey:@"Absent"];
            [paramDict setValue:@"1" forKey:@"Tardy"];
            
            break;
            
        case ATTENDANCE_LATE:
            
            studentObj.attendanceStatus = ATTENDANCE_PRESENT;
            
            [paramDict setValue:[NSString stringWithFormat:@"%ld",(long)ATTENDANCE_PRESENT] forKey:@"AttendanceStatus"];
            [paramDict setValue:@"1" forKey:@"Present"];
            [paramDict setValue:@"0" forKey:@"Absent"];
            [paramDict setValue:@"0" forKey:@"Tardy"];
            break;
            
        default:
            break;
    }
    
    [self updateStudentAttendanceCounts];
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 106;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 106)];
    
    float countLabelWidth = (headerView.frame.size.width/3)-30;
    
    {
        self.presentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth/3, headerView.frame.size.height)];
        [self.presentView setBackgroundColor:[UIColor whiteColor]];
        
        self.presentImgView = [[UIImageView alloc]initWithFrame:CGRectMake(self.presentView.center.x-25, self.presentView.center.y -40, 50, 50)];
        self.presentImgView.image = [UIImage imageNamed:@"present"];
        
        self.presentCountLabel = [[UILabel alloc]initWithFrame:CGRectMake((DeviceWidth/3-countLabelWidth)/2, self.presentImgView.frame.origin.y+self.presentImgView.frame.size.height+10,countLabelWidth, 21)];
        [self.presentCountLabel setTextColor:COLOR_ACCEPT_CREATEDTODAY];
        [self.presentCountLabel setTextAlignment:NSTextAlignmentCenter];
        [self.presentCountLabel setFont:[UIFont fontWithName:APP_FONT size:17]];
        
        [self.presentView addSubview:self.presentImgView];
        [self.presentView addSubview:self.presentCountLabel];
    }
    {
        self.absentView = [[UIView alloc]initWithFrame:CGRectMake(DeviceWidth/3, 0, DeviceWidth/3, headerView.frame.size.height)];
        [self.absentView setBackgroundColor:[UIColor whiteColor]];
        
        self.absentImgView = [[UIImageView alloc]initWithFrame:CGRectMake((self.absentView.frame.size.width-50)/2, self.absentView.center.y -40, 50, 50)];
        self.absentImgView.image = [UIImage imageNamed:@"absent"];
        
        
        self.absentCountLabel = [[UILabel alloc]initWithFrame:CGRectMake((DeviceWidth/3-countLabelWidth)/2, self.absentImgView.frame.origin.y+self.absentImgView.frame.size.height+10,countLabelWidth, 21)];
        [self.absentCountLabel setTextColor:COLOR_DECLINED_DUESTODAY];
        [self.absentCountLabel setTextAlignment:NSTextAlignmentCenter];
        [self.absentCountLabel setFont:[UIFont fontWithName:APP_FONT size:17]];
        
        [self.absentView addSubview:self.absentImgView];
        [self.absentView addSubview:self.absentCountLabel];
    }
    {
        self.lateView = [[UIView alloc]initWithFrame:CGRectMake(2*(DeviceWidth/3), 0, DeviceWidth/3, headerView.frame.size.height)];
        [self.lateView setBackgroundColor:[UIColor whiteColor]];
        
        self.lateImgView = [[UIImageView alloc]initWithFrame:CGRectMake((self.absentView.frame.size.width-50)/2, self.lateView.center.y -40, 50, 50)];
        
        self.lateImgView.image = [UIImage imageNamed:@"late"];
        
        self.lateCountLabel = [[UILabel alloc]initWithFrame:CGRectMake((DeviceWidth/3-countLabelWidth)/2, self.lateImgView.frame.origin.y+self.lateImgView.frame.size.height+10,countLabelWidth, 21)];
        [self.lateCountLabel setTextColor:COLOR_PENDING_VIEWALL];
        [self.lateCountLabel setTextAlignment:NSTextAlignmentCenter];
        [self.lateCountLabel setFont:[UIFont fontWithName:APP_FONT size:17]];
        
        [self.lateView addSubview:self.lateImgView];
        [self.lateView addSubview:self.lateCountLabel];
    }
    
    [self updateStudentAttendanceCounts];
    
    [headerView addSubview:self.presentView];
    
    [headerView addSubview:self.absentView];
    
    [headerView addSubview:self.lateView];
    
    return headerView;
}

#pragma mark - Attendance Count Update Method

// Method to Get Total Attendance Status Counts for Present, Absent, Late
-(void) updateStudentAttendanceCounts{
    
    NSPredicate *presentPredicate = [NSPredicate predicateWithFormat:@"attendanceStatus == %d",ATTENDANCE_PRESENT];
    
    NSPredicate *absentPredicate = [NSPredicate predicateWithFormat:@"attendanceStatus == %d",ATTENDANCE_ABSENT];
    
    NSPredicate *latePredicate = [NSPredicate predicateWithFormat:@"attendanceStatus == %d",ATTENDANCE_LATE];
    
    self.presentCountLabel.text = [NSString stringWithFormat:@"%ld",(unsigned long)[[studentsListArray filteredArrayUsingPredicate:presentPredicate] count]];
    
    self.absentCountLabel.text = [NSString stringWithFormat:@"%ld",(unsigned long)[[studentsListArray filteredArrayUsingPredicate:absentPredicate] count]];
    
    self.lateCountLabel.text = [NSString stringWithFormat:@"%ld",(unsigned long)[[studentsListArray filteredArrayUsingPredicate:latePredicate] count]];
}

#pragma mark - Service Call

// Fetch Class Students Attendance list api call
-(void) fetchTodayAttendanceRecordsAPICall{
    
    NSString *urlString = @"MastersMServices/AttendanceService.svc/ClassAttendanceByDate";
    
    NSString *dateString = [SINGLETON_INSTANCE stringFromDateWithFormate:DATE_FORMATE_SERVICE_CALL withDate:[NSDate date]];
    
    NSDictionary *params = @{@"guidBoardTransID":selectedBoardTransID,@"guidClassTansID":selectedClassTransID,@"guidSectionTransID":selectedSectionTransID,@"DTAttDate":dateString};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if([responseDictionary[@"ClassAttendanceByDateResult"][@"ResponseCode"] intValue] == 1) {
                
                [attendanceUpdateArray removeAllObjects];
                
                [studentsListArray removeAllObjects];
                    
                for (NSDictionary *studDict in responseDictionary[@"ClassAttendanceByDateResult"][@"AttendanceClassData"]) {
                    
                    StudentModel *studentObj = [StudentModel new];
                    
                    studentObj.enrollNo = studDict[@"EnrollNo"];
                    studentObj.studentName = studDict[@"NAME"];
                    studentObj.studentAbsent = [studDict[@"StudAbsent"] boolValue];
                    studentObj.studentImage = studDict[@"StudImage"];
                    studentObj.studentPresent = [studDict[@"StudPresent"] boolValue];
                    studentObj.studentTardy = [studDict[@"StudTardy"] boolValue];
                    studentObj.studentTransID = studDict[@"StudentTransID"];
                    studentObj.transID = studDict[@"TransId"];
                    
                    if ([studDict[@"StudAbsent"] boolValue]){
                        
                        studentObj.attendanceStatus = ATTENDANCE_ABSENT;
                    }
                    else if ([studDict[@"StudTardy"] boolValue]){
                        
                        studentObj.attendanceStatus = ATTENDANCE_LATE;
                    }
                    else{
                        
                        studentObj.attendanceStatus = ATTENDANCE_PRESENT;
                    }
                    
                    {
                        NSMutableDictionary *param = [NSMutableDictionary new];
                        
                        [param setValue:studDict[@"StudentTransID"] forKey:@"StudentTransID"];
                        
                        [param setValue:[NSString stringWithFormat:@"%ld",(long)studentObj.attendanceStatus] forKey:@"AttendanceStatus"];
                        
                        [param setValue:studDict[@"TransId"] forKey:@"TransId"];
                        
                        [param setValue:studDict[@"StudPresent"] forKey:@"Present"];
                        
                        [param setValue:studDict[@"StudAbsent"] forKey:@"Absent"];
                        
                        [param setValue:studDict[@"StudTardy"] forKey:@"Tardy"];
                        
                        [attendanceUpdateArray addObject:param];
                    }
                    
                    [studentsListArray addObject:studentObj];
                    
                    studentObj = nil;
                }
            }
            else{
                
                [attendanceUpdateArray removeAllObjects];
                
                [studentsListArray removeAllObjects];
                
                [TSSingleton showAlertWithMessage:@"Error fetching records"];
            }
            
            [self.studentsTableView reloadData];
        }
    }];
}

// Update Attendance Service Call
-(void) updateAttendanceDataWithParam:(NSMutableDictionary *)param{
    
    NSString *urlString = @"MastersMServices/AttendanceService.svc/UpdateClassAttendance";
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"UpdateClassAttendanceResult"][@"ResponseCode"] intValue] == 1) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:_boardTxtFld.text forKey:ANALYTICS_PARAM_BOARDNAME];
                
                [analyticsDict setValue:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_CLICKED_UPDATE parameter:analyticsDict];
                
//                [self fetchTodayAttendanceRecordsAPICall];
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            else{
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:_boardTxtFld.text forKey:ANALYTICS_PARAM_BOARDNAME];
                
                [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_CLICKED_UPDATE parameter:analyticsDict];
            }
            
            [TSSingleton showAlertWithMessage:responseDictionary[@"UpdateClassAttendanceResult"][@"ResponseMessage"]];
        }
        else{
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            [analyticsDict setValue:_boardTxtFld.text forKey:ANALYTICS_PARAM_BOARDNAME];
            
            [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
            
            [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_CLICKED_UPDATE parameter:analyticsDict];
        }
    }];
}
@end