//
//  NotificationInbox+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 28/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "NotificationInbox+CoreDataProperties.h"

@implementation NotificationInbox (CoreDataProperties)

@dynamic assignedList;
@dynamic createdBy;
@dynamic isHasAttachment;
@dynamic modifiedDate;
@dynamic notificationAttachment;
@dynamic notificationDate;
@dynamic notificationDescription;
@dynamic notificationTitle;
@dynamic priority;
@dynamic staffTransID;
@dynamic transID;
@dynamic staff;

@end
