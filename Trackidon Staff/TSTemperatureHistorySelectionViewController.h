//
//  TSTemperatureHistorySelectionViewController.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 13/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSTemperatureHistorySelectionViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,SelectionCallBackDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *selectionView;

@property (weak, nonatomic) IBOutlet UITextField *boardTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *classTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *sectionTxtFld;

@property (weak, nonatomic) IBOutlet UITableView *studentTableView;

@end
