//
//  BoardList+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 13/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BoardList.h"

NS_ASSUME_NONNULL_BEGIN

@interface BoardList (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *boardTransId;
@property (nullable, nonatomic, retain) NSString *schoolTransId;
@property (nullable, nonatomic, retain) StaffDetails *staff;

@end

NS_ASSUME_NONNULL_END
