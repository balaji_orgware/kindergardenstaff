//
//  TSCommonCreateEyeEarNoseViewController.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 09/08/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSCommonCreateEyeEarNoseViewController : UIViewController <UITextFieldDelegate,UITextViewDelegate,UIGestureRecognizerDelegate,datePickerDelegate,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *examineDateView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UIView *examinedByView;
@property (weak, nonatomic) IBOutlet UITextField *examinedByTxtFld;

@property (weak, nonatomic) IBOutlet UIView *firstInfoView;
@property (weak, nonatomic) IBOutlet UILabel *firstInfoLblName;
@property (weak, nonatomic) IBOutlet UITextField *firstInfoTxtFld;
@property (weak, nonatomic) IBOutlet UITextView *firstInfoTxtVw;

@property (weak, nonatomic) IBOutlet UIView *secondInfoView;
@property (weak, nonatomic) IBOutlet UILabel *secondInfoLblName;
@property (weak, nonatomic) IBOutlet UITextField *secondInfoTxtFld;
@property (weak, nonatomic) IBOutlet UITextView *secondInfoTxtVw;

@property (weak, nonatomic) IBOutlet UIView *doneView;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

- (IBAction)doneButtonTapped:(id)sender;

@property (nonatomic,strong) NSDictionary *generalInfoDict;
@property (nonatomic,assign) NSInteger healthType;

@end
