//
//  TSSelectionViewController.h
//  Trackidon Staff
//
//  Created by ORGware on 22/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
@protocol SelectionCallBackDelegate;

#import <UIKit/UIKit.h>
#import "TSHeaders.pch"

@protocol SelectionCallBackDelegate <NSObject>

-(void) selectedValueFromDict:(NSDictionary *)contentDict withSelectedValue:(NSString *)selectedValue andTransID:(NSString *)transID forTextField:(UITextField *)textField;

-(void) backViewTappedOn:(UIViewController *)viewController;

@end

@interface TSSelectionViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *backView;

@property (nonatomic,strong) UITextField *textField;

@property (nonatomic,strong) NSArray *contentArray;

@property (nonatomic,strong) NSMutableDictionary *contentDict;

@property (nonatomic,strong) NSString *selectedValue;

@property (nonatomic,assign) id<SelectionCallBackDelegate> delegate;

-(void) showSelectionViewController:(UIViewController *)viewController forTextField:(UITextField *)textField;

-(void)removeSelectionViewController:(UIViewController *)viewController;
@end
