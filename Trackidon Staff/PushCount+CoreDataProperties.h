//
//  PushCount+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 19/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PushCount.h"

NS_ASSUME_NONNULL_BEGIN

@interface PushCount (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *isRead;
@property (nullable, nonatomic, retain) NSString *commTypeID;
@property (nullable, nonatomic, retain) NSString *transId;
@property (nullable, nonatomic, retain) NSString *unreadCount;
@property (nullable, nonatomic, retain) NSString *staffTransId;
@property (nullable, nonatomic, retain) NSNumber *isForApproval;
@property (nullable, nonatomic, retain) StaffDetails *staff;

@end

NS_ASSUME_NONNULL_END
