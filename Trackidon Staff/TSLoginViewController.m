//
//  TSLoginViewController.m
//  Trackidon Staff
//
//  Created by ORGware on 16/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSLoginViewController.h"

@interface TSLoginViewController ()
{
    NSArray *numbersArray;
}
@end

@implementation TSLoginViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    numbersArray = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
    
    _usernameTxtFld.layer.cornerRadius = _usernameTxtFld.frame.size.height/2;
    
    _passwordTxtFld.layer.cornerRadius = _passwordTxtFld.frame.size.height/2;
    
    NSArray *trustArr = [SINGLETON_INSTANCE retrieveAllObjectsInEntity:@"Trust"];
    
    if (trustArr.count >0) {
        
        Trust *trust = [trustArr objectAtIndex:0];
        
        if(trust.schoolLogoData != nil)
            [_logoImgView setImage:[UIImage imageWithData:trust.schoolLogoData]];
        
    }
    
    
    if (!IS_IPHONE_4) {
        
        _goButton.center = CGPointMake(_redView.center.x, _redView.frame.origin.y);
        
        _forgotPwdButton.center = CGPointMake(_goButton.center.x, _goButton.frame.origin.y -30);
        
        _passwordTxtFld.center = CGPointMake(_forgotPwdButton.center.x, _forgotPwdButton.frame.origin.y -40);
        
        _usernameTxtFld.center = CGPointMake(_passwordTxtFld.center.x, _passwordTxtFld.frame.origin.y -40);
        
    }
    
    NSArray * objects = [[NSArray alloc] initWithObjects:self.forgotPwdButton.titleLabel.textColor, [NSNumber numberWithInt:NSUnderlineStyleSingle], nil];
    NSArray * keys = [[NSArray alloc] initWithObjects:NSForegroundColorAttributeName, NSUnderlineStyleAttributeName, nil];
    
    NSDictionary * linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    [self.forgotPwdButton.titleLabel setAttributedText:[[NSAttributedString alloc] initWithString:self.forgotPwdButton.titleLabel.text attributes:linkAttributes]];
    
    float logoYPos = ((self.view.frame.origin.y + _usernameTxtFld.frame.origin.y) - _logoImgView.frame.size.height)/2;
    
    _logoImgView.frame = CGRectMake(_logoImgView.frame.origin.x, logoYPos, _logoImgView.frame.size.width, _logoImgView.frame.size.height);
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_LOGIN parameter:[NSMutableDictionary new]];
    
    self.navigationController.navigationBarHidden = YES;
    
//    _usernameTxtFld.text = @"1313131313";
//    _usernameTxtFld.text = @"9944165755";
//    _passwordTxtFld.text = @"test1234";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Touch methods

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
}

#pragma mark - Textfield Delegates

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == _usernameTxtFld){
        [_passwordTxtFld becomeFirstResponder];
    }else{
        [_passwordTxtFld resignFirstResponder];
    }
    return YES;
}


#pragma mark - Button Actions

// Sign In Button Action
- (IBAction)goButtonAction:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([SINGLETON_INSTANCE isEmpty:_usernameTxtFld.text] && [SINGLETON_INSTANCE isEmpty:_passwordTxtFld.text]) {
        
        [SINGLETON_INSTANCE showAlertMessageWithTitle:@"Error" andMessage:@"Please enter Email Address/Mobile no and Password"];
        
        return;
    }
    else if ([SINGLETON_INSTANCE isEmpty:_usernameTxtFld.text]){
        
        [SINGLETON_INSTANCE showAlertMessageWithTitle:INCORRECT_LOGIN_TITLE andMessage:INCORRECT_USERNAME_MESSAGE];
        
        return;
    }
    else if ([SINGLETON_INSTANCE isEmpty:_passwordTxtFld.text]){
        
        [SINGLETON_INSTANCE showAlertMessageWithTitle:INCORRECT_LOGIN_TITLE andMessage:INCORRECT_PASSWORD_MESSAGE];
        
        return;
    }
    else{
        
        if([numbersArray containsObject:[_usernameTxtFld.text substringToIndex:1]]){ // If typed is Mobile no.
            
            if (![SINGLETON_INSTANCE isValidMobileNo:_usernameTxtFld.text]){
                
                [SINGLETON_INSTANCE showAlertMessageWithTitle:INCORRECT_LOGIN_TITLE andMessage:@"Please enter valid mobile no"];
                
                return;
            }
        }
        else{
            
            if (![SINGLETON_INSTANCE isValidEmailAddress:_usernameTxtFld.text]){
                
                [SINGLETON_INSTANCE showAlertMessageWithTitle:INCORRECT_LOGIN_TITLE andMessage:@"Please enter valid Email Address"];
                
                return;
            }
        }
    }
    
    [self loginServiceCallWithUserName:_usernameTxtFld.text andPassword:_passwordTxtFld.text];
    
}

// Forgot Password Button Tap Action
- (IBAction)forgotPwdButtonAction:(id)sender {
    
    [TSAnalytics logEvent:ANALYTICS_LOGIN_CLIKED_FORGOT_PASSWORD parameter:[NSMutableDictionary new]];
    
    TSForgotPasswordViewController *forgotPwdVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBForgotPassword"];
    
    [self.navigationController pushViewController:forgotPwdVC animated:YES];
    
    //[self presentViewController:forgotPwdVC animated:YES completion:nil];
}


#pragma mark - Service Calls

// Login Service API Call - FetchAccountsLogin
-(void) loginServiceCallWithUserName:(NSString *)username andPassword:(NSString *)password{
    
    NSString *loginURL = @"MastersMServices/AccountsMService.svc/FetchAccountLogin";//@"CommonApi/MastersMServices/CommonUsersService.svc/FetchAccountsLogin";
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    [param setValue:_usernameTxtFld.text forKey:@"strUsername"];
    
    [param setValue:_passwordTxtFld.text forKey:@"strPassword"];
    
    [param setValue:@"8" forKey:@"intLoginType"];
    
    [param setValue:@"2" forKey:@"intDeviceType"];
    
    [param setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"deviceToken"] forKey:@"strDeviceToken"];
    
    NSLog(@"%@",param);
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:loginURL baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if(success){
            
            if ([responseDictionary[@"FetchAccountLoginResult"][@"ResponseCode"] intValue] == 1) {
                
                @try {
                    
                    [[StaffDetails instance]saveStaffDetailsWithDict:responseDictionary];
                    
                    [USERDEFAULTS setObject:_passwordTxtFld.text forKey:USER_PASSWORD];
                    
                    [USERDEFAULTS setObject:_usernameTxtFld.text forKey:@"LoginId"];
                    
                    [USERDEFAULTS setObject:responseDictionary[@"FetchAccountLoginResult"][@"SelectAccountClass"][@"UserDetails"][@"Name"] forKey:USERNAME];

                    
                    [USERDEFAULTS setObject:responseDictionary[@"FetchAccountLoginResult"][@"SelectAccountClass"][@"UserDetails"][@"TransID"] forKey:@"staffTransId"];
                    
                    [USERDEFAULTS setValue:@"" forKey:@"academicTransId"];
                    
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@",[USERDEFAULTS valueForKey:@"staffTransId"]];
                    
                    NSArray *schoolListArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"School" withPredicate:predicate];
                    
                    //Analytics
                    
                    NSMutableDictionary *analyticDict = [NSMutableDictionary new];
                    
                    [analyticDict setObject:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_RESPONSE];
                    
                    [analyticDict setObject:responseDictionary[@"FetchAccountLoginResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                    
                    [analyticDict setObject:[NSString stringWithFormat:@"%lu",[schoolListArr count]] forKey:ANALYTICS_PARAM_SCHOOLCOUNT];
                    
                    [TSAnalytics logEvent:ANALYTICS_LOGIN_SUBMIT_LOGIN parameter:analyticDict];
                    
                    if (schoolListArr.count > 1) {
                        
                        TSSchoolListViewController *schoolListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBSchoolList"];
                        
                        [self.navigationController pushViewController:schoolListVC animated:YES];
                        
                    }else if(schoolListArr.count > 0 ){
                        
                        [USERDEFAULTS setBool:YES forKey:@"isLogin"];
                        
                        School *school = [schoolListArr objectAtIndex:0];
                        
                        [USERDEFAULTS setObject:school.schoolTransId  forKey:@"schoolTransId"];
                        
                         [USERDEFAULTS setObject:school.schoolName  forKey:@"schoolName"];
                        
                        SINGLETON_INSTANCE.selectedSchoolTransId = school.schoolTransId;
                        
                        [APPDELEGATE_INSTANCE initilizeInitialViewController];
                        
                    }
                }
                @catch (NSException *exception) {
                    
                    NSLog(@"%@",exception);
                    
                }
            } else{
                
                NSMutableDictionary *analyticDict = [NSMutableDictionary new];
                
                [analyticDict setObject:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_RESPONSE];
                
                [analyticDict setObject:responseDictionary[@"FetchAccountLoginResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                
                [TSAnalytics logEvent:ANALYTICS_LOGIN_SUBMIT_LOGIN parameter:analyticDict];
                
                [SINGLETON_INSTANCE showAlertMessageWithTitle:INCORRECT_LOGIN_TITLE andMessage:INCORRECT_LOGIN_MESSAGE];
            }
        } else {
            
            NSMutableDictionary *analyticDict = [NSMutableDictionary new];
            
            [analyticDict setObject:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_RESPONSE];
            
            [analyticDict setObject:ANALYTICS_VALUE_SERVICEFAILURE_RESPONSEMSG forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
            
            [TSAnalytics logEvent:ANALYTICS_LOGIN_SUBMIT_LOGIN parameter:analyticDict];
            
        }
    }];
}
@end
