//
//  TSHeightWeightDescViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 21/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSHeightWeightDescViewController.h"

@interface TSHeightWeightDescViewController ()

@end

@implementation TSHeightWeightDescViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);
    
    _descView.frame = CGRectMake(_descView.frame.origin.x, _descView.frame.origin.y, DeviceWidth, _descView.frame.size.height);
    _dateView.frame = CGRectMake(_dateView.frame.origin.x, _dateView.frame.origin.y, DeviceWidth, _dateView.frame.size.height);
    _backView.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);

    UITapGestureRecognizer *tapOnBackVw = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOnBackView)];
    [_backView addGestureRecognizer:tapOnBackVw];
    
    [TSSingleton layerDrawForView:_dateView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Helper Methods

-(void) tapOnBackView{
    
    [self hideAnimationView];
}

-(void) loadContentViewWithModelObj:(BMIInfoModel *)modelObj{
    
    _dateLbl.text = [SINGLETON_INSTANCE stringFromDate:modelObj.examinedDate withFormate:DATE_FORMATE_MNTH_DAY_YR];
    
    _heightLbl.text = [NSString stringWithFormat:@"%@ cm",modelObj.height];
    _weightLbl.text = [NSString stringWithFormat:@"%@ kg",modelObj.weight];
    _remarksTxtVw.text = [modelObj.remarks isEqualToString:@""] ? @"---":modelObj.remarks;
    
    _remarksTxtVw.font = [UIFont fontWithName:APP_FONT size:15];
}

-(void) showDescViewController:(UIViewController *)viewController withModelObj:(BMIInfoModel *)modelObj{
    
    [viewController.view addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    [self loadContentViewWithModelObj:modelObj];
    
    [viewController.view endEditing:YES];
    
    _descView.frame = CGRectMake(_descView.frame.origin.x, DeviceHeight, DeviceWidth,viewController.view.frame.size.height*3.1/4);
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [_backView setAlpha:0.5];
        _descView.frame = CGRectMake(_descView.frame.origin.x, viewController.view.frame.size.height-_descView.frame.size.height, _descView.frame.size.width, _descView.frame.size.height);
    }];
}


-(void) hideAnimationView{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [_backView setAlpha:0.0];
        _descView.frame = CGRectMake(_descView.frame.origin.x, DeviceHeight, _descView.frame.size.width, _descView.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

@end