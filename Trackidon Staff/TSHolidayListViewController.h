//
//  TSHolidayListViewController.h
//  Trackidon Staff
//
//  Created by Elango on 23/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSHolidayListViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *holidayListTableView;

@property (nonatomic,strong) NSArray *holidaysListArray;

@property (nonatomic,strong) NSString *holidayGroupTitle;


@end
