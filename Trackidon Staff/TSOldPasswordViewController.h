//
//  TSOldPasswordViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 29/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSHeaders.pch"

@interface TSOldPasswordViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTextField;

@property (weak, nonatomic) IBOutlet UIButton *changePasswordButton;

- (IBAction)changePasswordButtonAction:(id)sender;

@end
