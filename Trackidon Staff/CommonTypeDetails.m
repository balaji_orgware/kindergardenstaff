//
//  CommonTypeDetails.m
//  Trackidon Staff
//
//  Created by Balaji on 07/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "CommonTypeDetails.h"

@implementation CommonTypeDetails

// Method to get Instance of CommonTypeDetails
+(instancetype) instance{
    
    static CommonTypeDetails *obj = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        obj = [CommonTypeDetails new];
    });
    
    return obj;
}

// Method to store details with Dict
-(void)storeTypeDetailsWithDict:(NSDictionary *)dictionary{
    
    NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
    
    NSError *error = nil;
    
    for (NSDictionary *typeDict in dictionary[@"FetchCommonmasterResult"][@"MasterClass"])  {
        
        NSDictionary *dict = [typeDict dictionaryByReplacingNullsWithBlanks];
        
        if ([dict[@"TypeID"] integerValue] == TypeID_Subject || [dict[@"TypeID"] integerValue] == TypeID_AssignmentType) {
            
            CommonTypeDetails *detailObj = [self isTypeDetailsAlreadyExistsWithTransID:dict[@"TransID"] withContext:context];
            
            if (detailObj == nil) {
                
                detailObj = [NSEntityDescription insertNewObjectForEntityForName:@"CommonTypeDetails" inManagedObjectContext:context];
            }
            
            detailObj.typeID = dict[@"TypeID"];
            detailObj.transID = dict[@"TransID"];
            detailObj.name = dict[@"Description"];    
        }
    }
    
    if (![context save:&error]) {
        
        NSLog(@"Failed to save data");
    }
}

-(CommonTypeDetails *) isTypeDetailsAlreadyExistsWithTransID:(NSString *)transID withContext:(NSManagedObjectContext *)context{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CommonTypeDetails" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transID contains[cd] %@", transID];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
}

@end
