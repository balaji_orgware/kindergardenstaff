//
//  TSNotificationDescriptionViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/29/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSNotificationDescriptionViewController.h"

@interface TSNotificationDescriptionViewController ()
{
    NSURL *attachmentURL;
    
    Notifications *notificationsObj;
    
    NotificationInbox *notificationInboxObj;
    
    NSArray *assigneeArr;
    
    UITapGestureRecognizer *assignedViewTap;
}
@end

@implementation TSNotificationDescriptionViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [TSSingleton layerDrawForView:_topView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:_dateView position:LAYER_TOP color:[UIColor lightGrayColor]];
    
    _attachmentButton.frame = CGRectMake(DEVICE_SIZE.width - _attachmentButton.frame.size.width - 10, _topView.frame.origin.y + _topView.frame.size.height - _attachmentButton.frame.size.height/2 + 18, _attachmentButton.frame.size.height, _attachmentButton.frame.size.height);
    
    [TSSingleton layerDrawForView:_assignedView position:LAYER_TOP color:[UIColor lightGrayColor]];
    
//    _assignedView.center = CGPointMake(_dateView.center.x, _dateView.frame.origin.y-_assignedView.frame.size.height);
    
    assignedViewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(assignedViewTapped)];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(descViewTapped)];
    
    [_descriptionTextView addGestureRecognizer:tapGesture];
    
    if (SINGLETON_INSTANCE.isPushNotification) {
        
        SINGLETON_INSTANCE.isPushNotification = NO;
        
        [self fetchPushNotificationAPICall];
    }
    else{
        
        if ([_descriptionType isEqualToString:NotificationsString]) {
            
            notificationsObj = (Notifications *)self.commonObj;
            
            [self loadViewWithNotification:notificationsObj];
            
        }else if ([_descriptionType isEqualToString:NotificationsInboxString]){
            
            notificationInboxObj = (NotificationInbox *)self.commonObj;
            
            [self loadViewWithNotificationInbox:notificationInboxObj];
        }
    }
    
    if (IS_IPHONE_4) {
        
        _dateView.frame = CGRectMake(0, DeviceHeight -75, DeviceWidth, 75);
        
        _assignedView.frame = CGRectMake(0, DeviceHeight-_dateView.frame.size.height-40, DeviceWidth, 40);
        
        if (assigneeArr.count == 0) {
            
            _descriptionTextView.frame = CGRectMake(self.descriptionTextView.frame.origin.x, self.descriptionTextView.frame.origin.y, self.descriptionTextView.frame.size.width, 120+_assignedView.frame.size.height);
        }
        else{
            
            _descriptionTextView.frame = CGRectMake(self.descriptionTextView.frame.origin.x, self.descriptionTextView.frame.origin.y, self.descriptionTextView.frame.size.width, 120);
        }
        
        _notificationPriorityView.frame = CGRectMake(_notificationPriorityView.frame.origin.x, 25, 45, 45);
        
        _fromDateLbl.frame = CGRectMake(_fromDateLbl.frame.origin.x, _notificationPriorityView.frame.origin.y, 45, 45);
        
        _notificationPriorityTextLbl.center = CGPointMake(_notificationPriorityTextLbl.center.x, 15);
        
        _createdOnTextLabel.center = CGPointMake(_createdOnTextLabel.center.x, 15);
    }
    
    _fromDateLbl.layer.cornerRadius = _fromDateLbl.frame.size.width/2;
    
    _fromDateLbl.layer.masksToBounds = YES;
    
    _notificationPriorityView.layer.cornerRadius = _notificationPriorityView.frame.size.width/2;
    
    _notificationPriorityView.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    if ([_descriptionType isEqualToString:NotificationsString]){
    
        [TSAnalytics logScreen:ANALYTICS_SCREEN_NOTIFICATIONDECRIPTION parameter:[NSMutableDictionary new]];
        
        self.navigationItem.title = [NSString stringWithFormat:@"%@ Detail",SINGLETON_INSTANCE.barTitle];
    
    }else if ([_descriptionType isEqualToString:NotificationsInboxString]){
        
        [TSAnalytics logScreen:ANALYTICS_SCREEN_INBOXNOTIFICATION_DESCRIPTION           parameter:[NSMutableDictionary new]];
        
        self.navigationItem.title = @"Notification Inbox Detail";
    }
}

#pragma mark - Helper Methods

-(void)loadViewWithNotification:(Notifications *)notification{
    
    _titleLbl.text = [NSString stringWithFormat:@"Title : %@",notification.notificationTitle];
    
    _createdByLbl.text = [NSString stringWithFormat:@"- by %@",notification.userCreatedName];
    
    _descriptionTextView.text = notification.notificationDescription;
    
    NSLog(@"%@",notification.notificationDate);
    
    _fromDateLbl.attributedText = [SINGLETON_INSTANCE getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:notification.notificationDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_fromDateLbl];
    
    switch ([notification.notificationPriority integerValue]) {
            
        case 1:
            
            _notificationPriorityTextLbl.text = @"Priority High";
            
            _notificationPriorityView.backgroundColor = [UIColor colorWithRed:1.0 green:45.0/255.0 blue:85.0/255.0 alpha:1.0];
            
            break;
            
        case 2:
            
            _notificationPriorityTextLbl.text = @"Priority Medium";
            
            _notificationPriorityView.backgroundColor = [UIColor colorWithRed:1.0 green:149.0/255.0 blue:0.0/255.0 alpha:1.0];
            
            break;
            
        case 3:
            
            _notificationPriorityTextLbl.text = @"Priority Low";
            
            _notificationPriorityView.backgroundColor = [UIColor colorWithRed:15.0/255.0 green:170.0/255.0 blue:99.0/255.0 alpha:1.0];
            
            break;
            
        default:
            break;
    }
    
    if ([notification.notificationIsHasAttachment intValue] == 1) {
        
        [_attachmentButton setHidden:NO];
    }
    else{
        
        [_attachmentButton setHidden:YES];
    }
    
    /*NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transId MATCHES %@",notificationsObj.notificationTransId];
     
     assigneeArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"AssignedDetails" withPredicate:predicate];
     
     NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"assigneeType" ascending:YES];
     
     NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
     
     assigneeArr = [assigneeArr sortedArrayUsingDescriptors:descriptors];*/
    
    assigneeArr = [notification.assignedList componentsSeparatedByString:ASSIGNEE_SAPRATOR];

    _assignedView.hidden = NO;
    
    
    if (assigneeArr.count ==1) {
        
        _assignedTxtFld.text = [NSString stringWithFormat:@"%@ : %@",NotificationNotified,[assigneeArr objectAtIndex:0]];
        
    }else if (assigneeArr.count >1){
        
        [self.assignedView addGestureRecognizer:assignedViewTap];
        
        [SINGLETON_INSTANCE addRightViewToTextField:_assignedTxtFld withImageName:@"expand_arrow"];
    }
    else if (assigneeArr.count ==0){
        
        _assignedView.hidden = YES;
    }
}

-(void)loadViewWithNotificationInbox:(NotificationInbox *)notificationInbox{
    
    _titleLbl.text = [NSString stringWithFormat:@"Title : %@",notificationInbox.notificationTitle];
    
    _createdByLbl.text = [NSString stringWithFormat:@"- by %@",notificationInbox.createdBy];
    
    _descriptionTextView.text = notificationInbox.notificationDescription;
    
    _fromDateLbl.attributedText = [SINGLETON_INSTANCE getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:notificationInbox.notificationDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_fromDateLbl];
    
    switch ([notificationInbox.priority integerValue]) {
            
        case 1:
            
            _notificationPriorityTextLbl.text = @"Priority High";
            
            _notificationPriorityView.backgroundColor = [UIColor colorWithRed:1.0 green:45.0/255.0 blue:85.0/255.0 alpha:1.0];
            
            break;
            
        case 2:
            
            _notificationPriorityTextLbl.text = @"Priority Medium";
            
            _notificationPriorityView.backgroundColor = [UIColor colorWithRed:1.0 green:149.0/255.0 blue:0.0/255.0 alpha:1.0];
            
            break;
            
        case 3:
            
            _notificationPriorityTextLbl.text = @"Priority Low";
            
            _notificationPriorityView.backgroundColor = [UIColor colorWithRed:15.0/255.0 green:170.0/255.0 blue:99.0/255.0 alpha:1.0];
            
            break;
            
        default:
            break;
    }
    
    if ([notificationInbox.isHasAttachment intValue] == 1) {
        
        [_attachmentButton setHidden:NO];
    }
    else{
        
        [_attachmentButton setHidden:YES];
    }
    
    /*NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transId MATCHES %@",notificationInboxObj.transID];
     
     assigneeArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"AssignedDetails" withPredicate:predicate];
     
     NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"assigneeType" ascending:YES];
     
     NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
     
     assigneeArr = [assigneeArr sortedArrayUsingDescriptors:descriptors];*/
    
    assigneeArr = [notificationInbox.assignedList componentsSeparatedByString:ASSIGNEE_SAPRATOR];
    
    _assignedView.hidden = NO;
    
    if (assigneeArr.count ==1) {
        
        _assignedTxtFld.text = [NSString stringWithFormat:@"%@ : %@",NotificationNotified,[assigneeArr objectAtIndex:0]];
        
    }else if (assigneeArr.count >1){
        
        [self.assignedView addGestureRecognizer:assignedViewTap];
        
        [SINGLETON_INSTANCE addRightViewToTextField:_assignedTxtFld withImageName:@"expand_arrow"];
    }
    else if (assigneeArr.count ==0){
        
        _assignedView.hidden = YES;
    }
}

-(void)assignedViewTapped{
    
    if ([_descriptionType isEqualToString:NotificationsString]){
        
        [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_VIEW_RECEIVER parameter:[NSMutableDictionary new]];
        
    }else if ([_descriptionType isEqualToString:NotificationsInboxString]){
        
        [TSAnalytics logEvent:ANALYTICS_INBOXNOTIFICATION_CLICKEDVIEWRECEIVER parameter:[NSMutableDictionary new]];
    }
    
    AssignedListViewController *listVC = [[AssignedListViewController alloc]initWithNibName:@"AssignedListViewController" bundle:nil];
    
    [listVC setAssignedListArray:assigneeArr];
    
    [listVC setAssignedText:NotificationNotified];
    
    [listVC showInViewController:self];
    
}

- (IBAction)attachmentBtnClk:(id)sender {
        
    NSString *attachmentURLString;
    
    if([_descriptionType isEqualToString: NotificationsString]){
        
        attachmentURL = [NSURL URLWithString:notificationsObj.notificationAttachmentUrl];
        
        attachmentURLString = notificationsObj.notificationAttachmentUrl;
    }
    else{
        
        attachmentURL = [NSURL URLWithString:notificationInboxObj.notificationAttachment];
        
        attachmentURLString = notificationInboxObj.notificationAttachment;
    }
    

    if (attachmentURLString.length>0) {
        
        NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSString *attachmentFile = [documentsPath stringByAppendingPathComponent:[attachmentURL lastPathComponent]];
        
        NSLog(@"%@",attachmentFile);
        
        NSLog(@"%@",[attachmentURL lastPathComponent]);
        
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:attachmentFile];
        
        if (fileExists) {
            
            if ([_descriptionType isEqualToString:NotificationsString]) {
                
                [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_ATTACHMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_OPENEDFROMLOCAL,ANALYTICS_PARAM_ATTACHMENTSTATUS, nil]];
                
            }else if ([_descriptionType isEqualToString:NotificationsInboxString]){
                
                [TSAnalytics logEvent:ANALYTICS_INBOXNOTIFICATION_CLICKEDATTACHMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_OPENEDFROMLOCAL,ANALYTICS_PARAM_ATTACHMENTSTATUS, nil]];
                 
            }
                        
            [self navigateToAttachmentViewControllerWithFileName:attachmentURL];
        }
        else{
            
            if ([_descriptionType isEqualToString:NotificationsString]) {
                
                [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_ATTACHMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_DOWNLOADING,ANALYTICS_PARAM_ATTACHMENTSTATUS, nil]];
                
            }else if ([_descriptionType isEqualToString:NotificationsInboxString]){
                
                [TSAnalytics logEvent:ANALYTICS_INBOXNOTIFICATION_CLICKEDATTACHMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_DOWNLOADING,ANALYTICS_PARAM_ATTACHMENTSTATUS, nil]];
                
            }
            
            [[TSHandlerClass sharedHandler]downloadAttachmentWithURL:attachmentURLString currentView:self.view withHandler:^(BOOL success, NSURL *filePath) {
                
                if (success) {
                    
                    [self navigateToAttachmentViewControllerWithFileName:filePath];
                }
                else{
                    
                    [TSSingleton showAlertWithMessage:@"Attachment might be have broken url"];
                }
            }];
        }
    }
    else{
        
        [TSSingleton showAlertWithMessage:TEXT_INVALID_ATTACHMENT];
    }
}

-(void) navigateToAttachmentViewControllerWithFileName:(NSURL *)fileURL{
    
    TSAttachmentViewController *attachmentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAttachment"];
    
    [attachmentVC setFileName:[fileURL lastPathComponent]];
    
    UINavigationController *navCtrl = [[UINavigationController alloc]initWithRootViewController:attachmentVC];
    
    navCtrl.navigationBar.translucent = NO;
    
    navCtrl.navigationBar.barTintColor = APPTHEME_COLOR;
    
    navCtrl.navigationBar.tintColor = [UIColor whiteColor];
    
    [navCtrl.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:APP_FONT size:18]}];
    
    [self.navigationController presentViewController:navCtrl animated:YES completion:nil];
}

#pragma mark - API Call

// Fetch Particular notification from Push notification
-(void)fetchPushNotificationAPICall{
    
    NSString *urlString = @"MastersMServices/SchoolNotificationsService.svc/FetchSchoolNotificationsByTransID";
    
    NSDictionary *params = @{@"guidNotificationTransID":SINGLETON_INSTANCE.pushNotificationCommTransID};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchSchoolNotificationsByTransIDResult"][@"ResponseCode"] intValue] == 1) {
                
                if ([_descriptionType isEqualToString:NotificationsString]) {
                    
                    [[Notifications instance]savePushNotificationWithDict:responseDictionary];
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId MATCHES %@ AND notificationTransId MATCHES %@",SINGLETON_INSTANCE.staffTransId,SINGLETON_INSTANCE.pushNotificationCommTransID];
                    
                    NSArray *notificationsArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Notifications" withPredicate:predicate];
                    
                    if (notificationsArray.count>0) {
                        
                        Notifications *notification = [notificationsArray objectAtIndex:0];
                        
                        [self loadViewWithNotification:notification];
                    }
                }
                else if([_descriptionType isEqualToString:NotificationsInboxString]){
                    
                    [[NotificationInbox instance]savePushNotificationWithDict:responseDictionary];
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId MATCHES %@ AND notificationTransId MATCHES %@",SINGLETON_INSTANCE.staffTransId,SINGLETON_INSTANCE.pushNotificationCommTransID];
                    
                    NSArray *notificationInboxArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"NotificationInbox" withPredicate:predicate];
                    
                    if (notificationInboxArray.count>0) {
                        
                        NotificationInbox *notificationInbox = [notificationInboxArray objectAtIndex:0];
                        
                        [self loadViewWithNotificationInbox:notificationInbox];
                    }

                }
            }
            else{
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"FetchSchoolNotificationsByTransIDResult"][@"ResponseMessage"]];
            }
        }
        else{
            
            [TSSingleton showAlertWithMessage:@"Time out error. Please try again."];
        }
    }];
}

-(void) descViewTapped{
    
    if ([_descriptionType isEqualToString:NotificationsString]) {
        
        [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_DESCRIPTION parameter:[NSMutableDictionary new]];
        
    }else if ([_descriptionType isEqualToString:NotificationsInboxString]){
        
        [TSAnalytics logEvent:ANALYTICS_INBOXNOTIFICATION_CLICKEDDESCRIPTION parameter:[NSMutableDictionary new]];
        
    }
    
    TSDescriptionViewController *descVC = [[TSDescriptionViewController alloc] initWithNibName:@"TSDescriptionViewController" bundle:nil];
    
    [descVC showInViewController:self descriptionString:_descriptionTextView.text];
    
}

@end