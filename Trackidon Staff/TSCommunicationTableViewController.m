//
//  TSCommunicationTableViewController.m
//  Trackidon Staff
//
//  Created by ORGware on 17/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSCommunicationTableViewController.h"

@interface TSCommunicationTableViewController ()

@end

@implementation TSCommunicationTableViewController
{
    NSMutableArray *menuArray;
    
    NSArray *menuCountArr;

}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
     menuArray = SINGLETON_INSTANCE.communicationMenuArr;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMenuCount) name:@"reloadPushCount" object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_COMMINUCATION parameter:[NSMutableDictionary new]];
    
    [self.tableView setTableFooterView:[UIView new]];
    
    self.navigationItem.title = @"Communication";
    
    [self getMenuCount];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)getMenuCount{
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"staffTransId =  %@",SINGLETON_INSTANCE.staff.staffTransId];
    
    menuCountArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"PushCount" withPredicate:pred];
    
    [self.tableView reloadData];
}

#pragma mark TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return menuArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    UIImageView *menuIconImgVw = (UIImageView *)[cell viewWithTag:100];
    
    UILabel *menuTitleLbl = (UILabel *)[cell viewWithTag:101];
    
    menuTitleLbl.text = [menuArray objectAtIndex:indexPath.row][@"MenuName"];
    
    UILabel *countLbl = (UILabel *)[cell viewWithTag:102];
    
    countLbl.layer.cornerRadius = countLbl.frame.size.height/2;
    
    countLbl.layer.masksToBounds = YES;
    
    if ([SINGLETON_INSTANCE.staff.staffRole integerValue] == 7) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"commTypeID = %@ AND isRead = 0",[NSString stringWithFormat:@"%@",[menuArray objectAtIndex:indexPath.row][@"MenuID"]]];
        
        NSArray *countArray =[menuCountArr filteredArrayUsingPredicate:predicate];
        
        if(countArray.count > 0){
            
            PushCount *countObj = [countArray objectAtIndex:0];
            
            countLbl.text = [NSString stringWithFormat:@"%@",countObj.unreadCount];
            
            if([countLbl.text integerValue] == 0)
                [countLbl setHidden:YES];
            else
                [countLbl setHidden:NO];
            
        } else {
            [countLbl setHidden:YES];
        }

    } else {
        
        [countLbl setHidden:YES];
        
    }
    
    
    NSInteger menu = [[menuArray objectAtIndex:indexPath.row][@"MenuID"] integerValue];
    
    switch (menu) {
            
        case MENU_ATTENDANCE:
            menuIconImgVw.image = [UIImage imageNamed:@"attendance"];
            break;
            
        case MENU_ASSIGNMENTS:
            menuIconImgVw.image = [UIImage imageNamed:@"assignments"];
            break;
            
        case MENU_EVENTS:
            menuIconImgVw.image = [UIImage imageNamed:@"events"];
            break;
            
        case MENU_EXAMS:
            menuIconImgVw.image = [UIImage imageNamed:@"exams"];
            break;
            
        case MENU_HOLIDAY:
            menuIconImgVw.image = [UIImage imageNamed:@"holidays"];
            break;
            
        case MENU_NOTIFICATIONS:
            menuIconImgVw.image = [UIImage imageNamed:@"notifications"];
            break;
            
        case MENU_TIME_TABLE:
            menuIconImgVw.image = [UIImage imageNamed:@"timetable"];
            break;
            
        case MENU_PORTIONS:
            
            menuIconImgVw.image = [UIImage imageNamed:@"portions"];
            break;
            
        case MENU_HEALTHDETAILS:
            
            menuIconImgVw.image = [UIImage imageNamed:@"results"];
            break;
            
        case MENU_TEMPERATURE:
            
            menuIconImgVw.image = [UIImage imageNamed:@"library"];
            break;
            
        case MENU_LIBRARY:
            
            menuIconImgVw.image = [UIImage imageNamed:@"library"];
            
            
            break;
        case MENU_RESULTS:
            
            menuIconImgVw.image = [UIImage imageNamed:@"results"];
            
            break;
        case MENU_FEES:
            
            menuIconImgVw.image = [UIImage imageNamed:@"fee"];
            
            break;
            
        case MENU_HOSTEL:
            
            menuIconImgVw.image = [UIImage imageNamed:@"hostel"];
            
            break;
            
        default:
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    SINGLETON_INSTANCE.currentMenuId = [menuArray objectAtIndex:indexPath.row][@"MenuID"];
    
    NSInteger menu = [[menuArray objectAtIndex:indexPath.row][@"MenuID"] integerValue];
    
    [TSAnalytics logEvent:ANALYTICS_SUBMENU_SUBMENU_CLICKED parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:[menuArray objectAtIndex:indexPath.row][@"MenuName"], ANALYTICS_PARAM_SUBMENUNAME,[NSString stringWithFormat:@"%ld",(long)menu],ANALYTICS_PARAM_SUBMENUID, nil]];
    
    [[PushCount instance] updatePushCountsFormenuId:[NSString stringWithFormat:@"%@",[menuArray objectAtIndex:indexPath.row][@"MenuID"]] isForApproval:NO];
    
    SINGLETON_INSTANCE.barTitle = [menuArray objectAtIndex:indexPath.row][@"MenuName"];
    
    switch (menu) {
            
        case MENU_ATTENDANCE:
        {
            TSAttendanceTableViewController *attendanceVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAttendanceHome"];
            
            [self.navigationController pushViewController:attendanceVC animated:YES];
            
            break;
        }
            
        case MENU_ASSIGNMENTS:
        {
            TSAssignmentsViewController *assignmentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAssignments"];
            
            [self.navigationController pushViewController:assignmentVC animated:YES];
            
            break;
        }
            
        case MENU_EVENTS:
        {
            TSEventsHomeViewController *eventsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBEventsHome"];
            
            [self.navigationController pushViewController:eventsVC animated:YES];
            
            break;
        }
            
        case MENU_EXAMS:
        {
            //ExamsViewController *examVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBExam"];
            
            //[self.navigationController pushViewController:examVC animated:YES];
            
            break;
        }
            
        case MENU_HOLIDAY:
        {
            
            TSHolidayHomeViewController *holidayVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBHolidaysHome"];
            
            [self.navigationController pushViewController:holidayVC animated:YES];
            
            break;
        }
            
        case MENU_NOTIFICATIONS:
        {
            TSNotificationHomeViewController *notificationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBNotificationHome"];
            
            [self.navigationController pushViewController:notificationVC animated:YES];
            
            break;
        }
            
        case MENU_TIME_TABLE:
        {
            TSTimeTableHomeViewController *timeTableVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBTimeTableHome"];
            
            [self.navigationController pushViewController:timeTableVC animated:YES];
            
            break;
        }
            
        case MENU_PORTIONS:
        {
            TSPortionsHomeViewController *portionHomeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBPortions"];
            
            [self.navigationController pushViewController:portionHomeVC animated:YES];
            
            break;
        }
            
        case MENU_HEALTHDETAILS:
        {
            TSHealthDetailsHomeTableViewController *healthHomeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBHealthDetailsHome"];
            
            [self.navigationController pushViewController:healthHomeVC animated:YES];
            
            break;
        }
            
        default:
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0f;
}

@end