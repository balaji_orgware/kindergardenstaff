//
//  StaffDetails+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 19/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "StaffDetails+CoreDataProperties.h"

@implementation StaffDetails (CoreDataProperties)

@dynamic acadamicTransId;
@dynamic acadamicYearTransId;
@dynamic addressOne;
@dynamic addressThree;
@dynamic addressTwo;
@dynamic emailId;
@dynamic inChargeClassName;
@dynamic inChargeClassTransId;
@dynamic inChargeSectionName;
@dynamic inChargeSectionTransId;
@dynamic mobileNo;
@dynamic name;
@dynamic pincode;
@dynamic profileImageData;
@dynamic profileImageUrl;
@dynamic schoolTransId;
@dynamic staffClassName;
@dynamic staffRole;
@dynamic staffTransId;
@dynamic assignments;
@dynamic boardList;
@dynamic eventInbox;
@dynamic events;
@dynamic feedback;
@dynamic holidays;
@dynamic notificationInbox;
@dynamic notifications;
@dynamic portions;
@dynamic receiverDetails;
@dynamic receiverFilterList;
@dynamic receiverReceipientList;
@dynamic school;
@dynamic timetable;
@dynamic pushCount;
@dynamic portionInbox;

@end
