//
//  Feedback+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/11/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Feedback+CoreDataProperties.h"

@implementation Feedback (CoreDataProperties)

@dynamic acadamicYearTransID;
@dynamic classSection;
@dynamic dateCreated;
@dynamic feedbackDescription;
@dynamic feedBackType;
@dynamic feedBackTypeID;
@dynamic parentMobileNo;
@dynamic parentName;
@dynamic studentName;
@dynamic studentTransID;
@dynamic title;
@dynamic transID;
@dynamic staffTransID;
@dynamic staff;

@end
