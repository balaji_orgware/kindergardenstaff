//
//  NSDictionary+TPNullCheckCategory.m
//  Trackidon
//
//  Created by Balaji on 02/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "NSDictionary+TPNullCheckCategory.h"

@implementation NSDictionary (TPNullCheckCategory)

- (NSDictionary *)dictionaryByReplacingNullsWithBlanks {
    
    const NSMutableDictionary *replaced = [self mutableCopy];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    
    for (NSString *key in self) {
        id object = [self objectForKey:key];
        if (object == nul)
        {
            [replaced setObject:blank forKey:key];
        }
        
        else if ([object isKindOfClass:[NSDictionary class]])
        {
            [replaced setObject:[object dictionaryByReplacingNullsWithBlanks] forKey:key];
        }
        
        else if ([object isKindOfClass:[NSArray class]])
        {
            //NSLog(@"Array");
        }
    }
    return [NSDictionary dictionaryWithDictionary:[replaced copy]];
}

@end
