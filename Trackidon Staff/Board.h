//
//  Board.h
//  Trackidon Staff
//
//  Created by Elango on 18/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class School;

NS_ASSUME_NONNULL_BEGIN

@interface Board : NSManagedObject

+ (instancetype)instance;

- (void)saveBoardAndClassDetailsWithResponseDict:(NSDictionary *)dict schoolTransId:(NSString *)schoolTransId;

@end

NS_ASSUME_NONNULL_END

#import "Board+CoreDataProperties.h"
