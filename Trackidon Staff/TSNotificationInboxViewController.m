//
//  TSNotificationInboxViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/2/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSNotificationInboxViewController.h"

@interface TSNotificationInboxViewController ()
{
    NSArray *notificationArr,*attachmentImgArray;
}
@end

@implementation TSNotificationInboxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [TSAnalytics logEvent:ANALYTICS_INBOXNOTIFICATION_OPENED parameter:[NSMutableDictionary new]];
    
    attachmentImgArray = @[@"one",@"two",@"three",@"four",@"five"];
    
    [_notificationTableView setTableFooterView:[UIView new]];

    
    [self fetchNotifications];
    
    [self fetchNotificationInbox];

}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_INBOXNOTIFICATION parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Notification Log";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - TableView Delegate & DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return notificationArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    UILabel *titleLbl = (UILabel *)[cell viewWithTag:101];
    
    UILabel *createdByLbl = (UILabel *)[cell viewWithTag:102];
    
    UILabel *dateLbl = (UILabel *)[cell viewWithTag:103];
    
    UIImageView *attachmentImgVw = (UIImageView *)[cell viewWithTag:104];
    
    NotificationInbox *notificationInboxObj = [notificationArr objectAtIndex:indexPath.row];
    
    titleLbl.text = notificationInboxObj.notificationTitle;
    
    createdByLbl.text = [NSString stringWithFormat:@"Created By : %@",notificationInboxObj.createdBy];
    
    dateLbl.text = [SINGLETON_INSTANCE stringFromDateWithFormate:DATE_FORMATE_MNTH_DAY_YR withDate:notificationInboxObj.notificationDate];
        
    UIImageView *priorityImgVw = (UIImageView *)[cell viewWithTag:105];
    
    switch ([notificationInboxObj.priority intValue]) {
            
        case 1:
            
            priorityImgVw.image = [UIImage imageNamed:@"priority_red"];
            
            break;
            
        case 2:
            
            priorityImgVw.image = [UIImage imageNamed:@"priority_yellow"];
            
            break;
            
        case 3:
            
            priorityImgVw.image = [UIImage imageNamed:@"priority_green"];
            
            break;
            
        default:
            break;
    }

    if ([notificationInboxObj.isHasAttachment intValue] == 1) {
        
        NSString *attachmentImgName;
        
        switch (indexPath.row%5) {
                
            case 0:
                attachmentImgName = @"one";
                break;
                
            case 1:
                attachmentImgName = @"two";
                break;
                
            case 2:
                attachmentImgName = @"three";
                break;
                
            case 3:
                attachmentImgName = @"four";
                break;
                
            case 4:
                attachmentImgName = @"five";
                break;
                
            default:
                attachmentImgName = @"one";
                break;
        }
        attachmentImgVw.image = [UIImage imageNamed:attachmentImgName];
        
        [attachmentImgVw setHidden:NO];
    }
    else{
        
        [attachmentImgVw setHidden:YES];
    }

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [TSAnalytics logEvent:ANALYTICS_INBOXNOTIFICATION_CLICKEDINLIST parameter:[NSMutableDictionary new]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
     TSNotificationDescriptionViewController *notificationDesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBNotificationDescription"];
    
    id notificationInbox = (NotificationInbox *)[notificationArr objectAtIndex:indexPath.row];
    
    notificationDesVC.descriptionType = NotificationsInboxString;
    
    notificationDesVC.commonObj = notificationInbox;
    
    [self.navigationController pushViewController:notificationDesVC animated:YES];
    
}

#pragma mark - Service call

-(void)fetchNotificationInbox{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    NSString *url = @"MastersMServices/SchoolNotificationsService.svc/FetchSchoolStaffNotificationsByStaffID";
    
    [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidStaffTransID"];
    
    BOOL animated;
    
    if (notificationArr.count == 0)
        animated = YES;
    else
        animated = NO;
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:url baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:animated onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
      
        if (success) {
            
            if ([responseDictionary[@"FetchSchoolStaffNotificationsByStaffIDResult"][@"ResponseCode"]intValue]==1) {
                
                NSLog(@"%@",responseDictionary);
                
                if (responseDictionary[@"FetchSchoolStaffNotificationsByStaffIDResult"][@"SchoolNotificationClass"] != [NSNull null]) {
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                        [[NotificationInbox instance]saveNotificationsInboxWithResponseDict:responseDictionary];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self fetchNotifications];
                            
                        });
                    });
                }
            }
        }
    }];
}

#pragma mark - Methods

- (void)fetchNotifications{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransID CONTAINS[cd] %@",SINGLETON_INSTANCE.staffTransId];
    
    notificationArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"NotificationInbox" withPredicate:predicate];
    
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:NO];
    
    notificationArr = [[notificationArr sortedArrayUsingDescriptors:@[dateDescriptor]] mutableCopy];
    
    if(notificationArr.count >0){
        
        [self.notificationTableView setHidden:NO];
    }
    else{
        
        [self.notificationTableView setHidden:YES];
    }
    
    [self.notificationTableView reloadData];
    
}

@end
