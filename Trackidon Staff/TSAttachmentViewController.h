//
//  TSAttachmentViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/28/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSAttachmentViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (nonatomic,strong) NSString *fileName;

@end
