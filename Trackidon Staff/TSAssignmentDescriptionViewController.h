//
//  TSAssignmentDescriptionViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/28/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Assignments,AssignedDetails;

@interface TSAssignmentDescriptionViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *assingnmentTitleLbl;

@property (weak, nonatomic) IBOutlet UILabel *subjectLbl;

@property (weak, nonatomic) IBOutlet UILabel *typeLbl;

@property (weak, nonatomic) IBOutlet UIView *topView;

@property (weak, nonatomic) IBOutlet UIButton *attachmentButton;

@property (weak, nonatomic) IBOutlet UILabel *assignedTeacherLabel;

@property (weak, nonatomic) IBOutlet UIView *dateView;

@property (weak, nonatomic) IBOutlet UILabel *assignedDateLabel;

@property (weak, nonatomic) IBOutlet UILabel *dueDateLabel;

@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

@property (weak, nonatomic) IBOutlet UIView *assignmentCountView;

@property (weak, nonatomic) IBOutlet UILabel *assignmentRemainingCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *daysTextLabel;


@property (weak, nonatomic) IBOutlet UILabel *assignedLbl;

@property (weak, nonatomic) IBOutlet UILabel *dueLbl;

@property (nonatomic,strong) Assignments *assignmentObj;

@property (weak, nonatomic) IBOutlet UIView *assignedView;

@property (weak, nonatomic) IBOutlet UITextField *assignedTxtFld;

@property (weak, nonatomic) AssignedDetails *assignedObj;

@end
