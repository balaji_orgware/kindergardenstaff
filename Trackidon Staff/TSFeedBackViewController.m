//
//  TSFeedBackViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/24/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSFeedBackViewController.h"

@interface TSFeedBackViewController ()
{
    StaffDetails *staffObj;
    
    NSMutableArray *feedbacksArray;
}
@end

@implementation TSFeedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [TSAnalytics logEvent:ANALYTICS_FEEDBACK_OPENED parameter:[NSMutableDictionary new]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId MATCHES %@",SINGLETON_INSTANCE.staffTransId];

    staffObj = [[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"StaffDetails" withPredicate:predicate]objectAtIndex:0];


}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_FEEDBACKHOME parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    NSLog(@"%@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
    
    [self retrieveFeedbacks];
    
    [self fetchFeedbackServiceCall];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Feedback";
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return feedbacksArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sCellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier];
    
    UILabel *titleLbl = (UILabel *)[cell viewWithTag:100];
    
    UILabel *parentName = (UILabel *)[cell viewWithTag:101];
    
    UILabel *dateLbl = (UILabel *)[cell viewWithTag:102];
    
    Feedback *feedbackObj = [feedbacksArray objectAtIndex:indexPath.row];
    
    titleLbl.text = feedbackObj.title;
    
    parentName.text = [NSString stringWithFormat:@"From : %@",feedbackObj.parentName];
    
    dateLbl.text = [[TSSingleton singletonSharedInstance] changeDateFormatFromService:feedbackObj.dateCreated withFormat:@"MMMM dd, yyyy"];
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [TSAnalytics logEvent:ANALYTICS_FEEDBACK_CLICKEDFEEDINLIST parameter:[NSMutableDictionary new]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TSFeedBackCreateViewController *feedbackDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBFeedbackDetails"];
    
    [feedbackDetailVC setFeedbackObj:[feedbacksArray objectAtIndex:indexPath.row]];
    
    [self.navigationController pushViewController:feedbackDetailVC animated:YES];
}

-(void) retrieveFeedbacks{
    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ParentName MATCHES %@",staffObj.name];
//    
//    feedbacksArray = [NSMutableArray arrayWithArray:[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Feedback" withPredicate:predicate]];
    
    feedbacksArray = [NSMutableArray arrayWithArray:[SINGLETON_INSTANCE retrieveAllObjectsInEntity:@"Feedback"]];

    if (feedbacksArray.count>0) {
        
        [self.feedBackTableView setHidden:NO];
    }
    else{
        
        [self.feedBackTableView setHidden:YES];
    }
    
    [self.feedBackTableView reloadData];
}

#pragma mark - Service Call

-(void) fetchFeedbackServiceCall{
    
    NSString *urlString = @"MastersMServices/FeedBackService.svc/FetchAllFeedBacks";
    
    NSMutableDictionary *param = [NSMutableDictionary new];
    
    [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidStaffTransID"];
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            NSLog(@"%@",responseDictionary);
            
            if ([responseDictionary[@"FetchAllFeedBacksResult"][@"ResponseCode"]intValue] ==1) {
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                    [[Feedback instance] storeFeedbacks:staffObj withDict:responseDictionary];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self retrieveFeedbacks];
                        
                    });
                });
            }
        }
    }];
}


@end
