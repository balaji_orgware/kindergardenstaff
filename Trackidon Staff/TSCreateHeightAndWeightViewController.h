//
//  TSCreateHeightAndWeightViewController.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 25/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSCreateHeightAndWeightViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *heightView;
@property (weak, nonatomic) IBOutlet UITextField *heightTxtFld;

@property (weak, nonatomic) IBOutlet UIView *weightView;
@property (weak, nonatomic) IBOutlet UITextField *weightTxtFld;


@property (weak, nonatomic) IBOutlet UITextView *descriptionTxtVw;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *doneButtonView;

@property (weak, nonatomic) IBOutlet UIButton *doneButton;

- (IBAction)doneButtonTapped:(id)sender;

@property (nonatomic,strong) NSDictionary *generalInfoDict;

@end
