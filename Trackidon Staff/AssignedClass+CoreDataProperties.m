//
//  AssignedClass+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by ORGware on 24/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AssignedClass+CoreDataProperties.h"

@implementation AssignedClass (CoreDataProperties)

@dynamic assignmentTransID;
@dynamic classname;
@dynamic sectionName;
@dynamic isStaff;
@dynamic addlnRemarks;
@dynamic transID;

@dynamic assignment;

@end
