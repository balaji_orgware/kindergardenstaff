//
//  Timetable.m
//  Trackidon Staff
//
//  Created by Balaji on 23/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "Timetable.h"
#import "StaffDetails.h"

@implementation Timetable

// Method to Get Static Instance
+(instancetype)instance{
    
    static Timetable *timetableObj = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        timetableObj = [[Timetable alloc] init];
    });
    
    return timetableObj;
}

// Method to Store Timetable to core data
-(void)storeTimeTableForStaff:(StaffDetails *)staffObj withDict:(NSDictionary *)dictionary{
    
    @try {
        
        NSError *error = nil;
        
        //NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSManagedObjectContext *mainContext = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        
        [context setParentContext:mainContext];
                
        NSManagedObjectID *objectId = [SINGLETON_INSTANCE.staff objectID];
        
        StaffDetails *tmpStaff = [context objectWithID:objectId];

        
        for (NSDictionary *dataDict in dictionary[@"FetchTimeTableByStaffTransIDResult"][@"TimetableData"]) {
            
            NSDictionary *dict = [dataDict dictionaryByReplacingNullsWithBlanks];
            
            Timetable *timetableObj = [self isTimetableEntryAlreadyExistsWithTransID:dict[@"TransID"] andStaffObj:staffObj andDayTransID:dict[@"DayTransID"] context:context];
            
            if (timetableObj == nil) {
                
                timetableObj = [NSEntityDescription insertNewObjectForEntityForName:@"Timetable" inManagedObjectContext:context];
            }
            
            timetableObj.academicTransID = dict[@"AcadamicTransID"];
            timetableObj.boardTransID = dict[@"BoardTransID"];
            timetableObj.day = dict[@"T_Day"];
            timetableObj.dayTransID = dict[@"DayTransID"];
            timetableObj.dayDescription = dict[@"DayDescription"];
            
            
            timetableObj.periodOne = dict[@"IPeriodValue"];
            timetableObj.periodOneTransID = dict[@"IPeriod"];
            timetableObj.periodOneClass = dict[@"IPeriodClassValue"];
            timetableObj.periodOneClassTransID = dict[@"IPeriodClass"];
            timetableObj.periodOneSection = dict[@"IPeriodSectionValue"];
            timetableObj.periodOneSecTransID = dict[@"IPeriodSection"];
            
            timetableObj.periodTwo = dict[@"IIPeriodValue"];
            timetableObj.periodTwoTransID = dict[@"IIPeriod"];
            timetableObj.periodTwoClass = dict[@"IIPeriodClassValue"];
            timetableObj.periodTwoClassTransID = dict[@"IIPeriodClass"];
            timetableObj.periodTwoSection = dict[@"IIPeriodSectionValue"];
            timetableObj.periodTwoSecTransID = dict[@"IIPeriodSection"];
            
            
            timetableObj.periodThree = dict[@"IIIPeriodValue"];
            timetableObj.periodThreeTransID = dict[@"IIIPeriod"];
            timetableObj.periodThreeClass = dict[@"IIIPeriodClassValue"];
            timetableObj.periodThreeClassTransID = dict[@"IIIPeriodClass"];
            timetableObj.periodThreeSection = dict[@"IIIPeriodSectionValue"];
            timetableObj.periodThreeSecTransID = dict[@"IIIPeriodSection"];
            
            timetableObj.periodFour = dict[@"IVPeriodValue"];
            timetableObj.periodFourTransID = dict[@"IVPeriod"];
            timetableObj.periodFourClass = dict[@"IVPeriodClassValue"];
            timetableObj.periodFourClassTransID = dict[@"IVPeriodClass"];
            timetableObj.periodFourSection = dict[@"IVPeriodSectionValue"];
            timetableObj.periodFourSecTransID = dict[@"IVPeriodSection"];
            
            timetableObj.periodFive = dict[@"VPeriodValue"];
            timetableObj.periodFiveTransID = dict[@"VPeriod"];
            timetableObj.periodFiveClass = dict[@"VPeriodClassValue"];
            timetableObj.periodFiveClassTransID = dict[@"VPeriodClass"];
            timetableObj.periodFiveSection = dict[@"VPeriodSectionValue"];
            timetableObj.periodFiveSecTransID = dict[@"VPeriodSection"];

            
            timetableObj.periodSix = dict[@"VIPeriodValue"];
            timetableObj.periodSixTransID = dict[@"VIPeriod"];
            timetableObj.periodSixClass = dict[@"VIPeriodClassValue"];
            timetableObj.periodSixClassTransID = dict[@"VIPeriodClass"];
            timetableObj.periodSixSection = dict[@"VIPeriodSectionValue"];
            timetableObj.periodSixSecTransID = dict[@"VIPeriodSection"];

            
            timetableObj.periodSeven = dict[@"VIIPeriodValue"];
            timetableObj.periodSevenTransID = dict[@"VIIPeriod"];
            timetableObj.periodSevenClass = dict[@"VIIPeriodClassValue"];
            timetableObj.periodSevenClassTransID = dict[@"VIIPeriodClass"];
            timetableObj.periodSevenSection = dict[@"VIIPeriodSectionValue"];
            timetableObj.periodSevenSecTransID = dict[@"VIIPeriodSection"];

            
            timetableObj.periodEight = dict[@"VIIIPeriodValue"];
            timetableObj.periodEightTransID = dict[@"VIIIPeriod"];
            timetableObj.periodEightClass = dict[@"VIIIPeriodClassValue"];
            timetableObj.periodEightClassTransID = dict[@"VIIIPeriodClass"];
            timetableObj.periodEightSection = dict[@"VIIIPeriodSectionValue"];
            timetableObj.periodEightSecTransID = dict[@"VIIIPeriodSection"];

            
            timetableObj.specialClass = dict[@"SpecialClassValue"];
            timetableObj.specialClassTransID = dict[@"SpecialClass"];
            timetableObj.specialClassClass = dict[@"SpecialClassClassValue"];
            timetableObj.specialClassClassTransID = dict[@"SpecialClassClass"];
            timetableObj.specialClassSec = dict[@"SpecialClassSectionValue"];
            timetableObj.specialClassSecTransID = dict[@"SpecialClassSection"];
            
            timetableObj.transID = dict[@"TransID"];
            timetableObj.staffTransID = staffObj.staffTransId;
            
            timetableObj.staff = tmpStaff;
            
        }
        if (![context save:&error]) {
            
            NSLog(@"whoops.. Cannot save timetable");
        }
        
        [SINGLETON_INSTANCE storeContextInMainThread];

    } @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    } @finally {
        
    }
}

// Method to check whether Timetable exists for the child
-(Timetable *)isTimetableEntryAlreadyExistsWithTransID:(NSString *)transID andStaffObj:(StaffDetails *)staffObj andDayTransID:(NSString *)dayTransID context:(NSManagedObjectContext *)context{
    
    @try {
        NSError *error = nil;
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Timetable" inManagedObjectContext:context];
        
        [fetchRequest setReturnsObjectsAsFaults:NO];
        
        [fetchRequest setEntity:entity];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@ AND dayTransID MATCHES %@",staffObj.staffTransId,dayTransID];
        
        [fetchRequest setPredicate:predicate];
        
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
        if (fetchedObjects.count>0)
            return [fetchedObjects objectAtIndex:0];
        
        else
            return nil;
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception.debugDescription);
    }
    @finally {
        
    }
}

@end