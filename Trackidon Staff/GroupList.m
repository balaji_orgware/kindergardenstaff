//
//  GroupList.m
//  Trackidon Staff
//
//  Created by Elango on 08/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "GroupList.h"
#import "StaffDetails.h"

@implementation GroupList

+ (instancetype)instance{
    
    static GroupList *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (&onceToken, ^{
        
        kSharedInstance = [[GroupList alloc]init];
        
    });
    
    return kSharedInstance;
    
}

- (void)saveGroupListWithResponseDict:(NSDictionary *)dict schoolTransId:(NSString *)schoolTransId{
    
    NSLog(@"%@",dict);
    
    NSManagedObjectContext *context = [[TSAppDelegate instance] managedObjectContext];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"schoolTransId contains[cd] %@",schoolTransId];
    
    [SINGLETON_INSTANCE deleteSelectedObjectsInEntityWithEntityName:@"GroupList" withPredicate:predicate withContext:[APPDELEGATE_INSTANCE managedObjectContext]];
    
    NSArray *schoolArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"School" withPredicate:predicate];
    
    
    School *school;
    
    if (schoolArr.count >0)
        school = [schoolArr objectAtIndex:0];
    
    
    @try {
        
        for (NSDictionary *tmpGroupDict in dict[@"FetchAllGroupMResult"][@"GroupMListClass"])
        {
            NSDictionary *groupDict = [tmpGroupDict dictionaryByReplacingNullsWithBlanks];
            
            //GroupList *group;
            
            //if (group == nil) {
                
                GroupList *group = [NSEntityDescription insertNewObjectForEntityForName:@"GroupList" inManagedObjectContext:context];
            //}
            
            //group.schoolName = boardListDict[@"SchoolName"]!= [NSNull null] ? boardListDict[@"SchoolName"] : @"";
            
            group.schoolTransId = groupDict[@"SchoolTransID"]!= [NSNull null] ? groupDict[@"SchoolTransID"] : @"";
            
            //group.boardName = boardListDict[@"BoardName"]!= [NSNull null] ? boardListDict[@"BoardName"] : @"";
            
            group.boardTransId = groupDict[@"BoardTransID"]!= [NSNull null] ? groupDict[@"BoardTransID"] : @"";
            
            group.groupName = groupDict[@"GroupName"];
            
            group.groupTransId = groupDict[@"TransID"];
            
            group.school = school;

        }
        
        NSError *error;
        
        if (![context save:&error]) {
            
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    
}

@end
