//
//  TSApprovalsTableViewController.h
//  Trackidon Staff
//
//  Created by ORGware on 18/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Assignments,Events,Portions,Notifications,AssignedDetails;

@interface TSApprovalsTableViewController : UITableViewController

@end
