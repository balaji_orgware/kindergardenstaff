//
//  Holidays+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 23/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Holidays+CoreDataProperties.h"

@implementation Holidays (CoreDataProperties)

@dynamic academicTransID;
@dynamic approvedByTransID;
@dynamic approvedByName;
@dynamic approvedByDate;
@dynamic approvedStatus;
@dynamic attachment;
@dynamic dateTimeModified;
@dynamic holidayDescription;
@dynamic classSection;
@dynamic holidayTime;
@dynamic holidayTitle;
@dynamic holidayTypeID;
@dynamic fromDate;
@dynamic toDate;
@dynamic isAttachment;
@dynamic schoolTransID;
@dynamic userCreatedName;
@dynamic userModifiedName;
@dynamic venue;
@dynamic userCreatedTransID;
@dynamic userModifiedTransID;
@dynamic userTransID;
@dynamic transID;
@dynamic staffTransID;
@dynamic staff;

@end
