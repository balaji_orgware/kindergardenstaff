//
//  TSCreateEventViewController.h
//  Trackidon Staff
//
//  Created by Elango on 24/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSCreateEventViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollVw;

@property (weak, nonatomic) IBOutlet UILabel *fromDateDayLbl;
@property (weak, nonatomic) IBOutlet UILabel *fromDateMnthYearLbl;
@property (weak, nonatomic) IBOutlet UILabel *toDateDayLbl;
@property (weak, nonatomic) IBOutlet UILabel *toDateMnthYearLbl;

@property (weak, nonatomic) IBOutlet UITextField *titleTxtFld;

@property (weak, nonatomic) IBOutlet UIView *dateVw;

@property (weak, nonatomic) IBOutlet UITextField *timeTxtFld;

@property (weak, nonatomic) IBOutlet UITextField *venueTxtFld;

@property (weak, nonatomic) IBOutlet UITextField *attachementTxtFld;
@property (weak, nonatomic) IBOutlet UIImageView *attachmentPinImgVw;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTxtVw;
@property (weak, nonatomic) IBOutlet UIView *receiverSelectionVw;

@end
