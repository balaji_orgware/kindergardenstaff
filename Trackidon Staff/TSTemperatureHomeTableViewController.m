//
//  TSTemperatureHomeTableViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 11/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSTemperatureHomeTableViewController.h"

@interface TSTemperatureHomeTableViewController ()

@end

@implementation TSTemperatureHomeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationItem.title = @"Temperature";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
    UILabel *menuLabel = (UILabel *)[cell viewWithTag:101];
    
    switch (indexPath.row) {
        case 0:
            menuLabel.text = @"Temperature Today";
            imageView.image = [UIImage imageNamed:@"user_manual"];
            break;
            
        case 1:
            menuLabel.text = @"Temperature History";
            imageView.image = [UIImage imageNamed:@"user_manual"];
            break;
            
        default:
            break;
    }
    
    [tableView setTableFooterView:[UIView new]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 0:
        {
            TSTemperatureTodayViewController *temperatureTodayVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBTemperatureToday"];
            
            [self.navigationController pushViewController:temperatureTodayVC animated:YES];
            
            break;
        }
        case 1:
        {
            TSTemperatureHistorySelectionViewController *historySelectionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBTemperatureHistorySelection"];
            
            [self.navigationController pushViewController:historySelectionVC animated:YES];
            
            break;
        }
            
        default:
            break;
    }
}
@end
