//
//  NotificationInbox.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/4/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "NotificationInbox.h"
#import "StaffDetails.h"

@implementation NotificationInbox

+(instancetype)instance{

    static NotificationInbox *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (&onceToken, ^{
        
        kSharedInstance = [[NotificationInbox alloc]init];
        
    });
    
    return kSharedInstance;
}

-(void)saveNotificationsInboxWithResponseDict:(NSDictionary *)dict{
    
    NSLog(@"%@",dict);
    
    @try {
        
        [SINGLETON_INSTANCE deleteAllObjectsInEntity:@"NotificationInbox"];
        
        NSManagedObjectContext *mainContext = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        
        [context setParentContext:mainContext];
                
        NSManagedObjectID *objectId = [SINGLETON_INSTANCE.staff objectID];
        
        StaffDetails *tmpStaff = [context objectWithID:objectId];

        
        for (NSDictionary *tempNotificationInbox in dict[@"FetchSchoolStaffNotificationsByStaffIDResult"][@"SchoolNotificationClass"]) {
            
            NSDictionary *notificationDict = [tempNotificationInbox dictionaryByReplacingNullsWithBlanks];
            
            NotificationInbox *notificationInbox;
            
            //notificationInbox = [self checkAvailabilityWithTransId:notificationDict[@"TransID"] context:context];
            
            //if (notificationInbox == nil) {
                
                notificationInbox = [NSEntityDescription insertNewObjectForEntityForName:@"NotificationInbox" inManagedObjectContext:context];
            //}
            
            notificationInbox.notificationTitle =notificationDict[@"NotificationTitle"];
            notificationInbox.notificationDescription = notificationDict[@"NotificationDescription"];
            notificationInbox.priority = notificationDict[@"Priority"];
            notificationInbox.notificationDate = [SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:notificationDict[@"NotificationDate"]];
            notificationInbox.createdBy = notificationDict[@"CreatedBy"];
            notificationInbox.transID = notificationDict[@"TransID"];
            notificationInbox.staffTransID = SINGLETON_INSTANCE.staffTransId;
            notificationInbox.isHasAttachment = [NSNumber numberWithInteger:[notificationDict[@"IsAttachment"]integerValue]];
            notificationInbox.notificationAttachment = notificationDict[@"Attachment"];
            
            notificationInbox.modifiedDate = [SINGLETON_INSTANCE dateFromString:notificationDict[@"ModifiedDate"] withFormate:DATE_FORMATE_MODIFIED_DATE];
            
            NSString *assigneeList = [[TSCommonAssignedDetails instance] getAssignedDetailsWithArray:notificationDict[@"SchoolNotificationDetails"] withGroupType:notificationDict[@"GroupType"]];
            
            notificationInbox.assignedList = assigneeList;
            
            //[[AssignedDetails instance] saveAssignedDetailsWithDict:notificationDict[@"SchoolNotificationDetails"] withGroupType:notificationDict[@"GroupType"] transId:notificationInbox.transID forObject:notificationInbox objectType:OBJECT_TYPE_NOTIFICATIONINBOX context:context];
            
            notificationInbox.staff = tmpStaff;
            
        }
        
        NSError *error;
        
        if (![context save:&error]) {
            
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        
        [SINGLETON_INSTANCE storeContextInMainThread];
        
    } @catch (NSException *exception) {
    
        NSLog(@"%@",exception);
    }
}

-(NotificationInbox *)checkAvailabilityWithTransId:(NSString *)transId context:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"NotificationInbox" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transID contains[cd] %@", transId];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
    
}

- (void)savePushNotificationWithDict:(NSDictionary *)dictionary{
    
    @try {
        
        NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSError *error = nil;
        
        NSDictionary *notificationDict = [dictionary[@"FetchSchoolNotificationsByTransIDResult"][@"SchoolNotificationClass"] dictionaryByReplacingNullsWithBlanks];
        
        NotificationInbox *notificationInbox = [self checkAvailabilityWithTransId:notificationDict[@"TransID"] context:context];
        
        if (notificationInbox == nil) {
            
            notificationInbox = [NSEntityDescription insertNewObjectForEntityForName:@"NotificationInbox" inManagedObjectContext:context];
        }
        
        notificationInbox.notificationTitle =notificationDict[@"NotificationTitle"];
        notificationInbox.notificationDescription = notificationDict[@"NotificationDescription"];
        notificationInbox.priority = notificationDict[@"Priority"];
        notificationInbox.notificationDate = [SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:notificationDict[@"NotificationDate"]];
        notificationInbox.createdBy = notificationDict[@"CreatedBy"];
        notificationInbox.transID = notificationDict[@"TransID"];
        notificationInbox.staffTransID = SINGLETON_INSTANCE.staffTransId;
        notificationInbox.isHasAttachment = [NSNumber numberWithInteger:[notificationDict[@"IsAttachment"]integerValue]];
        notificationInbox.notificationAttachment = notificationDict[@"Attachment"];
        
        notificationInbox.modifiedDate = [SINGLETON_INSTANCE dateFromString:notificationDict[@"ModifiedDate"] withFormate:DATE_FORMATE_MODIFIED_DATE];
        
        NSString *assigneeList = [[TSCommonAssignedDetails instance] getAssignedDetailsWithArray:notificationDict[@"SchoolNotificationDetails"] withGroupType:notificationDict[@"GroupType"]];
        
        notificationInbox.assignedList = assigneeList;
        
        //[[AssignedDetails instance] saveAssignedDetailsWithDict:notificationDict[@"SchoolNotificationDetails"] withGroupType:notificationDict[@"GroupType"] transId:notificationInbox.transID forObject:notificationInbox objectType:OBJECT_TYPE_NOTIFICATIONINBOX context:context];
        
        notificationInbox.staff = SINGLETON_INSTANCE.staff;
        
        if (![context save:&error]) {
            
            NSLog(@"Whoops could not save");
        }
        
    } @catch (NSException *exception) {
        
        
    } @finally {
        
    }
}

@end
