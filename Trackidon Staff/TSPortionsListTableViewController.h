//
//  TSPortionsListTableViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 22/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSPortionsListTableViewController : UITableViewController
@property (nonatomic, strong) NSString *listType;

@property (nonatomic,strong) NSArray *portionsArray;

@end
