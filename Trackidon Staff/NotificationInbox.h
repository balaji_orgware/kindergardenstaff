//
//  NotificationInbox.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/4/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StaffDetails;

NS_ASSUME_NONNULL_BEGIN

@interface NotificationInbox : NSManagedObject

+ (instancetype)instance;

- (void)saveNotificationsInboxWithResponseDict:(NSDictionary *)dict;

- (void)savePushNotificationWithDict:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END

#import "NotificationInbox+CoreDataProperties.h"
