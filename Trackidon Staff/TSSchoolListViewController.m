//
//  TPSchoolListViewController.m
//  Trackidon
//
//  Created by Elango on 12/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSSchoolListViewController.h"

@interface TSSchoolListViewController ()

@end

@implementation TSSchoolListViewController
{
    NSArray *schoolListArr;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [TSAnalytics logEvent:ANALYTICS_CHANGESCHOOL_OPENED parameter:[NSMutableDictionary new]];
        
    self.navigationItem.title = @"Select School";
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@",[USERDEFAULTS valueForKey:@"staffTransId"]];
    
    schoolListArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"School" withPredicate:predicate];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_SCHOOLSELECTION parameter:[NSMutableDictionary new]];
    
    self.navigationController.navigationBarHidden = NO;
    
    self.navigationItem.hidesBackButton = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data sources & delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return schoolListArr.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UIImageView *schoolImageView = (UIImageView *)[cell viewWithTag:100];
    
    UILabel *schoolNameLabel = (UILabel *)[cell viewWithTag:101];
    
    School *school = [schoolListArr objectAtIndex:indexPath.row];
   
    schoolImageView.image = school.schoolLogoData == nil ? [UIImage imageNamed:@"alerts"] : [UIImage imageWithData:school.schoolLogoData];
    
    schoolNameLabel.text = [school.schoolName capitalizedString];
    
    schoolImageView.layer.cornerRadius = schoolImageView.frame.size.width/2;
    
    schoolImageView.layer.masksToBounds = YES;
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [TSAnalytics logEvent:ANALYTICS_SCHOOL_SELECTION_SCHOOLSELECTED parameter:[NSMutableDictionary new]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    [USERDEFAULTS setBool:YES forKey:@"isLogin"];
    
    School *school = [schoolListArr objectAtIndex:indexPath.row];
    
    [USERDEFAULTS setObject:school.schoolTransId forKey:@"schoolTransId"];
    
    [USERDEFAULTS setObject:school.schoolName forKey:@"schoolName"];
    
    SINGLETON_INSTANCE.selectedSchoolTransId = school.schoolTransId;
    
    [SINGLETON_INSTANCE createMenu];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 75;
}


@end
