//
//  TSPortionsApprovalsDescViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 18/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSPortionsApprovalsDescViewController.h"

@interface TSPortionsApprovalsDescViewController (){
    
    NSURL *attachmentURL;
    
    NSArray *assigneeArr;
}
@end

@implementation TSPortionsApprovalsDescViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(descViewTapped)];
    
    [_descriptionTextView addGestureRecognizer:tapGesture];
    
    _attachmentButton.frame = CGRectMake(DEVICE_SIZE.width - _attachmentButton.frame.size.width - 10, _subjectHeaderView.frame.origin.y + _subjectHeaderView.frame.size.height - _attachmentButton.frame.size.height/2 + 18, _attachmentButton.frame.size.height, _attachmentButton.frame.size.height);
    
    [TSSingleton layerDrawForView:_subjectHeaderView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:_assignedView position:LAYER_TOP color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:_assignedView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    _portionDateLabel.layer.cornerRadius = _portionDateLabel.frame.size.width/2;
    _portionDateLabel.layer.masksToBounds = YES;
    
    
    [SINGLETON_INSTANCE addRightViewToTextField:_assignedTxtFld withImageName:RIGHTARROW_IMGNAME];
    
    _assignedTeacherLabel.text = [NSString stringWithFormat:@"- by %@",_portionObj.createdByName];
    
    _descriptionTextView.text = [NSString stringWithFormat:@"\t%@",_portionObj.portionDescription];
    
    _titleLabel.text = [NSString stringWithFormat:@"Title : %@",_portionObj.portionTitle] ;
    
    
    _subjectTitleLabel.text = [NSString stringWithFormat:@"Subject : %@",_portionObj.subjectTitle];
    
    _portionDateLabel.attributedText = [SINGLETON_INSTANCE getDateAttributedStringForDateString:_portionObj.portionDate inLabel:_portionDateLabel];
    
    if ([_portionObj.isAttachment intValue] == 1) {
        
        [_attachmentButton setHidden:NO];
    }
    else{
        
        [_attachmentButton setHidden:YES];
    }
    
    /*NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transId MATCHES %@",self.portionObj.transID];
    
    assigneeArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"AssignedDetails" withPredicate:predicate];
    
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"assigneeType" ascending:YES];
    
    NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
    
    assigneeArr = [assigneeArr sortedArrayUsingDescriptors:descriptors];*/
    
     assigneeArr = [_portionObj.assignedList componentsSeparatedByString:ASSIGNEE_SAPRATOR];
    
    _assignedView.hidden = NO;
    
    UITapGestureRecognizer *assignedViewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(assignedViewTapped)];
    
    if (assigneeArr.count ==1) {
        
        //_assignedObj = [assigneeArr objectAtIndex:0];
        
        _assignedTxtFld.text = [NSString stringWithFormat:@"Assigned To : %@",[assigneeArr objectAtIndex:0]];
        
    }else if (assigneeArr.count >1){
        
        [self.assignedView addGestureRecognizer:assignedViewTap];
        
        [SINGLETON_INSTANCE addRightViewToTextField:_assignedTxtFld withImageName:@"expand_arrow"];
    }
    else if (assigneeArr.count ==0){
        
        [self.assignedView setHidden:YES];
    }
    
    UIBarButtonItem *approveBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"approve"] style:UIBarButtonItemStyleDone target:self action:@selector(approveButtonAction)];
    
    UIBarButtonItem *declineBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"decline"] style:UIBarButtonItemStyleDone target:self action:@selector(declineButtonAction)];
    
    self.navigationItem.rightBarButtonItems = @[approveBtn,declineBtn];
}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_APPROVALPORTION_DESCRIPTION parameter:[NSMutableDictionary new]];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@ Detail",SINGLETON_INSTANCE.barTitle];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)assignedViewTapped{
    
    AssignedListViewController *listVC = [[AssignedListViewController alloc]initWithNibName:@"AssignedListViewController" bundle:nil];
    
    [listVC setAssignedText:PortionAssigned];
    
    [listVC setAssignedListArray:assigneeArr];
    
    [listVC showInViewController:self];
}

// Method to calculate the no of days between two dates
-(NSInteger) calculateNoOfDaysBetween:(NSString *)dayOne andDayTwo:(NSString *)dayTwo{
    
    NSDate *firstDate = [SINGLETON_INSTANCE dateFromString:dayOne withFormate:DATE_FORMATE_SERVICE_RESPONSE];
    
    NSDate *secondDate = [SINGLETON_INSTANCE dateFromString:dayTwo withFormate:DATE_FORMATE_SERVICE_RESPONSE];
    
    float timeInterval = [secondDate timeIntervalSinceDate:firstDate];
    
    NSInteger noOfDays = (NSInteger)timeInterval/(60*60*24);
    
    return noOfDays+1;
}

#pragma mark - AlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == APPROVAL_TYPE_APPROVE) {
        
        if (buttonIndex) {
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidUserTransID"];
            
            [param setValue:[NSString stringWithFormat:@"%ld",(long)APPROVAL_TYPE_APPROVE] forKey:@"intApprovedStatus"];
            
            [param setValue:@[self.portionObj.transID] forKey:@"guidPortionTransID"];
            
            NSDictionary *updateParam = @{@"ApprovePortions":param};
            
            [self updatePortionsWithParam:updateParam withApprovalType:APPROVAL_TYPE_APPROVE];
        }
    }
    else if (alertView.tag == APPROVAL_TYPE_DECLINE){
        
        if (buttonIndex) {
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidUserTransID"];
            
            [param setValue:[NSString stringWithFormat:@"%ld",(long)APPROVAL_TYPE_DECLINE] forKey:@"intApprovedStatus"];
            
            [param setValue:@[self.portionObj.transID] forKey:@"guidPortionTransID"];
            
            NSDictionary *updateParam = @{@"ApprovePortions":param};
            
            [self updatePortionsWithParam:updateParam withApprovalType:APPROVAL_TYPE_DECLINE];
        }
    }
    
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

#pragma mark - Button actions

-(void) approveButtonAction{
    
    UIAlertView *approveAlertView = [[UIAlertView alloc]initWithTitle:@"Confirm" message:@"Do you want to Approve this Portion?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    
    [approveAlertView setTag:APPROVAL_TYPE_APPROVE];
    
    [approveAlertView show];
}

-(void) declineButtonAction{
    
    UIAlertView *declinedAlertView = [[UIAlertView alloc]initWithTitle:@"Confirm" message:@"Do you want to Decline this Portion?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    
    [declinedAlertView setTag:APPROVAL_TYPE_DECLINE];
    
    [declinedAlertView show];
}

- (IBAction)attachmentBtnClk:(id)sender {
    
    NSString *attachmentURLString;
    
    attachmentURL = [NSURL URLWithString:_portionObj.attachment];
    
    attachmentURLString = _portionObj.attachment;
    
    if (attachmentURLString.length>0) {
        
        NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSString *attachmentFile = [documentsPath stringByAppendingPathComponent:[attachmentURL lastPathComponent]];
        
        NSLog(@"%@",attachmentFile);
        
        NSLog(@"%@",[attachmentURL lastPathComponent]);
        
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:attachmentFile];
        
        if (fileExists) {
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            [analyticsDict setValue:ANALYTICS_VALUE_OPENEDFROMLOCAL forKey:ANALYTICS_PARAM_ATTACHMENTSTATUS];
            
            [TSAnalytics logEvent:ANALYTICS_PORTIONAPPROVALS_CLICKED_ATTACHMENT parameter:analyticsDict];
            
            [self navigateToAttachmentViewControllerWithFileName:attachmentURL];
        }
        else{
            
            [[TSHandlerClass sharedHandler]downloadAttachmentWithURL:attachmentURLString currentView:self.view withHandler:^(BOOL success, NSURL *filePath) {
                
                if (success) {
                    
                    NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DOWNLOADING forKey:ANALYTICS_PARAM_ATTACHMENTSTATUS];
                    
                    [TSAnalytics logEvent:ANALYTICS_PORTIONAPPROVALS_CLICKED_ATTACHMENT parameter:analyticsDict];
                    
                    [self navigateToAttachmentViewControllerWithFileName:filePath];
                }
                else{
                    
                    NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_URLNOTFOUND forKey:ANALYTICS_PARAM_ATTACHMENTSTATUS];
                    
                    [TSAnalytics logEvent:ANALYTICS_PORTIONAPPROVALS_CLICKED_ATTACHMENT parameter:analyticsDict];
                    
                    [TSSingleton showAlertWithMessage:@"Attachment might be have broken url"];
                }
            }];
        }
    }
    else{
        
        [TSSingleton showAlertWithMessage:TEXT_INVALID_ATTACHMENT];
    }
}

-(void) navigateToAttachmentViewControllerWithFileName:(NSURL *)fileURL{
    
    TSAttachmentViewController *attachmentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAttachment"];
    
    [attachmentVC setFileName:[fileURL lastPathComponent]];
    
    UINavigationController *navCtrl = [[UINavigationController alloc]initWithRootViewController:attachmentVC];
    
    navCtrl.navigationBar.translucent = NO;
    
    navCtrl.navigationBar.barTintColor = APPTHEME_COLOR;
    
    navCtrl.navigationBar.tintColor = [UIColor whiteColor];
    
    [navCtrl.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:APP_FONT size:18]}];
    
    [self.navigationController presentViewController:navCtrl animated:YES completion:nil];
}


#pragma mark - Service Call

// Update Assignments Service Call
-(void) updatePortionsWithParam:(NSDictionary *)param withApprovalType:(NSInteger)approvalType{
    
    NSString *urlString = @"MastersMServices/PortionsService.svc/MobileApprovePortions";
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
    
            
            if ([responseDictionary[@"MobileApprovePortionsResult"][@"ResponseCode"] integerValue] == 1) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                if (approvalType == APPROVAL_TYPE_APPROVE) {
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                else{
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                
                [TSAnalytics logEvent:ANALYTICS_PORTIONAPPROVALS_UPDATEAPPROVAL parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"MobileApprovePortionsResult"][@"ResponseMessage"]];

                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@ AND isForApprovals == 1",SINGLETON_INSTANCE.staffTransId];
                
                [SINGLETON_INSTANCE deleteSelectedObjectsInEntityWithEntityName:@"Portions" withPredicate:predicate withContext:[APPDELEGATE_INSTANCE managedObjectContext]];
                
                [[NSNotificationCenter defaultCenter]postNotificationName:@"updateTableView" object:nil];
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            else{
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                if (approvalType == APPROVAL_TYPE_APPROVE) {
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                else{
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                
                [TSAnalytics logEvent:ANALYTICS_PORTIONAPPROVALS_UPDATEAPPROVAL parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
            }
        }
        else{
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
            
            if (approvalType == APPROVAL_TYPE_APPROVE) {
                
                [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
            }
            else{
                
                [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
            }
            
            [TSAnalytics logEvent:ANALYTICS_PORTIONAPPROVALS_UPDATEAPPROVAL parameter:analyticsDict];
        }
    }];
}

-(void) descViewTapped{
    
    [TSAnalytics logEvent:ANALYTICS_PORTIONAPPROVALS_CLICKED_DESCRIPTION parameter:[NSMutableDictionary new]];
    
    TSDescriptionViewController *descVC = [[TSDescriptionViewController alloc] initWithNibName:@"TSDescriptionViewController" bundle:nil];
    
    [descVC showInViewController:self descriptionString:_descriptionTextView.text];
    
}


@end