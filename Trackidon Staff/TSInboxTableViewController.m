//
//  TSInboxTableViewController.m
//  Trackidon Staff
//
//  Created by ORGware on 18/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSInboxTableViewController.h"

@interface TSInboxTableViewController ()

@end

@implementation TSInboxTableViewController
{
    NSArray *menuCountArr;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMenuCount) name:@"reloadPushCount" object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_INBOXHOME parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    [self.tableView setTableFooterView:[UIView new]];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Inbox";
    
    [self getMenuCount];
//    [self fetchInbox];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (void)getMenuCount{
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"staffTransId =  %@",SINGLETON_INSTANCE.staff.staffTransId];
    
    menuCountArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"PushCount" withPredicate:pred];
    
    [self.tableView reloadData];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [SINGLETON_INSTANCE inboxMenuArr].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSDictionary *menuDict = [[SINGLETON_INSTANCE inboxMenuArr] objectAtIndex:indexPath.row];
    
    UIImageView *placeholderImgVw = (UIImageView *)[cell viewWithTag:400];
    
    UILabel *menuLbl = (UILabel *)[cell viewWithTag:401];
    
    UILabel *countLbl = (UILabel *)[cell viewWithTag:402];
    
    countLbl.layer.cornerRadius = countLbl.frame.size.height/2;
    
    countLbl.layer.masksToBounds = YES;
    
    //if ([SINGLETON_INSTANCE.staff.staffRole integerValue] == 7) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"commTypeID = %@ AND isRead = 0",[NSString stringWithFormat:@"%@",menuDict[@"MenuID"]]];
        
        NSArray *countArray =[menuCountArr filteredArrayUsingPredicate:predicate];
        
        if(countArray.count > 0){
            
            PushCount *countObj = [countArray objectAtIndex:0];
            
            countLbl.text = [NSString stringWithFormat:@"%@",countObj.unreadCount];
            
            if([countLbl.text integerValue] == 0)
                [countLbl setHidden:YES];
            else
                [countLbl setHidden:NO];
            
        } else {
            [countLbl setHidden:YES];
        }
        
    /*} else {
        
        [countLbl setHidden:YES];
        
    }*/

        
    switch ([menuDict[@"MenuID"] integerValue]) {
        
        case MENU_INBOX_NOTIFICATION:
            
            placeholderImgVw.image = [UIImage imageNamed:@"notifications"];
            break;
            
        case MENU_INBOX_EVENTS:
            
            placeholderImgVw.image = [UIImage imageNamed:@"events"];
            break;
            
        case MENU_INBOX_PORTION:
            
            placeholderImgVw.image = [UIImage imageNamed:@"portions"];
            break;
            
        default:
            break;
    }
    
    menuLbl.text = menuDict[@"MenuName"];
        
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *menuDict = [[SINGLETON_INSTANCE inboxMenuArr] objectAtIndex:indexPath.row];
    
    [[PushCount instance] updatePushCountsFormenuId:[NSString stringWithFormat:@"%@",menuDict[@"MenuID"]] isForApproval:NO];
    
    switch ([menuDict[@"MenuID"] integerValue]) {
            
        case MENU_INBOX_NOTIFICATION:
        {
            TSNotificationInboxViewController *notificationInboxVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBNotificationInbox"];
            
            [self.navigationController pushViewController:notificationInboxVC animated:YES];
            
            break;
        }
        case MENU_INBOX_EVENTS:
        {
            TSEventInboxViewController *eventInboxVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBEventInbox"];
            
            [self.navigationController pushViewController:eventInboxVC animated:YES];
        
            break;
        }
        case MENU_INBOX_PORTION:
        {
            TSPortionInboxViewController *portionInboxVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBPortionInbox"];
            
            [self.navigationController pushViewController:portionInboxVC animated:YES];
        }
        default:
            break;
         
    }
    
}

//-(void)fetchInbox{
//    
//    NSMutableDictionary *param = [NSMutableDictionary dictionary];
//    
//    NSString *url = @"MastersMServices/SchoolNotificationsService.svc/FetchSchoolStaffNotificationsByStaffID";
//    
//    [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidStaffTransID"];
//    
//    [[TSHandlerClass sharedHandler]postRequestWithURL:url baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
//      
//        if (success) {
//            
//            if ([responseDictionary[@"FetchSchoolStaffNotificationsByStaffIDResult"][@"ResponseCode"]intValue]==1){
//                
//            NSLog(@"%@",responseDictionary);
//                
//                if (responseDictionary[@"FetchSchoolStaffNotificationsByStaffIDResult"][@"SchoolNotificationClass"] != [NSNull null]) {
//                    
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                        
//                        [[NotificationInbox instance]saveNotificationsInboxWithResponseDict:responseDictionary];
//                        
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            
//                        });
//                    });
//                }
//                 if(responseDictionary[@"FetchSchoolStaffNotificationsByStaffIDResult"][@"EventsMClass"] != [NSNull null]){
//                    
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                        
//                        [[EventInbox instance]saveEventInboxWithResponseDict:responseDictionary];
//                        
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            
//                        });
//                    });
//                }
//                
//            }else{
//                
//                [TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
//            }
//        }
//    }];
//}
@end