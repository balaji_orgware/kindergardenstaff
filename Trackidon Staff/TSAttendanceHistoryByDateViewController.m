//
//  TSAttendanceHistoryByDateViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 31/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSAttendanceHistoryByDateViewController.h"

@interface TSAttendanceHistoryByDateViewController (){
    
    BOOL isTopScroll;
    
    float containerViewOriginY;
    
    Board *boardObj;
    
    NSArray *boardArray;
    
    NSDate *selectedDate;
    
    NSMutableArray *studentsListArray;
    
    NSString *selectedBoardTransID,*selectedClassTransID,*selectedSectionTransID;
    
    TSSelectionViewController *selectionView;
    
    UITapGestureRecognizer *tapOnDateView;
}

@end

@implementation TSAttendanceHistoryByDateViewController

#pragma mark - View lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    isTopScroll = YES;
    
    containerViewOriginY = self.containerView.frame.origin.y;
    
    studentsListArray = [NSMutableArray new];
    
    selectedDate = [NSDate date];
    
    NSPredicate *boardPredicate = [NSPredicate predicateWithFormat:@"schoolTransId contains[cd] %@ AND isViewableByStaff == 1",SINGLETON_INSTANCE.selectedSchoolTransId];
    
    boardArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Board" withPredicate:boardPredicate];
    
    tapOnDateView = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dateViewTapped)];
    
    [self.dateSelectionView addGestureRecognizer:tapOnDateView];
    
    [self changeDateSelectionViewWithDate:selectedDate];
    
    {
        [SINGLETON_INSTANCE addDropDownArrowToTextField:_selectBoardTxtFld];
        
        [SINGLETON_INSTANCE addDropDownArrowToTextField:_selectClassTxtFld];
        
        [SINGLETON_INSTANCE addDropDownArrowToTextField:_selectSectionTxtFld];
        
        [SINGLETON_INSTANCE addLeftInsetToTextField:_selectBoardTxtFld];
        
        [SINGLETON_INSTANCE addLeftInsetToTextField:_selectClassTxtFld];
        
        [SINGLETON_INSTANCE addLeftInsetToTextField:_selectSectionTxtFld];
        
        [TSSingleton layerDrawForView:_dateSelectionView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        
        [TSSingleton layerDrawForView:_selectBoardTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        
        [TSSingleton layerDrawForView:_selectClassTxtFld position:LAYER_RIGHT color:[UIColor lightGrayColor]];
                
        [TSSingleton layerDrawForView:_totalNoOfStudentsView position:LAYER_TOP color:[UIColor lightGrayColor]];
        
        [TSSingleton layerDrawForView:_totalNoOfStudentsView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    }
}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_ATTENDANCEVIEWHISTORY_BYDATE parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Attendance History";
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

#pragma mark - Methods

// Date Selection View Tap Action
-(void) dateViewTapped{
    
    TSDatePickerViewController *datePickerVC = [[TSDatePickerViewController alloc]initWithNibName:@"TSDatePickerViewController" bundle:nil];
    
    [datePickerVC showInViewController:self calendarType:CALENDAR_TYPE_DATE withMinDate:nil withMaxDate:[NSDate date]];
    
    datePickerVC.delegate = self;
}

// Method to assign given date to Date Label and Day name label.
-(void) changeDateSelectionViewWithDate:(NSDate *)date{
    
    _dateLabel.text = [SINGLETON_INSTANCE stringFromDateWithFormate:DATE_FORMATE_MNTH_DAY_YR withDate:date];
    
    _dayNameLabel.text = [SINGLETON_INSTANCE stringFromDateWithFormate:@"EEEE" withDate:date];
}

#pragma mark - DatePickerViewController Delegate Method

-(void)selectedDate:(NSDate *)date{
    
    [self changeDateSelectionViewWithDate:date];
    
    selectedDate = date;
    
    _studentsCountLabel.text = @"0"
    ;
    if (selectedBoardTransID != nil && selectedClassTransID !=nil && selectedSectionTransID !=nil) {
        
        [self fetchAttendanceRecordsAPICall];
    }
}

#pragma mark - Selection View

-(void) showSelectionViewWithSelected:(NSString *)selected andTextField:(UITextField *)textField{

    if (!selectionView) {
        
        selectionView = [[TSSelectionViewController alloc]initWithNibName:@"TSSelectionViewController" bundle:nil];
        
        selectionView.delegate = self;
        
        NSMutableDictionary *contentDict = [NSMutableDictionary new];
        
        if (textField == _selectBoardTxtFld) {
            
            for (Board *board in boardArray) {
                
                [contentDict setObject:board.boardTransId     forKey:board.boardName];
            }
        }
        else if (textField == _selectClassTxtFld){
            
            NSPredicate *filterPred = [NSPredicate predicateWithFormat:@"boardTransId MATCHES %@",selectedBoardTransID];
            
            NSArray *filteredArray = [NSMutableArray arrayWithArray: [boardArray filteredArrayUsingPredicate:filterPred]];
            
            boardObj = [filteredArray objectAtIndex:0];
            
            for (Classes *classObj in boardObj.classes) {
                
                [contentDict setObject:classObj.classTransId forKey:classObj.classStandardName];
            }
            
        }
        else if(textField == _selectSectionTxtFld){
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"classTransId MATCHES %@",selectedClassTransID];
            
            NSSet *filteredSet = [boardObj.classes filteredSetUsingPredicate:predicate];
            
            Classes *classObj = [[filteredSet allObjects] objectAtIndex:0];
            
            for (Section *secObj in classObj.section) {
                
                [contentDict setObject:secObj.sectionTransId forKey:secObj.sectionName];
            }
        }
        
        if (contentDict.count>0) {
            
            [selectionView setContentDict:contentDict];
            
            selectionView.selectedValue = textField.text.length>0? textField.text:@"";
            
            [selectionView showSelectionViewController:self forTextField:textField];
        }
        else{
            
            if (textField == _selectBoardTxtFld) {
                
                [TSSingleton showAlertWithMessage:@"No Boards to choose"];
            }
            else if (textField == _selectClassTxtFld){
                
                [TSSingleton showAlertWithMessage:@"No Classes to choose"];
            }
            else if (textField == _selectSectionTxtFld){
                
                [TSSingleton showAlertWithMessage:@"No Sections to choose"];
            }
            
            [selectionView removeSelectionViewController:self];
            
            selectionView = nil;
        }
    }
}

#pragma mark - Selection View Delegate Methods

-(void)selectedValueFromDict:(NSDictionary *)contentDict withSelectedValue:(NSString *)selectedValue andTransID:(NSString *)transID forTextField:(UITextField *)textField{
    
    if (textField == _selectBoardTxtFld) {
        
        selectedBoardTransID = transID;
    }
    else if (textField == _selectClassTxtFld){
        
        selectedClassTransID = transID;
    }
    else if (textField == _selectSectionTxtFld){
        
        selectedSectionTransID = transID;
        
        [self fetchAttendanceRecordsAPICall];
    }
    
    selectionView = nil;
}

-(void)backViewTappedOn:(UIViewController *)viewController{
    
    selectionView = nil;
}

#pragma mark - Scroll View Delegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView == _studentsTableView && [self.studentsTableView numberOfRowsInSection:0]>5) {
        
        if (scrollView.contentOffset.y>0) {
            
            if (isTopScroll) {
                
                CGRect newContainerFrame = self.containerView.frame;
                CGRect newStudentTableFrame = self.studentsTableView.frame;
                
                newContainerFrame.origin.x = self.containerView.frame.origin.x;
                newContainerFrame.origin.y = self.containerView.frame.origin.y - scrollView.contentOffset.y/2.0;
                [self.containerView setFrame:newContainerFrame];
                
                newStudentTableFrame.origin.x = self.studentsTableView.frame.origin.x;
                newStudentTableFrame.origin.y = self.studentsTableView.frame.origin.y-scrollView.contentOffset.y/2.0;
                newStudentTableFrame.size.height = DeviceHeight -64-newStudentTableFrame.origin.y;
                [self.studentsTableView setFrame: newStudentTableFrame];
                
                if (newStudentTableFrame.origin.y < containerViewOriginY) {
                    
                    newStudentTableFrame.origin.y = containerViewOriginY;
                    newStudentTableFrame.origin.x = 0;
                    newStudentTableFrame.size.height = DeviceHeight -64;
                    [self.studentsTableView setFrame:newStudentTableFrame];
                    
                    isTopScroll = NO;
                }
            }
        }
        else if(scrollView.contentOffset.y<0){
            
            //            if (!isTopScroll) {
            
            isTopScroll = YES;
            
            [UIView animateWithDuration:0.2 animations:^{
                
                [self.containerView setHidden:NO];
                
                [self.containerView setFrame:CGRectMake(0, 0, DeviceWidth, self.containerView.frame.size.height)];
                
                [self.studentsTableView setFrame:CGRectMake(0, self.containerView.frame.origin.y+self.containerView.frame.size.height, DeviceWidth, DeviceHeight -64 - self.containerView.frame.size.height)];
            }];
            
            //            }
        }
    }
}

#pragma mark - Textfield Delegate methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    [studentsListArray removeAllObjects];
    
    [self.studentsTableView reloadData];
        
    if (textField == _selectBoardTxtFld) {
        
        selectedClassTransID = nil;
        
        selectedSectionTransID = nil;
        
        _selectClassTxtFld.text = @"";
        
        _selectSectionTxtFld.text = @"";
        
        [self showSelectionViewWithSelected:textField.text andTextField:textField];
        
//        _selectBoardTxtFld.text = @"";
    }
    else if (textField == _selectClassTxtFld) {
        
        if (_selectBoardTxtFld.text.length>0) {
            
            selectedSectionTransID = nil;
            
            _selectSectionTxtFld.text = @"";
            
            [self showSelectionViewWithSelected:textField.text andTextField:textField];
            
//            _selectClassTxtFld.text = @"";
        }
    }
    else{
        
        if (_selectBoardTxtFld.text.length>0 && _selectClassTxtFld.text.length >0) {
            
            [self showSelectionViewWithSelected:textField.text andTextField:textField];
            
//            _selectSectionTxtFld.text = @"";
        }
    }
    
    return NO;
}

#pragma mark - Tableview Datasources and Delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    _studentsCountLabel.text = [NSString stringWithFormat:@"%ld",(long)studentsListArray.count];
    
    return studentsListArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sCellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    StudentModel *studentObj = [studentsListArray objectAtIndex:indexPath.row];
    
    UILabel *studentName = (UILabel *)[cell viewWithTag:100];
    
    UIImageView *statusImgVw = (UIImageView *)[cell viewWithTag:101];
    
    studentName.text = studentObj.studentName;
    
    switch (studentObj.attendanceStatus) {
            
        case ATTENDANCE_PRESENT:
            
            statusImgVw.image = [UIImage imageNamed:@"present_state"];
            break;
            
        case ATTENDANCE_ABSENT:
            
            statusImgVw.image = [UIImage imageNamed:@"absent_state"];
            break;
            
        case ATTENDANCE_LATE:
            
            statusImgVw.image = [UIImage imageNamed:@"tardy_toggle"];
            break;
            
        default:
            break;
    }

    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 106;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 106)];
    
    float countLabelWidth = (headerView.frame.size.width/3)-30;
    
    {
        self.presentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth/3, headerView.frame.size.height)];
        [self.presentView setBackgroundColor:[UIColor whiteColor]];
        
        self.presentImgView = [[UIImageView alloc]initWithFrame:CGRectMake(self.presentView.center.x-25, self.presentView.center.y -40, 50, 50)];
        self.presentImgView.image = [UIImage imageNamed:@"present"];
        
        self.presentCountLabel = [[UILabel alloc]initWithFrame:CGRectMake((DeviceWidth/3-countLabelWidth)/2, self.presentImgView.frame.origin.y+self.presentImgView.frame.size.height+10,countLabelWidth, 21)];
        [self.presentCountLabel setTextColor:COLOR_ACCEPT_CREATEDTODAY];
        [self.presentCountLabel setTextAlignment:NSTextAlignmentCenter];
        [self.presentCountLabel setFont:[UIFont fontWithName:APP_FONT size:17]];
        
        [self.presentView addSubview:self.presentImgView];
        [self.presentView addSubview:self.presentCountLabel];
    }
    {
        self.absentView = [[UIView alloc]initWithFrame:CGRectMake(DeviceWidth/3, 0, DeviceWidth/3, headerView.frame.size.height)];
        [self.absentView setBackgroundColor:[UIColor whiteColor]];
        
        self.absentImgView = [[UIImageView alloc]initWithFrame:CGRectMake((self.absentView.frame.size.width-50)/2, self.absentView.center.y -40, 50, 50)];
        self.absentImgView.image = [UIImage imageNamed:@"absent"];
        
        
        self.absentCountLabel = [[UILabel alloc]initWithFrame:CGRectMake((DeviceWidth/3-countLabelWidth)/2, self.absentImgView.frame.origin.y+self.absentImgView.frame.size.height+10,countLabelWidth, 21)];
        [self.absentCountLabel setTextColor:COLOR_DECLINED_DUESTODAY];
        [self.absentCountLabel setTextAlignment:NSTextAlignmentCenter];
        [self.absentCountLabel setFont:[UIFont fontWithName:APP_FONT size:17]];
        
        [self.absentView addSubview:self.absentImgView];
        [self.absentView addSubview:self.absentCountLabel];
    }
    {
        self.lateView = [[UIView alloc]initWithFrame:CGRectMake(2*(DeviceWidth/3), 0, DeviceWidth/3, headerView.frame.size.height)];
        [self.lateView setBackgroundColor:[UIColor whiteColor]];
        
        self.lateImgView = [[UIImageView alloc]initWithFrame:CGRectMake((self.absentView.frame.size.width-50)/2, self.lateView.center.y -40, 50, 50)];
        
        self.lateImgView.image = [UIImage imageNamed:@"late"];
        
        self.lateCountLabel = [[UILabel alloc]initWithFrame:CGRectMake((DeviceWidth/3-countLabelWidth)/2, self.lateImgView.frame.origin.y+self.lateImgView.frame.size.height+10,countLabelWidth, 21)];
        [self.lateCountLabel setTextColor:COLOR_PENDING_VIEWALL];
        [self.lateCountLabel setTextAlignment:NSTextAlignmentCenter];
        [self.lateCountLabel setFont:[UIFont fontWithName:APP_FONT size:17]];
        
        [self.lateView addSubview:self.lateImgView];
        [self.lateView addSubview:self.lateCountLabel];
    }
    
    [self updateStudentAttendanceCounts];
    
    [headerView addSubview:self.presentView];
    
    [headerView addSubview:self.absentView];
    
    [headerView addSubview:self.lateView];
    
    return headerView;
}

#pragma mark - Attendance Count Update Method

// Method to Get Total Attendance Status Counts for Present, Absent, Late
-(void) updateStudentAttendanceCounts{
    
    NSPredicate *presentPredicate = [NSPredicate predicateWithFormat:@"attendanceStatus == %d",ATTENDANCE_PRESENT];
    
    NSPredicate *absentPredicate = [NSPredicate predicateWithFormat:@"attendanceStatus == %d",ATTENDANCE_ABSENT];
    
    NSPredicate *latePredicate = [NSPredicate predicateWithFormat:@"attendanceStatus == %d",ATTENDANCE_LATE];
    
    self.presentCountLabel.text = [NSString stringWithFormat:@"%ld",(unsigned long)[[studentsListArray filteredArrayUsingPredicate:presentPredicate] count]];
    
    self.absentCountLabel.text = [NSString stringWithFormat:@"%ld",(unsigned long)[[studentsListArray filteredArrayUsingPredicate:absentPredicate] count]];
    
    self.lateCountLabel.text = [NSString stringWithFormat:@"%ld",(unsigned long)[[studentsListArray filteredArrayUsingPredicate:latePredicate] count]];
}

#pragma mark - Service Calls

// Fetch Class Students Attendance list api call
-(void) fetchAttendanceRecordsAPICall{
    
    NSString *urlString = @"MastersMServices/AttendanceService.svc/ClassAttendanceByDate";
    
    NSString *dateString = [SINGLETON_INSTANCE stringFromDateWithFormate:DATE_FORMATE_SERVICE_CALL withDate:selectedDate];
    
    NSDictionary *params = @{@"guidBoardTransID":selectedBoardTransID,@"guidClassTansID":selectedClassTransID,@"guidSectionTransID":selectedSectionTransID,@"DTAttDate":dateString};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if([responseDictionary[@"ClassAttendanceByDateResult"][@"ResponseCode"] intValue] == 1) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:_selectBoardTxtFld.text forKey:ANALYTICS_PARAM_BOARDNAME];
                
                [analyticsDict setValue:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_SEARCH_HISTORYBYDATE parameter:analyticsDict];
                
                [studentsListArray removeAllObjects];
                
                for (NSDictionary *studDict in responseDictionary[@"ClassAttendanceByDateResult"][@"AttendanceClassData"]) {
                    
                    StudentModel *studentObj = [StudentModel new];
                    
                    studentObj.enrollNo = studDict[@"EnrollNo"];
                    studentObj.studentName = studDict[@"NAME"];
                    studentObj.studentAbsent = [studDict[@"StudAbsent"] boolValue];
                    studentObj.studentImage = studDict[@"StudImage"];
                    studentObj.studentPresent = [studDict[@"StudPresent"] boolValue];
                    studentObj.studentTardy = [studDict[@"StudTardy"] boolValue];
                    studentObj.studentTransID = studDict[@"StudentTransID"];
                    studentObj.transID = studDict[@"TransId"];
                    
                    if ([studDict[@"StudPresent"] boolValue]) {
                        
                        studentObj.attendanceStatus = ATTENDANCE_PRESENT;

                        [studentsListArray addObject:studentObj];
                    }
                    else if ([studDict[@"StudAbsent"] boolValue]){
                        
                        studentObj.attendanceStatus = ATTENDANCE_ABSENT;
                        
                        [studentsListArray addObject:studentObj];
                    }
                    else if([studDict[@"StudTardy"] boolValue]){
                        
                        studentObj.attendanceStatus = ATTENDANCE_LATE;
                        
                        [studentsListArray addObject:studentObj];
                    }
                    else{
                        
                        studentObj.attendanceStatus = ATTENDANCE_NOTAPPLICABLE;
                    }
                    
                    studentObj = nil;
                }
            }
            else{
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:_selectBoardTxtFld.text forKey:ANALYTICS_PARAM_BOARDNAME];
                
                [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_SEARCH_HISTORYBYDATE parameter:analyticsDict];
                
                [studentsListArray removeAllObjects];
                
                [TSSingleton showAlertWithMessage:@"Error fetching records"];
            }
            
            if (studentsListArray.count>0) {
                
                [self.studentsTableView setHidden:NO];
                
                [self.studentsTableView reloadData];
            }
            else{
                
                [self.studentsTableView setHidden:YES];
                
//                [TSSingleton showAlertWithMessage:@"No Attendance history available for this date"];
            }
            
        }
        else{
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            [analyticsDict setValue:_selectBoardTxtFld.text forKey:ANALYTICS_PARAM_BOARDNAME];
            
            [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
            
            [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_SEARCH_HISTORYBYDATE parameter:analyticsDict];
        }
    }];
}

@end