//
//  TSDescriptionViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 5/20/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSDescriptionViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *descView;

@property (weak, nonatomic) IBOutlet UITextView *descTxtView;

- (void)showInViewController:(UIViewController *)viewController descriptionString:(NSString *)text;

@end
