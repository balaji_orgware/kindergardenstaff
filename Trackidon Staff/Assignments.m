//
//  Assignments.m
//  Trackidon Staff
//
//  Created by ORGware on 24/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "Assignments.h"

@implementation Assignments

// Method to get static instance
+(instancetype) instance{
    
    static Assignments *assignmentObj = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        assignmentObj = [[Assignments alloc]init];
    });
    
    return assignmentObj;
}

// Store Assignments details to Local db
-(void)storeAssignmentsDataForStaff:(StaffDetails *)staffObj withDict:(NSDictionary *)dictionary andIsForApprovals:(BOOL)isForApprovals{
    
    @try {
        
        NSManagedObjectContext *mainContext = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSArray *responseArray = [NSArray new];
        
        NSPredicate *predicate;
        
        if (isForApprovals) {// Assignments Approvals
            
            responseArray = dictionary[@"FetchUnApprovedCommunicationResult"][@"UnApprovalClass"][@"AssignmentClass"];
            
            predicate = [NSPredicate predicateWithFormat:@"isForApprovals == 1"];
            
        }
        else{ // Assignments
            
            responseArray = dictionary[@"FetchAssignmentsByStaffResult"][@"AssignmentClass"];
            
            predicate = [NSPredicate predicateWithFormat:@"isForApprovals == 0"];
        }
        
        [SINGLETON_INSTANCE deleteSelectedObjectsInEntityWithEntityName:@"Assignments" withPredicate:predicate withContext:mainContext];
        
        NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        
        [context setParentContext:mainContext];
        
        NSManagedObjectID *objectId = [SINGLETON_INSTANCE.staff objectID];
        
        StaffDetails *tmpStaff = [context objectWithID:objectId];
        
        for (NSDictionary *assignmentDict in responseArray) {
            
            NSDictionary *dict = [assignmentDict dictionaryByReplacingNullsWithBlanks];
            
            Assignments *assignmentObj;
            
            if (isForApprovals) {
                
                assignmentObj = [NSEntityDescription insertNewObjectForEntityForName:@"Assignments" inManagedObjectContext:context];
            }
            else{
                
                assignmentObj = [self isAssignmentAlreadyExistsWithTransID:dict[@"TransID"] forStaff:staffObj context:context withIsForApprovals:isForApprovals];
                
                if (assignmentObj == nil) {
                    
                    assignmentObj = [NSEntityDescription insertNewObjectForEntityForName:@"Assignments" inManagedObjectContext:context];
                }
            }
            
            assignmentObj.approvedByTransID = dict[@"ApprovedBy"];
            assignmentObj.approvedByName = dict[@"ApprovedByName"];
            assignmentObj.approvedDate = [SINGLETON_INSTANCE dateFromString:dict[@"ApprovedDate"] withFormate:DATE_FORMATE_SERVICE_RESPONSE];
            assignmentObj.approvedStatus = dict[@"ApprovedStatus"];
            assignmentObj.assignmentDate = [SINGLETON_INSTANCE dateFromString:dict[@"AssignmentDate"] withFormate:DATE_FORMATE_SERVICE_RESPONSE];
            assignmentObj.assignmentDescription = dict[@"AssignmentDescription"];
            assignmentObj.assignmentTitle = dict[@"AssignmentTitle"];
            assignmentObj.assignmentTypeName = dict[@"AssignmentTypeName"];
            assignmentObj.attachment = dict[@"Attachment"] == [NSNull null] ? @"" : dict[@"Attachment"];
            assignmentObj.createdBy = dict[@"CreatedBy"];
            assignmentObj.dueDate = [SINGLETON_INSTANCE dateFromString:dict[@"DueDate"] withFormate:DATE_FORMATE_SERVICE_RESPONSE];;
            assignmentObj.isAttachment = dict[@"IsAttachment"];
            assignmentObj.modifiedBy = dict[@"ModifiedBy"];
            assignmentObj.subjectName = dict[@"SubjectName"];
            assignmentObj.schoolName = dict[@"SchoolName"];
            assignmentObj.schoolTransID = dict[@"SchoolTransID"];
            assignmentObj.transID = dict[@"TransID"];
            assignmentObj.modifiedDate = [SINGLETON_INSTANCE dateFromString:dict[@"ModifiedDate"] withFormate:DATE_FORMATE_MODIFIED_DATE];
            
            assignmentObj.staffTransID = SINGLETON_INSTANCE.staffTransId;
            assignmentObj.staff = tmpStaff;
            
            assignmentObj.boardName = dict[@"BoardName"];
            assignmentObj.isForApprovals = [NSNumber numberWithBool:isForApprovals];
            
            //[[AssignedDetails instance]saveAssignedDetailsWithDict:dict[@"AssignmentDetails"] withGroupType:dict[@"GroupType"] transId:assignmentObj.transID forObject:assignmentObj objectType:OBJECT_TYPE_ASSIGNMENT context:context];
            
            NSString *assigneeList = [[TSCommonAssignedDetails instance] getAssignedDetailsWithArray:dict[@"AssignmentDetails"] withGroupType:dict[@"GroupType"]];
            
            assignmentObj.assignedList = assigneeList;
            
            NSLog(@"%@",assigneeList);
            
        }
        
        NSError *error = nil;
        
        if (![context save:&error]) {
            
            NSLog(@"Error during saving Assignments");
        }
        
        [SINGLETON_INSTANCE storeContextInMainThread];
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
    }
    @finally {
        
    }
}


// Method to check whether Holiday is already exists or not
-(Assignments *)isAssignmentAlreadyExistsWithTransID:(NSString *)transID forStaff:(StaffDetails *)staffObj context:(NSManagedObjectContext *)context withIsForApprovals:(BOOL)isForApprovals{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Assignments" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@ AND transID MATCHES %@ AND isForApprovals == %@",SINGLETON_INSTANCE.staffTransId,transID,[NSNumber numberWithBool:isForApprovals]];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
}

-(void)storePushNotificationAssignmentWithDict:(NSDictionary *)dictionary{
    
    @try {
        
        NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSError *error = nil;
        
        NSDictionary *dict = [dictionary[@"FetchAssignmentsByTransIDResult"][@"AssignmentClass"] dictionaryByReplacingNullsWithBlanks];
        
        Assignments *assignmentObj = [self isAssignmentAlreadyExistsWithTransID:dictionary[@"TransID"] forStaff:SINGLETON_INSTANCE.staff context:context withIsForApprovals:NO];
        
        if (assignmentObj == nil) {
            
            assignmentObj = [NSEntityDescription insertNewObjectForEntityForName:@"Assignments" inManagedObjectContext:context];
        }
        
        assignmentObj.approvedByTransID = dict[@"ApprovedBy"];
        assignmentObj.approvedByName = dict[@"ApprovedByName"];
        assignmentObj.approvedDate = [SINGLETON_INSTANCE dateFromString:dict[@"ApprovedDate"] withFormate:DATE_FORMATE_SERVICE_RESPONSE];
        assignmentObj.approvedStatus = dict[@"ApprovedStatus"];
        assignmentObj.assignmentDate = [SINGLETON_INSTANCE dateFromString:dict[@"AssignmentDate"] withFormate:DATE_FORMATE_SERVICE_RESPONSE];
        assignmentObj.assignmentDescription = dict[@"AssignmentDescription"];
        assignmentObj.assignmentTitle = dict[@"AssignmentTitle"];
        assignmentObj.assignmentTypeName = dict[@"AssignmentTypeName"];
        assignmentObj.attachment = dict[@"Attachment"] == [NSNull null] ? @"" : dict[@"Attachment"];
        assignmentObj.createdBy = dict[@"CreatedBy"];
        assignmentObj.dueDate = [SINGLETON_INSTANCE dateFromString:dict[@"DueDate"] withFormate:DATE_FORMATE_SERVICE_RESPONSE];;
        assignmentObj.isAttachment = dict[@"IsAttachment"];
        assignmentObj.modifiedBy = dict[@"ModifiedBy"];
        assignmentObj.subjectName = dict[@"SubjectName"];
        assignmentObj.schoolName = dict[@"SchoolName"];
        assignmentObj.schoolTransID = dict[@"SchoolTransID"];
        assignmentObj.transID = dict[@"TransID"];
        assignmentObj.modifiedDate = [SINGLETON_INSTANCE dateFromString:dict[@"ModifiedDate"] withFormate:DATE_FORMATE_MODIFIED_DATE];
        
        assignmentObj.staffTransID = SINGLETON_INSTANCE.staffTransId;
        assignmentObj.staff = SINGLETON_INSTANCE.staff;
        
        assignmentObj.boardName = dict[@"BoardName"];
        assignmentObj.isForApprovals = [NSNumber numberWithBool:NO];
        
        //[[AssignedDetails instance]saveAssignedDetailsWithDict:dict[@"AssignmentDetails"] withGroupType:dict[@"GroupType"] transId:assignmentObj.transID forObject:assignmentObj objectType:OBJECT_TYPE_ASSIGNMENT context:context];
        
        NSString *assigneeList = [[TSCommonAssignedDetails instance] getAssignedDetailsWithArray:dict[@"AssignmentDetails"] withGroupType:dict[@"GroupType"]];
        
        assignmentObj.assignedList = assigneeList;
        
        if (![context save:&error]) {
            
            NSLog(@"Whoops..Couldn't save");
        }
        
    } @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    } @finally {
        
    }
}
@end