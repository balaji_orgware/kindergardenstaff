//
//  TSConstants.h
//  Trackidon Staff
//
//  Created by ORGware on 16/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#ifndef TSConstants_h
#define TSConstants_h

// Web Service URLs

//#define SCHOOL_CODE_BASE_URL @"http://192.168.10.190/AdminApi/"

//#define SCHOOL_CODE_BASE_URL @"http://192.168.10.190/TrackidonAdminDemoApi/"

//#define SCHOOL_CODE_BASE_URL @"http://192.168.10.190/RCommonapi/"

#define SCHOOL_CODE_BASE_URL @"http://103.230.85.92/CommonDBMasterAPI/"

#define DEBUGGING YES
#define NSLog if(DEBUGGING)NSLog

// Constants

#define DEVICE_SIZE [UIScreen mainScreen].bounds.size

// iOS Devices
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_4  ([[UIScreen mainScreen] bounds].size.height == 480)?TRUE:FALSE
#define IS_IPHONE_5  ([[UIScreen mainScreen] bounds].size.height == 568)?TRUE:FALSE
#define IS_IPHONE_6  ([[UIScreen mainScreen] bounds].size.height == 667)?TRUE:FALSE
#define IS_IPHONE_6P  ([[UIScreen mainScreen] bounds].size.height == 736)?TRUE:FALSE

#define DeviceSystemVersion [[[UIDevice currentDevice]systemVersion]floatValue]

#define SYSTEM_VERSION_LESS_THAN(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)


#define DeviceWidth [[UIScreen mainScreen] bounds].size.width
#define DeviceHeight [[UIScreen mainScreen] bounds].size.height

#define APP_FONT @"VarelaRound-Regular"

// Title Length

#define MAX_TITLE_LENGTH 42
#define TEXT_TITLE_LENGTH_EXCEED @"Title should not exceed 42 characters"

// Border Positions

#define Bottom @"BOTTOM"
#define Top @"TOP"
#define Left @"LEFT"
#define Right @"Right"

#define layerThickness 0.5f
#define MAX_PASSWORD_LENGTH 5


// Colors

#define APPTHEME_COLOR  [UIColor colorWithRed:238.0/255.0 green:82.0/255.0 blue:87.0/255.0 alpha:1.0]

#define APP_BLACK_COLOR [UIColor colorWithRed:32.0/255.0 green:32.0/255.0 blue:32.0/255.0 alpha:1.0]

#define APP_PLACEHOLDER_COLOR [UIColor colorWithRed:199.0/255.0 green:199.0/255.0 blue:205.0/255.0 alpha:1.0]

#define COLOR_PENDING_VIEWALL [UIColor colorWithRed:250.0/255.0 green:173.0/255.0 blue:113.0/255.0 alpha:1.0]

#define COLOR_DECLINED_DUESTODAY [UIColor colorWithRed:247.0/255.0 green:115.0/255.0 blue:119.0/255.0 alpha:1.0]

#define COLOR_ACCEPT_CREATEDTODAY [UIColor colorWithRed:104.0/255.0 green:203.0/255.0 blue:159.0/255.0 alpha:1.0]

#define HolidaysSchoolBGcolor [UIColor colorWithRed:0.0/255.0 green:133/255.0 blue:199.0/255.0 alpha:1.0]

#define HolidaysPublicBGcolor [UIColor colorWithRed:0.0/255.0 green:190/255.0 blue:181.0/255.0 alpha:1.0]

#define BGcolor [UIColor colorWithRed:0.0/255.0 green:190/255.0 blue:181.0/255.0 alpha:1.0]

#define SAPRATOR_COLOR [UIColor lightGrayColor]

// Common Description

#define EventsString @"Events"
#define NotificationsString @"Notification"
#define EventsInboxString @"EventInbox"
#define NotificationsInboxString @"NotificationInbox"
#define PortionString @"Portion"
#define PortionInboxString @"PortionInbox"

// Font

#define APP_FONT @"VarelaRound-Regular"

// Assigned To

#define AssignmentAssigned @"Assigned To"
#define EventInvited @"Invited To"
#define NotificationNotified @"Notified To"
#define PortionAssigned @"Assigned To"
// EMPTY State Messages

#define TEXT_NOINTERNET @"Oops! No Internet connection"
#define TEXT_FAILURERESPONSE @"Something went wrong. Please try again."

// Date Formats

#define DATE_FORMATE_SERVICE_RESPONSE @"dd-MMM-yyyy"

#define DATE_FORMATE_SERVICE_CALL @"yyyy-MM-dd"

#define DATE_FORMATE_MODIFIED_DATE @"dd-MMM-yyyy HH:mm"

#define DATE_FORMATE_DAY_MNTH_YR @"dd MMM yyyy"

#define DATE_FORMATE_MNTH_DAY_YR @"MMMM dd, yyyy"

// UserDefaults

#define BASE_URL @"BaseURL"
#define IS_LOGIN @"isLogin"
#define USER_PASSWORD @"Password"
#define PARENT_TRANSID @"parentTransID"
#define USERNAME @"username"


#define USERDEFAULTS [NSUserDefaults standardUserDefaults]

#define DEVICE_SIZE [UIScreen mainScreen].bounds.size


// AppDelegate Instance

#define APPDELEGATE_INSTANCE (TSAppDelegate *)[[UIApplication sharedApplication] delegate]

#define SINGLETON_INSTANCE [TSSingleton singletonSharedInstance]

//STRING

#define TEXT_APPROVED @"Approved"
#define TEXT_DECLINED @"Declined"
#define TEXT_PENDING @"Pending"

#define INCORRECT_SCHOOL_CODE_TITLE @"School Not Found"
#define INCORRECT_SCHOOL_CODE_MESSAGE @"Enter Valid School Code"

#define INCORRECT_LOGIN_TITLE @"Can't Login"
#define INCORRECT_LOGIN_MESSAGE @"Please Check Your UserName or Password"
#define INCORRECT_USERNAME_MESSAGE @"Please Enter Valid EmailId or Phone No"
#define INCORRECT_PASSWORD_MESSAGE @"Please Enter Password"

#define LOGOUT_SUCCESS_MESSAGE @"Logout Successfully"
#define LOGOUT_FAILED_MESSAGE @"Logout Failed"

#define DESCRIPTION_TEXT @"Description"
#define TEXT_EMPTY_SUBJECT @"Please choose Subject"
#define TEXT_EMPTY_TITLE @"Please Enter Title"
#define TEXT_EMPTY_TIME @"Please Enter Time"
#define TEXT_EMPTY_VENUE @"Please Enter Venue"
#define TEXT_EMPTY_DESCRIPTION @"Please Enter Description"
#define TEXT_INVALID_ATTACHMENT @"Attachment URL seems to be invalid"


#define EXPAND_IMGNAME @"expand_arrow"
#define DROPDOWN_IMGNAME @"dropdown"
#define CALENDAR_IMGNAME @"calender"
#define ATTACHMENTPIN_IMGNAME @"pin"
#define RIGHTARROW_IMGNAME @"right"

#define EMPTY_TRANSID @"00000000-0000-0000-0000-000000000000"

#endif /* TSConstants_h */