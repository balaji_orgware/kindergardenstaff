//
//  TSEventInboxDetailViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/5/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EventInbox;

@interface TSEventInboxDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *eventTitleLbl;

@property (weak, nonatomic) IBOutlet UILabel *venueLbl;

@property (weak, nonatomic) IBOutlet UILabel *timeLbl;

@property (weak, nonatomic) IBOutlet UIView *topView;

@property (weak, nonatomic) IBOutlet UIButton *attachmentButton;

@property (weak, nonatomic) IBOutlet UILabel *assignedTeacherLabel;

@property (weak, nonatomic) IBOutlet UIView *dateView;

@property (weak, nonatomic) IBOutlet UILabel *fromDateLabel;

@property (weak, nonatomic) IBOutlet UILabel *toDateLabel;

@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

@property (weak, nonatomic) IBOutlet UIView *eventCountView;

@property (weak, nonatomic) IBOutlet UILabel *remainingCountLbl;

@property (weak, nonatomic) IBOutlet UILabel *daysLeftLbl;

@property (weak, nonatomic) EventInbox *eventInboxObj;
@property (weak, nonatomic) IBOutlet UILabel *fromLbl;
@property (weak, nonatomic) IBOutlet UILabel *toLbl;

@end
