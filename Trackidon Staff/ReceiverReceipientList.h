//
//  ReceiverReceipientList.h
//  Trackidon Staff
//
//  Created by Elango on 01/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StaffDetails;

NS_ASSUME_NONNULL_BEGIN

@interface ReceiverReceipientList : NSManagedObject

+ (instancetype)instance;

- (void)saveReceiverReceipientFilterListWithResponseDict:(NSDictionary *)dict menuId:(NSString *)menuId;
@end

NS_ASSUME_NONNULL_END

#import "ReceiverReceipientList+CoreDataProperties.h"
