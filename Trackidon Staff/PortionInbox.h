//
//  PortionInbox.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/21/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StaffDetails;

NS_ASSUME_NONNULL_BEGIN

@interface PortionInbox : NSManagedObject

+ (instancetype)instance;

- (void)saveProtionInboxWithResponseDict:(NSDictionary *)dict;

- (void)savePushNotificationPortionInboxWithDict:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END

#import "PortionInbox+CoreDataProperties.h"
