//
//  TSNewPasswordViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 29/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSNewPasswordViewController.h"

@interface TSNewPasswordViewController ()

@end

@implementation TSNewPasswordViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [TSAnalytics logEvent:ANALYTICS_CHANGEPASSWORD_OPENED parameter:[NSMutableDictionary new]];
    
    if ([_oldPwdTxtField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        
        UIColor *color = [UIColor colorWithRed:253.0/255.0 green:116.0/255.0 blue:118.0/255.0 alpha:1.0f];
        
        _oldPwdTxtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"* Old Password" attributes:@{NSForegroundColorAttributeName: color}];
    }
    
    UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(saveBtnClick:)];
    
    [self.navigationItem setRightBarButtonItem:saveBtn];
    
        
    [TSSingleton layerDrawForView:_oldPwdTxtField position:LAYER_TOP color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:_newpwdTxtField position:LAYER_TOP color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:_confirmPwdTxtField position:LAYER_TOP color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:_confirmPwdTxtField position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    UITapGestureRecognizer *scrollTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollTapped)];
    
    [_scrollView addGestureRecognizer:scrollTap];
    
    if (DEVICE_SIZE.height < 568) {
        
        [_scrollView setContentSize:CGSizeMake(DEVICE_SIZE.width, DEVICE_SIZE.height)];
        
    }

}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_CHANGEPASSWORD parameter:[NSMutableDictionary new]];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Change Password";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
    
    [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
}

-(void)scrollTapped{
    
    [self.view endEditing:YES];
    
    [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
}

#pragma mark - TextField Deleagate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField == _oldPwdTxtField) {
        
        if (DEVICE_SIZE.height < 568) {
            
            [_scrollView setContentOffset:CGPointMake(0, 80) animated:YES];
            
        }
        
    }else if (textField == _newpwdTxtField) {
        
        if (DEVICE_SIZE.height < 568) {
            
            [_scrollView setContentOffset:CGPointMake(0, 140) animated:YES];
            
        }else if (DEVICE_SIZE.height < 667) {
            
            [_scrollView setContentOffset:CGPointMake(0, 70) animated:YES];
        }
        
    }else if (textField == _confirmPwdTxtField) {
        
        if (DEVICE_SIZE.height < 568) {
            
            [_scrollView setContentOffset:CGPointMake(0, 200) animated:YES];
            
        }else if (DEVICE_SIZE.height < 667) {
            
            [_scrollView setContentOffset:CGPointMake(0, 100) animated:YES];
            
        }
        
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == _oldPwdTxtField) {
        
        [_newpwdTxtField becomeFirstResponder];
        
    }else if (textField == _newpwdTxtField){
        
        [_confirmPwdTxtField becomeFirstResponder];
        
    }else if (textField == _confirmPwdTxtField){
        
        [_confirmPwdTxtField resignFirstResponder];
        
        [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        
    }
    
    return YES;
}

#pragma mark - ButtonAction

-(void)saveBtnClick:(id)sender{

    NSLog(@"Save");
    
    if ( _oldPwdTxtField.text.length == 0 || _newpwdTxtField.text.length == 0 || _confirmPwdTxtField.text.length == 0 ) {
        
        [TSSingleton showAlertWithMessage:@"Please enter all fields"];
        
        return;
        
//    }else if (![_oldPwdTxtField.text isEqualToString:[USERDEFAULTS objectForKey:USER_PASSWORD]]){
//        
//        [TSSingleton showAlertWithMessage:@"Old Password doesn't match"];
//        
//        return;
        
    }else if (_newpwdTxtField.text.length < MAX_PASSWORD_LENGTH || _confirmPwdTxtField.text.length < MAX_PASSWORD_LENGTH){
        
        [TSSingleton showAlertWithMessage:@"Password should be minimum 5 characters."];
        
        return;
        
    }else{
        
        if ([_newpwdTxtField.text isEqualToString:_confirmPwdTxtField.text]) {
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"staffTransId"] forKey:@"guidAccountsTransID"];
            
            [param setValue:_oldPwdTxtField.text forKey:@"strOldPassword"];
            
            [param setValue:_newpwdTxtField.text forKey:@"strNewPassword"];
            
            NSLog(@"%@",param);
            
            [self changePwdWithURL:@"MastersMServices/AccountsMService.svc/Changepassword" params:param];
            
        }else{
            
            [TSSingleton showAlertWithMessage:@"New password and confirm Password mismatch"];
            
        }
    }
}

-(void) changePwdWithURL:(NSString *)url params:(NSMutableDictionary *)param{
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:url baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
      
        if (success) {
            
            NSLog(@"%@",responseDictionary);

            if ([responseDictionary[@"ChangepasswordResult"][@"ResponseCode"] boolValue]) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [analyticsDict setValue:responseDictionary[@"ChangepasswordResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                
                [TSAnalytics logEvent:ANALYTICS_CHANGEPASSWORD_CLICKEDUPDATE parameter:[NSMutableDictionary new]];
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"ChangepasswordResult"][@"ResponseMessage"]];
                
                [USERDEFAULTS setObject:_newpwdTxtField.text forKey:USER_PASSWORD];
                
                _oldPwdTxtField.text = @"";
                
                _newpwdTxtField.text = @"";
                
                _confirmPwdTxtField.text = @"";
                
            }else{
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [analyticsDict setValue:responseDictionary[@"ChangepasswordResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                
                [TSAnalytics logEvent:ANALYTICS_CHANGEPASSWORD_CLICKEDUPDATE parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"ChangepasswordResult"][@"ResponseMessage"]];
                
            }

            
        }else{
            NSLog(@"Error");
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
            
            [analyticsDict setValue:ANALYTICS_VALUE_SERVICEFAILURE_RESPONSEMSG forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
            
            [TSAnalytics logEvent:ANALYTICS_CHANGEPASSWORD_CLICKEDUPDATE parameter:analyticsDict];
            
            //[TSSingleton requestTimeOutErrorAlert];
        }
    }];
    
}

@end