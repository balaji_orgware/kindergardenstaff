//
//  School.h
//  Trackidon Staff
//
//  Created by Elango on 18/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Board, Menu, StaffDetails;

NS_ASSUME_NONNULL_BEGIN

@interface School : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "School+CoreDataProperties.h"
