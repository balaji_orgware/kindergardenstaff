//
//  TSTemperatureTodayViewController.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 11/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,Session) {
    Session_FORENOON = 0,
    Session_AFTERNOON
};

@interface TSTemperatureTodayViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,SelectionCallBackDelegate,UIAlertViewDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *selectionView;
@property (weak, nonatomic) IBOutlet UITextField *boardTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *classTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *sessionTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *sectionTxtFld;

@property (weak, nonatomic) IBOutlet UITableView *studentTableView;

@property (weak, nonatomic) IBOutlet UIView *doneButtonView;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@end

@interface TemperatureStudModel : NSObject

@property(nonatomic,strong) NSString *studentName;
@property(nonatomic,strong) NSString *studentTransID;
@property (nonatomic, retain) NSString *temperatureTransId;
@property (nonatomic, retain) NSDate *createdDate;
@property (nonatomic, retain) NSDate *modifiedDate;
@property (nonatomic, retain) NSString *morningTemperature;
@property (nonatomic, retain) NSString *eveningTemperature;
@property (nonatomic, retain) NSDate *takenDate;

@end
