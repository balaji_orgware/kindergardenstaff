//
//  TSFeedBackCreateViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/24/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSFeedBackCreateViewController.h"

@interface TSFeedBackCreateViewController ()
{
    
}
@end

@implementation TSFeedBackCreateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _feedbackDescriptionTxtVw.layer.cornerRadius = 5.0f;
    _feedbackDescriptionTxtVw.layer.masksToBounds = YES;
    
    _feedbackDescriptionTxtVw.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _feedbackDescriptionTxtVw.layer.borderWidth = layerThickness;
    
    [TSSingleton layerDrawForView:_feedbackTypeLbl position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_feedbackTitleLbl position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_parentNameLbl position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_studentNameLbl position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    
    _feedbackTypeLbl.text = _feedbackObj.feedBackType;
    
    _feedbackTitleLbl.text = _feedbackObj.title;
    
    _parentNameLbl.text = _feedbackObj.parentName;
    
    _studentNameLbl.text = _feedbackObj.studentName;
    
    _feedbackDescriptionTxtVw.text = _feedbackObj.feedbackDescription;

}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_FEEDBACKDETAIL parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Feedback Detail";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
