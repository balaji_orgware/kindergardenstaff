//
//  AssignedListViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/28/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Assignments;

@interface AssignedListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) NSArray *assignedListArray;

@property (weak, nonatomic) IBOutlet UIView *listView;

@property (weak, nonatomic) IBOutlet UITableView *assignedTableView;

-(void)showInViewController:(UIViewController *)viewController;

@property (nonatomic,strong) Assignments *assignmentObj;

@property (weak, nonatomic) IBOutlet UILabel *assignedToLbl;

@property (weak, nonatomic)NSString *assignedText;

@end
