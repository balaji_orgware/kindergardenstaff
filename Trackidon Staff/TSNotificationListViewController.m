//
//  TSNotificationListViewController.m
//  Trackidon Staff
//
//  Created by Elango on 22/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSNotificationListViewController.h"

@interface TSNotificationListViewController ()<NSFetchedResultsControllerDelegate,UITableViewDelegate,UITableViewDataSource>

@end

@implementation TSNotificationListViewController
{
    NSArray *attachmentImgArray;
    
    NSManagedObjectContext *managedObjectContext;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];

    managedObjectContext = [TSAppDelegate instance].managedObjectContext;
    
    attachmentImgArray = @[@"one",@"two",@"three",@"four",@"five"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_NOTIFICATIONLIST parameter:[NSMutableDictionary new]];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@ Log",SINGLETON_INSTANCE.barTitle];
    
    [self fetchNotifications];
    
}

#pragma mark - Table view data sources & delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    id  sectionInfo =
    [[_fetchedResultsController sections] objectAtIndex:section];
    
    if ([_fetchedResultsController fetchedObjects].count == 0)
        _notificationTblVw.hidden = YES;
    else
        _notificationTblVw.hidden = NO;
    
    return [sectionInfo numberOfObjects];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Notifications *notification = [_fetchedResultsController objectAtIndexPath:indexPath];
    
    UILabel *titleLbl = (UILabel *)[cell viewWithTag:101];
    
    UILabel *statusLbl = (UILabel *)[cell viewWithTag:102];
    
    UILabel *dateLbl = (UILabel *)[cell viewWithTag:103];
    
    UIImageView *attachmentImgVw = (UIImageView *)[cell viewWithTag:104];
    
    titleLbl.text = notification.notificationTitle;
    
    dateLbl.text = [SINGLETON_INSTANCE stringFromDateWithFormate:DATE_FORMATE_MNTH_DAY_YR withDate:notification.notificationDate];
    
    UIImageView *priorityImgVw = (UIImageView *)[cell viewWithTag:105];
    
    switch ([notification.notificationPriority intValue]) {
            
        case 1:
            
            priorityImgVw.image = [UIImage imageNamed:@"priority_red"];
            
            break;
            
        case 2:
            
            priorityImgVw.image = [UIImage imageNamed:@"priority_yellow"];
            
            break;
            
        case 3:
            
            priorityImgVw.image = [UIImage imageNamed:@"priority_green"];
            
            break;
            
        default:
            break;
    }

    
    switch ([notification.approvedStatus integerValue]) {
            
        case StatusTypeApproved:
            
            statusLbl.text = [NSString stringWithFormat:@"Approved by : %@",notification.approvedByName];
            
            [statusLbl setTextColor:COLOR_ACCEPT_CREATEDTODAY];
            
            break;
            
        case StatusTypePending:
            
            statusLbl.text = [NSString stringWithFormat:@"Pending"];
            
            [statusLbl setTextColor:COLOR_PENDING_VIEWALL];
            
            break;
            
        case StatusTypeDeclined:
            
            statusLbl.text = [NSString stringWithFormat:@"Declined by : %@",notification.approvedByName];
            
            [statusLbl setTextColor:COLOR_DECLINED_DUESTODAY];
            
            break;
            
            
        default:
            break;
    }
    
    if ([notification.notificationIsHasAttachment intValue] == 1) {
        
        NSString *attachmentImgName;
        
        switch (indexPath.row%5) {
                
            case 0:
                attachmentImgName = @"one";
                break;
                
            case 1:
                attachmentImgName = @"two";
                break;
                
            case 2:
                attachmentImgName = @"three";
                break;
                
            case 3:
                attachmentImgName = @"four";
                break;
                
            case 4:
                attachmentImgName = @"five";
                break;
                
            default:
                attachmentImgName = @"one";
                break;
        }
        attachmentImgVw.image = [UIImage imageNamed:attachmentImgName];
        
        [attachmentImgVw setHidden:NO];
    }
    else{
        
        [attachmentImgVw setHidden:YES];
    }
    
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TSNotificationDescriptionViewController *notificationDesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBNotificationDescription"];
    
    Notifications *notificationObj = (Notifications *)[_fetchedResultsController objectAtIndexPath:indexPath];
    
    notificationDesVC.descriptionType = NotificationsString;
    
    notificationDesVC.commonObj = notificationObj;
    
    [self.navigationController pushViewController:notificationDesVC animated:YES];
    
    switch (_listType) {
            
        case LIST_TYPE_OVERALL:
            
            [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_NOTIFICATION_IN_LIST parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_VIEWALL,ANALYTICS_PARAM_LISTTYPE, nil]];
            
            break;
            
        case LIST_TYPE_CREATED_TODAY:
        {
            [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_NOTIFICATION_IN_LIST parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_CREATEDTODAY,ANALYTICS_PARAM_LISTTYPE, nil]];
        }
            break;
            
        case LIST_TYPE_ACCEPTED:
            
            [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_NOTIFICATION_IN_LIST parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_APPROVED,ANALYTICS_PARAM_LISTTYPE, nil]];
            
            break;
            
        case LIST_TYPE_PENDING:
            
            [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_NOTIFICATION_IN_LIST parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_PENDING,ANALYTICS_PARAM_LISTTYPE, nil]];
            
            break;
            
        case LIST_TYPE_DECLINED:
            
            [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_NOTIFICATION_IN_LIST parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_DECLINED,ANALYTICS_PARAM_LISTTYPE, nil]];
            
            break;
            
        default:
            break;
    }

    
}


#pragma mark - Methods

- (void)fetchNotifications{
    
    NSError *error;
    
    if (![[self fetchedResultsController] performFetch:&error]) {
        
        NSLog(@"Error in fetch %@, %@", error, [error userInfo]);
        
    } else {
        
        [_notificationTblVw reloadData];
    }
}

#pragma mark - fetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Notifications" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                              initWithKey:@"modifiedDate" ascending:NO];
    
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setFetchBatchSize:20];
    
    NSPredicate *predicate;
    
    switch (_listType) {
            
        case LIST_TYPE_OVERALL:
            
            predicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@",SINGLETON_INSTANCE.staffTransId];
            
            break;
            
        case LIST_TYPE_CREATED_TODAY:
            
            predicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND notificationDate == %@ AND isForApprovals == 0",SINGLETON_INSTANCE.staffTransId,[SINGLETON_INSTANCE dateFromDate:[NSDate date] withFormate:DATE_FORMATE_SERVICE_RESPONSE]];
            
            break;
            
        case LIST_TYPE_ACCEPTED:
            
            predicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND approvedStatus == 1",SINGLETON_INSTANCE.staffTransId];
            
            break;
            
        case LIST_TYPE_PENDING:
            
            predicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND approvedStatus == 0",SINGLETON_INSTANCE.staffTransId];
            
            break;
            
        case LIST_TYPE_DECLINED:
            
            predicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND approvedStatus == 2",SINGLETON_INSTANCE.staffTransId];
            
            break;
            
        default:
            break;
    }
    
    [fetchRequest setPredicate:predicate];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:managedObjectContext sectionNameKeyPath:nil
                                                   cacheName:nil]; // better to not use cache
    self.fetchedResultsController = theFetchedResultsController;
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
    
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    
    [self.notificationTblVw reloadData];
}


@end
