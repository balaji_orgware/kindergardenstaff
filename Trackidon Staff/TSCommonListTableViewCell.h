//
//  TSCommonListTableViewCell.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 20/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSCommonListTableViewCell : UITableViewCell

-(void) loadContentForCell:(UITableViewCell *)tableViewCell withModuleType:(HealthType)moduleType withModelArray:(NSArray *)modelArray withRow:(NSInteger)row;

@end
