//
//  Events.h
//  Trackidon Staff
//
//  Created by Elango on 19/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Events : NSManagedObject

+ (instancetype)instance;

- (void)saveEventsWithResponseDict:(NSDictionary *)dict andIsForApprovals:(BOOL)isForApprovals;

- (void)savePushNotificationEventsWithResponseDict:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END

#import "Events+CoreDataProperties.h"
