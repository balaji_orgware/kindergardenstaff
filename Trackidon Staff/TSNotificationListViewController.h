//
//  TSNotificationListViewController.h
//  Trackidon Staff
//
//  Created by Elango on 22/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSNotificationListViewController : UIViewController

@property (nonatomic,assign) NSInteger listType;

@property (weak, nonatomic) IBOutlet UITableView *notificationTblVw;

@property (nonatomic,retain) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic,strong) NSArray *notificationArray;


@end
