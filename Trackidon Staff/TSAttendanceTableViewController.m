//
//  TSAttendanceTableViewController.m
//  Trackidon Staff
//
//  Created by ORGware on 25/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSAttendanceTableViewController.h"

@interface TSAttendanceTableViewController (){
    
    TSSelectionViewController *selectionViewVC;
}
@end

@implementation TSAttendanceTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_ATTENDANCEOPENED parameter:[NSMutableDictionary new]];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_ATTENDENCEHOME parameter:[NSMutableDictionary new]];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Attendance";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

#pragma mark - Selection View 

-(void)showSelectionView{
    
    if (selectionViewVC == nil) {
        
        selectionViewVC = [[TSSelectionViewController alloc]initWithNibName:@"TSSelectionViewController" bundle:nil];
        
        selectionViewVC.delegate = self;
        
        NSMutableDictionary *contentDict = [NSMutableDictionary new];
        
        [contentDict setValue:@"" forKey:@"View history by Date"];
        
        [contentDict setValue:@"" forKey:@"View history by Student"];
        
        [selectionViewVC setContentDict:contentDict];
        
        [selectionViewVC showSelectionViewController:self forTextField:nil];
    }
}

#pragma mark - Selection View Callback Delegate methods

-(void)selectedValueFromDict:(NSDictionary *)contentDict withSelectedValue:(NSString *)selectedValue andTransID:(NSString *)transID forTextField:(UITextField *)textField{
    
    if ([selectedValue isEqualToString:@"View history by Student"]) {
        
        NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
        
        [analyticsDict setValue:ANALYTICS_VALUE_STUDENT forKey:ANALYTICS_PARAM_HISTORYTYPE];
        
        [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_CLICKED_HISTORYTYPE parameter:analyticsDict];
        
        TSAttendanceHistoryByStudentViewController *historyByStudentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAttendanceHistoryByStudent"];
        
        [self.navigationController pushViewController:historyByStudentVC animated:YES];
    }
    else{
        
        NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
        
        [analyticsDict setValue:ANALYTICS_VALUE_DATE forKey:ANALYTICS_PARAM_HISTORYTYPE];
        
        [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_CLICKED_HISTORYTYPE parameter:analyticsDict];
        
        TSAttendanceHistoryByDateViewController *historyByDateVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAttendanceHistoryByDate"];
        
        [self.navigationController pushViewController:historyByDateVC animated:YES];
    }
    
    selectionViewVC = nil;
}

-(void)backViewTappedOn:(UIViewController *)viewController{
    
    selectionViewVC = nil;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
    
    UILabel *menuLabel = (UILabel *)[cell viewWithTag:101];
    
    switch ([[NSString stringWithFormat:@"%ld",(long)indexPath.row] integerValue]) {
            
        case 0:
            
            imageView.image = [UIImage imageNamed:@"attendance_today"];
            
            menuLabel.text = @"Attendance Today";
            
            break;
            
        case 1:
            
            imageView.image = [UIImage imageNamed:@"attendance_history"];
            
            menuLabel.text = @"View History";
            
            break;
            
        default:
            break;
    }
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
            
        case 0:
        {
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            [analyticsDict setValue:ANALYTICS_VALUE_TAKEATTENDANCE forKey:ANALYTICS_PARAM_ATTENDANCETYPE];
            
            [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_CLICKED_ATTENDANCETYPE parameter:analyticsDict];
            
            if ([SINGLETON_INSTANCE reachabilityCheck]) {
                
                TSAttendanceTodayViewController *takeAttendanceVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAttendanceToday"];
                
                [self.navigationController pushViewController:takeAttendanceVC animated:YES];
            }
            else{
                
                [TSSingleton showAlertWithMessage:TEXT_NOINTERNET];
            }
            
            break;
        }
        case 1:
        {
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            [analyticsDict setValue:ANALYTICS_VALUE_HISTORY forKey:ANALYTICS_PARAM_ATTENDANCETYPE];
            
            [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_CLICKED_ATTENDANCETYPE parameter:analyticsDict];
            
            if ([SINGLETON_INSTANCE reachabilityCheck]) {
                
                [self showSelectionView];
            }
            else{
                
                [TSSingleton showAlertWithMessage:TEXT_NOINTERNET];
            }
            break;
        }
            
        default:
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 75.0;
}
@end