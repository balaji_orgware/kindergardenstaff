//
//  TSHeightAndWeightTableViewCell.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 20/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSHeightAndWeightTableViewCell : UITableViewCell

-(void) loadContentForCell:(TSHeightAndWeightTableViewCell *)cell withModelArray:(NSArray *)modelArray withRow:(NSInteger)row;

@end
