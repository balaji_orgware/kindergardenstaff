//
//  TSEmergencyCreateViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 18/08/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSEmergencyCreateViewController.h"

#define TEXT_EMEGENCYREMARKS @"Emergency Remarks"

@interface TSEmergencyCreateViewController (){
    
    NSDate *selectedDate;
    CGRect viewRect, currentVisibleRect;
    UIView *currentResponder;
}

@end

@implementation TSEmergencyCreateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    selectedDate = [NSDate date];
    
    _dateLabel.text = [SINGLETON_INSTANCE stringFromDate:selectedDate withFormate:DATE_FORMATE_DAY_MNTH_YR];
    
    [TSSingleton layerDrawForView:_examineDateView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_examinedByView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_emergencyTitleView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
 
    UITapGestureRecognizer *examineDateTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(examineDateViewTapped)];
    [_examineDateView addGestureRecognizer:examineDateTap];
    
    _emergencyRemarksTxtVw.text = TEXT_EMEGENCYREMARKS;
    _emergencyRemarksTxtVw.textColor = APP_PLACEHOLDER_COLOR;
    _emergencyRemarksTxtVw.font = [UIFont fontWithName:APP_FONT size:16];
    _emergencyRemarksTxtVw.textContainerInset = UIEdgeInsetsMake(15,10,10,15); // top, left, bottom, right
    
    [SINGLETON_INSTANCE addDoneButtonForTextView:_emergencyRemarksTxtVw];

    viewRect = self.view.frame;

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationItem.title = @"Emergency";

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button Actions

-(void) examineDateViewTapped{
    
    TSDatePickerViewController *datePickerVC = [[TSDatePickerViewController alloc]initWithNibName:@"TSDatePickerViewController" bundle:nil];
    
    datePickerVC.delegate = self;
    
    [datePickerVC showInViewController:self calendarType:CALENDAR_TYPE_DATE withMinDate:nil withMaxDate:[NSDate date]];
}

- (IBAction)doneButtonTapped:(id)sender {

    if (_examinedByTxtFld.text.length == 0) {
        
        [TSSingleton showAlertWithMessage:@"Please enter Examiner name"];
        return;
    }
    else if (_emergencyTitleTxtFld.text.length == 0){
        
        [TSSingleton showAlertWithMessage:@"Please enter Emergency Title"];
        return;
    }
    else if (_emergencyTitleTxtFld.text.length >150){
        
        [TSSingleton showAlertWithMessage:@"Emergency Title info should be less than 150 characters"];
        return;
    }
    else if (_emergencyRemarksTxtVw.text.length >500){
        
        [TSSingleton showAlertWithMessage:@"Emergency Remarks info should be less than 500 characters"];
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setValue:[_generalInfoDict objectForKey:@"HCTransID"] forKey:@"HCTransID"];
    [dict setValue:SINGLETON_INSTANCE.staffTransId forKey:@"ExaminerID"];
    [dict setValue:_examinedByTxtFld.text forKey:@"ExaminedBy"];
    [dict setValue:[SINGLETON_INSTANCE stringFromDate:selectedDate withFormate:@"dd-MMMM-yyyy"] forKey:@"ExaminedDate"];
    [dict setValue:SINGLETON_INSTANCE.staff.mobileNo forKey:@"Mobileno"];
    [dict setValue:SINGLETON_INSTANCE.staffTransId forKey:@"UserTransID"];
    [dict setValue:@"" forKey:@"AddlnRemarks"];
    [dict setValue:_emergencyTitleTxtFld.text forKey:@"Title"];
    [dict setValue:_emergencyRemarksTxtVw.text forKey:@"EmergencyRemarks"];
    [dict setValue:@"" forKey:@"Remarks"];
    [dict setValue:@"" forKey:@"AddlnRemarks"];
    
    NSDictionary *param = @{@"EmergencyInfo":dict};
    
    [self updateServiceCall:param];
}

#pragma mark - Helper Methods

-(void) keyboardShown:(NSNotification *)notification{
    
    NSDictionary* info = [notification userInfo];
    
    CGSize currentKeyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    CGRect textFldViewFrame;
    
    if (currentResponder == _emergencyTitleTxtFld) {
        
        textFldViewFrame = _emergencyTitleView.frame;
    }
    
    CGPoint textFieldOrigin = CGPointMake(currentResponder.frame.origin.x, currentResponder.frame.origin.y+textFldViewFrame.origin.y);
    textFieldOrigin.y = textFieldOrigin.y + currentResponder.frame.size.height;
    CGFloat textFieldHeight = currentResponder.frame.size.height;
    
    CGRect visibleRect = viewRect;
    visibleRect.size.height -= currentKeyboardSize.height + textFieldHeight;
    currentVisibleRect = visibleRect;
    
    if ([_emergencyRemarksTxtVw isFirstResponder]) {
        
        CGRect descRect = viewRect;
        
        descRect.size.height -= currentKeyboardSize.height;
        
        _emergencyRemarksTxtVw.frame = CGRectMake(_emergencyRemarksTxtVw.frame.origin.x, _emergencyRemarksTxtVw.frame.origin.y, _emergencyRemarksTxtVw.frame.size.width, descRect.size.height - 64 );
        
        _scrollView.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, _emergencyRemarksTxtVw.frame.origin.y+_emergencyRemarksTxtVw.frame.size.height);
        
        CGPoint scrollPoint = CGPointMake(0.0, _emergencyRemarksTxtVw.frame.origin.y);
        
        [_scrollView setContentOffset:scrollPoint animated:YES];
        
        [_scrollView setScrollEnabled:NO];
    }
    else if (!CGRectContainsPoint(visibleRect, textFieldOrigin)){
        
        CGPoint scrollPoint = CGPointMake(0.0, textFieldOrigin.y - visibleRect.size.height  + textFieldHeight);
        
        [_scrollView setContentOffset:scrollPoint animated:YES];
    }
}

-(void) keyboardHide:(NSNotification *)notification{
    
    [_scrollView setScrollEnabled:YES];
    
    [_scrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - Date Picker Delegate

-(void)selectedDate:(NSDate *)date{
    
    selectedDate = date;
    
    _dateLabel.text = [SINGLETON_INSTANCE stringFromDate:selectedDate withFormate:DATE_FORMATE_DAY_MNTH_YR];
}

#pragma mark - TextField Delegates

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    currentResponder = textField;
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - TextView Delegates

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    currentResponder = textView;
    
    if ([textView.text isEqualToString:TEXT_EMEGENCYREMARKS]) {
        
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
        textView.font = [UIFont fontWithName:APP_FONT size:14];
    }
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {
        
        textView.text = TEXT_EMEGENCYREMARKS;
        textView.textColor = APP_PLACEHOLDER_COLOR;
        textView.font = [UIFont fontWithName:APP_FONT size:17];
    }
    
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    
    if (newFrame.size.height >50) {
        textView.frame = newFrame;
    }
    else{
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), 50);
        textView.frame = newFrame;
    }
    
    [_scrollView setFrame:CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, DeviceHeight - _doneView.frame.size.height - 64)];
    
    [_scrollView setContentSize:CGSizeMake(DeviceWidth,_emergencyRemarksTxtVw.frame.origin.y+_emergencyRemarksTxtVw.frame.size.height)];
    
    [_scrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - Service Call

-(void) updateServiceCall : (NSDictionary *)param{
    
    NSString *urlString = @"HealthDetails/Services/HealthCardService.svc/UpdateHealthCardInfo";
    
    NSDictionary *params = @{@"ObjUpdateHealthCard":param};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"UpdateHealthCardInfoResult"][@"ResponseCode"] integerValue] == 1) {
                
                SINGLETON_INSTANCE.needRefreshHealthRecords = YES;
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"UpdateHealthCardInfoResult"][@"ResponseMessage"]];
                
                NSArray *viewControllers = [self.navigationController viewControllers];
                
                NSUInteger currentIndex = [viewControllers indexOfObject:self];
                
                [self.navigationController popToViewController:[viewControllers objectAtIndex:currentIndex-2] animated:YES];
            }
            else{
                SINGLETON_INSTANCE.needRefreshHealthRecords = NO;
            }
        }
        else{
            SINGLETON_INSTANCE.needRefreshHealthRecords = NO;
        }
    }];
}

@end