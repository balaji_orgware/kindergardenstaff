//
//  Portions+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 28/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Portions+CoreDataProperties.h"

@implementation Portions (CoreDataProperties)

@dynamic academicTransID;
@dynamic academicYear;
@dynamic approvedByName;
@dynamic approvedByTransID;
@dynamic approvedDate;
@dynamic approvedStatus;
@dynamic assignedList;
@dynamic attachment;
@dynamic boardName;
@dynamic boardTransID;
@dynamic createdByName;
@dynamic isAttachment;
@dynamic isForApprovals;
@dynamic modifiedByName;
@dynamic modifiedDate;
@dynamic portionDate;
@dynamic portionDescription;
@dynamic portionTitle;
@dynamic priority;
@dynamic schoolName;
@dynamic schoolTransID;
@dynamic staffTransID;
@dynamic subjectTitle;
@dynamic transID;
@dynamic staff;

@end
