//
//  Holidays+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 23/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Holidays.h"

NS_ASSUME_NONNULL_BEGIN

@interface Holidays (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *academicTransID;
@property (nullable, nonatomic, retain) NSString *approvedByTransID;
@property (nullable, nonatomic, retain) NSString *approvedByName;
@property (nullable, nonatomic, retain) NSString *approvedByDate;
@property (nullable, nonatomic, retain) NSString *approvedStatus;
@property (nullable, nonatomic, retain) NSString *attachment;
@property (nullable, nonatomic, retain) NSString *dateTimeModified;
@property (nullable, nonatomic, retain) NSString *holidayDescription;
@property (nullable, nonatomic, retain) NSString *classSection;
@property (nullable, nonatomic, retain) NSString *holidayTime;
@property (nullable, nonatomic, retain) NSString *holidayTitle;
@property (nullable, nonatomic, retain) NSString *holidayTypeID;
@property (nullable, nonatomic, retain) NSString *fromDate;
@property (nullable, nonatomic, retain) NSString *toDate;
@property (nullable, nonatomic, retain) NSNumber *isAttachment;
@property (nullable, nonatomic, retain) NSString *schoolTransID;
@property (nullable, nonatomic, retain) NSString *userCreatedName;
@property (nullable, nonatomic, retain) NSString *userModifiedName;
@property (nullable, nonatomic, retain) NSString *venue;
@property (nullable, nonatomic, retain) NSString *userCreatedTransID;
@property (nullable, nonatomic, retain) NSString *userModifiedTransID;
@property (nullable, nonatomic, retain) NSString *userTransID;
@property (nullable, nonatomic, retain) NSString *transID;
@property (nullable, nonatomic, retain) NSString *staffTransID;
@property (nullable, nonatomic, retain) StaffDetails *staff;

@end

NS_ASSUME_NONNULL_END
