//
//  BoardList+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 13/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BoardList+CoreDataProperties.h"

@implementation BoardList (CoreDataProperties)

@dynamic boardTransId;
@dynamic schoolTransId;
@dynamic staff;

@end
