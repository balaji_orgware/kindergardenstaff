//
//  ReceiverReceipientList.m
//  Trackidon Staff
//
//  Created by Elango on 01/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "ReceiverReceipientList.h"
#import "StaffDetails.h"

@implementation ReceiverReceipientList

+ (instancetype)instance{
    
    static ReceiverReceipientList *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (&onceToken, ^{
        
        kSharedInstance = [[ReceiverReceipientList alloc]init];
        
    });
    
    return kSharedInstance;
}

- (void)saveReceiverReceipientFilterListWithResponseDict:(NSDictionary *)dict menuId:(NSString *)menuId
{
    //NSLog(@"%@",dict);
    
    @try {
        
        NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
        
        [SINGLETON_INSTANCE deleteAllObjectsInEntity:@"ReceiverReceipientList"];
        
        for (NSDictionary *tmpReceipientDict in dict[@"FetchMenuNameByIDResult"][@"ReceipientTypeDetails"]) {
            
            NSDictionary *receipientDict = [tmpReceipientDict dictionaryByReplacingNullsWithBlanks];
            
            ReceiverReceipientList *receiverReceipient;
            
            receiverReceipient = [NSEntityDescription insertNewObjectForEntityForName:@"ReceiverReceipientList" inManagedObjectContext:context];
            
            receiverReceipient.receipientDescription = receipientDict[@"value"];
            receiverReceipient.recepientTypeId = [NSString stringWithFormat:@"%@",receipientDict[@"key"]];
            receiverReceipient.menuId = [NSString stringWithFormat:@"%@",menuId];
            receiverReceipient.isSelected = [NSNumber numberWithBool:NO];
            receiverReceipient.staff = SINGLETON_INSTANCE.staff;
            
        }
        
        NSError *error;
        
        if (![context save:&error]) {
            
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            
        }
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    }
}

@end
