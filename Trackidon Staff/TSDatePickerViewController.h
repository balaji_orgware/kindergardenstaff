//
//  TSDatePickerViewController.h
//  Trackidon Staff
//
//  Created by Elango on 24/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol datePickerDelegate;

typedef NS_ENUM(NSInteger, CALENDAR_TYPE){
    
    CALENDAR_TYPE_DATE,
    CALENDAR_TYPE_TIME,
    
};

@interface TSDatePickerViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIView *datePickerVw;
@property (weak, nonatomic) id<datePickerDelegate>delegate;

- (void)showInViewController:(UIViewController *)viewController calendarType:(NSInteger)calendarType withMinDate:(NSDate *)minDate withMaxDate:(NSDate *)maxDate;

@end

@protocol datePickerDelegate <NSObject>

@required
- (void)selectedDate:(NSDate *)date;

@end