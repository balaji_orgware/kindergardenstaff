//
//  Section+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 04/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Section+CoreDataProperties.h"

@implementation Section (CoreDataProperties)

@dynamic boardTransId;
@dynamic classStandardName;
@dynamic classTransId;
@dynamic schoolName;
@dynamic schoolTransId;
@dynamic sectionName;
@dynamic sectionTransId;
@dynamic boardName;
@dynamic classes;

@end
