//
//  TSAttendanceHistoryByDateViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 31/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSDatePickerViewController.h"

@interface TSAttendanceHistoryByDateViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,SelectionCallBackDelegate,datePickerDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UIView *dateSelectionView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayNameLabel;

@property (weak, nonatomic) IBOutlet UIView *classSecSelectionView;
@property (weak, nonatomic) IBOutlet UITextField *selectClassTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *selectBoardTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *selectSectionTxtFld;

@property (weak, nonatomic) IBOutlet UIView *totalNoOfStudentsView;
@property (weak, nonatomic) IBOutlet UILabel *studentsCountLabel;

@property (weak, nonatomic) IBOutlet UITableView *studentsTableView;

@property (strong, nonatomic) UIView *presentView;
@property (strong, nonatomic) UIView *absentView;
@property (strong, nonatomic) UIView *lateView;
@property (strong, nonatomic) UIImageView *presentImgView;
@property (strong, nonatomic) UIImageView *absentImgView;
@property (strong, nonatomic) UIImageView *lateImgView;
@property (strong, nonatomic) UILabel *presentCountLabel;
@property (strong, nonatomic) UILabel *absentCountLabel;
@property (strong, nonatomic) UILabel *lateCountLabel;

@end