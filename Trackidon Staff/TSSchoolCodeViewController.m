//
//  TPSchoolCodeViewController.m
//  Trackidon
//
//  Created by ORGMacMini2 on 3/10/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSSchoolCodeViewController.h"

@interface TSSchoolCodeViewController ()

@end

@implementation TSSchoolCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    _schoolCodeTxtFld.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    
    //_schoolCodeTxtFld.text = @"STJ123";
    
    _schoolCodeTxtFld.layer.cornerRadius = _schoolCodeTxtFld.frame.size.height/2;
    
    //**UI changes for 4s
    //    if ([UIScreen mainScreen].bounds.size.height < 568) {
    //
    //        _inputFieldsVw.frame = CGRectMake(_inputFieldsVw.frame.origin.x,40, _inputFieldsVw.frame.size.width, _inputFieldsVw.frame.size.height);
    //
    //        _logoVw.frame = CGRectMake(_logoVw.frame.origin.x, _inputFieldsVw.frame.size.height+45, _logoVw.frame.size.width,[UIScreen mainScreen].bounds.size.height - _inputFieldsVw.frame.size.height-45);
    //
    //        _goBtn.frame = CGRectMake((_logoVw.frame.size.width/2) -30, _logoVw.frame.origin.y - 30, 60, 60);
    //    }
    
    if (! IS_IPHONE_4) {
        _goBtn.center = CGPointMake(_redView.center.x, _redView.frame.origin.y);
        
    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_LOGINSCHOOLCODE parameter:[NSMutableDictionary new]];
    
    self.navigationController.navigationBarHidden = YES;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
    
}

#pragma mark - TextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 6;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == _schoolCodeTxtFld) {
        
        [_schoolCodeTxtFld resignFirstResponder];
        
    }
    return YES;
}


#pragma mark - ButtonAction

- (IBAction)goBtnClk:(id)sender {
    
    if ([_schoolCodeTxtFld.text length] > 0) {
        
        [SINGLETON_INSTANCE deleteAllObjectsInEntity:@"Trust"];
        
        NSMutableDictionary *param = [NSMutableDictionary dictionary];
        
        [param setValue:_schoolCodeTxtFld.text forKey:@"strSchoolCode"];
        
        [self fetchSchoolUrlBySchoolCodeWithUrl:@"MastersMServices/CommonSchoolsService.svc/FetchSchoolsBySchoolCode" params:param];
        
    }else {
        
        [SINGLETON_INSTANCE showAlertMessageWithTitle:INCORRECT_SCHOOL_CODE_TITLE andMessage:INCORRECT_SCHOOL_CODE_MESSAGE];
        
    }
    
}

#pragma mark - ServiceCall

-(void)fetchSchoolUrlBySchoolCodeWithUrl:(NSString *)Url params:(NSMutableDictionary *)param{
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:Url baseURL:SCHOOL_CODE_BASE_URL param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchSchoolsBySchoolCodeResult"][@"ResponseCode"]boolValue]) {
                
                [TSAnalytics logEvent:ANALYTICS_LOGIN_SUBMIT_SCHOOL_CODE parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_SUCCESS,ANALYTICS_PARAM_RESPONSE,_schoolCodeTxtFld.text,ANALYTICS_PARAM_SCHOOLCODE,responseDictionary[@"FetchSchoolsBySchoolCodeResult"][@"ResponseMessage"],ANALYTICS_PARAM_RESPONSEMESSAGE, nil]];
                
                [[Trust instance]saveTrustWithResponseDict:responseDictionary];
                
                [[NSUserDefaults standardUserDefaults] setObject:responseDictionary[@"FetchSchoolsBySchoolCodeResult"][@"School"][@"MobileBaseUrl"] forKey:BASE_URL];
                
                //                [[NSUserDefaults standardUserDefaults] setObject:@"http://103.230.85.92/TrackidonTestingAPI" forKey:BaseURL];
                
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSLog(@"%@",responseDictionary);
                
                TSLoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBLogin"];
                
                [self.navigationController pushViewController:loginVC animated:YES];
            }
            else{
                
                [TSAnalytics logEvent:ANALYTICS_LOGIN_SUBMIT_SCHOOL_CODE parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_FAILURE,ANALYTICS_PARAM_RESPONSE,_schoolCodeTxtFld.text,ANALYTICS_PARAM_SCHOOLCODE,responseDictionary[@"FetchSchoolsBySchoolCodeResult"][@"ResponseMessage"],ANALYTICS_PARAM_RESPONSEMESSAGE, nil]];
                
                [SINGLETON_INSTANCE showAlertMessageWithTitle:INCORRECT_SCHOOL_CODE_TITLE andMessage:INCORRECT_SCHOOL_CODE_MESSAGE];
            }
        }
        else{
            
            [TSAnalytics logEvent:ANALYTICS_LOGIN_SUBMIT_SCHOOL_CODE parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_FAILURE,ANALYTICS_PARAM_RESPONSE,_schoolCodeTxtFld.text,ANALYTICS_PARAM_SCHOOLCODE,ANALYTICS_VALUE_SERVICEFAILURE_RESPONSEMSG,ANALYTICS_PARAM_RESPONSEMESSAGE, nil]];
            
            //[SINGLETON_INSTANCE showAlertMessageWithTitle:@"Information" andMessage:@"Something went wrong. Please try again"];
        }
    }];
    
}

@end
