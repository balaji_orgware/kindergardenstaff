//
//  TSCreateHeightAndWeightViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 25/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSCreateHeightAndWeightViewController.h"

@interface TSCreateHeightAndWeightViewController (){
    
    CGRect viewRect, currentVisibleRect;
    UIView *currentResponder;
}

@end

@implementation TSCreateHeightAndWeightViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [TSSingleton layerDrawForView:_heightView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_weightView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
//    [TSSingleton layerDrawForView:_descriptionTxtVw position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    [SINGLETON_INSTANCE addDoneButtonForTextField:_heightTxtFld];
    [SINGLETON_INSTANCE addDoneButtonForTextField:_weightTxtFld];
    [SINGLETON_INSTANCE addDoneButtonForTextView:_descriptionTxtVw];
    
    _descriptionTxtVw.text = DESCRIPTION_TEXT;
    _descriptionTxtVw.textColor = APP_PLACEHOLDER_COLOR;
    _descriptionTxtVw.font = [UIFont fontWithName:APP_FONT size:17];
    _descriptionTxtVw.textContainerInset =
    UIEdgeInsetsMake(15,5,10,15); // top, left, bottom, right
    
    viewRect = self.view.frame;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationItem.title = @"Height and weight";
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardShown:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Helper Methods

-(void) keyboardShown:(NSNotification *)notification{
    
    NSDictionary* info = [notification userInfo];
    
    CGSize currentKeyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    CGPoint textFieldOrigin = currentResponder.frame.origin;
    textFieldOrigin.y = textFieldOrigin.y + currentResponder.frame.size.height;
    CGFloat textFieldHeight = currentResponder.frame.size.height;
    
    CGRect visibleRect = viewRect;
    visibleRect.size.height -= currentKeyboardSize.height + textFieldHeight;
    currentVisibleRect = visibleRect;
    
    if ([_descriptionTxtVw isFirstResponder]) {
        
        CGRect descRect = viewRect;
        
        descRect.size.height -= currentKeyboardSize.height;
        
        _descriptionTxtVw.frame = CGRectMake(_descriptionTxtVw.frame.origin.x, _descriptionTxtVw.frame.origin.y, _descriptionTxtVw.frame.size.width, descRect.size.height - 64 );
                
        CGPoint scrollPoint = CGPointMake(0.0, _descriptionTxtVw.frame.origin.y);
        
        [_scrollView setContentOffset:scrollPoint animated:YES];
        
        [_scrollView setScrollEnabled:NO];
        
    }
    else if (!CGRectContainsPoint(visibleRect, textFieldOrigin))
    {
        CGPoint scrollPoint = CGPointMake(0.0, textFieldOrigin.y - visibleRect.size.height  + textFieldHeight);
        
        [_scrollView setContentOffset:scrollPoint animated:YES];
    }
}

-(void) keyboardHide:(NSNotification *)notification{
    
    [_scrollView setScrollEnabled:YES];
    
    [_scrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - TextField Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    currentResponder = textField;
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - TextView Delegate

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    currentResponder = textView;
    
    if ([textView.text isEqualToString:DESCRIPTION_TEXT]) {
        
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
        textView.font = [UIFont fontWithName:APP_FONT size:14];
    }
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {
        
        textView.text = DESCRIPTION_TEXT;
        textView.textColor = [UIColor lightGrayColor];
        textView.font = [UIFont fontWithName:APP_FONT size:17];
    }
    
    CGFloat fixedWidth = _descriptionTxtVw.frame.size.width;
    CGSize newSize = [_descriptionTxtVw sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = _descriptionTxtVw.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    
    if (newFrame.size.height >50) {
        _descriptionTxtVw.frame = newFrame;
    }
    else{
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), 50);
        _descriptionTxtVw.frame = newFrame;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_scrollView setContentSize:CGSizeMake(DeviceWidth,_descriptionTxtVw.frame.origin.y+_descriptionTxtVw.frame.size.height)];
    });
}

#pragma mark - AlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 1000) {
        
        if (buttonIndex) {
         
            if (_heightTxtFld.text.length == 0 || [_heightTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
                
                [TSSingleton showAlertWithMessage:@"The Height value you've entered seems wrong. Please enter valid Height"];
                return;
            }
            else if (_weightTxtFld.text.length == 0 || [_weightTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
                
                [TSSingleton showAlertWithMessage:@"The Weight value you've entered seems wrong. Please enter valid Weight"];
                return;
            }
            else if (_heightTxtFld.text.length >4 || _weightTxtFld.text.length >4){
                
                [TSSingleton showAlertWithMessage:@"Height/Weight entered value seems too high. Please enter correct value"];
                return;
            }
            
            
            // kg/m2  = BMI formula
            
            float heightInMetre = [_heightTxtFld.text floatValue]/100;
            float bmi = [_weightTxtFld.text floatValue]/(heightInMetre*heightInMetre);
            
            NSString *description = [_descriptionTxtVw.text isEqualToString:DESCRIPTION_TEXT] ? @"" : _descriptionTxtVw.text;
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setValue:EMPTY_TRANSID forKey:@"TransID"];
            [dict setValue:[_generalInfoDict valueForKey:@"HCTransID"] forKey:@"HCTransID"];
            [dict setValue:_heightTxtFld.text forKey:@"Height"];
            [dict setValue:_weightTxtFld.text forKey:@"Weight"];
            [dict setValue:[NSString stringWithFormat:@"%f",bmi] forKey:@"BMI"];
            [dict setValue:SINGLETON_INSTANCE.staffTransId forKey:@"ExaminerID"];
            [dict setValue:SINGLETON_INSTANCE.staff.name forKey:@"ExaminedBy"];
            [dict setValue:[SINGLETON_INSTANCE stringFromDate:[NSDate date] withFormate:@"dd-MMMM-yyyy"] forKey:@"ExaminedDate"];
            [dict setValue:SINGLETON_INSTANCE.staff.mobileNo forKey:@"Mobileno"];
            //    [dict setValue:SINGLETON_INSTANCE.staff.addressOne forKey:@"Address"];
            //    [dict setValue:SINGLETON_INSTANCE.staff.addressThree forKey:@"Place"];
            [dict setValue:description forKey:@"Remarks"];
            [dict setValue:description forKey:@"AddlnRemarks"];
            [dict setValue:SINGLETON_INSTANCE.staffTransId forKey:@"UserTransID"];
            
            NSDictionary *params = @{@"BMIInfo":dict};
            
            [self updateServiceCall:params];
        }
        else{
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        }
    }
}

#pragma mark - Button actions

- (IBAction)doneButtonTapped:(id)sender {
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Are you sure want to update ?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alertView.tag = 1000;
    
    [alertView show];
}

#pragma mark - Service Call

-(void) updateServiceCall : (NSDictionary *)param{
    
    NSString *urlString = @"HealthDetails/Services/HealthCardService.svc/UpdateHealthCardInfo";
    
    NSDictionary *params = @{@"ObjUpdateHealthCard":param};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"UpdateHealthCardInfoResult"][@"ResponseCode"] integerValue] == 1) {
                
                SINGLETON_INSTANCE.needRefreshHealthRecords = YES;
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"UpdateHealthCardInfoResult"][@"ResponseMessage"]];
                
                NSArray *viewControllers = [self.navigationController viewControllers];
                
                NSUInteger currentIndex = [viewControllers indexOfObject:self];
                
                [self.navigationController popToViewController:[viewControllers objectAtIndex:currentIndex-2] animated:YES];
            }
            else{
                SINGLETON_INSTANCE.needRefreshHealthRecords = NO;
            }
        }
        else{
            SINGLETON_INSTANCE.needRefreshHealthRecords = NO;
        }
    }];
}

@end
