//
//  TSEyeEarDescriptionViewController.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 20/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSEyeEarDescriptionViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *backView;

@property (weak, nonatomic) IBOutlet UIView *descView;

@property (weak, nonatomic) IBOutlet UIView *dateView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *examinedByNameLbl;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *leftEyeTitleLbl;
@property (weak, nonatomic) IBOutlet UITextView *leftEyeTxtVw;
@property (weak, nonatomic) IBOutlet UILabel *rightEyeTitleLbl;
@property (weak, nonatomic) IBOutlet UITextView *rightEyeTxtVw;

-(void) showDescViewController:(UIViewController *)viewController withModuleType:(NSInteger)moduleType andModelObj:(id)modelObj;

@end
