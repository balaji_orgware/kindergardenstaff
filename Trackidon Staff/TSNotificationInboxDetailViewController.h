//
//  TSNotificationInboxDetailViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/4/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NotificationInbox;

@interface TSNotificationInboxDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topView;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@property (weak, nonatomic) IBOutlet UIButton *attachmentButton;

@property (weak, nonatomic) IBOutlet UILabel *createdByLbl;

@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (weak, nonatomic) IBOutlet UIView *dateView;

@property (weak, nonatomic) IBOutlet UILabel *notificationPriorityTextLbl;

@property (weak, nonatomic) IBOutlet UIView *notificationPriorityView;

@property (weak, nonatomic) IBOutlet UILabel *fromDateLbl;

@property(nonatomic,assign) NotificationInbox *notificationInboxObj;

@property (weak, nonatomic)NSMutableArray *notificationDetailsArray;

@end
