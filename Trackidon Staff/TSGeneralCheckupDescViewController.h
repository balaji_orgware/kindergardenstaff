//
//  TSGeneralCheckupDescViewController.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 21/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSGeneralCheckupDescViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIView *descView;
@property (weak, nonatomic) IBOutlet UIView *dateView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *examinedByNameLbl;

@property (weak, nonatomic) IBOutlet UIView *topView;


@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@property (weak, nonatomic) IBOutlet UIButton *closeButton;

- (IBAction)closeButtonAction:(id)sender;


-(void) showDescViewController:(UIViewController *)viewController withModelObj:(GeneralCheckupModel *)modelObj;

@end
