//
//  TSEventsHomeViewController.m
//  Trackidon Staff
//
//  Created by Elango on 19/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSEventsHomeViewController.h"

@interface TSEventsHomeViewController ()<UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate>{
    
    NSString *title;
}

@end

@implementation TSEventsHomeViewController
{
    NSArray *eventsArr;
    
    int createdTodayCount;
    
    int approvedCount;
    
    int declinedCount;
    
    int pendingCount;
    
}

- (void)viewDidLoad {

    [super viewDidLoad];
    
    [self fetchEvents];
    
    [self fetchEventsServiceCall];
    
    title = SINGLETON_INSTANCE.barTitle;
    
    [TSAnalytics logEvent:ANALYTICS_EVENT_EVENTOPENED parameter:[NSMutableDictionary new]];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_EVENTHOME parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = title;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Tableview Datasource & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    UIImageView *placeHolderImgVw = (UIImageView *)[cell viewWithTag:100];
    
    UILabel *subMenuLabel = (UILabel *)[cell viewWithTag:101];
    
    UILabel *countLabel = (UILabel *)[cell viewWithTag:102];
    
    NSInteger row = [[NSString stringWithFormat:@"%ld",(long)indexPath.row] integerValue];
    
    switch (row) {
            
        case 0:
            
            placeHolderImgVw.image = [UIImage imageNamed:@"all_events"];
            subMenuLabel.text = @"View All";
            subMenuLabel.font = [UIFont fontWithName:APP_FONT size:18];
            countLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)eventsArr.count];
            countLabel.textColor = [UIColor colorWithRed:64.0/255.0 green:196.0/255.0 blue:231.0/255.0 alpha:1.0];
            break;
            
        case 1:
            
            placeHolderImgVw.image = [UIImage imageNamed:@"created_today"];
            subMenuLabel.text = @"Created Today";
            subMenuLabel.font = [UIFont fontWithName:APP_FONT size:16];
            countLabel.text = [NSString stringWithFormat:@"%d",createdTodayCount];
            countLabel.textColor = COLOR_ACCEPT_CREATEDTODAY;
            break;
            
        default:
            break;
    }
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SINGLETON_INSTANCE.barTitle = title;
    
    if (indexPath.row == 0) {
        
        TSEventsViewController *eventsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBEvents"];
        
        eventsVC.listType = LIST_TYPE_OVERALL;
        
        [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_VIEW_EVENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_VIEWALL,ANALYTICS_PARAM_VIEWTYPE, nil]];
        
        if(eventsArr.count >0)
            [self.navigationController pushViewController:eventsVC animated:YES];
        
    }else {
        
        TSEventsViewController *eventsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBEvents"];
        
        eventsVC.listType = LIST_TYPE_CREATED_TODAY;
        
        [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_VIEW_EVENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_CREATEDTODAY,ANALYTICS_PARAM_VIEWTYPE, nil]];
        
        if(createdTodayCount >0)
            [self.navigationController pushViewController:eventsVC animated:YES];
        
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 75;
}

#pragma mark - Collection View Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UIImageView *btnImg = (UIImageView *)[cell viewWithTag:101];
    
    UILabel *btnLbl = (UILabel *)[cell viewWithTag:102];
    
    UILabel *countLbl = (UILabel *)[cell viewWithTag:103];
    
    [TSSingleton layerDrawForView:cell.contentView position:LAYER_LEFT color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:cell.contentView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    //cell.contentView.layer.borderWidth = 0.5;
    //cell.contentView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    switch (indexPath.row) {
            
        case LIST_TYPE_ACCEPTED:
            
            btnImg.image = [UIImage imageNamed:@"approved"];
            btnLbl.text = @"Approved";
            countLbl.text= [NSString stringWithFormat:@"%lu",(unsigned long)approvedCount];
            [countLbl setTextColor:COLOR_ACCEPT_CREATEDTODAY];
            
            break;
            
        case LIST_TYPE_PENDING:
            
            btnImg.image = [UIImage imageNamed:@"pending"];
            btnLbl.text = @"Pending";
            countLbl.text= [NSString stringWithFormat:@"%lu",(unsigned long)pendingCount];
            [countLbl setTextColor:COLOR_PENDING_VIEWALL];
            
            break;
            
        case LIST_TYPE_DECLINED:
            
            btnImg.image = [UIImage imageNamed:@"declined"];
            btnLbl.text = @"Declined";
            countLbl.text= [NSString stringWithFormat:@"%lu",(unsigned long)declinedCount];
            [countLbl setTextColor:COLOR_DECLINED_DUESTODAY];
            
            break;

            
        default:
            break;
    }
    
    
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
            
        case LIST_TYPE_ACCEPTED:
        {
            
            TSEventsViewController *eventsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBEvents"];
            
            eventsVC.listType = LIST_TYPE_ACCEPTED;
            
            [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_VIEW_EVENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_APPROVED,ANALYTICS_PARAM_VIEWTYPE, nil]];
            
            if(approvedCount >0)
                [self.navigationController pushViewController:eventsVC animated:YES];
            
            SINGLETON_INSTANCE.barTitle = TEXT_APPROVED;
            
            break;
        }
        case LIST_TYPE_PENDING:
        {
            TSEventsViewController *eventsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBEvents"];
            
            eventsVC.listType = LIST_TYPE_PENDING;
            
            [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_VIEW_EVENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_PENDING,ANALYTICS_PARAM_VIEWTYPE, nil]];
            
            if(pendingCount >0)
                [self.navigationController pushViewController:eventsVC animated:YES];
            
            SINGLETON_INSTANCE.barTitle = TEXT_PENDING;
            
            break;
        }
        case LIST_TYPE_DECLINED:
        {
            TSEventsViewController *eventsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBEvents"];
            
            eventsVC.listType = LIST_TYPE_DECLINED;
            
            [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_VIEW_EVENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_DECLINED,ANALYTICS_PARAM_VIEWTYPE, nil]];
            
            if(declinedCount >0)
                [self.navigationController pushViewController:eventsVC animated:YES];
            
            SINGLETON_INSTANCE.barTitle = TEXT_DECLINED;
            
            break;
        }
            
        default:
            break;
    }
    
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float width = DeviceWidth/3;
    float height = 130;//width*1.214;
    
    return CGSizeMake(width, height);
    
}

#pragma mark - Methods

- (void)fetchEvents{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND isForApprovals == 0",SINGLETON_INSTANCE.staffTransId];
    
    eventsArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Events" withPredicate:predicate];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self setEventCounts];
        
    });
}

- (void)setEventCounts{
    
    NSPredicate *pendingPredicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND approvedStatus == 0 AND isForApprovals == 0",SINGLETON_INSTANCE.staffTransId];
    
    NSPredicate *approvedPredicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND approvedStatus == 1 AND isForApprovals == 0",SINGLETON_INSTANCE.staffTransId];
    
     NSPredicate *declinedPredicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND approvedStatus == 2 AND isForApprovals == 0",SINGLETON_INSTANCE.staffTransId];
    
    NSString *startDateStr = [[SINGLETON_INSTANCE stringFromDate:[NSDate date] withFormate:@"dd-MMM-yyyy"] stringByAppendingString:@" 00:01"];
    
    NSString *endDateStr = [[SINGLETON_INSTANCE stringFromDate:[NSDate date] withFormate:@"dd-MMM-yyyy"] stringByAppendingString:@" 23:59"];
    
    NSPredicate *createdTdyPredicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND modifiedDate >= %@ AND modifiedDate <= %@ AND isForApprovals == 0",SINGLETON_INSTANCE.staffTransId,[SINGLETON_INSTANCE dateFromString:startDateStr withFormate:DATE_FORMATE_MODIFIED_DATE], [SINGLETON_INSTANCE dateFromString:endDateStr withFormate:DATE_FORMATE_MODIFIED_DATE]];
    
    pendingCount = (int)[eventsArr filteredArrayUsingPredicate:pendingPredicate].count;
    
    approvedCount = (int)[eventsArr filteredArrayUsingPredicate:approvedPredicate].count;
    
    declinedCount = (int)[eventsArr filteredArrayUsingPredicate:declinedPredicate].count;
    
    createdTodayCount = (int)[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Events" withPredicate:createdTdyPredicate].count;
    
    [_tblVw reloadData];
    
    [_collectionVw reloadData];

}
#pragma mark - Button Action

- (IBAction)onCreateEvent:(id)sender {
    
    [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_CREATE_INHOME parameter:[NSMutableDictionary new]];
    
    if ([SINGLETON_INSTANCE checkForActiveInternet]){
        
        [SINGLETON_INSTANCE fetchFilterAndReceipientDetailsWithMenuId:SINGLETON_INSTANCE.currentMenuId showIndicator:YES onCompletion:nil];
        
        TSCreateEventViewController *createEventVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBCreateEvent"];
        
        [self.navigationController pushViewController:createEventVC animated:YES];
        
    }else{
        
        [TSSingleton showAlertWithMessage:TEXT_NOINTERNET];
    }
    
    
}


#pragma mark - ServiceCall

- (void)fetchEventsServiceCall{
    
    NSString *url = @"MastersMServices/EventsMService.svc/FetchEventsMDetailsSelectByStaffTransID";
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    [param setObject:SINGLETON_INSTANCE.staffTransId forKey:@"guidStaffTransID"];
    
    [param setObject:@"26" forKey:@"intEventTypeID"];
    
    BOOL showIndicator = NO;
    
    if (eventsArr.count  == 0) {
        showIndicator = YES;
    }
    
    [[TSHandlerClass sharedHandler] postRequestWithURL:url baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:param currentView:self.view showIndicator:showIndicator onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            if([responseDictionary[@"FetchEventsMDetailsSelectByStaffTransIDResult"][@"ResponseCode"] integerValue] == 1) {
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                    [[Events instance] saveEventsWithResponseDict:responseDictionary andIsForApprovals:NO];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self fetchEvents];
                        
                    });

                });
                
            }
            
        }else {
            
            //[TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
        }
        
    }];
}


@end
