//
//  TSAssignmentApprovalViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 05/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSAssignmentApprovalViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *assignmentsTableView;
@property (weak, nonatomic) IBOutlet UIView *selectionView;

// Button Actions
- (IBAction)approvedButtonTapped:(id)sender;
- (IBAction)declineButtonTapped:(id)sender;


@end
