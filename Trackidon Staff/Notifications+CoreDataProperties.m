//
//  Notifications+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 28/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Notifications+CoreDataProperties.h"

@implementation Notifications (CoreDataProperties)

@dynamic acadamicTransId;
@dynamic approveByTransId;
@dynamic approvedByName;
@dynamic approvedDate;
@dynamic approvedStatus;
@dynamic assignedList;
@dynamic boardName;
@dynamic boardTransID;
@dynamic isForApprovals;
@dynamic modifiedDate;
@dynamic notificationAttachmentUrl;
@dynamic notificationDate;
@dynamic notificationDescription;
@dynamic notificationGroupType;
@dynamic notificationIsHasAttachment;
@dynamic notificationPriority;
@dynamic notificationTitle;
@dynamic notificationTransId;
@dynamic schoolTransId;
@dynamic staffTransId;
@dynamic userCreatedName;
@dynamic userCreatedTransId;
@dynamic userModifiedName;
@dynamic userModifiedTransId;
@dynamic staff;

@end
