//
//  TSSettingsViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/24/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSSettingsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;


@end

@interface SettingsModel1 : NSObject

@property (nonatomic, strong) NSString *pushNotificationAlert;

@property (nonatomic, strong) NSString *emailAlert;

@property (nonatomic, strong) NSString *proximityAlert;

@property (nonatomic, strong) NSString *smsAlert;

@property (nonatomic, strong) NSString *notificationTransID;

@end
