//
//  TSPortionsListTableViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 22/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSPortionsListTableViewController.h"

@interface TSPortionsListTableViewController (){
    
    NSArray *attachmentImgArray;
}
@end

@implementation TSPortionsListTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    attachmentImgArray = @[@"one",@"two",@"three",@"four",@"five"];
    
    [self.tableView setTableFooterView:[UIView new]];
}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_PORTIONLIST parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@ Log",SINGLETON_INSTANCE.barTitle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

#pragma mark - Table view data source & delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.portionsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier     forIndexPath:indexPath];
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
    
    UILabel *approvedLabel = (UILabel *)[cell viewWithTag:101];
    
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:103];
    
    UIImageView *attachmentImgVw = (UIImageView *)[cell viewWithTag:102];
    
    Portions *portionObj = [self.portionsArray objectAtIndex:indexPath.row];
    
    titleLabel.text = [portionObj.portionTitle capitalizedString];
    
    dateLabel.text = [SINGLETON_INSTANCE stringFromDateWithFormate:DATE_FORMATE_MNTH_DAY_YR withDate:[SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:portionObj.portionDate]];
    
    if ([portionObj.approvedStatus intValue] == StatusTypeApproved) {
        
        approvedLabel.textColor = COLOR_ACCEPT_CREATEDTODAY;
        
        approvedLabel.text = [NSString stringWithFormat:@"Approved by %@",[portionObj.approvedByName uppercaseString]];
    }
    else if ([portionObj.approvedStatus intValue] == StatusTypeDeclined){
        
        approvedLabel.textColor = COLOR_DECLINED_DUESTODAY;
        
        approvedLabel.text = [NSString stringWithFormat:@"Declined by %@",[portionObj.approvedByName uppercaseString]];
    }
    else{
        
        approvedLabel.textColor = COLOR_PENDING_VIEWALL;
        
        approvedLabel.text = @"Pending";
    }
    
    if ([portionObj.isAttachment intValue]==1) {
        
        attachmentImgVw.image = [UIImage imageNamed:[attachmentImgArray objectAtIndex:indexPath.row%5]];
        
        attachmentImgVw.hidden = NO;
    }
    else{
        
        attachmentImgVw.hidden = YES;
    }
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [TSAnalytics logEvent:ANALYTICS_PORTION_CLICKED_PORTION_IN_LIST parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:self.listType,ANALYTICS_PARAM_LISTTYPE, nil]];
    
    TSPortionDescriptionViewController *portionDesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBPortionDescription"];
    
    Portions *portionsObj = [self.portionsArray objectAtIndex:indexPath.row];
    
    portionDesVC.descriptionType = PortionString;
    
    portionDesVC.commonObj = portionsObj;
    
    [self.navigationController pushViewController:portionDesVC animated:YES];

}

@end