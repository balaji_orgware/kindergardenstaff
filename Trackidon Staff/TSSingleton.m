//
//  Singleton.m
//  Trackidon Staff
//
//  Created by ORGware on 16/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSSingleton.h"

static TSSingleton *singleton;

@implementation TSSingleton

+(instancetype)singletonSharedInstance{
    
    static TSSingleton *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        kSharedInstance = [[TSSingleton alloc]init];
        
    });
    
    return kSharedInstance;
}

#pragma mark - Class Methods definitions

// Adding Easing Animation
- (void) addEaseAnimationToView: (UIView *)view{
    
    view.alpha = 0.0;
    
    [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        view.alpha = 1;
        
    } completion:^(BOOL finished) {
        
    }];
    
}

// Method to Detect Active internet connection
-(BOOL)checkForActiveInternet{
    
    Reachability *networkReach = [Reachability reachabilityForInternetConnection];
    
    [networkReach startNotifier];
    
    NetworkStatus networkStatus = [networkReach currentReachabilityStatus];
    
    switch (networkStatus) {
        case NotReachable:
        {
            return NO;
            break;
        }
        case ReachableViaWiFi:
        {
            return YES;
            break;
        }
        case ReachableViaWWAN:
        {
            return YES;
            break;
        }
    }
}

// Method to show alert view -
// MARK: UIAlertController for >= iOS 8
//        UIAlertView for < iOS 8
-(void) showAlertMessageWithTitle:(NSString *)title andMessage:(NSString *)message //classInstance:(id)instance
{
    /*
    if (DeviceSystemVersion >= 8.0) {
        
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okBtnAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        
        [alertView addAction:okBtnAction];
        
        [instance presentViewController:alertView animated:YES completion:nil];
    }
    else{
     */
    
        [[[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
//    }
}

//// Drawing Border Layer for the view
//-(void)drawLayerForView:(UIView *)view inPosition:(NSString *)position color:(UIColor *)color{
//    
//    CALayer *border = [CALayer layer];
//    
//    border.backgroundColor = color.CGColor;
//    
//    if ([position isEqualToString:Bottom]) {
//        
//        border.frame = CGRectMake(0, view.frame.size.height - layerThickness, view.frame.size.width, layerThickness);
//        
//    } else if ([position isEqualToString:Right]){
//        
//        border.frame = CGRectMake(view.frame.size.width - layerThickness, 0 , layerThickness , view.frame.size.height);
//        
//    } else if ([position isEqualToString:Top]){
//        
//        border.frame = CGRectMake(0, 0, view.frame.size.width, layerThickness);
//        
//    } else if ([position isEqualToString:Left]){
//        
//        border.frame = CGRectMake(0, 0, layerThickness, view.frame.size.height);
//    }
//    
//    [view.layer addSublayer:border];
//}

- (void)addDoneButtonForTextView:(UITextView *)textView
{
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    
    [keyboardDoneButtonView sizeToFit];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(doneClicked:)];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexibleSpace,doneButton, nil]];
    
    textView.inputAccessoryView = keyboardDoneButtonView;
}

-(void) addDoneButtonForTextField:(UITextField *)textField{
    
    UIToolbar *keyboardDoneButtonView = [UIToolbar new];
    
    [keyboardDoneButtonView sizeToFit];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(doneClicked:)];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexibleSpace,doneButton, nil]];
    
    textField.inputAccessoryView = keyboardDoneButtonView;
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    
    [[APPDELEGATE_INSTANCE window] endEditing:YES];
}

#pragma mark - DateConversion

- (NSString *)convertTimeToTweleveHousrFomateFromString:(NSString *)timeString{
    
    NSDate *tmpDate = [self dateFromString:timeString withFormate:@"HH:mm"];
    
    NSString *convertedTime = [self stringFromDate:tmpDate withFormate:@"hh:mm a"];
    
    if(convertedTime != nil)
        return convertedTime;
    
    return @"";
}

// Method to change date with given format and returns as NSDate
- (NSDate *)dateFromStringWithFormate:(NSString *)formate withDate:(NSString *)date{
    
    static NSDateFormatter *dateFormatter;
    
    if (!dateFormatter) {
        
        dateFormatter = [NSDateFormatter new];
    }
        
    [dateFormatter setDateFormat:formate];
    
    return [dateFormatter dateFromString:date];
}

- (NSDate *)dateFromDate:(NSDate *)date withFormate:(NSString *)formate{
    
    NSString *dateString = [self stringFromDateWithFormate:formate withDate:date];
    
    return [self dateFromStringWithFormate:formate withDate:dateString];
    
}

// Change Date with given format and returns as NSString
- (NSString *)stringFromDateWithFormate:(NSString *)formate withDate:(NSDate *)date{
    
    static NSDateFormatter *dateFormatter;
    
    if (!dateFormatter) {
        
        dateFormatter = [NSDateFormatter new];
    }
    
    [dateFormatter setDateFormat:formate];
    
    return [dateFormatter stringFromDate:date];
}

- (NSString *)dateLblTextWithFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate{
    
    NSString *dateStr;
    
    if ([fromDate compare:toDate] == NSOrderedSame) {
        
        dateStr = [self stringFromDateWithFormate:DATE_FORMATE_MNTH_DAY_YR withDate:toDate];
        
    }else {
        
        NSString *firstDateStr = [self stringFromDateWithFormate:DATE_FORMATE_MNTH_DAY_YR withDate:fromDate];
        
        NSString *secondDateStr = [self stringFromDateWithFormate:DATE_FORMATE_MNTH_DAY_YR withDate:toDate];
        
        dateStr = [NSString stringWithFormat:@"%@ - %@",firstDateStr,secondDateStr];
    }
    
    return dateStr;
    
}

-(NSString *)changeDateFormatFromService:(NSString *)dateString withFormat:(NSString *)dateFormat{
    
    static NSDateFormatter *df = nil;
    
    static NSDateFormatter *dateFormatter = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (& onceToken, ^{
        
        dateFormatter = [[NSDateFormatter alloc]init];
        
        df = [[NSDateFormatter alloc]init];
        
    });
    
    [dateFormatter setDateFormat:dateFormat];
    
    [df setDateFormat:DATE_FORMATE_SERVICE_RESPONSE];
    
    return [dateFormatter stringFromDate:[df dateFromString:dateString]];
}


// Method to calculate no of days between two dates
+(NSString *) dateCompareFromDate:(NSDate *)serviceFromDate toDate:(NSDate *)serviceToDate{
    
    NSDateFormatter *dateFormatter;
    
    if (dateFormatter == nil) {
        
        dateFormatter = [NSDateFormatter new];
        
    }
    
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    
    if ([serviceFromDate compare:serviceToDate] == NSOrderedSame) { // Both date same
        
        if ([serviceFromDate compare:[dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]]] == NSOrderedSame) { // Due or assigned date same with today date
            
            return @"Due Today";
            
            
        }else if([serviceFromDate compare:[dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]]] == NSOrderedAscending){
            
            return @"";
            
        }
        
    }
    
    if ([serviceFromDate compare:[dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]]] == NSOrderedDescending) {
        
        NSLog(@"NSOrderedDescending");
        
        
        return [NSString stringWithFormat:@"%@ days to start",[self daysBetweenFromDate:[dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]] toDate:serviceFromDate]];
        
    }/*else if ([serviceFromDate compare:[dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]]] == NSOrderedSame){
      
      //NSLog(@"Same");
      
      return @"Today start";*/
    
    else{
        //if ([serviceFromDate compare:[dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]]] == NSOrderedAscending){
        
        if ([serviceToDate compare:[dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]]] == NSOrderedAscending) {
            
            return @"";
            
        }else if ([serviceToDate compare:[dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]]] == NSOrderedSame){
            
            return @"Due Today";
            
        }else{
            //if ([serviceToDate compare:[dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]]] == NSOrderedDescending){
            
            return [NSString stringWithFormat:@"%@ days left",[self daysBetweenFromDate:[dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]] toDate:serviceToDate]];
            
        }
    }
}


+(NSString *)daysBetweenFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate{
    
    NSLog(@"FromTo::%@--%@",fromDate,toDate);
    
    
    NSDate *localFromDate, *localToDate;
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    [cal rangeOfUnit:NSCalendarUnitDay startDate:&localFromDate interval:NULL forDate:fromDate];
    
    [cal rangeOfUnit:NSCalendarUnitDay startDate:&localToDate interval:NULL forDate:toDate];
    
    NSDateComponents *diff = [cal components:NSCalendarUnitDay fromDate:localFromDate toDate:localToDate options:0];
    
    return [NSString stringWithFormat:@"%ld",(long)[diff day]+1];
    
}

// Method to retrieve date string For given date with given format
- (NSString *)stringFromDate:(NSDate *)date withFormate:(NSString *)formate{
    
    static NSDateFormatter *dateFormatter = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (& onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc]init];
        
    });
    
    [dateFormatter setDateFormat:formate];
    
    return [dateFormatter stringFromDate:date];
}

// Method to retrieve NSDate from String with given format
-(NSDate *)dateFromString:(NSString *)dateString withFormate:(NSString *)dateFormat{
    
    static NSDateFormatter *dateFormatter = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (& onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc]init];
        
    });
    
    [dateFormatter setDateFormat:dateFormat];
    
    return [dateFormatter dateFromString:dateString];
}

+(NSDate *) dateFromString:(NSString *)dateStr dateFormat:(NSString *)dateFormat{
    
    NSDateFormatter *dateFormatter;
    
    if (dateFormatter == nil) {
        
        dateFormatter = [NSDateFormatter new];
        
    }
    
    [dateFormatter setDateFormat:dateFormat];
    
    return [dateFormatter dateFromString:dateStr];
    
}


#pragma mark - Instance Methods definitions

// Method to get Attributed string from Date String

-(NSMutableAttributedString *) getDateAttributedStringForDateString:(NSString *)dateString inLabel:(UILabel *)label{
    
    [label setNumberOfLines:2];
    
    NSString *dateNo = [SINGLETON_INSTANCE changeDateFormatFromService:dateString withFormat:@"dd"];
    
    NSString *monthName = [SINGLETON_INSTANCE changeDateFormatFromService:dateString withFormat:@"MMM"];
    
    UIFont *dateFont,*monthFont;
    
    if (IS_IPHONE_4) {
        
        dateFont = [UIFont fontWithName:APP_FONT size:22.0f];
        
        monthFont = [UIFont fontWithName:APP_FONT size:10.0f];
    }
    else{
        
        dateFont = [UIFont fontWithName:APP_FONT size:25.0f];
        
        monthFont = [UIFont fontWithName:APP_FONT size:12.0f];
    }
    
    NSDictionary *dateTextAttr = [NSDictionary dictionaryWithObject: dateFont forKey:NSFontAttributeName];
    NSMutableAttributedString *countAttrStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@   ",dateNo] attributes: dateTextAttr];
    
    NSDictionary *monthTextAttr = [NSDictionary dictionaryWithObject:monthFont forKey:NSFontAttributeName];
    NSMutableAttributedString *textAttrStr = [[NSMutableAttributedString alloc]initWithString:monthName attributes:monthTextAttr];
    
    [countAttrStr appendAttributedString:textAttrStr];
    
    return countAttrStr;
}

// Method to validate proper email address
-(BOOL) isValidEmailAddress:(NSString *)email{

    NSString *emailAddress = [email stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegEx];
    
    return [emailPredicate evaluateWithObject:emailAddress];
}

// Method to validate proper mobile no
-(BOOL) isValidMobileNo:(NSString *)mobile{
    
    if (mobile.length == 10) {
        
        NSString *mobileNo = [mobile stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSString *phoneNoRegex = @"^(?:|0|[0-9]\\d*)(?:\\.\\d*)?$";
        
        NSPredicate *phonePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneNoRegex];
        
        return [phonePredicate evaluateWithObject:mobileNo];
    }
    else
        return NO;
}


// Checking whether the string is Empty/Null/Nil or not
-(BOOL) isEmpty :(NSString *)string{
    
    NSString *trimmedString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([trimmedString isKindOfClass:[NSNull class]] || [trimmedString isEqualToString:@""]|| string==nil || [trimmedString isEqualToString:@"<null>"])
        return YES;
    else
        return NO;
    
}

// Method to add Right Arrow to TextField Right
-(void)addRightViewToTextField:(UITextField *)textField withImageName:(NSString *)imageName{
    
    UIView *containerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    
    imageView.image = [UIImage imageNamed:imageName];
    
    [containerView addSubview:imageView];
    
    imageView.center = containerView.center;
    
    textField.rightView = containerView;
    
    textField.rightViewMode = UITextFieldViewModeAlways;
}

// Method to add Drop down arrow to the textfield right
-(void)addDropDownArrowToTextField:(UITextField *)textField{
    
    UIView *containerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    
    UIImageView *dropDownImgVw = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    
    dropDownImgVw.image = [UIImage imageNamed:@"dropdown"];
    
    [containerView addSubview:dropDownImgVw];
    
    dropDownImgVw.center = containerView.center;

    textField.rightView = containerView;
    
    textField.rightViewMode = UITextFieldViewModeAlways;
}

// Method to add Left Inset View to Textfield
-(void)addLeftInsetToTextField:(UITextField *)textField{
    
    UIView *insetView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, textField.frame.size.height)];
    
    textField.leftView = insetView;
    
    textField.leftViewMode = UITextFieldViewModeAlways;
}

// Method to check Null value. If null returns, returns empty string.
-(NSString *) nullValueCheck:(id)string{
    
    NSString *trimmedString = [NSString stringWithFormat:@"%@",string];
    
    trimmedString = [trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([trimmedString isKindOfClass:[NSNull class]] || [trimmedString isEqualToString:@""]|| string==nil || [trimmedString isEqualToString:@"<null>"])
        return @"";
    else
        return trimmedString;
}

+(void)showAlertWithMessage:(NSString *)message{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:nil, nil];
        [toast show];
        
        int duration = 2; // duration in seconds
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [toast dismissWithClickedButtonIndex:0 animated:YES];
        });
        
        
    });
    
}

+(void) requestTimeOutErrorAlert{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Request timed out" message:@"Something went wrong, please try again after some time." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    [alert show];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [alert dismissWithClickedButtonIndex:0 animated:YES];
    });
    
}

// Reachability check for Internet Connection
-(BOOL)reachabilityCheck{
    
    BOOL isReachability;
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {
        
        isReachability = NO;
        NSLog(@"NO Internet Connection");
        
    }else{
        NSLog(@"Internet Connection available");
        isReachability = YES;
    }
    return isReachability;
}

+(void)layerDrawForView:(UIView *)view position:(LAYER_SIDES)position color:(UIColor *)color{
    
    CALayer *border = [CALayer layer];
    
    border.backgroundColor = color.CGColor;
    
    switch (position) {
            
        case LAYER_TOP:
            
            border.frame = CGRectMake(0, 0, view.frame.size.width, layerThickness);
            
            break;
            
        case LAYER_BOTTOM:
            
            border.frame = CGRectMake(0, view.frame.size.height - layerThickness, view.frame.size.width, layerThickness);
            
            break;
            
        case LAYER_LEFT:
            
            border.frame = CGRectMake(0, 0, layerThickness, view.frame.size.height);
            
            break;
            
        case LAYER_RIGHT:
            
            border.frame = CGRectMake(view.frame.size.width - layerThickness, 0 , layerThickness , view.frame.size.height);
            
            break;
            
        case LAYER_ALL:
            
            border.frame = view.frame;
            
            break;
        default:
            break;
    }
    
    [view.layer addSublayer:border];    
}

+(void)removeAllLayersForView:(UIView *)view{
    
    @try {
        for (CALayer *layer in view.layer.sublayers) {
            [layer removeFromSuperlayer];
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

// MARK: Core Data Methods

- (void)storeContextInMainThread{
    
    @try {
        
        if (![[NSThread currentThread] isMainThread]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSError *error;
                
                if (![[APPDELEGATE_INSTANCE managedObjectContext] save:&error]) {
                    
                    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                    
                }
                
            });
            
        }else {
            
            NSError *error;
            
            if (![[APPDELEGATE_INSTANCE managedObjectContext] save:&error]) {
                
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                
            }
            
        }
        
    } @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    }
    
}

// Delete all entries in the given Entity
- (void)deleteAllObjectsInEntity:(NSString *)nameEntity{
    
    @try {
        NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:nameEntity];
        [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError *error;
        
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
        if ([[NSThread currentThread] isMainThread]) {
            
            for (NSManagedObject *object in fetchedObjects)
            {
                [context deleteObject:object];
            }
            
            NSError *error;
            
            [context save:&error];

        }else {
            
            [context performBlock:^{
                
                for (NSManagedObject *object in fetchedObjects)
                {
                    [context deleteObject:object];
                }
                
                NSError *error;
                
                [context save:&error];

            }];
            
        }
        
        //dispatch_async(dispatch_get_main_queue(), ^{
            
            //NSError *error;
            
            //[context save:&error];
            
        //});
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
}

// Method to delete Particular Objects in an entity

-(void)deleteSelectedObjectsInEntityWithEntityName:(NSString *)entityName withPredicate:(NSPredicate *)predicate withContext:(NSManagedObjectContext *)context{
    
    @try {
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
        
        [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        [fetchRequest setPredicate:predicate];
        
        NSError *error;
        
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
        if ([[NSThread currentThread] isMainThread]) {
            
            for (NSManagedObject *object in fetchedObjects)
            {
                [context deleteObject:object];
            }
            
            NSError *error;
            
            [context save:&error];
            
        }else {
            
            [context performBlock:^{
                
                for (NSManagedObject *object in fetchedObjects)
                {
                    [context deleteObject:object];
                }
                
                NSError *error;
                
                [context save:&error];
                
            }];
            
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
}

// Method to retrieve all objects in an entity
-(NSArray *)retrieveAllObjectsInEntity:(NSString *)entityName{
    
    NSArray *retrievedArray;
    
    @try {
        
        NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
        
        [fetchRequest setReturnsObjectsAsFaults:NO];
        
        NSError *error;
        
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
        retrievedArray = [NSArray arrayWithArray:fetchedObjects];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    
    return retrievedArray;
}

// Method to retrieve particular row from an entity
- (NSArray *)retrieveSelectedObjectsInEntityWithEntityName:(NSString *)entityName withPredicate:(NSPredicate *)predicate{
    
    NSArray *resultsArray;
    
    @try {

        NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSError *error = nil;
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:entityName];
        
        //[fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        [fetchRequest setPredicate:predicate];
        
        [fetchRequest setReturnsObjectsAsFaults:NO];
        
        resultsArray = [context executeFetchRequest:fetchRequest error:&error];
    }
    @catch (NSException *exception) {

        NSLog(@"%@",exception);
    }
    
    return resultsArray;
}



#pragma mark - Menu Formation

- (void)createMenu{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"schoolTransId CONTAINS[cd] %@",SINGLETON_INSTANCE.selectedSchoolTransId];
    
    NSArray *menuArray = [self retrieveSelectedObjectsInEntityWithEntityName:@"Menu" withPredicate:predicate];
    
    NSInteger logoutPriority = 1000;
    
    if (_moduleMenuArr == nil)
        _moduleMenuArr = [[NSMutableArray alloc]init];
    
    if (_communicationMenuArr == nil)
        _communicationMenuArr = [[NSMutableArray alloc]init];
    
    if (_moreMenuArr == nil)
        _moreMenuArr = [[NSMutableArray alloc]init];
    
    if (_inboxMenuArr == nil)
        _inboxMenuArr = [[NSMutableArray alloc]init];
    
    if (_approvalMenuArr == nil)
        _approvalMenuArr = [[NSMutableArray alloc]init];
    
    [_moduleMenuArr removeAllObjects];
    [_communicationMenuArr removeAllObjects];
    [_moreMenuArr removeAllObjects];
    [_inboxMenuArr removeAllObjects];
    [_approvalMenuArr removeAllObjects];
    
    for (Menu *menu in menuArray) {
        
        if ([menu.isModule integerValue] == 1 && [menu.isShow integerValue] == 1) {
            
            NSDictionary *optionDict = [[NSDictionary alloc]initWithObjectsAndKeys:menu.moduleName,@"ModuleName",[NSNumber numberWithInteger:[menu.moduleId integerValue]],@"ModuleID",[NSNumber numberWithInteger:[menu.modulePriority integerValue]],@"ModulePriority", nil];
            
            [_moduleMenuArr addObject:optionDict];
            
            [_moduleMenuArr sortUsingDescriptors: [NSArray arrayWithObjects: [NSSortDescriptor sortDescriptorWithKey:@"ModulePriority" ascending:YES], nil]];
            
        } else {
            
            if ([menu.moduleId integerValue] == MENU_COMMUNICATION && [menu .isShow integerValue] == 1) {
                
                NSDictionary *optionDict = [[NSDictionary alloc]initWithObjectsAndKeys:menu.menuName,@"MenuName",[NSNumber numberWithInteger:[menu.menuId integerValue]],@"MenuID",[NSNumber numberWithInteger:[menu.menuPriority integerValue]],@"MenuPriority", nil];
                
                [_communicationMenuArr addObject:optionDict];
                
                [_communicationMenuArr sortUsingDescriptors: [NSArray arrayWithObjects: [NSSortDescriptor sortDescriptorWithKey:@"MenuPriority" ascending:YES], nil]];
                
            }
            else if ([menu.moduleId integerValue] == MENU_INBOX && [menu .isShow integerValue] == 1){
                
                NSDictionary *optionDict = [[NSDictionary alloc]initWithObjectsAndKeys:menu.menuName,@"MenuName",[NSNumber numberWithInteger:[menu.menuId integerValue]],@"MenuID",[NSNumber numberWithInteger:[menu.menuPriority integerValue]],@"MenuPriority", nil];
                
                [_inboxMenuArr addObject:optionDict];
                
                [_inboxMenuArr sortUsingDescriptors: [NSArray arrayWithObjects: [NSSortDescriptor sortDescriptorWithKey:@"MenuPriority" ascending:YES], nil]];
            }
            else if ([menu.moduleId integerValue] == MENU_APPROVALS && [menu .isShow integerValue] == 1){
                
                NSDictionary *optionDict = [[NSDictionary alloc]initWithObjectsAndKeys:menu.menuName,@"MenuName",[NSNumber numberWithInteger:[menu.menuId integerValue]],@"MenuID",[NSNumber numberWithInteger:[menu.menuPriority integerValue]],@"MenuPriority", nil];
                
                [_approvalMenuArr addObject:optionDict];
                
                [_approvalMenuArr sortUsingDescriptors: [NSArray arrayWithObjects: [NSSortDescriptor sortDescriptorWithKey:@"MenuPriority" ascending:YES], nil]];
            }
            else if ([menu.moduleId integerValue] == MENU_MORE && [menu.isShow integerValue] == 1) {
                
                NSDictionary *optionDict = [[NSDictionary alloc]initWithObjectsAndKeys:menu.menuName,@"MenuName",[NSNumber numberWithInteger:[menu.menuId integerValue]],@"MenuID",[NSNumber numberWithInteger:[menu.menuPriority integerValue]],@"MenuPriority", nil];
                
                [_moreMenuArr addObject:optionDict];
                
                [_moreMenuArr sortUsingDescriptors: [NSArray arrayWithObjects: [NSSortDescriptor sortDescriptorWithKey:@"MenuPriority" ascending:YES], nil]];
                
                if([menu.menuId integerValue] == MENU_LOGOUT)
                    logoutPriority = [menu.menuPriority integerValue];

            }
        }
    }
    
    NSArray *schoolArray = [self retrieveAllObjectsInEntity:@"School"];
    
    //Adding Change School Options
    
    if (schoolArray.count > 1) {
        
        NSDictionary *optionDict = [[NSDictionary alloc]initWithObjectsAndKeys:@"Change School",@"MenuName",[NSNumber numberWithInt:MENU_CHANGE_SCHOOL],@"MenuID",[NSNumber numberWithInteger:logoutPriority - 1],@"MenuPriority", nil];
        
        [_moreMenuArr addObject:optionDict];
        
        [_moreMenuArr sortUsingDescriptors: [NSArray arrayWithObjects: [NSSortDescriptor sortDescriptorWithKey:@"MenuPriority" ascending:YES], nil]];
        
    }
    
    //if([SINGLETON_INSTANCE.staff.staffRole integerValue] == 7)
        [SINGLETON_INSTANCE updatePushCountServiceCall];
    
    [SINGLETON_INSTANCE fetchBoardAndClassDetailsServiceCallWithSchoolTransId:SINGLETON_INSTANCE.selectedSchoolTransId showIndicator:NO onCompletion:nil];
    
    [SINGLETON_INSTANCE fetchStaffListBySchoolTransId:SINGLETON_INSTANCE.selectedSchoolTransId showIndicator:NO onCompletion:nil];
    
    [SINGLETON_INSTANCE fetchGroupListBySchoolTransId:SINGLETON_INSTANCE.selectedSchoolTransId showIndicator:NO onCompletion:nil];
    
    [SINGLETON_INSTANCE fetchReceiverDetailsWithSchoolTransId:SINGLETON_INSTANCE.selectedSchoolTransId showIndicator:NO onCompletion:nil];
    
    if(_moduleMenuArr.count == 0)
        [USERDEFAULTS setBool:NO forKey:@"isLogin"];
    
    [APPDELEGATE_INSTANCE initilizeInitialViewController];
}

#pragma mark - ServiceCall
- (void)fetchFilterAndReceipientDetailsWithMenuId:(NSString *)menuId showIndicator:(BOOL)showIndicator onCompletion:(onFilterAndReceipientFetchCompletion)completionHandler  {
    
    NSString *url = @"MastersMServices/SchoolNotificationsService.svc/FetchMenuNameByID";
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    [param setObject:menuId forKey:@"menuTypeID"];
    
    
    [[TSHandlerClass sharedHandler] postRequestWithURL:url baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:param currentView:nil showIndicator:showIndicator onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            //NSLog(@"%@",responseDictionary);
            
            if ([responseDictionary[@"FetchMenuNameByIDResult"][@"ResponseCode"] integerValue] == 1) {
                
                if ([responseDictionary[@"FetchMenuNameByIDResult"][@"FilterTypeDetails"] count] > 0) {
                    
                    [[ReceiverFilterList instance]saveReceiverFilterListWithResponseDict:responseDictionary menuId:menuId];
                    
                }
                
                if ([responseDictionary[@"FetchMenuNameByIDResult"][@"ReceipientTypeDetails"] count] > 0) {
                    
                    [[ReceiverReceipientList instance] saveReceiverReceipientFilterListWithResponseDict:responseDictionary menuId:menuId];
                    
                }
                
            }
            
        }else{
            //Service Call Failure
        }
        
    }];
    
    
}

//Initial Service Calls

- (void)fetchBoardAndClassDetailsServiceCallWithSchoolTransId:(NSString *)schoolTransId showIndicator:(BOOL)showIndicator onCompletion:(onBoardFetchCompletion)completionHandler {
    
    NSString *url = @"MastersMRef/Service/MasterRefService.svc/FetchMastersbySchool";
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    [param setObject:schoolTransId forKey:@"guidSchoolTransID"];
    
    [param setObject:@"1" forKey:@"intTypeID"];
    
    [[TSHandlerClass sharedHandler] postRequestWithURL:url baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:param currentView:nil showIndicator:showIndicator onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            //NSLog(@"%@",responseDictionary);
            
            if ([responseDictionary[@"FetchMastersbySchoolResult"][@"ResponseCode"] integerValue] == 1) {
                
                [[Board instance]saveBoardAndClassDetailsWithResponseDict:responseDictionary schoolTransId:schoolTransId];
                
                if (completionHandler != nil)
                    completionHandler(YES);
                
            }else {
                if (completionHandler != nil)
                    completionHandler(NO);
            }
            
        }else{
            if (completionHandler != nil)
                completionHandler(NO);
        }
        
    }];
    
}

- (void)fetchReceiverDetailsWithSchoolTransId:(NSString *)schoolTransId showIndicator:(BOOL)showIndicator onCompletion:(onReceiverFetchCompletion)completionHandler  {
    
    NSString *url = @"MastersMRef/Service/MasterRefService.svc/FetchCommonmaster";
    
    /*NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    [param setObject:menuId forKey:@"menuTypeID"];*/
    
    
    [[TSHandlerClass sharedHandler] postRequestWithURL:url baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:nil currentView:nil showIndicator:showIndicator onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            //NSLog(@"%@",responseDictionary);
            
            if ([responseDictionary[@"FetchCommonmasterResult"][@"MasterClassResponseCode"] integerValue] == 1) {
                
                [[CommonTypeDetails instance]storeTypeDetailsWithDict:responseDictionary];
            }
            
            if ([responseDictionary[@"FetchCommonmasterResult"][@"MasterRefClassResponseCode"] integerValue] == 1) {
                
                [[ReceiverDetails instance]saveReceiverDetailsWithResponseDict:responseDictionary];
                
                if (completionHandler != nil)
                    completionHandler(YES);
                
            }else {
                if (completionHandler != nil)
                    completionHandler(NO);
            }
            
        }else{
            //Service Call Failure
            if (completionHandler != nil)
                completionHandler(NO);
        }
        
    }];
    
}

- (void)fetchStaffListBySchoolTransId:(NSString *)schoolTransId showIndicator:(BOOL)showIndicator onCompletion:(onStaffListFetchCompletion)completionHandler {
    
    NSString *url = @"MastersMServices/AccountsMService.svc/FetchAccountsbyType";
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
     
    [param setObject:@"8" forKey:@"intTypeID"];
    
    [param setObject:schoolTransId forKey:@"guidSchoolTransID"];
    
    
    [[TSHandlerClass sharedHandler] postRequestWithURL:url baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:param currentView:nil showIndicator:showIndicator onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            //NSLog(@"%@",responseDictionary);
            
            if ([responseDictionary[@"FetchAccountsbyTypeResult"][@"ResponseCode"] integerValue] == 1) {
                
                [[StaffList instance] saveStaffListWithResponseDict:responseDictionary schoolTransId:schoolTransId];
                
                if (completionHandler != nil)
                    completionHandler(YES);
                
            }else {
                if (completionHandler != nil)
                    completionHandler(NO);
            }
            
        }else{
            //Service Call Failure
            if (completionHandler != nil)
                completionHandler(NO);
        }
        
    }];
}

- (void)fetchGroupListBySchoolTransId:(NSString *)schoolTransId showIndicator:(BOOL)showIndicator onCompletion:(onGroupListFetchCompletion)completionHandler {
    
    NSString *url = @"GroupConfig/Service/GroupMService.svc/FetchAllGroupM";

    //NSString *url = @"GroupConfig/Service/NotificationGroup.svc/FetchMastersbySchool";
    
    //NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    //[param setObject:@"1" forKey:@"intTypeID"];
    
    //[param setObject:schoolTransId forKey:@"guidSchoolTransID"];
    
    
    [[TSHandlerClass sharedHandler] postRequestWithURL:url baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:nil currentView:nil showIndicator:showIndicator onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            //NSLog(@"%@",responseDictionary);
            
            if ([responseDictionary[@"FetchAllGroupMResult"][@"ResponseCode"] integerValue] == 1) {
                
                [[GroupList instance] saveGroupListWithResponseDict:responseDictionary schoolTransId:schoolTransId];
                
                if (completionHandler != nil)
                    completionHandler(YES);
                
            }else {
                if (completionHandler != nil)
                    completionHandler(NO);
            }
            
        }else{
            //Service Call Failure
            if (completionHandler != nil)
                completionHandler(NO);
        }
        
    }];
}

#pragma mark - Push Cont

-(void) updatePushCountServiceCall{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId = %@ AND isRead = 1 AND isForApproval = 0",SINGLETON_INSTANCE.staff.staffTransId];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    [param setValue:SINGLETON_INSTANCE.staff.staffTransId forKey:@"AccountTransID"];
    
    NSArray *array = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"PushCount" withPredicate:predicate];
    
    NSMutableArray *countArray = [NSMutableArray new];
    
    NSMutableArray *commTypeArray = [NSMutableArray new];
    
    for (PushCount *obj in array) {
        
        [countArray addObject:obj.unreadCount];
        
        [commTypeArray addObject:obj.commTypeID];
        
    }
    
    [param setValue:countArray forKey:@"PushCount"];
    
    [param setValue:commTypeArray forKey:@"CommunicationTypeID"];
    
    NSString *url = @"CommunicationPortal/Service/PushCountService.svc/ResetPushCount";
    
    [[TSHandlerClass sharedHandler] postRequestWithURL:url baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:param currentView:nil showIndicator:NO onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"ResetPushCountResult"][@"ResponseCode"] boolValue]) {
                
                [[PushCount instance] saveUpdatedPushCountsWithDict:responseDictionary isForApproval:NO];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadPushCount" object:nil];
                
            }
        }
        
    }];
}


-(void)fetchPushCountServiceCall{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"AccountTransID"];
    
    NSString *url = @"CommunicationPortal/Service/PushCountService.svc/FetchPushCountbyAccountID";
    
    [[TSHandlerClass sharedHandler] postRequestWithURL:url baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:param currentView:nil showIndicator:NO onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        if (success) {
            
            if ([responseDictionary[@"FetchPushCountbyAccountIDResult"][@"ResponseCode"] boolValue]) {
                
                [[PushCount instance] savePushCountsWithDict:responseDictionary];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadPushCount" object:nil];
                
            }
        }
    }];
}

@end