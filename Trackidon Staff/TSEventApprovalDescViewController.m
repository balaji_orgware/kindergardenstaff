//
//  TSEventApprovalDescViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 04/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSEventApprovalDescViewController.h"

@interface TSEventApprovalDescViewController (){
    
    Events *eventsObj;
    
    NSURL *attachmentURL;
    
    NSArray *assigneeArr;
}
@end

@implementation TSEventApprovalDescViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(descViewTapped)];
    
    [_descriptionTextView addGestureRecognizer:tapGesture];

    
    eventsObj = (Events *)self.eventObj;
    
    {
        [TSSingleton layerDrawForView:_topView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        
        [TSSingleton layerDrawForView:_dateView position:LAYER_TOP color:[UIColor lightGrayColor]];
        
        [TSSingleton layerDrawForView:_assignedToView position:LAYER_TOP color:[UIColor lightGrayColor]];
        
        [SINGLETON_INSTANCE addLeftInsetToTextField:_assignedToTxtFld];
    }
    
    /*NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transId MATCHES %@",eventsObj.eventTransId];
    
    assigneeArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"AssignedDetails" withPredicate:predicate];
    
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"assigneeType" ascending:YES];
    
    NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
    
    assigneeArr = [assigneeArr sortedArrayUsingDescriptors:descriptors];*/
    
    assigneeArr = [eventsObj.assignedList componentsSeparatedByString:ASSIGNEE_SAPRATOR];
    
    if (assigneeArr.count ==0) {
     
        [_assignedToView setHidden:YES];
    }
    else{
        
        [_assignedToView setHidden:NO];
        
        if (assigneeArr.count == 1) {
            
            //AssignedDetails *assignedDetails = [assigneeArr objectAtIndex:0];
            
            _assignedToTxtFld.text = [NSString stringWithFormat:@"Assigned to : %@",[assigneeArr objectAtIndex:0]];
        }
        else{
            
            _assignedToTxtFld.text = @"Assigned to :";
            
            [SINGLETON_INSTANCE addRightViewToTextField:_assignedToTxtFld withImageName:EXPAND_IMGNAME];
        }
    }
    
    {
        _attachmentButton.frame = CGRectMake(DEVICE_SIZE.width - _attachmentButton.frame.size.width - 10, _topView.frame.origin.y + _topView.frame.size.height - _attachmentButton.frame.size.height/2 + 18, _attachmentButton.frame.size.height, _attachmentButton.frame.size.height);
        
        _progressView.frame = CGRectMake(_fromDateLabel.center.x +_fromDateLabel.frame.size.width/2, _fromDateLabel.center.y, _progressView.frame.size.width, _progressView.frame.size.height);
        
        _toDateLabel.frame = CGRectMake(_progressView.center.x +_progressView.frame.size.width/2, _fromDateLabel.frame.origin.y, _toDateLabel.frame.size.width, _toDateLabel.frame.size.height);
        
        _fromLbl.center = CGPointMake(_fromDateLabel.center.x, _fromDateLabel.frame.origin.y-10);
        
        _toLbl.center = CGPointMake(_toDateLabel.center.x, _toDateLabel.frame.origin.y-10);
    }
    
    
    if (IS_IPHONE_4) {
        
//        int navigationBarHeight = 64;
        
        _dateView.frame = CGRectMake(0, DeviceHeight -75, DeviceWidth, 75);
        
        _assignedToView.frame = CGRectMake(0, DeviceHeight-_dateView.frame.size.height-40, DeviceWidth, 40);    
        
        if (assigneeArr.count == 0) {
            
            _descriptionTextView.frame = CGRectMake(self.descriptionTextView.frame.origin.x, self.descriptionTextView.frame.origin.y, self.descriptionTextView.frame.size.width, 120+_assignedToView.frame.size.height);
        }
        else{
            
            _descriptionTextView.frame = CGRectMake(self.descriptionTextView.frame.origin.x, self.descriptionTextView.frame.origin.y, self.descriptionTextView.frame.size.width, 120);
        }
        
        
        _fromDateLabel.frame = CGRectMake(_fromDateLabel.frame.origin.x, 25, 45, 45);
        
        _toDateLabel.frame = CGRectMake(_toDateLabel.frame.origin.x, _fromDateLabel.frame.origin.y, 45, 45);
        
        _fromLbl.center = CGPointMake(_fromDateLabel.center.x, 15);
        
        _toLbl.center = CGPointMake(_toDateLabel.center.x, 15);
        
        _progressView.frame = CGRectMake(_fromDateLabel.frame.origin.x+_fromDateLabel.frame.size.width, _fromDateLabel.frame.origin.y+_fromDateLabel.frame.size.height/2, _toDateLabel.frame.origin.x -(_fromDateLabel.frame.origin.x+_fromDateLabel.frame.size.width), 2);
        
        _remainingCountLbl.frame = CGRectMake(_remainingCountLbl.frame.origin.x, 20, _remainingCountLbl.frame.size.width, 40);
        
        _daysLeftLbl.frame = CGRectMake(_daysLeftLbl.frame.origin.x, _remainingCountLbl.frame.origin.y+_remainingCountLbl.frame.size.height, _daysLeftLbl.frame.size.width, _daysLeftLbl.frame.size.height);
    }

    
    _fromDateLabel.attributedText = [self getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:eventsObj.eventFromDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_fromDateLabel];
    
    _toDateLabel.attributedText = [self getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:eventsObj.eventToDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_toDateLabel];
    
    if (eventsObj.approvedByName.length >0) {
        
        _assignedTeacherLabel.text = [NSString stringWithFormat:@"- by %@",eventsObj.approvedByName];
    }
    else{
        
        _assignedTeacherLabel.text = @"";
    }
    
    _venueLbl.text = [NSString stringWithFormat:@"Venue : %@",eventsObj.eventVenue];
    
    _eventTitleLbl.text = [NSString stringWithFormat:@"Title : %@",eventsObj.eventTitle];
    
    _timeLbl.text = [NSString stringWithFormat:@"Time : %@",[SINGLETON_INSTANCE convertTimeToTweleveHousrFomateFromString:eventsObj.eventTime]];
    
    _descriptionTextView.text = [NSString stringWithFormat:@"\t%@",eventsObj.eventDescription];
    
    if ([eventsObj.eventIsHasAttachment intValue] == 1) {
        
        [_attachmentButton setHidden:NO];
    }
    else{
        
        [_attachmentButton setHidden:YES];
    }
    
    if ([eventsObj.eventFromDate compare:eventsObj.eventToDate]==NSOrderedSame) {
        
        _remainingCountLbl.text = [NSString stringWithFormat:@"%@",[TSSingleton daysBetweenFromDate:eventsObj.eventFromDate toDate:eventsObj.eventToDate]];
        
        _daysLeftLbl.text = @"Day Event";
        
    }else{
        
        _remainingCountLbl.text = [NSString stringWithFormat:@"%@",[TSSingleton daysBetweenFromDate:eventsObj.eventFromDate toDate:eventsObj.eventToDate]];
        
        _daysLeftLbl.text = @"Days Event";
    }
    
    {
        _fromDateLabel.layer.cornerRadius = _fromDateLabel.frame.size.width/2;
        
        _fromDateLabel.layer.masksToBounds = YES;
        
        _toDateLabel.layer.cornerRadius = _toDateLabel.frame.size.width/2;
        
        _toDateLabel.layer.masksToBounds = YES;
    }
    
    UIBarButtonItem *approveBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"approve"] style:UIBarButtonItemStyleDone target:self action:@selector(approveButtonAction)];
    
    UIBarButtonItem *declineBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"decline"] style:UIBarButtonItemStyleDone target:self action:@selector(declineButtonAction)];
    
    self.navigationItem.rightBarButtonItems = @[approveBtn,declineBtn];
}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_APPROVALEVENT_DESCRITION parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@ Detail",SINGLETON_INSTANCE.barTitle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button actions

-(void) approveButtonAction{
    
    UIAlertView *approveAlertView = [[UIAlertView alloc]initWithTitle:@"Confirm" message:@"Do you want to Approve this event?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    
    [approveAlertView setTag:APPROVAL_TYPE_APPROVE];
    
    [approveAlertView show];
}

-(void) declineButtonAction{
    
    UIAlertView *declinedAlertView = [[UIAlertView alloc]initWithTitle:@"Confirm" message:@"Do you want to Decline this event?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    
    [declinedAlertView setTag:APPROVAL_TYPE_DECLINE];
    
    [declinedAlertView show];
}

- (IBAction)attachmentBtnClk:(id)sender {
    
    [_attachmentButton setUserInteractionEnabled:NO];
    
    NSString *attachmentURLString;
    
    attachmentURL = [NSURL URLWithString:eventsObj.eventAttachmentUrl];
    
    attachmentURLString = eventsObj.eventAttachmentUrl;
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *attachmentFile = [documentsPath stringByAppendingPathComponent:[attachmentURL lastPathComponent]];
    
    NSLog(@"%@",attachmentFile);
    
    NSLog(@"%@",[attachmentURL lastPathComponent]);
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:attachmentFile];
    
    if (fileExists) {
        
        NSMutableDictionary *analyticsDict = [NSMutableDictionary new];
        
        [analyticsDict setValue:ANALYTICS_VALUE_OPENEDFROMLOCAL forKey:ANALYTICS_PARAM_ATTACHMENTSTATUS];
        
        [TSAnalytics logEvent:ANALYTICS_EVENTAPPROVALS_CLICKED_ATTACHMENT parameter:analyticsDict];
        
        _attachmentButton.userInteractionEnabled = YES;

        [self navigateToAttachmentViewControllerWithFileName:attachmentURL];
    }
    else{
        
        [[TSHandlerClass sharedHandler]downloadAttachmentWithURL:attachmentURLString currentView:self.view withHandler:^(BOOL success, NSURL *filePath) {
            
            if (success) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary new];
                
                [analyticsDict setValue:ANALYTICS_VALUE_DOWNLOADING forKey:ANALYTICS_PARAM_ATTACHMENTSTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_EVENTAPPROVALS_CLICKED_ATTACHMENT parameter:analyticsDict];
                
                _attachmentButton.userInteractionEnabled = YES;

                [self navigateToAttachmentViewControllerWithFileName:filePath];
            }
            else{
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary new];
                
                [analyticsDict setValue:ANALYTICS_VALUE_URLNOTFOUND forKey:ANALYTICS_PARAM_ATTACHMENTSTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_EVENTAPPROVALS_CLICKED_ATTACHMENT parameter:analyticsDict];
                
                _attachmentButton.userInteractionEnabled = YES;
            }
        }];
    }
}

#pragma mark - AlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == APPROVAL_TYPE_APPROVE) {
        
        if (buttonIndex) {
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidUserTransID"];
            
            [param setValue:[NSString stringWithFormat:@"%ld",(long)APPROVAL_TYPE_APPROVE] forKey:@"intApprovedStatus"];
            
            [param setValue:@[eventsObj.eventTransId] forKey:@"guidEventTransID"];
            
            NSDictionary *updateParam = @{@"ApproveEvents":param};
            
            [self updateEventWithParam:updateParam withApprovalType:APPROVAL_TYPE_APPROVE];
        }
    }
    else if (alertView.tag == APPROVAL_TYPE_DECLINE){
        
        if (buttonIndex) {
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidUserTransID"];
            
            [param setValue:[NSString stringWithFormat:@"%ld",(long)APPROVAL_TYPE_DECLINE] forKey:@"intApprovedStatus"];
            
            [param setValue:@[eventsObj.eventTransId] forKey:@"guidEventTransID"];
            
            NSDictionary *updateParam = @{@"ApproveEvents":param};
            
            [self updateEventWithParam:updateParam withApprovalType:APPROVAL_TYPE_DECLINE];
        }
    }
    
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

#pragma mark - Helper methods

-(NSMutableAttributedString *) getDateAttributedStringForDateString:(NSString *)dateString inLabel:(UILabel *)label{
    
    [label setNumberOfLines:2];
    
    UIFont *dateFont,*monthFont;
    
    if (IS_IPHONE_4) {
        
        dateFont = [UIFont fontWithName:APP_FONT size:22.0f];
        
        monthFont = [UIFont fontWithName:APP_FONT size:10.0f];
    }
    else{
        
        dateFont = [UIFont fontWithName:APP_FONT size:25.0f];
        
        monthFont = [UIFont fontWithName:APP_FONT size:12.0f];
    }
    
    NSString *dateNo = [SINGLETON_INSTANCE changeDateFormatFromService:dateString withFormat:@"dd"];
    
    NSString *monthName = [SINGLETON_INSTANCE changeDateFormatFromService:dateString withFormat:@"MMM"];
    
    NSDictionary *dateTextAttr = [NSDictionary dictionaryWithObject: dateFont forKey:NSFontAttributeName];
    NSMutableAttributedString *countAttrStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@   ",dateNo] attributes: dateTextAttr];
    
    NSDictionary *monthTextAttr = [NSDictionary dictionaryWithObject:monthFont forKey:NSFontAttributeName];
    NSMutableAttributedString *textAttrStr = [[NSMutableAttributedString alloc]initWithString:monthName attributes:monthTextAttr];
    
    [countAttrStr appendAttributedString:textAttrStr];
    
    return countAttrStr;
}

#pragma mark - TextField Delegates

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (assigneeArr.count>1) {
        
        AssignedListViewController *listVC = [[AssignedListViewController alloc]initWithNibName:@"AssignedListViewController" bundle:nil];
        
        [listVC setAssignedText:EventInvited];
        
        [listVC setAssignedListArray:assigneeArr];
        
        [listVC showInViewController:self];
    }
    
    return NO;
}

-(void) navigateToAttachmentViewControllerWithFileName:(NSURL *)fileURL{
    
    TSAttachmentViewController *attachmentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAttachment"];
    
    [attachmentVC setFileName:[fileURL lastPathComponent]];
    
    UINavigationController *navCtrl = [[UINavigationController alloc]initWithRootViewController:attachmentVC];
    
    navCtrl.navigationBar.translucent = NO;
    
    navCtrl.navigationBar.barTintColor = APPTHEME_COLOR;
    
    navCtrl.navigationBar.tintColor = [UIColor whiteColor];
    
    [navCtrl.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:APP_FONT size:18]}];
    
    [self.navigationController presentViewController:navCtrl animated:YES completion:nil];
}

#pragma mark - Service Calls

-(void) updateEventWithParam:(NSDictionary *)param withApprovalType:(NSInteger)approvalType{
    
    NSString *urlString = @"MastersMServices/EventsMService.svc/MobileApproveEvents";
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"MobileApproveEventsResult"][@"ResponseCode"] integerValue] == 1) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary new];
                
                if (approvalType == APPROVAL_TYPE_APPROVE) {
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                else{
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                
                [analyticsDict setValue:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_EVENTAPPROVALS_UPDATEAPPROVAL parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"MobileApproveEventsResult"][@"ResponseMessage"]];
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId MATCHES %@ AND isForApprovals == 1",SINGLETON_INSTANCE.staffTransId];
                
                [SINGLETON_INSTANCE deleteSelectedObjectsInEntityWithEntityName:@"Events" withPredicate:predicate withContext:[APPDELEGATE_INSTANCE managedObjectContext]];
                
                [[NSNotificationCenter defaultCenter]postNotificationName:@"updateTableView" object:nil];
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            else{
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary new];
                
                if (approvalType == APPROVAL_TYPE_APPROVE) {
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                else{
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                
                [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_EVENTAPPROVALS_UPDATEAPPROVAL parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
            }
        }
        else{
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary new];
            
            if (approvalType == APPROVAL_TYPE_APPROVE) {
                
                [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
            }
            else{
                
                [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
            }
            
            [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
            
            [TSAnalytics logEvent:ANALYTICS_EVENTAPPROVALS_UPDATEAPPROVAL parameter:analyticsDict];
        }
    }];
}

-(void) descViewTapped{
    
    [TSAnalytics logEvent:ANALYTICS_EVENTAPPROVALS_CLICKED_DESCRIPTION parameter:[NSMutableDictionary new]];

    TSDescriptionViewController *descVC = [[TSDescriptionViewController alloc] initWithNibName:@"TSDescriptionViewController" bundle:nil];
    
    [descVC showInViewController:self descriptionString:_descriptionTextView.text];
    
}

@end