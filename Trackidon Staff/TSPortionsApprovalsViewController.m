//
//  TSPortionsApprovalsViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 18/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSPortionsApprovalsViewController.h"

@interface TSPortionsApprovalsViewController (){
    
    NSMutableArray *portionsArray, *portionsModelArray;
    
    NSArray *attachmentImgArray;
    
    UIBarButtonItem *selectAllBtn;
    
    UILongPressGestureRecognizer *longPressGesture;
}

@end

@implementation TSPortionsApprovalsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    attachmentImgArray = @[@"one",@"two",@"three",@"four",@"five"];

    portionsArray = [NSMutableArray new];
    
    portionsModelArray = [NSMutableArray new];
    
    selectAllBtn = [[UIBarButtonItem alloc]initWithTitle:@"Select All" style:UIBarButtonItemStyleDone target:self action:@selector(selectAllAction)];
    
    longPressGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressGestureAction:)];
    
    longPressGesture.delegate = self;
    
    [self.portionsTableView addGestureRecognizer:longPressGesture];
    
    [self.portionsTableView setTableFooterView:[UIView new]];
    
    self.selectionView.frame = CGRectMake(self.selectionView.frame.origin.x, DeviceHeight+64, self.selectionView.frame.size.width, self.selectionView.frame.size.height);
    
    [self categorizePortions];
    
//    [self fetchUnapprovedPortionsAPICall];
    
    [TSAnalytics logEvent:ANALYTICS_PORTIONAPPROVALS_OPENED_APPROVALS parameter:[NSMutableDictionary new]];
}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_APPROVALPORTION_LIST parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@ Log",SINGLETON_INSTANCE.barTitle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

#pragma mark - Button Actions

- (IBAction)approvedButtonTapped:(id)sender {
    
    NSPredicate *approvePredicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *approvedArray = [portionsModelArray filteredArrayUsingPredicate:approvePredicate];
    
    if (approvedArray.count>0) {
        
        UIAlertView *approveAlertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:[NSString stringWithFormat:@"Do you want to Approve these %ld Lesson Plans?",(unsigned long)approvedArray.count] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        
        approveAlertView.tag = APPROVAL_TYPE_APPROVE;
        
        approveAlertView.delegate = self;
        
        [approveAlertView show];
    }
}

- (IBAction)declineButtonTapped:(id)sender {
    
    NSPredicate *declinePredicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *declinedArray = [portionsModelArray filteredArrayUsingPredicate:declinePredicate];
    
    if (declinedArray.count>0) {
        
        UIAlertView *declineAlertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:[NSString stringWithFormat:@"Do you want to Decline these %ld Lesson Plans?",(unsigned long)declinedArray.count] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        
        declineAlertView.tag = APPROVAL_TYPE_DECLINE;
        
        declineAlertView.delegate = self;
        
        [declineAlertView show];
    }
}

#pragma mark - Alert View Delegates

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == APPROVAL_TYPE_APPROVE) {
        
        if (buttonIndex) {
            
            NSPredicate *approvePredicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *approvedArray = [portionsModelArray filteredArrayUsingPredicate:approvePredicate];
            
            NSMutableArray *portionsTransIDArr = [NSMutableArray new];
            
            for (ApprovalsModel *modelObj in approvedArray) {
                
                [portionsTransIDArr addObject:modelObj.transID];
            }
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidUserTransID"];
            
            [param setValue:[NSString stringWithFormat:@"%ld",(long)APPROVAL_TYPE_APPROVE] forKey:@"intApprovedStatus"];
            
            [param setValue:portionsTransIDArr forKey:@"guidPortionTransID"];
            
            NSDictionary *updateParam = @{@"ApprovePortions":param};
            
            [self updatePortionsWithParam:updateParam withApprovalType:APPROVAL_TYPE_APPROVE];
        }
    }
    else if (alertView.tag == APPROVAL_TYPE_DECLINE){
        
        if (buttonIndex) {
            
            NSPredicate *declinePredicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *declinedArray = [portionsModelArray filteredArrayUsingPredicate:declinePredicate];
            
            NSMutableArray *portionsTransIDArr = [NSMutableArray new];
            
            for (ApprovalsModel *modelObj in declinedArray) {
                
                [portionsTransIDArr addObject:modelObj.transID];
            }
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidUserTransID"];
            
            [param setValue:[NSString stringWithFormat:@"%ld",(long)APPROVAL_TYPE_DECLINE] forKey:@"intApprovedStatus"];
            
            [param setValue:portionsTransIDArr forKey:@"guidPortionTransID"];
            
            NSDictionary *updateParam = @{@"ApprovePortions":param};
            
            [self updatePortionsWithParam:updateParam withApprovalType:APPROVAL_TYPE_DECLINE];
        }
    }
    
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}


#pragma mark - Helper methods

// Select All Button Action
-(void) selectAllAction{
    
    for (ApprovalsModel *modelObj in portionsModelArray) {
        
        modelObj.isSelected = YES;
    }
    
    self.navigationItem.title = [NSString stringWithFormat:@"%ld   Selected",(unsigned long)portionsModelArray.count];
    
    [self.portionsTableView reloadData];
}

// Method to Show/hide Selection View
-(void) animateSelectionView{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *filteredArray = [portionsModelArray filteredArrayUsingPredicate:predicate];
    
    if (filteredArray.count>0) {
        
        NSLog(@"%@",self.selectionView);
        
        if(self.selectionView.frame.origin.y >= DeviceHeight){
            
            [UIView animateWithDuration:0.2f animations:^{
                
                self.portionsTableView.frame = CGRectMake(self.portionsTableView.frame.origin.x, self.portionsTableView.frame.origin.y, self.portionsTableView.frame.size.width, DeviceHeight - 64 - self.selectionView.frame.size.height);
                
                self.selectionView.frame = CGRectMake(self.selectionView.frame.origin.x, self.portionsTableView.frame.origin.y+self.portionsTableView.frame.size.height, self.selectionView.frame.size.width, self.selectionView.frame.size.height);
            }];
        }
        
        self.navigationItem.rightBarButtonItem = selectAllBtn;
        
        self.navigationItem.title = [NSString stringWithFormat:@"%ld   Selected",(unsigned long)filteredArray.count];
    }
    else{
        
        self.navigationItem.rightBarButtonItem = nil;
        
        self.navigationItem.title = @"Lesson Plan Approval";
        
        if (self.selectionView.frame.origin.y < DeviceHeight) {
            
            [UIView animateWithDuration:0.2f animations:^{
                
                self.portionsTableView.frame = CGRectMake(self.portionsTableView.frame.origin.x, self.portionsTableView.frame.origin.y, self.portionsTableView.frame.size.width, DeviceHeight-64);
                
                self.selectionView.frame = CGRectMake(self.selectionView.frame.origin.x, DeviceHeight, self.selectionView.frame.size.width, self.selectionView.frame.size.height);
            }];
        }
    }
}

// Long press Gesture Action
-(void)longPressGestureAction:(UILongPressGestureRecognizer *)sender{
    
    CGPoint touchPoint = [sender locationOfTouch:0 inView:self.portionsTableView];
    
    NSIndexPath *indexPath = [_portionsTableView indexPathForRowAtPoint:touchPoint];
    
    if (indexPath != nil && sender.state == UIGestureRecognizerStateBegan) {
        
        ApprovalsModel *modelObj = [portionsModelArray objectAtIndex:indexPath.row];
        
        modelObj.isSelected = !modelObj.isSelected;
        
        [self.portionsTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        [self animateSelectionView];
    }
}

// Post Notification Method to Update Tableview from TSEventApprovalDescViewController
-(void) updateTableView{
    
    [self categorizePortions];
    
    [self fetchUnapprovedPortionsAPICall];
}

// Method to Clear Approve/Decline Selection View
-(void) clearSelectionView{
    
    [UIView animateWithDuration:0.2f animations:^{
        
        self.navigationItem.title = @"Portions Approval";
        
        self.navigationItem.rightBarButtonItem = nil;
        
        self.portionsTableView.frame = CGRectMake(self.portionsTableView.frame.origin.x, self.portionsTableView.frame.origin.y, self.portionsTableView.frame.size.width, DeviceHeight-64);
        
        self.selectionView.frame = CGRectMake(self.selectionView.frame.origin.x, DeviceHeight, self.selectionView.frame.size.width, self.selectionView.frame.size.height);
        
        self.navigationItem.rightBarButtonItem = nil;
    }];
}


#pragma mark - Tableview Datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return portionsArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sCellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier];
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
    
    UILabel *boardLabel = (UILabel *)[cell viewWithTag:101];
    
    UILabel *createdLabel = (UILabel *)[cell viewWithTag:102];
    
    UIImageView *attachmentImgVw = (UIImageView *)[cell viewWithTag:103];
    
    Portions *portionObj = [portionsArray objectAtIndex:indexPath.row];
    
    titleLabel.text = portionObj.portionTitle;
    
    createdLabel.text = [NSString stringWithFormat:@"Created by %@",portionObj.createdByName];
    
    boardLabel.text = [NSString stringWithFormat:@"Board : %@",portionObj.boardName];
    
    if ([portionObj.isAttachment boolValue]) {
        
        attachmentImgVw.hidden = NO;
        
        attachmentImgVw.image = [UIImage imageNamed:[attachmentImgArray objectAtIndex:indexPath.row%5]];
    }
    else{
        
        attachmentImgVw.hidden = YES;
    }
    
    ApprovalsModel *modelObj = [portionsModelArray objectAtIndex:indexPath.row];
    
    if (!modelObj.isSelected) {
        
        [cell setBackgroundColor:[UIColor whiteColor]];
    }
    else{
        
        [cell setBackgroundColor:[UIColor lightGrayColor]];
    }
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [TSAnalytics logEvent:ANALYTICS_PORTIONAPPROVALS_CLICKED_APPROVALSINLIST parameter:[NSMutableDictionary new]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateTableView) name:@"updateTableView" object:nil];

    TSPortionsApprovalsDescViewController * portionApprovalDescVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBPortionsApprovalsDesc"];
    
    [portionApprovalDescVC setPortionObj:[portionsArray objectAtIndex:indexPath.row]];
    
    [self.navigationController pushViewController:portionApprovalDescVC animated:YES];
}

#pragma mark - Fetch from DB

-(void) categorizePortions{

    [portionsArray removeAllObjects];
    
    [portionsModelArray removeAllObjects];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isForApprovals == 1 AND staffTransID MATCHES %@",SINGLETON_INSTANCE.staffTransId];
    
    portionsArray = [NSMutableArray arrayWithArray:[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Portions" withPredicate:predicate]];
    
    for (Portions *portionObj in portionsArray) {
        
        ApprovalsModel *modelObj = [ApprovalsModel new];
        modelObj.title = portionObj.portionTitle;
        modelObj.transID = portionObj.transID;
        modelObj.isSelected = NO;
        modelObj.approvalStatus = [portionObj.approvedStatus integerValue];
        
        [portionsModelArray addObject:modelObj];
    }
    
    if(portionsArray.count>0){
        
        [self.portionsTableView setHidden:NO];
    }
    else{
        
        [self.portionsTableView setHidden:YES];
    }
    
    [self.portionsTableView reloadData];
}


#pragma mark - Service Calls

-(void) fetchUnapprovedPortionsAPICall{
    
    NSString *urlString = @"MastersMServices/PortionsService.svc/FetchUnApprovedPortions";
    
    NSDictionary *params = @{@"AccountTransID":SINGLETON_INSTANCE.staffTransId};
    
    BOOL animated;
    
    if (portionsArray.count>0) {
        animated = NO;
    }
    else{
        animated = YES;
    }
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:animated onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchUnApprovedPortionsResult"][@"ResponseCode"] intValue] == 1) {
                
                [[Portions instance]storePortionsForStaffObj:SINGLETON_INSTANCE.staff withDict:responseDictionary isForApprovals:YES];
                
                [self categorizePortions];
            }
        }
    }];
}

// Update Events Service Call
-(void) updatePortionsWithParam:(NSDictionary *)param withApprovalType:(NSInteger)approvalType{
    
    NSString *urlString = @"MastersMServices/PortionsService.svc/MobileApprovePortions";
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"MobileApprovePortionsResult"][@"ResponseCode"] integerValue] == 1) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                if (approvalType == APPROVAL_TYPE_APPROVE) {
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                else{
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                
                [TSAnalytics logEvent:ANALYTICS_PORTIONAPPROVALS_CLICKED_BULKAPPROVALS parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"MobileApprovePortionsResult"][@"ResponseMessage"]];
                
                NSArray *portionsTransIDArr = param[@"ApprovePortions"][@"guidPortionTransID"];
                
                for (NSString *transID in portionsTransIDArr) {
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@ AND isForApprovals == 1 AND  transID MATCHES %@",SINGLETON_INSTANCE.staffTransId,transID];
                    
                    [SINGLETON_INSTANCE deleteSelectedObjectsInEntityWithEntityName:@"Portions" withPredicate:predicate withContext:[APPDELEGATE_INSTANCE managedObjectContext]];
                }
                
                [portionsModelArray removeAllObjects];
                
                [portionsArray removeAllObjects];
                
                [self clearSelectionView];
                
                [self categorizePortions];
            }
            else{
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                if (approvalType == APPROVAL_TYPE_APPROVE) {
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                else{
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                
                [TSAnalytics logEvent:ANALYTICS_PORTIONAPPROVALS_CLICKED_BULKAPPROVALS parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
            }
        }
        else{
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
            
            if (approvalType == APPROVAL_TYPE_APPROVE) {
                
                [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
            }
            else{
                
                [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
            }
            
            [TSAnalytics logEvent:ANALYTICS_PORTIONAPPROVALS_CLICKED_BULKAPPROVALS parameter:analyticsDict];
        }
    }];
}
@end