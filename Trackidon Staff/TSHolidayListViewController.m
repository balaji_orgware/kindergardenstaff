//
//  TSHolidayListViewController.m
//  Trackidon Staff
//
//  Created by Elango on 23/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSHolidayListViewController.h"

@interface TSHolidayListViewController ()

@end

@implementation TSHolidayListViewController
{
    NSArray *attachmentImgArray;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_HOLIDAYLIST parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = _holidayGroupTitle;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data sources & delegates


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.holidaysListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UILabel *holidayTitleLbl = (UILabel *)[cell viewWithTag:100];
    
    UILabel *holidayDateLbl = (UILabel *)[cell viewWithTag:101];
    
    //UIImageView *attachmentImgVw = (UIImageView *)[cell viewWithTag:102];
    
    Holidays *holidayObj = [self.holidaysListArray objectAtIndex:indexPath.row];
    
    holidayTitleLbl.text = [holidayObj.holidayTitle capitalizedString];
    
    //holidayDateLbl.text = [NSString stringWithFormat:@"%@ - %@",[[TSSingleton singletonSharedInstance] changeDateFormatFromService:holidayObj.fromDate withFormat:@"MMMM dd"],[[TSSingleton singletonSharedInstance]changeDateFormatFromService:holidayObj.toDate withFormat:@"MMMM dd, yyyy"]];
    
    holidayDateLbl.text = [self dateLblTextWithFromDateStr:holidayObj.fromDate toDate:holidayObj.toDate];
    
    //[attachmentImgVw setHidden:YES];
    
    
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
    
    if ([self.holidayGroupTitle isEqualToString:@"School holidays"]) {
        
        [analyticsDict setValue:ANALYTICS_VALUE_SCHOOLHOLIDAY forKey:ANALYTICS_PARAM_HOLIDAYTYPE];
    }
    else{
        
        [analyticsDict setValue:ANALYTICS_VALUE_PUBLICHOLIDAY forKey:ANALYTICS_PARAM_HOLIDAYTYPE];
    }
    
    [TSAnalytics logEvent:ANALYTICS_HOLIDAY_CLICKED_HOLIDAY_IN_LIST parameter:analyticsDict];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TSHolidayDescriptionViewController *holidayDesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBHolidayDescription"];
    
    Holidays *holidayObj = [self.holidaysListArray objectAtIndex:indexPath.row];
    
    holidayDesVC.holidaysObj = holidayObj;
    
    [self.navigationController pushViewController:holidayDesVC animated:YES];
    
}

- (NSString *)dateLblTextWithFromDateStr:(NSString *)fromDate toDate:(NSString *)toDate{
    
    NSString *dateStr;
    
    if ([fromDate compare:toDate]== NSOrderedSame){
        
        dateStr = [SINGLETON_INSTANCE changeDateFormatFromService:toDate withFormat:DATE_FORMATE_MNTH_DAY_YR];
        
    }
    else{
        
        NSString *firstDateStr = [SINGLETON_INSTANCE changeDateFormatFromService:fromDate withFormat:DATE_FORMATE_MNTH_DAY_YR];
        
        NSString *secondDateStr = [SINGLETON_INSTANCE changeDateFormatFromService:toDate withFormat:DATE_FORMATE_MNTH_DAY_YR];
        
        dateStr = [NSString stringWithFormat:@"%@ - %@",firstDateStr,secondDateStr];
        
    }
    
    return dateStr;
}


@end
