//
//  Assignments+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 28/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Assignments.h"

NS_ASSUME_NONNULL_BEGIN

@interface Assignments (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *academicTransID;
@property (nullable, nonatomic, retain) NSString *academicYear;
@property (nullable, nonatomic, retain) NSString *approvedByName;
@property (nullable, nonatomic, retain) NSString *approvedByTransID;
@property (nullable, nonatomic, retain) NSDate *approvedDate;
@property (nullable, nonatomic, retain) NSNumber *approvedStatus;
@property (nullable, nonatomic, retain) NSString *assignedList;
@property (nullable, nonatomic, retain) NSDate *assignmentDate;
@property (nullable, nonatomic, retain) NSString *assignmentDescription;
@property (nullable, nonatomic, retain) NSString *assignmentTitle;
@property (nullable, nonatomic, retain) NSString *assignmentTypeName;
@property (nullable, nonatomic, retain) NSString *attachment;
@property (nullable, nonatomic, retain) NSString *boardName;
@property (nullable, nonatomic, retain) NSString *boardTransID;
@property (nullable, nonatomic, retain) NSString *createdBy;
@property (nullable, nonatomic, retain) NSDate *dueDate;
@property (nullable, nonatomic, retain) NSNumber *isAttachment;
@property (nullable, nonatomic, retain) NSNumber *isForApprovals;
@property (nullable, nonatomic, retain) NSString *modifiedBy;
@property (nullable, nonatomic, retain) NSDate *modifiedDate;
@property (nullable, nonatomic, retain) NSString *schoolName;
@property (nullable, nonatomic, retain) NSString *schoolTransID;
@property (nullable, nonatomic, retain) NSString *staffTransID;
@property (nullable, nonatomic, retain) NSString *subjectName;
@property (nullable, nonatomic, retain) NSString *transID;
@property (nullable, nonatomic, retain) StaffDetails *staff;

@end

NS_ASSUME_NONNULL_END
