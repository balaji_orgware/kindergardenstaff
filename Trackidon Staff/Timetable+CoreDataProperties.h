//
//  Timetable+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Balaji on 08/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Timetable.h"

NS_ASSUME_NONNULL_BEGIN

@interface Timetable (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *academicTransID;
@property (nullable, nonatomic, retain) NSString *boardTransID;
@property (nullable, nonatomic, retain) NSString *day;
@property (nullable, nonatomic, retain) NSString *dayDescription;
@property (nullable, nonatomic, retain) NSString *dayTransID;
@property (nullable, nonatomic, retain) NSString *periodEight;
@property (nullable, nonatomic, retain) NSString *periodEightTransID;
@property (nullable, nonatomic, retain) NSString *periodFive;
@property (nullable, nonatomic, retain) NSString *periodFiveTransID;
@property (nullable, nonatomic, retain) NSString *periodFour;
@property (nullable, nonatomic, retain) NSString *periodFourTransID;
@property (nullable, nonatomic, retain) NSString *periodOne;
@property (nullable, nonatomic, retain) NSString *periodOneTransID;
@property (nullable, nonatomic, retain) NSString *periodSeven;
@property (nullable, nonatomic, retain) NSString *periodSevenTransID;
@property (nullable, nonatomic, retain) NSString *periodSix;
@property (nullable, nonatomic, retain) NSString *periodSixTransID;
@property (nullable, nonatomic, retain) NSString *periodThree;
@property (nullable, nonatomic, retain) NSString *periodThreeTransID;
@property (nullable, nonatomic, retain) NSString *periodTwo;
@property (nullable, nonatomic, retain) NSString *periodTwoTransID;
@property (nullable, nonatomic, retain) NSString *specialClass;
@property (nullable, nonatomic, retain) NSString *specialClassTransID;
@property (nullable, nonatomic, retain) NSString *staffTransID;
@property (nullable, nonatomic, retain) NSString *transID;
@property (nullable, nonatomic, retain) NSString *periodOneClassTransID;
@property (nullable, nonatomic, retain) NSString *periodOneSecTransID;
@property (nullable, nonatomic, retain) NSString *periodTwoClassTransID;
@property (nullable, nonatomic, retain) NSString *periodTwoSecTransID;
@property (nullable, nonatomic, retain) NSString *periodThreeClassTransID;
@property (nullable, nonatomic, retain) NSString *periodThreeSecTransID;
@property (nullable, nonatomic, retain) NSString *periodFourClassTransID;
@property (nullable, nonatomic, retain) NSString *periodFourSecTransID;
@property (nullable, nonatomic, retain) NSString *periodFiveClassTransID;
@property (nullable, nonatomic, retain) NSString *periodFiveSecTransID;
@property (nullable, nonatomic, retain) NSString *periodSixClassTransID;
@property (nullable, nonatomic, retain) NSString *periodSixSecTransID;
@property (nullable, nonatomic, retain) NSString *periodSevenClassTransID;
@property (nullable, nonatomic, retain) NSString *periodSevenSecTransID;
@property (nullable, nonatomic, retain) NSString *periodEightClassTransID;
@property (nullable, nonatomic, retain) NSString *periodEightSecTransID;
@property (nullable, nonatomic, retain) NSString *periodEightClass;
@property (nullable, nonatomic, retain) NSString *periodEightSection;
@property (nullable, nonatomic, retain) NSString *periodSevenClass;
@property (nullable, nonatomic, retain) NSString *periodSevenSection;
@property (nullable, nonatomic, retain) NSString *periodSixClass;
@property (nullable, nonatomic, retain) NSString *periodSixSection;
@property (nullable, nonatomic, retain) NSString *periodFiveClass;
@property (nullable, nonatomic, retain) NSString *periodFiveSection;
@property (nullable, nonatomic, retain) NSString *periodFourClass;
@property (nullable, nonatomic, retain) NSString *periodFourSection;
@property (nullable, nonatomic, retain) NSString *periodThreeClass;
@property (nullable, nonatomic, retain) NSString *periodThreeSection;
@property (nullable, nonatomic, retain) NSString *periodTwoClass;
@property (nullable, nonatomic, retain) NSString *periodTwoSection;
@property (nullable, nonatomic, retain) NSString *periodOneClass;
@property (nullable, nonatomic, retain) NSString *periodOneSection;
@property (nullable, nonatomic, retain) NSString *specialClassClassTransID;
@property (nullable, nonatomic, retain) NSString *specialClassSecTransID;
@property (nullable, nonatomic, retain) NSString *specialClassClass;
@property (nullable, nonatomic, retain) NSString *specialClassSec;
@property (nullable, nonatomic, retain) StaffDetails *staff;

@end

NS_ASSUME_NONNULL_END
