//
//  CommonTypeDetails.h
//  Trackidon Staff
//
//  Created by Balaji on 07/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommonTypeDetails : NSManagedObject

// Method to get Instance of CommonTypeDetails
+(instancetype) instance;

// Method to store details with Dict
-(void)storeTypeDetailsWithDict:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END

#import "CommonTypeDetails+CoreDataProperties.h"
