//
//  StaffList+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 06/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "StaffList+CoreDataProperties.h"

@implementation StaffList (CoreDataProperties)

@dynamic staffName;
@dynamic staffTransId;
@dynamic schoolTransId;
@dynamic boardTransId;
@dynamic boardName;
@dynamic schoolName;
@dynamic school;

@end
