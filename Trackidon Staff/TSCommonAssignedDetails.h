//
//  TSCommonAssignedDetails.h
//  Trackidon Staff
//
//  Created by Elango on 27/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSCommonAssignedDetails : NSObject

#define ASSIGNEE_SAPRATOR @"~#"

typedef NS_ENUM(NSInteger, OBJECT_TYPE)
{
    OBJECT_TYPE_EVENT = 1,
    OBJECT_TYPE_ASSIGNMENT,
    OBJECT_TYPE_NOTIFICATION,
    OBJECT_TYPE_PORTIONS,
    OBJECT_TYPE_EVENTINBOX,
    OBJECT_TYPE_NOTIFICATIONINBOX,
    OBJECT_TYPE_PORTIONINBOX
};

+ (instancetype)instance;

- (NSString *)getAssignedDetailsWithArray:(NSArray *)assignedArr withGroupType:(NSString *)groupType;

@end

@interface AssigneeModel : NSObject

@property (nullable, nonatomic, retain) NSString *assigneeName;
@property (nullable, nonatomic, retain) NSNumber *assigneeType;

@end

