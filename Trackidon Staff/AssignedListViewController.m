//
//  AssignedListViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/28/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "AssignedListViewController.h"

@interface AssignedListViewController ()



@end

@implementation AssignedListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"%@",_assignedListArray);
    
    NSLog(@"%@",_assignmentObj);
    
    _assignedToLbl.text = _assignedText;
    
    self.view.frame = CGRectMake(0,0, DEVICE_SIZE.width, DEVICE_SIZE.height);
    
    if (IS_IPHONE_4) {
        self.view.frame = CGRectMake(0,-30, DEVICE_SIZE.width, DEVICE_SIZE.height);

    }
    
    if ([[UIScreen mainScreen]bounds].size.height >= 568) {
        
       // _listView.frame =CGRectMake(0, 0, 260, 425);
        
       // _listView.center = self.view.center;
        
        _listView.center = CGPointMake(self.view.center.x, self.view.center.y -30);
        
    }
   
    
//    _assignedTableView.center = _listView.center;
    
    //self.view.frame = CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    
    _assignedTableView.delegate = self;
    _assignedTableView.dataSource = self;
    
    [self.assignedTableView setTableFooterView:[UIView new]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self.view removeFromSuperview];
    
    [self removeFromParentViewController];
    
}

-(void)showInViewController:(UIViewController *)viewController{
    
    [viewController addChildViewController:self];
    
    [viewController.view addSubview:self.view];
    
    [self didMoveToParentViewController:viewController];
    
}

- (IBAction)okBtnClk:(id)sender {
    
    [self.view removeFromSuperview];
    
    [self removeFromParentViewController];
    
}


#pragma mark - TableView Delegate & DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _assignedListArray.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    //AssignedDetails *assignee = (AssignedDetails *)[_assignedListArray objectAtIndex:indexPath.row];
    
    //cell.textLabel.text = assignee.assigneeName;
    
    cell.textLabel.text = [_assignedListArray objectAtIndex:indexPath.row];
    
    UIFont *myFont = [ UIFont fontWithName:APP_FONT size: 16.0 ];
    
    cell.textLabel.font  = myFont;
    
    cell.textLabel.adjustsFontSizeToFitWidth=YES;
    
    cell.textLabel.minimumScaleFactor=0.7;
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}




@end
