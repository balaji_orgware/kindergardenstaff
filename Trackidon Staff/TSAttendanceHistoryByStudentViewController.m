//
//  TSAttendanceHistoryByStudentViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 31/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSAttendanceHistoryByStudentViewController.h"

@interface TSAttendanceHistoryByStudentViewController (){
    
    BOOL isFromDateTapped;
    
    Board *boardObj;
    
    NSArray *boardArray;
    
    NSDate *fromDate,*toDate;
    
    NSMutableArray *studentsListArray,*dateTableArray;
    
    NSString *selectedBoardTransID,*selectedClassTransID,*selectedSectionTransID,*selectedStudentTransID, *attendanceStatusType;
    
    TSSelectionViewController *selectionView;
}
@end

@implementation TSAttendanceHistoryByStudentViewController

#pragma mark - View lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    isFromDateTapped = YES;
    
    fromDate = [self startOfMonth:[NSDate date]];
    
    toDate = [NSDate date];
    
    studentsListArray = [NSMutableArray new];
    
    NSPredicate *boardPredicate = [NSPredicate predicateWithFormat:@"schoolTransId contains[cd] %@ AND isViewableByStaff == 1",SINGLETON_INSTANCE.selectedSchoolTransId];
    
    boardArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Board" withPredicate:boardPredicate];
    
    if (IS_IPHONE_4) {
        
        _studentSelectionView.frame = CGRectMake(_studentSelectionView.frame.origin.x, _studentSelectionView.frame.origin.y, DeviceWidth, 45);
        
        _selectStudentTxtFld.frame = CGRectMake(0, 0, _studentSelectionView.frame.size.width, _studentSelectionView.frame.size.height);
        
        _selectStudentTxtFld.font = [UIFont fontWithName:APP_FONT size:18];
        
        _dateSelectionView.frame = CGRectMake(_dateSelectionView.frame.origin.x, _studentSelectionView.frame.origin.y+_studentSelectionView.frame.size.height, DeviceWidth, _dateSelectionView.frame.size.height);
        
        _totalNoOfDaysView.frame = CGRectMake(_totalNoOfDaysView.frame.origin.x, _dateSelectionView.frame.origin.y+_dateSelectionView.frame.size.height, DeviceWidth, 45);
        
        _collectionView.frame = CGRectMake(_collectionView.frame.origin.x, _totalNoOfDaysView.frame.origin.y+_totalNoOfDaysView.frame.size.height, DeviceWidth, DeviceHeight - (_totalNoOfDaysView.frame.origin.y+_totalNoOfDaysView.frame.size.height));
    }
    
    _submitButton.layer.cornerRadius = _submitButton.frame.size.height/2;
    
    _submitButton.layer.masksToBounds = YES;
    
    [self changeDateWithDate:fromDate andFromDateCheck:YES];
    
    [self changeDateWithDate:toDate andFromDateCheck:NO];
    
    [self.blackView setHidden:YES];
    
    [self.dateTableView setHidden:YES];
    
    {
        [SINGLETON_INSTANCE addDropDownArrowToTextField:_selectBoardTxtFld];
        
        [SINGLETON_INSTANCE addDropDownArrowToTextField:_selectClassTxtFld];
        
        [SINGLETON_INSTANCE addDropDownArrowToTextField:_selectSectionTxtFld];
        
        [SINGLETON_INSTANCE addDropDownArrowToTextField:_selectStudentTxtFld];
        
        [SINGLETON_INSTANCE addLeftInsetToTextField:_selectBoardTxtFld];
        
        [SINGLETON_INSTANCE addLeftInsetToTextField:_selectClassTxtFld];
        
        [SINGLETON_INSTANCE addLeftInsetToTextField:_selectSectionTxtFld];

        [SINGLETON_INSTANCE addLeftInsetToTextField:_selectStudentTxtFld];

        [TSSingleton layerDrawForView:_selectBoardTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];

        [TSSingleton layerDrawForView:_selectClassTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];

        [TSSingleton layerDrawForView:_selectSectionTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        
        [TSSingleton layerDrawForView:_studentSelectionView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];

        [TSSingleton layerDrawForView:_dateSelectionView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
        
        [TSSingleton layerDrawForView:_selectClassTxtFld position:LAYER_RIGHT color:[UIColor lightGrayColor]];
    }
}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_ATTENDANCEVIEWHISTORY_BYSTUDENT parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Attendance History";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Touches Began Methods

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self hideDateTableView];
}

#pragma mark - Helper Methods

-(void) clearView{
    
    fromDate = [self startOfMonth:[NSDate date]];
    
    toDate = [NSDate date];
    
    [self changeDateWithDate:fromDate andFromDateCheck:YES];
    
    [self changeDateWithDate:toDate andFromDateCheck:NO];
    
    [_submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    
    _totalWorkingDays = 0;
    _totalTardy = 0;
    _totalAbsent = 0;
    _totalPresent = 0;
    
    [self.collectionView reloadData];
}

-(void) hideDateTableView{
    
    [UIView animateWithDuration:0.2 animations:^{
        
        _blackView.alpha = 0.0;
        
        _dateTableView.frame = CGRectMake(_dateTableView.frame.origin.x, DeviceHeight, _dateTableView.frame.size.width, _dateTableView.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        _blackView.hidden = YES;
        
        _dateTableView.hidden = YES;
    }];
}

-(void) showDateTableView{
    
    _blackView.alpha = 0.0;
    
    _blackView.hidden = NO;
    
    _dateTableView.hidden = NO;
    
    [self.dateTableView reloadData];
    
    [UIView animateWithDuration:0.2 animations:^{
        
        _blackView.alpha = 0.5;
        
        _dateTableView.center = CGPointMake(self.view.center.x, self.view.center.y-66);
    }];
}

// Gives Start date of the month
-(NSDate *) startOfMonth :(NSDate *)date
{
    NSCalendar * calendar = [NSCalendar currentCalendar];
    
    NSDateComponents * currentDateComponents = [calendar components: NSYearCalendarUnit | NSMonthCalendarUnit fromDate: date];
    
    NSDate * startOfMonth = [calendar dateFromComponents: currentDateComponents];
    
    return startOfMonth;
}

-(void) changeDateWithDate:(NSDate *)date andFromDateCheck:(BOOL)boolCheck{
    
    if (boolCheck){ // From Date
        
        _fromDateNoLabel.text = [SINGLETON_INSTANCE stringFromDate:date withFormate:@"dd"];
    
        _fromDateMonYrLabel.text = [SINGLETON_INSTANCE stringFromDate:date withFormate:@"MMM, yyyy"];
    }
    else{ // To date
        
        _toDateNoLabel.text = [SINGLETON_INSTANCE stringFromDate:date withFormate:@"dd"];
        
        _toDateMonYrLabel.text = [SINGLETON_INSTANCE stringFromDate:date withFormate:@"MMM, yyyy"];
    }
}
#pragma mark - Button Actions

- (IBAction)fromDateButtonTapped:(id)sender {
    
    [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_CLICKED_FROMDATE parameter:[NSMutableDictionary new]];
    
    [self clearView];
    
    isFromDateTapped = YES;
    
    TSDatePickerViewController *datePickerVC = [[TSDatePickerViewController alloc]initWithNibName:@"TSDatePickerViewController" bundle:nil];
    
    datePickerVC.delegate = self;
    
    [datePickerVC showInViewController:self calendarType:CALENDAR_TYPE_DATE withMinDate:nil withMaxDate:toDate];
}

- (IBAction)toDateButtonTapped:(id)sender {
    
    [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_CLICKED_TODATE parameter:[NSMutableDictionary new]];
    
    [self clearView];

    isFromDateTapped = NO;
    
    TSDatePickerViewController *datePickerVC = [[TSDatePickerViewController alloc]initWithNibName:@"TSDatePickerViewController" bundle:nil];
    
    datePickerVC.delegate = self;
    
    [datePickerVC showInViewController:self calendarType:CALENDAR_TYPE_DATE withMinDate:fromDate withMaxDate:[NSDate date]];
}

- (IBAction)submitButtonTapped:(id)sender {
    
    if ([_submitButton.titleLabel.text isEqualToString:@"Submit"]) {
        
        if (_selectBoardTxtFld.text.length == 0){
            
            [TSSingleton showAlertWithMessage:@"Select board"];
            
            return;
        }
        else if (_selectClassTxtFld.text.length == 0){
            
            [TSSingleton showAlertWithMessage:@"Select Class"];
            
            return;
        }
        else if (_selectSectionTxtFld.text.length == 0){
            
            [TSSingleton showAlertWithMessage:@"Select Section"];
            
            return;
        }
        else if (_selectStudentTxtFld.text.length == 0){
            
            [TSSingleton showAlertWithMessage:@"Select Student"];
            
            return;
        }
        
        [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_CLICKED_SUBMIT parameter:[NSMutableDictionary new]];
        
        [self fetchAttendanceHistoryAPICall];
    }
    else{
        
        [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_CLICKED_CLEAR parameter:[NSMutableDictionary new]];

        [self clearView];
    }
}

#pragma mark - Date Selection Delegate methods

-(void)selectedDate:(NSDate *)date{
    
    if (isFromDateTapped) {
        
        fromDate = date;
        
        [self changeDateWithDate:date andFromDateCheck:YES];
    }
    else{
        
        toDate = date;
        
        [self changeDateWithDate:date andFromDateCheck:NO];
    }
}

#pragma mark - Selection View

-(void) showSelectionViewWithSelected:(NSString *)selected andTextField:(UITextField *)textField{
    
    if (!selectionView) {
        
        selectionView = [[TSSelectionViewController alloc]initWithNibName:@"TSSelectionViewController" bundle:nil];
        
        selectionView.delegate = self;
        
        NSMutableDictionary *contentDict = [NSMutableDictionary new];
        
        if (textField == _selectBoardTxtFld) {
            
            for (Board *board in boardArray) {
                
                [contentDict setObject:board.boardTransId     forKey:board.boardName];
            }
        }
        else if (textField == _selectClassTxtFld){
            
            NSPredicate *filterPred = [NSPredicate predicateWithFormat:@"boardTransId MATCHES %@",selectedBoardTransID];
            
            NSArray *filteredArray = [NSMutableArray arrayWithArray: [boardArray filteredArrayUsingPredicate:filterPred]];
            
            boardObj = [filteredArray objectAtIndex:0];
            
            for (Classes *classObj in boardObj.classes) {
                
                [contentDict setObject:classObj.classTransId forKey:classObj.classStandardName];
            }
            
        }
        else if(textField == _selectSectionTxtFld){
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"classTransId MATCHES %@",selectedClassTransID];
            
            NSSet *filteredSet = [boardObj.classes filteredSetUsingPredicate:predicate];
            
            Classes *classObj = [[filteredSet allObjects] objectAtIndex:0];
            
            for (Section *secObj in classObj.section) {
                
                [contentDict setObject:secObj.sectionTransId forKey:secObj.sectionName];
            }
        }
        else if (textField == _selectStudentTxtFld){
            
            for (StudentModel *studentObj in studentsListArray) {
                
                [contentDict setObject:studentObj.studentTransID forKey:studentObj.studentName];
            }
        }
        
        if (contentDict.count>0) {
            
            [selectionView setContentDict:contentDict];
            
            selectionView.selectedValue = textField.text.length>0? textField.text:@"";
            
            [selectionView showSelectionViewController:self forTextField:textField];
        }
        else{
            
            if (textField == _selectBoardTxtFld) {
                
                [TSSingleton showAlertWithMessage:@"No Boards to choose"];
            }
            else if (textField == _selectClassTxtFld){
                
                [TSSingleton showAlertWithMessage:@"No Classes to choose"];
            }
            else if (textField == _selectSectionTxtFld){
                
                [TSSingleton showAlertWithMessage:@"No Sections to choose"];
            }
            else if (textField == _selectStudentTxtFld){
                
                [TSSingleton showAlertWithMessage:@"No Students to choose"];
            }
            
            [selectionView removeSelectionViewController:self];
            
            selectionView = nil;
        }
    }
}

#pragma mark - Selection View Delegate Methods

-(void)selectedValueFromDict:(NSDictionary *)contentDict withSelectedValue:(NSString *)selectedValue andTransID:(NSString *)transID forTextField:(UITextField *)textField{
    
    if (textField == _selectBoardTxtFld) {
        
        selectedBoardTransID = transID;
    }
    else if (textField == _selectClassTxtFld){
        
        selectedClassTransID = transID;
    }
    else if (textField == _selectSectionTxtFld){
        
        selectedSectionTransID = transID;
        
        [self fetchStudentsListAPICall];
    }
    else if (textField == _selectStudentTxtFld){
        
        selectedStudentTransID = transID;
    }
    
    selectionView = nil;
}

-(void)backViewTappedOn:(UIViewController *)viewController{
    
    selectionView = nil;
}


#pragma mark - Textfield Delegate methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField == _selectBoardTxtFld) {
        
        selectedClassTransID = nil;
        
        selectedSectionTransID = nil;
        
        selectedStudentTransID = nil;
        
        _selectClassTxtFld.text = @"";
        
        _selectSectionTxtFld.text = @"";
        
        _selectStudentTxtFld.text = @"";
        
        [self showSelectionViewWithSelected:textField.text andTextField:textField];
        
//        _selectBoardTxtFld.text = @"";
    }
    else if (textField == _selectClassTxtFld) {
        
        if (_selectBoardTxtFld.text.length>0) {
            
            selectedSectionTransID = nil;
            
            selectedStudentTransID = nil;
            
            _selectSectionTxtFld.text = @"";
            
            _selectStudentTxtFld.text = @"";
            
            [self showSelectionViewWithSelected:textField.text andTextField:textField];
            
//            _selectClassTxtFld.text = @"";
        }
    }
    else if(textField == _selectSectionTxtFld){
        
        if (_selectBoardTxtFld.text.length>0 && _selectClassTxtFld.text.length >0) {
            
            selectedStudentTransID = nil;
            
            _selectStudentTxtFld.text = @"";
            
            [self showSelectionViewWithSelected:textField.text andTextField:textField];
            
//            _selectSectionTxtFld.text = @"";
        }
    }
    else{
        
        if (_selectBoardTxtFld.text.length>0 && _selectClassTxtFld.text.length >0 && _selectSectionTxtFld.text.length>0) {
            
            [self showSelectionViewWithSelected:textField.text andTextField:textField];
            
//            _selectStudentTxtFld.text = @"";
        }
    }
    
    [self clearView];
    
    return NO;
}

#pragma mark - Tableview Datasources & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return dateTableArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sCellIdentifier = @"tableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier];
    
    cell.textLabel.text = [dateTableArray objectAtIndex:indexPath.row];
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    return attendanceStatusType;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 44;
}

#pragma mark - Collection view Datasources & Delegates

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 3;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sCellIdentifier = @"cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:sCellIdentifier forIndexPath:indexPath];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:1000];
    
    UILabel *countLabel = (UILabel *)[cell viewWithTag:1001];
    
    _noOfDaysLabel.text = [NSString stringWithFormat:@"%ld",(long)_totalWorkingDays];
    
    switch (indexPath.row) {
            
        case ATTENDANCE_PRESENT:
            
            imageView.image = [UIImage imageNamed:@"Present_cal"];
            countLabel.textColor = COLOR_ACCEPT_CREATEDTODAY;
            countLabel.text = [NSString stringWithFormat:@"%ld",(long)_totalPresent];
            break;
            
        case ATTENDANCE_ABSENT:
            
            imageView.image = [UIImage imageNamed:@"absent_cal"];
            countLabel.textColor = COLOR_DECLINED_DUESTODAY;
            countLabel.text = [NSString stringWithFormat:@"%ld",(long)_totalAbsent];
            break;
            
        case ATTENDANCE_LATE:
            
            imageView.image = [UIImage imageNamed:@"tardy_cal"];
            countLabel.textColor = COLOR_PENDING_VIEWALL;
            countLabel.text = [NSString stringWithFormat:@"%ld",(long)_totalTardy];
            break;
            
        default:
            break;
    }
    
    cell.layer.borderWidth = layerThickness;
    
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    UILabel *countLabel = (UILabel *)[cell viewWithTag:1001];
    
    if ([countLabel.text integerValue]>0) {
        
        switch (indexPath.row) {
                
            case ATTENDANCE_PRESENT:
            {
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_ATTENDANCE_PRESENT forKey:ANALYTICS_PARAM_ATTENDANCE_STATUS];
                
                [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_CLICKED_LISTOFDAYS parameter:analyticsDict];
                
                [self fetchHistoryAttendanceDatesWithSelectedStatus:@"1"];
                break;
            }
            case ATTENDANCE_ABSENT:
            {
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_ATTENDANCE_ABSENT forKey:ANALYTICS_PARAM_ATTENDANCE_STATUS];
                
                [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_CLICKED_LISTOFDAYS parameter:analyticsDict];
                
                [self fetchHistoryAttendanceDatesWithSelectedStatus:@"2"];
                break;
            }
            case ATTENDANCE_LATE:
            {
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_ATTENDANCE_TARDY forKey:ANALYTICS_PARAM_ATTENDANCE_STATUS];
                
                [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_CLICKED_LISTOFDAYS parameter:analyticsDict];
                
                [self fetchHistoryAttendanceDatesWithSelectedStatus:@"3"];
                break;
            }
            default:
                break;
        }
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(DeviceWidth/3, DeviceWidth/3);
}

#pragma mark - Service Calls

// Service call to fetch Students list
-(void) fetchStudentsListAPICall{
    
    NSString *urlString = @"MastersMServices/StudentsMService.svc/FetchStudentsByScectionList";
    
    NSDictionary *params = @{@"SectionList":@[selectedSectionTransID]};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchStudentsByScectionListResult"][@"ResponseCode"] intValue] ==1) {
                
                [studentsListArray removeAllObjects];
                
                for (NSDictionary *dict in responseDictionary[@"FetchStudentsByScectionListResult"][@"StudentsMClass"]) {
                    
                    StudentModel *studentObj = [StudentModel new];
                    studentObj.classSection = dict[@"ClassSection"];
                    studentObj.enrollNo = dict[@"EnrollNo"];
                    studentObj.studentName = dict[@"StudentName"];
                    studentObj.secondLangTransID = dict[@"SecondLangTransID"];
                    studentObj.studentTransID = dict[@"TransID"];
                    
                    [studentsListArray addObject:studentObj];
                    
                    studentObj = nil;
                }
            }
        }
    }];
}

// Fetch Attendance History API call
-(void) fetchAttendanceHistoryAPICall{

    NSString *urlString = @"MastersMServices/AttendanceService.svc/FetchAttendanceByStudent";
    
    NSString *fromDateStr = [SINGLETON_INSTANCE stringFromDate:fromDate withFormate:DATE_FORMATE_SERVICE_CALL];
    
    NSString *toDateStr = [SINGLETON_INSTANCE stringFromDate:toDate withFormate:DATE_FORMATE_SERVICE_CALL];
    
    NSDictionary *params = @{@"guidBoardTransID":selectedBoardTransID,@"guidStudentsMTransID":selectedStudentTransID,@"strFromDate":fromDateStr,@"strTodate":toDateStr,@"guidSchoolTransID":SINGLETON_INSTANCE.selectedSchoolTransId};

    [[TSHandlerClass sharedHandler] postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchAttendanceByStudentResult"][@"ResponseCode"] intValue] == 1) {
    
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:_selectBoardTxtFld.text forKey:ANALYTICS_PARAM_BOARDNAME];
                
                [analyticsDict setValue:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_SEARCH_HISTORYBYSTUDENT parameter:analyticsDict];
                
                NSDictionary *dict = responseDictionary[@"FetchAttendanceByStudentResult"][@"AttendanceClassData"];
                
                _totalPresent = [dict[@"TotalPresntDays"] integerValue];
                _totalAbsent = [dict[@"TotalAbsentDays"] integerValue];
                _totalTardy = [dict[@"TotalLateDays"] integerValue];
                _totalWorkingDays = [dict[@"TotalWorkingDays"] integerValue];
                
                [_collectionView reloadData];
                
                [_submitButton setTitle:@"Clear" forState:UIControlStateNormal];
            }
            else{
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:_selectBoardTxtFld.text forKey:ANALYTICS_PARAM_BOARDNAME];
                
                [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_SEARCH_HISTORYBYSTUDENT parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
            }
        }
        else{
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            [analyticsDict setValue:_selectBoardTxtFld.text forKey:ANALYTICS_PARAM_BOARDNAME];
            
            [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
            
            [TSAnalytics logEvent:ANALYTICS_ATTENDANCE_SEARCH_HISTORYBYSTUDENT parameter:analyticsDict];
        }
    }];
}

// API to fetch Attendance history dates with selected type
-(void) fetchHistoryAttendanceDatesWithSelectedStatus:(NSString*)statusType{
    
    NSString *urlString = @"MastersMServices/AttendanceService.svc/FetchAttendenceRequest";
    
    NSString *fromDateStr = [SINGLETON_INSTANCE stringFromDate:fromDate withFormate:DATE_FORMATE_SERVICE_CALL];
    
    NSString *toDateStr = [SINGLETON_INSTANCE stringFromDate:toDate withFormate:DATE_FORMATE_SERVICE_CALL];
    
    NSDictionary *params = @{@"guidStudentsMTransID":selectedStudentTransID,@"strFromDate":fromDateStr,@"strTodate":toDateStr,@"guidSchoolTransID":SINGLETON_INSTANCE.selectedSchoolTransId,@"intRequestOn":statusType};
    
    if ([statusType intValue] == 1)
        attendanceStatusType = @"Present Days";
    else if ([statusType intValue] == 2)
        attendanceStatusType = @"Absent Days";
    else
        attendanceStatusType = @"Late Days";
        
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchAttendenceRequestResult"][@"ResponseCode"] intValue] == 1) {
                
                dateTableArray = [NSMutableArray new];
                
                for (NSDictionary *dict in responseDictionary[@"FetchAttendenceRequestResult"][@"AttendanceClassData"]) {
                    
                    [dateTableArray addObject:dict[@"Date"]];
                }
                
                [self showDateTableView];
            }
            else{
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"FetchAttendenceRequestResult"][@"ResponseMessage"]];
            }
        }
    }];
}
@end