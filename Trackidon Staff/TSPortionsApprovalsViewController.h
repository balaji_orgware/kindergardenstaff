//
//  TSPortionsApprovalsViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 18/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSPortionsApprovalsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *portionsTableView;
@property (weak, nonatomic) IBOutlet UIView *selectionView;

// Button Actions
- (IBAction)approvedButtonTapped:(id)sender;
- (IBAction)declineButtonTapped:(id)sender;

@end
