//
//  TSHolidayDescriptionViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/30/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Holidays;

@interface TSHolidayDescriptionViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topView;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (weak, nonatomic) IBOutlet UIView *dateView;

@property (weak, nonatomic) IBOutlet UILabel *fromDateLbl;

@property (weak, nonatomic) IBOutlet UILabel *toDateLbl;

@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

@property (weak, nonatomic) IBOutlet UILabel *remainingCountLbl;

@property (weak, nonatomic) IBOutlet UILabel *daysLeftLbl;

@property (weak, nonatomic) IBOutlet UILabel *fromLbl;

@property (weak, nonatomic) IBOutlet UILabel *toLbl;

@property (weak, nonatomic) Holidays *holidaysObj;








@end
