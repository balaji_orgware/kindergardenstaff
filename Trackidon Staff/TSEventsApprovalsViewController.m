//
//  TSEventsApprovalsViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 02/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSEventsApprovalsViewController.h"

@interface TSEventsApprovalsViewController (){
    
    NSMutableArray *eventsArray,*eventsModelArray;
    
    UIBarButtonItem *selectAllBtn;
    
    UILongPressGestureRecognizer *longPressGesture;
    
    TSSelectionViewController *selectionVC;
    
    NSArray *attachmentImgArray;

}
@end

@implementation ApprovalsModel

@end

@implementation TSEventsApprovalsViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
        
    attachmentImgArray = @[@"one",@"two",@"three",@"four",@"five"];
    
    eventsArray = [NSMutableArray new];
    
    eventsModelArray = [NSMutableArray new];
    
    selectAllBtn = [[UIBarButtonItem alloc]initWithTitle:@"Select All" style:UIBarButtonItemStyleDone target:self action:@selector(selectAllAction)];

    longPressGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressGestureAction:)];
    
    longPressGesture.delegate = self;
    
    [self.eventsTableView addGestureRecognizer:longPressGesture];
    
    [self.eventsTableView setTableFooterView:[UIView new]];
    
    self.selectionView.frame = CGRectMake(self.selectionView.frame.origin.x, DeviceHeight+64, self.selectionView.frame.size.width, self.selectionView.frame.size.height);
    
    [self categorizeEvents];
    
    [TSAnalytics logEvent:ANALYTICS_EVENTAPPROVALS_OPENED_APPROVALS parameter:[NSMutableDictionary new]];
}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_APPROVALEVENT_LIST parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@ Log",SINGLETON_INSTANCE.barTitle];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

#pragma mark - Button Actions

- (IBAction)approvedButtonTapped:(id)sender {
    
    NSPredicate *approvePredicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];

    NSArray *approvedArray = [eventsModelArray filteredArrayUsingPredicate:approvePredicate];

    if (approvedArray.count>0) {
        
        UIAlertView *approveAlertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:[NSString stringWithFormat:@"Do you want to Approve these %ld events?",(unsigned long)approvedArray.count] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        
        approveAlertView.tag = APPROVAL_TYPE_APPROVE;
        
        approveAlertView.delegate = self;
        
        [approveAlertView show];
    }
}

- (IBAction)declineButtonTapped:(id)sender {
   
    NSPredicate *declinePredicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *declinedArray = [eventsModelArray filteredArrayUsingPredicate:declinePredicate];
    
    if (declinedArray.count>0) {
        
        UIAlertView *declineAlertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:[NSString stringWithFormat:@"Do you want to Decline these %ld events?",(unsigned long)declinedArray.count] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        
        declineAlertView.tag = APPROVAL_TYPE_DECLINE;
        
        declineAlertView.delegate = self;
        
        [declineAlertView show];
    }
}

#pragma mark - Alert View Delegates

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == APPROVAL_TYPE_APPROVE) {
        
        if (buttonIndex) {
            
            NSPredicate *approvePredicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *approvedArray = [eventsModelArray filteredArrayUsingPredicate:approvePredicate];
            
            NSMutableArray *eventsTransIDArr = [NSMutableArray new];
            
            for (ApprovalsModel *modelObj in approvedArray) {
                
                [eventsTransIDArr addObject:modelObj.transID];
            }
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidUserTransID"];
            
            [param setValue:[NSString stringWithFormat:@"%ld",(long)APPROVAL_TYPE_APPROVE] forKey:@"intApprovedStatus"];
            
            [param setValue:eventsTransIDArr forKey:@"guidEventTransID"];
            
            NSDictionary *updateParam = @{@"ApproveEvents":param};
            
            [self updateEventsWithParam:updateParam withApprovalType:APPROVAL_TYPE_APPROVE];
        }
    }
    else if (alertView.tag == APPROVAL_TYPE_DECLINE){
        
        if (buttonIndex) {
                
            NSPredicate *declinePredicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *declinedArray = [eventsModelArray filteredArrayUsingPredicate:declinePredicate];
            
            NSMutableArray *eventsTransIDArr = [NSMutableArray new];
            
            for (ApprovalsModel *modelObj in declinedArray) {
                
                [eventsTransIDArr addObject:modelObj.transID];
            }
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidUserTransID"];
            
            [param setValue:[NSString stringWithFormat:@"%ld",(long)APPROVAL_TYPE_DECLINE] forKey:@"intApprovedStatus"];
            
            [param setValue:eventsTransIDArr forKey:@"guidEventTransID"];
            
            NSDictionary *updateParam = @{@"ApproveEvents":param};
            
            [self updateEventsWithParam:updateParam withApprovalType:APPROVAL_TYPE_DECLINE];
        }
    }
    
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}


#pragma mark - Helper methods

// Select All Button Action
-(void) selectAllAction{
    
    for (ApprovalsModel *modelObj in eventsModelArray) {
        
        modelObj.isSelected = YES;
    }
    
    self.navigationItem.title = [NSString stringWithFormat:@"%ld   Selected",(unsigned long)eventsModelArray.count];
    
    [self.eventsTableView reloadData];
}

// Method to Show/hide Selection View
-(void) animateSelectionView{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *filteredArray = [eventsModelArray filteredArrayUsingPredicate:predicate];
    
    if (filteredArray.count>0) {
        
        NSLog(@"%@",self.selectionView);
        
        if(self.selectionView.frame.origin.y >= DeviceHeight){
            
            [UIView animateWithDuration:0.2f animations:^{
                
                self.eventsTableView.frame = CGRectMake(self.eventsTableView.frame.origin.x, self.eventsTableView.frame.origin.y, self.eventsTableView.frame.size.width, DeviceHeight - 64 - self.selectionView.frame.size.height);
                
                self.selectionView.frame = CGRectMake(self.selectionView.frame.origin.x, self.eventsTableView.frame.origin.y+self.eventsTableView.frame.size.height, self.selectionView.frame.size.width, self.selectionView.frame.size.height);
            }];
        }
        
        self.navigationItem.rightBarButtonItem = selectAllBtn;
        
        self.navigationItem.title = [NSString stringWithFormat:@"%ld   Selected",(unsigned long)filteredArray.count];
    }
    else{
        
        self.navigationItem.rightBarButtonItem = nil;
        
        self.navigationItem.title = @"Event Approval";
        
        if (self.selectionView.frame.origin.y < DeviceHeight) {
            
            [UIView animateWithDuration:0.2f animations:^{
                
                self.eventsTableView.frame = CGRectMake(self.eventsTableView.frame.origin.x, self.eventsTableView.frame.origin.y, self.eventsTableView.frame.size.width, DeviceHeight-64);
                
                self.selectionView.frame = CGRectMake(self.selectionView.frame.origin.x, DeviceHeight, self.selectionView.frame.size.width, self.selectionView.frame.size.height);
            }];
        }
    }
}

// Long press Gesture Action
-(void)longPressGestureAction:(UILongPressGestureRecognizer *)sender{
    
    CGPoint touchPoint = [sender locationOfTouch:0 inView:self.eventsTableView];
    
    NSIndexPath *indexPath = [_eventsTableView indexPathForRowAtPoint:touchPoint];
    
    if (indexPath != nil && sender.state == UIGestureRecognizerStateBegan) {
        
        ApprovalsModel *modelObj = [eventsModelArray objectAtIndex:indexPath.row];
        
        modelObj.isSelected = !modelObj.isSelected;
        
        [self.eventsTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        [self animateSelectionView];
    }
    
}

// Post Notification Method to Update Tableview from TSEventApprovalDescViewController
-(void) updateTableView{
    
    [self categorizeEvents];
    
//    [self fetchUnapprovedEventsAPICall];
}

// Method to Clear Approve/Decline Selection View
-(void) clearSelectionView{
    
    [UIView animateWithDuration:0.2f animations:^{
        
        self.navigationItem.title = @"Event Approval";
        
        self.navigationItem.rightBarButtonItem = nil;
        
        self.eventsTableView.frame = CGRectMake(self.eventsTableView.frame.origin.x, self.eventsTableView.frame.origin.y, self.eventsTableView.frame.size.width, DeviceHeight-64);
        
        self.selectionView.frame = CGRectMake(self.selectionView.frame.origin.x, DeviceHeight, self.selectionView.frame.size.width, self.selectionView.frame.size.height);
        
        self.navigationItem.rightBarButtonItem = nil;
    }];
}
/*
#pragma mark - Selection View

-(void) showSelectionViewWithSelectedText:(NSString *)selected andTextField:(UITextField *)textField{
        
    if (selectionVC == nil) {
        
        selectionVC = [[TSSelectionViewController alloc]initWithNibName:@"TSSelectionViewController" bundle:nil];
        
        selectionVC.delegate = self;
        
        NSMutableDictionary *contentDict = [NSMutableDictionary new];
        
        for (Board *board in boardArray) {
            
            [contentDict setValue:board.boardTransId forKey:board.boardName];
        }
        
        [selectionVC setContentDict:contentDict];
        
        [selectionVC setSelectedValue:selected.length>0?selected:@""];
        
        [selectionVC showSelectionViewController:self forTextField:textField];
    }
    else{
        
        selectionVC = nil;
    }
}

// MARK: Selection Callback Delegate

-(void)selectedValueFromDict:(NSDictionary *)contentDict withSelectedValue:(NSString *)selectedValue andTransID:(NSString *)transID forTextField:(UITextField *)textField{
    
    selectedBoardTransID = transID;
    
    [self clearSelectionView];
    
    [self categorizeEvents];
    
    [self fetchUnapprovedEventsAPICall];
    
    selectionVC = nil;
}

-(void)backViewTappedOn:(UIViewController *)viewController{
    
    selectionVC = nil;
}
*/
#pragma mark - Tableview Datasources & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return eventsArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sCellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier];
    
    UILabel *eventTitleLabel = (UILabel *)[cell viewWithTag:100];
    
    UILabel *eventStatusLabel = (UILabel *)[cell viewWithTag:101];
    
    UILabel *eventDateLabel = (UILabel *)[cell viewWithTag:102];
    
    UILabel *eventBoardLabel = (UILabel *)[cell viewWithTag:104];
    
    UIImageView *attachmentImgVw = (UIImageView *)[cell viewWithTag:103];

    Events *eventObj = [eventsArray objectAtIndex:indexPath.row];
    
    eventTitleLabel.text = [eventObj.eventTitle capitalizedString];
    
    eventDateLabel.text = [NSString stringWithFormat:@"%@ - %@",[SINGLETON_INSTANCE stringFromDate:eventObj.eventFromDate withFormate:@"MMMM dd"],[SINGLETON_INSTANCE stringFromDate:eventObj.eventToDate withFormate:DATE_FORMATE_MNTH_DAY_YR]];
    
    eventStatusLabel.text = [NSString stringWithFormat:@"Created by %@",eventObj.userCreatedName];
    
    eventBoardLabel.text = eventObj.boardName;
    
    ApprovalsModel *modelObj = [eventsModelArray objectAtIndex:indexPath.row];
    
    if (!modelObj.isSelected) {
        
        [cell setBackgroundColor:[UIColor whiteColor]];
    }
    else{
        
        [cell setBackgroundColor:[UIColor lightGrayColor]];
    }
    
    [tableView setTableFooterView:[UIView new]];
    
    if ([eventObj.eventIsHasAttachment intValue]==1) {
        
        attachmentImgVw.image = [UIImage imageNamed:[attachmentImgArray objectAtIndex:indexPath.row%5]];
        
        attachmentImgVw.hidden = NO;
    }
    else{
        
        attachmentImgVw.hidden = YES;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [TSAnalytics logEvent:ANALYTICS_EVENTAPPROVALS_CLICKED_APPROVALSINLIST parameter:[NSMutableDictionary new]];

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateTableView) name:@"updateTableView" object:nil];
    
    TSEventApprovalDescViewController *eventApprovalDescVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBEventApprovalDesc"];
    
    [eventApprovalDescVC setEventObj:[eventsArray objectAtIndex:indexPath.row]];

    [self.navigationController pushViewController:eventApprovalDescVC animated:YES];
}
#pragma mark - Categorize Events

-(void) categorizeEvents{
    
    [eventsArray removeAllObjects];

    [eventsModelArray removeAllObjects];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId MATCHES %@ AND isForApprovals == 1",SINGLETON_INSTANCE.staffTransId];
    
    eventsArray = [NSMutableArray arrayWithArray:[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Events" withPredicate:predicate]];
    
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:NO];
    
    eventsArray = [[eventsArray sortedArrayUsingDescriptors:@[dateDescriptor]] mutableCopy];
    
    
    for (Events *eventObj in eventsArray) {
        
        ApprovalsModel *modelObj = [ApprovalsModel new];
        modelObj.title = eventObj.eventTitle;
        modelObj.transID = eventObj.eventTransId;
        modelObj.isSelected = NO;
        modelObj.approvalStatus = [eventObj.approvedStatus integerValue];
        
        [eventsModelArray addObject:modelObj];
    }
    
    if(eventsArray.count>0){
        
        [self.eventsTableView setHidden:NO];
    }
    else{
        
        [self.eventsTableView setHidden:YES];
    }
    
    [self.eventsTableView reloadData];
}

#pragma mark - Service Calls

/*
// Fetch Unapproved Events Service Call
-(void) fetchUnapprovedEventsAPICall{
    
    NSString *urlString = @"MastersMServices/EventsMService.svc/FetchUnApprovedEvents";
    
    NSDictionary *params = @{@"intEventTypeID":@"26",@"guidBoardTransID":selectedBoardTransID};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchUnApprovedEventsResult"][@"ResponseCode"] intValue] == 1) {
                
                [[Events instance]saveEventsWithResponseDict:responseDictionary andIsForApprovals:YES withBoardTransID:selectedBoardTransID];
                
                [self categorizeEvents];
            }
        }
    }];
}
*/
// Update Events Service Call
-(void) updateEventsWithParam:(NSDictionary *)param withApprovalType:(NSInteger)approvalType{
    
    NSString *urlString = @"MastersMServices/EventsMService.svc/MobileApproveEvents";
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"MobileApproveEventsResult"][@"ResponseCode"] integerValue] == 1) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary new];
                
                if (approvalType == APPROVAL_TYPE_APPROVE) {
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                else{
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                
                [analyticsDict setValue:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_EVENTAPPROVALS_CLICKED_BULKAPPROVALS parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"MobileApproveEventsResult"][@"ResponseMessage"]];                
                
                NSArray *eventTransIDArr = param[@"ApproveEvents"][@"guidEventTransID"];

                for (NSString *eventTransID in eventTransIDArr) {
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId MATCHES %@ AND isForApprovals == 1 AND eventTransId MATCHES %@",SINGLETON_INSTANCE.staffTransId,eventTransID];
                    
                    [SINGLETON_INSTANCE deleteSelectedObjectsInEntityWithEntityName:@"Events" withPredicate:predicate withContext:[APPDELEGATE_INSTANCE managedObjectContext]];
                }
                
                [eventsModelArray removeAllObjects];
                
                [eventsArray removeAllObjects];
                
                [self clearSelectionView];
                
                [self categorizeEvents];
            }
            else{
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary new];
                
                if (approvalType == APPROVAL_TYPE_APPROVE) {
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                else{
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                
                [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_EVENTAPPROVALS_CLICKED_BULKAPPROVALS parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
            }
        }
        else{
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary new];
            
            if (approvalType == APPROVAL_TYPE_APPROVE) {
                
                [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
            }
            else{
                
                [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
            }
            
            [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
            
            [TSAnalytics logEvent:ANALYTICS_EVENTAPPROVALS_CLICKED_BULKAPPROVALS parameter:analyticsDict];
        }
    }];
}

@end