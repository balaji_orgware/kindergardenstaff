//
//  EventInbox+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 28/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "EventInbox.h"

NS_ASSUME_NONNULL_BEGIN

@interface EventInbox (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *assignedList;
@property (nullable, nonatomic, retain) NSString *eventAttachment;
@property (nullable, nonatomic, retain) NSString *eventDescription;
@property (nullable, nonatomic, retain) NSString *eventTime;
@property (nullable, nonatomic, retain) NSString *eventTitle;
@property (nullable, nonatomic, retain) NSString *eventVenue;
@property (nullable, nonatomic, retain) NSDate *fromDate;
@property (nullable, nonatomic, retain) NSNumber *isHasAttachment;
@property (nullable, nonatomic, retain) NSDate *modifiedDate;
@property (nullable, nonatomic, retain) NSString *staffTransID;
@property (nullable, nonatomic, retain) NSDate *toDate;
@property (nullable, nonatomic, retain) NSString *transID;
@property (nullable, nonatomic, retain) NSString *userCreatedName;
@property (nullable, nonatomic, retain) StaffDetails *staff;

@end

NS_ASSUME_NONNULL_END
