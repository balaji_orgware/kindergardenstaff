//
//  TSHealthTodayViewController.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 25/06/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSHealthStudSelectionViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,SelectionCallBackDelegate>

@property (weak, nonatomic) IBOutlet UIView *selectionView;
@property (weak, nonatomic) IBOutlet UITableView *studentsTableView;

@property (weak, nonatomic) IBOutlet UITextField *boardTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *classTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *sectionTxtFld;

@end