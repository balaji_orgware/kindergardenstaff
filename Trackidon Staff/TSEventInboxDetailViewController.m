//
//  TSEventInboxDetailViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/5/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSEventInboxDetailViewController.h"

@interface TSEventInboxDetailViewController ()
{
    
    NSURL *attachmentURL;
    
}
@end

@implementation TSEventInboxDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [TSSingleton layerDrawForView:_topView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    _attachmentButton.frame = CGRectMake(DEVICE_SIZE.width - _attachmentButton.frame.size.width - 10, _topView.frame.origin.y + _topView.frame.size.height - _attachmentButton.frame.size.height/2 + 18, _attachmentButton.frame.size.height, _attachmentButton.frame.size.height);
    
    
    [TSSingleton layerDrawForView:_dateView position:LAYER_TOP color:[UIColor lightGrayColor]];
    
    _fromDateLabel.layer.cornerRadius = _fromDateLabel.frame.size.width/2;
    
    _fromDateLabel.layer.masksToBounds = YES;
    
    _toDateLabel.layer.cornerRadius = _toDateLabel.frame.size.width/2;
    
    _toDateLabel.layer.masksToBounds = YES;
    
    _progressView.frame = CGRectMake(_fromDateLabel.center.x +_fromDateLabel.frame.size.width/2, _fromDateLabel.center.y, _progressView.frame.size.width, _progressView.frame.size.height);
    
    _toDateLabel.frame = CGRectMake(_progressView.center.x +_progressView.frame.size.width/2, _fromDateLabel.frame.origin.y, _toDateLabel.frame.size.width, _toDateLabel.frame.size.height);
    
    _fromLbl.center = CGPointMake(_fromDateLabel.center.x, _fromDateLabel.frame.origin.y-10);
    
    _toLbl.center = CGPointMake(_toDateLabel.center.x, _toDateLabel.frame.origin.y-10);
    
    _fromDateLabel.attributedText = [self getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:_eventInboxObj.fromDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_fromDateLabel];
    
    _toDateLabel.attributedText = [self getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:_eventInboxObj.toDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_toDateLabel];
    
    _assignedTeacherLabel.text = [NSString stringWithFormat:@"- by %@",_eventInboxObj.userCreatedName];
    
    _venueLbl.text = [NSString stringWithFormat:@"Venue : %@",_eventInboxObj.eventVenue];
    
    _eventTitleLbl.text = [NSString stringWithFormat:@"Title : %@",_eventInboxObj.eventTitle];
    
    _timeLbl.text = [NSString stringWithFormat:@"Time : %@",[SINGLETON_INSTANCE convertTimeToTweleveHousrFomateFromString:_eventInboxObj.eventTime]];
    
    _descriptionTextView.text = [NSString stringWithFormat:@"\t%@",_eventInboxObj.eventDescription];
    
    if ([_eventInboxObj.isHasAttachment intValue] == 1) {
        
        [_attachmentButton setHidden:NO];
    }
    else{
        
        [_attachmentButton setHidden:YES];
    }
    
    NSLog(@"FromTo::%@--%@",_eventInboxObj.fromDate,_eventInboxObj.toDate);
    
    if ([_eventInboxObj.fromDate compare:_eventInboxObj.toDate]==NSOrderedSame) {
        
        _remainingCountLbl.text = [NSString stringWithFormat:@"%@",[TSSingleton daysBetweenFromDate:_eventInboxObj.fromDate toDate:_eventInboxObj.toDate]];
        
        _daysLeftLbl.text = @"Day Event";
        
    }else{
        
        _remainingCountLbl.text = [NSString stringWithFormat:@"%@",[TSSingleton daysBetweenFromDate:_eventInboxObj.fromDate toDate:_eventInboxObj.toDate]];
        
        _daysLeftLbl.text = @"Days Event";
    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Event Description";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(NSMutableAttributedString *) getDateAttributedStringForDateString:(NSString *)dateString inLabel:(UILabel *)label{
    
    [label setNumberOfLines:2];
    
    NSString *dateNo = [SINGLETON_INSTANCE changeDateFormatFromService:dateString withFormat:@"dd"];
    
    NSString *monthName = [SINGLETON_INSTANCE changeDateFormatFromService:dateString withFormat:@"MMM"];
    
    UIFont *dateFont = [UIFont fontWithName:APP_FONT size:25.0];
    NSDictionary *dateTextAttr = [NSDictionary dictionaryWithObject: dateFont forKey:NSFontAttributeName];
    NSMutableAttributedString *countAttrStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@   ",dateNo] attributes: dateTextAttr];
    
    UIFont *monthFont = [UIFont fontWithName:APP_FONT size:12.0];
    NSDictionary *monthTextAttr = [NSDictionary dictionaryWithObject:monthFont forKey:NSFontAttributeName];
    NSMutableAttributedString *textAttrStr = [[NSMutableAttributedString alloc]initWithString:monthName attributes:monthTextAttr];
    
    [countAttrStr appendAttributedString:textAttrStr];
    
    return countAttrStr;
}

- (IBAction)attachmentBtnClk:(id)sender {
    
    [_attachmentButton setUserInteractionEnabled:NO];
    
    NSString *attachmentURLString;
    
    attachmentURL = [NSURL URLWithString:_eventInboxObj.eventAttachment];
    
    attachmentURLString = _eventInboxObj.eventAttachment;
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *attachmentFile = [documentsPath stringByAppendingPathComponent:[attachmentURL lastPathComponent]];
    
    NSLog(@"%@",attachmentFile);
    
    NSLog(@"%@",[attachmentURL lastPathComponent]);
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:attachmentFile];
    
    if (fileExists) {
        
        _attachmentButton.userInteractionEnabled = YES;

        [self navigateToAttachmentViewControllerWithFileName:attachmentURL];
    }
    else{
        
        [[TSHandlerClass sharedHandler]downloadAttachmentWithURL:attachmentURLString currentView:self.view withHandler:^(BOOL success, NSURL *filePath) {
            
            if (success) {
                
                _attachmentButton.userInteractionEnabled = YES;

                [self navigateToAttachmentViewControllerWithFileName:filePath];
            }
            else{
                
                _attachmentButton.userInteractionEnabled = YES;
            }
        }];
    }
}

-(void) navigateToAttachmentViewControllerWithFileName:(NSURL *)fileURL{
    
    TSAttachmentViewController *attachmentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAttachment"];
    
    [attachmentVC setFileName:[fileURL lastPathComponent]];
    
    UINavigationController *navCtrl = [[UINavigationController alloc]initWithRootViewController:attachmentVC];
    
    navCtrl.navigationBar.translucent = NO;
    
    navCtrl.navigationBar.barTintColor = APPTHEME_COLOR;
    
    navCtrl.navigationBar.tintColor = [UIColor whiteColor];
    
    [navCtrl.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:APP_FONT size:18]}];
    
    [self.navigationController presentViewController:navCtrl animated:YES completion:nil];
}


@end
