//
//  TSAppDelegate.m
//  Trackidon Staff
//
//  Created by ORGware on 16/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSAppDelegate.h"
#import "Localytics.h"

@interface TSAppDelegate ()<UITabBarControllerDelegate>

@end

@implementation TSAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [GMSServices provideAPIKey:@"AIzaSyB1OXNMS7C7DvBk_jCpWLvRRBD7LnRC04c"];
    
    [Localytics autoIntegrate:@"fe25e8fed4999256d0b65f7-bad85970-15cb-11e6-b09b-00342b7f5075" launchOptions:launchOptions];
    
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)]){
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
        
        //[self registerForNotification];
        
    }else{
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
    [Localytics closeSession];
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isLogin"])
        [SINGLETON_INSTANCE updatePushCountServiceCall];
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [Localytics openSession];
    
    [TSAnalytics logEvent:ANALYTICS_GLOBAL_APPLAUNCHED parameter:[NSMutableDictionary new]];
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"deviceToken"];
}

-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    [self initilizeInitialViewController];
    
    return YES;
}

-(BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder{
    
    return YES;
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler{
    
    if ([USERDEFAULTS objectForKey:@"isLogin"]) {
        
        // Already logged in
        
        [[TSRemoteNotificationHandler instance]handleNotificationWithPayload:userInfo];
    }
    else{
        
        // When not logged in
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
        {
            
            [TSAnalytics logEvent:ANALYTICS_PUSHNOTIFICATION_RECEIVED parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:userInfo[@"data"][@"MID"],ANALYTICS_PARAM_NOTIFICATIONTYPE, ANALYTICS_VALUE_NOTLOGGEDIN,ANALYTICS_PARAM_ISLOGGEDIN,nil]];
            
        }else {
            
            [TSAnalytics logEvent:ANALYTICS_PUSHNOTIFICATION_CLICKED_NOTIFICATION parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:userInfo[@"data"][@"MID"],ANALYTICS_PARAM_NOTIFICATIONTYPE, ANALYTICS_VALUE_NOTLOGGEDIN,ANALYTICS_PARAM_ISLOGGEDIN,nil]];
        }
    }
    completionHandler(UIBackgroundFetchResultNoData);
}

#pragma mark - TabbarController Delegate

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    
    NSLog(@"%lu",(unsigned long)[self.tabBarController selectedIndex]);
    
    NSLog(@"%@",[[self.tabBarController.tabBar items] objectAtIndex:[self.tabBarController selectedIndex]].title);
    
    [TSAnalytics logEvent:ANALYTICS_MAINMENU_MAINMENU_CLICKED parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:[[self.tabBarController.tabBar items] objectAtIndex:[self.tabBarController selectedIndex]].title, ANALYTICS_PARAM_MENUNAME,[NSString stringWithFormat:@"%ld",[self.tabBarController selectedIndex]],ANALYTICS_PARAM_MENUID, nil]];
    
}


#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.orgware.Trackidon" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"TrackidonStaff" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"TrackidonStaff.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Load View Controllers

+ (instancetype)instance{
    
    return (TSAppDelegate *)[[UIApplication sharedApplication] delegate];
}

// Method to initiate view controllers dynamically
- (void)initilizeInitialViewController{
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isLogin"]) {
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        
        UIViewController *schoolCodeVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBSchoolCode"];
        
        UINavigationController *navigationController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
        
        navigationController.navigationBarHidden = YES;
        
        self.window.rootViewController = navigationController;
        
        [navigationController setViewControllers:@[schoolCodeVC] animated:NO];
        
    } else {
        
        @try {
            
            [self.managedObjectContext setMergePolicy:[[NSMergePolicy alloc] initWithMergeType:NSMergeByPropertyObjectTrumpMergePolicyType]];
            
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
            
            SINGLETON_INSTANCE.staffTransId = [USERDEFAULTS valueForKey:@"staffTransId"];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId MATCHES %@",SINGLETON_INSTANCE.staffTransId];
            
            NSArray *staffArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"StaffDetails" withPredicate:predicate];
            
            if (staffArr.count>0) {
                
                SINGLETON_INSTANCE.staff = [staffArr objectAtIndex:0];
            }
            
            SINGLETON_INSTANCE.academicTransId = SINGLETON_INSTANCE.staff.acadamicTransId;
            
            SINGLETON_INSTANCE.selectedSchoolTransId = [USERDEFAULTS valueForKey:@"schoolTransId"];
            
            SINGLETON_INSTANCE.selectedSchoolName = [USERDEFAULTS valueForKey:@"schoolName"];
            
            if (SINGLETON_INSTANCE.moduleMenuArr.count == 0) {
                
                [SINGLETON_INSTANCE createMenu];
                
            }
            
            self.tabBarController = [storyBoard instantiateViewControllerWithIdentifier:@"SBTabBarController"];
            
            self.tabBarController.viewControllers = [self initilizeViewControllersForTabBar];
            
            self.tabBarController.tabBar.tintColor = APPTHEME_COLOR;
            
            [[UINavigationBar appearance]setTintColor:[UIColor whiteColor]];
            
            [[UINavigationBar appearance]setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:APP_FONT size:18],NSForegroundColorAttributeName:[UIColor whiteColor]}];
            
            self.window.rootViewController = self.tabBarController;
            
        } @catch (NSException *exception) {
            
            NSLog(@"%@",exception);
            
        } @finally {
            
            //[USERDEFAULTS setBool:NO forKey:@"isLogin"];

        }
    }
}

- (NSMutableArray *)initilizeViewControllersForTabBar{
    
    NSMutableArray *viewControllersArray = [[NSMutableArray alloc]init];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    for (NSDictionary *dict in SINGLETON_INSTANCE.moduleMenuArr) {
        
        NSInteger menu = [dict[@"ModuleID"] integerValue];
        
        switch (menu) {
                
            case MENU_COMMUNICATION:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *communicationVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommunication"];
                [navController setViewControllers:@[communicationVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@"communication"] selectedImage:[UIImage imageNamed:@"communication_selected"]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
            }
            case MENU_PROFILE:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *profileVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBProfile"];
                [navController setViewControllers:@[profileVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@"profile"] selectedImage:[UIImage imageNamed:@"profile_selected"]];
                
                navController.navigationBarHidden = NO;
                
                [viewControllersArray addObject:navController];
                
                break;
            }
            case MENU_LIVE_TRACK:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *busTrackVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBBusTrack"];
                [navController setViewControllers:@[busTrackVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@"live_track"] selectedImage:[UIImage imageNamed:@"live_track_default"]];
                
                navController.navigationBarHidden = NO;
                
                [viewControllersArray addObject:navController];
                
                break;
            }
            case MENU_SETTINGS:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *settingsVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBSettings"];
                [navController setViewControllers:@[settingsVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@"settings"] selectedImage:[UIImage imageNamed:@"settings_selected"]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
            }
            case MENU_INBOX:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *inboxVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBInbox"];
                [navController setViewControllers:@[inboxVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@"inbox_default"] selectedImage:[UIImage imageNamed:@"inbox"]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
            }
            case MENU_APPROVALS:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *approvalsVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBApprovals"];
                [navController setViewControllers:@[approvalsVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@"approval_default"] selectedImage:[UIImage imageNamed:@"approval"]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
            }
            case MENU_MORE:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *aboutVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBMore"];
                [navController setViewControllers:@[aboutVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@"more"] selectedImage:[UIImage imageNamed:@"more_selected"]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
                
            }
            case MENU_ALERTS:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *aboutVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBMore"];
                [navController setViewControllers:@[aboutVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@"more"] selectedImage:[UIImage imageNamed:@"more_selected"]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
            }
            case MENU_ASSIGNMENTS:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *alertVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBAssignments"];
                [navController setViewControllers:@[alertVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@""] selectedImage:[UIImage imageNamed:@""]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
            }
            case MENU_EVENTS:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *alertVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBAssignments"];
                [navController setViewControllers:@[alertVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@""] selectedImage:[UIImage imageNamed:@""]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
            }
            case MENU_ATTENDANCE:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *alertVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBAttendanceHome"];
                [navController setViewControllers:@[alertVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@""] selectedImage:[UIImage imageNamed:@""]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
            }
            case MENU_EXAMS:
            {
                
                break;
            }
            case MENU_NOTIFICATIONS:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *alertVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBAttendanceHome"];
                [navController setViewControllers:@[alertVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@""] selectedImage:[UIImage imageNamed:@""]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
            }
            case MENU_HOLIDAY:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *alertVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBAttendanceHome"];
                [navController setViewControllers:@[alertVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@""] selectedImage:[UIImage imageNamed:@""]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
            }
            case MENU_PORTIONS:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *alertVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBAttendanceHome"];
                [navController setViewControllers:@[alertVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@""] selectedImage:[UIImage imageNamed:@""]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
            }
            case MENU_LIBRARY:
            {
                
                break;
            }
            case MENU_TIME_TABLE:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *alertVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBAttendanceHome"];
                [navController setViewControllers:@[alertVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@""] selectedImage:[UIImage imageNamed:@""]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
            }
            case MENU_RESULTS:
            {
                
                break;
            }
            case MENU_FEES:
            {
                
                break;
            }
            case MENU_HOSTEL:
            {
                
                break;
            }
            case MENU_CHANGE_PASSWORD:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *changePswdVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBOldPassword"];
                [navController setViewControllers:@[changePswdVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@""] selectedImage:[UIImage imageNamed:@""]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
                
            }
            case MENU_ABOUT_US:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *aboutVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBOldPassword"];
                [navController setViewControllers:@[aboutVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@""] selectedImage:[UIImage imageNamed:@""]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
                
            }
            case MENU_USER_MANUAL:
            {
                
                break;
            }
            case MENU_FEEDBACK:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *aboutVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBFeedBack"];
                [navController setViewControllers:@[aboutVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@"feedback"] selectedImage:[UIImage imageNamed:@"feedback_selected"]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
            }
            case MENU_HEALTHDETAILS:
            {
                UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
                UIViewController *healthDetailsVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBHealthHome"];
                [navController setViewControllers:@[healthDetailsVC] animated:NO];
                navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:dict[@"ModuleName"] image:[UIImage imageNamed:@""] selectedImage:[UIImage imageNamed:@""]];
                navController.navigationBarHidden = NO;
                [viewControllersArray addObject:navController];
                break;
            }
                
            default:
                break;
        }
    }
    return viewControllersArray;
}

void uncaughtExceptionHandler(NSException *exception) {
    
    NSLog(@"%@",exception);
    
    NSString *userName = [USERDEFAULTS valueForKey:@"LoginId"] == nil ? @"NotLoggedIn" : [USERDEFAULTS valueForKey:@"LoginId"];
    
    [TSAnalytics logEvent:ANALYTICS_GLOBAL_APPEXCEPTION parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:userName,@"userName",exception,@"Exception", nil]];

    
    NSLog(@"%@",exception);
    
}

@end