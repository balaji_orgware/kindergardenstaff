//
//  ReceiverFilterList+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 02/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ReceiverFilterList+CoreDataProperties.h"

@implementation ReceiverFilterList (CoreDataProperties)

@dynamic filterDescription;
@dynamic filterTypeId;
@dynamic menuId;
@dynamic isSelected;
@dynamic staff;

@end
