//
//  TSPortionInboxViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/15/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSPortionInboxViewController.h"

@interface TSPortionInboxViewController ()
{
    NSArray *attachmentImgArray, *portionArray;
}
@end

@implementation TSPortionInboxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [TSAnalytics logEvent:ANALYTICS_INBOXPORTION_OPENED parameter:[NSMutableDictionary new]];
    
    attachmentImgArray = @[@"one",@"two",@"three",@"four",@"five"];
    
    [_portionTableView setTableFooterView:[UIView new]];


    [self fetchPortion];
    
    [self fetchPortionInbox];
}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_INBOXPORTION parameter:[NSMutableDictionary new]];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Lesson Plan Log";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Tableview Delegate & Datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return portionArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    UILabel *titleLbl = (UILabel *)[cell viewWithTag:101];
    
    UILabel *createdByLbl = (UILabel *)[cell viewWithTag:102];
    
    UILabel *dateLbl = (UILabel *)[cell viewWithTag:103];
    
    UIImageView *attachmentImgVw = (UIImageView *)[cell viewWithTag:104];
    
    PortionInbox *portionInboxObj = [portionArray objectAtIndex:indexPath.row];
    
    titleLbl.text = portionInboxObj.portionsTitle;
    
    createdByLbl.text =[NSString stringWithFormat:@"Created By : %@",portionInboxObj.createdBy];
    
    dateLbl.text = [SINGLETON_INSTANCE stringFromDateWithFormate:DATE_FORMATE_MNTH_DAY_YR withDate:[SINGLETON_INSTANCE dateFromDate:portionInboxObj.portionsDate withFormate:DATE_FORMATE_SERVICE_RESPONSE]];
    
    if ([portionInboxObj.isHasAttachment intValue] == 1) {
        
        NSString *attachmentImgName;
        
        switch (indexPath.row%5) {
                
            case 0:
                attachmentImgName = @"one";
                break;
                
            case 1:
                attachmentImgName = @"two";
                break;
                
            case 2:
                attachmentImgName = @"three";
                break;
                
            case 3:
                attachmentImgName = @"four";
                break;
                
            case 4:
                attachmentImgName = @"five";
                break;
                
            default:
                attachmentImgName = @"one";
                break;
        }
        attachmentImgVw.image = [UIImage imageNamed:attachmentImgName];
        
        [attachmentImgVw setHidden:NO];
    }
    else{
        
        [attachmentImgVw setHidden:YES];
    }
    
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [TSAnalytics logEvent:ANALYTICS_INBOXPORTION_CLICKEDINLIST parameter:[NSMutableDictionary new]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TSPortionDescriptionViewController *portionDesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBPortionDescription"];
    
    id portionInbox = (PortionInbox *)[portionArray objectAtIndex:indexPath.row];
    
    portionDesVC.descriptionType = PortionInboxString;
    
    portionDesVC.commonObj = portionInbox;
    
    [self.navigationController pushViewController:portionDesVC animated:YES];
}

-(void)fetchPortionInbox{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    NSString *url = @"MastersMServices/SchoolNotificationsService.svc/FetchSchoolStaffNotificationsByStaffID";
    
    [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidStaffTransID"];
    
    BOOL animated;
    
    if (portionArray.count == 0)
        animated = YES;
    
    else
        animated = NO;
    
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:url baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:animated onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchSchoolStaffNotificationsByStaffIDResult"][@"ResponseCode"]intValue]==1) {
                
                NSLog(@"%@",responseDictionary);
                
                if (responseDictionary[@"FetchSchoolStaffNotificationsByStaffIDResult"][@"PortionsClass"] != [NSNull null]) {
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                        [[PortionInbox instance]saveProtionInboxWithResponseDict:responseDictionary];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self fetchPortion];
                            
                        });
                    });
                }
            }
        }
    }];
}

- (void)fetchPortion{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransID CONTAINS[cd] %@",SINGLETON_INSTANCE.staffTransId];
    
    portionArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"PortionInbox" withPredicate:predicate];
    
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:NO];
    
    portionArray = [[portionArray sortedArrayUsingDescriptors:@[dateDescriptor]] mutableCopy];
    
    if (portionArray.count>0) {
        
        [self.portionTableView setHidden:NO];
    }
    else{
        
        [self.portionTableView setHidden:YES];
    }
    
    [self.portionTableView reloadData];
    
}



@end
