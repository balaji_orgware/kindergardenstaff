//
//  TSMoreTableViewController.m
//  Trackidon Staff
//
//  Created by ORGware on 18/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSMoreTableViewController.h"

@interface TSMoreTableViewController ()

@end

@implementation TSMoreTableViewController
{
    NSArray *menuListArray;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    menuListArray = SINGLETON_INSTANCE.moreMenuArr;
    
//    UIBarButtonItem *logoutBtn = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStyleDone target:self action:@selector(onLogoutClk)];
//    
//    self.navigationItem.rightBarButtonItem = logoutBtn;
    
}

-(void)viewWillAppear:(BOOL)animated{
 
    [TSAnalytics logScreen:ANALYTICS_SCREEN_MORE parameter:[NSMutableDictionary new]];
    
    [self.tableView setTableFooterView:[UIView new]];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"More";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button Actions

- (void) onLogoutClk{
    
    [self logoutCurrentUserServiceCall];
    
}

#pragma mark - Tableview Datasources & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return menuListArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sCellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:300];
    
    UILabel *textLabel = (UILabel *)[cell viewWithTag:301];
    
    textLabel.text = [menuListArray objectAtIndex:indexPath.row][@"MenuName"];
    
    NSInteger menuID = [[menuListArray objectAtIndex:indexPath.row][@"MenuID"] integerValue];
    
    switch (menuID) {
        
        case MENU_PROFILE:
            
            imageView.image = [UIImage imageNamed:@"Profile"];
            break;
            
        case MENU_USER_MANUAL:
            
            imageView.image = [UIImage imageNamed:@"user_manual"];
            break;
            
        case MENU_ALERTS:
            
            imageView.image = [UIImage imageNamed:@"alerts"];
            break;
            
        case MENU_CHANGE_PASSWORD:
            
            imageView.image = [UIImage imageNamed:@"change_password"];
            break;
            
        case MENU_ABOUT_US:
            
            imageView.image = [UIImage imageNamed:@"about_us"];
            break;
            
        case MENU_CHANGE_SCHOOL:
            
            imageView.image = [UIImage imageNamed:@"school"];
            break;
        
        case MENU_SUPPORT:
            
            imageView.image = [UIImage imageNamed:@"helpline"];
            break;
            
        case MENU_LOGOUT:
             
             imageView.image = [UIImage imageNamed:@"logout"];
             break;
            
        case MENU_FEEDBACK:
            
            imageView.image = [UIImage imageNamed:@"feedback"];
            break;
         case MENU_SETTINGS:
            
            imageView.image = [UIImage imageNamed:@"settings_List"];
            break;
            
        default:
            break;
    }
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger menuID = [[menuListArray objectAtIndex:indexPath.row][@"MenuID"] integerValue];
    
    [TSAnalytics logEvent:ANALYTICS_SUBMENU_SUBMENU_CLICKED parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:[menuListArray objectAtIndex:indexPath.row][@"MenuName"], ANALYTICS_PARAM_SUBMENUNAME,[NSString stringWithFormat:@"%ld",(long)menuID],ANALYTICS_PARAM_SUBMENUID, nil]];
    
    switch (menuID) {
            
        case MENU_PROFILE:
        {
            UIViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBProfile"];

            [self.navigationController pushViewController:profileVC animated:YES];
            
            break;
        }
            
        case MENU_USER_MANUAL:
        {
            
            [TSAnalytics logScreen:ANALYTICS_SCREEN_USERMANUAL parameter:[NSMutableDictionary new]];
            
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"schoolTransId MATCHES %@",SINGLETON_INSTANCE.selectedSchoolTransId];
            
            NSArray *schoolsArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"School" withPredicate:pred];
            
            if (schoolsArray.count>0) {
                
                School *schoolObj = [schoolsArray objectAtIndex:0];
                
                if (schoolObj.staffUserMannualUrl.length >0) {
                 
                    NSURL *attachmentURL = [NSURL URLWithString:schoolObj.staffUserMannualUrl];
                    
                    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                    
                    NSString *attachmentFile = [documentsPath stringByAppendingPathComponent:[attachmentURL lastPathComponent]];
                    
                    NSLog(@"%@",attachmentFile);
                    
                    NSLog(@"%@",[attachmentURL lastPathComponent]);
                    
                    BOOL isDir = YES;
                    
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:attachmentFile isDirectory:&isDir];
                    
                    if (fileExists) {
                        
                        NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                        
                        [analyticsDict setValue:ANALYTICS_VALUE_OPENEDFROMLOCAL forKey:ANALYTICS_PARAM_STATUS];
                        
                        [TSAnalytics logEvent:ANALYTICS_USERMANUAL_OPENED parameter:analyticsDict];
                        
                        [self navigateToAttachmentViewControllerWithFileName:attachmentURL];
                    }
                    else{
                        
                        [[TSHandlerClass sharedHandler]downloadAttachmentWithURL:schoolObj.staffUserMannualUrl currentView:self.view withHandler:^(BOOL success, NSURL *filePath) {
                            
                            if (success) {
                                
                                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                                
                                [analyticsDict setValue:ANALYTICS_VALUE_DOWNLOADING forKey:ANALYTICS_PARAM_STATUS];
                                
                                [TSAnalytics logEvent:ANALYTICS_USERMANUAL_OPENED parameter:analyticsDict];
                                
                                [self navigateToAttachmentViewControllerWithFileName:filePath];
                            }
                            else{
                                
                                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                                
                                [analyticsDict setValue:ANALYTICS_VALUE_URLNOTFOUND forKey:ANALYTICS_PARAM_STATUS];
                                
                                [TSAnalytics logEvent:ANALYTICS_USERMANUAL_OPENED parameter:analyticsDict];
                                
                                [TSSingleton showAlertWithMessage:@"File not found"];
                            }
                        }];
                    }
                }
                else{
                    
                    NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_URLNOTFOUND forKey:ANALYTICS_PARAM_STATUS];
                    
                    [TSAnalytics logEvent:ANALYTICS_USERMANUAL_OPENED parameter:analyticsDict];
                    
                    [TSSingleton showAlertWithMessage:@"No User manual currently available"];
                }
            }
            
            break;
        }

            
        case MENU_ALERTS:
        {
            
            
            break;
        }
            
        case MENU_CHANGE_PASSWORD:
        {
            
            TSNewPasswordViewController *changePasswordVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBNewPassword"];
            
            [self.navigationController pushViewController:changePasswordVC animated:YES];
            
            
            break;
            
        }
        case MENU_ABOUT_US:
        {
            
            TSAboutSchoolViewController *aboutSchoolVC =  [self.storyboard instantiateViewControllerWithIdentifier:@"SBAboutSchool"];
            
            [self.navigationController pushViewController:aboutSchoolVC animated:YES];

            
            break;
        }
        case MENU_CHANGE_SCHOOL:
        {
            
            TSSchoolListViewController *schoolListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBSchoolList"];
            
            UINavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"SBCommonNavigationController"];
            
            navigationController.navigationBarHidden = YES;
            
            [APPDELEGATE_INSTANCE window].rootViewController = navigationController;
            
            [navigationController setViewControllers:@[schoolListVC] animated:NO];
            
            break;
            
        }
            
        case MENU_LOGOUT:
        {
            [self onLogoutClk];
         
            break;
        }
            
        case MENU_SUPPORT:
        {
            TSAboutViewController *aboutVC =  [self.storyboard instantiateViewControllerWithIdentifier:@"SBAbout"];
            
            [self.navigationController pushViewController:aboutVC animated:YES];
            
            break;
        }
        case MENU_FEEDBACK:{
            TSFeedBackViewController *feedbackVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBFeedBack"];
            
            [self.navigationController pushViewController:feedbackVC animated:YES];
            break;
        }
        case MENU_SETTINGS:{
            TSSettingsViewController *settingsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBSettings"];
            
            [self.navigationController pushViewController:settingsVC animated:YES];
            break;
        }
        default:
            break;
    }
}


-(void) navigateToAttachmentViewControllerWithFileName:(NSURL *)fileURL{
    
    TSAttachmentViewController *attachmentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAttachment"];
    
    [attachmentVC setFileName:[fileURL lastPathComponent]];
    
    UINavigationController *navCtrl = [[UINavigationController alloc]initWithRootViewController:attachmentVC];
    
    navCtrl.navigationBar.translucent = NO;
    
    navCtrl.navigationBar.barTintColor = APPTHEME_COLOR;
    
    navCtrl.navigationBar.tintColor = [UIColor whiteColor];
    
    [navCtrl.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:APP_FONT size:18]}];
    
    //    if (DeviceSystemVersion >8) {
    //        Ç
    //        [[UINavigationBar appearance]setTranslucent:NO];
    //
    //        [[UINavigationBar appearance]setTintColor:[UIColor whiteColor]];
    //
    //        [[UINavigationBar appearance]setBarTintColor:AppThemeBlue];
    //
    //        [[UINavigationBar appearance]setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:VarelaRegFont size:18]}];
    //    }
    
    [self.navigationController presentViewController:navCtrl animated:YES completion:nil];
}

#pragma mark - Logout ServiceCall

- (void)logoutCurrentUserServiceCall{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"TransID"];
    
    [[TSHandlerClass sharedHandler] postRequestWithURL:@"MastersMServices/AccountsMService.svc/AccountLogout" baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        if (success) {
            
            NSLog(@"%@",responseDictionary[@"AccountLogoutResult"][@"ResponseCode"]);
            
            if ([responseDictionary[@"AccountLogoutResult"][@"ResponseCode"] integerValue] == 1) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_STATUS];
                
                [analyticsDict setValue:responseDictionary[@"AccountLogoutResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                
                [TSAnalytics logEvent:ANALYTICS_LOGOUT_CLICKED parameter:analyticsDict];
                
                [SINGLETON_INSTANCE deleteAllObjectsInEntity:@"StaffDetails"];
                [SINGLETON_INSTANCE deleteAllObjectsInEntity:@"Trust"];
                
                [SINGLETON_INSTANCE.moduleMenuArr removeAllObjects];
                [SINGLETON_INSTANCE.communicationMenuArr removeAllObjects];
                [SINGLETON_INSTANCE.moreMenuArr removeAllObjects];
                
                
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isLogin"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[TSAppDelegate instance]initilizeInitialViewController];
                
                [TSSingleton showAlertWithMessage:LOGOUT_SUCCESS_MESSAGE];
            }else {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_STATUS];
                
                [analyticsDict setValue:responseDictionary[@"AccountLogoutResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                
                [TSAnalytics logEvent:ANALYTICS_LOGOUT_CLICKED parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:LOGOUT_FAILED_MESSAGE];
            }
            
        }else {
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            [analyticsDict setValue:ANALYTICS_VALUE_SERVICEFAILURE_RESPONSEMSG forKey:ANALYTICS_PARAM_STATUS];
            
            [TSAnalytics logEvent:ANALYTICS_LOGOUT_CLICKED parameter:analyticsDict];
            
            [TSSingleton showAlertWithMessage:LOGOUT_FAILED_MESSAGE];
        }
        
    }];
}

@end