//
//  TSGeneralCheckupDescViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 21/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSGeneralCheckupDescViewController.h"

@interface TSGeneralCheckupDescViewController (){
    
    NSArray *nameListArr;
    
    GeneralCheckupModel *modelObj;
}

@end

@implementation TSGeneralCheckupDescViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    nameListArr = @[@"Neck",@"Hands",@"Leg",@"Abdomen",@"Allergy",@"Anemia",@"Nerve system"];
    
    self.view.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);
    
    _closeButton.frame = CGRectMake(DeviceWidth-10-_closeButton.frame.size.width, _closeButton.frame.origin.y, _closeButton.frame.size.width, _closeButton.frame.size.height);
    _backView.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);
    _descView.frame = CGRectMake(_descView.frame.origin.x, _descView.frame.origin.y, _descView.frame.size.width, _descView.frame.size.height);
    _listTableView.frame = CGRectMake(_listTableView.frame.origin.x, _dateView.frame.origin.y+_dateView.frame.size.height, DeviceWidth, _descView.frame.size.height -_dateView.frame.origin.y-_dateView.frame.size.height-44);
    
    UITapGestureRecognizer *tapOnBackView = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOnBackView)];
    [_backView addGestureRecognizer:tapOnBackView];
    
    [TSSingleton layerDrawForView:_topView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_dateView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Helper methods

-(void)tapOnBackView{
    
    [self hideAnimationView];
}

- (IBAction)closeButtonAction:(id)sender {
    
    [self hideAnimationView];
}

-(void) loadViewWithContent{
    
    _dateLabel.text = [SINGLETON_INSTANCE stringFromDate:modelObj.examinedDate withFormate:DATE_FORMATE_MNTH_DAY_YR];
    
    _examinedByNameLbl.text = modelObj.examinedBy;
    
    [_listTableView reloadData];
}

-(void) showDescViewController:(UIViewController *)viewController withModelObj:(GeneralCheckupModel *)model{
    
    [viewController.view addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    [viewController.view endEditing:YES];
    
    _descView.frame = CGRectMake(_descView.frame.origin.x, DeviceHeight, viewController.view.frame.size.width, viewController.view.frame.size.height);
    
    _listTableView.frame = CGRectMake(_listTableView.frame.origin.x, _dateView.frame.origin.y+_dateView.frame.size.height, _descView.frame.size.width, _descView.frame.size.height - _dateView.frame.origin.y-_dateView.frame.size.height);
    
    modelObj = model;
    
    [self loadViewWithContent];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [_backView setAlpha:0.5];
        
        _descView.frame = CGRectMake(_descView.frame.origin.x, viewController.view.frame.size.height-_descView.frame.size.height, _descView.frame.size.width, _descView.frame.size.height);
    }];
}

-(void) hideAnimationView{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [_backView setAlpha:0.0];
        _descView.frame = CGRectMake(_descView.frame.origin.x, DeviceHeight, _descView.frame.size.width, _descView.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

#pragma mark - TableView Datasource & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return nameListArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TSGeneralCheckupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"generalCheckupCell"];
    
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:@"TSGeneralCheckupTableViewCell" bundle:nil] forCellReuseIdentifier:@"generalCheckupCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"generalCheckupCell"];
    }
    
    UILabel *nameLbl = (UILabel *)[cell viewWithTag:100];
    UILabel *valueLbl = (UILabel *)[cell viewWithTag:101];
    
    nameLbl.text = [nameListArr objectAtIndex:indexPath.row];
    
    switch (indexPath.row) {
        case 0:
            valueLbl.text = modelObj.neckRemarks;
            break;
            
        case 1:
            valueLbl.text = modelObj.handsRemarks;
            break;
            
        case 2:
            valueLbl.text = modelObj.legsRemarks;
            break;
        
        case 3:
            valueLbl.text = modelObj.abdomen;
            break;
            
        case 4:
            valueLbl.text = modelObj.allergy;
            break;
            
        case 5:
            valueLbl.text = modelObj.anemia;
            break;
        
        case 6:
            valueLbl.text = modelObj.nerveSystem;
            break;
            
        default:
            break;
    }
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
}

@end
