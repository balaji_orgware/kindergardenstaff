//
//  TSNotificationApprovalDescViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 06/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSNotificationApprovalDescViewController.h"

@interface TSNotificationApprovalDescViewController (){
    
    NSURL *attachmentURL;
    
    NSArray *assigneeArr;
}
@end

@implementation TSNotificationApprovalDescViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(descViewTapped)];
    
    [_descriptionTextView addGestureRecognizer:tapGesture];
    
    {
        [TSSingleton layerDrawForView:_topView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
                
        [TSSingleton layerDrawForView:_dateView position:LAYER_TOP color:[UIColor lightGrayColor]];
        
        [TSSingleton layerDrawForView:_assignedToView position:LAYER_TOP color:[UIColor lightGrayColor]];
        
        [SINGLETON_INSTANCE addLeftInsetToTextField:_assignedToTxtFld];
    }
    
    if (IS_IPHONE_4) {
        
//        int navigationBarHeight = 64;
        
        _dateView.frame = CGRectMake(0, DeviceHeight -75, DeviceWidth, 75);
        
        _assignedToView.frame = CGRectMake(0, DeviceHeight-_dateView.frame.size.height-40, DeviceWidth, 40);
        
        if (assigneeArr.count == 0) {
            
            _descriptionTextView.frame = CGRectMake(self.descriptionTextView.frame.origin.x, self.descriptionTextView.frame.origin.y, self.descriptionTextView.frame.size.width, 120+_assignedToView.frame.size.height);
        }
        else{
            
            _descriptionTextView.frame = CGRectMake(self.descriptionTextView.frame.origin.x, self.descriptionTextView.frame.origin.y, self.descriptionTextView.frame.size.width, 120);
        }
        
        _fromDateLbl.frame = CGRectMake(_fromDateLbl.frame.origin.x, 25, 45, 45);
        
        _notificationPriorityView.frame = CGRectMake(_notificationPriorityView.frame.origin.x, _fromDateLbl.frame.origin.y, 45, 45);
        
        _notificationPriorityTextLbl.center = CGPointMake(_notificationPriorityView.center.x, 15);
        
        _createdOnTextLbl.center = CGPointMake(_fromDateLbl.center.x, 15);
//        _assignedLbl.center = CGPointMake(_assignedDateLabel.center.x, 15);
    }
    {
    
    _attachmentButton.frame = CGRectMake(DEVICE_SIZE.width - _attachmentButton.frame.size.width - 10, _topView.frame.origin.y + _topView.frame.size.height - _attachmentButton.frame.size.height/2 + 18, _attachmentButton.frame.size.height, _attachmentButton.frame.size.height);
    
        {
            _fromDateLbl.layer.cornerRadius = _fromDateLbl.frame.size.width/2;
            
            _fromDateLbl.layer.masksToBounds = YES;
            
            _notificationPriorityView.layer.cornerRadius = _notificationPriorityView.frame.size.width/2;
            
            _notificationPriorityView.layer.masksToBounds = YES;
        }
        
        _titleLbl.text = [NSString stringWithFormat:@"Title : %@",self.notification.notificationTitle];
        
        _boardLbl.text = [NSString stringWithFormat:@"Board : %@",self.notification.boardName];
        
        _createdByLbl.text = [NSString stringWithFormat:@"- by %@",self.notification.userCreatedName];
        
        _descriptionTextView.text = self.notification.notificationDescription;
        
        if (self.notification.notificationDate != nil) {
            
            _fromDateLbl.attributedText = [self getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:self.notification.notificationDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_fromDateLbl];
        }
       
        switch ([self.notification.notificationPriority integerValue]) {
                
            case 1:
                
                _notificationPriorityTextLbl.text = @"Priority High";
                
                _notificationPriorityView.backgroundColor = [UIColor colorWithRed:1.0 green:45.0/255.0 blue:85.0/255.0 alpha:1.0];
                
                break;
                
            case 2:
                
                _notificationPriorityTextLbl.text = @"Priority Medium";
                
                _notificationPriorityView.backgroundColor = [UIColor colorWithRed:1.0 green:149.0/255.0 blue:0.0/255.0 alpha:1.0];
                
                break;
                
            case 3:
                
                _notificationPriorityTextLbl.text = @"Priority Low";
                
                _notificationPriorityView.backgroundColor = [UIColor colorWithRed:15.0/255.0 green:170.0/255.0 blue:99.0/255.0 alpha:1.0];
                
                break;
                
            default:
                break;
        }
        
        if ([self.notification.notificationIsHasAttachment intValue] == 1) {
            
            [_attachmentButton setHidden:NO];
        }
        else{
            
            [_attachmentButton setHidden:YES];
        }
    }
    
    /*NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transId MATCHES %@",self.notification.notificationTransId];
    
    assigneeArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"AssignedDetails" withPredicate:predicate];
    
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"assigneeType" ascending:YES];
    
    NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
    
    assigneeArr = [assigneeArr sortedArrayUsingDescriptors:descriptors];*/
    
    assigneeArr = [_notification.assignedList componentsSeparatedByString:ASSIGNEE_SAPRATOR];
    
    if (assigneeArr.count == 0) {
        
        [_assignedToView setHidden:YES];
    }
    else{
        
        [_assignedToView setHidden:NO];
        
        if (assigneeArr.count == 1) {
            
            //AssignedDetails *obj = [assigneeArr objectAtIndex:0];
            
            _assignedToTxtFld.text = [NSString stringWithFormat:@"Assigned to : %@",[assigneeArr objectAtIndex:0]];
        }
        else{
         
            [SINGLETON_INSTANCE addRightViewToTextField:_assignedToTxtFld withImageName:EXPAND_IMGNAME];
            
            _assignedToTxtFld.text = @"Assigned to :";
        }
    }
    
    UIBarButtonItem *approveBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"approve"] style:UIBarButtonItemStyleDone target:self action:@selector(approveButtonAction)];
    
    UIBarButtonItem *declineBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"decline"] style:UIBarButtonItemStyleDone target:self action:@selector(declineButtonAction)];
    
    self.navigationItem.rightBarButtonItems = @[approveBtn,declineBtn];
    
    self.navigationItem.title = @"Assignment Approval";
    
    self.navigationController.navigationBar.topItem.title = @"";
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{

    [TSAnalytics logScreen:ANALYTICS_SCREEN_APPROVALNOTIFICATION_DESCRIPTION parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@ Detail",SINGLETON_INSTANCE.barTitle];
}


#pragma mark - Button actions

-(void) approveButtonAction{
    
    UIAlertView *approveAlertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Do you want to Approve this Notification?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    
    [approveAlertView setTag:APPROVAL_TYPE_APPROVE];
    
    [approveAlertView show];
}

-(void) declineButtonAction{
    
    UIAlertView *declinedAlertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Do you want to Decline this Notification?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    
    [declinedAlertView setTag:APPROVAL_TYPE_DECLINE];
    
    [declinedAlertView show];
}

#pragma mark - TextField Delegates

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (assigneeArr.count>1) {
        
        AssignedListViewController *listVC = [[AssignedListViewController alloc]initWithNibName:@"AssignedListViewController" bundle:nil];
        
        [listVC setAssignedText:NotificationNotified];
        
        [listVC setAssignedListArray:assigneeArr];
        
        [listVC showInViewController:self];
    }
    
    return NO;
}

#pragma mark - AlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == APPROVAL_TYPE_APPROVE) {
        
        if (buttonIndex) {
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidUserTransID"];
            
            [param setValue:[NSString stringWithFormat:@"%ld",(long)APPROVAL_TYPE_APPROVE] forKey:@"intApprovedStatus"];
            
            [param setValue:@[self.notification.notificationTransId] forKey:@"guidNotificationTransID"];
            
            NSDictionary *updateParam = @{@"ApproveSchoolNotification":param};
            
            [self updateNotificationsWithParam:updateParam withApprovalType:APPROVAL_TYPE_APPROVE];
        }
    }
    else if (alertView.tag == APPROVAL_TYPE_DECLINE){
        
        if (buttonIndex) {
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidUserTransID"];
            
            [param setValue:[NSString stringWithFormat:@"%ld",(long)APPROVAL_TYPE_DECLINE] forKey:@"intApprovedStatus"];
            
            [param setValue:@[self.notification.notificationTransId] forKey:@"guidNotificationTransID"];
            
            NSDictionary *updateParam = @{@"ApproveSchoolNotification":param};
            
            [self updateNotificationsWithParam:updateParam withApprovalType:APPROVAL_TYPE_DECLINE];
        }
    }
    
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

#pragma mark - Helper methods

-(NSMutableAttributedString *) getDateAttributedStringForDateString:(NSString *)dateString inLabel:(UILabel *)label{
    
    [label setNumberOfLines:2];
    
    UIFont *dateFont,*monthFont;
    
    if (IS_IPHONE_4) {
        
        dateFont = [UIFont fontWithName:APP_FONT size:22.0f];
        
        monthFont = [UIFont fontWithName:APP_FONT size:10.0f];
    }
    else{
        
        dateFont = [UIFont fontWithName:APP_FONT size:25.0f];
        
        monthFont = [UIFont fontWithName:APP_FONT size:12.0f];
    }
    
    NSString *dateNo = [SINGLETON_INSTANCE changeDateFormatFromService:dateString withFormat:@"dd"];
    
    NSString *monthName = [SINGLETON_INSTANCE changeDateFormatFromService:dateString withFormat:@"MMM"];
    
    NSDictionary *dateTextAttr = [NSDictionary dictionaryWithObject: dateFont forKey:NSFontAttributeName];
    NSMutableAttributedString *countAttrStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@   ",dateNo] attributes: dateTextAttr];
    
    NSDictionary *monthTextAttr = [NSDictionary dictionaryWithObject:monthFont forKey:NSFontAttributeName];
    NSMutableAttributedString *textAttrStr = [[NSMutableAttributedString alloc]initWithString:monthName attributes:monthTextAttr];
    
    [countAttrStr appendAttributedString:textAttrStr];
    
    return countAttrStr;
}

- (IBAction)attachmentBtnClk:(id)sender {
    
    [_attachmentButton setUserInteractionEnabled:NO];
    
    NSString *attachmentURLString;
    
    attachmentURL = [NSURL URLWithString:self.notification.notificationAttachmentUrl];
    
    attachmentURLString = self.notification.notificationAttachmentUrl;
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *attachmentFile = [documentsPath stringByAppendingPathComponent:[attachmentURL lastPathComponent]];
    
    NSLog(@"%@",attachmentFile);
    
    NSLog(@"%@",[attachmentURL lastPathComponent]);
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:attachmentFile];
    
    if (fileExists) {
        
        NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
        
        [analyticsDict setValue:ANALYTICS_VALUE_OPENEDFROMLOCAL forKey:ANALYTICS_PARAM_ATTACHMENTSTATUS];
        
        [TSAnalytics logEvent:ANALYTICS_NOTIFAPPROVALS_CLICKED_ATTACHMENT parameter:analyticsDict];
        
        _attachmentButton.userInteractionEnabled = YES;

        [self navigateToAttachmentViewControllerWithFileName:attachmentURL];
    }
    else{
        
        [[TSHandlerClass sharedHandler]downloadAttachmentWithURL:attachmentURLString currentView:self.view withHandler:^(BOOL success, NSURL *filePath) {
            
            if (success) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_DOWNLOADING forKey:ANALYTICS_PARAM_ATTACHMENTSTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_NOTIFAPPROVALS_CLICKED_ATTACHMENT parameter:analyticsDict];
                
                _attachmentButton.userInteractionEnabled = YES;

                [self navigateToAttachmentViewControllerWithFileName:filePath];
            }
            else{
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_URLNOTFOUND forKey:ANALYTICS_PARAM_ATTACHMENTSTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_NOTIFAPPROVALS_CLICKED_ATTACHMENT parameter:analyticsDict];
                
                _attachmentButton.userInteractionEnabled = YES;
            }
        }];
    }
    
    
}

-(void) navigateToAttachmentViewControllerWithFileName:(NSURL *)fileURL{
    
    TSAttachmentViewController *attachmentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAttachment"];
    
    [attachmentVC setFileName:[fileURL lastPathComponent]];
    
    UINavigationController *navCtrl = [[UINavigationController alloc]initWithRootViewController:attachmentVC];
    
    navCtrl.navigationBar.translucent = NO;
    
    navCtrl.navigationBar.barTintColor = APPTHEME_COLOR;
    
    navCtrl.navigationBar.tintColor = [UIColor whiteColor];
    
    [navCtrl.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:APP_FONT size:18]}];
    
    [self.navigationController presentViewController:navCtrl animated:YES completion:nil];
}

#pragma mark - Service Call

// Update Assignments Service Call
-(void) updateNotificationsWithParam:(NSDictionary *)param withApprovalType:(NSInteger)approvalType{
    
    NSString *urlString = @"MastersMServices/SchoolNotificationsService.svc/MobileApproveSchoolNotification";
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"MobileApproveSchoolNotificationResult"][@"ResponseCode"] integerValue] == 1) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                if (approvalType == APPROVAL_TYPE_APPROVE) {
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                else{
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                
                [TSAnalytics logEvent:ANALYTICS_NOTIFAPPROVALS_UPDATEAPPROVAL parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"MobileApproveSchoolNotificationResult"][@"ResponseMessage"]];
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId MATCHES %@ AND isForApprovals == 1",SINGLETON_INSTANCE.staffTransId];
                
                [SINGLETON_INSTANCE deleteSelectedObjectsInEntityWithEntityName:@"Notifications" withPredicate:predicate withContext:[APPDELEGATE_INSTANCE managedObjectContext]];
                
                [[NSNotificationCenter defaultCenter]postNotificationName:@"updateTableView" object:nil];
                
                [self.navigationController popViewControllerAnimated:YES];
                
            }
            else{
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                if (approvalType == APPROVAL_TYPE_APPROVE) {
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                else{
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                
                [TSAnalytics logEvent:ANALYTICS_NOTIFAPPROVALS_UPDATEAPPROVAL parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
            }
        }
        else{
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
            
            if (approvalType == APPROVAL_TYPE_APPROVE) {
                
                [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
            }
            else{
                
                [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
            }
            
            [TSAnalytics logEvent:ANALYTICS_NOTIFAPPROVALS_UPDATEAPPROVAL parameter:analyticsDict];
        }
    }];
}

-(void) descViewTapped{
    
    [TSAnalytics logEvent:ANALYTICS_NOTIFAPPROVALS_CLICKED_DESCRIPTION parameter:[NSMutableDictionary new]];
    
    TSDescriptionViewController *descVC = [[TSDescriptionViewController alloc] initWithNibName:@"TSDescriptionViewController" bundle:nil];
    
    [descVC showInViewController:self descriptionString:_descriptionTextView.text];
    
}


@end