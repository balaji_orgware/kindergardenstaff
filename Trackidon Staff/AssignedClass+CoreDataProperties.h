//
//  AssignedClass+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by ORGware on 24/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AssignedClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface AssignedClass (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *assignmentTransID;
@property (nullable, nonatomic, retain) NSString *classname;
@property (nullable, nonatomic, retain) NSString *sectionName;
@property (nullable, nonatomic, retain) NSNumber *isStaff;
@property (nullable, nonatomic, retain) NSString *addlnRemarks;
@property (nullable, nonatomic, retain) NSString *transID;

@property (nullable, nonatomic, retain) Assignments *assignment;

@end

NS_ASSUME_NONNULL_END
