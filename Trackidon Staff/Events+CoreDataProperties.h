//
//  Events+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 28/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Events.h"

NS_ASSUME_NONNULL_BEGIN

@interface Events (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *acadamicTransId;
@property (nullable, nonatomic, retain) NSString *approvedByName;
@property (nullable, nonatomic, retain) NSString *approvedByTransId;
@property (nullable, nonatomic, retain) NSDate *approvedDate;
@property (nullable, nonatomic, retain) NSNumber *approvedStatus;
@property (nullable, nonatomic, retain) NSString *assignedList;
@property (nullable, nonatomic, retain) NSString *boardName;
@property (nullable, nonatomic, retain) NSString *boardTransID;
@property (nullable, nonatomic, retain) NSString *eventAttachmentUrl;
@property (nullable, nonatomic, retain) NSString *eventDescription;
@property (nullable, nonatomic, retain) NSDate *eventFromDate;
@property (nullable, nonatomic, retain) NSNumber *eventGroupType;
@property (nullable, nonatomic, retain) NSNumber *eventIsHasAttachment;
@property (nullable, nonatomic, retain) NSString *eventTime;
@property (nullable, nonatomic, retain) NSString *eventTitle;
@property (nullable, nonatomic, retain) NSDate *eventToDate;
@property (nullable, nonatomic, retain) NSString *eventTransId;
@property (nullable, nonatomic, retain) NSNumber *eventTypeId;
@property (nullable, nonatomic, retain) NSString *eventVenue;
@property (nullable, nonatomic, retain) NSNumber *isForApprovals;
@property (nullable, nonatomic, retain) NSDate *modifiedDate;
@property (nullable, nonatomic, retain) NSString *schoolTransId;
@property (nullable, nonatomic, retain) NSString *secondLangTransID;
@property (nullable, nonatomic, retain) NSString *staffTransId;
@property (nullable, nonatomic, retain) NSString *userCreatedName;
@property (nullable, nonatomic, retain) NSString *userCreatedTransId;
@property (nullable, nonatomic, retain) NSString *userModifiedName;
@property (nullable, nonatomic, retain) NSString *userModifiedTransId;
@property (nullable, nonatomic, retain) StaffDetails *staff;

@end

NS_ASSUME_NONNULL_END
