//
//  TSCommonListTableViewCell.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 20/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSCommonListTableViewCell.h"

@implementation TSCommonListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

-(void) loadContentForCell:(UITableViewCell *)tableViewCell withModuleType:(HealthType)moduleType withModelArray:(NSArray *)modelArray withRow:(NSInteger)row{
    
    TSCommonListTableViewCell *cell = (TSCommonListTableViewCell *)tableViewCell;
    
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:100];
    UILabel *examinedByName = (UILabel *)[cell viewWithTag:101];

    switch (moduleType) {
        case HealthType_Eyes:
        {
            EyeInfoModel *modelObj = [modelArray objectAtIndex:row];
            
            dateLabel.text = [SINGLETON_INSTANCE stringFromDate:modelObj.examinedDate withFormate:DATE_FORMATE_DAY_MNTH_YR];
            examinedByName.text = modelObj.examinedBy;
            
            break;
        }
            
        case HealthType_Ears:
        {
            EarInfoModel *modelObj = [modelArray objectAtIndex:row];
            
            dateLabel.text = [SINGLETON_INSTANCE stringFromDate:modelObj.examinedDate withFormate:DATE_FORMATE_DAY_MNTH_YR];
            examinedByName.text = modelObj.examinedBy;
            
            break;
        }
        
        case HealthType_Dental:
        {
            DentalInfoModel *modelObj = [modelArray objectAtIndex:row];
            
            dateLabel.text = [SINGLETON_INSTANCE stringFromDate:modelObj.examinedDate withFormate:DATE_FORMATE_DAY_MNTH_YR];
            examinedByName.text = modelObj.examinedBy;
            
            break;
        }
            
        case HealthType_NoseAndThroat:
        {
            NoseThroatInfoModel *modelObj = [modelArray objectAtIndex:row];
            
            dateLabel.text = [SINGLETON_INSTANCE stringFromDate:modelObj.examinedDate withFormate:DATE_FORMATE_DAY_MNTH_YR];
            examinedByName.text = modelObj.examinedBy;
            
            break;
        }
            
        case HealthType_GeneralCheckup:
        {
            GeneralCheckupModel *modelObj = [modelArray objectAtIndex:row];
            
            dateLabel.text = [SINGLETON_INSTANCE stringFromDate:modelObj.examinedDate withFormate:DATE_FORMATE_DAY_MNTH_YR];
            examinedByName.text = modelObj.examinedBy;
            
            break;
        }
            
        default:
            break;
    }
    
}

@end
