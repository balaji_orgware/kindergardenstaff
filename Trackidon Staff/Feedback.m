//
//  Feedback.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/11/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "Feedback.h"
#import "StaffDetails.h"

@implementation Feedback

// Method to Get Static Instance
+(instancetype)instance{
    
    static Feedback *feedbackObj = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        feedbackObj = [[Feedback alloc]init];
    });
    
    return feedbackObj;
}

-(void)storeFeedbacks:(StaffDetails *)staffObj withDict:(NSDictionary *)dictionary{
    
    @try {
    
    NSError *error = nil;
        
    [SINGLETON_INSTANCE deleteAllObjectsInEntity:@"Feedback"];
    
    NSManagedObjectContext *mainContext = [APPDELEGATE_INSTANCE managedObjectContext];
    
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    
    [context setParentContext:mainContext];
            
    NSManagedObjectID *objectId = [SINGLETON_INSTANCE.staff objectID];
    
    StaffDetails *tmpStaff = [context objectWithID:objectId];
    
    for (NSDictionary *dictObj in dictionary[@"FetchAllFeedBacksResult"][@"FeedBackClass"]) {
        
        NSDictionary *dict = [dictObj dictionaryByReplacingNullsWithBlanks];
        
        //Feedback *feedbackObj = [self isFeedbackEntryAlreadyExistsWithTransID:dict[@"TransID"] andParentObj:staffObj context:context];
        
        //if (feedbackObj == nil) {
        
            Feedback *feedbackObj = [NSEntityDescription insertNewObjectForEntityForName:@"Feedback" inManagedObjectContext:context];
            
        //}
        
        feedbackObj.acadamicYearTransID = dict[@"AcadamicYearTransID"];
        feedbackObj.classSection = dict[@"ClassSection"];
        feedbackObj.dateCreated = dict[@"DateCreated"];
        feedbackObj.feedbackDescription = dict[@"Description"];
        feedbackObj.feedBackType = dict[@"FeedBackType"];
        feedbackObj.feedBackTypeID = dict[@"FeedBackTypeID"];
        feedbackObj.parentMobileNo = dict[@"ParentMobileNo"];
        feedbackObj.parentName = dict[@"ParentName"];
        feedbackObj.studentName = dict[@"StudentName"];
        feedbackObj.studentTransID = dict[@"StudentTransID"];
        feedbackObj.title = dict[@"Title"];
        feedbackObj.transID = dict[@"TransID"];
        feedbackObj.staffTransID = SINGLETON_INSTANCE.staffTransId;
    
        feedbackObj.staff = tmpStaff;
        
        
        if (![context save:&error]) {
            
            NSLog(@"Failed to save Feedbacks");
        }
        
        [SINGLETON_INSTANCE storeContextInMainThread];
    }
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    }
}

-(Feedback *)isFeedbackEntryAlreadyExistsWithTransID:(NSString *)transID andParentObj:(StaffDetails *)staffObj context:(NSManagedObjectContext *)context{
    
    @try {
        
        NSError *error = nil;
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Feedback" inManagedObjectContext:context];
        
        [fetchRequest setReturnsObjectsAsFaults:NO];
        
        [fetchRequest setEntity:entity];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transID MATCHES %@",transID];
        
        [fetchRequest setPredicate:predicate];
        
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
        if (fetchedObjects.count>0)
            
            return [fetchedObjects objectAtIndex:0];
        
        else
            
            return nil;
        
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception.debugDescription);
    }
}

@end
