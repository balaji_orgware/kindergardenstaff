//
//  PortionInbox+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 28/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PortionInbox.h"

NS_ASSUME_NONNULL_BEGIN

@interface PortionInbox (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *approvedByName;
@property (nullable, nonatomic, retain) NSString *assignedList;
@property (nullable, nonatomic, retain) NSString *createdBy;
@property (nullable, nonatomic, retain) NSNumber *isHasAttachment;
@property (nullable, nonatomic, retain) NSDate *modifiedDate;
@property (nullable, nonatomic, retain) NSString *portionsAttachment;
@property (nullable, nonatomic, retain) NSDate *portionsDate;
@property (nullable, nonatomic, retain) NSString *portionsDescription;
@property (nullable, nonatomic, retain) NSString *portionsTitle;
@property (nullable, nonatomic, retain) NSString *staffTransID;
@property (nullable, nonatomic, retain) NSString *subjectName;
@property (nullable, nonatomic, retain) NSString *transID;
@property (nullable, nonatomic, retain) StaffDetails *staff;

@end

NS_ASSUME_NONNULL_END
