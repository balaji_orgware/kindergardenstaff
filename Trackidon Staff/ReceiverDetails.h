//
//  ReceiverDetails.h
//  Trackidon Staff
//
//  Created by Elango on 01/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StaffDetails;

NS_ASSUME_NONNULL_BEGIN

@interface ReceiverDetails : NSManagedObject

+ (instancetype)instance;

- (void)saveReceiverDetailsWithResponseDict:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END

#import "ReceiverDetails+CoreDataProperties.h"
