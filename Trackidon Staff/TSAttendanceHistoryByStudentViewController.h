//
//  TSAttendanceHistoryByStudentViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 31/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TSSelectionViewController,StudentModel;

@interface TSAttendanceHistoryByStudentViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,datePickerDelegate,SelectionCallBackDelegate,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *classSelectionView;
@property (weak, nonatomic) IBOutlet UITextField *selectClassTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *selectBoardTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *selectSectionTxtFld;

@property (weak, nonatomic) IBOutlet UIView *studentSelectionView;
@property (weak, nonatomic) IBOutlet UITextField *selectStudentTxtFld;

@property (weak, nonatomic) IBOutlet UIView *dateSelectionView;
@property (weak, nonatomic) IBOutlet UIButton *fromDateButton;
@property (weak, nonatomic) IBOutlet UIButton *toDateButton;
@property (weak, nonatomic) IBOutlet UILabel *fromDateNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *toDateNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *toDateMonYrLabel;
@property (weak, nonatomic) IBOutlet UILabel *fromDateMonYrLabel;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@property (weak, nonatomic) IBOutlet UIView *totalNoOfDaysView;
@property (weak, nonatomic) IBOutlet UILabel *noOfDaysLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic,assign) NSInteger totalPresent;
@property (nonatomic,assign) NSInteger totalAbsent;
@property (nonatomic,assign) NSInteger totalTardy;
@property (nonatomic,assign) NSInteger totalWorkingDays;

@property (weak, nonatomic) IBOutlet UIView *blackView;
@property (weak, nonatomic) IBOutlet UITableView *dateTableView;

// Button Actions
- (IBAction)fromDateButtonTapped:(id)sender;

- (IBAction)toDateButtonTapped:(id)sender;

- (IBAction)submitButtonTapped:(id)sender;

@end