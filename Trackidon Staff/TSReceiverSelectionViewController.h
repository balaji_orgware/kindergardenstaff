//
//  TSCommonCreateViewController.h
//  Trackidon Staff
//
//  Created by Elango on 01/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, RECEIVER_LIST_TYPE) {
    RECEIVER_LIST_TYPE_BOARD,
    RECEIVER_LIST_TYPE_FILTER,
    RECEIVER_LIST_TYPE_FILTEROPTION,
    RECEIVER_LIST_TYPE_RECEIPIENT,
    RECEIVER_LIST_TYPE_GROUP,
    RECEIVER_LIST_TYPE_CLASS,
    RECEIVER_LIST_TYPE_SECTION,
    RECEIVER_LIST_TYPE_STUDENT,
    RECEIVER_LIST_TYPE_STAFF,
};

#define SELECTED_COLOR COLOR_ACCEPT_CREATEDTODAY

#define UNSELECTED_COLOR [UIColor colorWithRed:238.0/255.0 green:82.0/255.0 blue:87.0/255.0 alpha:1.0]


#define FILTER_TYPE_NONE @"0"

#define RECEPIENT_TYPE_ENTIRESCHOOL @"1"//@"EntireSchool"
#define RECEPIENT_TYPE_ALLSTUDENT @"2"//@"AllStudents"
#define RECEPIENT_TYPE_ALLSTAFF @"3"//@"AllStaffs"
#define RECEPIENT_TYPE_CLASS @"4"//@"Class"
#define RECEPIENT_TYPE_SECTION @"5"//@"Section"
#define RECEPIENT_TYPE_STUDENT @"6"//@"Student"
#define RECEPIENT_TYPE_STAFF @"7"//@"Staff"
#define RECEPIENT_TYPE_GROUP @"8"//@"Group"

#define RADIO_BUTTON_ON @"radio_on"
#define RADIO_BUTTON_OFF @"radio_off"
#define CHECKBOX_BUTTON_ON @"check"
#define CHECKBOX_BUTTON_OFF @"uncheck"

#define TEXT_SELECT_ALL @"Select All"


@interface TSReceiverSelectionViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollVw;
@property (weak, nonatomic) IBOutlet UIView *listVw;
@property (weak, nonatomic) IBOutlet UIView *listHeaderVw;
@property (weak, nonatomic) IBOutlet UITableView *listTblVw;

@property (weak, nonatomic) IBOutlet UIView *boardSelctionVw;
@property (weak, nonatomic) IBOutlet UILabel *boardSelectionLbl;

@property (weak, nonatomic) IBOutlet UIView *filterSelectionVw;
@property (weak, nonatomic) IBOutlet UILabel *filterSelectionLbl;

@property (weak, nonatomic) IBOutlet UIView *filterOptionSelectionVw;
@property (weak, nonatomic) IBOutlet UILabel *filterOptionTiltleLbl;
@property (weak, nonatomic) IBOutlet UILabel *filterOptionSelectionLbl;

@property (weak, nonatomic) IBOutlet UIView *receipientVw;

@property (weak, nonatomic) IBOutlet UIView *receipientSelectionVw;
@property (weak, nonatomic) IBOutlet UILabel *receipientSelectionLbl;

@property (weak, nonatomic) IBOutlet UIView *classGroupSelectionVw;

@property (weak, nonatomic) IBOutlet UILabel *classGroupTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *classGroupSelctionLbl;

@property (weak, nonatomic) IBOutlet UIView *sectionSelectionVw;
@property (weak, nonatomic) IBOutlet UILabel *sectionSelectionLbl;

@property (weak, nonatomic) IBOutlet UIView *studentSelectionVw;
@property (weak, nonatomic) IBOutlet UILabel *studentSelectionLbl;

@property (weak, nonatomic) IBOutlet UIView *staffSelectionVw;
@property (weak, nonatomic) IBOutlet UILabel *staffSelectionLbl;

@property (nonnull, nonatomic, strong)NSString *menuId;
@property (nonnull, nonatomic, strong)NSMutableDictionary *commonDict;
@property (nonatomic, assign)NSInteger communicationType;


@end

@interface BoardModel : NSObject

@property (nullable, nonatomic, strong) NSString *boardName;
@property (nullable, nonatomic, retain) NSString *boardTransId;
@property (nullable, nonatomic, retain) NSString *schoolName;
@property (nullable, nonatomic, retain) NSString *schoolTransId;
@property (nonatomic, assign) BOOL isSelected;

@end

@interface ReceiverFilterListModel : NSObject

@property (nullable, nonatomic, retain) NSString *filterDescription;
@property (nullable, nonatomic, retain) NSString *filterTypeId;
@property (nullable, nonatomic, retain) NSString *menuId;
@property (nonatomic, assign) BOOL isSelected;

@end

@interface ReceiverReceipientListModel : NSObject

@property (nullable, nonatomic, retain) NSString *receipientDescription;
@property (nullable, nonatomic, retain) NSString *receipientTypeId;
@property (nullable, nonatomic, retain) NSString *menuId;
@property (nonatomic, assign) BOOL isSelected;

@end

@interface ReceiverDetailsModel : NSObject

@property (nullable, nonatomic, retain) NSString *boardTransId;
@property (nullable, nonatomic, retain) NSString *receiverDescription;
@property (nullable, nonatomic, retain) NSString *receiverTransId;
@property (nullable, nonatomic, retain) NSString *receiverTypeId;
@property (nullable, nonatomic, retain) NSString *boardName;
@property (nonatomic, assign) BOOL isSelected;

@end

@interface ClassesModel : NSObject

@property (nullable, nonatomic, retain) NSString *boardTransId;
@property (nullable, nonatomic, retain) NSString *boardName;
@property (nullable, nonatomic, retain) NSString *classStandardName;
@property (nullable, nonatomic, retain) NSString *classTransId;
@property (nullable, nonatomic, retain) NSString *schoolTransId;
@property (nonatomic, assign) BOOL isSelected;

@end

@interface SectionModel : NSObject

@property (nullable, nonatomic, retain) NSString *boardTransId;
@property (nullable, nonatomic, retain) NSString *classStandardName;
@property (nullable, nonatomic, retain) NSString *classTransId;
@property (nullable, nonatomic, retain) NSString *schoolTransId;
@property (nullable, nonatomic, retain) NSString *sectionName;
@property (nullable, nonatomic, retain) NSString *sectionTransId;
@property (nullable, nonatomic, retain) NSString *boardName;
@property (nonatomic, assign) BOOL isSelected;

@end

@interface StaffModel : NSObject

@property (nullable, nonatomic, retain) NSString *staffName;
@property (nullable, nonatomic, retain) NSString *staffTransId;
@property (nullable, nonatomic, retain) NSString *schoolTransId;
@property (nullable, nonatomic, retain) NSString *boardTransId;
@property (nullable, nonatomic, retain) NSString *boardName;
@property (nullable, nonatomic, retain) NSString *schoolName;
@property (nonatomic, assign) BOOL isSelected;

@end

@interface GroupModel : NSObject

@property (nullable, nonatomic, retain) NSString *boardTransId;
@property (nullable, nonatomic, retain) NSString *boardName;
@property (nullable, nonatomic, retain) NSString *schoolTransId;
@property (nullable, nonatomic, retain) NSString *schoolName;
@property (nullable, nonatomic, retain) NSString *groupTransId;
@property (nullable, nonatomic, retain) NSString *groupName;
@property (nonatomic, assign) BOOL isSelected;

@end


