//
//  TSCommonCreateEyeEarNoseViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 09/08/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSCommonCreateEyeEarNoseViewController.h"

#define TEXT_LeftEyeDesc @"Left Eye description"
#define TEXT_RightEyeDesc @"Right Eye description"
#define TEXT_LeftEarDesc @"Left Ear description"
#define TEXT_RightEarDesc @"Right Ear description"
#define TEXT_NoseDesc @"Nose test remarks"
#define TEXT_ThroatDesc @"Throat test remarks"

@interface TSCommonCreateEyeEarNoseViewController (){
    NSDate *selectedDate;
    CGRect viewRect, currentVisibleRect;
    UIView *currentResponder;
}

@end

@implementation UITextField (custom)

- (CGRect)textRectForBounds:(CGRect)bounds {
    
    if ([[[TSAttachmentPicker instance] getCurrentViewController] isKindOfClass:[TSCommonCreateEyeEarNoseViewController class]] || [[[TSAttachmentPicker instance] getCurrentViewController] isKindOfClass:[TSCreateGeneralCheckupDentalViewController class]]) {
    
        return CGRectMake(bounds.origin.x , bounds.origin.y,bounds.size.width - 20,bounds.size.height);
    }
    else{
        return CGRectMake(bounds.origin.x+10, bounds.origin.y, bounds.size.width-20, bounds.size.height);
    }
}

- (CGRect)editingRectForBounds:(CGRect)bounds {

    return [self textRectForBounds:bounds];
}
 
@end

@implementation TSCommonCreateEyeEarNoseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    selectedDate = [NSDate date];
    
    _dateLabel.text = [SINGLETON_INSTANCE stringFromDate:selectedDate withFormate:DATE_FORMATE_DAY_MNTH_YR];
    
    [TSSingleton layerDrawForView:_examineDateView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_examinedByView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_firstInfoView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_secondInfoView position:LAYER_TOP color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_secondInfoView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
//    [TSSingleton layerDrawForView:_secondInfoTxtVw position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    [SINGLETON_INSTANCE addDoneButtonForTextView:_firstInfoTxtVw];
    [SINGLETON_INSTANCE addDoneButtonForTextView:_secondInfoTxtVw];
    
    for (id obj in _scrollView.subviews) {
        
        if ([obj isKindOfClass:[UITextView class]]) {
            
            UITextView *txtVw = (UITextView *)obj;
            txtVw.textColor = APP_PLACEHOLDER_COLOR;
            txtVw.font = [UIFont fontWithName:APP_FONT size:16];
            txtVw.textContainerInset = UIEdgeInsetsMake(15,10,10,15); // top, left, bottom, right
        }
    }
    
    switch (_healthType) {
        case HealthType_Eyes:
            
            _firstInfoLblName.text = @"Left Eye";
            _firstInfoTxtFld.placeholder = @"Left Eye title";
            _firstInfoTxtVw.text = TEXT_LeftEyeDesc;
            
            _secondInfoLblName.text = @"Right Eye";
            _secondInfoTxtFld.placeholder = @"Right Eye title";
            _secondInfoTxtVw.text = TEXT_RightEyeDesc;
            
            break;
            
        case HealthType_Ears:
            
            _firstInfoLblName.text = @"Left Ear";
            _firstInfoTxtFld.placeholder = @"Left Ear title";
            _firstInfoTxtVw.text = TEXT_LeftEarDesc;
            
            _secondInfoLblName.text = @"Right Ear";
            _secondInfoTxtFld.placeholder = @"Right Ear title";
            _secondInfoTxtVw.text = TEXT_RightEarDesc;
            
            break;
            
        case HealthType_NoseAndThroat:
            
            _firstInfoLblName.text = @"Nose";
            _firstInfoTxtFld.placeholder = @"Nose test info";
            _firstInfoTxtVw.text = TEXT_NoseDesc;
            
            _secondInfoLblName.text = @"Throat";
            _secondInfoTxtFld.placeholder = @"Throat test info";
            _secondInfoTxtVw.text = TEXT_ThroatDesc;
            break;
            
        default:
            break;
    }
    
    UITapGestureRecognizer *tapOnDateSelection = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dateViewTapped)];
    tapOnDateSelection.delegate = self;
    
    [_examineDateView addGestureRecognizer:tapOnDateSelection];
    
    viewRect = self.view.frame;

    [_scrollView setContentSize:CGSizeMake(DeviceWidth, _secondInfoTxtVw.frame.origin.y+_secondInfoTxtVw.frame.size.height)];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    NSString *title;
    
    switch (_healthType) {
        case HealthType_Eyes:
            title = @"Eyes Information";
            break;
        case HealthType_Ears:
            title = @"Ears Information";
            break;
        case HealthType_NoseAndThroat:
            title = @"Nose and Throat Information";
            break;
        default:
            break;
    }

    self.navigationItem.title = title;
    self.navigationController.navigationBar.topItem.title = @"";
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Alert View Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 1000) {
        
        if (buttonIndex) {
            
            NSMutableDictionary *dict = [NSMutableDictionary  new];
            
            NSDictionary *param = [NSDictionary new];
            
            [dict setValue:EMPTY_TRANSID forKey:@"TransID"];
            [dict setValue:[_generalInfoDict valueForKey:@"HCTransID"] forKey:@"HCTransID"];
            [dict setValue:SINGLETON_INSTANCE.staffTransId forKey:@"ExaminerID"];
            [dict setValue:_examinedByTxtFld.text forKey:@"ExaminedBy"];
            [dict setValue:[SINGLETON_INSTANCE stringFromDate:selectedDate withFormate:@"dd-MMMM-yyyy"] forKey:@"ExaminedDate"];
            [dict setValue:SINGLETON_INSTANCE.staff.mobileNo forKey:@"Mobileno"];
            [dict setValue:@"" forKey:@"Remarks"];
            [dict setValue:@"" forKey:@"AddlnRemarks"];
            [dict setValue:SINGLETON_INSTANCE.staffTransId forKey:@"UserTransID"];
            
            switch (_healthType) {
                case HealthType_Ears:
                {
                    NSString *firstInfoRemarks = [_firstInfoTxtVw.text isEqualToString:TEXT_LeftEarDesc] ? @"" : _firstInfoTxtVw.text;
                    NSString *secondInfoRemarks = [_secondInfoTxtVw.text isEqualToString:TEXT_RightEarDesc] ? @"" : _secondInfoTxtVw.text;
                    
                    [dict setValue:_firstInfoTxtFld.text forKey:@"LeftEarTitle"];
                    [dict setValue:firstInfoRemarks forKey:@"LeftEarRemarks"];
                    [dict setValue:_secondInfoTxtFld.text forKey:@"RightEarTitle"];
                    [dict setValue:secondInfoRemarks forKey:@"RightEarRemarks"];
                    
                    param = @{@"EarInfo":dict};
                    break;
                }
                case HealthType_Eyes:
                {
                    NSString *firstInfoRemarks = [_firstInfoTxtVw.text isEqualToString:TEXT_LeftEyeDesc] ? @"" : _firstInfoTxtVw.text;
                    NSString *secondInfoRemarks = [_secondInfoTxtVw.text isEqualToString:TEXT_RightEyeDesc] ? @"" : _secondInfoTxtVw.text;
                    
                    [dict setValue:_firstInfoTxtFld.text forKey:@"LeftEyeTitle"];
                    [dict setValue:firstInfoRemarks forKey:@"LeftEyeRemarks"];
                    [dict setValue:_secondInfoTxtFld.text forKey:@"RightEyeTitle"];
                    [dict setValue:secondInfoRemarks forKey:@"RightEyeRemarks"];
                    
                    param = @{@"EyeInfo":dict};
                    break;
                }
                case HealthType_NoseAndThroat:
                {
                    NSString *firstInfoRemarks = [_firstInfoTxtVw.text isEqualToString:TEXT_NoseDesc] ? @"" : _firstInfoTxtVw.text;
                    NSString *secondInfoRemarks = [_secondInfoTxtVw.text isEqualToString:TEXT_ThroatDesc] ? @"" : _secondInfoTxtVw.text;
                    
                    [dict setValue:_firstInfoTxtFld.text forKey:@"NoseTitle"];
                    [dict setValue:firstInfoRemarks forKey:@"NoseDescription"];
                    [dict setValue:_secondInfoTxtFld.text forKey:@"ThroatTitle"];
                    [dict setValue:secondInfoRemarks forKey:@"ThroatDescription"];
                    
                    param = @{@"NoseThroatInfo":dict};
                    break;
                }
                default:
                    break;
            }
            
            [self updateServiceCall:param];
        }
        else{
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        }
    }
}

#pragma mark - Button Action

- (IBAction)doneButtonTapped:(id)sender {
    
    if (_examinedByTxtFld.text.length == 0) {
        [TSSingleton showAlertWithMessage:@"Please enter Examiner name"];
        return;
    }
    else{
        
        _firstInfoTxtFld.text = [_firstInfoTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        _secondInfoTxtFld.text = [_secondInfoTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        _firstInfoTxtVw.text = [_firstInfoTxtVw.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        _secondInfoTxtVw.text = [_secondInfoTxtVw.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        switch (_healthType) {
            case HealthType_Ears:
            {
                if (_firstInfoTxtFld.text.length == 0) {
                    [TSSingleton showAlertWithMessage:@"Left Ear info can't be empty"];
                    return;
                }
                else if(_firstInfoTxtFld.text.length >100){
                    [TSSingleton showAlertWithMessage:@"Left Ear info can't exceed 100 characters"];
                    return;
                }
                else if (_firstInfoTxtVw.text.length >500){
                    [TSSingleton showAlertWithMessage:@"Left Ear Desc can't exceed 500 characters"];
                    return;
                }
                else if (_secondInfoTxtFld.text.length == 0){
                    [TSSingleton showAlertWithMessage:@"Right Ear info can't  be empty"];
                    return;
                }
                else if (_secondInfoTxtFld.text.length >100){
                    [TSSingleton showAlertWithMessage:@"Right Ear info can't exceed 100 characters"];
                    return;
                }
                else if (_secondInfoTxtVw.text.length >500){
                    [TSSingleton showAlertWithMessage:@"Right Ear Desc can't exceed 500 characters"];
                    return;
                }
                break;
            }
                
            case HealthType_Eyes:
            {
                if (_firstInfoTxtFld.text.length == 0) {
                    [TSSingleton showAlertWithMessage:@"Left Eye info can't be empty"];
                    return;
                }
                else if(_firstInfoTxtFld.text.length >100){
                    [TSSingleton showAlertWithMessage:@"Left Eye info can't exceed 100 characters"];
                    return;
                }
                else if (_firstInfoTxtVw.text.length >500){
                    [TSSingleton showAlertWithMessage:@"Left Eye Desc can't exceed 500 characters"];
                    return;
                }
                else if (_secondInfoTxtFld.text.length == 0){
                    [TSSingleton showAlertWithMessage:@"Right Eye info can't  be empty"];
                    return;
                }
                else if (_secondInfoTxtFld.text.length >100){
                    [TSSingleton showAlertWithMessage:@"Right Eye info can't exceed 100 characters"];
                    return;
                }
                else if (_secondInfoTxtVw.text.length >500){
                    [TSSingleton showAlertWithMessage:@"Right Eye Desc can't exceed 500 characters"];
                    return;
                }
                break;
            }
                
            case HealthType_NoseAndThroat:
            {
                if (_firstInfoTxtFld.text.length == 0) {
                    [TSSingleton showAlertWithMessage:@"Nose info can't be empty"];
                    return;
                }
                else if(_firstInfoTxtFld.text.length >150){
                    [TSSingleton showAlertWithMessage:@"Nose info can't exceed 100 characters"];
                    return;
                }
                else if (_firstInfoTxtVw.text.length >500){
                    [TSSingleton showAlertWithMessage:@"Nose Desc can't exceed 500 characters"];
                    return;
                }
                else if (_secondInfoTxtFld.text.length == 0){
                    [TSSingleton showAlertWithMessage:@"Throat info can't  be empty"];
                    return;
                }
                else if (_secondInfoTxtFld.text.length >150){
                    [TSSingleton showAlertWithMessage:@"Throat info can't exceed 100 characters"];
                    return;
                }
                else if (_secondInfoTxtVw.text.length >500){
                    [TSSingleton showAlertWithMessage:@"Throat Desc can't exceed 500 characters"];
                    return;
                }
                break;
            }
                
            default:
                break;
        }
    }
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Are you sure want to update ?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alertView.tag = 1000;
    [alertView show];
}

-(void) dateViewTapped{
    
    [self.view endEditing:YES];
    
    TSDatePickerViewController *datePickerVC = [[TSDatePickerViewController alloc]initWithNibName:@"TSDatePickerViewController" bundle:nil];
    
    datePickerVC.delegate = self;
    
    [datePickerVC showInViewController:self calendarType:CALENDAR_TYPE_DATE withMinDate:nil withMaxDate:nil];
}

#pragma mark - Date Picker Delegate methods

-(void)selectedDate:(NSDate *)date{
    
    selectedDate = date;
    
    _dateLabel.text = [SINGLETON_INSTANCE stringFromDate:selectedDate withFormate:DATE_FORMATE_DAY_MNTH_YR];
}

#pragma mark - TextField Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{

    currentResponder = textField;
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - TextView Delegate

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    currentResponder = textView;
    
    if ([textView.text isEqualToString:TEXT_LeftEyeDesc] || [textView.text isEqualToString:TEXT_RightEyeDesc] || [textView.text isEqualToString:TEXT_LeftEarDesc] || [textView.text isEqualToString:TEXT_RightEarDesc] || [textView.text isEqualToString:TEXT_NoseDesc] || [textView.text isEqualToString:TEXT_ThroatDesc]) {
        
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
        textView.font = [UIFont fontWithName:APP_FONT size:14];
    }
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {
     
        switch (_healthType) {
            case HealthType_Eyes:
            {
                if (textView == _firstInfoTxtVw) {
                    textView.text = TEXT_LeftEyeDesc;
                }
                else{
                    textView.text = TEXT_RightEyeDesc;
                }
                break;
            }
                
            case HealthType_Ears:
            {
                if (textView == _firstInfoTxtVw) {
                    textView.text = TEXT_LeftEarDesc;
                }
                else{
                    textView.text = TEXT_RightEarDesc;
                }
                break;
            }
                
            case HealthType_NoseAndThroat:
            {
                if (textView == _firstInfoTxtVw) {
                    textView.text = TEXT_NoseDesc;
                }
                else{
                    textView.text = TEXT_ThroatDesc;
                }
                break;
            }
            default:
                break;
        }
        textView.textColor = [UIColor lightGrayColor];
        textView.font = [UIFont fontWithName:APP_FONT size:17];
    }
    
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    
    if (newFrame.size.height >50) {
        textView.frame = newFrame;
    }
    else{
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), 50);
        textView.frame = newFrame;
    }
    
    [self rearrangeControls];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_scrollView setContentSize:CGSizeMake(DeviceWidth,_secondInfoTxtVw.frame.origin.y+_secondInfoTxtVw.frame.size.height)];
    });
}

-(void)textViewDidChange:(UITextView *)textView{
    
    [self rearrangeControls];
}

#pragma mark - Helper Methods

-(void) keyboardShown:(NSNotification *)notification{
    
    NSDictionary* info = [notification userInfo];
    
    CGSize currentKeyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    CGRect textFieldFrame;
    
    if (currentResponder == _firstInfoTxtFld) {
        textFieldFrame = _firstInfoView.frame;
    }
    else if (currentResponder == _secondInfoTxtFld){
        textFieldFrame = _secondInfoView.frame;
    }
    
    CGPoint textFieldOrigin = CGPointMake(currentResponder.frame.origin.x, currentResponder.frame.origin.y+textFieldFrame.origin.y);
    textFieldOrigin.y = textFieldOrigin.y + currentResponder.frame.size.height;
    CGFloat textFieldHeight = currentResponder.frame.size.height;
    
    CGRect visibleRect = viewRect;
    visibleRect.size.height -= currentKeyboardSize.height + textFieldHeight;
    currentVisibleRect = visibleRect;
    
    if ([_firstInfoTxtVw isFirstResponder]) {
        
        CGRect descRect = viewRect;
        
        descRect.size.height -= currentKeyboardSize.height;
        
        _firstInfoTxtVw.frame = CGRectMake(_firstInfoTxtVw.frame.origin.x, _firstInfoTxtVw.frame.origin.y, _firstInfoTxtVw.frame.size.width, descRect.size.height - 64 );
        
        [self rearrangeControls];
        
        CGPoint scrollPoint = CGPointMake(0.0, _firstInfoTxtVw.frame.origin.y);
        
        [_scrollView setContentOffset:scrollPoint animated:YES];
        
        [_scrollView setScrollEnabled:NO];
    }
    else if ([_secondInfoTxtVw isFirstResponder]){
        
        CGRect descRect = viewRect;
        
        descRect.size.height -= currentKeyboardSize.height;
        
        _secondInfoTxtVw.frame = CGRectMake(_secondInfoTxtVw.frame.origin.x, _secondInfoTxtVw.frame.origin.y, _secondInfoTxtVw.frame.size.width, descRect.size.height - 64 );
        
        [self rearrangeControls];
        
        CGPoint scrollPoint = CGPointMake(0.0, _secondInfoTxtVw.frame.origin.y);
        
        [_scrollView setContentOffset:scrollPoint animated:YES];
        
        [_scrollView setScrollEnabled:NO];
    }
    else if (!CGRectContainsPoint(visibleRect, textFieldOrigin))
    {
        CGPoint scrollPoint = CGPointMake(0.0, textFieldOrigin.y - visibleRect.size.height  + textFieldHeight);
        
        [_scrollView setContentOffset:scrollPoint animated:YES];
    }
}

-(void) keyboardHide:(NSNotification *)notification{
    
    [_scrollView setScrollEnabled:YES];
    
    [_scrollView setContentOffset:CGPointZero animated:YES];
}

-(void) rearrangeControls{

    if (currentResponder == _firstInfoTxtVw) {
        _secondInfoView.frame = CGRectMake(_secondInfoView.frame.origin.x, _firstInfoTxtVw.frame.origin.y+_firstInfoTxtVw.frame.size.height, _secondInfoView.frame.size.width, _secondInfoView.frame.size.height);
        _secondInfoTxtVw.frame = CGRectMake(_secondInfoView.frame.origin.x, _secondInfoView.frame.origin.y+_secondInfoView.frame.size.height, _secondInfoTxtVw.frame.size.width,_secondInfoTxtVw.frame.size.height);
    }
}

#pragma mark - Update Service Call

-(void) updateServiceCall : (NSDictionary *)param{
    
    NSString *urlString = @"HealthDetails/Services/HealthCardService.svc/UpdateHealthCardInfo";
    
    NSDictionary *params = @{@"ObjUpdateHealthCard":param};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"UpdateHealthCardInfoResult"][@"ResponseCode"] integerValue] == 1) {
                
                SINGLETON_INSTANCE.needRefreshHealthRecords = YES;
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"UpdateHealthCardInfoResult"][@"ResponseMessage"]];
                
                NSArray *viewControllers = [self.navigationController viewControllers];
                
                NSUInteger currentIndex = [viewControllers indexOfObject:self];
                
                [self.navigationController popToViewController:[viewControllers objectAtIndex:currentIndex-2] animated:YES];
            }
            else{
                SINGLETON_INSTANCE.needRefreshHealthRecords = NO;
            }
        }
        else{
            SINGLETON_INSTANCE.needRefreshHealthRecords = NO;
        }
    }];
}

@end