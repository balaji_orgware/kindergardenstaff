//
//  TSTemperatureHistorySelectionViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 13/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSTemperatureHistorySelectionViewController.h"

@interface TSTemperatureHistorySelectionViewController (){
    
    NSArray *boardArray;
    
    NSMutableArray *studentsListArray;
    
    NSString *selectedBoardTransID,*selectedClassTransID,*selectedSectionTransID,*trustTransID;
    
    TSSelectionViewController *selectionVC;
}

@end

@implementation TSTemperatureHistorySelectionViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    studentsListArray = [NSMutableArray new];
    
    [SINGLETON_INSTANCE addLeftInsetToTextField:_boardTxtFld];
    [SINGLETON_INSTANCE addLeftInsetToTextField:_classTxtFld];
    [SINGLETON_INSTANCE addLeftInsetToTextField:_sectionTxtFld];
    
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_boardTxtFld];
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_classTxtFld];
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_sectionTxtFld];
    
    [TSSingleton layerDrawForView:_boardTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_classTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_sectionTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_classTxtFld position:LAYER_RIGHT color:[UIColor lightGrayColor]];
    
    NSPredicate *boardPredicate = [NSPredicate predicateWithFormat:@"schoolTransId contains[cd] %@ AND isViewableByStaff == 1",SINGLETON_INSTANCE.selectedSchoolTransId];
    
    boardArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Board" withPredicate:boardPredicate];
    
    if (boardArray.count == 1) {
        Board *boardObj = [boardArray objectAtIndex:0];
        
        _boardTxtFld.text = boardObj.boardName;
        selectedBoardTransID = boardObj.boardTransId;
        
        _selectionView.frame = CGRectMake(_selectionView.frame.origin.x, _selectionView.frame.origin.y-_boardTxtFld.frame.size.height, _selectionView.frame.size.width, _selectionView.frame.size.height);
        
        _studentTableView.frame = CGRectMake(_studentTableView.frame.origin.x, _selectionView.frame.origin.y+_selectionView.frame.size.height, _studentTableView.frame.size.width, _studentTableView.frame.size.height+_boardTxtFld.frame.size.height);
    }
    
    [_studentTableView setTableFooterView:[UIView new]];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationItem.title = @"Temperature History";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Selection View

-(void)showSelectionViewControllerWithSelectedTextField:(UITextField *)textField withSelectedValue:(NSString *)selected{
    
    if (!selectionVC) {
        
        selectionVC = [[TSSelectionViewController alloc]initWithNibName:@"TSSelectionViewController" bundle:nil];
        
        selectionVC.delegate = self;
        
        NSMutableDictionary *contentDict = [NSMutableDictionary new];
        
        if (textField == _boardTxtFld) {
            
            for (Board *boardObj  in boardArray) {
                
                [contentDict setValue:boardObj.boardTransId forKey:boardObj.boardName];
            }
        }
        else if (textField == _classTxtFld){
            
            NSPredicate *classPredicate = [NSPredicate predicateWithFormat:@"boardTransId MATCHES %@",selectedBoardTransID];
            Board *boardObj = [boardArray filteredArrayUsingPredicate:classPredicate].count>0 ? [[boardArray filteredArrayUsingPredicate:classPredicate] objectAtIndex:0] : nil;
            
            for (Classes *classObj in boardObj.classes) {
                [contentDict setValue:classObj.classTransId forKey:classObj.classStandardName];
            }
        }
        else if (textField == _sectionTxtFld){
            
            NSPredicate *boardPredicate = [NSPredicate predicateWithFormat:@"boardTransId MATCHES %@",selectedBoardTransID];
            Board *boardObj = [boardArray filteredArrayUsingPredicate:boardPredicate].count>0 ? [[boardArray filteredArrayUsingPredicate:boardPredicate] objectAtIndex:0] : nil;
            
            NSPredicate *classPredicate = [NSPredicate predicateWithFormat:@"classTransId MATCHES %@",selectedClassTransID];
            
            Classes *classObj = [[boardObj.classes filteredSetUsingPredicate:classPredicate] allObjects].count >0 ? [[[boardObj.classes filteredSetUsingPredicate:classPredicate] allObjects] objectAtIndex:0] : nil;
            
            for (Section *secObj in classObj.section) {
                
                [contentDict setValue:secObj.sectionTransId forKey:secObj.sectionName];
            }
        }
        
        if (contentDict.allKeys.count >0) {
            
            [selectionVC setContentDict:contentDict];
            [selectionVC setSelectedValue:selected];
            
            [selectionVC showSelectionViewController:self forTextField:textField];
        }
        else{
            if (textField == _boardTxtFld) {
                
                [TSSingleton showAlertWithMessage:@"No boards to Choose"];
            }
            else if (textField == _classTxtFld){
                
                [TSSingleton showAlertWithMessage:@"No Classes to Choose"];
            }
            else if (textField == _sectionTxtFld){
                
                [TSSingleton showAlertWithMessage:@"No Sections to Choose"];
            }
            selectionVC = nil;
        }
    }
}

#pragma mark - SelectionCallBack Delegate

-(void)selectedValueFromDict:(NSDictionary *)contentDict withSelectedValue:(NSString *)selectedValue andTransID:(NSString *)transID forTextField:(UITextField *)textField{
    
    if (textField == _boardTxtFld) {
        
        selectedBoardTransID = transID;
    }
    else if (textField  == _classTxtFld){
        
        selectedClassTransID = transID;
    }
    else if(textField == _sectionTxtFld){
        
        selectedSectionTransID = transID;
        
        [self fetchStudentListAPICall];
    }
    selectionVC = nil;
}

-(void)backViewTappedOn:(UIViewController *)viewController{
    selectionVC = nil;
}

#pragma mark - TableView Datasource & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return studentsListArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:100];
    
    StudentModel *studentObj = [studentsListArray objectAtIndex:indexPath.row];
    
    nameLabel.text = studentObj.studentName;
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    StudentModel *studentObj = [studentsListArray objectAtIndex:indexPath.row];
    
    TSTemperatureHistoryViewController *historyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBTemperatureHistory"];
    
    [historyVC setStudentObj:studentObj];
    
    [self.navigationController pushViewController:historyVC animated:YES];
}


#pragma mark - TextField Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField == _boardTxtFld) {
        
        if (boardArray.count == 1) {
            
            return NO;
        }
        else{
            _classTxtFld.text = @"";
            _sectionTxtFld.text = @"";
            selectedClassTransID = nil;
            selectedSectionTransID = nil;
        }
    }
    else if (textField == _classTxtFld){
        
        if (_boardTxtFld.text.length == 0) {
            [TSSingleton showAlertWithMessage:@"Please Choose Board"];
            return  NO;
        }
        else{
            _sectionTxtFld.text = @"";
            selectedSectionTransID = nil;
        }
    }
    else if (textField == _sectionTxtFld){
        
        if (_boardTxtFld.text.length == 0) {
            [TSSingleton showAlertWithMessage:@"Please Choose Board"];
            return  NO;
        }
        else if (_classTxtFld.text.length == 0){
            [TSSingleton showAlertWithMessage:@"Please Choose Class"];
            return  NO;
        }
    }
    
    [studentsListArray removeAllObjects];
    
    [self.studentTableView reloadData];
    
    [self showSelectionViewControllerWithSelectedTextField:textField withSelectedValue:textField.text];
    
    return NO;
}

#pragma mark - Service Call

-(void) fetchStudentListAPICall{
    
    NSString *urlString = @"MastersMServices/StudentsMService.svc/FetchStudentsByScectionList";
    
    NSDictionary *params = @{@"SectionList":@[selectedSectionTransID]};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchStudentsByScectionListResult"][@"ResponseCode"] integerValue] == 1) {
                
                [studentsListArray removeAllObjects];
                
                for (NSDictionary *dict in responseDictionary[@"FetchStudentsByScectionListResult"][@"StudentsMClass"]) {
                    
                    StudentModel *studentObj = [StudentModel new];
                    
                    studentObj.studentName = dict[@"StudentName"];
                    studentObj.studentTransID = dict[@"TransID"];
                    
                    [studentsListArray addObject:studentObj];
                    
                    studentObj = nil;
                }
                
                if (studentsListArray.count >0) {
                    
                    [self.studentTableView setHidden:NO];
                    [self.studentTableView reloadData];
                }
                else{
                    [self.studentTableView setHidden:YES];
                }
            }
            else{
                
                [self.studentTableView setHidden:YES];
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"FetchStudentsByScectionListResult"][@"ResponseMessage"]];
            }
        }
        else{
            [self.studentTableView setHidden:YES];
            
            [TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
        }
    }];
}

@end
