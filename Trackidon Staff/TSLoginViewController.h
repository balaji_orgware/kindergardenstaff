//
//  TSLoginViewController.h
//  Trackidon Staff
//
//  Created by ORGware on 16/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSHeaders.pch"

@interface TSLoginViewController : UIViewController <UITextFieldDelegate>

//Declarations

@property (weak, nonatomic) IBOutlet UIButton *goButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotPwdButton;

@property (weak, nonatomic) IBOutlet UITextField *usernameTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxtFld;
@property (weak, nonatomic) IBOutlet UIImageView *logoImgView;
@property (weak, nonatomic) IBOutlet UIImageView *redView;

//Methods Declaration

- (IBAction)goButtonAction:(id)sender;
- (IBAction)forgotPwdButtonAction:(id)sender;

@end
