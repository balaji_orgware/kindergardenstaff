//
//  ReceiverDetails+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 02/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ReceiverDetails.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReceiverDetails (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *boardTransId;
@property (nullable, nonatomic, retain) NSString *receiverDescription;
@property (nullable, nonatomic, retain) NSString *receiverTransId;
@property (nullable, nonatomic, retain) NSString *receiverTypeId;
@property (nullable, nonatomic, retain) NSString *boardName;
@property (nullable, nonatomic, retain) NSNumber *isSelected;
@property (nullable, nonatomic, retain) StaffDetails *staff;

@end

NS_ASSUME_NONNULL_END
