//
//  School+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 08/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "School+CoreDataProperties.h"

@implementation School (CoreDataProperties)

@dynamic latitude;
@dynamic longtitude;
@dynamic schoolAboutUs;
@dynamic schoolCode;
@dynamic schoolContactNo;
@dynamic schoolEmailID;
@dynamic schoolLogoData;
@dynamic schoolLogoUrl;
@dynamic schoolName;
@dynamic schoolTransId;
@dynamic staffTransId;
@dynamic staffUserMannualUrl;
@dynamic board;
@dynamic menu;
@dynamic staff;
@dynamic staffList;
@dynamic groupList;

@end
