//
//  Assignments.h
//  Trackidon Staff
//
//  Created by ORGware on 24/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AssignedDetails,StaffDetails;

NS_ASSUME_NONNULL_BEGIN

@interface Assignments : NSManagedObject

// Get Static Instance Of Assignments
+(instancetype) instance;

// Store Assignments details to Local db
-(void)storeAssignmentsDataForStaff:(StaffDetails *)staffObj withDict:(NSDictionary *)dictionary andIsForApprovals:(BOOL)isForApprovals;

// Method to check whether Holiday is already exists or not
-(Assignments *)isAssignmentAlreadyExistsWithTransID:(NSString *)transID forStaff:(StaffDetails *)staffObj context:(NSManagedObjectContext *)context withIsForApprovals:(BOOL)isForApprovals;

-(void) storePushNotificationAssignmentWithDict:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END

#import "Assignments+CoreDataProperties.h"
