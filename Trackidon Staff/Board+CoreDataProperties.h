//
//  Board+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 14/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Board.h"

NS_ASSUME_NONNULL_BEGIN

@interface Board (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *boardName;
@property (nullable, nonatomic, retain) NSString *boardTransId;
@property (nullable, nonatomic, retain) NSNumber *isViewableByStaff;
@property (nullable, nonatomic, retain) NSString *schoolName;
@property (nullable, nonatomic, retain) NSString *schoolTransId;
@property (nullable, nonatomic, retain) NSSet<Classes *> *classes;
@property (nullable, nonatomic, retain) School *school;

@end

@interface Board (CoreDataGeneratedAccessors)

- (void)addClassesObject:(Classes *)value;
- (void)removeClassesObject:(Classes *)value;
- (void)addClasses:(NSSet<Classes *> *)values;
- (void)removeClasses:(NSSet<Classes *> *)values;

@end

NS_ASSUME_NONNULL_END
