//
//  StaffList.h
//  Trackidon Staff
//
//  Created by Elango on 06/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class School;

NS_ASSUME_NONNULL_BEGIN

@interface StaffList : NSManagedObject

+ (instancetype)instance;

- (void)saveStaffListWithResponseDict:(NSDictionary *)dict schoolTransId:(NSString *)schoolTransId;

@end

NS_ASSUME_NONNULL_END

#import "StaffList+CoreDataProperties.h"
