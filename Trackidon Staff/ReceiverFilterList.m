//
//  ReceiverFilterList.m
//  Trackidon Staff
//
//  Created by Elango on 01/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "ReceiverFilterList.h"
#import "StaffDetails.h"

@implementation ReceiverFilterList

+ (instancetype)instance{
    
    static ReceiverFilterList *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (&onceToken, ^{
        
        kSharedInstance = [[ReceiverFilterList alloc]init];
        
    });
    
    return kSharedInstance;
}

- (void)saveReceiverFilterListWithResponseDict:(NSDictionary *)dict menuId:(NSString *)menuId
{
    //NSLog(@"%@",dict);
    
    @try {
        
        NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
        
        [SINGLETON_INSTANCE deleteAllObjectsInEntity:@"ReceiverFilterList"];
        
        for (NSDictionary *tmpFilterDict in dict[@"FetchMenuNameByIDResult"][@"FilterTypeDetails"]) {
            
            NSDictionary *filterDict = [tmpFilterDict dictionaryByReplacingNullsWithBlanks];
            
            ReceiverFilterList *receiverFilterList;
            
            receiverFilterList = [NSEntityDescription insertNewObjectForEntityForName:@"ReceiverFilterList" inManagedObjectContext:context];
            
            receiverFilterList.filterDescription = filterDict[@"value"];
            receiverFilterList.filterTypeId = [NSString stringWithFormat:@"%@",filterDict[@"key"]];
            receiverFilterList.menuId = [NSString stringWithFormat:@"%@",menuId];
            receiverFilterList.isSelected = [NSNumber numberWithBool:NO];
            receiverFilterList.staff = SINGLETON_INSTANCE.staff;

        }
        
        NSError *error;
        
        if (![context save:&error]) {
            
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            
        }
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    }
}

@end
