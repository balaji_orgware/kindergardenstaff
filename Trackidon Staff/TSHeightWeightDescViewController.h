//
//  TSHeightWeightDescViewController.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 21/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSHeightWeightDescViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *backView;

@property (weak, nonatomic) IBOutlet UIView *descView;

@property (weak, nonatomic) IBOutlet UIView *dateView;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIView *heightView;
@property (weak, nonatomic) IBOutlet UILabel *heightLbl;


@property (weak, nonatomic) IBOutlet UIView *weightView;
@property (weak, nonatomic) IBOutlet UILabel *weightLbl;

@property (weak, nonatomic) IBOutlet UITextView *remarksTxtVw;


-(void) showDescViewController:(UIViewController *)viewController withModelObj:(BMIInfoModel *)modelObj;

@end
