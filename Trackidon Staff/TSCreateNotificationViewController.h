//
//  TSCreateNotificationViewController.h
//  Trackidon Staff
//
//  Created by Elango on 29/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSCreateNotificationViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollVw;
@property (weak, nonatomic) IBOutlet UITextField *titleTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *attachementTxtFld;
@property (weak, nonatomic) IBOutlet UIImageView *attachmentPinImgVw;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTxtVw;
@property (weak, nonatomic) IBOutlet UIView *receiverSelectionVw;

@property (weak, nonatomic) IBOutlet UIView *priorityVw;
@property (weak, nonatomic) IBOutlet UIButton *highPriorityBtn;
@property (weak, nonatomic) IBOutlet UIButton *mediumPriorityBtn;
@property (weak, nonatomic) IBOutlet UIButton *lowPriorityBtn;

@end
