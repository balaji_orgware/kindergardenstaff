//
//  TSNotificationApprovalsViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 06/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSNotificationApprovalsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *notificationTableView;
@property (weak, nonatomic) IBOutlet UIView *selectionView;

// Button Actions
- (IBAction)approvedButtonTapped:(id)sender;
- (IBAction)declineButtonTapped:(id)sender;

@end
