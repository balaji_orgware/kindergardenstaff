//
//  GroupList.h
//  Trackidon Staff
//
//  Created by Elango on 08/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StaffDetails;

NS_ASSUME_NONNULL_BEGIN

@interface GroupList : NSManagedObject

+ (instancetype)instance;

- (void)saveGroupListWithResponseDict:(NSDictionary *)dict schoolTransId:(NSString *)schoolTransId;

@end

NS_ASSUME_NONNULL_END

#import "GroupList+CoreDataProperties.h"
