//
//  StaffDetails+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 19/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "StaffDetails.h"

@class PushCount;

NS_ASSUME_NONNULL_BEGIN

@interface StaffDetails (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *acadamicTransId;
@property (nullable, nonatomic, retain) NSString *acadamicYearTransId;
@property (nullable, nonatomic, retain) NSString *addressOne;
@property (nullable, nonatomic, retain) NSString *addressThree;
@property (nullable, nonatomic, retain) NSString *addressTwo;
@property (nullable, nonatomic, retain) NSString *emailId;
@property (nullable, nonatomic, retain) NSString *inChargeClassName;
@property (nullable, nonatomic, retain) NSString *inChargeClassTransId;
@property (nullable, nonatomic, retain) NSString *inChargeSectionName;
@property (nullable, nonatomic, retain) NSString *inChargeSectionTransId;
@property (nullable, nonatomic, retain) NSString *mobileNo;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *pincode;
@property (nullable, nonatomic, retain) NSData *profileImageData;
@property (nullable, nonatomic, retain) NSString *profileImageUrl;
@property (nullable, nonatomic, retain) NSString *schoolTransId;
@property (nullable, nonatomic, retain) NSString *staffClassName;
@property (nullable, nonatomic, retain) NSString *staffRole;
@property (nullable, nonatomic, retain) NSString *staffTransId;
@property (nullable, nonatomic, retain) NSSet<Assignments *> *assignments;
@property (nullable, nonatomic, retain) NSSet<BoardList *> *boardList;
@property (nullable, nonatomic, retain) NSSet<EventInbox *> *eventInbox;
@property (nullable, nonatomic, retain) NSSet<Events *> *events;
@property (nullable, nonatomic, retain) NSSet<Feedback *> *feedback;
@property (nullable, nonatomic, retain) NSSet<Holidays *> *holidays;
@property (nullable, nonatomic, retain) NSSet<NotificationInbox *> *notificationInbox;
@property (nullable, nonatomic, retain) NSSet<Notifications *> *notifications;
@property (nullable, nonatomic, retain) NSSet<Portions *> *portions;
@property (nullable, nonatomic, retain) NSSet<ReceiverDetails *> *receiverDetails;
@property (nullable, nonatomic, retain) NSSet<ReceiverFilterList *> *receiverFilterList;
@property (nullable, nonatomic, retain) NSSet<ReceiverReceipientList *> *receiverReceipientList;
@property (nullable, nonatomic, retain) NSSet<School *> *school;
@property (nullable, nonatomic, retain) Timetable *timetable;

@property (nullable, nonatomic, retain) NSSet<PushCount *> *pushCount;
@property (nullable, nonatomic, retain) NSSet<PortionInbox *> *portionInbox;

@end

@interface StaffDetails (CoreDataGeneratedAccessors)

- (void)addAssignmentsObject:(Assignments *)value;
- (void)removeAssignmentsObject:(Assignments *)value;
- (void)addAssignments:(NSSet<Assignments *> *)values;
- (void)removeAssignments:(NSSet<Assignments *> *)values;

- (void)addBoardListObject:(BoardList *)value;
- (void)removeBoardListObject:(BoardList *)value;
- (void)addBoardList:(NSSet<BoardList *> *)values;
- (void)removeBoardList:(NSSet<BoardList *> *)values;

- (void)addEventInboxObject:(EventInbox *)value;
- (void)removeEventInboxObject:(EventInbox *)value;
- (void)addEventInbox:(NSSet<EventInbox *> *)values;
- (void)removeEventInbox:(NSSet<EventInbox *> *)values;

- (void)addEventsObject:(Events *)value;
- (void)removeEventsObject:(Events *)value;
- (void)addEvents:(NSSet<Events *> *)values;
- (void)removeEvents:(NSSet<Events *> *)values;

- (void)addFeedbackObject:(Feedback *)value;
- (void)removeFeedbackObject:(Feedback *)value;
- (void)addFeedback:(NSSet<Feedback *> *)values;
- (void)removeFeedback:(NSSet<Feedback *> *)values;

- (void)addHolidaysObject:(Holidays *)value;
- (void)removeHolidaysObject:(Holidays *)value;
- (void)addHolidays:(NSSet<Holidays *> *)values;
- (void)removeHolidays:(NSSet<Holidays *> *)values;

- (void)addNotificationInboxObject:(NotificationInbox *)value;
- (void)removeNotificationInboxObject:(NotificationInbox *)value;
- (void)addNotificationInbox:(NSSet<NotificationInbox *> *)values;
- (void)removeNotificationInbox:(NSSet<NotificationInbox *> *)values;

- (void)addNotificationsObject:(Notifications *)value;
- (void)removeNotificationsObject:(Notifications *)value;
- (void)addNotifications:(NSSet<Notifications *> *)values;
- (void)removeNotifications:(NSSet<Notifications *> *)values;

- (void)addPortionsObject:(Portions *)value;
- (void)removePortionsObject:(Portions *)value;
- (void)addPortions:(NSSet<Portions *> *)values;
- (void)removePortions:(NSSet<Portions *> *)values;

- (void)addReceiverDetailsObject:(ReceiverDetails *)value;
- (void)removeReceiverDetailsObject:(ReceiverDetails *)value;
- (void)addReceiverDetails:(NSSet<ReceiverDetails *> *)values;
- (void)removeReceiverDetails:(NSSet<ReceiverDetails *> *)values;

- (void)addReceiverFilterListObject:(ReceiverFilterList *)value;
- (void)removeReceiverFilterListObject:(ReceiverFilterList *)value;
- (void)addReceiverFilterList:(NSSet<ReceiverFilterList *> *)values;
- (void)removeReceiverFilterList:(NSSet<ReceiverFilterList *> *)values;

- (void)addReceiverReceipientListObject:(ReceiverReceipientList *)value;
- (void)removeReceiverReceipientListObject:(ReceiverReceipientList *)value;
- (void)addReceiverReceipientList:(NSSet<ReceiverReceipientList *> *)values;
- (void)removeReceiverReceipientList:(NSSet<ReceiverReceipientList *> *)values;

- (void)addSchoolObject:(School *)value;
- (void)removeSchoolObject:(School *)value;
- (void)addSchool:(NSSet<School *> *)values;
- (void)removeSchool:(NSSet<School *> *)values;

- (void)addPushCountObject:(PushCount *)value;
- (void)removePushCountObject:(PushCount *)value;
- (void)addPushCount:(NSSet<PushCount *> *)values;
- (void)removePushCount:(NSSet<PushCount *> *)values;

- (void)addPortionInboxObject:(PortionInbox *)value;
- (void)removePortionInboxObject:(PortionInbox *)value;
- (void)addPortionInbox:(NSSet<PortionInbox *> *)values;
- (void)removePortionInbox:(NSSet<PortionInbox *> *)values;

@end

NS_ASSUME_NONNULL_END
