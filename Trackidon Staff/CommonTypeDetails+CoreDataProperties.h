//
//  CommonTypeDetails+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Balaji on 07/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CommonTypeDetails.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommonTypeDetails (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *typeID;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *transID;

@end

NS_ASSUME_NONNULL_END
