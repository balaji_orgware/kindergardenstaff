//
//  TSEmergencyDescViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 21/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSEmergencyDescViewController.h"

@interface TSEmergencyDescViewController ()

@end

@implementation TSEmergencyDescViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);
    
    _backView.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);
    _descView.frame = CGRectMake(_descView.frame.origin.x, _descView.frame.origin.y,DeviceWidth, _descView.frame.size.height);
    
    UITapGestureRecognizer *tapOnBackView = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOnBackView)];
    [_backView addGestureRecognizer:tapOnBackView];
    
    [TSSingleton layerDrawForView:_dateView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Helper methods

-(void) tapOnBackView{
    [self hideAnimationView];
}

-(void) loadViewWithContent:(EmergencyInfoModel *)modelObj{
    
    _dateLabel.text = [SINGLETON_INSTANCE stringFromDate:modelObj.examinedDate withFormate:DATE_FORMATE_MNTH_DAY_YR];
    
    _titleLabel.text = modelObj.emergencyTitle;
    
    _descTxtVw.text = [modelObj.emergencyRemarks isEqualToString:@""] ? @"---" : modelObj.emergencyRemarks;
    
    _descTxtVw.font = [UIFont fontWithName:APP_FONT size:15];
}

-(void) showDescViewController:(UIViewController *)viewController withModelObj:(EmergencyInfoModel *)modelObj{
    
    [viewController.view addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    [viewController.view endEditing:YES];
    
    _descView.frame = CGRectMake(_descView.frame.origin.x,DeviceHeight, viewController.view.frame.size.width,viewController.view.frame.size.height*3/4);
    
    [self loadViewWithContent:modelObj];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [_backView setAlpha:0.5];
        
        _descView.frame = CGRectMake(_descView.frame.origin.x,viewController.view.frame.size.height - _descView.frame.size.height, viewController.view.frame.size.width, _descView.frame.size.height);
    }];
}

-(void) hideAnimationView{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [_backView setAlpha:0.0];
        _descView.frame = CGRectMake(_descView.frame.origin.x, DeviceHeight, _descView.frame.size.width, _descView.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}
@end
