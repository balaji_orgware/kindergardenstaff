//
//  Notifications.h
//  Trackidon Staff
//
//  Created by Elango on 22/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AssignedDetails, StaffDetails;

NS_ASSUME_NONNULL_BEGIN

@interface Notifications : NSManagedObject

+ (instancetype)instance;

- (void)saveNotificationsWithResponseDict:(NSDictionary *)dict  andIsForApprovals:(BOOL)isForApprovals;

- (void)savePushNotificationWithDict:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END

#import "Notifications+CoreDataProperties.h"
