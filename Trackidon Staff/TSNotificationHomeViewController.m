//
//  TSNotificationHomeViewController.m
//  Trackidon Staff
//
//  Created by Elango on 22/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSNotificationHomeViewController.h"

@interface TSNotificationHomeViewController ()<UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate>

@end

@implementation TSNotificationHomeViewController
{
    NSArray *notificationArr;
    
    int createdTodayCount;
    
    int approvedCount;
    
    int declinedCount;
    
    int pendingCount;
    
    NSString *title;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self fetchNotifications];
    
    [self fetchNotificationServiceCall];
    
    title = SINGLETON_INSTANCE.barTitle;
    
    [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_NOTIFICATIONOPENED parameter:[NSMutableDictionary new]];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_NOTIFICATIONHOME parameter:[NSMutableDictionary new]];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = title;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Tableview Datasource & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    UIImageView *placeHolderImgVw = (UIImageView *)[cell viewWithTag:100];
    
    UILabel *subMenuLabel = (UILabel *)[cell viewWithTag:101];
    
    UILabel *countLabel = (UILabel *)[cell viewWithTag:102];
    
    NSInteger row = [[NSString stringWithFormat:@"%ld",(long)indexPath.row] integerValue];
    
    switch (row) {
            
        case 0:
            
            placeHolderImgVw.image = [UIImage imageNamed:@"notification_overall"];
            subMenuLabel.text = @"View All";
            subMenuLabel.font = [UIFont fontWithName:APP_FONT size:18];
            countLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)notificationArr.count];
            countLabel.textColor = COLOR_PENDING_VIEWALL;
            break;
            
        case 1:
            
            placeHolderImgVw.image = [UIImage imageNamed:@"created_today"];
            subMenuLabel.text = @"Created Today";
            subMenuLabel.font = [UIFont fontWithName:APP_FONT size:16];
            countLabel.text = [NSString stringWithFormat:@"%d",createdTodayCount];
            countLabel.textColor = COLOR_ACCEPT_CREATEDTODAY;
            break;
            
        default:
            break;
    }
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SINGLETON_INSTANCE.barTitle = title;
    
    if (indexPath.row == 0) {
        
        TSNotificationListViewController *notificationListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBNotificationList"];
        
        notificationListVC.listType = LIST_TYPE_OVERALL;
        
        [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_VIEW_NOTIFICATION parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_VIEWALL,ANALYTICS_PARAM_VIEWTYPE, nil]];
        
        if(notificationArr.count >0)
            [self.navigationController pushViewController:notificationListVC animated:YES];
        
    }else {
        
        TSNotificationListViewController *notificationListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBNotificationList"];
        
        notificationListVC.listType = LIST_TYPE_CREATED_TODAY;
        
        [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_VIEW_NOTIFICATION parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_CREATEDTODAY,ANALYTICS_PARAM_VIEWTYPE, nil]];
        
        if(createdTodayCount > 0)
            [self.navigationController pushViewController:notificationListVC animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 75;
}

#pragma mark - Collection View Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UIImageView *btnImg = (UIImageView *)[cell viewWithTag:101];
    
    UILabel *btnLbl = (UILabel *)[cell viewWithTag:102];
    
    UILabel *countLbl = (UILabel *)[cell viewWithTag:103];
    
    //cell.contentView.layer.borderWidth = 0.5;
    //cell.contentView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    [TSSingleton layerDrawForView:cell.contentView position:LAYER_LEFT color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:cell.contentView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    switch (indexPath.row) {
            
        case LIST_TYPE_ACCEPTED:
            
            btnImg.image = [UIImage imageNamed:@"approved"];
            btnLbl.text = @"Approved";
            countLbl.text= [NSString stringWithFormat:@"%lu",(unsigned long)approvedCount];
            [countLbl setTextColor:COLOR_ACCEPT_CREATEDTODAY];
            
            break;
            
        case LIST_TYPE_PENDING:
            
            btnImg.image = [UIImage imageNamed:@"pending"];
            btnLbl.text = @"Pending";
            countLbl.text= [NSString stringWithFormat:@"%lu",(unsigned long)pendingCount];
            [countLbl setTextColor:COLOR_PENDING_VIEWALL];
            
            break;
            
        case LIST_TYPE_DECLINED:
            
            btnImg.image = [UIImage imageNamed:@"declined"];
            btnLbl.text = @"Declined";
            countLbl.text= [NSString stringWithFormat:@"%lu",(unsigned long)declinedCount];
            [countLbl setTextColor:COLOR_DECLINED_DUESTODAY];
            
            break;
            
            
        default:
            break;
    }
    
    
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
            
        case LIST_TYPE_ACCEPTED:
        {
            
            TSNotificationListViewController *notificationListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBNotificationList"];
            
            notificationListVC.listType = LIST_TYPE_ACCEPTED;
            
            [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_VIEW_NOTIFICATION parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_APPROVED,ANALYTICS_PARAM_VIEWTYPE, nil]];
            
            if(approvedCount >0)
                [self.navigationController pushViewController:notificationListVC animated:YES];
            
            SINGLETON_INSTANCE.barTitle = TEXT_APPROVED;
            break;
        }
        case LIST_TYPE_PENDING:
        {
            TSNotificationListViewController *notificationListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBNotificationList"];
            
            notificationListVC.listType = LIST_TYPE_PENDING;
            
            [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_VIEW_NOTIFICATION parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_PENDING,ANALYTICS_PARAM_VIEWTYPE, nil]];
            
            if(pendingCount >0)
                [self.navigationController pushViewController:notificationListVC animated:YES];
            
            SINGLETON_INSTANCE.barTitle = TEXT_PENDING;
            break;
        }
        case LIST_TYPE_DECLINED:
        {
            TSNotificationListViewController *notificationListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBNotificationList"];
            
            notificationListVC.listType = LIST_TYPE_DECLINED;
            
            [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_VIEW_NOTIFICATION parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_DECLINED,ANALYTICS_PARAM_VIEWTYPE, nil]];
            
            if(declinedCount >0)
                [self.navigationController pushViewController:notificationListVC animated:YES];
            
            SINGLETON_INSTANCE.barTitle = TEXT_DECLINED;
            break;
        }
            
        default:
            break;
    }
    
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float width = DeviceWidth/3;
    float height = 130;//width*1.214;
    
    return CGSizeMake(width, height);
    
}

#pragma mark - Methods

- (void)fetchNotifications{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND isForApprovals == 0",SINGLETON_INSTANCE.staffTransId];
    
    notificationArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Notifications" withPredicate:predicate];
    
    dispatch_async(dispatch_get_main_queue(), ^{

        [self setNotificationsCounts];
        
    });
    
}

- (void)setNotificationsCounts{
    
    NSPredicate *pendingPredicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND approvedStatus == 0 AND isForApprovals == 0",SINGLETON_INSTANCE.staffTransId];
    
    NSPredicate *approvedPredicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND approvedStatus == 1 AND isForApprovals == 0",SINGLETON_INSTANCE.staffTransId];
    
    NSPredicate *declinedPredicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND approvedStatus == 2 AND isForApprovals == 0",SINGLETON_INSTANCE.staffTransId];
    
    NSPredicate *createdTdyPredicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND notificationDate == %@ AND isForApprovals == 0",SINGLETON_INSTANCE.staffTransId,[SINGLETON_INSTANCE dateFromDate:[NSDate date] withFormate:DATE_FORMATE_SERVICE_RESPONSE]];
    
    NSLog(@"%@",[SINGLETON_INSTANCE dateFromDate:[NSDate date] withFormate:DATE_FORMATE_SERVICE_RESPONSE]);
    
    pendingCount = (int)[notificationArr filteredArrayUsingPredicate:pendingPredicate].count;
    
    approvedCount = (int)[notificationArr filteredArrayUsingPredicate:approvedPredicate].count;
    
    declinedCount = (int)[notificationArr filteredArrayUsingPredicate:declinedPredicate].count;
    
    createdTodayCount = (int)[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Notifications" withPredicate:createdTdyPredicate].count;
    
    [_tblVw reloadData];
    
    [_collectionVw reloadData];
    
}

#pragma mark - Button Action

- (IBAction)onCreateNotificationClk:(id)sender {
    
    [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_CREATE_INHOME parameter:[NSMutableDictionary new]];
    
    if ([SINGLETON_INSTANCE checkForActiveInternet]) {
        
        [SINGLETON_INSTANCE fetchFilterAndReceipientDetailsWithMenuId:SINGLETON_INSTANCE.currentMenuId showIndicator:YES onCompletion:nil];
        
        TSCreateNotificationViewController *createNotificationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBCreateNotification"];
        
        [self.navigationController pushViewController:createNotificationVC animated:YES];
        
    }else{
        [TSSingleton showAlertWithMessage:TEXT_NOINTERNET];
    }
    
    
}

#pragma mark - ServiceCall

- (void)fetchNotificationServiceCall{
    
    NSString *url = @"MastersMServices/SchoolNotificationsService.svc/FetchSchoolNotificationsByStaff";
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    [param setObject:SINGLETON_INSTANCE.staffTransId forKey:@"guidStaffTransID"];
    
    BOOL showIndicator = NO;
    
    if (notificationArr.count  == 0) {
        showIndicator = YES;
    }
    
    [[TSHandlerClass sharedHandler] postRequestWithURL:url baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:param currentView:self.view showIndicator:showIndicator onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            if([responseDictionary[@"FetchSchoolNotificationsByStaffResult"][@"ResponseCode"] integerValue] == 1) {
                
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                    [[Notifications instance]saveNotificationsWithResponseDict:responseDictionary andIsForApprovals:NO];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self fetchNotifications];
                        
                    });
                    
                });
            }
            
        }else {
            
            //[TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
        }
        
    }];
}



@end
