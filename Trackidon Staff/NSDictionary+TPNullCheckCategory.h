//
//  NSDictionary+TPNullCheckCategory.h
//  Trackidon
//
//  Created by Balaji on 02/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (TPNullCheckCategory)

- (NSDictionary *)dictionaryByReplacingNullsWithBlanks;
@end
