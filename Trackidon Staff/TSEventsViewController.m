//
//  TSEventsViewController.m
//  Trackidon Staff
//
//  Created by Elango on 19/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSEventsViewController.h"

@interface TSEventsViewController ()<NSFetchedResultsControllerDelegate,UITableViewDelegate,UITableViewDataSource>

@end

@implementation TSEventsViewController
{
    NSArray *attachmentImgArray;
    
    NSManagedObjectContext *managedObjectContext;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    managedObjectContext = [TSAppDelegate instance].managedObjectContext;
    
    attachmentImgArray = @[@"one",@"two",@"three",@"four",@"five"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_EVENTLIST parameter:[NSMutableDictionary new]];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@ Log",SINGLETON_INSTANCE.barTitle];
    
    [self fetchEvents];
    
}

#pragma mark - Table view data sources & delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    id  sectionInfo =
    [[_fetchedResultsController sections] objectAtIndex:section];
    
    if ([_fetchedResultsController fetchedObjects].count == 0)
        _eventsTblVw.hidden = YES;
    else
        _eventsTblVw.hidden = NO;
    
    return [sectionInfo numberOfObjects];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Events *event = [_fetchedResultsController objectAtIndexPath:indexPath];
    
    UILabel *eventTitleLbl = (UILabel *)[cell viewWithTag:101];
    
    UILabel *eventStatusLbl = (UILabel *)[cell viewWithTag:102];
    
    UILabel *eventDateLbl = (UILabel *)[cell viewWithTag:103];
    
    UIImageView *attachmentImgVw = (UIImageView *)[cell viewWithTag:104];
    
    eventTitleLbl.text = event.eventTitle;
    
    eventDateLbl.text = [SINGLETON_INSTANCE dateLblTextWithFromDate:event.eventFromDate toDate:event.eventToDate];
    
    switch ([event.approvedStatus integerValue]) {
            
        case StatusTypeApproved:
            
            eventStatusLbl.text = [NSString stringWithFormat:@"Approved by : %@",event.approvedByName];
            
            [eventStatusLbl setTextColor:COLOR_ACCEPT_CREATEDTODAY];
            
            break;
            
        case StatusTypePending:
            
            eventStatusLbl.text = [NSString stringWithFormat:@"Pending"];
            
            [eventStatusLbl setTextColor:COLOR_PENDING_VIEWALL];
            
            break;
            
        case StatusTypeDeclined:
            
            eventStatusLbl.text = [NSString stringWithFormat:@"Declined by : %@",event.approvedByName];
            
            [eventStatusLbl setTextColor:COLOR_DECLINED_DUESTODAY];
            
            break;
        
            
        default:
            break;
    }
    
    if ([event.eventIsHasAttachment intValue] == 1) {
        
        NSString *attachmentImgName;
        
        switch (indexPath.row%5) {
                
            case 0:
                attachmentImgName = @"one";
                break;
                
            case 1:
                attachmentImgName = @"two";
                break;
                
            case 2:
                attachmentImgName = @"three";
                break;
                
            case 3:
                attachmentImgName = @"four";
                break;
                
            case 4:
                attachmentImgName = @"five";
                break;
                
            default:
                attachmentImgName = @"one";
                break;
        }
        attachmentImgVw.image = [UIImage imageNamed:attachmentImgName];
        
        [attachmentImgVw setHidden:NO];
    }
    else{
        
        [attachmentImgVw setHidden:YES];
    }
    
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TSEventsDescriptionViewController *eventDesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBEventsDescription"];
    
    Events *event = (Events *)[_fetchedResultsController objectAtIndexPath:indexPath];
    
    eventDesVC.descriptionType = EventsString;
    
    eventDesVC.commonObj = event;
    
    [self.navigationController pushViewController:eventDesVC animated:YES];
    
    switch (_listType) {
            
        case LIST_TYPE_OVERALL:
            
            [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_EVENT_IN_LIST parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_VIEWALL,ANALYTICS_PARAM_LISTTYPE, nil]];
            
            break;
            
        case LIST_TYPE_CREATED_TODAY:
        {
            [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_EVENT_IN_LIST parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_CREATEDTODAY,ANALYTICS_PARAM_LISTTYPE, nil]];
        }
            break;
            
        case LIST_TYPE_ACCEPTED:
            
            [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_EVENT_IN_LIST parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_APPROVED,ANALYTICS_PARAM_LISTTYPE, nil]];
            
            break;
            
        case LIST_TYPE_PENDING:
            
            [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_EVENT_IN_LIST parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_PENDING,ANALYTICS_PARAM_LISTTYPE, nil]];
            
            break;
            
        case LIST_TYPE_DECLINED:
            
            [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_EVENT_IN_LIST parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_DECLINED,ANALYTICS_PARAM_LISTTYPE, nil]];
            
            break;
            
        default:
            break;
    }

    
}


#pragma mark - Methods

- (void)fetchEvents{
    
    NSError *error;
    
    if (![[self fetchedResultsController] performFetch:&error]) {
        
        NSLog(@"Error in fetch %@, %@", error, [error userInfo]);
        
    } else {
        
        [_eventsTblVw reloadData];
    }
}

#pragma mark - fetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Events" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                              initWithKey:@"modifiedDate" ascending:NO];
    
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    [fetchRequest setFetchBatchSize:20];
    
    NSPredicate *predicate;
    
    switch (_listType) {
            
        case LIST_TYPE_OVERALL:
            
            predicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@",SINGLETON_INSTANCE.staffTransId];
            
            break;
            
        case LIST_TYPE_CREATED_TODAY:
        {
            
            NSString *startDateStr = [[SINGLETON_INSTANCE stringFromDate:[NSDate date] withFormate:@"dd-MMM-yyyy"] stringByAppendingString:@" 00:01"];
            
            NSString *endDateStr = [[SINGLETON_INSTANCE stringFromDate:[NSDate date] withFormate:@"dd-MMM-yyyy"] stringByAppendingString:@" 23:59"];
            
            predicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND modifiedDate >= %@ AND modifiedDate <= %@ AND isForApprovals == 0",SINGLETON_INSTANCE.staffTransId,[SINGLETON_INSTANCE dateFromString:startDateStr withFormate:DATE_FORMATE_MODIFIED_DATE], [SINGLETON_INSTANCE dateFromString:endDateStr withFormate:DATE_FORMATE_MODIFIED_DATE]];
            
        }
            break;
            
        case LIST_TYPE_ACCEPTED:
            
            predicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND approvedStatus == 1",SINGLETON_INSTANCE.staffTransId];
            
            break;
            
        case LIST_TYPE_PENDING:
            
            predicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND approvedStatus == 0",SINGLETON_INSTANCE.staffTransId];
            
            break;
            
        case LIST_TYPE_DECLINED:
            
            predicate = [NSPredicate predicateWithFormat:@"staffTransId CONTAINS[cd] %@ AND approvedStatus == 2",SINGLETON_INSTANCE.staffTransId];
            
            break;
            
        default:
            break;
    }
    
    [fetchRequest setPredicate:predicate];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:managedObjectContext sectionNameKeyPath:nil
                                                   cacheName:nil]; // better to not use cache
    self.fetchedResultsController = theFetchedResultsController;
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
    
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    
    [self.eventsTblVw reloadData];
}


@end
