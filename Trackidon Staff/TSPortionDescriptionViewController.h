//
//  TSPortionDescriptionViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/29/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Portions,AssignedDetails;

@interface TSPortionDescriptionViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *subjectHeaderView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *subjectTitleLabel;

@property (weak, nonatomic) IBOutlet UIButton *attachmentButton;

@property (weak, nonatomic) IBOutlet UILabel *assignedTeacherLabel;

@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (weak, nonatomic) IBOutlet UILabel *portionDateLabel;

@property (weak, nonatomic) IBOutlet UILabel *createdOnLbl;

@property (weak, nonatomic) Portions *portionObj;

@property (weak, nonatomic)NSMutableArray *portionDetailsArray;

@property (weak, nonatomic) IBOutlet UIButton *viewReceiverBtn;

@property (weak, nonatomic) IBOutlet UIView *assignedView;

@property (weak, nonatomic) IBOutlet UITextField *assignedTxtFld;
@property (weak, nonatomic) IBOutlet UIImageView *assignedImgView;

@property (weak, nonatomic) IBOutlet UIView *dateView;

//@property (weak, nonatomic) AssignedDetails *assignedObj;

@property (nonatomic,strong) NSString *descriptionType;

@property (nonatomic,strong) id commonObj;

@end
