//
//  TSNotificationInboxViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/2/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSNotificationInboxViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *notificationTableView;

@property (nonatomic, assign) int listType;
@property (nonatomic,retain) NSFetchedResultsController *fetchedResultsController;
@end
