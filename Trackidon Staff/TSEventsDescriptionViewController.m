//
//  TSEventsDescriptionViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/29/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSEventsDescriptionViewController.h"

@interface TSEventsDescriptionViewController ()
{
    NSURL *attachmentURL;
    
    NSArray *assigneeArr;
    
    UITapGestureRecognizer *assignedViewTap;
    
    EventInbox *eventInboxObj;
    
    Events *eventObj;
}
@end

@implementation TSEventsDescriptionViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    assignedViewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(assignedViewTapped)];
    
    [TSSingleton layerDrawForView:_topView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    _attachmentButton.frame = CGRectMake(DEVICE_SIZE.width - _attachmentButton.frame.size.width - 10, _topView.frame.origin.y + _topView.frame.size.height - _attachmentButton.frame.size.height/2 + 18, _attachmentButton.frame.size.height, _attachmentButton.frame.size.height);
    
    [TSSingleton layerDrawForView:_dateView position:LAYER_TOP color:[UIColor lightGrayColor]];
    
    _progressView.frame = CGRectMake(_fromDateLabel.center.x +_fromDateLabel.frame.size.width/2, _fromDateLabel.center.y, _progressView.frame.size.width, _progressView.frame.size.height);
    
    _toDateLabel.frame = CGRectMake(_progressView.center.x +_progressView.frame.size.width/2, _fromDateLabel.frame.origin.y, _toDateLabel.frame.size.width, _toDateLabel.frame.size.height);
    
    [TSSingleton layerDrawForView:_assignedView position:LAYER_TOP color:[UIColor lightGrayColor]];
    
//    _assignedView.center = CGPointMake(_dateView.center.x, _dateView.frame.origin.y-_assignedView.frame.size.height);
    
    _fromLbl.center = CGPointMake(_fromDateLabel.center.x, _fromDateLabel.frame.origin.y-10);
    
    _toLbl.center = CGPointMake(_toDateLabel.center.x, _toDateLabel.frame.origin.y-10);
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(descViewTapped)];
    
    [_descriptionTextView addGestureRecognizer:tapGesture];
   
    if (SINGLETON_INSTANCE.isPushNotification) {
        
        // When Push notification arrives..
        
        SINGLETON_INSTANCE.isPushNotification = NO;
        
        [self fetchEventByEventTransIDAPICall]; // Same API for both event & eventInbox
    }
    else{
        
        if ([_descriptionType isEqualToString:EventsString]) {
            
            eventObj = (Events *)self.commonObj;
            
            [self loadViewWithEventObj:eventObj];
            
        }
        else if ([_descriptionType isEqualToString:EventsInboxString]){
            
            eventInboxObj = (EventInbox *)self.commonObj;
            
            [self loadViewWithEventInboxObj:eventInboxObj];
        }
    }
    
    if (IS_IPHONE_4) {
        
        _dateView.frame = CGRectMake(0, DeviceHeight -75, DeviceWidth, 75);
        
        _assignedView.frame = CGRectMake(0, DeviceHeight-_dateView.frame.size.height-40, DeviceWidth, 40);
        
        if (assigneeArr.count == 0) {
            
            _descriptionTextView.frame = CGRectMake(self.descriptionTextView.frame.origin.x, self.descriptionTextView.frame.origin.y, self.descriptionTextView.frame.size.width, 120+_assignedView.frame.size.height);
        }
        else{
            
            _descriptionTextView.frame = CGRectMake(self.descriptionTextView.frame.origin.x, self.descriptionTextView.frame.origin.y, self.descriptionTextView.frame.size.width, 120);
        }
        
        _fromDateLabel.frame = CGRectMake(_fromDateLabel.frame.origin.x, 25, 45, 45);
        
        _toDateLabel.frame = CGRectMake(_toDateLabel.frame.origin.x, _fromDateLabel.frame.origin.y, 45, 45);
        
        _fromLbl.center = CGPointMake(_fromDateLabel.center.x, 15);
        
        _toLbl.center = CGPointMake(_toDateLabel.center.x, 15);
        
        _progressView.frame = CGRectMake(_fromDateLabel.frame.origin.x+_fromDateLabel.frame.size.width, _fromDateLabel.frame.origin.y+_fromDateLabel.frame.size.height/2, _toDateLabel.frame.origin.x -(_fromDateLabel.frame.origin.x+_fromDateLabel.frame.size.width), 2);
        
        _daysLeftLbl.frame = CGRectMake(_daysLeftLbl.frame.origin.x, _remainingCountLbl.frame.origin.y+_remainingCountLbl.frame.size.height, _daysLeftLbl.frame.size.width, _daysLeftLbl.frame.size.height);
    }
    
    _fromDateLabel.layer.cornerRadius = _fromDateLabel.frame.size.width/2;
    
    _fromDateLabel.layer.masksToBounds = YES;
    
    _toDateLabel.layer.cornerRadius = _toDateLabel.frame.size.width/2;
    
    _toDateLabel.layer.masksToBounds = YES;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    if ([_descriptionType isEqualToString:EventsString]){
        
        [TSAnalytics logScreen:ANALYTICS_SCREEN_EVENTDESCRIPTION parameter:[NSMutableDictionary new]];
        
        self.navigationItem.title = [NSString stringWithFormat:@"%@ Detail",SINGLETON_INSTANCE.barTitle];
        
    }else if ([_descriptionType isEqualToString:EventsInboxString]){
        
        [TSAnalytics logScreen:ANALYTICS_SCREEN_INBOXEVENT_DESCRIPTION parameter:[NSMutableDictionary new]];
        
        self.navigationItem.title = @"Event Inbox Detail";
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Methods

-(void)assignedViewTapped{
    
    if ([_descriptionType isEqualToString:EventsString]) {
        
        [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_VIEW_RECEIVER parameter:[NSMutableDictionary new]];
        
    }else if ([_descriptionType isEqualToString:EventsInboxString]){
        
        [TSAnalytics logEvent:ANALYTICS_INBOXEVENT_CLICKEDVIEWRECEIVER parameter:[NSMutableDictionary new]];
        
    }
    
    AssignedListViewController *listVC = [[AssignedListViewController alloc]initWithNibName:@"AssignedListViewController" bundle:nil];
    
    [listVC setAssignedListArray:assigneeArr];
    
    [listVC setAssignedText:EventInvited];

    [listVC showInViewController:self];
    
}


#pragma mark - Helper methods

-(void) loadViewWithEventObj:(Events *)event{
    
    _fromDateLabel.attributedText = [SINGLETON_INSTANCE getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:event.eventFromDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_fromDateLabel];
    
    _toDateLabel.attributedText = [SINGLETON_INSTANCE getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:event.eventToDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_toDateLabel];
    
    _assignedTeacherLabel.text = [NSString stringWithFormat:@"- by %@",event.userCreatedName];
    
    _venueLbl.text = [NSString stringWithFormat:@"Venue : %@",event.eventVenue];
    
    _eventTitleLbl.text = [NSString stringWithFormat:@"Title : %@",event.eventTitle];
    
    _timeLbl.text = [NSString stringWithFormat:@"Time : %@",[SINGLETON_INSTANCE convertTimeToTweleveHousrFomateFromString:event.eventTime]];
    
    _descriptionTextView.text = [NSString stringWithFormat:@"\t%@",event.eventDescription];
    
    if ([event.eventIsHasAttachment intValue] == 1) {
        
        [_attachmentButton setHidden:NO];
    }
    else{
        
        [_attachmentButton setHidden:YES];
    }
    
    NSLog(@"FromTo::%@--%@",event.eventFromDate,event.eventToDate);
    
    if ([eventObj.eventFromDate compare:eventObj.eventToDate]==NSOrderedSame) {
        
        _remainingCountLbl.text = [NSString stringWithFormat:@"%@",[TSSingleton daysBetweenFromDate:event.eventFromDate toDate:event.eventToDate]];
        
        _daysLeftLbl.text = @"Day Event";
        
    }else{
        
        _remainingCountLbl.text = [NSString stringWithFormat:@"%@",[TSSingleton daysBetweenFromDate:event.eventFromDate toDate:event.eventToDate]];
        
        _daysLeftLbl.text = @"Days Event";
    }
    
    /*NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transId MATCHES %@",eventObj.eventTransId];
     
     assigneeArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"AssignedDetails" withPredicate:predicate];
     
     NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"assigneeType" ascending:YES];
     
     NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
     
     assigneeArr = [assigneeArr sortedArrayUsingDescriptors:descriptors];*/
    
    assigneeArr = [event.assignedList componentsSeparatedByString:ASSIGNEE_SAPRATOR];
    
    _assignedView.hidden = NO;
    
    if (assigneeArr.count ==1) {
        
        //_assignedObj = [assigneeArr objectAtIndex:0];
        
        _assignedTxtFld.text = [NSString stringWithFormat:@"Invited To : %@",[assigneeArr objectAtIndex:0]];
        
    }else if (assigneeArr.count >1){
        
        [self.assignedView addGestureRecognizer:assignedViewTap];
        
        [SINGLETON_INSTANCE addRightViewToTextField:_assignedTxtFld withImageName:@"expand_arrow"];
    }
    else if (assigneeArr.count ==0){
        
        _assignedView.hidden = YES;
    }

}

-(void) loadViewWithEventInboxObj:(EventInbox *)eventInbox{
    
    _fromDateLabel.attributedText = [SINGLETON_INSTANCE getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:eventInbox.fromDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_fromDateLabel];
    
    _toDateLabel.attributedText = [SINGLETON_INSTANCE getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:eventInbox.toDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_toDateLabel];
    
    _assignedTeacherLabel.text = [NSString stringWithFormat:@"- by %@",eventInbox.userCreatedName];
    
    _venueLbl.text = [NSString stringWithFormat:@"Venue : %@",eventInbox.eventVenue];
    
    _eventTitleLbl.text = [NSString stringWithFormat:@"Title : %@",eventInbox.eventTitle];
    
    _timeLbl.text = [NSString stringWithFormat:@"Time : %@",[SINGLETON_INSTANCE convertTimeToTweleveHousrFomateFromString:eventInbox.eventTime]];
    
    _descriptionTextView.text = [NSString stringWithFormat:@"\t%@",eventInbox.eventDescription];
    
    if ([eventInbox.isHasAttachment intValue] == 1) {
        
        [_attachmentButton setHidden:NO];
    }
    else{
        
        [_attachmentButton setHidden:YES];
    }
    
    if ([eventInbox.fromDate compare:eventInbox.toDate]==NSOrderedSame) {
        
        _remainingCountLbl.text = [NSString stringWithFormat:@"%@",[TSSingleton daysBetweenFromDate:eventInbox.fromDate toDate:eventInbox.toDate]];
        
        _daysLeftLbl.text = @"Day Event";
        
    }else{
        
        _remainingCountLbl.text = [NSString stringWithFormat:@"%@",[TSSingleton daysBetweenFromDate:eventInbox.fromDate toDate:eventInbox.toDate]];
        
        _daysLeftLbl.text = @"Days Event";
    }
    
    /*NSPredicate *predicateInbox = [NSPredicate predicateWithFormat:@"transId MATCHES %@",eventInboxObj.transID];
     
     assigneeArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"AssignedDetails" withPredicate:predicateInbox];
     
     NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"assigneeType" ascending:YES];
     
     NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
     
     assigneeArr = [assigneeArr sortedArrayUsingDescriptors:descriptors];*/
    
    assigneeArr = [eventInbox.assignedList componentsSeparatedByString:ASSIGNEE_SAPRATOR];
    
    _assignedView.hidden = NO;
    
    if (assigneeArr.count ==1) {
        
        //_assignedObj = [assigneeArr objectAtIndex:0];
        
        _assignedTxtFld.text = [NSString stringWithFormat:@"%@ : %@",EventInvited,[assigneeArr objectAtIndex:0]];
        
    }else if (assigneeArr.count >1){
        
        [self.assignedView addGestureRecognizer:assignedViewTap];
        
        [SINGLETON_INSTANCE addRightViewToTextField:_assignedTxtFld withImageName:@"expand_arrow"];
    }
    else if (assigneeArr.count ==0){
        
        _assignedView.hidden = YES;
    }
}

- (IBAction)attachmentBtnClk:(id)sender {
    
    NSString *attachmentURLString;
    
    if ([_descriptionType isEqualToString:EventsString]) {
        
        attachmentURL = [NSURL URLWithString:eventObj.eventAttachmentUrl];
        
        attachmentURLString = eventObj.eventAttachmentUrl;
        
    }else if ([_descriptionType isEqualToString:EventsInboxString]){
        
        attachmentURL = [NSURL URLWithString:eventInboxObj.eventAttachment];
        
        attachmentURLString = eventInboxObj.eventAttachment;
    }
    
    
    if (attachmentURLString.length>0) {
        
        NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSString *attachmentFile = [documentsPath stringByAppendingPathComponent:[attachmentURL lastPathComponent]];
        
        NSLog(@"%@",attachmentFile);
        
        NSLog(@"%@",[attachmentURL lastPathComponent]);
        
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:attachmentFile];
        
        if (fileExists) {
            
            if ([_descriptionType isEqualToString:EventsString]) {
                
                [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_ATTACHMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_OPENEDFROMLOCAL,ANALYTICS_PARAM_ATTACHMENTSTATUS, nil]];
                
            }else if ([_descriptionType isEqualToString:EventsInboxString]){
                
                [TSAnalytics logEvent:ANALYTICS_INBOXEVENT_CLICKEDATTACHMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_OPENEDFROMLOCAL,ANALYTICS_PARAM_ATTACHMENTSTATUS, nil]];
                
            }
            
            [self navigateToAttachmentViewControllerWithFileName:attachmentURL];
        }
        else{
            
            if ([_descriptionType isEqualToString:EventsString]) {
                
                [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_ATTACHMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_DOWNLOADING,ANALYTICS_PARAM_ATTACHMENTSTATUS, nil]];
                
            }else if ([_descriptionType isEqualToString:EventsInboxString]){
                
                [TSAnalytics logEvent:ANALYTICS_INBOXEVENT_CLICKEDATTACHMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_DOWNLOADING,ANALYTICS_PARAM_ATTACHMENTSTATUS, nil]];
                
            }

            [[TSHandlerClass sharedHandler]downloadAttachmentWithURL:attachmentURLString currentView:self.view withHandler:^(BOOL success, NSURL *filePath) {
                
                if (success) {
                    
                    [self navigateToAttachmentViewControllerWithFileName:filePath];
                }
                else{
                    
                    [TSSingleton showAlertWithMessage:@"Attachment might be have broken url"];
                }
            }];
        }
    }
    else{
        
        [TSSingleton showAlertWithMessage:TEXT_INVALID_ATTACHMENT];
    }
}

-(void) navigateToAttachmentViewControllerWithFileName:(NSURL *)fileURL{
    
    TSAttachmentViewController *attachmentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAttachment"];
    
    [attachmentVC setFileName:[fileURL lastPathComponent]];
    
    UINavigationController *navCtrl = [[UINavigationController alloc]initWithRootViewController:attachmentVC];
    
    navCtrl.navigationBar.translucent = NO;
    
    navCtrl.navigationBar.barTintColor = APPTHEME_COLOR;
    
    navCtrl.navigationBar.tintColor = [UIColor whiteColor];
    
    [navCtrl.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:APP_FONT size:18]}];
    
    [self.navigationController presentViewController:navCtrl animated:YES completion:nil];
}

// Fetch Particular Event when Push Notification arrives.
-(void) fetchEventByEventTransIDAPICall{
    
    NSString *urlString = @"MastersMServices/EventsMService.svc/FetchEventByTransID";
    
    NSDictionary *params = @{@"guidEventTransID":SINGLETON_INSTANCE.pushNotificationCommTransID};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchEventByTransIDResult"][@"ResponseCode"] intValue] == 1) {
                
                if ([_descriptionType isEqualToString:EventsString]) {
                 
                    [[Events instance]savePushNotificationEventsWithResponseDict:responseDictionary];
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventTransId MATCHES %@ AND staffTransId MATCHES %@ AND isForApprovals == 0",SINGLETON_INSTANCE.pushNotificationCommTransID,SINGLETON_INSTANCE.staffTransId];
                    
                    NSArray *eventArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Events" withPredicate:predicate];
                    
                    if (eventArray.count>0) {
                        
                        Events *event = [eventArray objectAtIndex:0];
                        
                        [self loadViewWithEventObj:event];
                    }
                }
                else if([_descriptionType isEqualToString:EventsInboxString]){
                    
                    [[EventInbox instance]savePushNotificationEventWithResponseDict:responseDictionary];
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@ AND transID MATCHES %@",SINGLETON_INSTANCE.staffTransId,SINGLETON_INSTANCE.pushNotificationCommTransID];
                    
                    NSArray *eventInboxArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"EventInbox" withPredicate:predicate];
                    
                    if (eventInboxArray.count>0) {
                        
                        EventInbox *eventInbox = [eventInboxArray objectAtIndex:0];
                        
                        [self loadViewWithEventInboxObj:eventInbox];
                    }
                }
            }
            else{
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"FetchEventByTransIDResult"][@"ResponseMessage"]];
            }
        }
        else{
            
            [TSSingleton showAlertWithMessage:@"Something went wrong. Please try again."];
        }
    }];
}

-(void) descViewTapped{
    
    if ([_descriptionType isEqualToString:EventsString]) {
        
        [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_DESCRIPTION parameter:[NSMutableDictionary new]];
        
    }else if ([_descriptionType isEqualToString:EventsInboxString]){
        
        [TSAnalytics logEvent:ANALYTICS_INBOXEVENT_CLICKEDDESCRIPTIOIN parameter:[NSMutableDictionary new]];
    }
    
    TSDescriptionViewController *descVC = [[TSDescriptionViewController alloc] initWithNibName:@"TSDescriptionViewController" bundle:nil];
    
    [descVC showInViewController:self descriptionString:_descriptionTextView.text];
    
}

@end