//
//  TSProfileViewController.h
//  Trackidon Staff
//
//  Created by Elango on 17/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSProfileViewController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *staffImgView;

@property (weak, nonatomic) IBOutlet UILabel *nameTxtLbl;

@property (weak, nonatomic) IBOutlet UILabel *phoneTextLbl;

@property (weak, nonatomic) IBOutlet UILabel *emailTxtLbl;

@end
