//
//  TSFeedBackCreateViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/24/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Feedback;

@interface TSFeedBackCreateViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *feedbackTypeLbl;

@property (weak, nonatomic) IBOutlet UILabel *feedbackTitleLbl;

@property (weak, nonatomic) IBOutlet UILabel *parentNameLbl;

@property (weak, nonatomic) IBOutlet UILabel *studentNameLbl;

@property (weak, nonatomic) IBOutlet UITextView *feedbackDescriptionTxtVw;

@property (nonatomic,strong) Feedback *feedbackObj;

@end
