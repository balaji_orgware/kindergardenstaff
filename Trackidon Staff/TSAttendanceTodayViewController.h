//
//  TSAttendanceTodayViewController.h
//  Trackidon Staff
//
//  Created by ORGware on 25/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSHeaders.pch"
#import "TSSelectionViewController.h"

@interface TSAttendanceTodayViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIAlertViewDelegate,UIScrollViewDelegate,SelectionCallBackDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *classSecSelectionVw;
@property (weak, nonatomic) IBOutlet UITextField *boardTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *classTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *sectionTxtFld;

@property (weak, nonatomic) IBOutlet UIView *noOfStudentsView;
@property (weak, nonatomic) IBOutlet UILabel *totalStudentsCountLabel;
@property (weak, nonatomic) IBOutlet UITableView *studentsTableView;

@property (weak, nonatomic) IBOutlet UIView *doneView;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

- (IBAction)doneButtonAction:(id)sender;

@property (strong, nonatomic) UIView *presentView;
@property (strong, nonatomic) UIView *absentView;
@property (strong, nonatomic) UIView *lateView;
@property (strong, nonatomic) UIImageView *presentImgView;
@property (strong, nonatomic) UIImageView *absentImgView;
@property (strong, nonatomic) UIImageView *lateImgView;
@property (strong, nonatomic) UILabel *presentCountLabel;
@property (strong, nonatomic) UILabel *absentCountLabel;
@property (strong, nonatomic) UILabel *lateCountLabel;

@end

@interface StudentModel : NSObject

@property (nonatomic,strong) NSString *classSection;
@property (nonatomic,strong) NSString *enrollNo;
@property (nonatomic,strong) NSString *secondLangTransID;
@property (nonatomic,strong) NSString *studentName;
@property (nonatomic,strong) NSString *studentImage;
@property (nonatomic,strong) NSString *studentTransID;
@property (nonatomic,strong) NSString *transID;
@property (nonatomic,assign) NSInteger attendanceStatus;
@property (nonatomic,assign) BOOL studentPresent;
@property (nonatomic,assign) BOOL studentAbsent;
@property (nonatomic,assign) BOOL studentTardy;
@property (nonatomic,assign) BOOL isSelected;

@end