//
//  TSRemoteNotificationHandler.m
//  Trackidon Staff
//
//  Created by Balaji on 12/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSRemoteNotificationHandler.h"

@implementation TSRemoteNotificationHandler

// Class method to Get instance of Remote Notification Handler
+(instancetype) instance{
    
    static TSRemoteNotificationHandler *handler;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        handler = [[TSRemoteNotificationHandler alloc]init];
    });
    
    return handler;
}

#pragma mark - AlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 100) {
        
        if (buttonIndex) {
            
            [self userSelectionWithMenuID:_payloadDictionary];
        }
    }
}

// Method to handle Remote Notification with given Payload
-(void) handleNotificationWithPayload:(NSDictionary *)payload{
    
    @try {
        
        self.payloadDictionary = payload;
        
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
            
            //            NSString *staffTransID = self.payloadDictionary[@"data"][@"StID"];
            
            //            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId contains[cd] %@",staffTransID];
            
            //            NSArray *staffArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"StaffDetails" withPredicate:predicate];
            
            //            StaffDetails *staff;
            //
            //            if (staffArr.count > 0) {
            //
            //                staff = [staffArr objectAtIndex:0];
            //        }
            
            SINGLETON_INSTANCE.staffTransId = [USERDEFAULTS valueForKey:@"staffTransId"];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId MATCHES %@",SINGLETON_INSTANCE.staffTransId];
            
            NSArray *staffArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"StaffDetails" withPredicate:predicate];
            
            if (staffArr.count>0) {
                
                SINGLETON_INSTANCE.staff = [staffArr objectAtIndex:0];
            }
            
            SINGLETON_INSTANCE.academicTransId = SINGLETON_INSTANCE.staff.acadamicTransId;
            
            SINGLETON_INSTANCE.selectedSchoolTransId = [USERDEFAULTS valueForKey:@"schoolTransId"];
            
            SINGLETON_INSTANCE.selectedSchoolName = [USERDEFAULTS valueForKey:@"schoolName"];
            
            if (SINGLETON_INSTANCE.moduleMenuArr.count == 0) {
                
                [SINGLETON_INSTANCE createMenu];
            }
            
            [[TWMessageBarManager sharedInstance]showMessageWithTitle:@"Notification" description:self.payloadDictionary[@"aps"][@"alert"] type:TWMessageBarMessageTypeInfo callback:^{
                
                if ([SINGLETON_INSTANCE.staff.schoolTransId isEqualToString:[USERDEFAULTS objectForKey:@"SchoolTransId"]]) {
                    
                    [TSAnalytics logEvent:ANALYTICS_PUSHNOTIFICATION_RECEIVED parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:self.payloadDictionary[@"data"][@"MID"],ANALYTICS_PARAM_NOTIFICATIONTYPE, ANALYTICS_VALUE_LOGGEDIN,ANALYTICS_PARAM_ISLOGGEDIN,NO,ANALYTICS_PARAM_ISNEEDSSCHOOLCHANGE,nil]];
                    
                    [self userSelectionWithMenuID:_payloadDictionary];
                }
                else{
                    
                    [TSAnalytics logEvent:ANALYTICS_PUSHNOTIFICATION_RECEIVED parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:self.payloadDictionary[@"data"][@"MID"],ANALYTICS_PARAM_NOTIFICATIONTYPE, ANALYTICS_VALUE_LOGGEDIN,ANALYTICS_PARAM_ISLOGGEDIN,YES,ANALYTICS_PARAM_ISNEEDSSCHOOLCHANGE,nil]];
                    
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Change School" message:@"This notification is not for this school. Do you want to swap the school?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok",nil];
                    
                    alertView.delegate = self;
                    alertView.tag = 100;
                    
                    [alertView show];
                }
            }];
        }
        else{
            
            [self userSelectionWithMenuID:_payloadDictionary];
        }
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
    }
    @finally {
        
    }
}

-(void)userSelectionWithMenuID:(NSDictionary *)payload{
    
    @try {
        
        //NSError *error;
        
        //NSDictionary *jsonResponse = payload[@"data"];
        
        NSString *staffTransId = SINGLETON_INSTANCE.staffTransId;
        NSString *menuID = payload[@"data"][@"MID"];
        
        SINGLETON_INSTANCE.isPushNotification = YES;
        
        SINGLETON_INSTANCE.pushNotificationCommTransID = payload[@"data"][@"CID"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId contains[cd] %@",staffTransId];
        
        NSArray *staffArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"StaffDetails" withPredicate:predicate];
        
        if (staffArr.count > 0) {
            
            StaffDetails *staff = [staffArr objectAtIndex:0];
            
            SINGLETON_INSTANCE.staffTransId = staff.staffTransId;
            
            SINGLETON_INSTANCE.staff = staff;
            
            SINGLETON_INSTANCE.selectedSchoolTransId = staff.schoolTransId;
            
            [SINGLETON_INSTANCE createMenu];
            
            [TSAnalytics logEvent:ANALYTICS_PUSHNOTIFICATION_CLICKED_NOTIFICATION parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:self.payloadDictionary[@"data"][@"MID"],ANALYTICS_PARAM_NOTIFICATIONTYPE, ANALYTICS_VALUE_LOGGEDIN,ANALYTICS_PARAM_ISLOGGEDIN,nil]];
            
            [self navigateWithMenuID:menuID];
            
        }else {
            
            // No staffs with transID
        }
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    }
}

-(void)navigateWithMenuID:(NSString *)menuID{
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSInteger typeId = [menuID integerValue];
    
    switch (typeId) {
            
        case MENU_ASSIGNMENTS:
        {
            [self navigateToAssignmentDesc];
            
            break;
        }
            
        case MENU_ASSIGNMENT_CC:
        {
            [self navigateToAssignmentDesc];
            
            break;
        }
            
        case MENU_EVENTS:
        {
            [self navigateToEventDesc];
            
            break;
        }
            
        case MENU_EVENT_CC:
        {
            [self navigateToEventDesc];
            
            break;
        }
        case MENU_NOTIFICATIONS:
        {
            [self navigateToNotificationDesc];
            
            break;
        }
            
        case MENU_NOTIFICATION_CC:
        {
            [self navigateToNotificationDesc];
            
            break;
        }
            
        case MENU_PORTIONS:
        {
            [APPDELEGATE_INSTANCE tabBarController].selectedIndex = 0;
            
            UINavigationController *navController = [[APPDELEGATE_INSTANCE tabBarController]selectedViewController];
            
            UIViewController *communicationVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommunication"];
            
            UIViewController *portionVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBPortions"];
            
            UIViewController *portionDetailVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBPortionDescription"];
            
            TSPortionDescriptionViewController *portionDescVC = (TSPortionDescriptionViewController *)portionDetailVC;
            
            [portionDescVC setDescriptionType:PortionString];
            
            SINGLETON_INSTANCE.barTitle = @"Lesson Planner";
            
            [navController setViewControllers:@[communicationVC,portionVC,portionDetailVC] animated:NO];
            
            navController.navigationBarHidden = NO;
            
            break;
        }
            
        case MENU_TIME_TABLE:
        {
            [APPDELEGATE_INSTANCE tabBarController].selectedIndex = 0;
            
            UINavigationController *navController = [[APPDELEGATE_INSTANCE tabBarController]selectedViewController];
            
            UIViewController *communicationVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommunication"];
            
            UIViewController *portionVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBTimetable"];
            
            [navController setViewControllers:@[communicationVC,portionVC] animated:NO];
            
            navController.navigationBarHidden = NO;
            
            break;
            
        }
            
        case MENU_ATTENDANCE:
        {
            [APPDELEGATE_INSTANCE tabBarController].selectedIndex = 0;
            
            UINavigationController *navController = [[APPDELEGATE_INSTANCE tabBarController]selectedViewController];
            
            UIViewController *communicationVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommunication"];
            
            UIViewController *attendanceVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBAttendanceHome"];
            
            [navController setViewControllers:@[communicationVC,attendanceVC] animated:NO];
            
            navController.navigationBarHidden = NO;
            
            break;
            
        }
            
        case MENU_HOLIDAY:
        {
            [APPDELEGATE_INSTANCE tabBarController].selectedIndex = 0;
            
            UINavigationController *navController = [[APPDELEGATE_INSTANCE tabBarController]selectedViewController];
            
            UIViewController *communicationVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommunication"];
            
            UIViewController *holidayVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBHolidaysHome"];
            
            SINGLETON_INSTANCE.barTitle = @"Holiday";
            
            [navController setViewControllers:@[communicationVC,holidayVC] animated:NO];
            
            navController.navigationBarHidden = NO;
            
            break;
        }
            
        case MENU_INBOX_PORTION:
        {
            [APPDELEGATE_INSTANCE tabBarController].selectedIndex = 1;
            
            UINavigationController *navController = [[APPDELEGATE_INSTANCE tabBarController]selectedViewController];
            
            UIViewController *inboxVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBInbox"];
            
            UIViewController *inboxPortionsVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBPortionInbox"];
            
            UIViewController *inboxPortionDetailVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBPortionDescription"];
            
            TSPortionDescriptionViewController *inboxPortionDescVC = (TSPortionDescriptionViewController *)inboxPortionDetailVC;
            
            [inboxPortionDescVC setDescriptionType:PortionInboxString];
            
            [navController setViewControllers:@[inboxVC,inboxPortionsVC,inboxPortionDetailVC] animated:NO];
            
            navController.navigationBarHidden = NO;
            
            break;
            
        }
            
        case MENU_INBOX_EVENTS:
        {
            [APPDELEGATE_INSTANCE tabBarController].selectedIndex = 2;
            
            UINavigationController *navController = [[APPDELEGATE_INSTANCE tabBarController]selectedViewController];
            
            UIViewController *inboxVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBInbox"];
            
            UIViewController *inboxEventsVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBEventInbox"];
            
            UIViewController *inboxEventsDetailVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBEventsDescription"];
            
            TSEventsDescriptionViewController *inboxEventsDescVC = (TSEventsDescriptionViewController *)inboxEventsDetailVC;
            
            [inboxEventsDescVC setDescriptionType:EventsInboxString];
            
            [navController setViewControllers:@[inboxVC,inboxEventsVC,inboxEventsDetailVC] animated:NO];
            
            navController.navigationBarHidden = NO;
            
            break;
            
        }
            
        case MENU_INBOX_NOTIFICATION:
        {
            [APPDELEGATE_INSTANCE tabBarController].selectedIndex = 1;
            
            UINavigationController *navController = [[APPDELEGATE_INSTANCE tabBarController]selectedViewController];
            
            UIViewController *inboxVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBInbox"];
            
            UIViewController *inboxNotificationVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBNotificationInbox"];
            
            UIViewController *inboxNotificationDetailVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBNotificationDescription"];
            
            TSNotificationDescriptionViewController *inboxNotificationDescVC = (TSNotificationDescriptionViewController *)inboxNotificationDetailVC;
            
            [inboxNotificationDescVC setDescriptionType:NotificationsInboxString];
            
            [navController setViewControllers:@[inboxVC,inboxNotificationVC,inboxNotificationDetailVC] animated:NO];
            
            navController.navigationBarHidden = NO;
            
            break;
        }
            
        case MENU_ASSIGNMENT_APPROVALS:
        {
            [APPDELEGATE_INSTANCE tabBarController].selectedIndex = 3;
            
            UINavigationController *navController = [[APPDELEGATE_INSTANCE tabBarController]selectedViewController];
            
            UIViewController *approvalsVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBApprovals"];
            
            UIViewController *assignmentApprovalVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBAssignmentApprovals"];
            
            [navController setViewControllers:@[approvalsVC,assignmentApprovalVC] animated:NO];
            
            navController.navigationBarHidden = NO;
            
            break;
        }
            
        case MENU_EVENT_APPROVALS:
        {
            [APPDELEGATE_INSTANCE tabBarController].selectedIndex = 3;
            
            UINavigationController *navController = [[APPDELEGATE_INSTANCE tabBarController]selectedViewController];
            
            UIViewController *approvalsVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBApprovals"];
            
            UIViewController *eventApprovalVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBEventsApprovals"];
            
            [navController setViewControllers:@[approvalsVC,eventApprovalVC] animated:NO];
            
            navController.navigationBarHidden = NO;
            
            break;
        }
            
        case MENU_NOTIFICATION_APPROVALS:
        {
            [APPDELEGATE_INSTANCE tabBarController].selectedIndex = 3;
            
            UINavigationController *navController = [[APPDELEGATE_INSTANCE tabBarController]selectedViewController];
            
            UIViewController *approvalsVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBApprovals"];
            
            UIViewController *notificationApprovalVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBNotificationApprovals"];
            
            [navController setViewControllers:@[approvalsVC,notificationApprovalVC] animated:NO];
            
            navController.navigationBarHidden = NO;
            
            break;
        }
            
        default:
            break;
    }
}

-(void) navigateToAssignmentDesc{
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    [APPDELEGATE_INSTANCE tabBarController].selectedIndex = 0;
    
    UINavigationController *navController = [[APPDELEGATE_INSTANCE tabBarController]selectedViewController];
    
    UIViewController *communicationVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommunication"];
    
    UIViewController *assignmentsVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBAssignments"];
    
    UIViewController *assignmentsDescVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBAssignmentDescription"];
    
    SINGLETON_INSTANCE.barTitle = @"Assignment";
    
    [navController setViewControllers:@[communicationVC,assignmentsVC,assignmentsDescVC] animated:NO];
    
    navController.navigationBarHidden = NO;
}

-(void) navigateToEventDesc{
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    [APPDELEGATE_INSTANCE tabBarController].selectedIndex = 0;
    
    UINavigationController *navController = [[APPDELEGATE_INSTANCE tabBarController]selectedViewController];
    
    UIViewController *communicationVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommunication"];
    
    UIViewController *eventsVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBEventsHome"];
    
    UIViewController *eventsDetailVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBEventsDescription"];
    
    TSEventsDescriptionViewController *eventDescVC = (TSEventsDescriptionViewController *)eventsDetailVC;
    
    [eventDescVC setDescriptionType:EventsString];
    
    SINGLETON_INSTANCE.barTitle = @"Event";
    
    [navController setViewControllers:@[communicationVC,eventsVC,eventsDetailVC] animated:NO];
    
    navController.navigationBarHidden = NO;
}

-(void) navigateToNotificationDesc{
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    [APPDELEGATE_INSTANCE tabBarController].selectedIndex = 0;
    
    UINavigationController *navController = [[APPDELEGATE_INSTANCE tabBarController]selectedViewController];
    
    UIViewController *communicationVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBCommunication"];
    
    UIViewController *notificationVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBNotificationHome"];
    
    UIViewController *notificationDetailVC = [storyBoard instantiateViewControllerWithIdentifier:@"SBNotificationDescription"];
    
    TSNotificationDescriptionViewController *notificationDescVC = (TSNotificationDescriptionViewController *)notificationDetailVC;
    
    [notificationDescVC setDescriptionType:NotificationsString];
    
    SINGLETON_INSTANCE.barTitle = @"Notification";
    
    [navController setViewControllers:@[communicationVC,notificationVC,notificationDetailVC] animated:NO];
    
    navController.navigationBarHidden = NO;
}
@end