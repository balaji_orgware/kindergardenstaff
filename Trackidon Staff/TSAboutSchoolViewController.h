//
//  TSAboutSchoolViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/2/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface TSAboutSchoolViewController : UIViewController<MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *aboutScrollView;

@property (weak, nonatomic) IBOutlet UIImageView *schoolLogoImgView;

@property (weak, nonatomic) IBOutlet UITextView *descriptionTxtView;

@property (weak, nonatomic) IBOutlet UILabel *contactNoLabelFirst;
@property (weak, nonatomic) IBOutlet UILabel *schoolNameLbl;

@property (weak, nonatomic) IBOutlet UILabel *contactNoLabelSecond;

@property (weak, nonatomic) IBOutlet UILabel *emailLabel;

@property (weak, nonatomic) IBOutlet UILabel *webLabel;
@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UIView *contactView;

@end
