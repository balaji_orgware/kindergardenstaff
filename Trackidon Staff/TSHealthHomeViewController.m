//
//  TSHealthHomeViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 25/06/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSHealthHomeViewController.h"

#define ExaminedDateFormat @"dd-MMM-yyyy hh:mm"

@interface TSHealthHomeViewController (){
    
    NSArray *nameArray,*imageArray;
    
    NSMutableDictionary *healthRecordDict;
}
@end

@implementation BMIInfoModel

@end

@implementation DentalInfoModel

@end

@implementation EarInfoModel

@end

@implementation EyeInfoModel

@end

@implementation NoseThroatInfoModel

@end

@implementation EmergencyInfoModel

@end

@implementation GeneralInfoModel

@end

@implementation GeneralCheckupModel

@end


@implementation TSHealthHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    healthRecordDict = [NSMutableDictionary new];
    
    nameArray = @[@"General",@"Emergency",@"Eyes",@"Ears",@"Dental",@"Nose and Throat",@"General Checkup",@"Height and Weight"];
    
    imageArray = @[@"general",@"emergency",@"eye",@"ear",@"dental",@"throat",@"general_checkup",@"height"];
    
    _studentNameLbl.text = _studentObj.studentName;
    
    [self fetchHealthRecordsAPICall];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = SINGLETON_INSTANCE.barTitle;
    
    if (SINGLETON_INSTANCE.needRefreshHealthRecords) {
        [self fetchHealthRecordsAPICall];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Helper methods

-(NSMutableDictionary *)getGeneralInfoUpdateDictFromModelObj:(GeneralInfoModel *)modelObj{
    
    GeneralInfoModel *model = (GeneralInfoModel *)modelObj;
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setValue:model.hcTransID forKey:@"HCTransID"];
    
//    [dict setValue:model.transID forKey:@"TransID"];
//    [dict setValue:_studentObj.studentTransID forKey:@"StudentTransID"];
//    [dict setValue:model.bloodGroup forKey:@"BloodGroup"];
//    [dict setValue:model.skinColor forKey:@"SkinColour"];
//    [dict setValue:model.hairColor forKey:@"HairColour"];
//    [dict setValue:model.identityMarkOne forKey:@"IdentityMark1"];
//    [dict setValue:model.identityMarkTwo forKey:@"IdentityMark2"];
//    [dict setValue:model.foodAllergy forKey:@"FoodAllergy"];
//    [dict setValue:model.examinerID forKey:@"ExaminerID"];
//    [dict setValue:model.examinedBy forKey:@"ExaminedBy"];
//    [dict setValue:[SINGLETON_INSTANCE stringFromDate:model.examinedDate withFormate:@"dd-MMMM-yyyy"] forKey:@"ExaminedDate"];
    
    return dict;
}

#pragma mark - Collection view Datasources & Delegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 8;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:101];
    
    imageView.image = [UIImage imageNamed:[imageArray objectAtIndex:indexPath.row]];
    nameLabel.text = [nameArray objectAtIndex:indexPath.row];
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake((DeviceWidth-30)/2, (DeviceWidth-30)/2);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case HealthType_GeneralInfo:
        {
            TSHealthGeneralInfoViewController *generalInfoVC = (TSHealthGeneralInfoViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"SBHealthGeneralInfo"];
            
            NSArray *generalInfoArr = [healthRecordDict objectForKey:@"GeneralInfo"];
            
            [generalInfoVC setStudentObj:_studentObj];
            
            if (generalInfoArr.count >0) {
                
                GeneralInfoModel *modelObj = (GeneralInfoModel *)[generalInfoArr objectAtIndex:0];
                
                [generalInfoVC setGeneralInfoModelObj:modelObj];
            }
            
            [self.navigationController pushViewController:generalInfoVC animated:YES];

            break;
        }
            
        default:
        {
            NSArray *generalInfoArr = [healthRecordDict objectForKey:@"GeneralInfo"];
            
            GeneralInfoModel *modelObj = generalInfoArr.count>0 ? [generalInfoArr objectAtIndex:0] : nil;
            
            if (![modelObj.hcTransID isEqualToString:EMPTY_TRANSID] && modelObj != nil) {
        
                TSCommonListViewController *commonListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBHealthCommonList"];
                
                [commonListVC setModuleType:indexPath.row];
                [commonListVC setStudentObj:_studentObj];
                
                if (generalInfoArr.count>0) {
                    [commonListVC setGeneralInfoDict:[self getGeneralInfoUpdateDictFromModelObj:[generalInfoArr objectAtIndex:0]]];
                }
                
                NSArray *contentArray;
                
                switch (indexPath.row) {
                    case HealthType_Eyes:
                        contentArray = [healthRecordDict objectForKey:@"EyeInfo"];
                        break;
                        
                    case HealthType_Ears:
                        contentArray = [healthRecordDict objectForKey:@"EarInfo"];
                        break;
                        
                    case HealthType_Dental:
                        contentArray = [healthRecordDict objectForKey:@"DentalInfo"];
                        break;
                        
                    case HealthType_NoseAndThroat:
                        contentArray = [healthRecordDict objectForKey:@"NoseThroatInfo"];
                        break;
                        
                    case HealthType_GeneralCheckup:
                        contentArray = [healthRecordDict objectForKey:@"GeneralCheckUp"];
                        break;
                        
                    case HealthType_Emergency:
                        contentArray = [healthRecordDict objectForKey:@"EmergencyInfo"];
                        break;
                        
                    case HealthType_HeightAndWeight:
                        contentArray = [healthRecordDict objectForKey:@"BMIInfo"];
                        break;
                        
                    default:
                        break;
                }
                
                if (contentArray.count>0){
                    
                    NSSortDescriptor *sortDesc = [[NSSortDescriptor alloc]initWithKey:@"examinedDate" ascending:NO];
                    NSArray *sortedArray = [contentArray sortedArrayUsingDescriptors:@[sortDesc]];
                    
                    [commonListVC setContentArray:sortedArray];
                }
                
                [self.navigationController pushViewController:commonListVC animated:YES];
                
                break;

            }
            else{
                [TSSingleton showAlertWithMessage:@"Please update General Info Records first. So that you can continue other updates"];
                break;
            }
        }
    }
}

#pragma mark - Service call

-(void) fetchHealthRecordsAPICall{
    
    NSString *urlString = @"HealthDetails/Services/HealthCardService.svc/StudentHealthCard";
    
//    NSDictionary *params = @{@"StudentTransID":_studentObj.studentTransID,@"LastRecDate":@"10-Aug-2015"};
    
    NSDictionary *params = @{@"StudentTransID":_studentObj.studentTransID};
    
    [[TSHandlerClass sharedHandler] postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            NSDictionary *healthCardResult = responseDictionary[@"StudentHealthCardResult"];
            
            if ([healthCardResult[@"ResponseCode"] integerValue] == 1 ) {
                
                [healthRecordDict removeAllObjects];
                
                // BMI Info Records
                
                NSMutableArray *bmiRecordsArr = [NSMutableArray new];
                
                for (NSDictionary *tmpDict in healthCardResult[@"BMIInfo"]) {
                    
                    NSDictionary *dict = [tmpDict dictionaryByReplacingNullsWithBlanks];
                    
                    BMIInfoModel *modelObj = [BMIInfoModel new];
                    modelObj.examinedBy = dict[@"ExaminedBy"];
                    modelObj.examinerID = dict[@"ExaminerID"];
                    modelObj.examinedDate = [SINGLETON_INSTANCE dateFromString:dict[@"ExaminedDate"] withFormate:ExaminedDateFormat];
                    modelObj.hcTransID = dict[@"HCTransID"];
                    modelObj.transID = dict[@"TransID"];
                    modelObj.height = [NSString stringWithFormat:@"%@",dict[@"Height"]];
                    modelObj.weight = [NSString stringWithFormat:@"%@",dict[@"Weight"]];
                    modelObj.bmi = [NSString stringWithFormat:@"%@",dict[@"BMI"]];
                    modelObj.remarks = dict[@"Remarks"];
                    
                    [bmiRecordsArr addObject:modelObj];
                    modelObj = nil;
                }
                
                [healthRecordDict setObject:bmiRecordsArr forKey:@"BMIInfo"];
                
                // DentalInfo Records
                
                NSMutableArray *dentalRecordsArr = [NSMutableArray new];
                
                for (NSDictionary *tmpDict in healthCardResult[@"DentalInfo"]) {
                    
                    NSDictionary *dict = [tmpDict dictionaryByReplacingNullsWithBlanks];
                    
                    DentalInfoModel *modelObj = [DentalInfoModel new];
                    modelObj.examinedBy = dict[@"ExaminedBy"];
                    modelObj.examinerID = dict[@"ExaminerID"];
                    modelObj.examinedDate = [SINGLETON_INSTANCE dateFromString:dict[@"ExaminedDate"] withFormate:ExaminedDateFormat];
                    modelObj.hcTransID = dict[@"HCTransID"];
                    modelObj.transID = dict[@"TransID"];
                    modelObj.remarks = dict[@"Remarks"];
                    modelObj.intraOral = dict[@"IntraOral"];
                    modelObj.extraOral = dict[@"ExtraOral"];
                    modelObj.badBreath = [dict[@"BadBreath"] boolValue];
                    modelObj.gumBleeding = [dict[@"GumBleeding"] boolValue];
                    modelObj.gumInflammation = [dict[@"GumInflammation"] boolValue];
                    modelObj.plaque = [dict[@"Plaque"] boolValue];
                    modelObj.softTissue = [dict[@"SoftTissue"] boolValue];
                    modelObj.stains = [dict[@"Stains"] boolValue];
                    modelObj.tarter = [dict[@"Tarter"] boolValue];
                    modelObj.toothCavity = [dict[@"ToothCavity"] boolValue];
                    
                    [dentalRecordsArr addObject:modelObj];
                    modelObj = nil;
                }
                
                [healthRecordDict setObject:dentalRecordsArr forKey:@"DentalInfo"];
                
                // Ear Info Records
                
                NSMutableArray *earInfoRecordsArray = [NSMutableArray new];
                
                for (NSDictionary *tmpDict in healthCardResult[@"EarInfo"]) {
                    
                    NSDictionary *dict = [tmpDict dictionaryByReplacingNullsWithBlanks];
                    
                    EarInfoModel *modelObj = [EarInfoModel new];
                    modelObj.examinedBy = dict[@"ExaminedBy"];
                    modelObj.examinerID = dict[@"ExaminerID"];
                    modelObj.examinedDate = [SINGLETON_INSTANCE dateFromString:dict[@"ExaminedDate"] withFormate:ExaminedDateFormat];
                    modelObj.hcTransID = dict[@"HCTransID"];
                    modelObj.transID = dict[@"TransID"];
                    modelObj.leftEarTitle = dict[@"LeftEarTitle"];
                    modelObj.leftEarRemarks = dict[@"LeftEarRemarks"];
                    modelObj.rightEarTitle = dict[@"RightEarTitle"];
                    modelObj.rightEarRemarks = dict[@"RightEarRemarks"];
                    
                    [earInfoRecordsArray addObject:modelObj];
                    modelObj = nil;
                }
                
                [healthRecordDict setObject:earInfoRecordsArray forKey:@"EarInfo"];
                
                // Emergency Info Records
                
                NSMutableArray *emergencyInfoRecordsArr = [NSMutableArray new];
                
                for (NSDictionary *tmpDict in healthCardResult[@"EmergencyInfo"]) {
                    
                    NSDictionary *dict = [tmpDict dictionaryByReplacingNullsWithBlanks];
                    
                    EmergencyInfoModel *modelObj = [EmergencyInfoModel new];
                    modelObj.examinedBy = dict[@"ExaminedBy"];
                    modelObj.examinerID = dict[@"ExaminerID"];
                    modelObj.examinedDate = [SINGLETON_INSTANCE dateFromString:dict[@"ExaminedDate"] withFormate:ExaminedDateFormat];
                    modelObj.hcTransID = dict[@"HCTransID"];
                    modelObj.transID = dict[@"TransID"];
                    modelObj.emergencyTitle = dict[@"Title"];
                    modelObj.emergencyRemarks = dict[@"EmergencyRemarks"];
                    
                    [emergencyInfoRecordsArr addObject:modelObj];
                    modelObj = nil;
                }
                
                [healthRecordDict setObject:emergencyInfoRecordsArr forKey:@"EmergencyInfo"];
                
                // EyeInfo Records
                
                NSMutableArray *eyeInfoRecordsArr = [NSMutableArray new];
                
                for (NSDictionary *tmpDict in healthCardResult[@"EyeInfo"]) {
                    
                    NSDictionary *dict = [tmpDict dictionaryByReplacingNullsWithBlanks];
                    
                    EyeInfoModel *modelObj = [EyeInfoModel new];
                    modelObj.examinedBy = dict[@"ExaminedBy"];
                    modelObj.examinerID = dict[@"ExaminerID"];
                    modelObj.examinedDate = [SINGLETON_INSTANCE dateFromString:dict[@"ExaminedDate"] withFormate:ExaminedDateFormat];
                    modelObj.hcTransID = dict[@"HCTransID"];
                    modelObj.transID = dict[@"TransID"];
                    modelObj.leftEyeTitle = dict[@"LeftEyeTitle"];
                    modelObj.leftEyeRemarks = dict[@"LeftEyeRemarks"];
                    modelObj.rightEyeTitle = dict[@"RightEyeTitle"];
                    modelObj.rightEyeRemarks = dict[@"RightEyeRemarks"];
                    
                    [eyeInfoRecordsArr addObject:modelObj];
                    modelObj = nil;
                }
                
                [healthRecordDict setObject:eyeInfoRecordsArr forKey:@"EyeInfo"];
                
                // General Checkup Records
                
                NSMutableArray *generalCheckupArr = [NSMutableArray new];
                
                for (NSDictionary *tmpDict in healthCardResult[@"GeneralCheckUp"]) {
                    
                    NSDictionary *dict = [tmpDict dictionaryByReplacingNullsWithBlanks];
                    
                    GeneralCheckupModel *modelObj = [GeneralCheckupModel new];
                    modelObj.examinedBy = dict[@"ExaminedBy"];
                    modelObj.examinerID = dict[@"ExaminerID"];
                    modelObj.examinedDate = [SINGLETON_INSTANCE dateFromString:dict[@"ExaminedDate"] withFormate:ExaminedDateFormat];
                    modelObj.hcTransID = dict[@"HCTransID"];
                    modelObj.transID = dict[@"TransID"];
                    modelObj.abdomen = dict[@"Abdomen"];
                    modelObj.allergy = dict[@"Allergy"];
                    modelObj.anemia = dict[@"Anemia"];
                    modelObj.cardioVascular = dict[@"CardioVascular"];
                    modelObj.handsRemarks = dict[@"HandsRemarks"];
                    modelObj.legsRemarks = dict[@"LegsRemarks"];
                    modelObj.neckRemarks = dict[@"NeckRemarks"];
                    modelObj.nerveSystem = dict[@"NerveSystem"];
                    modelObj.respiratary = dict[@"Respiratary"];
                    
                    [generalCheckupArr addObject:modelObj];
                    modelObj = nil;
                }
                
                [healthRecordDict setObject:generalCheckupArr forKey:@"GeneralCheckUp"];
                
                
                // General Info Records
                
                NSMutableArray *generalInfoArr = [NSMutableArray new];
                
                NSDictionary *dict = [healthCardResult[@"GeneralInfo"] dictionaryByReplacingNullsWithBlanks];
                
                GeneralInfoModel *modelObj = [GeneralInfoModel new];
                modelObj.examinedBy = dict[@"ExaminedBy"];
                modelObj.examinerID = dict[@"ExaminerID"];
                modelObj.examinedDate = [SINGLETON_INSTANCE dateFromString:dict[@"ExaminedDate"] withFormate:ExaminedDateFormat];
                modelObj.hcTransID = dict[@"HCTransID"];
                modelObj.transID = dict[@"TransID"];
                modelObj.bloodGroup = dict[@"BloodGroup"];
                modelObj.foodAllergy = dict[@"FoodAllergy"];
                modelObj.hairColor = dict[@"HairColour"];
                modelObj.identityMarkOne = dict[@"IdentityMark1"];
                modelObj.identityMarkTwo = dict[@"IdentityMark2"];
                modelObj.skinColor = dict[@"SkinColour"];
                modelObj.studentTransID = dict[@"StudentTransID"];
                
                [generalInfoArr addObject:modelObj];
                    
                [healthRecordDict setObject:generalInfoArr forKey:@"GeneralInfo"];
                
                // Nose Throat Info Records
                
                NSMutableArray *noseThroatInfoArr = [NSMutableArray new];
                
                for (NSDictionary *tmpDict in healthCardResult[@"NoseThroatInfo"]) {
                    
                    NSDictionary *dict = [tmpDict dictionaryByReplacingNullsWithBlanks];
                    
                    NoseThroatInfoModel *modelObj = [NoseThroatInfoModel new];
                    modelObj.examinedBy = dict[@"ExaminedBy"];
                    modelObj.examinerID = dict[@"ExaminerID"];
                    modelObj.examinedDate = [SINGLETON_INSTANCE dateFromString:dict[@"ExaminedDate"] withFormate:ExaminedDateFormat];
                    modelObj.hcTransID = dict[@"HCTransID"];
                    modelObj.transID = dict[@"TransID"];
                    modelObj.noseDescription = dict[@"NoseDescription"];
                    modelObj.noseTitle = dict[@"NoseTitle"];
                    modelObj.throatDescription = dict[@"ThroatDescription"];
                    modelObj.throatTitle = dict[@"ThroatTitle"];
                    
                    [noseThroatInfoArr addObject:modelObj];
                    modelObj = nil;
                }
                
                [healthRecordDict setObject:noseThroatInfoArr forKey:@"NoseThroatInfo"];
            }
            else{
                [TSSingleton showAlertWithMessage:responseDictionary[@"StudentHealthCardResult"][@"ResponseMessage"]];
//                [self.navigationController popViewControllerAnimated:NO];
            }
        }
    }];
}

@end
