//
//  TSHealthGeneralInfoViewController.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 15/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSHealthGeneralInfoViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,SelectionCallBackDelegate>

@property (weak, nonatomic) IBOutlet UIView *studentNameVw;
@property (weak, nonatomic) IBOutlet UILabel *studentNameLbl;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *editButtonVw;
@property (weak, nonatomic) IBOutlet UIButton *editButton;

- (IBAction)editButtonTapped:(id)sender;

@property (nonatomic,strong) StudentModel *studentObj;
@property (nonatomic,strong) GeneralInfoModel *generalInfoModelObj;

@end
