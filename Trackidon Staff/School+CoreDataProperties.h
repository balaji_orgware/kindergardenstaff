//
//  School+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 08/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "School.h"

@class StaffList;
@class GroupList;

NS_ASSUME_NONNULL_BEGIN

@interface School (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *latitude;
@property (nullable, nonatomic, retain) NSString *longtitude;
@property (nullable, nonatomic, retain) NSString *schoolAboutUs;
@property (nullable, nonatomic, retain) NSString *schoolCode;
@property (nullable, nonatomic, retain) NSString *schoolContactNo;
@property (nullable, nonatomic, retain) NSString *schoolEmailID;
@property (nullable, nonatomic, retain) NSData *schoolLogoData;
@property (nullable, nonatomic, retain) NSString *schoolLogoUrl;
@property (nullable, nonatomic, retain) NSString *schoolName;
@property (nullable, nonatomic, retain) NSString *schoolTransId;
@property (nullable, nonatomic, retain) NSString *staffTransId;
@property (nullable, nonatomic, retain) NSString *staffUserMannualUrl;
@property (nullable, nonatomic, retain) NSSet<Board *> *board;
@property (nullable, nonatomic, retain) NSSet<Menu *> *menu;
@property (nullable, nonatomic, retain) StaffDetails *staff;
@property (nullable, nonatomic, retain) NSSet<StaffList *> *staffList;
@property (nullable, nonatomic, retain) NSSet<GroupList *> *groupList;

@end

@interface School (CoreDataGeneratedAccessors)

- (void)addBoardObject:(Board *)value;
- (void)removeBoardObject:(Board *)value;
- (void)addBoard:(NSSet<Board *> *)values;
- (void)removeBoard:(NSSet<Board *> *)values;

- (void)addMenuObject:(Menu *)value;
- (void)removeMenuObject:(Menu *)value;
- (void)addMenu:(NSSet<Menu *> *)values;
- (void)removeMenu:(NSSet<Menu *> *)values;

- (void)addStaffListObject:(StaffList *)value;
- (void)removeStaffListObject:(StaffList *)value;
- (void)addStaffList:(NSSet<StaffList *> *)values;
- (void)removeStaffList:(NSSet<StaffList *> *)values;

- (void)addGroupListObject:(GroupList *)value;
- (void)removeGroupListObject:(GroupList *)value;
- (void)addGroupList:(NSSet<GroupList *> *)values;
- (void)removeGroupList:(NSSet<GroupList *> *)values;

@end

NS_ASSUME_NONNULL_END
