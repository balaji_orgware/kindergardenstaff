//
//  TSPortionDescriptionViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/29/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSPortionDescriptionViewController.h"

@interface TSPortionDescriptionViewController ()
{
    NSURL *attachmentURL;
    
    Portions *portionsObj;
    
    PortionInbox *portionInboxObj;
    
    NSArray *assigneeArr;
    
    UITapGestureRecognizer *assignedViewTap;
}
@end

@implementation TSPortionDescriptionViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _attachmentButton.frame = CGRectMake(DEVICE_SIZE.width - _attachmentButton.frame.size.width - 10, _subjectHeaderView.frame.origin.y + _subjectHeaderView.frame.size.height - _attachmentButton.frame.size.height/2 + 18, _attachmentButton.frame.size.height, _attachmentButton.frame.size.height);
    
    [TSSingleton layerDrawForView:_subjectHeaderView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];

    [TSSingleton layerDrawForView:_assignedView position:LAYER_TOP color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:_assignedView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    _portionDateLabel.layer.cornerRadius = _portionDateLabel.frame.size.width/2;
    _portionDateLabel.layer.masksToBounds = YES;
    
    _assignedTxtFld.rightView = _assignedImgView;
    
    _assignedTxtFld.rightViewMode = UITextFieldViewModeAlways;
    
    assignedViewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(assignedViewTapped)];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(descViewTapped)];
    
    [_descriptionTextView addGestureRecognizer:tapGesture];
    
    if (SINGLETON_INSTANCE.isPushNotification) {
        
        SINGLETON_INSTANCE.isPushNotification = NO;
        
        [self fetchPortionsAPICall];
    }
    else{
        
        if ([_descriptionType isEqualToString:PortionString]) {
            
            portionsObj = (Portions *)self.commonObj;
            
            [self loadViewWithPortions:portionsObj];
        }
        else if ([_descriptionType isEqualToString:PortionInboxString]){
            
            portionInboxObj = (PortionInbox *)self.commonObj;
            
            [self loadViewWithPortionsObj:portionInboxObj];
        }
    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    if ([_descriptionType isEqualToString:PortionString]) {
        
        [TSAnalytics logScreen:ANALYTICS_SCREEN_PORTIONDESCRIPTION parameter:[NSMutableDictionary new]];
        
        self.navigationItem.title = [NSString stringWithFormat:@"%@ Detail",SINGLETON_INSTANCE.barTitle];
        
    }else if ([_descriptionType isEqualToString:PortionInboxString]){
        
        [TSAnalytics logScreen:ANALYTICS_SCREEN_INBOXPORTION_DESCRIPTION parameter:[NSMutableDictionary new]];
        
        self.navigationItem.title = @"Lesson Plan Detail";
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - Helper Methods

-(void) loadViewWithPortions:(Portions *)portion{
    
    _assignedTeacherLabel.text = [NSString stringWithFormat:@"- by %@",portion.createdByName];
    
    _descriptionTextView.text = [NSString stringWithFormat:@"\t%@",portion.portionDescription];
    
    _titleLabel.text = [NSString stringWithFormat:@"Title : %@",portion.portionTitle] ;
    
    _subjectTitleLabel.text = [NSString stringWithFormat:@"Subject : %@",portion.subjectTitle];
    
    _portionDateLabel.attributedText = [SINGLETON_INSTANCE getDateAttributedStringForDateString:portion.portionDate inLabel:_portionDateLabel];
    
    if ([portion.isAttachment intValue] == 1) {
        
        [_attachmentButton setHidden:NO];
    }
    else{
        
        [_attachmentButton setHidden:YES];
    }
    
    /*NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transId MATCHES %@",portionsObj.transID];
     
     assigneeArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"AssignedDetails" withPredicate:predicate];
     
     NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"assigneeType" ascending:YES];
     
     NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
     
     assigneeArr = [assigneeArr sortedArrayUsingDescriptors:descriptors];*/
    
    assigneeArr = [portion.assignedList componentsSeparatedByString:ASSIGNEE_SAPRATOR];
    
    _assignedView.hidden = NO;
    
    if (assigneeArr.count ==1) {
        
        _assignedTxtFld.text = [NSString stringWithFormat:@"%@ : %@",PortionAssigned,[assigneeArr objectAtIndex:0]];
        
    }else if (assigneeArr.count >1){
        
        [self.assignedView addGestureRecognizer:assignedViewTap];
        
        [SINGLETON_INSTANCE addRightViewToTextField:_assignedTxtFld withImageName:@"expand_arrow"];
    }
    else if (assigneeArr.count ==0){
        
        _assignedView.hidden = YES;
    }
}

-(void) loadViewWithPortionsObj:(PortionInbox *)portionInbox{
    
    _assignedTeacherLabel.text = [NSString stringWithFormat:@"- by %@",portionInbox.createdBy];
    
    _descriptionTextView.text = [NSString stringWithFormat:@"\t%@",portionInbox.portionsDescription];
    
    _titleLabel.text = [NSString stringWithFormat:@"Title : %@",portionInbox.portionsTitle];
    
    _subjectTitleLabel.text = [NSString stringWithFormat:@"Subject : %@",portionInbox.subjectName];
    
    _portionDateLabel.attributedText = [SINGLETON_INSTANCE getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:portionInbox.portionsDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_portionDateLabel];
    
    if ([portionInbox.isHasAttachment intValue] == 1) {
        
        [_attachmentButton setHidden:NO];
    }
    else{
        
        [_attachmentButton setHidden:YES];
    }
    
    /*NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transId MATCHES %@",portionInboxObj.transID];
     
     assigneeArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"AssignedDetails" withPredicate:predicate];
     
     NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"assigneeType" ascending:YES];
     
     NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
     
     assigneeArr = [assigneeArr sortedArrayUsingDescriptors:descriptors];*/
    
    assigneeArr = [portionInbox.assignedList componentsSeparatedByString:ASSIGNEE_SAPRATOR];
    
    _assignedView.hidden = NO;
    
    if (assigneeArr.count ==1) {
        
        _assignedTxtFld.text = [NSString stringWithFormat:@"%@ : %@",PortionAssigned,[assigneeArr objectAtIndex:0]];
        
    }else if (assigneeArr.count >1){
        
        [self.assignedView addGestureRecognizer:assignedViewTap];
        
        [SINGLETON_INSTANCE addRightViewToTextField:_assignedTxtFld withImageName:@"expand_arrow"];
    }
    else if (assigneeArr.count ==0){
        
        _assignedView.hidden = YES;
    }
}

-(void)assignedViewTapped{
    
    if ([_descriptionType isEqualToString:PortionString]) {
        
        [TSAnalytics logEvent:ANALYTICS_PORTION_CLICKED_VIEW_RECEIVER parameter:[NSMutableDictionary new]];

        
    }else if ([_descriptionType isEqualToString:PortionInboxString]){
        
        [TSAnalytics logEvent:ANALYTICS_INBOXPORTION_CLICKEDVIEWRECEIVER parameter:[NSMutableDictionary new]];
    }
    
    AssignedListViewController *listVC = [[AssignedListViewController alloc]initWithNibName:@"AssignedListViewController" bundle:nil];
    
    [listVC setAssignedListArray:assigneeArr];
    
    [listVC setAssignedText:PortionAssigned];
    
    [listVC showInViewController:self];
    
}

// Method to calculate the no of days between two dates
-(NSInteger) calculateNoOfDaysBetween:(NSString *)dayOne andDayTwo:(NSString *)dayTwo{
    
    NSDate *firstDate = [SINGLETON_INSTANCE dateFromString:dayOne withFormate:DATE_FORMATE_SERVICE_RESPONSE];
    
    NSDate *secondDate = [SINGLETON_INSTANCE dateFromString:dayTwo withFormate:DATE_FORMATE_SERVICE_RESPONSE];
    
    float timeInterval = [secondDate timeIntervalSinceDate:firstDate];
    
    NSInteger noOfDays = (NSInteger)timeInterval/(60*60*24);
    
    return noOfDays+1;
}

- (IBAction)attachmentBtnClk:(id)sender {
        
    NSString *attachmentURLString;
    
    if ([_descriptionType isEqualToString:PortionString]) {
        
        attachmentURL = [NSURL URLWithString:portionsObj.attachment];
        
        attachmentURLString = portionsObj.attachment;
    }
    else if([_descriptionType isEqualToString:PortionInboxString]){
        
        attachmentURL = [NSURL URLWithString:portionInboxObj.portionsAttachment];
        
        attachmentURLString = portionInboxObj.portionsAttachment;
    }
    
    
    if (attachmentURLString.length>0) {
        
        NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSString *attachmentFile = [documentsPath stringByAppendingPathComponent:[attachmentURL lastPathComponent]];
        
        NSLog(@"%@",attachmentFile);
        
        NSLog(@"%@",[attachmentURL lastPathComponent]);
        
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:attachmentFile];
        
        if (fileExists) {
            
            if ([_descriptionType isEqualToString:PortionString]) {
                
                [TSAnalytics logEvent:ANALYTICS_PORTION_CLICKED_ATTACHMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_OPENEDFROMLOCAL,ANALYTICS_PARAM_ATTACHMENTSTATUS, nil]];
                
                
            }else if ([_descriptionType isEqualToString:PortionInboxString]){
                
                [TSAnalytics logEvent:ANALYTICS_INBOXPORTION_CLICKEDATTECTMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_OPENEDFROMLOCAL,ANALYTICS_PARAM_ATTACHMENTSTATUS, nil]];
                
            }
            
            [self navigateToAttachmentViewControllerWithFileName:attachmentURL];
        }
        else{
            
            if ([_descriptionType isEqualToString:PortionString]) {
                
                [TSAnalytics logEvent:ANALYTICS_PORTION_CLICKED_ATTACHMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_DOWNLOADING,ANALYTICS_PARAM_ATTACHMENTSTATUS, nil]];
                
                
            }else if ([_descriptionType isEqualToString:PortionInboxString]){
                
                [TSAnalytics logEvent:ANALYTICS_INBOXPORTION_CLICKEDATTECTMENT parameter:[NSMutableDictionary dictionaryWithObjectsAndKeys:ANALYTICS_VALUE_DOWNLOADING,ANALYTICS_PARAM_ATTACHMENTSTATUS, nil]];
                
            }

            [[TSHandlerClass sharedHandler]downloadAttachmentWithURL:attachmentURLString currentView:self.view withHandler:^(BOOL success, NSURL *filePath) {
                
                if (success) {
                    
                    [self navigateToAttachmentViewControllerWithFileName:filePath];
                }
                else{
                    
                    [TSSingleton showAlertWithMessage:@"Attachment might be have broken url"];
                }
            }];
        }
    }
    else{
        
        [TSSingleton showAlertWithMessage:TEXT_INVALID_ATTACHMENT];
    }
}

-(void) navigateToAttachmentViewControllerWithFileName:(NSURL *)fileURL{
    
    TSAttachmentViewController *attachmentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAttachment"];
    
    [attachmentVC setFileName:[fileURL lastPathComponent]];
    
    UINavigationController *navCtrl = [[UINavigationController alloc]initWithRootViewController:attachmentVC];
    
    navCtrl.navigationBar.translucent = NO;
    
    navCtrl.navigationBar.barTintColor = APPTHEME_COLOR;
    
    navCtrl.navigationBar.tintColor = [UIColor whiteColor];
    
    [navCtrl.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:APP_FONT size:18]}];
    
    [self.navigationController presentViewController:navCtrl animated:YES completion:nil];
}

#pragma mark - API Call

-(void) fetchPortionsAPICall{
    
    NSString *urlString = @"MastersMServices/PortionsService.svc/FetchPortionsByTransID";
    
    NSDictionary *params = @{@"guidPortionsTransID":SINGLETON_INSTANCE.pushNotificationCommTransID};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchPortionsByTransIDResult"][@"ResponseCode"]intValue] == 1) {
                
                if ([_descriptionType isEqualToString:PortionString]) {
                    
                    [[Portions instance]storePushNotificationPortionWithDict:responseDictionary];
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@ AND transID MATCHES %@",SINGLETON_INSTANCE.staffTransId,SINGLETON_INSTANCE.pushNotificationCommTransID];
                    
                    NSArray *portionsArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Portions" withPredicate:predicate];
                    
                    if (portionsArray.count>0) {
                        
                        Portions *portionObj = [portionsArray objectAtIndex:0];
                        
                        [self loadViewWithPortions:portionObj];
                    }
                    
                }
                else if([_descriptionType isEqualToString:PortionInboxString]){
                    
                    [[PortionInbox instance]savePushNotificationPortionInboxWithDict:responseDictionary];
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@ AND transID MATCHES %@",SINGLETON_INSTANCE.staffTransId,SINGLETON_INSTANCE.pushNotificationCommTransID];
                    
                    NSArray *portionInboxArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"PortionInbox" withPredicate:predicate];
                    
                    if (portionInboxArray.count>0) {
                        
                        PortionInbox *portionInbox = [portionInboxArray objectAtIndex:0];
                        
                        [self loadViewWithPortionsObj:portionInbox];
                    }
                }
            }
            else{
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"FetchPortionsByTransIDResult"][@"ResponseMessage"]];
            }
        }
        else{
            
            [TSSingleton showAlertWithMessage:@"Time out error. Please try again"];
        }
    }];
}

-(void) descViewTapped{
    
    if ([_descriptionType isEqualToString:PortionString]) {
        
        [TSAnalytics logEvent:ANALYTICS_PORTION_CLICKED_DESCRIPTION parameter:[NSMutableDictionary new]];
        
    }else if ([_descriptionType isEqualToString:PortionInboxString]){
        
        [TSAnalytics logEvent:ANALYTICS_INBOXPORTION_CLICKEDDESCRIPTION parameter:[NSMutableDictionary new]];
        
    }
    
    TSDescriptionViewController *descVC = [[TSDescriptionViewController alloc] initWithNibName:@"TSDescriptionViewController" bundle:nil];
    
    [descVC showInViewController:self descriptionString:_descriptionTextView.text];
    
}

@end