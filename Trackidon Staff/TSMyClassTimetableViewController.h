//
//  TSMyClassTimetableViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 15/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSMyClassTimetableViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *timeTableTableView;

@property (weak, nonatomic) IBOutlet UIView *daysView;

- (IBAction)dayButtonTapped:(id)sender;

@end
