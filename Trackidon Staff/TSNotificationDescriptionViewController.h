//
//  TSNotificationDescriptionViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 3/29/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Notifications;

@interface TSNotificationDescriptionViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topView;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@property (weak, nonatomic) IBOutlet UIButton *attachmentButton;

@property (weak, nonatomic) IBOutlet UILabel *createdByLbl;

@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (weak, nonatomic) IBOutlet UIView *dateView;

@property (weak, nonatomic) IBOutlet UILabel *notificationPriorityTextLbl;

@property (weak, nonatomic) IBOutlet UIView *notificationPriorityView;

@property (weak, nonatomic) IBOutlet UILabel *fromDateLbl;

@property (weak, nonatomic)NSMutableArray *notificationDetailsArray;

@property (weak, nonatomic) IBOutlet UIView *assignedView;

@property (weak, nonatomic) IBOutlet UILabel *createdOnTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *alertImageView;


@property (weak, nonatomic) IBOutlet UITextField *assignedTxtFld;

//@property (weak, nonatomic) AssignedDetails *assignedObj;

@property (nonatomic,strong) NSString *descriptionType;

@property (nonatomic,strong) id commonObj;

@end
