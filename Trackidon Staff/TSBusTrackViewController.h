//
//  TSBusTrackViewController.h
//  Trackidon Staff
//
//  Created by ORGware on 20/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSHeaders.pch"
#import <GoogleMaps/GoogleMaps.h>
#import "TSSelectionViewController.h"

@interface TSBusTrackViewController : UIViewController <GMSMapViewDelegate,UITextFieldDelegate,SelectionCallBackDelegate>

@property (strong, nonatomic) IBOutlet GMSMapView *mapView;

@property (weak, nonatomic) IBOutlet UITextField *busSelectTxtFld;

@end


@interface LiveTrackModel : NSObject

@property (strong, nonatomic) NSString *busLatitude;

@property (strong, nonatomic) NSString *busLongitude;

@property (strong, nonatomic) NSString *busNo;

@property (strong, nonatomic) NSString *busRouteName;

@property (strong, nonatomic) NSString *busRouteTransID;

@end