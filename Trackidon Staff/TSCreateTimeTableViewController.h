//
//  TSCreateTimeTableViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 24/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Timetable,Board,Classes,Section;

@interface TSCreateTimeTableViewController : UIViewController<UITextFieldDelegate,SelectionCallBackDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *dayNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *periodNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *leisureButton;
@property (weak, nonatomic) IBOutlet UIImageView *tickImgVw;
@property (weak, nonatomic) IBOutlet UITextField *selectSubjectTxtFld;

@property (weak, nonatomic) IBOutlet UITextField *selectBoardTxtFld;

@property (weak, nonatomic) IBOutlet UITextField *selectClassTxtFld;

@property (weak, nonatomic) IBOutlet UITextField *selectSectionTxtFld;

@property (nonatomic,retain) Timetable *timetableObj;

@property (nonatomic,strong) NSString *selectedDay;

@property (nonatomic,strong) NSString *selectedPeriod;


- (IBAction)leisureButtonTapped:(id)sender;

@end
