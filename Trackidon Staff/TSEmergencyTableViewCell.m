//
//  TSEmergencyTableViewCell.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 20/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSEmergencyTableViewCell.h"

@implementation TSEmergencyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) loadContentForCell:(TSEmergencyTableViewCell *)cell withModelArray:(NSArray *)modelArray withRow:(NSInteger)row{
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:300];
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:301];
    
    EmergencyInfoModel *modelObj = [modelArray objectAtIndex:row];
    
    titleLabel.text = modelObj.emergencyTitle;
    dateLabel.text = [SINGLETON_INSTANCE stringFromDate:modelObj.examinedDate withFormate:DATE_FORMATE_DAY_MNTH_YR];
}

@end
