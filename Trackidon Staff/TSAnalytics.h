//
//  TSAnalytics.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 5/9/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Localytics.h"

@interface TSAnalytics : NSObject

+(void)logEvent:(NSString *)event parameter:(NSMutableDictionary *)parameter;

+(void)logScreen:(NSString *)screen parameter:(NSMutableDictionary *)parameter;

+ (void)sendScreen:(NSString *)screen withParams:(NSMutableDictionary *)parameter;

+(void)sendEvent:(NSString *)event withParams:(NSMutableDictionary *)parameter;

@end

#pragma Mark - analytics Screens

#define ANALYTICS_SCREEN_LOGINSCHOOLCODE @"[Login] SchoolCode Screen"
#define ANALYTICS_SCREEN_LOGIN @"[Login] Login Screen"
#define ANALYTICS_SCREEN_FORGOTPASSWORD @"[Login] ForgotPassword Screen"
#define ANALYTICS_SCREEN_SCHOOLSELECTION @"[SchoolSelection] SchoolSelection Screen"
#define ANALYTICS_SCREEN_COMMINUCATION @"[Communication] Communication Screen"
#define ANALYTICS_SCREEN_ATTENDENCEHOME @"[Attendance] Home Screen"
#define ANALYTICS_SCREEN_ATTENDANCELIST @"[Attendance] List Screen"
#define ANALYTICS_SCREEN_ATTENDANCECREATE @"[Attendance] CreateAttendance Screen"
#define ANALYTICS_SCREEN_ATTENDANCEVIEWHISTORY_BYDATE @"[Attendance] ViewHistoryByDate Screen"
#define ANALYTICS_SCREEN_ATTENDANCEVIEWHISTORY_BYSTUDENT @"[Attendance] ViewHistoryByStudent Screen"
#define ANALYTICS_SCREEN_ASSIGNMENTHOME @"[[Assignment] Home Screen]"
#define ANALYTICS_SCREEN_ASSIGNMENTLIST @"[Assignment] List Screen"
#define ANALYTICS_SCREEN_ASSIGNMENTDESCRIPTION @"[Assignment] Description Screen"
#define ANALYTICS_SCREEN_ASSIGNMENTCREATE @"[Assignment] CreateAssignment Screen"
#define ANALYTICS_SCREEN_ASSIGNMENTRECEIVER @"[Assignment] AssignmentReceiver Selection Screen"
#define ANALYTICS_SCREEN_EVENTHOME @"[Event] Home Screen"
#define ANALYTICS_SCREEN_EVENTLIST @"[Event] List Screen"
#define ANALYTICS_SCREEN_EVENTDESCRIPTION @"[Event] Description Screen"
#define ANALYTICS_SCREEN_EVENTCREATE @"[Event] CreateEvent Screen"
#define ANALYTICS_SCREEN_EVENTRECEIVER @"[Event] EventReceiver Selection Screen"
#define ANALYTICS_SCREEN_NOTIFICATIONHOME @"[Notification] Home Screen"
#define ANALYTICS_SCREEN_NOTIFICATIONLIST @"[Notification] List Screen"
#define ANALYTICS_SCREEN_NOTIFICATIONDECRIPTION @"[Notification] Description Screen"
#define ANALYTICS_SCREEN_NOTIFICATIONCREATE @"[Notification] CreateNotification Screen"
#define ANALYTICS_SCREEN_NETIFICATIONRECEIVER @"[Notification] NotificationReceiver Selection Screen"
#define ANALYTICS_SCREEN_PORTIONHOME @"[Portion] Home Screen"
#define ANALYTICS_SCREEN_PORTIONLIST @"[Portion] List Screen"
#define ANALYTICS_SCREEN_PORTIONDESCRIPTION @"[Portion] Description Screen"
#define ANALYTICS_SCREEN_PORTIONCREATE @"[Portion] CreatePortion Screen"
#define ANALYTICS_SCREEN_PORTIONRECEIVER @"[Portion] PortionReceiver Selection Screen"
#define ANALYTICS_SCREEN_HOLIDAYHOME @"[Holidays] Home Screen"
#define ANALYTICS_SCREEN_HOLIDAYLIST @"[Holidays] List Screen"
#define ANALYTICS_SCREEN_HOLIDAYDESCRIPTION @"[Holidays] Description Screen"
#define ANALYTICS_SCREEN_TIMETABLEHOME @"[TimeTable] Home Screen"
#define ANALYTICS_SCREEN_MYTIMETABLE @"[TimeTable] MyTimeTable Screen"
#define ANALYTICS_SCREEN_MYCLASSTIMETABLE @"[TimeTable] MyClass TimeTable Screen"
#define ANALYTICS_SCREEN_TIMETABLECREATE @"[TimeTable] TimeTableCreate"
#define ANALYTICS_SCREEN_PROFILEHOME @"[Profile] Home Screen"
#define ANALYTICS_SCREEN_SETTINGS @"[Settings] Settings Screen"
#define ANALYTICS_SCREEN_MORE @"[More] More Screen"
#define ANALYTICS_SCREEN_INBOXHOME @"[Inbox] Home Screen"
#define ANALYTICS_SCREEN_INBOXNOTIFICATION @"[Inbox] InboxNotification Screen"
#define ANALYTICS_SCREEN_INBOXEVENT @"[Inbox] InboxEvent Screen"
#define ANALYTICS_SCREEN_INBOXPORTION @"[Inbox] InboxPortion Screen"
#define ANALYTICS_SCREEN_INBOXNOTIFICATION_DESCRIPTION @"[Inbox] InboxNotification Description Screen"
#define ANALYTICS_SCREEN_INBOXEVENT_DESCRIPTION @"[Inbox] InboxEvent Description Screen"
#define ANALYTICS_SCREEN_INBOXPORTION_DESCRIPTION @"[Inbox] InboxPortion Description Screen"
#define ANALYTICS_SCREEN_APPROVALSHOME @"[Approvals] Home Screen"
#define ANALYTICS_SCREEN_APPROVALASSIGNMENT_LIST @"[Approvals] AssignmentApproval List Screen"
#define ANALYTICS_SCREEN_APPROVALEVENT_LIST @"[Approvals] EventApproval List Screen"
#define ANALYTICS_SCREEN_APPROVALNOTIFICATION_LIST @"[Approvals] NotificationApproval List Screen"
#define ANALYTICS_SCREEN_APPROVALPORTION_LIST @"[Approvals] PortionApproval List Screen"
#define ANALYTICS_SCREEN_APPROVALASSIGNMENT_DESCRIPTION @"[Approvals] AssignmentApproval Description Screen"
#define ANALYTICS_SCREEN_APPROVALEVENT_DESCRITION @"[Approvals] EventApproval Description Screen"
#define ANALYTICS_SCREEN_APPROVALNOTIFICATION_DESCRIPTION @"[Approvals] NotificationApproval Description Screen"
#define ANALYTICS_SCREEN_APPROVALPORTION_DESCRIPTION @"[Approvals] PortionApproval Description Screen"
#define ANALYTICS_SCREEN_FEEDBACKHOME @"[FeedBack] Home Screen"
#define ANALYTICS_SCREEN_FEEDBACKDETAIL @"[FeedBack] Detail Screen"
#define ANALYTICS_SCREEN_CHANGEPASSWORD @"[ChangePassword] ChangePassword Screen"
#define ANALYTICS_SCREEN_ABOUTUS @"[AboutUs] AboutUs Screen"
#define ANALYTICS_SCREEN_SUPPORT @"[Support] Support Screen"
#define ANALYTICS_SCREEN_USERMANUAL @"[UserManual] UserManual Screen"
#define ANALYTICS_SCREEN_ATTACHMENTVIEWER @"[Atachment] AttachmentViewer Screen"


#pragma mark - AnalyticsEvent

//GlobalEvent

#define ANALYTICS_GLOBAL_APPLAUNCHED @"[GlobalEvent] AppLaunched"
#define ANALYTICS_GLOBAL_APPEXCEPTION @"[GlobalEvent] AppException"
//PushNotification

#define ANALYTICS_PUSHNOTIFICATION_RECEIVED @"[PushNotification] ReceivedPushNotification"
#define ANALYTICS_PUSHNOTIFICATION_CLICKED_NOTIFICATION @"[PushNotification] ClickedPushNotification"

//Login

#define ANALYTICS_LOGIN_SUBMIT_SCHOOL_CODE @"[Login] SubmittedSchoolCode"
#define ANALYTICS_LOGIN_SUBMIT_LOGIN @"[Login] SubmittedLoginDetails"
#define ANALYTICS_LOGIN_CLIKED_FORGOT_PASSWORD @"[Login] ClickedForgotPassword"

#define ANALYTICS_LOGIN_SUBMIT_FORGOT_PASSWORD @"[Login] SubmittedForgotPasswordDetails"

//SchoolSelection

#define ANALYTICS_SCHOOL_SELECTION_SCHOOLSELECTED @"[SchoolSelection] schoolSelected"
#define ANALYTICS_SCHOOL_SELECTION_CLICKED_NEXT @"[SchoolSelection] ClickedNextButton"

//KidSelection

#define ANALYTICS_KID_SELECTION_CLICKED_KIDSELECTION @"[KidSelection] ClickedKidSelection"
#define ANALYTICS_KID_SELECTION_KIDSELECTED @"[KidSelection] KidSelected"

//MainMenu

#define ANALYTICS_MAINMENU_MAINMENU_CLICKED @"[MainMenu] MainMenuClicked"

//SubMenu

#define ANALYTICS_SUBMENU_SUBMENU_CLICKED @"[SubMenu] SubMenuClicked"

//Attendance

#define ANALYTICS_ATTENDANCE_ATTENDANCEOPENED @"[Attendance] AttendanceOpened"
#define ANALYTICS_ATTENDANCE_CLICKED_ATTENDANCETYPE @"[Attendance] ClickedAttendanceType"
#define ANALYTICS_ATTENDANCE_CLICKED_CLASSSELECTION @"[Attendance] ClickedClassSelection"
#define ANALYTICS_ATTENDANCE_CLICKED_UPDATE @"[Attendance] ClickedUpdate"
#define ANALYTICS_ATTENDANCE_CLICKED_HISTORYTYPE @"[Attendance] ClickedHistoryType"
#define ANALYTICS_ATTENDANCE_SEARCH_HISTORYBYDATE @"[Attendance] SearchHistoryByDate"
#define ANALYTICS_ATTENDANCE_SEARCH_HISTORYBYSTUDENT @"[Attendance] SearchHistoryByStudent"
#define ANALYTICS_ATTENDANCE_CLICKED_LISTOFDAYS @"[Attendance] ClickedListOfDays"
#define ANALYTICS_ATTENDANCE_CLICKED_FROMDATE @"[Attendance] ClickedFromDate"
#define ANALYTICS_ATTENDANCE_CLICKED_TODATE @"[Attendance] ClickedToDate"
#define ANALYTICS_ATTENDANCE_CLICKED_SUBMIT @"[Attendance] ClickedSubmit"
#define ANALYTICS_ATTENDANCE_CLICKED_CLEAR @"[Attendance] ClickedClear"

//Assignment

#define ANALYTICS_ASSIGNMENT_ASSIGNMENTOPENED @"[Assignment] AssignmentOpened"
#define ANALYTICS_ASSIGNMENT_CLICKED_CREATE_INHOME @"[Assignment] ClickedCreateAssignmentInHome"
#define ANALYTICS_ASSIGNMENT_CLICKED_ASSIGN_RECEIVER @"[Assignment] ClickedAssignReceiver"
#define ANALYTICS_ASSIGNMENT_CLICKED_VIEW_RECEIVER @"[Assignment] ClickedViewReceiver"
#define ANALYTICS_ASSIGNMENT_CLICKED_CREATE_ASSIGNMENT @"[Assignment] ClickedCreateAssignment"
#define ANALYTICS_ASSIGNMENT_CLICKED_VIEW_ASSIGNMENT @"[Assignment] ClickedViewAssignment"
#define ANALYTICS_ASSIGNMENT_CLICKED_ASSIGNMENT_IN_LIST @"[Assignment] ClickedAssignmentInList"
#define ANALYTICS_ASSIGNMENT_CLICKED_DESCRIPTION @"[Assignment] ClickedDescription"
#define ANALYTICS_ASSIGNMENT_CLICKED_ATTACHMENT @"[Assignment] ClickedAttachment"

//Event

#define ANALYTICS_EVENT_EVENTOPENED @"[Event] EventOpened"
#define ANALYTICS_EVENT_CLICKED_CREATE_INHOME @"[Event] ClickedCreateEventInHome"
#define ANALYTICS_EVENT_CLICKED_ASSIGN_RECEIVER @"[Event] ClickedAssignReceiver"
#define ANALYTICS_EVENT_CLICKED_VIEW_RECEIVER @"[Event] ClickedViewReceiver"
#define ANALYTICS_EVENT_CLICKED_CREATE_EVENT @"[Event] ClickedCreateEvent"
#define ANALYTICS_EVENT_CLICKED_VIEW_EVENT @"[Event] ClickedViewEvent"
#define ANALYTICS_EVENT_CLICKED_EVENT_IN_LIST @"[Event] ClickedEventInList"
#define ANALYTICS_EVENT_CLICKED_EVENT_IN_CALANDER @"[Event] ClickedEventInCalander"
#define ANALYTICS_EVENT_CLICKED_PREV_MNTH_BTN @"[Event] ClickedPreviousMnthCalendar"
#define ANALYTICS_EVENT_CLICKED_NXT_MNTH_BTN @"[Event] ClickedNextMnthCalendar"
#define ANALYTICS_EVENT_CLICKED_DESCRIPTION @"[Event] ClickedDescription"
#define ANALYTICS_EVENT_CLICKED_ATTACHMENT @"[Event] ClickedAttachment"


//Notification

#define ANALYTICS_NOTIFICATION_NOTIFICATIONOPENED @"[Notification] NotificationOpened"
#define ANALYTICS_NOTIFICATION_CLICKED_CREATE_INHOME @"[Notification] ClickedCreateNotificationInHome"
#define ANALYTICS_NOTIFICATION_CLICKED_ASSIGN_RECEIVER @"[Notification] ClickedAssignReceiver"
#define ANALYTICS_NOTIFICATION_CLICKED_VIEW_RECEIVER @"[Notification] ClickedViewReceiver"
#define ANALYTICS_NOTIFICATION_CLICKED_CREATE_NOTIFICATION @"[Notification] ClickedCreateNotification"
#define ANALYTICS_NOTIFICATION_CLICKED_VIEW_NOTIFICATION @"[Notification] ClickedViewNotification"
#define ANALYTICS_NOTIFICATION_CLICKED_NOTIFICATION_IN_LIST @"[Notification] ClickedNotificationInList"
#define ANALYTICS_NOTIFICATION_CLICKED_DESCRIPTION @"[Notification] ClickedDescription"
#define ANALYTICS_NOTIFICATION_CLICKED_ATTACHMENT @"[Notification] ClickedAttachment"

//Portion

#define ANALYTICS_PORTION_PORTIONOPENED @"[Portion] PortionOpened"
#define ANALYTICS_PORTION_CLICKED_CREATE_INHOME @"[Portion] ClickedCreatePortionInHome"
#define ANALYTICS_PORTION_CLICKED_ASSIGN_RECEIVER @"[Portion] ClickedAssignReceiver"
#define ANALYTICS_PORTION_CLICKED_VIEW_RECEIVER @"[Portion] ClickedViewReceiver"
#define ANALYTICS_PORTION_CLICKED_CREATE_PORTION @"[Portion] ClickedCreatePortion"
#define ANALYTICS_PORTION_CLICKED_VIEW_PORTION @"[Portion] ClickedViewPortion"
#define ANALYTICS_PORTION_CLICKED_PORTION_IN_LIST @"[Portion] ClickedPortionInList"
#define ANALYTICS_PORTION_CLICKED_DESCRIPTION @"[Portion] ClickedDescription"
#define ANALYTICS_PORTION_CLICKED_ATTACHMENT @"[Portion] ClickedAttachment"


//Holidays

#define ANALYTICS_HOLIDAY_HOLIDAYOPENED @"[Holidays] HolidaysOpened"
#define ANALYTICS_HOLIDAY_CLICKED_PUBLIC_HOLIDAYS @"[Holidays] ClickedPublicHolidays"
#define ANALYTICS_HOLIDAY_CLICKED_SCHOOL_HOLIDAYS @"[Holidays] ClickedSchoolHolidays"
#define ANALYTICS_HOLIDAY_CLICKED_HOLIDAY_IN_LIST @"[Holidays] ClickedHolidayInList"
#define ANALYTICS_HOLIDAY_CLICKED_HOLIDAY_IN_CALANDER @"[Holidays] ClickedHolidayInCalander"
#define ANALYTICS_HOLIDAY_CLICKED_PREV_MNTH_BTN @"[Holidays] ClickedPreviousMnthCalendar"
#define ANALYTICS_HOLIDAY_CLICKED_NXT_MNTH_BTN @"[Holidays] ClickedNextMnthCalendar"
#define ANALYTICS_HOLIDAY_CLICKED_DESCRIPTION @"[Holidays] ClickedDescription"
#define ANALYTICS_HOLIDAY_CLICKED_ATTACHMENT @"[Holidays] ClickedAttachment"

//TimeTable

#define ANALYTICS_TIMETABLE_TIMETABLEOPENED @"[TimeTable] TimeTableOpened"
#define ANALYTICS_TIMETABLE_CLICKED_DAY @"[TimeTable] ClickedDay"
#define ANALYTICS_TIMETABLE_CLICKED_TIMETABLETYPE @"[TimeTable] ClickedTimetableType"
#define ANALYTICS_TIMETABLE_CLICKED_CHANGEPERIOD @"[TimeTable] ClickedChangePeriod"
#define ANALYTICS_TIMETABLE_CLICKED_UPDATE @"[TimeTable] ClickedUpdate"

//ExamSchedule

#define ANALYTICS_EXAMSCHEDULE_EXAMSCHEDULEOPENED @"[ExamSchedule] OpenedExamSchedule"

//ExamResult

#define ANALYTICS_EXAMRESULT_EXAMRESULTOPENED @"[ExamResult] OpenedExamResult"

//Fee

#define ANALYTICS_FEE_FEESOPENED @"[Fee] OpenedFee"

//Hostel

#define ANALYTICS_HOSTEL_HOSTELOPENED @"[Hostel] OpenedHostel"

// Approvals

// Assignment approvals

#define ANALYTICS_ASSIGNMENTAPPROVALS_OPENED_APPROVALS @"[AssignmentApprovals] OpenedApprovals"
#define ANALYTICS_ASSIGNMENTAPPROVALS_CLICKED_APPROVALSINLIST @"[AssignmentApprovals] ClickedApprovalsInList"
#define ANALYTICS_ASSIGNMENTAPPROVALS_CLICKED_BULKAPPROVALS @"[AssignmentApprovals] ClickedBulkApprovals"
#define ANALYTICS_ASSIGNMENTAPPROVALS_CLICKED_DESCRIPTION @"[AssignmentApprovals] ClickedDescription"
#define ANALYTICS_ASSIGNMENTAPPROVALS_CLICKED_ATTACHMENT @"[AssignmentApprovals] ClickedAttachment"
#define ANALYTICS_ASSIGNMENTAPPROVALS_UPDATEAPPROVAL @"[AssignmentApprovals] UpdateApproval"

// Event Approvals

#define ANALYTICS_EVENTAPPROVALS_OPENED_APPROVALS @"[EventApprovals] OpenedApprovals"
#define ANALYTICS_EVENTAPPROVALS_CLICKED_APPROVALSINLIST @"[EventApprovals] ClickedApprovalsInList"
#define ANALYTICS_EVENTAPPROVALS_CLICKED_BULKAPPROVALS @"[EventApprovals] ClickedBulkApprovals"
#define ANALYTICS_EVENTAPPROVALS_CLICKED_DESCRIPTION @"[EventApprovals] ClickedDescription"
#define ANALYTICS_EVENTAPPROVALS_CLICKED_ATTACHMENT @"[EventApprovals] ClickedAttachment"
#define ANALYTICS_EVENTAPPROVALS_UPDATEAPPROVAL @"[EventApprovals] UpdateApproval"

// Notification Approvals

#define ANALYTICS_NOTIFAPPROVALS_OPENED_APPROVALS @"[NotificationApprovals] OpenedApprovals"
#define ANALYTICS_NOTIFAPPROVALS_CLICKED_APPROVALSINLIST @"[NotificationApprovals] ClickedApprovalsInList"
#define ANALYTICS_NOTIFAPPROVALS_CLICKED_BULKAPPROVALS @"[NotificationApprovals] ClickedBulkApprovals"
#define ANALYTICS_NOTIFAPPROVALS_CLICKED_DESCRIPTION @"[NotificationApprovals] ClickedDescription"
#define ANALYTICS_NOTIFAPPROVALS_CLICKED_ATTACHMENT @"[NotificationApprovals] ClickedAttachment"
#define ANALYTICS_NOTIFAPPROVALS_UPDATEAPPROVAL @"[NotificationApprovals] UpdateApproval"

// Portion

#define ANALYTICS_PORTIONAPPROVALS_OPENED_APPROVALS @"[PortionApprovals] OpenedApprovals"
#define ANALYTICS_PORTIONAPPROVALS_CLICKED_APPROVALSINLIST @"[PortionApprovals] ClickedApprovalsInList"
#define ANALYTICS_PORTIONAPPROVALS_CLICKED_BULKAPPROVALS @"[PortionApprovals] ClickedBulkApprovals"
#define ANALYTICS_PORTIONAPPROVALS_CLICKED_DESCRIPTION @"[PortionApprovals] ClickedDescription"
#define ANALYTICS_PORTIONAPPROVALS_CLICKED_ATTACHMENT @"[PortionApprovals] ClickedAttachment"
#define ANALYTICS_PORTIONAPPROVALS_UPDATEAPPROVAL @"[PortionApprovals] UpdateApproval"


//Profile
#define ANALYTICS_PROFILE_OPENED @"[Profile] OpenedProfile"
#define ANALYTICS_PROFILE_CHENGEDPROFILEIMAGE @"[Profile] ClickedChangeProfileImage"
#define ANALYTICS_PROFILE_CLICKEDIMAGEOPTION @"[Profile] ClickedProfileImageOption"
#define ANALYTICS_PROFILE_UPLOADIMAGE @"[Profile] UploadProfileImage"
#define ANALYTICS_PROFILE_CLICKEDMYWARDS @"[Profile] ClickedMyWards"
#define ANALYTICS_PROFILE_CLICKEDCHILDINLIST @"[Profile] ClickedChildInWardList"

//Settings
#define ANALYTICS_SETTINGS_OPENED @"[Settings] Opened Settings"
#define ANALYTICS_SETTINGS_CHANGEDEMAIL @"[Settings] ChangedEmailSettings"
#define ANALYTICS_SETTINGS_CHANGEDNOTIFICATION @"[Settings] ChangedPushNotificationSettings"

//Feedback
#define ANALYTICS_FEEDBACK_OPENED @"[FeedBack] OpenedFeedBack"
#define ANALYTICS_FEEDBACK_CLICKEDFEEDINLIST @"[FeedBack] ClickedFeedInList"
#define ANALYTICS_FEEDBACK_CLICKEDCREATE @"[FeedBack] ClickedCreateFeedBack"
#define ANALYTICS_FEEDBACK_CLICKEDSUBMIT @"[FeedBack] ClickedSubmitFeedBack"

//Change Password
#define ANALYTICS_CHANGEPASSWORD_OPENED @"[ChangePassword] OpenedChangePassword"
#define ANALYTICS_CHANGEPASSWORD_CLICKEDUPDATE @"[ChangePassword] ClickedUpdatePassword"

//AboutUs
#define ANALYTICS_ABOUTUS_OPENED @"[AboutUs] OpenedAboutUs"
#define ANALYTICS_ABOUTUS_CLICKEDPHONE @"[AboutUs] ClickedPhone"
#define ANALYTICS_ABOUTUS_CLICKEDMAIL @"[AboutUs] ClickedMail"

//Support
#define ANALYTICS_SUPPORT_OPENED @"[Support] OpenedSupport"
#define ANALYTICS_SUPPORT_CLICKEDPHONE @"[Support] ClickedPhone"
#define ANALYTICS_SUPPORT_CLICKEDMAIL @"[Support] ClickedMail"
#define ANALYTICS_SUPPORT_CLICKEDWEB @"[Support] ClickedWeb"

//UserManual
#define ANALYTICS_USERMANUAL_OPENED @"[UserManual] OpenedUserManual"

//ChangeSchool
#define ANALYTICS_CHANGESCHOOL_OPENED @"[ChangeSchool] OpenedChangeSchool"

//LogOut
#define ANALYTICS_LOGOUT_CLICKED @"[LogOut] LogoutClicked"

//WebService
#define ANALYTICS_WEBSERVICE_SERVICECALL @"[WebService] ServiceCall"

//DownloadAttachment
#define ANALYTICS_DOWNLOADATTACHMENT_REQUEST @"[DownloadAttachment] DownloadRequest"

//Inbox
#define ANALYTICS_INBOXEVENT_OPENED @"[InboxEvents] EventInbox Opened"
#define ANALYTICS_INBOXEVENT_CLICKEDINLIST @"[InboxEvents] ClickedEventInboxInList"
#define ANALYTICS_INBOXEVENT_CLICKEDDESCRIPTIOIN @"[InboxEvents] ClickedDescription"
#define ANALYTICS_INBOXEVENT_CLICKEDATTACHMENT @"[InboxEvents] ClickedAttachment"
#define ANALYTICS_INBOXEVENT_CLICKEDVIEWRECEIVER @"[InboxEvents] ClickedViewReceiver"
#define ANALYTICS_INBOXNOTIFICATION_OPENED @"[InboxNotification] NotificationInbox Opened"
#define ANALYTICS_INBOXNOTIFICATION_CLICKEDINLIST @"[InboxNotification] ClickedNotificationInboxInList"
#define ANALYTICS_INBOXNOTIFICATION_CLICKEDDESCRIPTION @"[InboxNotification] ClickedDescription"
#define ANALYTICS_INBOXNOTIFICATION_CLICKEDATTACHMENT @"[InboxNotification] ClickedAttachment"
#define ANALYTICS_INBOXNOTIFICATION_CLICKEDVIEWRECEIVER @"[InboxNotification] ClickedViewReceiver"
#define ANALYTICS_INBOXPORTION_OPENED @"[InboxPortions] PortionInbox Opened"
#define ANALYTICS_INBOXPORTION_CLICKEDINLIST @"[InboxPortions] ClickedPortionInboxInList"
#define ANALYTICS_INBOXPORTION_CLICKEDDESCRIPTION @"[InboxPortions] ClickedDescription"
#define ANALYTICS_INBOXPORTION_CLICKEDATTECTMENT @"[InboxPortions] ClickedAttachment"
#define ANALYTICS_INBOXPORTION_CLICKEDVIEWRECEIVER @"[InboxPortions] ClickedViewReceiver"

#pragma mark - Params
// Params

#define ANALYTICS_PARAM_NETWORK_STATUS @"networkStatus"
#define ANALYTICS_PARAM_SCHOOL_NAME @"schoolName"
#define ANALYTICS_PARAM_CLASS_NAME @"className"
#define ANALYTICS_PARAM_ISLOGGEDIN @"loggedInStatus"
#define ANALYTICS_PARAM_ISNEEDSSCHOOLCHANGE @"isNeedsSchoolChange"

#define ANALYTICS_PARAM_ISHASATTACHMENT @"hasAttachment"
#define ANALYTICS_PARAM_SUBJECT_NAME @"subjectName"
#define ANALYTICS_PARAM_SUBJECT_TYPE @"subjectType"
#define ANALYTICS_PARAM_CATEGORY_TYPE @"categoryType"
#define ANALYTICS_PARAM_RECEIPIENT_TYPE @"receipientType"
#define ANALYTICS_PARAM_RECEIPIENT_PRIORITY @"priority"

#define ANALYTICS_PARAM_ATTENDANCE_STATUS @"attendanceStatus"
#define ANALYTICS_PARAM_NOTIFICATIONTYPE @"notificationType"
#define ANALYTICS_PARAM_RESPONSE @"response"
#define ANALYTICS_PARAM_SCHOOLCODE @"schoolCode"
#define ANALYTICS_PARAM_RESPONSEMESSAGE @"responseMessage"
#define ANALYTICS_PARAM_SCHOOLCOUNT @"schoolCount"
#define ANALYTICS_PARAM_MENUNAME @"menuName"
#define ANALYTICS_PARAM_SUBMENUNAME @"subMenuName"
#define ANALYTICS_PARAM_MENUID @"menuIndex"
#define ANALYTICS_PARAM_SUBMENUID @"subMenuID"
#define ANALYTICS_PARAM_VIEWTYPE @"viewType"
#define ANALYTICS_PARAM_LISTTYPE @"listType"
#define ANALYTICS_PARAM_ATTACHMENTSTATUS @"attachmentStatus"
#define ANALYTICS_PARAM_HOLIDAYTYPE @"holidayType"
#define ANALYTICS_PARAM_DAY @"day"
#define ANALYTICS_PARAM_SOURCE @"source"
#define ANALYTICS_PARAM_UPDATESTATUS @"updateStatus"
#define ANALYTICS_PARAM_TO @"to"
#define ANALYTICS_PARAM_TYPE @"type"
#define ANALYTICS_PARAM_STATUS @"status"
#define ANALYTICS_PARAM_URL @"url"
#define ANALYTICS_PARAM_ERRORCODE @"errorCode"
#define ANALYTICS_PARAM_ERROR @"error"

#define ANALYTICS_PARAM_ATTENDANCETYPE @"attendanceType"
#define ANALYTICS_PARAM_BOARDNAME @"boardName"
#define ANALYTICS_PARAM_CLASSNAME @"className"
#define ANALYTICS_PARAM_SECNAME @"sectionName"
#define ANALYTICS_PARAM_HISTORYTYPE @"historyType"
#define ANALYTICS_PARAM_APPROVALTYPE @"approvalType"


//Values

#define ANALYTICS_VALUE_APPROVED @"Approved"
#define ANALYTICS_VALUE_PENDING @"Pending"
#define ANALYTICS_VALUE_DECLINED @"Declined"
#define ANALYTICS_VALUE_YES @"YES"
#define ANALYTICS_VALUE_NO @"NO"
#define ANALYTICS_VALUE_HIGH @"High"
#define ANALYTICS_VALUE_MEDIUM @"Medium"
#define ANALYTICS_VALUE_LOW @"Low"




#define ANALYTICS_VALUE_ONLINE @"Online"
#define ANALYTICS_VALUE_OFFLINE @"Offline"
#define ANALYTICS_VALUE_SERVICEFAILURE_RESPONSEMSG @"Service Call Failure"
#define ANALYTICS_VALUE_NOTLOGGEDIN @"NotLoggedIn"
#define ANALYTICS_VALUE_LOGGEDIN @"LoggedIn"
#define ANALYTICS_VALUE_FAILURE @"Failed"
#define ANALYTICS_VALUE_SUCCESS @"Success"
#define ANALYTICS_VALUE_VIEWALL @"ViewAll"
#define ANALYTICS_VALUE_DUESTODAY @"DuesToday"
#define ANALYTICS_VALUE_CREATEDTODAY @"CreatedToday"
#define ANALYTICS_VALUE_OPENEDFROMLOCAL @"OpenedFromLocal"
#define ANALYTICS_VALUE_DOWNLOADING @"Downloading"
#define ANALYTICS_VALUE_PUBLICHOLIDAY @"PublicHoliday"
#define ANALYTICS_VALUE_SCHOOLHOLIDAY @"SchoolHoliday"
#define ANALYTICS_VALUE_CAMERA @"Camera"
#define ANALYTICS_VALUE_GALLERY @"Gallery"
#define ANALYTICS_VALUE_CANCEL @"Cancel"
#define ANALYTICS_VALUE_ON @"On"
#define ANALYTICS_VALUE_OFF @"Off"
#define ANALYTICS_VALUE_CHOSENTYPE @"ChoosenType"
#define ANALYTICS_VALUE_URLNOTFOUND @"URLNotFound"
#define ANALYTICS_VALUE_ATTENDANCE_PRESENT @"Present"
#define ANALYTICS_VALUE_ATTENDANCE_ABSENT @"Absent"
#define ANALYTICS_VALUE_ATTENDANCE_TARDY @"Tardy"

#define ANALYTICS_VALUE_MYTIMETABLE @"MyTimetable"
#define ANALYTICS_VALUE_MYCLASSTIMETABLE @"MyClassTimetable"
#define ANALYTICS_VALUE_TAKEATTENDANCE @"TakeAttendance"
#define ANALYTICS_VALUE_HISTORY @"History"
#define ANALYTICS_VALUE_DATE @"Date"
#define ANALYTICS_VALUE_STUDENT @"Student"
#define ANALYTICS_VALUE_APPROVE @"Approve"
#define ANALYTICS_VALUE_DECLINE @"Decline"


