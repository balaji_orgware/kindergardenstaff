//
//  TSNotificationHomeViewController.h
//  Trackidon Staff
//
//  Created by Elango on 22/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSNotificationHomeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblVw;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionVw;

@end
