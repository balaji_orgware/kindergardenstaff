//
//  GroupList+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 08/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "GroupList+CoreDataProperties.h"

@implementation GroupList (CoreDataProperties)

@dynamic boardTransId;
@dynamic boardName;
@dynamic schoolTransId;
@dynamic schoolName;
@dynamic groupTransId;
@dynamic groupName;
@dynamic school;

@end
