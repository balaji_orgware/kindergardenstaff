//
//  StaffList.m
//  Trackidon Staff
//
//  Created by Elango on 06/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "StaffList.h"
#import "School.h"

@implementation StaffList

+ (instancetype)instance{
    
    static StaffList *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (&onceToken, ^{
        
        kSharedInstance = [[StaffList alloc]init];
        
    });
    
    return kSharedInstance;
    
}

- (void)saveStaffListWithResponseDict:(NSDictionary *)dict schoolTransId:(NSString *)schoolTransId{
    
    NSLog(@"%@",dict);
    
    NSManagedObjectContext *context = [[TSAppDelegate instance] managedObjectContext];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"schoolTransId contains[cd] %@",schoolTransId];
    
    [SINGLETON_INSTANCE deleteSelectedObjectsInEntityWithEntityName:@"StaffList" withPredicate:predicate withContext:[APPDELEGATE_INSTANCE managedObjectContext]];
    
    NSArray *schoolArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"School" withPredicate:predicate];
    
    
    School *school;
    
    if (schoolArr.count >0)
        school = [schoolArr objectAtIndex:0];

    
    @try {
        
        for (NSDictionary *tmpStaffDict in dict[@"FetchAccountsbyTypeResult"][@"AccountsClass"]) {
            
            NSDictionary *staffDict = [tmpStaffDict dictionaryByReplacingNullsWithBlanks];
            
            //StaffList *staff;
            
            //if (staff == nil) {
                
                StaffList *staff = [NSEntityDescription insertNewObjectForEntityForName:@"StaffList" inManagedObjectContext:context];
            //}
            
            staff.staffName = staffDict[@"Name"];
            staff.staffTransId = staffDict[@"TransID"];
            //staff.boardName = staffDict[@""];
            staff.boardTransId = staffDict[@"BoardTransID"];
            staff.schoolName = staffDict[@"SchoolName"];
            staff.schoolTransId = staffDict[@"SchoolTransID"];
            staff.school = school;
            
        }
        
        
        
        NSError *error;
        
        if (![context save:&error]) {
            
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    
}

@end
