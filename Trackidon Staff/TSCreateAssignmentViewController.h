//
//  TSCreateAssignmentViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 07/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSDatePickerViewController.h"

@interface TSCreateAssignmentViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,SelectionCallBackDelegate,datePickerDelegate,AttachmentPikerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *assignedToButton;

@property (weak, nonatomic) IBOutlet UITextField *subjectNameTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *assignmentTitleTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *subjectTypeTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *dueDateTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *addAttachmentTxtFld;

@property (weak, nonatomic) IBOutlet UITextView *descriptionTxtVw;

// Button Actions
- (IBAction)assignedToButtonTapped:(id)sender;

@end
