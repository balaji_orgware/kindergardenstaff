//
//  TSDatePickerViewController.m
//  Trackidon Staff
//
//  Created by Elango on 24/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSDatePickerViewController.h"

@interface TSDatePickerViewController ()

@end

@implementation TSDatePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.frame = CGRectMake(0, 0, DEVICE_SIZE.width, DEVICE_SIZE.height);
    
    _datePickerVw.frame = CGRectMake(0, DEVICE_SIZE.height, _datePickerVw.frame.size.width, _datePickerVw.frame.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showInViewController:(UIViewController *)viewController calendarType:(NSInteger)calendarType withMinDate:(NSDate *)minDate withMaxDate:(NSDate *)maxDate{
    
    [viewController addChildViewController:self];
    
    [viewController.view addSubview:self.view];
    
    [self didMoveToParentViewController:viewController];
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f]];
    
    if (calendarType == CALENDAR_TYPE_DATE) {
        _datePicker.datePickerMode = UIDatePickerModeDate;
    }else {
        _datePicker.datePickerMode = UIDatePickerModeTime;
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        
        [self.view setBackgroundColor:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.5f]];
        
        _datePickerVw.frame = CGRectMake(0, viewController.view.frame.size.height - _datePickerVw.frame.size.height, viewController.view.frame.size.width, viewController.view.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        if (minDate)
            [_datePicker setMinimumDate:minDate];
        
        if (maxDate)
            [_datePicker setMaximumDate:maxDate];
        
    }];
    
}

- (IBAction)onDoneClk:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectedDate:)]) {
        
        [self.delegate selectedDate:_datePicker.date];
        
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        
        _datePickerVw.frame = CGRectMake(0, DEVICE_SIZE.height,_datePickerVw.frame.size.width, _datePickerVw.frame.size.height);
        
        [self.view setBackgroundColor:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f]];
        
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        
        [self removeFromParentViewController];
        
    }];
    
}

- (IBAction)onCancelClk:(id)sender {
    
    [UIView animateWithDuration:0.3f animations:^{
        
        _datePickerVw.frame = CGRectMake(0, DEVICE_SIZE.height,_datePickerVw.frame.size.width, _datePickerVw.frame.size.height);
        
        [self.view setBackgroundColor:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f]];
        
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        
        [self removeFromParentViewController];
        
    }];
    
}

@end
