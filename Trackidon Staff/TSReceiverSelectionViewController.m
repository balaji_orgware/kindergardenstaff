//
//  TSCommonCreateViewController.m
//  Trackidon Staff
//
//  Created by Elango on 01/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSReceiverSelectionViewController.h"

@interface TSReceiverSelectionViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation BoardModel

@end

@implementation ReceiverFilterListModel

@end

@implementation ReceiverReceipientListModel

@end

@implementation ReceiverDetailsModel

@end

@implementation ClassesModel

@end

@implementation SectionModel

@end

@implementation StaffModel

@end

@implementation GroupModel

@end

@implementation TSReceiverSelectionViewController
{
    
    RECEIVER_LIST_TYPE listType;
    
    UILabel *selectLabel;
    
    NSMutableDictionary *selectionDetailsDict;
    
    BOOL isBoardSelctionDone;
    
    BOOL isFilterSelctionDone;
    
    BOOL isFilterOptionSelectionDone;
    
    BOOL isReceipientSelectionDone;
    
    BOOL isNeedToCheckGroup;
    
    BOOL isGroupSelectionDone;
    
    BOOL isClassSelectionDone;
    
    BOOL isSectionSelectionDone;
    
    BOOL isStudentSelectionDone;
    
    BOOL isStaffSelectionDone;
    
    ReceiverFilterListModel *selectedFilterObj;
    
    ReceiverReceipientListModel *selectedReceipientObj;
    
    //ReceiverOptions
    
    NSMutableArray *receiverOptionsArr;
    
    //Board Selection
    
    NSMutableArray *boardListArr;
    BOOL isBoardSelectAll;
    
    //Filter Selection
    NSMutableArray *filterListArr;
    NSArray *filterOptinsListArr;
    BOOL isFilterOptionSelectAll;
    
    //StaffSelection
    NSMutableArray *staffListArr;
    BOOL isStaffSelectAll;

    
    //Receipient Selection
    NSMutableArray *receipientListArr;
    
    //--GroupSelection
    NSMutableArray *groupOptinsListArr;
    BOOL isGroupSelectAll;
    
    //--ClassSelection
    NSMutableArray *classOptinsListArr;
    BOOL isClassSelectAll;
    
    //--SectionSelection
    NSMutableArray *sectionOptionsListArr;
    BOOL isSectionSelectAll;
    
    //--StudentSelection
    NSMutableArray *studentListArr;
    BOOL isStudentSelectAll;
    

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    selectLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, DeviceWidth - 70, 40)];
    selectLabel.text = @"Select";
    selectLabel.textColor = APPTHEME_COLOR;
    selectLabel.font = [UIFont fontWithName:APP_FONT size:15];
    
    _listVw.hidden = YES;
    [_listTblVw setTableFooterView:[UIView new]];
    _listTblVw.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    boardListArr = [[NSMutableArray alloc] init];
    filterListArr = [[NSMutableArray alloc] init];
    receiverOptionsArr = [[NSMutableArray alloc]init];
    receipientListArr = [[NSMutableArray alloc]init];
    groupOptinsListArr = [[NSMutableArray alloc]init];
    classOptinsListArr = [[NSMutableArray alloc]init];
    sectionOptionsListArr = [[NSMutableArray alloc]init];
    studentListArr = [[NSMutableArray alloc] init];
    staffListArr = [[NSMutableArray alloc] init];
    selectionDetailsDict = [NSMutableDictionary new];

    
    [_classGroupSelectionVw setHidden:YES];
    [_sectionSelectionVw setHidden:YES];
    [_studentSelectionVw setHidden:YES];

    [self createFilterOptions];
    
    CGRect staffFrame = [_staffSelectionVw convertRect:_staffSelectionVw.frame toView:self.view];
    
    [_scrollVw setContentSize:CGSizeMake(DeviceWidth, staffFrame.origin.y + staffFrame.size.height)];
    
    if (_communicationType == OBJECT_TYPE_NOTIFICATION || _communicationType == OBJECT_TYPE_EVENT || _communicationType == OBJECT_TYPE_PORTIONS) {
        [_staffSelectionVw setHidden:NO];
    }
    
    [TSSingleton layerDrawForView:_boardSelctionVw position:LAYER_BOTTOM color:SAPRATOR_COLOR];
    [TSSingleton layerDrawForView:_filterSelectionVw position:LAYER_BOTTOM color:SAPRATOR_COLOR];
    [TSSingleton layerDrawForView:_filterOptionSelectionVw position:LAYER_BOTTOM color:SAPRATOR_COLOR];
    [TSSingleton layerDrawForView:_receipientSelectionVw position:LAYER_BOTTOM color:SAPRATOR_COLOR];
    [TSSingleton layerDrawForView:_classGroupSelectionVw position:LAYER_BOTTOM color:SAPRATOR_COLOR];
    [TSSingleton layerDrawForView:_sectionSelectionVw position:LAYER_BOTTOM color:SAPRATOR_COLOR];
    [TSSingleton layerDrawForView:_studentSelectionVw position:LAYER_BOTTOM color:SAPRATOR_COLOR];
    [TSSingleton layerDrawForView:_staffSelectionVw position:LAYER_BOTTOM color:SAPRATOR_COLOR];
    
    //UITapGestureRecognizer *backgroundTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideListTblVw)];
    
    //[_listVw addGestureRecognizer:backgroundTap];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self hideListTblVw];
}

- (void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    switch (_communicationType) {
            
        case OBJECT_TYPE_ASSIGNMENT:
            
            [TSAnalytics logScreen:ANALYTICS_SCREEN_ASSIGNMENTRECEIVER parameter:[NSMutableDictionary new]];
            
            self.navigationItem.title = @"Create Assignment";
            break;
            
        case OBJECT_TYPE_EVENT:
            
            [TSAnalytics logScreen:ANALYTICS_SCREEN_EVENTRECEIVER parameter:[NSMutableDictionary new]];
            
            self.navigationItem.title = @"Create Event";
            break;
            
        case OBJECT_TYPE_NOTIFICATION:
            
             [TSAnalytics logScreen:ANALYTICS_SCREEN_NETIFICATIONRECEIVER parameter:[NSMutableDictionary new]];
            
            self.navigationItem.title = @"Create Notification";
            break;
            
        case OBJECT_TYPE_PORTIONS:
            
            [TSAnalytics logScreen:ANALYTICS_SCREEN_PORTIONRECEIVER parameter:[NSMutableDictionary new]];
            
            self.navigationItem.title = @"Create Lesson Plan";
            break;
            
        default:
            self.navigationItem.title = @"Create";
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - Methods

- (void)createFilterOptions{
    
    isBoardSelctionDone = NO;

    isGroupSelectionDone = NO;
    
    //BoardSelection
    
    isBoardSelectAll = NO;
    
    NSPredicate *boardPredicate = [NSPredicate predicateWithFormat:@"schoolTransId contains[cd] %@ AND isViewableByStaff == 1",SINGLETON_INSTANCE.selectedSchoolTransId];
    
    NSArray *boardArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Board" withPredicate:boardPredicate];
    
    [boardListArr removeAllObjects];
    
    for (Board *board in boardArr) {
        
        BoardModel *boardObj =[[BoardModel alloc]init];
        
        boardObj.boardName = board.boardName;
        boardObj.boardTransId = board.boardTransId;
        boardObj.schoolTransId = board.schoolTransId;
        boardObj.isSelected = NO;
                
        [boardListArr addObject:boardObj];
        
        boardObj = nil;
        
    }
    
    if (boardListArr.count == 1) {
        
        BoardModel *board = [boardListArr objectAtIndex:0];
        board.isSelected = YES;
        
        
        isBoardSelctionDone = YES;
        _boardSelectionLbl.text = board.boardName;
    
        self.scrollVw.frame = CGRectMake(self.scrollVw.frame.origin.x, self.scrollVw.frame.origin.y - 60, self.scrollVw.frame.size.width, self.scrollVw.frame.size.height);
        
        [self getReceiversForSelectedBoardTransId];
        
        [self getFilterAndReceipientDetatils];
        
        [self getStaffListForSelectedBoard];
    }
    
    [boardListArr sortedArrayUsingComparator:^NSComparisonResult(BoardModel *obj1,BoardModel *obj2) {
        
        return [ obj1.boardName compare:obj2.boardName];
    }];
    
}

- (void)getFilterAndReceipientDetatils{
    
    isFilterSelctionDone = NO;

    isFilterOptionSelectionDone = NO;
    
    isReceipientSelectionDone = NO;

    //Filter Selection
    
    _filterSelectionLbl.text =@"No Category Selected";
    isFilterSelctionDone = NO;
    [_filterSelectionLbl setTextColor:UNSELECTED_COLOR];
    
    isFilterOptionSelectAll = NO;
    
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"menuId contains[cd] %@",[NSString stringWithFormat:@"%@",_menuId]];
    
    NSArray *filterArr =[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"ReceiverFilterList" withPredicate:filterPredicate];
    
    [filterListArr removeAllObjects];
    
    for (ReceiverFilterList *filter in filterArr) {
        
        ReceiverFilterListModel *filterObj = [[ReceiverFilterListModel alloc] init];
        
        filterObj.filterDescription = filter.filterDescription;
        filterObj.filterTypeId = filter.filterTypeId;
        filterObj.menuId = filter.menuId;
        filterObj.isSelected = NO;
        
        [filterListArr addObject:filterObj];
        
        filterObj = nil;
        
    }
    
    [filterListArr sortUsingComparator:^NSComparisonResult(ReceiverFilterListModel *obj1, ReceiverFilterListModel *obj2) {
        
        return [obj1.filterTypeId compare:obj2.filterTypeId];
    }];
    
    //Receipient Selection
    isGroupSelectAll = NO;
    isNeedToCheckGroup =NO;
    
    NSPredicate *receipientPredicate = [NSPredicate predicateWithFormat:@"menuId contains[cd] %@",[NSString stringWithFormat:@"%@",_menuId]];
    
    NSArray *receipientArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"ReceiverReceipientList" withPredicate:receipientPredicate];
    
    [receipientListArr removeAllObjects];
    
    for (ReceiverReceipientList *receipient in receipientArr) {
        
        ReceiverReceipientListModel *receipientObj = [[ReceiverReceipientListModel alloc] init];
        
        receipientObj.receipientDescription = receipient.receipientDescription;
        receipientObj.receipientTypeId = receipient.recepientTypeId;
        receipientObj.menuId = receipient.menuId;
        receipientObj.isSelected = NO;
        
        [receipientListArr addObject:receipientObj];
        
        receipientObj = nil;
        
    }
    
    [receipientListArr sortUsingComparator:^NSComparisonResult(ReceiverReceipientListModel *obj1, ReceiverReceipientListModel *obj2) {
        
        return [obj1.receipientTypeId compare:obj2.receipientTypeId];
    }];


}

- (void)getReceiversForSelectedBoardTransId{
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *selectedBoardArr = [boardListArr filteredArrayUsingPredicate:pred];
    
    NSMutableArray *tmpReceiverOptionsArr = [[NSMutableArray alloc]init];
    
    //ReceiverOptions
    
    [receiverOptionsArr removeAllObjects];
    
    for (BoardModel *board in selectedBoardArr) {
        
        NSPredicate *receiverOptionsPreidicate = [NSPredicate predicateWithFormat:@"boardTransId contains[cd] %@",board.boardTransId];
        
        [tmpReceiverOptionsArr addObjectsFromArray:[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"ReceiverDetails" withPredicate:receiverOptionsPreidicate]];
    }
    
    for (ReceiverDetails *receiverOption in tmpReceiverOptionsArr) {
        
        ReceiverDetailsModel *receiverOptionObj = [[ReceiverDetailsModel alloc] init];
        
        receiverOptionObj.boardName = receiverOption.boardName;
        receiverOptionObj.boardTransId = receiverOption.boardTransId;
        receiverOptionObj.receiverDescription = receiverOption.receiverDescription;
        receiverOptionObj.receiverTransId = receiverOption.receiverTransId;
        receiverOptionObj.receiverTypeId = receiverOption.receiverTypeId;
        receiverOptionObj.isSelected = NO;
        
        [receiverOptionsArr addObject:receiverOptionObj];
        
        receiverOptionObj = nil;
        
    }
    
    tmpReceiverOptionsArr = nil;
    
     /*_receipientVw.frame = CGRectMake(_receipientVw.frame.origin.x, _filterSelectionVw.frame.origin.y + _filterSelectionVw.frame.size.height, _receipientVw.frame.size.width, _receipientVw.frame.size.height);
    
    _filterSelectionLbl.text =@"No Category Selected";
    [_filterSelectionLbl setTextColor:UNSELECTED_COLOR];*/
    
}

- (void)getStaffListForSelectedBoard{
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *selectedBoardArr = [boardListArr filteredArrayUsingPredicate:pred];
    
    //StaffList
    
    NSMutableArray *tmpStaffArr = [[NSMutableArray alloc]init];
    
    [staffListArr removeAllObjects];
    
    for (Board *board in selectedBoardArr) {
        
        NSPredicate *staffListPreidicate = [NSPredicate predicateWithFormat:@"boardTransId contains[cd] %@",board.boardTransId];
        
        [tmpStaffArr addObjectsFromArray:[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"StaffList" withPredicate:staffListPreidicate]];
    }
    
    for (StaffList *staff in tmpStaffArr) {
        
        StaffModel *staffObj = [[StaffModel alloc] init];
        
        staffObj.staffName = staff.staffName;
        staffObj.staffTransId = staff.staffTransId;
        //staffObj.boardName = staff.staffName;
        staffObj.boardTransId = staff.boardTransId;
        staffObj.schoolName = staff.schoolName;
        staffObj.schoolTransId = staff.schoolTransId;
        
        [staffListArr addObject:staffObj];
        
        staffObj = nil;
        
    }
    
    tmpStaffArr = nil;
    
    [staffListArr sortUsingComparator:^NSComparisonResult(StaffModel *obj1, StaffModel *obj2) {
        
        return [obj1.staffName compare:obj2.staffName];
        
    }];
    
    isStaffSelectAll = NO;
    isStaffSelectionDone = NO;
    
    _staffSelectionLbl.text = @"No Staff Selected";
    [_staffSelectionLbl setTextColor:UNSELECTED_COLOR];
    
}

- (void)getClassForSelectedBoard{
    
    isClassSelectAll = NO;
    isClassSelectionDone = NO;
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *selectedBoardArr = [boardListArr filteredArrayUsingPredicate:pred];
    
    NSMutableArray *tmpClassOptionsArr = [[NSMutableArray alloc]init];
    
    //ReceiverOptions
    
    [classOptinsListArr removeAllObjects];
    
    for (Board *board in selectedBoardArr) {
        
        NSPredicate *classOptionsPreidicate = [NSPredicate predicateWithFormat:@"boardTransId contains[cd] %@",board.boardTransId];
        
        [tmpClassOptionsArr addObjectsFromArray:[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Classes" withPredicate:classOptionsPreidicate]];
    }
    
    for (Classes *class in tmpClassOptionsArr) {
        
        ClassesModel *classObj = [[ClassesModel alloc]init];
        
        classObj.classTransId = class.classTransId;
        classObj.classStandardName = class.classStandardName;
        classObj.boardTransId = class.boardTransId;
        classObj.boardName = class.boardName;
        classObj.schoolTransId = class.schoolTransId;
        classObj.isSelected = NO;
        
        [classOptinsListArr addObject:classObj];
        
        classObj = nil;
        
    }
    
    tmpClassOptionsArr = nil;
    
    [classOptinsListArr sortUsingComparator:^NSComparisonResult(ClassesModel *obj1,ClassesModel *obj2) {
        
        return [obj1.classStandardName compare:obj2.classStandardName];
        
    }];
    
    [classOptinsListArr sortUsingComparator:^NSComparisonResult(ClassesModel *obj1,ClassesModel *obj2) {
        
        return [obj1.boardName compare:obj2.boardName];
        
    }];
    
}

- (void)getGroupForSelectedBoard{
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *selectedBoardArr = [boardListArr filteredArrayUsingPredicate:pred];
    
    //GroupList
    
    NSMutableArray *tmpGroupArr = [[NSMutableArray alloc]init];
    
    [groupOptinsListArr removeAllObjects];
    
    for (Board *board in selectedBoardArr) {
        
        NSPredicate *groupListPreidicate = [NSPredicate predicateWithFormat:@"boardTransId contains[cd] %@",board.boardTransId];
        
        [tmpGroupArr addObjectsFromArray:[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"GroupList" withPredicate:groupListPreidicate]];
    }
    
    for (GroupList *group in tmpGroupArr) {
        
        GroupModel *groupObj = [[GroupModel alloc] init];
        
        groupObj.groupName = group.groupName;
        groupObj.groupTransId = group.groupTransId;
        groupObj.boardName = [self getBoardNameForBoardTransId:group.boardTransId];
        groupObj.boardTransId = group.groupTransId;
        groupObj.isSelected = NO;

        [groupOptinsListArr addObject:groupObj];
        
        groupObj = nil;
        
    }
    
    tmpGroupArr = nil;
    
    [groupOptinsListArr sortUsingComparator:^NSComparisonResult(GroupModel *obj1, GroupModel *obj2) {
        
        return [obj1.groupName compare:obj2.groupName];
        
    }];
    
    isGroupSelectAll = NO;
    isGroupSelectionDone = NO;
    
    _classGroupSelctionLbl.text = @"No Group Selected";
    
}

- (NSString *)getBoardNameForBoardTransId:(NSString *)boardTransId{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"boardTransId contains[cd] %@",boardTransId];
    
    NSArray *boardArr = [boardListArr filteredArrayUsingPredicate:predicate];
    
    if (boardArr.count > 0){
        
        BoardModel *board = [boardArr objectAtIndex:0];
        
        return board.boardName;
    }

    return @"";
    
}

- (void)getSectionForSelectedClass{
    
    isSectionSelectAll = NO;
    isSectionSelectionDone = NO;
    
    isStudentSelectionDone = NO;
    isStudentSelectAll = NO;
    
    _sectionSelectionLbl.text = @"No Section Selected";
    [_sectionSelectionLbl setTextColor:UNSELECTED_COLOR];
    
    _studentSelectionLbl.text = @"No Students Selected";
    [_studentSelectionLbl setTextColor:UNSELECTED_COLOR];

    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    [sectionOptionsListArr removeAllObjects];
    
    
    NSArray *selectedClassArr = [classOptinsListArr filteredArrayUsingPredicate:pred];
    
    NSMutableArray *tmpSectionOptionsArr = [[NSMutableArray alloc]init];
    
    for (ClassesModel *class in selectedClassArr) {
        
        NSPredicate *classOptionsPreidicate = [NSPredicate predicateWithFormat:@"classTransId contains[cd] %@",class.classTransId];
        
        [tmpSectionOptionsArr addObjectsFromArray:[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Section" withPredicate:classOptionsPreidicate]];
        
    }
    
    for (Section *section in tmpSectionOptionsArr) {
        
        SectionModel *sectionObj = [[SectionModel alloc]init];
        
        sectionObj.schoolTransId = section.schoolTransId;
        sectionObj.boardTransId = section.boardTransId;
        sectionObj.boardName = section.boardName;
        sectionObj.classTransId = section.classTransId;
        sectionObj.classStandardName =section.classStandardName;
        sectionObj.sectionTransId = section.sectionTransId;
        sectionObj.sectionName = section.sectionName;
        
        [sectionOptionsListArr addObject:sectionObj];
        
        sectionObj = nil;
        
    }
    
    tmpSectionOptionsArr = nil;
    
    [sectionOptionsListArr sortUsingComparator:^NSComparisonResult(SectionModel *obj1,SectionModel *obj2) {
        
        return [obj1.classStandardName compare:obj2.classStandardName];
        
    }];
    
    [sectionOptionsListArr sortUsingComparator:^NSComparisonResult(SectionModel *obj1,SectionModel *obj2) {
        
        return [obj1.sectionName compare:obj2.sectionName];
        
    }];
    
    [sectionOptionsListArr sortUsingComparator:^NSComparisonResult(SectionModel *obj1,SectionModel *obj2) {
        
        return [obj1.boardName compare:obj2.boardName];
        
    }];
    
}

- (void)updateSelectionList{
    
    switch (listType) {
        case RECEIVER_LIST_TYPE_BOARD://Board Selection
        {

            NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *selectedBoardArr = [boardListArr filteredArrayUsingPredicate:pred];
            
            //Select All Status
            
            if (selectedBoardArr.count == boardListArr.count)
                isBoardSelectAll = YES;
            else
                isBoardSelectAll = NO;
            
            [_listTblVw reloadData];
            
            //Selection list Header
            if (isBoardSelectAll) {
                
                [self getReceiversForSelectedBoardTransId];
                
                [self getFilterAndReceipientDetatils];
                
                [self getStaffListForSelectedBoard];
                
                selectLabel.text = @"All board selected";
                
            } else if (selectedBoardArr.count > 0) {
                
                [self getReceiversForSelectedBoardTransId];
                
                [self getFilterAndReceipientDetatils];
                
                [self getStaffListForSelectedBoard];
                
                selectLabel.text = [NSString stringWithFormat:@"%d board selected",(int)selectedBoardArr.count];
                
            }else {
                
                selectLabel.text = @"Select Board";
                
                
            }
            
            //Board TrextField
            [_boardSelectionLbl setTextColor:SELECTED_COLOR];
            
            if (isBoardSelectAll) {
                
                _boardSelectionLbl.text = @"All board selected";
                
                isBoardSelctionDone = YES;
                
            } else if (selectedBoardArr.count > 1) {
                
                _boardSelectionLbl.text = [NSString stringWithFormat:@"%d board selected",(int)selectedBoardArr.count];
                
                isBoardSelctionDone = YES;

            }else if (selectedBoardArr.count == 1){
                
                Board *board = [selectedBoardArr objectAtIndex:0];
                
                _boardSelectionLbl.text = board.boardName;
                
                isBoardSelctionDone = YES;

                
            }else {
                
                isBoardSelctionDone = NO;
                
                _boardSelectionLbl.text = @"No Boards Selected";
                
                [_boardSelectionLbl setTextColor:UNSELECTED_COLOR];
                
            }
            
            break;
        }
            
        case RECEIVER_LIST_TYPE_FILTER: //Filter Selection
        {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *selectedFilterArr = [filterListArr filteredArrayUsingPredicate:pred];
            
            //Filter TextField
            [_filterSelectionLbl setTextColor:SELECTED_COLOR];
            if (selectedFilterArr.count > 0)
            {
                ReceiverFilterList *filter = [selectedFilterArr objectAtIndex:0];
                
                _filterSelectionLbl.text =filter.filterDescription;
                
                isFilterSelctionDone = YES;
                
            } else {
                
                _filterSelectionLbl.text =@"No Category Selected";
                
                [_filterSelectionLbl setTextColor:UNSELECTED_COLOR];

                
                isFilterSelctionDone = NO;

            }
            
            break;
        }
        case RECEIVER_LIST_TYPE_FILTEROPTION: //Filter Option Selection
        {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *selectedFilterArr = [filterOptinsListArr filteredArrayUsingPredicate:pred];
            
            //Select All Status
            
            if (selectedFilterArr.count == filterOptinsListArr.count)
                isFilterOptionSelectAll = YES;
            else
                isFilterOptionSelectAll = NO;
            
            [_listTblVw reloadData];
            
            //Selection list Header
            
            if (isFilterOptionSelectAll) {
                
                selectLabel.text = [NSString stringWithFormat:@"All %@ selected",selectedFilterObj.filterDescription];
                
            }else if (selectedFilterArr.count > 0) {
                                
                selectLabel.text = [NSString stringWithFormat:@"%d %@ selected",(int)selectedFilterArr.count,selectedFilterObj.filterDescription];
                
            }else {
                
                selectLabel.text = [NSString stringWithFormat:@"Select %@",selectedFilterObj.filterDescription];
            }
            
            //Filter Option TextField
            
            [_filterOptionSelectionLbl setTextColor:SELECTED_COLOR];

            
            if (isFilterOptionSelectAll) {
                
                _filterOptionSelectionLbl.text = [NSString stringWithFormat:@"All %@ selected",selectedFilterObj.filterDescription];
                
                isFilterOptionSelectionDone = YES;
                
            }else if (selectedFilterArr.count > 1) {
                
                _filterOptionSelectionLbl.text = [NSString stringWithFormat:@"%d %@ selected",(int)selectedFilterArr.count,selectedFilterObj.filterDescription];
                
                isFilterOptionSelectionDone = YES;
                
            }else if (selectedFilterArr.count == 1)
            {
                ReceiverDetailsModel *filterOption = [selectedFilterArr objectAtIndex:0];
                
                _filterOptionSelectionLbl.text =filterOption.receiverDescription;
                
                isFilterOptionSelectionDone = YES;
                
            } else {
                
                _filterOptionSelectionLbl.text = [NSString stringWithFormat:@"NO %@ Selected",selectedFilterObj.filterDescription ];
                
                [_filterOptionSelectionLbl setTextColor:UNSELECTED_COLOR];

                isFilterOptionSelectionDone = NO;
                
            }
            
            break;
        }
        case RECEIVER_LIST_TYPE_RECEIPIENT: //Receipient Selection
        {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *selectedReceipientArr = [receipientListArr filteredArrayUsingPredicate:pred];
            
            
            //Filter TextField
            
            [_receipientSelectionLbl setTextColor:SELECTED_COLOR];
            
            if (selectedReceipientArr.count > 0)
            {
                ReceiverReceipientList *receipient = [selectedReceipientArr objectAtIndex:0];
                
                _receipientSelectionLbl.text =receipient.receipientDescription;
                
                isReceipientSelectionDone = YES;
                
                [self arrangeReceipientSelection];
    
                
            } else {
                
                _receipientSelectionLbl.text =@"No Receipient Selected";
                
                [_receipientSelectionLbl setTextColor:UNSELECTED_COLOR];
                
                isReceipientSelectionDone = NO;
                
                [_classGroupSelectionVw setHidden:YES];
                [_sectionSelectionVw setHidden:YES];
                [_studentSelectionVw setHidden:YES];
                
            }
            
            break;
        }
        case RECEIVER_LIST_TYPE_GROUP: //Receipient Group Option Selection
        {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *selectedGroupArr = [groupOptinsListArr filteredArrayUsingPredicate:pred];
            
            //Select All Status
            
            if (selectedGroupArr.count == groupOptinsListArr.count)
                isGroupSelectAll = YES;
            else
                isGroupSelectAll = NO;
            
            [_listTblVw reloadData];
            
            //Selection list Header
            
            if (isGroupSelectAll) {
                
                selectLabel.text = [NSString stringWithFormat:@"All %@ selected",selectedReceipientObj.receipientDescription];
                
            }else if (selectedGroupArr.count > 0) {
                
                selectLabel.text = [NSString stringWithFormat:@"%d %@ selected",(int)selectedGroupArr.count,selectedReceipientObj.receipientDescription];
                
            }else {
                
                selectLabel.text = [NSString stringWithFormat:@"Select %@",selectedReceipientObj.receipientDescription];
            }
            
            //Filter Option TextField
            
            [_classGroupSelctionLbl setTextColor:SELECTED_COLOR];

            
            if (isGroupSelectAll) {
                
                _classGroupSelctionLbl.text = [NSString stringWithFormat:@"All %@ selected",selectedReceipientObj.receipientDescription];
                
                isGroupSelectionDone = YES;
                
            }else if (selectedGroupArr.count > 1) {
                
                _classGroupSelctionLbl.text = [NSString stringWithFormat:@"%d %@s selected",(int)selectedGroupArr.count,selectedReceipientObj.receipientDescription];
                
                isGroupSelectionDone = YES;
                
            }else if (selectedGroupArr.count == 1)
            {
                GroupModel *groupOption = [selectedGroupArr objectAtIndex:0];
                
                _classGroupSelctionLbl.text = groupOption.groupName;
                
                isGroupSelectionDone = YES;
                
            } else {
                
                _classGroupSelctionLbl.text = [NSString stringWithFormat:@"NO %@ Selected",selectedReceipientObj.receipientDescription ];
                
                [_classGroupSelctionLbl setTextColor:UNSELECTED_COLOR];

                
                isGroupSelectionDone = NO;
                
            }
            
            break;
        }
        case RECEIVER_LIST_TYPE_CLASS: //Receipient Class Selection
        {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *selectedClassArr = [classOptinsListArr filteredArrayUsingPredicate:pred];
            
            //Select All Status
            
            if (selectedClassArr.count == classOptinsListArr.count)
                isClassSelectAll = YES;
            else
                isClassSelectAll = NO;
            
            [_listTblVw reloadData];
            
            //Selection list Header
            
            if (isClassSelectAll) {
                
                selectLabel.text = @"All Classes Selected";
                
            }else if (selectedClassArr.count > 0) {
                
                selectLabel.text = [NSString stringWithFormat:@"%d %@ selected",(int)selectedClassArr.count,selectedReceipientObj.receipientDescription];
                
            }else {
                
                selectLabel.text = [NSString stringWithFormat:@"Select %@",selectedReceipientObj.receipientDescription];
            }
            
            //Filter Option TextField
            
            [_classGroupSelctionLbl setTextColor:SELECTED_COLOR];
            
            if (isClassSelectAll) {
                
                _classGroupSelctionLbl.text = @"All Classes Selected";
                
                isClassSelectionDone = YES;
                
            }else if (selectedClassArr.count > 1) {
                
                _classGroupSelctionLbl.text = [NSString stringWithFormat:@"%d Classes Selected",(int)selectedClassArr.count];
                
                isClassSelectionDone = YES;
                
            }else if (selectedClassArr.count == 1)
            {
                ClassesModel *class = [selectedClassArr objectAtIndex:0];
                
                _classGroupSelctionLbl.text = [NSString stringWithFormat:@"%@ - %@",class.boardName,class.classStandardName];
                
                isClassSelectionDone = YES;
                
            } else {
                
                _classGroupSelctionLbl.text = @"NO Classes Selected";
                
                [_classGroupSelctionLbl setTextColor:UNSELECTED_COLOR];
                
                isClassSelectionDone = NO;
                
            }
            
            break;
        }
        case RECEIVER_LIST_TYPE_SECTION: //Receipient Class Selection
        {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *selectedSectionArr = [sectionOptionsListArr filteredArrayUsingPredicate:pred];
            
            //Select All Status
            
            if (selectedSectionArr.count == sectionOptionsListArr.count)
                isSectionSelectAll = YES;
            else
                isSectionSelectAll = NO;
            
            [_listTblVw reloadData];
            
            //Selection list Header
            
            if (isSectionSelectAll) {
                
                selectLabel.text = @"All Section Selected";
                
            }else if (selectedSectionArr.count > 0) {
                
                selectLabel.text = [NSString stringWithFormat:@"%d %@ selected",(int)selectedSectionArr.count,@"Sections"];
                
            }else {
                
                selectLabel.text = @"Select Section";
            }
            
            //Filter Option TextField
            
            [_sectionSelectionLbl setTextColor:SELECTED_COLOR];

            
            if (isSectionSelectAll) {
                
                _sectionSelectionLbl.text = @"All Section Selected";
                
                isSectionSelectionDone = YES;
                
            }else if (selectedSectionArr.count > 1) {
                
                _sectionSelectionLbl.text = [NSString stringWithFormat:@"%d Section Selected",(int)selectedSectionArr.count];
                
                isSectionSelectionDone = YES;
                
            }else if (selectedSectionArr.count == 1)
            {
                SectionModel *section = [selectedSectionArr objectAtIndex:0];
                
                _sectionSelectionLbl.text = [NSString stringWithFormat:@"%@ %@ %@",section.boardName,section.classStandardName,section.sectionName];
                
                isSectionSelectionDone = YES;
                
            } else {
                
                _sectionSelectionLbl.text = @"No Section Selected";
                
                [_sectionSelectionLbl setTextColor:UNSELECTED_COLOR];
                
                isSectionSelectionDone = NO;
                
            }
            
            //Data Clear For Student
            
            _studentSelectionLbl.text = @"No Student Selected";
            
            [_studentSelectionLbl setTextColor:UNSELECTED_COLOR];

            
            [studentListArr removeAllObjects];
            
            break;
        }
        case RECEIVER_LIST_TYPE_STUDENT: //Receipient Student Selection
        {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *selectedStudentArr = [studentListArr filteredArrayUsingPredicate:pred];
            
            //Select All Status
            
            if (selectedStudentArr.count == studentListArr.count)
                isStudentSelectAll = YES;
            else
                isStudentSelectAll = NO;
            
            [_listTblVw reloadData];
            
            //Selection list Header
            
            if (isStudentSelectAll) {
                
                selectLabel.text = @"All Student Selected";
                
            }else if (selectedStudentArr.count > 0) {
                
                selectLabel.text = [NSString stringWithFormat:@"%d %@ selected",(int)selectedStudentArr.count,@"Students"];
                
            }else {
                
                selectLabel.text = @"Select Student";
            }
            
            //Filter Option TextField
            
            [_studentSelectionLbl setTextColor:SELECTED_COLOR];

            
            if (isStudentSelectAll) {
                
                _studentSelectionLbl.text = @"All Students Selected";
                
                isStudentSelectionDone = YES;
                
            }else if (selectedStudentArr.count > 1) {
                
                _studentSelectionLbl.text = [NSString stringWithFormat:@"%d Student Selected",(int)selectedStudentArr.count];
                
                isStudentSelectionDone = YES;
                
            }else if (selectedStudentArr.count == 1)
            {
                StudentModel *student = [selectedStudentArr objectAtIndex:0];
                
                _studentSelectionLbl.text = student.studentName;
                
                isStudentSelectionDone = YES;
                
            } else {
                
                _studentSelectionLbl.text = @"No Student Selected";
                
                [_studentSelectionLbl setTextColor:UNSELECTED_COLOR];
                
                isStudentSelectionDone = NO;
                
            }
            
            break;
        }
        case RECEIVER_LIST_TYPE_STAFF: //Receipient Staff Selection
        {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *selectedStaffArr = [staffListArr filteredArrayUsingPredicate:pred];
            
            //Select All Status
            
            if (selectedStaffArr.count == staffListArr.count)
                isStaffSelectAll = YES;
            else
                isStaffSelectAll = NO;
            
            [_listTblVw reloadData];
            
            //Selection list Header
            
            if (isStaffSelectAll) {
                
                selectLabel.text = @"All Staff Selected";
                
            }else if (selectedStaffArr.count > 0) {
                
                selectLabel.text = [NSString stringWithFormat:@"%d %@ Selected",(int)selectedStaffArr.count,@"Staffs"];
                
            }else {
                
                selectLabel.text = @"Select Staff";
            }
            
            //Staff TextField
            
            [_staffSelectionLbl setTextColor:SELECTED_COLOR];
            
            if (isStaffSelectAll) {
                
                _staffSelectionLbl.text = @"All Staff Selected";
                
                isStaffSelectionDone = YES;
                
            }else if (selectedStaffArr.count > 1) {
                
                _staffSelectionLbl.text = [NSString stringWithFormat:@"%d Staffs Selected",(int)selectedStaffArr.count];
                
                isStaffSelectionDone = YES;
                
            }else if (selectedStaffArr.count == 1)
            {
                StaffModel *staff = [selectedStaffArr objectAtIndex:0];
                
                _staffSelectionLbl.text = staff.staffName;
                
                isStaffSelectionDone = YES;
                
            } else {
                
                _staffSelectionLbl.text = @"No Staff Selected";
                
                [_staffSelectionLbl setTextColor:UNSELECTED_COLOR];
                
                isStaffSelectionDone = NO;
                
            }
            
            break;
        }


            
        default:
            break;
    }
    
    CGRect staffFrame = [_staffSelectionVw convertRect:_staffSelectionVw.frame toView:self.scrollVw];

    
    [_scrollVw setContentSize:CGSizeMake(DeviceWidth, staffFrame.origin.y + staffFrame.size.height)];
    
}

- (void)arrangeReceipientSelection{
    
    if (_communicationType == OBJECT_TYPE_NOTIFICATION || _communicationType == OBJECT_TYPE_EVENT || _communicationType == OBJECT_TYPE_PORTIONS) {
        
        [_staffSelectionVw setHidden:NO];
        
    }
    
    if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_ENTIRESCHOOL]) {
        
        [_classGroupSelectionVw setHidden:YES];
        [_sectionSelectionVw setHidden:YES];
        [_studentSelectionVw setHidden:YES];
        [_staffSelectionVw setHidden:YES];
        
        _staffSelectionVw.frame = CGRectMake(_staffSelectionVw.frame.origin.x, _receipientSelectionVw.frame.origin.y + _receipientSelectionVw.frame.size.height, _staffSelectionVw.frame.size.width, _staffSelectionVw.frame.size.height);
        
    }else if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_ALLSTUDENT])
    {
        
        [_classGroupSelectionVw setHidden:YES];
        [_sectionSelectionVw setHidden:YES];
        [_studentSelectionVw setHidden:YES];
        
        _staffSelectionVw.frame = CGRectMake(_staffSelectionVw.frame.origin.x, _receipientSelectionVw.frame.origin.y + _receipientSelectionVw.frame.size.height, _staffSelectionVw.frame.size.width, _staffSelectionVw.frame.size.height);

        
    }else if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_GROUP])
    {
        
        [_classGroupSelectionVw setHidden:NO];
        [_sectionSelectionVw setHidden:YES];
        [_studentSelectionVw setHidden:YES];
        
        [self getGroupForSelectedBoard];

        
        _classGroupTitleLbl.text = [NSString stringWithFormat:@"Select %@",selectedReceipientObj.receipientDescription];
        
        _classGroupSelctionLbl.text = [NSString stringWithFormat:@"No %@ Selected",selectedReceipientObj.receipientDescription];
        
        [_classGroupSelctionLbl setTextColor:UNSELECTED_COLOR];
        
        _staffSelectionVw.frame = CGRectMake(_staffSelectionVw.frame.origin.x, _classGroupSelectionVw.frame.origin.y + _classGroupSelectionVw.frame.size.height, _staffSelectionVw.frame.size.width, _staffSelectionVw.frame.size.height);
        
    }else if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_CLASS]){
        
        [_classGroupSelectionVw setHidden:NO];
        [_sectionSelectionVw setHidden:YES];
        [_studentSelectionVw setHidden:YES];
        
        _classGroupTitleLbl.text = [NSString stringWithFormat:@"Select %@",selectedReceipientObj.receipientDescription];
        
        _classGroupSelctionLbl.text = [NSString stringWithFormat:@"No %@ Selected",selectedReceipientObj.receipientDescription];
        
        [_classGroupSelctionLbl setTextColor:UNSELECTED_COLOR];
        
        _staffSelectionVw.frame = CGRectMake(_staffSelectionVw.frame.origin.x, _classGroupSelectionVw.frame.origin.y + _classGroupSelectionVw.frame.size.height, _staffSelectionVw.frame.size.width, _staffSelectionVw.frame.size.height);
        
    }else if([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_SECTION]){
        
        [_classGroupSelectionVw setHidden:NO];
        [_sectionSelectionVw setHidden:NO];
        [_studentSelectionVw setHidden:YES];
        
        _classGroupTitleLbl.text = @"Select Class";
        _classGroupSelctionLbl.text = @"No Classes Selected";
        [_classGroupSelctionLbl setTextColor:UNSELECTED_COLOR];
        
        _sectionSelectionLbl.text = @"No Sections Selected";
        [_sectionSelectionLbl setTextColor:UNSELECTED_COLOR];
        
        _staffSelectionVw.frame = CGRectMake(_staffSelectionVw.frame.origin.x, _sectionSelectionVw.frame.origin.y + _sectionSelectionVw.frame.size.height, _staffSelectionVw.frame.size.width, _staffSelectionVw.frame.size.height);
        
        
    }else if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_STUDENT]){
        
        [_classGroupSelectionVw setHidden:NO];
        [_sectionSelectionVw setHidden:NO];
        [_studentSelectionVw setHidden:NO];
        
        _classGroupTitleLbl.text = @"Select Class";
        _classGroupSelctionLbl.text = @"No Classes Selected";
        [_classGroupSelctionLbl setTextColor:UNSELECTED_COLOR];
        
        _sectionSelectionLbl.text = @"No Sections Selected";
        [_sectionSelectionLbl setTextColor:UNSELECTED_COLOR];
        
        _studentSelectionLbl.text = @"No Students Selected";
        [_studentSelectionLbl setTextColor:UNSELECTED_COLOR];

        
        _staffSelectionVw.frame = CGRectMake(_staffSelectionVw.frame.origin.x, _studentSelectionVw.frame.origin.y + _studentSelectionVw.frame.size.height, _staffSelectionVw.frame.size.width, _staffSelectionVw.frame.size.height);
        
    } else {
        [_classGroupSelectionVw setHidden:YES];
        [_sectionSelectionVw setHidden:YES];
        [_studentSelectionVw setHidden:YES];
        
        _staffSelectionVw.frame = CGRectMake(_staffSelectionVw.frame.origin.x, _receipientSelectionVw.frame.origin.y + _receipientSelectionVw.frame.size.height, _staffSelectionVw.frame.size.width, _staffSelectionVw.frame.size.height);
    }
    
}

- (void)clearData
{
    switch (listType) {
        case RECEIVER_LIST_TYPE_BOARD://Board Selection
        {
            
            _filterSelectionLbl.text =@"No Category Selected";
            isFilterSelctionDone = NO;
            [_filterSelectionLbl setTextColor:UNSELECTED_COLOR];

            
            isFilterOptionSelectionDone = NO;
            _receipientVw.frame = CGRectMake(_receipientVw.frame.origin.x, _filterSelectionVw.frame.origin.y + _filterSelectionVw.frame.size.height, _receipientVw.frame.size.width, _receipientVw.frame.size.height);
            
            isReceipientSelectionDone = NO;
            _receipientSelectionLbl.text = @"No Receipient Selected";
            [_receipientSelectionLbl setTextColor:UNSELECTED_COLOR];

            
            
            _staffSelectionVw.frame = CGRectMake(_staffSelectionVw.frame.origin.x, _receipientSelectionVw.frame.origin.y + _receipientSelectionVw.frame.size.height, _staffSelectionVw.frame.size.width, _staffSelectionVw.frame.size.height);
            _staffSelectionLbl.text = @"No Staff Selected";
            [_staffSelectionLbl setTextColor:UNSELECTED_COLOR];

            
            isClassSelectionDone = NO;
            isSectionSelectionDone = NO;
            isStudentSelectionDone = NO;
            isStaffSelectionDone = NO;
            isStaffSelectAll = NO;
            
            [_classGroupSelectionVw setHidden:YES];
            [_sectionSelectionVw setHidden:YES];
            [_studentSelectionVw setHidden:YES];
            
            break;
        }
            
        case RECEIVER_LIST_TYPE_FILTER: //Filter Selection
        {
            
            break;
        }
        case RECEIVER_LIST_TYPE_FILTEROPTION: //Filter Option Selection
        {
            
            break;
        }
        case RECEIVER_LIST_TYPE_RECEIPIENT: //Receipient Selection
        {
            break;
        }
            
            
        default:
            break;
    }
}

- (void)showListTblVw{
    
    _listVw.hidden = NO;
    
    [_listTblVw reloadData];
    
    
    _listHeaderVw.frame =CGRectMake(self.view.frame.origin.x, DeviceHeight, self.view.frame.size.width, self.view.frame.size.height/2);
    
    _listTblVw.frame = CGRectMake(self.view.frame.origin.x, DeviceHeight, self.view.frame.size.width, self.view.frame.size.height/2);
    
    [UIView animateWithDuration:0.3 animations:^{
        
        _listTblVw.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.size.height/2 - 100, self.view.frame.size.width, self.view.frame.size.height/2 + 100);
        
    }];
}

- (void)hideListTblVw{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        _listTblVw.frame = CGRectMake(self.view.frame.origin.x, DeviceHeight, self.view.frame.size.width, self.view.frame.size.height/2);
        
    } completion:^(BOOL finished) {
        
        _listVw.hidden = YES;
        
    }];
}

#pragma mark - Tap Action

- (IBAction)onBoardSelctionTap:(id)sender {
    
    listType = RECEIVER_LIST_TYPE_BOARD;
    
    selectLabel.text = @"Select Board";
    
    if (boardListArr.count > 0) {
        
        [self showListTblVw];
        
    }else {
        
        [SINGLETON_INSTANCE fetchBoardAndClassDetailsServiceCallWithSchoolTransId:SINGLETON_INSTANCE.selectedSchoolTransId showIndicator:YES onCompletion:^(BOOL isSuccess) {
            
            if (isSuccess) {
                
                [self createFilterOptions];
                
                if (boardListArr.count > 0){
                    
                    [self showListTblVw];
                
                } else{
                    
                    NSPredicate *boardPredicate = [NSPredicate predicateWithFormat:@"schoolTransId contains[cd] %@",SINGLETON_INSTANCE.selectedSchoolTransId];
                    
                    NSArray *boardArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Board" withPredicate:boardPredicate];
                    
                    if (boardArr.count > 0)
                        [TSSingleton showAlertWithMessage:@"You does not have access to any board"];
                    else
                        [TSSingleton showAlertWithMessage:@"No Boards Available for School"];
                }
                
            }else {
                [TSSingleton showAlertWithMessage:@"Failed to get Board List"];
            }
            
        }];
    }
    

}
- (IBAction)onFilterSelectionTap:(id)sender {
    
    if (isBoardSelctionDone) {
        
        listType = RECEIVER_LIST_TYPE_FILTER;
        
        selectLabel.text = @"Select Category";
        
        if (filterListArr.count > 0) {
            
            [self showListTblVw];
            
        }else {
            
            [SINGLETON_INSTANCE fetchFilterAndReceipientDetailsWithMenuId:_menuId showIndicator:YES onCompletion:^(BOOL isSuccess) {
                
                if (isSuccess) {
                    
                    [self getFilterAndReceipientDetatils];
                    
                    if (filterListArr.count > 0)
                        [self showListTblVw];
                    else
                        [TSSingleton showAlertWithMessage:@"No Category Available for Selected Board"];
                    
                }else {
                    [TSSingleton showAlertWithMessage:@"Failed to get Category List"];
                }

                
            }];
            
        }

    } else {
        
        [TSSingleton showAlertWithMessage:@"Please Select Board"];
        
    }
    
}

- (IBAction)onFilterOptionSelectionTap:(id)sender {
    
    if (isFilterSelctionDone) {
        
        NSPredicate *filterOptionPred = [NSPredicate predicateWithFormat:@"receiverTypeId contains[cd] %@",selectedFilterObj.filterTypeId];
        
        filterOptinsListArr = [receiverOptionsArr filteredArrayUsingPredicate:filterOptionPred];
        
        listType = RECEIVER_LIST_TYPE_FILTEROPTION;
        
        selectLabel.text = @"Select Category";
        
        if (filterOptinsListArr.count > 0)
            [self showListTblVw];
        else
        {
            
            [SINGLETON_INSTANCE fetchReceiverDetailsWithSchoolTransId:SINGLETON_INSTANCE.selectedSchoolTransId showIndicator:YES onCompletion:^(BOOL isSuccess) {
                
                if (isSuccess) {
                    
                    [self getReceiversForSelectedBoardTransId];
                    
                    [self getFilterAndReceipientDetatils];
                    
                    [self getStaffListForSelectedBoard];
                    
                    if (filterOptinsListArr.count > 0) {
                        [self showListTblVw];
                    }else {
                        isFilterOptionSelectionDone = NO;
                        
                        [TSSingleton showAlertWithMessage:[NSString stringWithFormat:@"No %@ available",selectedFilterObj.filterDescription]];
                    }
                }else {
                    [TSSingleton showAlertWithMessage:[NSString stringWithFormat:@"Failed To Get %@ List",selectedFilterObj.filterDescription]];
                }
                
            }];
        }
        
    } else {
        
        [TSSingleton showAlertWithMessage:@"Please Select Category"];
    }
    
    
}


- (IBAction)onReceipientTap:(id)sender {
    
    if (isFilterOptionSelectionDone) {
        
        listType = RECEIVER_LIST_TYPE_RECEIPIENT;
        
        selectLabel.text = @"Select Receipient";
        
        [self showListTblVw];
        
    } else {
        
        if (isFilterSelctionDone)
            
            [TSSingleton showAlertWithMessage:[NSString stringWithFormat:@"Please Select %@",selectedFilterObj.filterDescription]];
        else
            
            [TSSingleton showAlertWithMessage:@"Please Select Category"];

    }
}

- (IBAction)onClassGroupTap:(id)sender {
    
    if (isReceipientSelectionDone) {
        
        if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_GROUP])//Group Selection
        {
            isNeedToCheckGroup = YES;
            
            listType = RECEIVER_LIST_TYPE_GROUP;
            
            selectLabel.text = @"Select Group";
            
            if (groupOptinsListArr.count > 0)
                [self showListTblVw];
            else
            {
                
                [SINGLETON_INSTANCE fetchGroupListBySchoolTransId:SINGLETON_INSTANCE.selectedSchoolTransId showIndicator:YES onCompletion:^(BOOL isSuccess) {
                    
                    if (isSuccess) {
                        
                        [self getGroupForSelectedBoard];
                        
                        if (groupOptinsListArr.count > 0)
                            [self showListTblVw];
                        else
                            [TSSingleton showAlertWithMessage:@"No Groups  Available For Selected Board"];
                        
                    }else {
                        
                        isGroupSelectionDone = NO;

                        [TSSingleton showAlertWithMessage:@"Failed To Get Group List"];
                    }
                }];
            }
        }else if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_CLASS] || [selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_SECTION] || [selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_STUDENT]){//Class Selection
            
            isNeedToCheckGroup = NO;
            
            listType = RECEIVER_LIST_TYPE_CLASS;
            
            selectLabel.text = @"Select Class";
            
            if (classOptinsListArr.count > 0)
                [self showListTblVw];
            else
            {
                isClassSelectionDone = NO;
                
                [TSSingleton showAlertWithMessage:[NSString stringWithFormat:@"No %@ available",selectedReceipientObj.receipientDescription]];
            }

        }
        
    }else {
        
        [TSSingleton showAlertWithMessage:@"Please Select Receipient Type"];
    }
}


- (IBAction)onSectionTap:(id)sender {
    
    if (isClassSelectionDone) {
        
        listType = RECEIVER_LIST_TYPE_SECTION;
        
        selectLabel.text = @"Select Section";
        
        if (sectionOptionsListArr.count > 0)
            [self showListTblVw];
        else
        {
            isSectionSelectionDone = NO;
            
            [TSSingleton showAlertWithMessage:@"No Sections Available for Selected Class"];
        }
    }else {
        
        [TSSingleton showAlertWithMessage:@"Please Select Class"];
    }
    
}

- (IBAction)onStudentTap:(id)sender {
    
    if (isSectionSelectionDone) {
        
        if (studentListArr.count == 0)
            [self fetchStudentListForSelectedSection];
        else
            [self showStudentSelectionList];

    }else {
        
        [TSSingleton showAlertWithMessage:@"Please Select Section"];
    }
}

- (void)showStudentSelectionList{
    
    if (isClassSelectionDone) {
        
        listType = RECEIVER_LIST_TYPE_STUDENT;
        
        selectLabel.text = @"Select Student";
        
        if (sectionOptionsListArr.count > 0)
            [self showListTblVw];
        else
        {
            isSectionSelectionDone = NO;
            
            [TSSingleton showAlertWithMessage:@"No Student Available for Selected Sections"];
        }
    }else {
        
        [TSSingleton showAlertWithMessage:@"Please Select Class"];
    }
    
}
- (IBAction)onStaffSelectionTap:(id)sender {
    
    if (isReceipientSelectionDone) {
        
        listType = RECEIVER_LIST_TYPE_STAFF;
        
        selectLabel.text = @"Select Staff";
        
        if (staffListArr.count > 0)
            [self showListTblVw];
        
        else
        {
            
            [SINGLETON_INSTANCE fetchStaffListBySchoolTransId:SINGLETON_INSTANCE.selectedSchoolTransId showIndicator:YES onCompletion:^(BOOL isSuccess) {
                
                if (isSuccess) {
                    
                    [self getStaffListForSelectedBoard];
                    
                    if (staffListArr.count > 0)
                        [self showListTblVw];
                    else
                        [TSSingleton showAlertWithMessage:@"No Staff Available For Selected Board"];
                    
                }else{
                    
                    [TSSingleton showAlertWithMessage:@"Failed To Get Staff List"];
                    
                }
            }];
        }
    }else {
        
        [TSSingleton showAlertWithMessage:@"Please Select Send To"];
    }

}

#pragma mark - Create 

- (NSMutableArray *)getSelectedFilterTransId
{
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *selectedFilterArr = [filterOptinsListArr filteredArrayUsingPredicate:pred];
    
    NSMutableArray *filterTransIdArr = [[NSMutableArray alloc]init];
    
    for (ReceiverDetailsModel *filter in selectedFilterArr) {
        
        [filterTransIdArr addObject:filter.receiverTransId];
    }
    
    return filterTransIdArr;
    
}

- (NSMutableArray *)getSelectedGroupTransId
{
    
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *selectedGroupArr = [groupOptinsListArr filteredArrayUsingPredicate:pred];
    
    NSMutableArray *groupTransIdArr = [[NSMutableArray alloc]init];
    
    for (GroupModel *group in selectedGroupArr) {
        
        [groupTransIdArr addObject:group.groupTransId];
        
    }

    return groupTransIdArr;
}

- (NSMutableArray *)getSelectedClassTransId
{
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *selectedClassArr = [classOptinsListArr filteredArrayUsingPredicate:pred];
    
    NSMutableArray *classTransIdArr = [[NSMutableArray alloc]init];
    
    for (ClassesModel *class in selectedClassArr) {
        
        [classTransIdArr addObject:class.classTransId];
        
    }
    
    return classTransIdArr;
}

- (NSMutableArray *)getSelectedSectionTransId
{
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *selectedSectionArr = [sectionOptionsListArr filteredArrayUsingPredicate:pred];
    
    NSMutableArray *sectionTransIdArr = [[NSMutableArray alloc]init];
    
    for (SectionModel *section in selectedSectionArr) {
        
        [sectionTransIdArr addObject:section.sectionTransId];
        
    }
    
    return sectionTransIdArr;
}

- (NSMutableArray *)getSelectedStudentTransId
{
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *selectedStudentArr = [studentListArr filteredArrayUsingPredicate:pred];
    
    NSMutableArray *studentTransIdArr = [[NSMutableArray alloc]init];
    
    for (StudentModel *student in selectedStudentArr) {
        
        [studentTransIdArr addObject:student.studentTransID];
        
    }
    
    return studentTransIdArr;
}


- (NSMutableArray *)getSelectedStaffTransId
{
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *selectedStaffArr = [staffListArr filteredArrayUsingPredicate:pred];
    
    NSMutableArray *staffTransIdArr = [[NSMutableArray alloc]init];
    
    for (StaffModel *staff in selectedStaffArr) {
        
        [staffTransIdArr addObject:staff.staffTransId];
        
    }
    
    return staffTransIdArr;
}

- (NSMutableArray *)getSelectedBoardTransId
{
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *selectedBoardArr = [boardListArr filteredArrayUsingPredicate:pred];
    
    NSMutableArray *boardTransIdArr = [[NSMutableArray alloc]init];
    
    for (BoardModel *board in selectedBoardArr) {
        
        [boardTransIdArr addObject:board.boardTransId];
        
    }
    
    return boardTransIdArr;
}

- (void)formDetailsDictFromSelection{
    
    [selectionDetailsDict removeAllObjects];
    
    //[selectionDetailsDict setObject:[self getSelectedBoardTransId] forKey:@"BoardTransID"];
    
    [selectionDetailsDict setObject:[self getSelectedStaffTransId] forKey:@"StaffsTransID"];
    
    
    if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_ENTIRESCHOOL]) {
        
        [_commonDict setObject:@"1" forKey:@"GroupType"];
        
    }else if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_ALLSTUDENT])
    {
        [_commonDict setObject:@"2" forKey:@"GroupType"];
        
        
    }else if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_GROUP])
    {
        [_commonDict setObject:@"0" forKey:@"GroupType"];
        
        [selectionDetailsDict setObject:[self getSelectedGroupTransId] forKey:@"GroupTransID"];
        
    }else if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_CLASS]){
        
        [_commonDict setObject:@"0" forKey:@"GroupType"];
        
        if (isClassSelectAll)
            
            [_commonDict setObject:@"2" forKey:@"GroupType"];//All Student
        
        else
            
            [selectionDetailsDict setObject:[self getSelectedClassTransId] forKey:@"ClassTransID"];
        
    }else if([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_SECTION]){
        
        [_commonDict setObject:@"0" forKey:@"GroupType"];
        
        if (isSectionSelectAll){
            
            [selectionDetailsDict setObject:[self getSelectedClassTransId] forKey:@"ClassTransID"];
            
        } else {
            
            [selectionDetailsDict setObject:[self getSelectedSectionTransId] forKey:@"SectionTransID"];
        }
        
        
    }else if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_STUDENT]){
        
        [_commonDict setObject:@"0" forKey:@"GroupType"];
        
        if (isStudentSelectAll){
            
            [selectionDetailsDict setObject:[self getSelectedSectionTransId] forKey:@"SectionTransID"];
            
        } else {
            
            [selectionDetailsDict setObject:[self getSelectedStudentTransId] forKey:@"StudentsTransID"];
        }
        
    } else {
        
    }
    
}

- (BOOL)checkIsAllFieldSeclected{
    
    if (!isBoardSelctionDone) {
        
        [TSSingleton showAlertWithMessage:@"Please Choose Board"];
        
        return NO;
        
    }else if (!isFilterSelctionDone){
        
        [TSSingleton showAlertWithMessage:@"Please Choose Category"];
        
        return NO;
        
    }else if (![selectedFilterObj.filterTypeId isEqualToString:FILTER_TYPE_NONE] && !isFilterOptionSelectionDone){
        
        [TSSingleton showAlertWithMessage:[NSString stringWithFormat:@"Please Choose %@",selectedFilterObj.filterDescription]];
        
        return NO;
        
    }else if (!isReceipientSelectionDone){
        
        [TSSingleton showAlertWithMessage:@"Please Choose Receipient"];
        
        return NO;
        
    }else { //Checking Receipient Selection
        
        if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_ENTIRESCHOOL]) {
            
            
        }else if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_ALLSTUDENT])
        {
            
            
        }else if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_GROUP])
        {
            if (!isGroupSelectionDone){
                
                [TSSingleton showAlertWithMessage:@"Please Choose Group"];
                
                return NO;
                
            }
            
            
        }else if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_CLASS]){
            
            if (!isClassSelectionDone){
                
                [TSSingleton showAlertWithMessage:@"Please Choose Class"];
                
                return NO;
                
            }
            
            
        }else if([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_SECTION]){
            
            if (!isSectionSelectionDone){
                
                [TSSingleton showAlertWithMessage:@"Please Choose Section"];
                
                return NO;
                
            }
            
        }else if ([selectedReceipientObj.receipientTypeId isEqualToString:RECEPIENT_TYPE_STUDENT]){
            
            if (!isStudentSelectionDone){
                
                [TSSingleton showAlertWithMessage:@"Please Choose Student"];
                
                return NO;
                
            }
            
        }
    }
    
    if ([selectedFilterObj.filterTypeId isEqualToString:FILTER_TYPE_NONE] && [selectedReceipientObj.receipientTypeId isEqualToString:FILTER_TYPE_NONE] && !isStaffSelectionDone) {
        
        [TSSingleton showAlertWithMessage:@"Please Choose Any Receiver"];
        
        return NO;
        
    }
    
    return YES;
}



#pragma mark Create Button Action

- (IBAction)onCreateBtnClk:(id)sender {
    
    if ([self checkIsAllFieldSeclected]) {
        
    NSMutableDictionary *analyticsDict = [NSMutableDictionary new];
        
    [_commonDict setValue:SINGLETON_INSTANCE.selectedSchoolTransId forKey:@"SchoolTransID"];
    
    [_commonDict setValue:[SINGLETON_INSTANCE staff].acadamicTransId forKey:@"AcadamicTransID"];
        
    [_commonDict setValue:[SINGLETON_INSTANCE staff].staffTransId forKey:@"UserTransID"];
    
    //[_commonDict setObject:selectedReceipientObj.receipientTypeId forKey:@"GroupType"];
    
    [_commonDict setObject:selectedFilterObj.filterTypeId forKey:@"FilterType"];
    
    [_commonDict setObject:[self getSelectedFilterTransId] forKey:@"FilterTransID"];
        
        [analyticsDict setObject:_filterSelectionLbl.text forKey:ANALYTICS_PARAM_CATEGORY_TYPE];
        
        [analyticsDict setObject:_receipientSelectionLbl.text forKey:ANALYTICS_PARAM_RECEIPIENT_TYPE];
    
    [self formDetailsDictFromSelection];
        
    switch (_communicationType) {
            
        case OBJECT_TYPE_EVENT:
        {
            NSString *url = @"MastersMServices/EventsMService.svc/UpdateEventsData";
            
            [selectionDetailsDict setObject:[self getSelectedBoardTransId] forKey:@"BoardTransID"];
            
            [_commonDict setObject:selectionDetailsDict forKey:@"EventsMDetails"];
            
            NSMutableDictionary *params = [NSMutableDictionary new];
            
            [params setObject:_commonDict forKey:@"UpdateEventsM"];
            
            
            [[TSHandlerClass sharedHandler]postRequestWithURL:url baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
                
                if (success) {
                    
                    NSLog(@"%@",responseDictionary);
                    if ([responseDictionary[@"UpdateEventsDataResult"][@"ResponseCode"] integerValue] == 1 ) {
                        
                        [analyticsDict setObject:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_RESPONSE];
                        
                        [analyticsDict setObject:responseDictionary[@"UpdateEventsDataResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                        
                        [TSAnalytics logEvent:ANALYTICS_EVENT_CLICKED_CREATE_EVENT parameter:analyticsDict];
                        
                        [TSSingleton showAlertWithMessage:@"Event Created Successfully"];
                        
                        [self.navigationController popToRootViewControllerAnimated:YES];
                        
                    }else{
                        
                        [SINGLETON_INSTANCE showAlertMessageWithTitle:@"Failed To Create Event" andMessage:@"Some thing went wrong, Please try again"];
                        
                    }
                    
                }else {//Service call Failure
                    //[TSSingleton requestTimeOutErrorAlert];
                }
                
            }];
            
            break;
        }
        case OBJECT_TYPE_NOTIFICATION:
        {
            NSString *url = @"MastersMServices/SchoolNotificationsService.svc/UpdateSchoolNotifications";
            
            [_commonDict setObject:[self getSelectedBoardTransId] forKey:@"BoardTransID"];
            
            [_commonDict setObject:selectionDetailsDict forKey:@"SchoolNotificationDetails"];
            
            NSMutableDictionary *params = [NSMutableDictionary new];
            
            [params setObject:_commonDict forKey:@"UpdateSchoolNotification"];
            
            
            [[TSHandlerClass sharedHandler]postRequestWithURL:url baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
                
                if (success) {
                    
                    NSLog(@"%@",responseDictionary);
                
                    if([responseDictionary[@"UpdateSchoolNotificationsResult"][@"ResponseCode"] integerValue] == 1 ) {
                        
                        [analyticsDict setObject:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_RESPONSE];
                        
                        [analyticsDict setObject:responseDictionary[@"UpdateSchoolNotificationsResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                        
                        [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_CREATE_NOTIFICATION parameter:analyticsDict];
                        
                        [TSSingleton showAlertWithMessage:@"Notification Created Successfully"];
                        
                        [self.navigationController popToRootViewControllerAnimated:YES];
                        
                    }else{
                        
                        [analyticsDict setObject:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_RESPONSE];
                        
                        [analyticsDict setObject:responseDictionary[@"UpdateSchoolNotificationsResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                        
                        [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_CREATE_NOTIFICATION parameter:analyticsDict];
                        
                        [SINGLETON_INSTANCE showAlertMessageWithTitle:@"Failed To Create Notification" andMessage:@"Some thing went wrong, Please try again"];
                        
                    }
                    
                }else {//Service call Failure
                    
                    [analyticsDict setObject:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_RESPONSE];
                    
                    [analyticsDict setObject:ANALYTICS_VALUE_SERVICEFAILURE_RESPONSEMSG forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                    
                    [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_CREATE_NOTIFICATION parameter:analyticsDict];
                    //[TSSingleton requestTimeOutErrorAlert];
                }
                
            }];
            
            break;
        }
        case OBJECT_TYPE_PORTIONS:
        {
            NSString *url = @"MastersMServices/PortionsService.svc/UpdatePortions";
            
            [selectionDetailsDict setObject:[self getSelectedBoardTransId] forKey:@"BoardTransID"];
            
            if (isStaffSelectionDone)
                [selectionDetailsDict setValue:@"true" forKey:@"IsStaff"];
            else
                [selectionDetailsDict setValue:@"false" forKey:@"IsStaff"];
            
            [_commonDict setObject:selectionDetailsDict forKey:@"PortionsDetails"];
            
            NSMutableDictionary *params = [NSMutableDictionary new];
            
            [params setObject:_commonDict forKey:@"UpdatePortion"];
            
            
            [[TSHandlerClass sharedHandler]postRequestWithURL:url baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
                
                if (success) {
                    
                    NSLog(@"%@",responseDictionary);
                    
                    if([responseDictionary[@"UpdatePortionsResult"][@"ResponseCode"] integerValue] == 1 ) {
                        
                        
                        [analyticsDict setObject:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_RESPONSE];
                        
                        [analyticsDict setObject:responseDictionary[@"UpdatePortionsResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                        
                        [TSAnalytics logEvent:ANALYTICS_PORTION_CLICKED_CREATE_PORTION parameter:analyticsDict];
                        
                        [TSSingleton showAlertWithMessage:@"Portion Created Successfully"];
                        
                        [self.navigationController popToRootViewControllerAnimated:YES];
                        
                    }else{
                        
                        [analyticsDict setObject:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_RESPONSE];
                        
                        [analyticsDict setObject:responseDictionary[@"UpdatePortionsResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                        
                        [TSAnalytics logEvent:ANALYTICS_PORTION_CLICKED_CREATE_PORTION parameter:analyticsDict];
                        
                        [SINGLETON_INSTANCE showAlertMessageWithTitle:@"Failed To Create Portion" andMessage:@"Some thing went wrong, Please try again"];
                        
                    }
                    
                }else {//Service call Failure
                    //[TSSingleton requestTimeOutErrorAlert];
                    [analyticsDict setObject:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_RESPONSE];
                    
                    [analyticsDict setObject:ANALYTICS_VALUE_SERVICEFAILURE_RESPONSEMSG forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                    
                    [TSAnalytics logEvent:ANALYTICS_PORTION_CLICKED_CREATE_PORTION parameter:analyticsDict];
                }
                
            }];
            
            break;
        }
        case OBJECT_TYPE_ASSIGNMENT:
        {
            
            NSString *url = @"MastersMServices/AssignmentService.svc/UpdateAssignments";
            
            [selectionDetailsDict setObject:[self getSelectedBoardTransId] forKey:@"BoardTransID"];
            
            [_commonDict setObject:selectionDetailsDict forKey:@"AssignmentDetails"];
            
            NSMutableDictionary *params = [NSMutableDictionary new];
            
            [params setObject:_commonDict forKey:@"UpdateAssignment"];
            
            
            [[TSHandlerClass sharedHandler]postRequestWithURL:url baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
                
                if (success) {
                    
                    NSLog(@"%@",responseDictionary);
                    if([responseDictionary[@"UpdateAssignmentsResult"][@"ResponseCode"] integerValue] == 1 ) {
                        
                        [TSSingleton showAlertWithMessage:@"Assignment Created Successfully"];
                        
                        [self.navigationController popToRootViewControllerAnimated:YES];
                        
                        [analyticsDict setObject:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_RESPONSE];
                        
                        [analyticsDict setObject:responseDictionary[@"UpdateAssignmentsResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                        
                        [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_CREATE_ASSIGNMENT parameter:analyticsDict];
                        
                    }else{
                        
                        [analyticsDict setObject:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_RESPONSE];
                        
                        [analyticsDict setObject:responseDictionary[@"UpdateAssignmentsResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                        
                        [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_CREATE_ASSIGNMENT parameter:analyticsDict];
                        
                        [SINGLETON_INSTANCE showAlertMessageWithTitle:@"Failed To Create Assignmentz" andMessage:@"Some thing went wrong, Please try again"];
                        
                    }
                    
                }else {//Service call Failure
                    //[TSSingleton requestTimeOutErrorAlert];
                    [analyticsDict setObject:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_RESPONSE];
                    
                    [analyticsDict setObject:ANALYTICS_VALUE_SERVICEFAILURE_RESPONSEMSG forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                    
                    [TSAnalytics logEvent:ANALYTICS_ASSIGNMENT_CLICKED_CREATE_ASSIGNMENT parameter:analyticsDict];
                }
                
            }];
            
            break;
        }
            
            
        default:
            break;
        }
    }
}


#pragma mark - Table view data sources & delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    switch (listType) {
            
        case RECEIVER_LIST_TYPE_BOARD:
        {
            
            return boardListArr.count + 1;
            
            break;
        }
        case RECEIVER_LIST_TYPE_FILTER:
        {
            return filterListArr.count;
            
            break;
        }
        case RECEIVER_LIST_TYPE_FILTEROPTION:
        {
            return filterOptinsListArr.count + 1;
            
            break;
        }
        case RECEIVER_LIST_TYPE_RECEIPIENT:
        {
            return receipientListArr.count;
            
            break;
        }
        case RECEIVER_LIST_TYPE_GROUP:
        {
            return groupOptinsListArr.count + 1;
            
            break;
        }
        case RECEIVER_LIST_TYPE_CLASS:
        {
            return classOptinsListArr.count + 1;
            
            break;
        }
        case RECEIVER_LIST_TYPE_SECTION:
        {
            
            return sectionOptionsListArr.count + 1;
            
            break;
        }
        case RECEIVER_LIST_TYPE_STUDENT:
        {
            
            return studentListArr.count + 1;
            
            break;
        }
        case RECEIVER_LIST_TYPE_STAFF:
        {
            
            return staffListArr.count + 1;
            
            break;
        }
            
        default:
            break;
    }

    
    return 0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UILabel *titleLbl = (UILabel *)[cell viewWithTag:101];
    
    UIImageView *typeImgVw = (UIImageView *)[cell viewWithTag:102];
    
    switch (listType) {
            
        case RECEIVER_LIST_TYPE_BOARD: //BoardSelection
        {
            if (indexPath.row == 0) {
                
                titleLbl.text = TEXT_SELECT_ALL;
                
                if (isBoardSelectAll)
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_ON];
                else
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_OFF];
                
                
            } else {
            
                BoardModel *board = (BoardModel *)[boardListArr objectAtIndex:indexPath.row - 1];
                
                titleLbl.text = board.boardName;
                
                if (board.isSelected )
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_ON];
                else
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_OFF];

            }
            
            break;
        }
        case RECEIVER_LIST_TYPE_FILTER: //FilterSelection
        {
            ReceiverFilterListModel *filter = (ReceiverFilterListModel *)[filterListArr objectAtIndex:indexPath.row];
            
            titleLbl.text = filter.filterDescription;
            
            if (filter.isSelected )
                typeImgVw.image = [UIImage imageNamed:RADIO_BUTTON_ON];
            else
                typeImgVw.image = [UIImage imageNamed:RADIO_BUTTON_OFF];
            
            break;
        }
        case RECEIVER_LIST_TYPE_FILTEROPTION: //FilterOptionSelection
        {
            
            if (indexPath.row == 0) {
                
                titleLbl.text = TEXT_SELECT_ALL;
                
                if (isFilterOptionSelectAll)
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_ON];
                else
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_OFF];
                
                
            } else {
                ReceiverDetailsModel *filterOption = (ReceiverDetailsModel *)[filterOptinsListArr objectAtIndex:indexPath.row - 1];
                
                titleLbl.text = [NSString stringWithFormat:@"%@ - %@",filterOption.boardName,filterOption.receiverDescription];
                
                if (filterOption.isSelected)
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_ON];
                else
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_OFF];
            }
            
            break;
        }
        case RECEIVER_LIST_TYPE_RECEIPIENT: //ReceipientSelection
        {
            ReceiverReceipientListModel *receipient = (ReceiverReceipientListModel *)[receipientListArr objectAtIndex:indexPath.row];
            
            titleLbl.text = receipient.receipientDescription;
            
            if (receipient.isSelected )
                typeImgVw.image = [UIImage imageNamed:RADIO_BUTTON_ON];
            else
                typeImgVw.image = [UIImage imageNamed:RADIO_BUTTON_OFF];
            
            break;
        }
        case RECEIVER_LIST_TYPE_GROUP:
        {
            if (indexPath.row == 0) {
                
                titleLbl.text = TEXT_SELECT_ALL;
                
                if (isGroupSelectAll)
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_ON];
                else
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_OFF];
                
                
            } else {
                
                GroupModel *groupOption = (GroupModel *)[groupOptinsListArr objectAtIndex:indexPath.row - 1];
                
                titleLbl.text = [NSString stringWithFormat:@"%@ - %@",groupOption.boardName,groupOption.groupName];
                
                if (groupOption.isSelected )
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_ON];
                else
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_OFF];
            }
            
            break;
        }
        case RECEIVER_LIST_TYPE_CLASS:
        {
            if (indexPath.row == 0) {
                
                titleLbl.text = TEXT_SELECT_ALL;
                
                if (isClassSelectAll)
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_ON];
                else
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_OFF];
                
            } else {
                
                ClassesModel *class = (ClassesModel *)[classOptinsListArr objectAtIndex:indexPath.row - 1];
                
                titleLbl.text = [NSString stringWithFormat:@"%@ - %@",class.boardName,class.classStandardName];
                
                if (class.isSelected )
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_ON];
                else
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_OFF];
            }
            
            break;
        }
        case RECEIVER_LIST_TYPE_SECTION:
        {
            if (indexPath.row == 0) {
                
                titleLbl.text = TEXT_SELECT_ALL;
                
                if (isSectionSelectAll)
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_ON];
                else
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_OFF];
                
            } else {
                
                SectionModel *section = (SectionModel *)[sectionOptionsListArr objectAtIndex:indexPath.row - 1];
                
                titleLbl.text = [NSString stringWithFormat:@"%@ - %@ - %@",section.boardName,section.classStandardName,section.sectionName];
                
                if (section.isSelected )
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_ON];
                else
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_OFF];
            }
            
            break;
        }
        case RECEIVER_LIST_TYPE_STUDENT:
        {
            if (indexPath.row == 0) {
                
                titleLbl.text = TEXT_SELECT_ALL;
                
                if (isStudentSelectAll)
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_ON];
                else
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_OFF];
                
            } else {
                
                StudentModel *student = (StudentModel *)[studentListArr objectAtIndex:indexPath.row - 1];
                
                titleLbl.text = student.studentName;
                
                if (student.isSelected )
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_ON];
                else
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_OFF];
            }
            
            break;
        }
        case RECEIVER_LIST_TYPE_STAFF:
        {
            if (indexPath.row == 0) {
                
                titleLbl.text = TEXT_SELECT_ALL;
                
                if (isStaffSelectAll)
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_ON];
                else
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_OFF];
                
            } else {
                
                StaffModel *staff = (StaffModel *)[staffListArr objectAtIndex:indexPath.row - 1];
                
                titleLbl.text = staff.staffName;
                
                if (staff.isSelected )
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_ON];
                else
                    typeImgVw.image = [UIImage imageNamed:CHECKBOX_BUTTON_OFF];
            }
            
            break;
        }
    
        default:
            break;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    switch (listType) {
            
        case RECEIVER_LIST_TYPE_BOARD: //Board Selection
        {
            if (indexPath.row == 0) {//SelectAll Action
                
                isBoardSelectAll = !isBoardSelectAll;
                
                for (BoardModel *board in boardListArr) {
                    board.isSelected = isBoardSelectAll;
                    [_listTblVw reloadData];
                }
            } else {
                
                BoardModel *board = (BoardModel *)[boardListArr objectAtIndex:indexPath.row - 1];
                
                board.isSelected = !board.isSelected ;
                
                [_listTblVw reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            
            [self clearData];
            
            break;
        }
        case RECEIVER_LIST_TYPE_FILTER://Filter Selection
        {
            
            for (ReceiverFilterListModel *filter in filterListArr) {
                filter.isSelected = NO;
            }
            
            ReceiverFilterListModel *filter = (ReceiverFilterListModel *)[filterListArr objectAtIndex:indexPath.row];
            
            filter.isSelected = !filter.isSelected ;
            
            selectedFilterObj = filter;
            
            if (![filter.filterTypeId isEqualToString:FILTER_TYPE_NONE]) {
                
                _receipientVw.frame = CGRectMake(_receipientVw.frame.origin.x, _filterOptionSelectionVw.frame.origin.y + _filterOptionSelectionVw.frame.size.height, _receipientVw.frame.size.width, _receipientVw.frame.size.height);
                
                _filterOptionTiltleLbl.text = [NSString stringWithFormat:@"Select  %@",filter.filterDescription];
                
                _filterOptionSelectionLbl.text = [NSString stringWithFormat:@"No %@ Selected",filter.filterDescription];
                
                [_filterOptionSelectionLbl setTextColor:UNSELECTED_COLOR];
                
                isFilterOptionSelectionDone = NO;
                
                isFilterOptionSelectAll = NO;
                
                [self getReceiversForSelectedBoardTransId];
                
                
            }else {
                
                isFilterOptionSelectionDone = YES;
                
                _receipientVw.frame = CGRectMake(_receipientVw.frame.origin.x, _filterSelectionVw.frame.origin.y + _filterSelectionVw.frame.size.height, _receipientVw.frame.size.width, _receipientVw.frame.size.height);
                
            }
            
            [_listTblVw reloadData];
            
            break;
        }
        case RECEIVER_LIST_TYPE_FILTEROPTION://Filter Option Selection
        {
            
            if (indexPath.row == 0) {//SelectAll Action
                
                isFilterOptionSelectAll = !isFilterOptionSelectAll;
                
                for (ReceiverDetailsModel *filterOption in filterOptinsListArr) {
                    filterOption.isSelected = isFilterOptionSelectAll;
                    [_listTblVw reloadData];
                }
            }else
            {
                
                ReceiverDetailsModel *filterOption = (ReceiverDetailsModel *)[filterOptinsListArr objectAtIndex:indexPath.row - 1];
                
                filterOption.isSelected = !filterOption.isSelected;
                
                 [_listTblVw reloadData];
            }
            
            break;
            
        }
        case RECEIVER_LIST_TYPE_RECEIPIENT://Receipient Selection
        {
            
            for (ReceiverReceipientListModel *receipient in receipientListArr) {
                receipient.isSelected = NO;
            }
            
            ReceiverReceipientListModel *receipient = (ReceiverReceipientListModel *)[receipientListArr objectAtIndex:indexPath.row];
            
            receipient.isSelected = !receipient.isSelected;
            
            selectedReceipientObj = receipient;
            
            [self getStaffListForSelectedBoard];
            
            [self getClassForSelectedBoard];
            
            [_listTblVw reloadData];
            
            break;
        }
        case RECEIVER_LIST_TYPE_GROUP: //Receipient Group Option Selection
        {
            if (indexPath.row == 0) {//SelectAll Action
                
                isGroupSelectAll = !isGroupSelectAll;
                
                for (GroupModel *groupOption in groupOptinsListArr) {
                    groupOption.isSelected = isGroupSelectAll;
                    [_listTblVw reloadData];
                }
            }else
            {
                
                GroupModel *groupOption = (GroupModel *)[groupOptinsListArr objectAtIndex:indexPath.row - 1];
                
                groupOption.isSelected = !groupOption.isSelected;
                
                [_listTblVw reloadData];
            }
            
            break;

        }
        case RECEIVER_LIST_TYPE_CLASS: //Receipient Class Option Selection
        {
            if (indexPath.row == 0) {//SelectAll Action
                
                isClassSelectAll = !isClassSelectAll;
                
                for (ClassesModel *class in classOptinsListArr) {
                    class.isSelected = isClassSelectAll;
                    [_listTblVw reloadData];
                }
            }else
            {
                
                ClassesModel *class = (ClassesModel *)[classOptinsListArr objectAtIndex:indexPath.row - 1];
                
                class.isSelected = !class.isSelected;
                
                
                [_listTblVw reloadData];
            }
            
            [self getSectionForSelectedClass];
            
            break;
            
        }
        case RECEIVER_LIST_TYPE_SECTION: //Receipient Section Selection
        {
            if (indexPath.row == 0) {//SelectAll Action
                
                isSectionSelectAll = !isSectionSelectAll;
                
                for (SectionModel *section in sectionOptionsListArr) {
                    section.isSelected = isSectionSelectAll;
                    [_listTblVw reloadData];
                }
            }else
            {
                
                SectionModel *section = (SectionModel *)[sectionOptionsListArr objectAtIndex:indexPath.row - 1];
                
                section.isSelected = !section.isSelected;
                
                
                [_listTblVw reloadData];
            }
            
            break;
            
        }
        case RECEIVER_LIST_TYPE_STUDENT: //Receipient Student Selection
        {
            if (indexPath.row == 0) {//SelectAll Action
                
                isStudentSelectAll = !isStudentSelectAll;
                
                for (StudentModel *student in studentListArr) {
                    student.isSelected = isStudentSelectAll;
                    [_listTblVw reloadData];
                }
            }else
            {
                StudentModel *student = (StudentModel *)[studentListArr objectAtIndex:indexPath.row - 1];
                
                student.isSelected = !student.isSelected;
                
                
                [_listTblVw reloadData];
            }
            break;
        }
        case RECEIVER_LIST_TYPE_STAFF: //Receipient Staff Selection
        {
            if (indexPath.row == 0) {//SelectAll Action
                
                isStaffSelectAll = !isStaffSelectAll;
                
                for (StaffModel *staff in staffListArr) {
                    staff.isSelected = isStaffSelectAll;
                    [_listTblVw reloadData];
                }
            }else
            {
                StaffModel *staff = (StaffModel *)[staffListArr objectAtIndex:indexPath.row - 1];
                
                staff.isSelected = !staff.isSelected;
                
                
                [_listTblVw reloadData];
            }
            break;
        }




        default:
            break;
    }
    
    [self updateSelectionList];
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView;
    
    headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, tableView.tableHeaderView.frame.size.height)];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    
    UIButton *doneBtn = [UIButton buttonWithType: UIButtonTypeSystem];
    doneBtn.frame = CGRectMake(DeviceWidth - 100, 0, 100, 40);
    doneBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    doneBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 45, 0, 0);
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    [doneBtn addTarget:self action:@selector(onListDoneClk:) forControlEvents:UIControlEventTouchUpInside];
    doneBtn.titleLabel.font = [UIFont fontWithName:APP_FONT size:17];
    [doneBtn setTitleColor:APPTHEME_COLOR forState:UIControlStateNormal];
    
    //doneBtn.layer.borderColor = [UIColor greenColor].CGColor;
    //doneBtn.layer.borderWidth = 1;
    
    
    [headerView addSubview: selectLabel];
    [headerView addSubview:doneBtn];

    
    return headerView;
}

- (IBAction)onListDoneClk:(id)sender {
    
    //[self updateSelectedListCount];
    
    [self hideListTblVw];
    
}

#pragma mark - Service Call

- (void)fetchStudentListForSelectedSection{
    
    isStudentSelectionDone = NO;
    
    isStudentSelectAll = NO;
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *selectedSectionArr = [sectionOptionsListArr filteredArrayUsingPredicate:pred];
    
    NSMutableArray *sectionTransIdArr = [NSMutableArray new];
    
    for (SectionModel *section in selectedSectionArr) {
        
        [sectionTransIdArr addObject:section.sectionTransId];
        
    }
    
    NSDictionary *params = [NSDictionary dictionaryWithObject:sectionTransIdArr forKey:@"SectionList"];
    
    sectionTransIdArr = nil;
    
    
    NSString *url = @"MastersMServices/StudentsMService.svc/FetchStudentsByScectionList";
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:url baseURL:[USERDEFAULTS valueForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchStudentsByScectionListResult"][@"ResponseCode"] integerValue] == 1) {
                
                [studentListArr removeAllObjects];

                
                for (NSDictionary *tmpStudentDict in responseDictionary[@"FetchStudentsByScectionListResult"][@"StudentsMClass"]) {
                    
                    NSDictionary *studentDict = [tmpStudentDict dictionaryByReplacingNullsWithBlanks];
                    
                    StudentModel *student = [[StudentModel alloc] init];
                    
                    student.studentName = studentDict[@"StudentName"];
                    student.studentTransID = studentDict[@"TransID"];
                    student.enrollNo = [NSString stringWithFormat:@"%@", studentDict[@"EnrollNo"]];
                    student.classSection = studentDict[@"ClassSection"];
                    student.secondLangTransID = studentDict[@"SecondLangTransID"];
                    student.isSelected = NO;
                    
                    [studentListArr addObject:student];
                    
                    student = nil;

                }
                
                [studentListArr sortUsingComparator:^NSComparisonResult(StudentModel *obj1, StudentModel *obj2) {
                    
                    return [obj1.studentName compare:obj2.studentName];
                    
                }];
                
                [self showStudentSelectionList];
                
            }else {
                
                [SINGLETON_INSTANCE showAlertMessageWithTitle:@"No Students Found" andMessage:@"No student available for selected section"];
                
            }
            
        }
        
    }];
    
}


@end
