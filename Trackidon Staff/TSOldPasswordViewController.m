//
//  TSOldPasswordViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 29/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSOldPasswordViewController.h"

@interface TSOldPasswordViewController ()

@end

@implementation TSOldPasswordViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapOnScrollView = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOnScrollViewAction)];
    
    [_scrollView addGestureRecognizer:tapOnScrollView];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Change Password";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Methods

-(void) tapOnScrollViewAction{
    
    [self.view endEditing:YES];
    
    [_scrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - Touches Began method

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self tapOnScrollViewAction];
}

#pragma mark - Textfield Delegates

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if(IS_IPHONE_4){
        
        [_scrollView setContentOffset:CGPointMake(_scrollView.contentOffset.x, _scrollView.contentOffset.y+100)];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [_scrollView setContentOffset:CGPointZero animated:YES];
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Button Actions

- (IBAction)changePasswordButtonAction:(id)sender {
    
    if (_oldPasswordTextField.text.length>0) {
        
        [self.view endEditing:YES];
        
        TSNewPasswordViewController *newPasswordVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBNewPassword"];

        [newPasswordVC setOldPassword:_oldPasswordTextField.text];
        
        [self.navigationController pushViewController:newPasswordVC animated:YES];
    }
    else{
        
        [SINGLETON_INSTANCE showAlertMessageWithTitle:@"Information" andMessage:@"Password field cannot be empty"];
    }
    
}

@end