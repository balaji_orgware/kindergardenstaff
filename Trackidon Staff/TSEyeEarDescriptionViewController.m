//
//  TSEyeEarDescriptionViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 20/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSEyeEarDescriptionViewController.h"

@interface TSEyeEarDescriptionViewController ()

@end

@implementation TSEyeEarDescriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Prevents from overTapping from 6,6P devices.
    self.view.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);
    
    _dateView.frame = CGRectMake(_dateView.frame.origin.x, _dateView.frame.origin.y, DeviceWidth, _dateView.frame.size.height);
    
    _descView.frame = CGRectMake(_descView.frame.origin.x, _descView.frame.origin.y, DeviceWidth, _descView.frame.size.height);
    
    [_backView setFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    
    [_backView setAlpha:0.5];
    
    UITapGestureRecognizer *tapOnBackView = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOnBackViewAction)];
    
    [_backView addGestureRecognizer:tapOnBackView];
    
    [TSSingleton layerDrawForView:_dateView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Helper Methods

-(void)tapOnBackViewAction{
    
    [self hideAnimationView];
}

-(void) loadViewWithData:(id)modelObj withModuleType:(NSInteger)moduleType{
    
    switch (moduleType) {
        case HealthType_Eyes:
        {
            EyeInfoModel *eyeObj = (EyeInfoModel *)modelObj;
            
            _dateLabel.text = [SINGLETON_INSTANCE stringFromDate:eyeObj.examinedDate withFormate:DATE_FORMATE_MNTH_DAY_YR];
            _examinedByNameLbl.text = eyeObj.examinedBy;
            
            _leftEyeTitleLbl.attributedText = [self attributedStringWithTitle:eyeObj.leftEyeTitle withRightSideValue:@"in Left Eye"];
            _leftEyeTxtVw.text = eyeObj.leftEyeRemarks;
            
            _rightEyeTitleLbl.attributedText = [self attributedStringWithTitle:eyeObj.rightEyeTitle withRightSideValue:@"in Right Eye"];
            _rightEyeTxtVw.text = eyeObj.rightEyeRemarks;
            
            _leftEyeTxtVw.font = [UIFont fontWithName:APP_FONT size:14];
            _rightEyeTxtVw.font = [UIFont fontWithName:APP_FONT size:14];
            
            [_leftEyeTxtVw scrollRangeToVisible:NSMakeRange(0, 0)];
            [_rightEyeTxtVw scrollRangeToVisible:NSMakeRange(0, 0)];
            
            break;
        }

        case HealthType_Ears:
        {
            EarInfoModel *earObj = (EarInfoModel *)modelObj;
            
            _dateLabel.text = [SINGLETON_INSTANCE stringFromDate:earObj.examinedDate withFormate:DATE_FORMATE_MNTH_DAY_YR];
            _examinedByNameLbl.text = earObj.examinedBy;
            
            _leftEyeTitleLbl.attributedText = [self attributedStringWithTitle:earObj.leftEarTitle withRightSideValue:@"in Left Ear"];
            _leftEyeTxtVw.text = earObj.leftEarRemarks;
            
            _rightEyeTitleLbl.attributedText = [self attributedStringWithTitle:earObj.rightEarTitle withRightSideValue:@"in Right Ear"];
            _rightEyeTxtVw.text = earObj.rightEarRemarks;
            
            _leftEyeTxtVw.font = [UIFont fontWithName:APP_FONT size:14];
            _rightEyeTxtVw.font = [UIFont fontWithName:APP_FONT size:14];
            
            [_leftEyeTxtVw scrollRangeToVisible:NSMakeRange(0, 0)];
            [_rightEyeTxtVw scrollRangeToVisible:NSMakeRange(0, 0)];
            
            break;
        }
            
        case HealthType_NoseAndThroat:
        {
            NoseThroatInfoModel *noseThroatObj = (NoseThroatInfoModel *)modelObj;
            
            _dateLabel.text = [SINGLETON_INSTANCE stringFromDate:noseThroatObj.examinedDate withFormate:DATE_FORMATE_MNTH_DAY_YR];
            _examinedByNameLbl.text = noseThroatObj.examinedBy;
            
            _leftEyeTitleLbl.attributedText = [self attributedStringWithTitle:noseThroatObj.throatTitle withRightSideValue:@"in Throat"];
            _leftEyeTxtVw.text = noseThroatObj.throatDescription;
            
            _rightEyeTitleLbl.attributedText = [self attributedStringWithTitle:noseThroatObj.noseTitle withRightSideValue:@"in Nose"];
            _rightEyeTxtVw.text = noseThroatObj.noseDescription;
            
            _leftEyeTxtVw.font = [UIFont fontWithName:APP_FONT size:14];
            _rightEyeTxtVw.font = [UIFont fontWithName:APP_FONT size:14];
            
            [_leftEyeTxtVw scrollRangeToVisible:NSMakeRange(0, 0)];
            [_rightEyeTxtVw scrollRangeToVisible:NSMakeRange(0, 0)];
            
            break;
        }
            
        default:
            break;
    }
}

-(NSAttributedString *)attributedStringWithTitle:(NSString *)titleText withRightSideValue:(NSString *)redText{
    
    NSString *string = [NSString stringWithFormat:@"%@ %@",titleText,redText];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:string];
    
    NSDictionary *titleTextPropDict = @{NSFontAttributeName: [UIFont fontWithName:APP_FONT size:21],NSForegroundColorAttributeName:[UIColor blackColor]};
    
    NSDictionary *redTextPropDict = @{NSFontAttributeName:[UIFont fontWithName:APP_FONT size:15],NSForegroundColorAttributeName:COLOR_DECLINED_DUESTODAY};
    
    [attributedString addAttributes:titleTextPropDict range:[string rangeOfString:titleText]];
    
    [attributedString addAttributes:redTextPropDict range:[string rangeOfString:redText]];
    
    return attributedString;
}

-(void) showDescViewController:(UIViewController *)viewController withModuleType:(NSInteger)moduleType andModelObj:(id)modelObj{
    
    [viewController addChildViewController:self];
    
    [viewController.view addSubview:self.view];
    
    [self didMoveToParentViewController:viewController];
    
    [viewController.view endEditing:YES];
    
    [self loadViewWithData:modelObj withModuleType:moduleType];
    
    self.descView.frame = CGRectMake(self.descView.frame.origin.x, DeviceHeight, viewController.view.frame.size.width, viewController.view.frame.size.height*3.3/4);
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [_backView setAlpha:0.5];
        
        self.descView.frame = CGRectMake(viewController.view.frame.origin.x, viewController.view.frame.size.height - self.descView.frame.size.height, viewController.view.frame.size.width, self.descView.frame.size.height);
    }];
}

-(void) hideAnimationView{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [_backView setAlpha:0.0];
        _descView.frame = CGRectMake(_descView.frame.origin.x, DeviceHeight, _descView.frame.size.width, _descView.frame.size.height);
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

@end
