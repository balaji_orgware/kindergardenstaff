//
//  PushCount.m
//  Trackidon Staff
//
//  Created by Elango on 19/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "PushCount.h"
#import "StaffDetails.h"

@implementation PushCount

+ (instancetype)instance{
    
    static PushCount *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (&onceToken, ^{
        
        kSharedInstance = [[PushCount alloc]init];
        
    });
    
    return kSharedInstance;
}

- (void)updatePushCountsFormenuId:(NSString *)menuId isForApproval:(BOOL)isForApproval{
    
    NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
    
    NSFetchRequest *fetchRequest = [NSFetchRequest new];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"PushCount" inManagedObjectContext:context];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransId = %@ AND commTypeID = %@",SINGLETON_INSTANCE.staff.staffTransId, menuId];
    
    [fetchRequest setEntity:entityDesc];
    
    [fetchRequest setPredicate:predicate];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    NSError *error;
    
    NSArray *array = [context executeFetchRequest:fetchRequest error:&error];
    
    if (array.count > 0) {
        
        PushCount *pushCountObj = [array objectAtIndex:0];
        
        pushCountObj.isRead = [NSNumber numberWithBool:YES];

        if (isForApproval) {
         
            pushCountObj.unreadCount = @"0";
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSError *error;
        
        if (![context save:&error]) {
            
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        
    });
}

- (void)savePushCountsWithDict:(NSDictionary *)dict{
    
    @try {
        NSManagedObjectContext *cntx = [APPDELEGATE_INSTANCE managedObjectContext];
        
        [SINGLETON_INSTANCE deleteAllObjectsInEntity:@"PushCount"];
        
        for (NSDictionary *dictionary in dict[@"FetchPushCountbyAccountIDResult"][@"PushCounts"]) {
            
            PushCount *pushCountObj = [NSEntityDescription insertNewObjectForEntityForName:@"PushCount" inManagedObjectContext:cntx];
            
            pushCountObj.isForApproval = [NSNumber numberWithBool:NO];
            
            pushCountObj.isRead = [NSNumber numberWithBool:NO];
            
            pushCountObj.commTypeID = [NSString stringWithFormat:@"%@",dictionary[@"CommunicationTypeID"]];
            
            pushCountObj.transId = dictionary[@"TransID"];
            
            pushCountObj.unreadCount = [NSString stringWithFormat:@"%@",dictionary[@"PushCount"]];
            
            pushCountObj.staffTransId = dictionary[@"AccountTransID"];
            
            pushCountObj.staff = SINGLETON_INSTANCE.staff;
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError *error;
            
            if (![cntx save:&error]) {
                
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            
        });
        
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    }
    
}

- (void)saveUpdatedPushCountsWithDict:(NSDictionary *)dict isForApproval:(BOOL)isForApproval{
    
    @try {
        
        NSManagedObjectContext *cntx = [APPDELEGATE_INSTANCE  managedObjectContext];
        
        NSPredicate *deletePred = [NSPredicate predicateWithFormat:@"isForApproval == %@",[NSNumber numberWithBool:isForApproval]];
        
        [SINGLETON_INSTANCE deleteSelectedObjectsInEntityWithEntityName:@"PushCount" withPredicate:deletePred withContext:[APPDELEGATE_INSTANCE managedObjectContext]];
        
        if (isForApproval) {
            
            NSDictionary *countDict = dict[@"FetchUnApprovedCommunicationResult"][@"UnApprovalCount"];
            
            for (NSString *value in [countDict allKeys]) {
                
                PushCount *pushCountObj = [NSEntityDescription insertNewObjectForEntityForName:@"PushCount" inManagedObjectContext:cntx];
                
                pushCountObj.isForApproval = [NSNumber numberWithBool:isForApproval];
                
                pushCountObj.isRead = [NSNumber numberWithBool:NO];
                
                if ([value isEqualToString:@"AssignmentCount"]) {
                    
                    pushCountObj.commTypeID = [NSString stringWithFormat:@"%ld",(long)MENU_ASSIGNMENT_APPROVALS];
                }
                else if([value isEqualToString:@"EventsMCount"]){
                    
                    pushCountObj.commTypeID = [NSString stringWithFormat:@"%ld",(long)MENU_EVENT_APPROVALS];
                }
                else if([value isEqualToString:@"PortionsCount"]){
                    
                    pushCountObj.commTypeID = [NSString stringWithFormat:@"%ld",(long)MENU_PORTION_APPROVALS];
                }
                else if([value isEqualToString:@"SchoolNotificationCount"]){
                    
                    pushCountObj.commTypeID = [NSString stringWithFormat:@"%ld",(long)MENU_NOTIFICATION_APPROVALS];
                }
                
                pushCountObj.unreadCount = [NSString stringWithFormat:@"%@",[countDict valueForKey:value]];
                
                pushCountObj.staffTransId = SINGLETON_INSTANCE.staffTransId;
                
                pushCountObj.staff = SINGLETON_INSTANCE.staff;
            }
        }
        else{
        
            for (NSDictionary *dictionary in dict[@"ResetPushCountResult"][@"PushCounts"]) {
                
                PushCount *pushCountObj = [NSEntityDescription insertNewObjectForEntityForName:@"PushCount" inManagedObjectContext:cntx];
                
                pushCountObj.isForApproval = [NSNumber numberWithBool:NO];
                
                pushCountObj.isRead = [NSNumber numberWithBool:NO];
                
                pushCountObj.commTypeID = [NSString stringWithFormat:@"%@",dictionary[@"CommunicationTypeID"]];
                
                pushCountObj.transId = dictionary[@"TransID"];
                
                pushCountObj.unreadCount = [NSString stringWithFormat:@"%@",dictionary[@"PushCount"]];
                
                pushCountObj.staffTransId = dictionary[@"AccountTransID"];
                
                pushCountObj.staff = SINGLETON_INSTANCE.staff;
                
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError *error;
            
            if (![cntx save:&error]) {
                
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            
        });

    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    }
}
@end
