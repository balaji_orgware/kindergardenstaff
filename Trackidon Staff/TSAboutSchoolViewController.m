//
//  TSAboutSchoolViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/2/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSAboutSchoolViewController.h"

@interface TSAboutSchoolViewController ()

@end

@implementation TSAboutSchoolViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [TSAnalytics logEvent:ANALYTICS_ABOUTUS_OPENED parameter:[NSMutableDictionary new]];
    
    [self fetchSchoolDetails];
    
    UITapGestureRecognizer *contactViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contactViewTapped)];
    
    [_contactView addGestureRecognizer:contactViewTap];
    
    UITapGestureRecognizer *emailViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(emailViewTapped)];
    
    [_emailView addGestureRecognizer:emailViewTap];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"About Us";

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_ABOUTUS parameter:[NSMutableDictionary new]];
    
}


#pragma mark - Methods

-(void) arrangeViewsAndDescString:(NSString *)string{
    
    [_descriptionTxtView setFont:[UIFont fontWithName:APP_FONT size:14.0f]];
    
    CGSize newSize = [_descriptionTxtView sizeThatFits:CGSizeMake(_descriptionTxtView.frame.size.width, MAXFLOAT)];
    
    _descriptionTxtView.scrollEnabled = NO;
    
    _descriptionTxtView.frame = CGRectMake(_descriptionTxtView.frame.origin.x, _descriptionTxtView.frame.origin.y, _descriptionTxtView.frame.size.width, newSize.height);
    
    _contactView.frame = CGRectMake(_contactView.frame.origin.x, _descriptionTxtView.frame.origin.y + _descriptionTxtView.frame.size.height + 20, _contactView.frame.size.width, _contactView.frame.size.height);
    
    _emailView.frame = CGRectMake(_emailView.frame.origin.x, _contactView.frame.origin.y+_contactView.frame.size.height + 20, _emailView.frame.size.width, _emailView.frame.size.height);
    
    
    _aboutScrollView.contentSize = CGSizeMake(DEVICE_SIZE.width, _emailView.frame.origin.y + 50 +30);
    
}

-(void)fetchSchoolDetails{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"schoolTransId MATCHES %@",SINGLETON_INSTANCE.selectedSchoolTransId];
        
    NSArray *schoolArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"School" withPredicate:predicate];
    
    if (schoolArray.count > 0) {
        
        School *schoolObj = [schoolArray objectAtIndex:0];
        
        if (schoolObj.schoolLogoData == NULL)
            _schoolLogoImgView.image = [UIImage imageNamed:@"no_image"];
        
        else{
            _schoolLogoImgView.image = [UIImage imageWithData:schoolObj.schoolLogoData];
        }
        
        _descriptionTxtView.text = [NSString stringWithFormat:@"\t%@",schoolObj.schoolAboutUs];
        
        _emailLabel.text = schoolObj.schoolEmailID;
        
        _contactNoLabelFirst.text = schoolObj.schoolContactNo;
        
        _schoolNameLbl.text = [NSString stringWithFormat:@"Welcome to %@",schoolObj.schoolName];
        
        [self arrangeViewsAndDescString:_descriptionTxtView.text];
        
    }
    
}

-(void) contactViewTapped{
    
    [TSAnalytics logEvent:ANALYTICS_ABOUTUS_CLICKEDPHONE parameter:[NSMutableDictionary new]];
    
    [self callContactWithNo:[_contactNoLabelFirst.text stringByReplacingOccurrencesOfString:@" " withString:@""]];;
    
}

-(void) emailViewTapped{
    
    [TSAnalytics logEvent:ANALYTICS_ABOUTUS_CLICKEDMAIL parameter:[NSMutableDictionary new]];
    
    MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc]init];
    
    if ([MFMailComposeViewController canSendMail]) {
        
        mailComposer.mailComposeDelegate = self;
                
        [mailComposer.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor blackColor]}];
        
        [mailComposer setToRecipients:[NSArray arrayWithObjects:_emailLabel.text, nil]];
        
        [mailComposer setSubject:[NSString stringWithFormat:@"Report from %@",[USERDEFAULTS objectForKey:USERNAME]]];
        
        [self presentViewController:mailComposer animated:YES completion:nil];
    }
}

-(void) callContactWithNo:(NSString *)number{
    
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",number]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
        
    } else{
        
        [TSSingleton showAlertWithMessage:@"Call facility is not available!!!"];
    }
}

#pragma mark - Mail Composer Delegate

-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    if (result == MFMailComposeResultSent){
        
        NSLog(@"\n\n Email Sent");
        
    }
    
    if([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)])
        [self dismissViewControllerAnimated:YES completion:nil];
}

@end