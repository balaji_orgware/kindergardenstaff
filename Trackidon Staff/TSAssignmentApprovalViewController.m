//
//  TSAssignmentApprovalViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 05/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSAssignmentApprovalViewController.h"

@interface TSAssignmentApprovalViewController (){
    
    NSMutableArray *assignmentArray,*assignmentModelArray;
    
    UIBarButtonItem *selectAllBtn;
    
    UILongPressGestureRecognizer *longPressGesture;
    
    TSSelectionViewController *selectionVC;
    
    NSArray *attachmentImgArray;

}

@end

@implementation TSAssignmentApprovalViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    attachmentImgArray = @[@"one",@"two",@"three",@"four",@"five"];
    
    assignmentArray = [NSMutableArray new];
    
    assignmentModelArray = [NSMutableArray new];
    
    
    selectAllBtn = [[UIBarButtonItem alloc]initWithTitle:@"Select All" style:UIBarButtonItemStyleDone target:self action:@selector(selectAllAction)];
    
    longPressGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressGestureAction:)];
    
    longPressGesture.delegate = self;
    
    [self.assignmentsTableView addGestureRecognizer:longPressGesture];
    
    [self.assignmentsTableView setTableFooterView:[UIView new]];
    
    self.selectionView.frame = CGRectMake(self.selectionView.frame.origin.x, DeviceHeight+64, self.selectionView.frame.size.width, self.selectionView.frame.size.height);
    
    [self categorizeAssignments];
    
    [TSAnalytics logEvent:ANALYTICS_ASSIGNMENTAPPROVALS_OPENED_APPROVALS parameter:[NSMutableDictionary new]];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_APPROVALASSIGNMENT_LIST parameter:[NSMutableDictionary new]];
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@ Log",SINGLETON_INSTANCE.barTitle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button Actions

- (IBAction)approvedButtonTapped:(id)sender {
    
    NSPredicate *approvePredicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *approvedArray = [assignmentModelArray filteredArrayUsingPredicate:approvePredicate];
    
    if (approvedArray.count>0) {
        
        UIAlertView *approveAlertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:[NSString stringWithFormat:@"Do you want to Approve these %ld assignments?",(unsigned long)approvedArray.count] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        
        approveAlertView.tag = APPROVAL_TYPE_APPROVE;
        
        approveAlertView.delegate = self;
        
        [approveAlertView show];
    }
}

- (IBAction)declineButtonTapped:(id)sender {
    
    NSPredicate *declinePredicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *declinedArray = [assignmentModelArray filteredArrayUsingPredicate:declinePredicate];
    
    if (declinedArray.count>0) {
        
        UIAlertView *declineAlertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:[NSString stringWithFormat:@"Do you want to Decline these %ld assignments?",(unsigned long)declinedArray.count] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        
        declineAlertView.tag = APPROVAL_TYPE_DECLINE;
        
        declineAlertView.delegate = self;
        
        [declineAlertView show];
    }
}

#pragma mark - Alert View Delegates

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == APPROVAL_TYPE_APPROVE) {
        
        if (buttonIndex) {
            
            NSPredicate *approvePredicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *approvedArray = [assignmentModelArray filteredArrayUsingPredicate:approvePredicate];
            
            NSMutableArray *assignmentsTransIDArr = [NSMutableArray new];
            
            for (ApprovalsModel *modelObj in approvedArray) {
                
                [assignmentsTransIDArr addObject:modelObj.transID];
            }
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidUserTransID"];
            
            [param setValue:[NSString stringWithFormat:@"%ld",(long)APPROVAL_TYPE_APPROVE] forKey:@"intApprovedStatus"];
            
            [param setValue:assignmentsTransIDArr forKey:@"guidAssignmentTransID"];
            
            NSDictionary *updateParam = @{@"ApproveAssignments":param};
            
            [self updateAssignmentsWithParam:updateParam withApprovalType:APPROVAL_TYPE_APPROVE];
        }
    }
    else if (alertView.tag == APPROVAL_TYPE_DECLINE){
        
        if (buttonIndex) {
            
            NSPredicate *declinePredicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
            
            NSArray *declinedArray = [assignmentModelArray filteredArrayUsingPredicate:declinePredicate];
            
            NSMutableArray *assignmentsTransIDArr = [NSMutableArray new];
            
            for (ApprovalsModel *modelObj in declinedArray) {
                
                [assignmentsTransIDArr addObject:modelObj.transID];
            }
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidUserTransID"];
            
            [param setValue:[NSString stringWithFormat:@"%ld",(long)APPROVAL_TYPE_DECLINE] forKey:@"intApprovedStatus"];
            
            [param setValue:assignmentsTransIDArr forKey:@"guidAssignmentTransID"];
            
            NSDictionary *updateParam = @{@"ApproveAssignments":param};
            
            [self updateAssignmentsWithParam:updateParam withApprovalType:APPROVAL_TYPE_DECLINE];
        }
    }
    
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}


#pragma mark - Helper methods

// Select All Button Action
-(void) selectAllAction{
    
    for (ApprovalsModel *modelObj in assignmentModelArray) {
        
        modelObj.isSelected = YES;
    }
    
    self.navigationItem.title = [NSString stringWithFormat:@"%ld   Selected",(unsigned long)assignmentModelArray.count];
    
    [self.assignmentsTableView reloadData];
}

// Method to Show/hide Selection View
-(void) animateSelectionView{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSelected == 1"];
    
    NSArray *filteredArray = [assignmentModelArray filteredArrayUsingPredicate:predicate];
    
    if (filteredArray.count>0) {
        
        NSLog(@"%@",self.selectionView);
        
        if(self.selectionView.frame.origin.y >= DeviceHeight){
            
            [UIView animateWithDuration:0.2f animations:^{
                
                self.assignmentsTableView.frame = CGRectMake(self.assignmentsTableView.frame.origin.x, self.assignmentsTableView.frame.origin.y, self.assignmentsTableView.frame.size.width, DeviceHeight - 64 - self.selectionView.frame.size.height);
                
                self.selectionView.frame = CGRectMake(self.selectionView.frame.origin.x, self.assignmentsTableView.frame.origin.y+self.assignmentsTableView.frame.size.height, self.selectionView.frame.size.width, self.selectionView.frame.size.height);
            }];
        }
        
        self.navigationItem.rightBarButtonItem = selectAllBtn;
        
        self.navigationItem.title = [NSString stringWithFormat:@"%ld   Selected",(unsigned long)filteredArray.count];
    }
    else{
        
        self.navigationItem.rightBarButtonItem = nil;
        
        self.navigationItem.title = @"Assignment Approval";
        
        if (self.selectionView.frame.origin.y < DeviceHeight) {
            
            [UIView animateWithDuration:0.2f animations:^{
                
                self.assignmentsTableView.frame = CGRectMake(self.assignmentsTableView.frame.origin.x, self.assignmentsTableView.frame.origin.y, self.assignmentsTableView.frame.size.width, DeviceHeight-64);
                
                self.selectionView.frame = CGRectMake(self.selectionView.frame.origin.x, DeviceHeight, self.selectionView.frame.size.width, self.selectionView.frame.size.height);
            }];
        }
    }
}

// Long press Gesture Action
-(void)longPressGestureAction:(UILongPressGestureRecognizer *)sender{
    
    CGPoint touchPoint = [sender locationOfTouch:0 inView:self.assignmentsTableView];
    
    NSIndexPath *indexPath = [self.assignmentsTableView indexPathForRowAtPoint:touchPoint];
    
    if (indexPath != nil && sender.state == UIGestureRecognizerStateBegan) {
        
        ApprovalsModel *modelObj = [assignmentModelArray objectAtIndex:indexPath.row];
        
        modelObj.isSelected = !modelObj.isSelected;
        
        [self.assignmentsTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        [self animateSelectionView];
    }
    
}

// Post Notification Method to Update Tableview from TSEventApprovalDescViewController
-(void) updateTableView{
    
    [self categorizeAssignments];
    
//    [self fetchUnapprovedAssignmentsAPICall:NO];
}

// Method to Clear Approve/Decline Selection View
-(void) clearSelectionView{
    
    [UIView animateWithDuration:0.2f animations:^{
        
        self.navigationItem.title = @"Assignment Approval";
        
        self.navigationItem.rightBarButtonItem = nil;

        self.assignmentsTableView.frame = CGRectMake(self.assignmentsTableView.frame.origin.x, self.assignmentsTableView.frame.origin.y, self.assignmentsTableView.frame.size.width, DeviceHeight-64);
        
        self.selectionView.frame = CGRectMake(self.selectionView.frame.origin.x, DeviceHeight, self.selectionView.frame.size.width, self.selectionView.frame.size.height);
        
        self.navigationItem.rightBarButtonItem = nil;
    }];
}
/*
#pragma mark - Selection View

-(void) showSelectionViewWithSelectedText:(NSString *)selected andTextField:(UITextField *)textField{
    
    if (selectionVC == nil) {
        
        selectionVC = [[TSSelectionViewController alloc]initWithNibName:@"TSSelectionViewController" bundle:nil];
        
        selectionVC.delegate = self;
        
        NSMutableDictionary *contentDict = [NSMutableDictionary new];
        
        for (Board *board in boardArray) {
            
            [contentDict setValue:board.boardTransId forKey:board.boardName];
        }
        
        [selectionVC setContentDict:contentDict];
        
        [selectionVC setSelectedValue:selected.length>0?selected:@""];
        
        [selectionVC showSelectionViewController:self forTextField:textField];
    }
    else{
        
        selectionVC = nil;
    }
}

// MARK: Selection Callback Delegate

-(void)selectedValueFromDict:(NSDictionary *)contentDict withSelectedValue:(NSString *)selectedValue andTransID:(NSString *)transID forTextField:(UITextField *)textField{
    
    selectedBoardTransID = transID;
    
    [self clearSelectionView];
    
    [self categorizeAssignments];
    
    [self fetchUnapprovedAssignmentsAPICall:YES];
    
    selectionVC = nil;
}

-(void)backViewTappedOn:(UIViewController *)viewController{
    
    selectionVC = nil;
}
*/
#pragma mark - Tableview Datasources & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return assignmentArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *sCellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier];
    
    UILabel *assignmentTitleLabel = (UILabel *)[cell viewWithTag:100];
    
    UILabel *assignmentStatusLabel = (UILabel *)[cell viewWithTag:101];
    
    UILabel *assignmentBoardLabel = (UILabel *)[cell viewWithTag:102];
    
    UIImageView *attachmentImgVw = (UIImageView *)[cell viewWithTag:103];

    Assignments *assignmentObj = [assignmentArray objectAtIndex:indexPath.row];
    
    assignmentTitleLabel.text = [assignmentObj.assignmentTitle capitalizedString];
        
    assignmentStatusLabel.text = [NSString stringWithFormat:@"Created by %@",assignmentObj.createdBy];
    
    assignmentBoardLabel.text = assignmentObj.boardName;
    
    ApprovalsModel *modelObj = [assignmentModelArray objectAtIndex:indexPath.row];
    
    if (!modelObj.isSelected) {
        
        [cell setBackgroundColor:[UIColor whiteColor]];
    }
    else{
        
        [cell setBackgroundColor:[UIColor lightGrayColor]];
    }
    
    [tableView setTableFooterView:[UIView new]];
    
    if ([assignmentObj.isAttachment intValue]==1) {
        
        attachmentImgVw.image = [UIImage imageNamed:[attachmentImgArray objectAtIndex:indexPath.row%5]];
        
        attachmentImgVw.hidden = NO;
    }
    else{
        
        attachmentImgVw.hidden = YES;
    }

    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [TSAnalytics logEvent:ANALYTICS_ASSIGNMENTAPPROVALS_CLICKED_APPROVALSINLIST parameter:[NSMutableDictionary new]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateTableView) name:@"updateTableView" object:nil];
    
    TSAssignmentApprovalDescViewController *assignmentApprovalDescVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAssignmentApprovalsDesc"];
    
    [assignmentApprovalDescVC setAssignmentObj:[assignmentArray objectAtIndex:indexPath.row]];
    
    
    
    [self.navigationController pushViewController:assignmentApprovalDescVC animated:YES];
}

#pragma mark - Categorize Events

-(void) categorizeAssignments{
    
    [assignmentArray removeAllObjects];
    
    [assignmentModelArray removeAllObjects];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@ AND isForApprovals == 1",SINGLETON_INSTANCE.staffTransId];
    
    assignmentArray = [NSMutableArray arrayWithArray:[SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Assignments" withPredicate:predicate]];
    
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:NO];
    
    assignmentArray = [[assignmentArray sortedArrayUsingDescriptors:@[dateDescriptor]] mutableCopy];
    
    for (Assignments *assignmentObj in assignmentArray) {
        
        ApprovalsModel *modelObj = [ApprovalsModel new];
        modelObj.title = assignmentObj.assignmentTitle;
        modelObj.transID = assignmentObj.transID;
        modelObj.isSelected = NO;
        modelObj.approvalStatus = [assignmentObj.approvedStatus integerValue];
        
        [assignmentModelArray addObject:modelObj];
    }
    
    NSLog(@"%@",assignmentArray);
    
    if(assignmentArray.count>0){
        
        [self.assignmentsTableView setHidden:NO];
    }
    else{
        
        [self.assignmentsTableView setHidden:YES];
    }
    
    [self.assignmentsTableView reloadData];
}


#pragma mark - Service Calls

/*
// Fetch Unapproved Assignments Service Call
-(void) fetchUnapprovedAssignmentsAPICall:(BOOL)animated{
    
    NSString *urlString = @"MastersMServices/AssignmentService.svc/FetchUnApprovedAssignments";
    
    NSDictionary *params = @{@"guidBoardTransID":selectedBoardTransID};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchUnApprovedAssignmentsResult"][@"ResponseCode"] intValue] == 1) {
                
                [[Assignments instance]storeAssignmentsDataForStaff:SINGLETON_INSTANCE.staff withDict:responseDictionary andIsForApprovals:YES withBoardTransID:selectedBoardTransID];
                
                [self categorizeAssignments];
            }
        }
    }];
}
 */

// Update Assignments Service Call
-(void) updateAssignmentsWithParam:(NSDictionary *)param withApprovalType:(NSInteger)approvalType{
    
    NSString *urlString = @"MastersMServices/AssignmentService.svc/MobileApproveAssignments";
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"MobileApproveAssignmentsResult"][@"ResponseCode"] integerValue] == 1) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                if (approvalType == APPROVAL_TYPE_APPROVE) {
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                else{
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                
                [analyticsDict setValue:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_ASSIGNMENTAPPROVALS_CLICKED_BULKAPPROVALS parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"MobileApproveAssignmentsResult"][@"ResponseMessage"]];
                
                NSArray *assignmentTransIDArr = param[@"ApproveAssignments"][@"guidAssignmentTransID"];
                
                for (NSString *assignmentTransID in assignmentTransIDArr) {
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@ AND isForApprovals == 1 AND transID MATCHES %@",SINGLETON_INSTANCE.staffTransId,assignmentTransID];
                    
                    [SINGLETON_INSTANCE deleteSelectedObjectsInEntityWithEntityName:@"Assignments" withPredicate:predicate withContext:[APPDELEGATE_INSTANCE managedObjectContext]];
                }
                
                [assignmentArray removeAllObjects];
                
                [assignmentModelArray removeAllObjects];
                
                [self clearSelectionView];
                
                [self categorizeAssignments];
            }
            else{
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                if (approvalType == APPROVAL_TYPE_APPROVE) {
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                else{
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                
                [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_ASSIGNMENTAPPROVALS_CLICKED_BULKAPPROVALS parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
            }
        }
        else{
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            if (approvalType == APPROVAL_TYPE_APPROVE) {
                
                [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
            }
            else{
                
                [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
            }
            
            [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
            
            [TSAnalytics logEvent:ANALYTICS_ASSIGNMENTAPPROVALS_CLICKED_BULKAPPROVALS parameter:analyticsDict];
        }
    }];
}
@end