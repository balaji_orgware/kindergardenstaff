//
//  StaffDetails.h
//  Trackidon Staff
//
//  Created by Elango on 16/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Portions,Notifications,Assignments,Events,School,ReceiverDetails,Feedback,ReceiverReceipientList,ReceiverFilterList,PortionInbox;


NS_ASSUME_NONNULL_BEGIN

@interface StaffDetails : NSManagedObject

+ (instancetype)instance;

- (void)saveStaffDetailsWithDict:(NSDictionary *)dict;

-(StaffDetails *)checkAvailablibilityWithParentTransId:(NSString *)staffTransId;

-(void)updateStaffImageWithData:(NSData *)imageData parentTransId:(NSString *)transID;

@end

NS_ASSUME_NONNULL_END

#import "StaffDetails+CoreDataProperties.h"
