//
//  Trust+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 14/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Trust+CoreDataProperties.h"

@implementation Trust (CoreDataProperties)

@dynamic addressOne;
@dynamic addressThree;
@dynamic addressTwo;
@dynamic baseUrl;
@dynamic contactFaxNo;
@dynamic contactMail;
@dynamic contactPerson;
@dynamic contactPhone;
@dynamic latitude;
@dynamic longtitude;
@dynamic pincode;
@dynamic schoolCode;
@dynamic schoolLogoData;
@dynamic schoolLogoUrl;
@dynamic schoolName;
@dynamic schoolTransId;
@dynamic startDate;
@dynamic trustName;
@dynamic trustTransId;

@end
