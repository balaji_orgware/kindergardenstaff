//
//  Timetable+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Balaji on 08/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Timetable+CoreDataProperties.h"

@implementation Timetable (CoreDataProperties)

@dynamic academicTransID;
@dynamic boardTransID;
@dynamic day;
@dynamic dayDescription;
@dynamic dayTransID;
@dynamic periodEight;
@dynamic periodEightTransID;
@dynamic periodFive;
@dynamic periodFiveTransID;
@dynamic periodFour;
@dynamic periodFourTransID;
@dynamic periodOne;
@dynamic periodOneTransID;
@dynamic periodSeven;
@dynamic periodSevenTransID;
@dynamic periodSix;
@dynamic periodSixTransID;
@dynamic periodThree;
@dynamic periodThreeTransID;
@dynamic periodTwo;
@dynamic periodTwoTransID;
@dynamic specialClass;
@dynamic specialClassTransID;
@dynamic staffTransID;
@dynamic transID;
@dynamic periodOneClassTransID;
@dynamic periodOneSecTransID;
@dynamic periodTwoClassTransID;
@dynamic periodTwoSecTransID;
@dynamic periodThreeClassTransID;
@dynamic periodThreeSecTransID;
@dynamic periodFourClassTransID;
@dynamic periodFourSecTransID;
@dynamic periodFiveClassTransID;
@dynamic periodFiveSecTransID;
@dynamic periodSixClassTransID;
@dynamic periodSixSecTransID;
@dynamic periodSevenClassTransID;
@dynamic periodSevenSecTransID;
@dynamic periodEightClassTransID;
@dynamic periodEightSecTransID;
@dynamic periodEightClass;
@dynamic periodEightSection;
@dynamic periodSevenClass;
@dynamic periodSevenSection;
@dynamic periodSixClass;
@dynamic periodSixSection;
@dynamic periodFiveClass;
@dynamic periodFiveSection;
@dynamic periodFourClass;
@dynamic periodFourSection;
@dynamic periodThreeClass;
@dynamic periodThreeSection;
@dynamic periodTwoClass;
@dynamic periodTwoSection;
@dynamic periodOneClass;
@dynamic periodOneSection;
@dynamic specialClassClassTransID;
@dynamic specialClassSecTransID;
@dynamic specialClassClass;
@dynamic specialClassSec;
@dynamic staff;

@end
