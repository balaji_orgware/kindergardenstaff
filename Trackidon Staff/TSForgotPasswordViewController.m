//
//  TSForgotPasswordViewController.m
//  Trackidon Staff
//
//  Created by ORGware on 16/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSForgotPasswordViewController.h"

@interface TSForgotPasswordViewController ()

@end

@implementation TSForgotPasswordViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _usernameTxtFld.layer.cornerRadius = _usernameTxtFld.frame.size.height/2;
    
    if (IS_IPHONE_4) {
        //
        //            _forgotPwdContainerVw.frame = CGRectMake(0, 0,DeviceWidth, 320);
        //            _forgotPwdFieldsVw.center = _forgotPwdContainerVw.center;
        //            _logoView.frame = CGRectMake(0, _forgotPwdContainerVw.frame.size.height, DeviceWidth, DeviceHeight-_forgotPwdContainerVw.frame.size.height);
        //        }
        //
        //        _usernameTxtFld.layer.cornerRadius = 20.0;
        //        _usernameTxtFld.layer.masksToBounds = YES;
        //
        //        _getButton.center = CGPointMake(self.view.center.x, _logoView.frame.origin.y);
        
        _forgotPwdContainerVw.frame = CGRectMake(_forgotPwdContainerVw.frame.origin.x,40, _forgotPwdContainerVw.frame.size.width, _forgotPwdContainerVw.frame.size.height);
        
        _logoView.frame = CGRectMake(_logoView.frame.origin.x, _forgotPwdContainerVw.frame.size.height+45, _logoView.frame.size.width,[UIScreen mainScreen].bounds.size.height - _forgotPwdContainerVw.frame.size.height-45);
        
        _getButton.frame = CGRectMake((_logoView.frame.size.width/2) -30, _logoView.frame.origin.y - 30, 60, 60);
        
        
    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_FORGOTPASSWORD parameter:[NSMutableDictionary new]];
    
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [_logoView setHidden:YES];
}

- (void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
    
}

#pragma mark - Button Action

- (IBAction)getButtonAction:(id)sender {
    
    if (_usernameTxtFld.text.length > 0) {
        
        if (_usernameTxtFld.text.length == 10) { //***** Verify the length for finding whether it is Mobile no. or not *****//
            
            NSScanner *scanner = [NSScanner scannerWithString:_usernameTxtFld.text];
            
            BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
            
            if (isNumeric) {
                
                NSMutableDictionary *param = [NSMutableDictionary dictionary];
                
                [param setValue:_usernameTxtFld.text forKey:@"strUserName"];
                
                [param setValue:@"9" forKey:@"intTypeID"];
                
                [self forgotPasswordServiceWithURL:@"MastersMServices/AccountsMService.svc/ForgotPassword" params:param];
                
            }else{
                
                if ([SINGLETON_INSTANCE isValidEmailAddress:_usernameTxtFld.text]){ //***** Email Id validation *****//
                    
                    NSMutableDictionary *param = [NSMutableDictionary dictionary];
                    
                    [param setValue:_usernameTxtFld.text forKey:@"strUserName"];
                    
                    [param setValue:@"9" forKey:@"intTypeID"];
                    
                    [self forgotPasswordServiceWithURL:@"MastersMServices/AccountsMService.svc/ForgotPassword" params:param];
                    
                }else{
                    
                    [TSSingleton showAlertWithMessage:@"Invalid EmailID / Mobile No."];
                    
                }
                
            }
            
        }else{//***** More than 10 characters was typed *****//
            
            NSLog(@"!=10");
            
            if ([SINGLETON_INSTANCE isValidEmailAddress: _usernameTxtFld.text]){
                
                NSMutableDictionary *param = [NSMutableDictionary dictionary];
                
                [param setValue:_usernameTxtFld.text forKey:@"strUserName"];
                
                [param setValue:@"9" forKey:@"intTypeID"];
                
                [self forgotPasswordServiceWithURL:@"MastersMServices/AccountsMService.svc/ForgotPassword" params:param];
                
            }else{
                
                [TSSingleton showAlertWithMessage:@"Invalid EmailID / Mobile No."];
                
            }
        }
        
    }else{
        
        [TSSingleton showAlertWithMessage:@"Please enter Mobile No. or Email ID"];
        
        /*CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
         
         animation.keyPath = @"position.x";
         
         animation.values = @[ @0, @10, @-10, @10, @0 ];
         
         animation.keyTimes = @[ @0, @(1 / 6.0), @(3 / 6.0), @(5 / 6.0), @1 ];
         
         animation.duration = 0.4;
         
         animation.additive = YES;
         
         [_emailIDTxtField.layer addAnimation:animation forKey:@"shake"];*/
    }
}

- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Textfield Delegates

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Service Call

//***** Forgot Password Service *****//

-(void) forgotPasswordServiceWithURL:(NSString *)url params:(NSMutableDictionary *)param{
    
    [[TSHandlerClass sharedHandler] postRequestWithURL:url baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            NSLog(@"%@",responseDictionary);
            
            if ([responseDictionary[@"ForgotPasswordResult"][@"ResponseCode"] intValue] == 1) {
                
                NSMutableDictionary *analyticDict = [NSMutableDictionary new];
                
                [analyticDict setObject:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_RESPONSE];
                
                [analyticDict setObject:responseDictionary[@"ForgotPasswordResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                
                [TSAnalytics logEvent:ANALYTICS_LOGIN_SUBMIT_FORGOT_PASSWORD parameter:analyticDict];
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"ForgotPasswordResult"][@"ResponseMessage"]];
                
                [self.navigationController popViewControllerAnimated:YES];
            } else{
                
                NSMutableDictionary *analyticDict = [NSMutableDictionary new];
                
                [analyticDict setObject:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_RESPONSE];
                
                [analyticDict setObject:responseDictionary[@"ForgotPasswordResult"][@"ResponseMessage"] forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
                
                [TSAnalytics logEvent:ANALYTICS_LOGIN_SUBMIT_FORGOT_PASSWORD parameter:analyticDict];
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"ForgotPasswordResult"][@"ResponseMessage"]];
                
                //[TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
            }

        } else{
            
            NSMutableDictionary *analyticDict = [NSMutableDictionary new];
            
            [analyticDict setObject:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_RESPONSE];
            
            [analyticDict setObject:ANALYTICS_VALUE_SERVICEFAILURE_RESPONSEMSG forKey:ANALYTICS_PARAM_RESPONSEMESSAGE];
            
            [TSAnalytics logEvent:ANALYTICS_LOGIN_SUBMIT_FORGOT_PASSWORD parameter:analyticDict];
            
            //[TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
        }
    }];
}
@end