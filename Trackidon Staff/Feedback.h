//
//  Feedback.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/11/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StaffDetails;

NS_ASSUME_NONNULL_BEGIN

@interface Feedback : NSManagedObject

// Method to Get Static Instance
+(instancetype)instance;

// Method to Store Feedback to core data
-(void)storeFeedbacks:(StaffDetails *)staffObj withDict:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END

#import "Feedback+CoreDataProperties.h"
