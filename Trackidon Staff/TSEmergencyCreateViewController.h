//
//  TSEmergencyCreateViewController.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 18/08/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSEmergencyCreateViewController : UIViewController <UITextFieldDelegate,UITextViewDelegate,datePickerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *examineDateView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UIView *examinedByView;
@property (weak, nonatomic) IBOutlet UITextField *examinedByTxtFld;

@property (weak, nonatomic) IBOutlet UIView *emergencyTitleView;
@property (weak, nonatomic) IBOutlet UITextField *emergencyTitleTxtFld;

@property (weak, nonatomic) IBOutlet UITextView *emergencyRemarksTxtVw;

@property (weak, nonatomic) IBOutlet UIView *doneView;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

- (IBAction)doneButtonTapped:(id)sender;

@property (nonatomic,strong) NSDictionary *generalInfoDict;

@end
