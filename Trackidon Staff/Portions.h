//
//  Portions.h
//  Trackidon Staff
//
//  Created by Balaji on 22/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Portions : NSManagedObject

// Get Static Instance
+(instancetype) instance;

// Method to store Portions Data to Core Data
-(void) storePortionsForStaffObj:(StaffDetails *)staffObj withDict:(NSDictionary *)dictionary isForApprovals:(BOOL)isForApprovals;

// Method to store push notification portion
-(void) storePushNotificationPortionWithDict:(NSDictionary *)dictionary;
@end

NS_ASSUME_NONNULL_END

#import "Portions+CoreDataProperties.h"
