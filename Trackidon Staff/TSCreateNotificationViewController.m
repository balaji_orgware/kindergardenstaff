//
//  TSCreateNotificationViewController.m
//  Trackidon Staff
//
//  Created by Elango on 29/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

@implementation UITextField (custom)

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectMake(bounds.origin.x + 10, bounds.origin.y,
                      bounds.size.width - 20, bounds.size.height);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}
@end

#import "TSCreateNotificationViewController.h"

@interface TSCreateNotificationViewController ()<UITextFieldDelegate,UITextViewDelegate,AttachmentPikerDelegate>

@end

@implementation TSCreateNotificationViewController
{
    NSMutableDictionary *notificationDict;
    NSInteger notificationPriority;
    BOOL isHasNotificationAttachment;
    NSData *attachmentData;
    UIView *currentResponder;
    CGRect viewRect;
    CGRect currentVisibleRect;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self makeImageAspectFitForButton:_highPriorityBtn];
    [self makeImageAspectFitForButton:_mediumPriorityBtn];
    [self makeImageAspectFitForButton:_lowPriorityBtn];
    
    notificationDict = [NSMutableDictionary new];
    
    notificationPriority = 1;
    [_highPriorityBtn setSelected:YES];
    isHasNotificationAttachment = NO;
    
    
    [TSSingleton layerDrawForView:_titleTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_priorityVw position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_attachementTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_attachementTxtFld position:LAYER_TOP color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_receiverSelectionVw position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    [SINGLETON_INSTANCE addDoneButtonForTextView:_descriptionTxtVw];
    
    _descriptionTxtVw.text = @"Description";
    _descriptionTxtVw.textColor = APP_PLACEHOLDER_COLOR;
    _descriptionTxtVw.font = [UIFont fontWithName:APP_FONT size:17];
    _descriptionTxtVw.textContainerInset =
    UIEdgeInsetsMake(15,5,10,15); // top, left, bottom, right
    
    viewRect = self.view.frame;
    
    [_scrollVw setContentSize:CGSizeMake(DeviceWidth, _attachementTxtFld.frame.origin.y + _attachementTxtFld.frame.size.height + 60)];

    
}

- (void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_NOTIFICATIONCREATE parameter:[NSMutableDictionary new]];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Create Notification";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)makeImageAspectFitForButton:(UIButton*)button{
    
    [button setImage:[UIImage imageNamed:@"radio_on"] forState:UIControlStateSelected];
    
    [button setImage:[UIImage imageNamed:@"radio_off"] forState:UIControlStateNormal];
    
    button.imageView.contentMode=UIViewContentModeScaleAspectFit;
    
    //button.layer.backgroundColor = [UIColor greenColor].CGColor;
    
    //button.layer.borderWidth = 0.6;
    
    button.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    button.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    
    NSLog(@"%@",button.imageView);

}

#pragma mark - Button Action

- (IBAction)onPriorityBtnClk:(id)sender {
    
    for (id btnObj in _priorityVw.subviews) {
        
        if ([btnObj isKindOfClass:[UIButton class]]) {
            
            UIButton *button = (UIButton *)btnObj;
            
            [button setSelected:NO];
        }
    }
    
    UIButton *button = (UIButton *)sender;
    [button setSelected:YES];
    notificationPriority = [sender tag];
}

- (IBAction)onSelectReceiverClk:(id)sender {
    
    if ([_titleTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
        
        [TSSingleton showAlertWithMessage:TEXT_EMPTY_TITLE];
        
    } else if (_titleTxtFld.text.length> MAX_TITLE_LENGTH){
        
        [TSSingleton showAlertWithMessage:TEXT_TITLE_LENGTH_EXCEED];
        
    } else if ([_descriptionTxtVw.text isEqualToString:DESCRIPTION_TEXT])
    {
        [TSSingleton showAlertWithMessage:TEXT_EMPTY_DESCRIPTION];
        
    }else {
        
        [notificationDict removeAllObjects];
        
        NSMutableDictionary *analyticsDict = [NSMutableDictionary new];
        
        [notificationDict setValue:[NSString stringWithFormat:@"%ld",(long)notificationPriority] forKey:@"Priority"];
        
        [notificationDict setValue:_titleTxtFld.text forKey:@"NotificationTitle"];
        
        [notificationDict setValue:_descriptionTxtVw.text forKey:@"NotificationDescription"];
        
        
        [notificationDict setValue:[SINGLETON_INSTANCE stringFromDate:[NSDate date] withFormate:DATE_FORMATE_SERVICE_CALL] forKey:@"NotificationDate"];
        
        if (notificationPriority == 1) {
            
            [analyticsDict setValue:ANALYTICS_VALUE_HIGH forKey:ANALYTICS_PARAM_RECEIPIENT_PRIORITY];
            
        } else if (notificationPriority == 2){
            
            [analyticsDict setValue:ANALYTICS_VALUE_MEDIUM forKey:ANALYTICS_PARAM_RECEIPIENT_PRIORITY];
            
        }else {
            [analyticsDict setValue:ANALYTICS_VALUE_LOW forKey:ANALYTICS_PARAM_RECEIPIENT_PRIORITY];
        }
        
        
        if (isHasNotificationAttachment) {
            
            [analyticsDict setValue:ANALYTICS_VALUE_YES forKey:ANALYTICS_PARAM_ISHASATTACHMENT];

            
            [notificationDict setValue:@"true" forKey:@"IsAttachment"];
            
            [notificationDict setValue:[@"data:image/jpeg;base64,"stringByAppendingString:[[attachmentData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength] stringByReplacingOccurrencesOfString:@"\n" withString:@""]] forKey:@"Attachment"];
            
        }else{
            
            [analyticsDict setValue:ANALYTICS_VALUE_NO forKey:ANALYTICS_PARAM_ISHASATTACHMENT];

            
            [notificationDict setValue:@"false" forKey:@"IsAttachment"];
            
        }
        
        [TSAnalytics logEvent:ANALYTICS_NOTIFICATION_CLICKED_ASSIGN_RECEIVER parameter:analyticsDict];
        
        TSReceiverSelectionViewController *receiverSelectionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBReceiverSelection"];
        
        receiverSelectionVC.menuId = SINGLETON_INSTANCE.currentMenuId;
        
        receiverSelectionVC.communicationType = OBJECT_TYPE_NOTIFICATION;
        
        receiverSelectionVC.commonDict = notificationDict;
        
        [self.navigationController pushViewController:receiverSelectionVC animated:YES];

        
    }
}


#pragma mark - Attachment Delegate

- (void) selectedAttachmentImageData:(NSData *)imageData imageName:(NSString *)imageName isHasAttachment:(BOOL)isHasAttachment
{
    if (isHasAttachment) {
        NSLog(@"%@",imageData);
        attachmentData = imageData;
         isHasNotificationAttachment= YES;
    }
}

- (void)selectedAttachmentImageName:(NSString *)imageName{
    
    if (imageName != nil) {
        _attachementTxtFld.text = imageName;
    }else {
        _attachementTxtFld.text = @"Photo";
    }
}

#pragma mark - TextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    return YES;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    currentResponder = textField;
    
   if(textField == _attachementTxtFld) {
        
        [self.view endEditing:YES];
        
        [TSAttachmentPicker instance].delegate = self;
        
        [[TSAttachmentPicker instance] showAttachmentPickerForView:self frame:textField.frame];
        
        return NO;
        
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - TextView Delegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    currentResponder = textView;
    
    if ([textView.text isEqualToString:DESCRIPTION_TEXT]) {
        
        textView.text = @"";
        textView.textColor = APP_BLACK_COLOR;
        textView.font = [UIFont fontWithName:APP_FONT size:14];
        
    }
    
    return YES;
    
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if ([textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
        textView.text = DESCRIPTION_TEXT;
        textView.textColor = APP_PLACEHOLDER_COLOR;
        textView.font = [UIFont fontWithName:APP_FONT size:17];
        
    }
    
    CGFloat fixedWidth = _descriptionTxtVw.frame.size.width;
    CGSize newSize = [_descriptionTxtVw sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = _descriptionTxtVw.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    
    if (newFrame.size.height > 50) {
        _descriptionTxtVw.frame = newFrame;
    }else {
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), 50);
        _descriptionTxtVw.frame = newFrame;
    }

    
    [self reArrangeControles];
    
    //ScrollView Fix
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_scrollVw setContentSize:CGSizeMake(DeviceWidth, _attachementTxtFld.frame.origin.y + _attachementTxtFld.frame.size.height + 60)];
    });
    
}

- (void)textViewDidChange:(UITextView *)textView{
    
   /* CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    
    if (newFrame.size.height > 50 && newFrame.size.height< currentVisibleRect.size.height - textView.frame.size.height) {
        textView.frame = newFrame;
    }else if(newFrame.size.height > 50){
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), currentVisibleRect.size.height - 66);
        textView.frame = newFrame;
    }*/
    
    [self reArrangeControles];

}

- (void)reArrangeControles{
    
    _attachementTxtFld.frame = CGRectMake(_attachementTxtFld.frame.origin.x,_descriptionTxtVw.frame.origin.y + _descriptionTxtVw.frame.size.height, _attachementTxtFld.frame.size.width, _attachementTxtFld.frame.size.height);
    
    _attachmentPinImgVw.frame = CGRectMake(_attachmentPinImgVw.frame.origin.x,_attachementTxtFld.frame.origin.y + 15, _attachmentPinImgVw.frame.size.width, _attachmentPinImgVw.frame.size.height);
    
    //_receiverSelectionVw.frame = CGRectMake(_receiverSelectionVw.frame.origin.x,_attachementTxtFld.frame.origin.y + _attachementTxtFld.frame.size.height, _receiverSelectionVw.frame.size.width, _receiverSelectionVw.frame.size.height);
    
}


#pragma mark - Keyboard Hide

- (void)keyboardWasShown:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    CGSize currentKeyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    CGPoint textFieldOrigin = currentResponder.frame.origin;
    
    CGRect visibleRect = viewRect;
    visibleRect.size.height -= currentKeyboardSize.height;
    currentVisibleRect = visibleRect;
    
    if ([_descriptionTxtVw isFirstResponder]) {
        
        
        _descriptionTxtVw.frame = CGRectMake(_descriptionTxtVw.frame.origin.x, _descriptionTxtVw.frame.origin.y, _descriptionTxtVw.frame.size.width, visibleRect.size.height - 66);
        
        
        [self reArrangeControles];
        
        CGPoint scrollPoint = CGPointMake(0.0, textFieldOrigin.y);
        
        [_scrollVw setContentOffset:scrollPoint animated:YES];
        
        [_scrollVw setScrollEnabled:NO];
        
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)notification
{
    [_scrollVw setScrollEnabled:YES];
    [_scrollVw setContentOffset:CGPointZero animated:YES];
}




@end
