//
//  PushCount+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 19/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PushCount+CoreDataProperties.h"

@implementation PushCount (CoreDataProperties)

@dynamic isRead;
@dynamic commTypeID;
@dynamic transId;
@dynamic unreadCount;
@dynamic staffTransId;
@dynamic isForApproval;
@dynamic staff;

@end
