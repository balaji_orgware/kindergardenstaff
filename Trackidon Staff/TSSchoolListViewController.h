//
//  TPSchoolListViewController.h
//  Trackidon
//
//  Created by Elango on 12/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSSchoolListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *schoolListTblVw;

@end
