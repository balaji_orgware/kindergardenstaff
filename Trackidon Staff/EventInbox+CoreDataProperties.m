//
//  EventInbox+CoreDataProperties.m
//  Trackidon Staff
//
//  Created by Elango on 28/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "EventInbox+CoreDataProperties.h"

@implementation EventInbox (CoreDataProperties)

@dynamic assignedList;
@dynamic eventAttachment;
@dynamic eventDescription;
@dynamic eventTime;
@dynamic eventTitle;
@dynamic eventVenue;
@dynamic fromDate;
@dynamic isHasAttachment;
@dynamic modifiedDate;
@dynamic staffTransID;
@dynamic toDate;
@dynamic transID;
@dynamic userCreatedName;
@dynamic staff;

@end
