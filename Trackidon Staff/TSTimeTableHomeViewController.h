//
//  TSTimeTableHomeViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 15/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSTimeTableHomeViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@end
