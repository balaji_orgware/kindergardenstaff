//
//  TSCommonListViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 20/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSCommonListViewController.h"

@interface TSCommonListViewController ()

@end

@implementation TSCommonListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _studentNameLbl.text = _studentObj.studentName;
    
    [TSSingleton layerDrawForView:_studentNameVw position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    switch (_moduleType) {
        case HealthType_Emergency:
            [_listTableView registerNib:[UINib nibWithNibName:@"TSEmergencyTableViewCell" bundle:nil] forCellReuseIdentifier:@"emergencyCell"];
            break;
            
        case HealthType_HeightAndWeight:
            [_listTableView registerNib:[UINib nibWithNibName:@"TSHeightAndWeightTableViewCell" bundle:nil] forCellReuseIdentifier:@"heightAndWeightCell"];
            break;
            
        default:
            [_listTableView registerNib:[UINib nibWithNibName:@"TSCommonListTableViewCell" bundle:nil] forCellReuseIdentifier:@"commonCell"];
            break;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    switch (_moduleType) {
        case HealthType_Emergency:
            self.navigationItem.title = @"Emergency";
            break;
            
        case HealthType_HeightAndWeight:
            self.navigationItem.title = @"Height And Weight";
            break;
        
        case HealthType_Eyes:
            self.navigationItem.title = @"Eyes";
            break;
            
        case HealthType_Ears:
            self.navigationItem.title = @"Ears";
            break;
        
        case HealthType_Dental:
            self.navigationItem.title = @"Dental";
            break;
            
        case HealthType_NoseAndThroat:
            self.navigationItem.title = @"Nose and Throat";
            break;
        
        case HealthType_GeneralCheckup:
            self.navigationItem.title = @"General Checkup";
            break;
            
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button Actions

- (IBAction)updateButtonTapped:(id)sender {
    
    switch (_moduleType) {
        
        case HealthType_HeightAndWeight:
        {
            TSCreateHeightAndWeightViewController *heightWeightVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBCreateHeightAndWeight"];
            
            [heightWeightVC setGeneralInfoDict:_generalInfoDict];
            
            [self.navigationController pushViewController:heightWeightVC animated:YES];
            
            break;
        }
            
        case HealthType_Eyes:
        {
            TSCommonCreateEyeEarNoseViewController *eyeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBCommonCreateEyeEarNose"];
            
            [eyeVC setGeneralInfoDict:_generalInfoDict];
            [eyeVC setHealthType:HealthType_Eyes];
            
            [self.navigationController pushViewController:eyeVC animated:YES];
            
            break;
        }
            
        case HealthType_Ears:
        {
            TSCommonCreateEyeEarNoseViewController *earVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBCommonCreateEyeEarNose"];
            [earVC setGeneralInfoDict:_generalInfoDict];
            [earVC setHealthType:HealthType_Ears];
            
            [self.navigationController pushViewController:earVC animated:YES];
            
            break;
        }
            
        case HealthType_NoseAndThroat:
        {
            TSCommonCreateEyeEarNoseViewController *noseVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBCommonCreateEyeEarNose"];
            [noseVC setGeneralInfoDict:_generalInfoDict];
            [noseVC setHealthType:HealthType_NoseAndThroat];
            
            [self.navigationController pushViewController:noseVC animated:YES];
            
            break;
        }
            
        case HealthType_GeneralCheckup:
        {
            TSCreateGeneralCheckupDentalViewController *genCheckupVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBCreateGeneralCheckupDental"];
            [genCheckupVC setGeneralInfoDict:_generalInfoDict];
            [genCheckupVC setHealthType:HealthType_GeneralCheckup];
            [self.navigationController pushViewController:genCheckupVC animated:YES];
            
            break;
        }
        
        case HealthType_Dental:
        {
            TSCreateGeneralCheckupDentalViewController *dentalVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBCreateGeneralCheckupDental"];
            [dentalVC setGeneralInfoDict:_generalInfoDict];
            [dentalVC setHealthType:HealthType_Dental];
            
            [self.navigationController pushViewController:dentalVC animated:YES];
            
            break;
        }
        
        case HealthType_Emergency:
        {
            TSEmergencyCreateViewController *emergencyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBEmergencyCreate"];
            [emergencyVC setGeneralInfoDict:_generalInfoDict];
            [self.navigationController pushViewController:emergencyVC animated:YES];
            
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - Tableview Datasources & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (_contentArray.count >0)
        [tableView setHidden:NO];
    else
        [tableView setHidden:YES];
    
    return _contentArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (_moduleType) {
            
        case HealthType_Emergency:
        {
            TSEmergencyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"emergencyCell"];
            
            if (!cell) {
                [tableView registerNib:[UINib nibWithNibName:@"TSEmergencyTableViewCell" bundle:nil] forCellReuseIdentifier:@"emergencyCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"emergencyCell"];
            }
            
            [cell loadContentForCell:cell withModelArray:_contentArray withRow:indexPath.row];
            
            return cell;
            
            break;
        }
            
        case HealthType_HeightAndWeight:
        {
            TSHeightAndWeightTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"heightAndWeightCell"];
            
            if (!cell) {
                [tableView registerNib:[UINib nibWithNibName:@"TSHeightAndWeightTableViewCell" bundle:nil] forCellReuseIdentifier:@"heightAndWeightCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"heightAndWeightCell"];
            }
            
            [cell loadContentForCell:cell withModelArray:_contentArray withRow:indexPath.row];
            
            return cell;
            
            break;
        }
            
        default:
        {
            TSCommonListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"commonCell"];
            
            if (!cell) {
                [tableView registerNib:[UINib nibWithNibName:@"TSCommonTableViewCell" bundle:nil] forCellReuseIdentifier:@"commonCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"commonCell"];
            }
            
            [cell loadContentForCell:cell withModuleType:_moduleType withModelArray:_contentArray withRow:indexPath.row];
            
            return cell;
        }
            break;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (_moduleType) {
            
        case HealthType_Eyes:
        {
            TSEyeEarDescriptionViewController *descVC = [[TSEyeEarDescriptionViewController alloc]initWithNibName:@"TSEyeEarDescriptionViewController" bundle:nil];
            
            id modelObj = [_contentArray objectAtIndex:indexPath.row];
            
            [descVC showDescViewController:self withModuleType:_moduleType andModelObj:modelObj];
            
            break;
        }
        case HealthType_Ears:
        {
            TSEyeEarDescriptionViewController *descVC = [[TSEyeEarDescriptionViewController alloc]initWithNibName:@"TSEyeEarDescriptionViewController" bundle:nil];
            
            id modelObj = [_contentArray objectAtIndex:indexPath.row];
            
            [descVC showDescViewController:self withModuleType:_moduleType andModelObj:modelObj];
            
            break;
        }
            
        case HealthType_NoseAndThroat:
        {
            TSEyeEarDescriptionViewController *descVC = [[TSEyeEarDescriptionViewController alloc]initWithNibName:@"TSEyeEarDescriptionViewController" bundle:nil];
            
            id modelObj = [_contentArray objectAtIndex:indexPath.row];
            
            [descVC showDescViewController:self withModuleType:_moduleType andModelObj:modelObj];
            
            break;
        }
            
        case HealthType_HeightAndWeight:
        {
            TSHeightWeightDescViewController *descVC = [[TSHeightWeightDescViewController alloc]initWithNibName:@"TSHeightWeightDescViewController" bundle:nil];
            BMIInfoModel *modelObj = [_contentArray objectAtIndex:indexPath.row];
            
            [descVC showDescViewController:self withModelObj:modelObj];
            
            break;
        }
            
        case HealthType_GeneralCheckup:
        {
            TSGeneralCheckupDescViewController *descVC =[[TSGeneralCheckupDescViewController alloc]initWithNibName:@"TSGeneralCheckupDescViewController"bundle:nil];
            
            GeneralCheckupModel *modelObj = [_contentArray objectAtIndex:indexPath.row];
            
            [descVC showDescViewController:self withModelObj:modelObj];
            
            break;
        }
            
        case HealthType_Emergency:
        {
            TSEmergencyDescViewController *descVC = [[TSEmergencyDescViewController alloc]initWithNibName:@"TSEmergencyDescViewController" bundle:nil];
            
            EmergencyInfoModel *modelObj = [_contentArray objectAtIndex:indexPath.row];
            
            [descVC showDescViewController:self withModelObj:modelObj];
            
            break;
        }
            
        case HealthType_Dental:
        {
            TSDentalDescViewController *descVC = [[TSDentalDescViewController alloc]initWithNibName:@"TSDentalDescViewController" bundle:nil];
            
            DentalInfoModel *modelObj = [_contentArray objectAtIndex:indexPath.row];
            
            [descVC showDescViewController:self withModelObj:modelObj];
            
            break;
        }
        default:
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

@end
