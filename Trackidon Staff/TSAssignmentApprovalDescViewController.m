//
//  TSAssignmentApprovalDescViewController.m
//  Trackidon Staff
//
//  Created by Balaji on 05/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSAssignmentApprovalDescViewController.h"

@interface TSAssignmentApprovalDescViewController (){
    
    Assignments *assignmentsObj;
    
    NSURL *attachmentURL;
    
    NSArray *assigneeArr;

}
@end

@implementation TSAssignmentApprovalDescViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(descViewTapped)];
    
    [_descriptionTextView addGestureRecognizer:tapGesture];
    
    {
        [TSSingleton layerDrawForView:_topView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
                
        [TSSingleton layerDrawForView:_dateView position:LAYER_TOP color:[UIColor lightGrayColor]];
        
        [TSSingleton layerDrawForView:_assignedToView position:LAYER_TOP color:[UIColor lightGrayColor]];
        
        [SINGLETON_INSTANCE addLeftInsetToTextField:_assignedToTxtFld];
    }

    {
        _attachmentButton.frame = CGRectMake(DEVICE_SIZE.width - _attachmentButton.frame.size.width - 10, _topView.frame.origin.y + _topView.frame.size.height - _attachmentButton.frame.size.height/2 + 18, _attachmentButton.frame.size.height, _attachmentButton.frame.size.height);
        
        _progressView.frame = CGRectMake(_assignedDateLabel.center.x +_assignedDateLabel.frame.size.width/2, _assignedDateLabel.center.y, _progressView.frame.size.width, _progressView.frame.size.height);
        
        _dueDateLabel.frame = CGRectMake(_progressView.center.x +_progressView.frame.size.width/2, _assignedDateLabel.frame.origin.y, _dueDateLabel.frame.size.width, _dueDateLabel.frame.size.height);
        
        _assignedLbl.center = CGPointMake(_assignedDateLabel.center.x, _assignedDateLabel.frame.origin.y-10);
        
        _dueLbl.center = CGPointMake(_dueDateLabel.center.x, _dueDateLabel.frame.origin.y-10);
    }
    
    /*NSPredicate *predicate = [NSPredicate predicateWithFormat:@"transId MATCHES %@",_assignmentObj.transID];
    
    assigneeArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"AssignedDetails" withPredicate:predicate];
    
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"assigneeType" ascending:YES];
    
    NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
    
    assigneeArr = [assigneeArr sortedArrayUsingDescriptors:descriptors];*/
    
    assigneeArr = [_assignmentObj.assignedList componentsSeparatedByString:ASSIGNEE_SAPRATOR];
    
    if (assigneeArr.count ==0) {
        
        [_assignedToView setHidden:YES];
    }
    else{
        
        [_assignedToView setHidden:NO];
        
        if (assigneeArr.count == 1) {
            
            //AssignedDetails *obj = [assigneeArr objectAtIndex:0];
            
            _assignedToTxtFld.text = [NSString stringWithFormat:@"Assigned to : %@",[assigneeArr objectAtIndex:0]];
        }
        else{

            [SINGLETON_INSTANCE addRightViewToTextField:_assignedToTxtFld withImageName:EXPAND_IMGNAME];

            _assignedToTxtFld.text = @"Assigned to :";
        }
    }
    
    if (IS_IPHONE_4) {
        
        //int navigationBarHeight = 64;
        
        _dateView.frame = CGRectMake(0, DeviceHeight -75, DeviceWidth, 75);
        
        _assignedToView.frame = CGRectMake(0, DeviceHeight-_dateView.frame.size.height-40, DeviceWidth, 40);
        
        if (assigneeArr.count == 0) {
            
            _descriptionTextView.frame = CGRectMake(self.descriptionTextView.frame.origin.x, self.descriptionTextView.frame.origin.y, self.descriptionTextView.frame.size.width, 120+_assignedToView.frame.size.height);
        }
        else{
            
            _descriptionTextView.frame = CGRectMake(self.descriptionTextView.frame.origin.x, self.descriptionTextView.frame.origin.y, self.descriptionTextView.frame.size.width, 120);
        }
        
        
        _assignedDateLabel.frame = CGRectMake(_assignedDateLabel.frame.origin.x, 25, 45, 45);
        
        _dueDateLabel.frame = CGRectMake(_dueDateLabel.frame.origin.x, _assignedDateLabel.frame.origin.y, 45, 45);
        
        _assignedLbl.center = CGPointMake(_assignedDateLabel.center.x, 15);
        
        _dueLbl.center = CGPointMake(_dueDateLabel.center.x, 15);
        
        _progressView.frame = CGRectMake(_assignedDateLabel.frame.origin.x+_assignedDateLabel.frame.size.width, _assignedDateLabel.frame.origin.y+_assignedDateLabel.frame.size.height/2, _dueDateLabel.frame.origin.x -(_assignedDateLabel.frame.origin.x+_assignedDateLabel.frame.size.width), 2);
    }
    
    {
        _assignedDateLabel.layer.cornerRadius = _assignedDateLabel.frame.size.height/2;
        
        _assignedDateLabel.layer.masksToBounds = YES;
        
        _dueDateLabel.layer.cornerRadius = _dueDateLabel.frame.size.height/2;
        
        _dueDateLabel.layer.masksToBounds = YES;
    }
    
    assignmentsObj = (Assignments *)self.assignmentObj;
    
    _assignedDateLabel.attributedText = [self getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:assignmentsObj.assignmentDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_assignedDateLabel];
    
    _dueDateLabel.attributedText = [self getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:assignmentsObj.dueDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_dueDateLabel];
    
    _descriptionTextView.text = [NSString stringWithFormat:@"\t%@",assignmentsObj.assignmentDescription];
    
    _assignmentRemainingCountLbl.text = [TSSingleton daysBetweenFromDate:assignmentsObj.assignmentDate toDate:assignmentsObj.dueDate];
    
    _assignedTeacherLabel.text = [NSString stringWithFormat:@"- by %@",assignmentsObj.approvedByName];
    
    _assignmentTitleLbl.text = [NSString stringWithFormat:@"Title : %@",assignmentsObj.assignmentTitle];
    
    _subjectLbl.text = [NSString stringWithFormat:@"Subject : %@",assignmentsObj.subjectName];
    
    _typeLbl.text = [NSString stringWithFormat:@"Type : %@",assignmentsObj.assignmentTypeName];
    
    if ([assignmentsObj.isAttachment intValue] == 1) {
        
        [_attachmentButton setHidden:NO];
    }
    else{
        
        [_attachmentButton setHidden:YES];
    }
    
    UIBarButtonItem *approveBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"approve"] style:UIBarButtonItemStyleDone target:self action:@selector(approveButtonAction)];
    
    UIBarButtonItem *declineBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"decline"] style:UIBarButtonItemStyleDone target:self action:@selector(declineButtonAction)];
    
    self.navigationItem.rightBarButtonItems = @[approveBtn,declineBtn];
    
     self.navigationItem.title = [NSString stringWithFormat:@"%@ Detail",SINGLETON_INSTANCE.barTitle];
    
    self.navigationController.navigationBar.topItem.title = @"";
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [TSAnalytics logScreen:ANALYTICS_SCREEN_APPROVALASSIGNMENT_DESCRIPTION parameter:[NSMutableDictionary new]];
    
}

#pragma mark - Button actions

-(void) approveButtonAction{
    
    UIAlertView *approveAlertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Do you want to Approve this Assignment?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    
    [approveAlertView setTag:APPROVAL_TYPE_APPROVE];
    
    [approveAlertView show];
}

-(void) declineButtonAction{
    
    UIAlertView *declinedAlertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Do you want to Decline this Assignment?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    
    [declinedAlertView setTag:APPROVAL_TYPE_DECLINE];
    
    [declinedAlertView show];
}

#pragma mark - TextField Delegates

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (assigneeArr.count>1) {
        
        AssignedListViewController *listVC = [[AssignedListViewController alloc]initWithNibName:@"AssignedListViewController" bundle:nil];
        
        [listVC setAssignedText:AssignmentAssigned];
        
        [listVC setAssignedListArray:assigneeArr];
        
        [listVC showInViewController:self];
    }
    
    return NO;
}

#pragma mark - AlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == APPROVAL_TYPE_APPROVE) {
        
        if (buttonIndex) {
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidUserTransID"];
            
            [param setValue:[NSString stringWithFormat:@"%ld",(long)APPROVAL_TYPE_APPROVE] forKey:@"intApprovedStatus"];
            
            [param setValue:@[assignmentsObj.transID] forKey:@"guidAssignmentTransID"];
            
            NSDictionary *updateParam = @{@"ApproveAssignments":param};
            
            [self updateAssignmentsWithParam:updateParam withApprovalType:APPROVAL_TYPE_APPROVE];
        }
    }
    else if (alertView.tag == APPROVAL_TYPE_DECLINE){
        
        if (buttonIndex) {
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            
            [param setValue:SINGLETON_INSTANCE.staffTransId forKey:@"guidUserTransID"];
            
            [param setValue:[NSString stringWithFormat:@"%ld",(long)APPROVAL_TYPE_DECLINE] forKey:@"intApprovedStatus"];
            
            [param setValue:@[assignmentsObj.transID] forKey:@"guidAssignmentTransID"];
            
            NSDictionary *updateParam = @{@"ApproveAssignments":param};
            
            [self updateAssignmentsWithParam:updateParam withApprovalType:APPROVAL_TYPE_DECLINE];
        }
    }
    
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

#pragma mark - Helper Methods

-(NSMutableAttributedString *) getDateAttributedStringForDateString:(NSString *)dateString inLabel:(UILabel *)label{
    
    [label setNumberOfLines:2];
    
    UIFont *dateFont,*monthFont;
    
    if (IS_IPHONE_4) {
        
        dateFont = [UIFont fontWithName:APP_FONT size:22.0f];
        
        monthFont = [UIFont fontWithName:APP_FONT size:10.0f];
    }
    else{
        
        dateFont = [UIFont fontWithName:APP_FONT size:25.0f];
        
        monthFont = [UIFont fontWithName:APP_FONT size:12.0f];
    }
    
    NSString *dateNo = [SINGLETON_INSTANCE changeDateFormatFromService:dateString withFormat:@"dd"];
    
    NSString *monthName = [SINGLETON_INSTANCE changeDateFormatFromService:dateString withFormat:@"MMM"];
    
    NSDictionary *dateTextAttr = [NSDictionary dictionaryWithObject: dateFont forKey:NSFontAttributeName];
    NSMutableAttributedString *countAttrStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@   ",dateNo] attributes: dateTextAttr];
    
    
    NSDictionary *monthTextAttr = [NSDictionary dictionaryWithObject:monthFont forKey:NSFontAttributeName];
    NSMutableAttributedString *textAttrStr = [[NSMutableAttributedString alloc]initWithString:monthName attributes:monthTextAttr];
    
    [countAttrStr appendAttributedString:textAttrStr];
    
    return countAttrStr;
}

- (IBAction)attachmentButtonTapped:(id)sender {
    
    [_attachmentButton setUserInteractionEnabled:NO];
    
    NSString *attachmentURLString;
    
    attachmentURL = [NSURL URLWithString:assignmentsObj.attachment];
    
    attachmentURLString = assignmentsObj.attachment;
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *attachmentFile = [documentsPath stringByAppendingPathComponent:[attachmentURL lastPathComponent]];
    
    NSLog(@"%@",attachmentFile);
    
    NSLog(@"%@",[attachmentURL lastPathComponent]);
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:attachmentFile];
    
    if (fileExists) {
     
        NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
        
        [analyticsDict setValue:ANALYTICS_VALUE_OPENEDFROMLOCAL forKey:ANALYTICS_PARAM_ATTACHMENTSTATUS];
        
        [TSAnalytics logEvent:ANALYTICS_ASSIGNMENTAPPROVALS_CLICKED_ATTACHMENT parameter:analyticsDict];
        
        _attachmentButton.userInteractionEnabled = YES;

        [self navigateToAttachmentViewControllerWithFileName:attachmentURL];
    }
    else{
        
        [[TSHandlerClass sharedHandler]downloadAttachmentWithURL:attachmentURLString currentView:self.view withHandler:^(BOOL success, NSURL *filePath) {
            
            if (success) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_DOWNLOADING forKey:ANALYTICS_PARAM_ATTACHMENTSTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_ASSIGNMENTAPPROVALS_CLICKED_ATTACHMENT parameter:analyticsDict];
                
                _attachmentButton.userInteractionEnabled = YES;

                [self navigateToAttachmentViewControllerWithFileName:filePath];
            }
            else{
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                [analyticsDict setValue:ANALYTICS_VALUE_URLNOTFOUND forKey:ANALYTICS_PARAM_ATTACHMENTSTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_ASSIGNMENTAPPROVALS_CLICKED_ATTACHMENT parameter:analyticsDict];
                
                _attachmentButton.userInteractionEnabled = YES;
            }
        }];
    }
}

-(void) navigateToAttachmentViewControllerWithFileName:(NSURL *)fileURL{
    
    TSAttachmentViewController *attachmentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAttachment"];
    
    [attachmentVC setFileName:[fileURL lastPathComponent]];
    
    UINavigationController *navCtrl = [[UINavigationController alloc]initWithRootViewController:attachmentVC];
    
    navCtrl.navigationBar.translucent = NO;
    
    navCtrl.navigationBar.barTintColor = APPTHEME_COLOR;
    
    navCtrl.navigationBar.tintColor = [UIColor whiteColor];
    
    [navCtrl.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:APP_FONT size:18]}];
    
    [self.navigationController presentViewController:navCtrl animated:YES completion:nil];
}

#pragma mark - Service Call

// Update Assignments Service Call
-(void) updateAssignmentsWithParam:(NSDictionary *)param withApprovalType:(NSInteger)approvalType{
    
    NSString *urlString = @"MastersMServices/AssignmentService.svc/MobileApproveAssignments";
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"MobileApproveAssignmentsResult"][@"ResponseCode"] integerValue] == 1) {
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                if (approvalType == APPROVAL_TYPE_APPROVE) {
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                else{
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                
                [analyticsDict setValue:ANALYTICS_VALUE_SUCCESS forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_ASSIGNMENTAPPROVALS_UPDATEAPPROVAL parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"MobileApproveAssignmentsResult"][@"ResponseMessage"]];
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"staffTransID MATCHES %@ AND isForApprovals == 1",SINGLETON_INSTANCE.staffTransId];
                
                [SINGLETON_INSTANCE deleteSelectedObjectsInEntityWithEntityName:@"Assignments" withPredicate:predicate withContext:[APPDELEGATE_INSTANCE managedObjectContext]];
                
                [[NSNotificationCenter defaultCenter]postNotificationName:@"updateTableView" object:nil];
                
                [self.navigationController popViewControllerAnimated:YES];
                
            }
            else{
                
                NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
                
                if (approvalType == APPROVAL_TYPE_APPROVE) {
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                else{
                    
                    [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
                }
                
                [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
                
                [TSAnalytics logEvent:ANALYTICS_ASSIGNMENTAPPROVALS_UPDATEAPPROVAL parameter:analyticsDict];
                
                [TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
            }
        }
        else{
            
            NSMutableDictionary *analyticsDict = [NSMutableDictionary dictionary];
            
            if (approvalType == APPROVAL_TYPE_APPROVE) {
                
                [analyticsDict setValue:ANALYTICS_VALUE_APPROVE forKey:ANALYTICS_PARAM_APPROVALTYPE];
            }
            else{
                
                [analyticsDict setValue:ANALYTICS_VALUE_DECLINE forKey:ANALYTICS_PARAM_APPROVALTYPE];
            }
            
            [analyticsDict setValue:ANALYTICS_VALUE_FAILURE forKey:ANALYTICS_PARAM_UPDATESTATUS];
            
            [TSAnalytics logEvent:ANALYTICS_ASSIGNMENTAPPROVALS_UPDATEAPPROVAL parameter:analyticsDict];
        }
    }];
}

-(void) descViewTapped{
    
    [TSAnalytics logEvent:ANALYTICS_ASSIGNMENTAPPROVALS_CLICKED_DESCRIPTION parameter:[NSMutableDictionary new]];
    
    TSDescriptionViewController *descVC = [[TSDescriptionViewController alloc] initWithNibName:@"TSDescriptionViewController" bundle:nil];
    
    [descVC showInViewController:self descriptionString:_descriptionTextView.text];
    
}

@end