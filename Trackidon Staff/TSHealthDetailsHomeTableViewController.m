//
//  TSHealthDetailsHomeTableViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 11/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSHealthDetailsHomeTableViewController.h"

@interface TSHealthDetailsHomeTableViewController ()

@end

@implementation TSHealthDetailsHomeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationItem.title = SINGLETON_INSTANCE.barTitle;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
    UILabel *menuLabel = (UILabel *)[cell viewWithTag:101];
    
    switch (indexPath.row) {
        case 0:
            menuLabel.text = @"Health";
            imageView.image = [UIImage imageNamed:@"user_manual"];
            break;
            
        case 1:
            menuLabel.text = @"Temperature";
            imageView.image = [UIImage imageNamed:@"user_manual"];
            break;
            
        default:
            break;
    }
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([SINGLETON_INSTANCE checkForActiveInternet]) {
        
        switch (indexPath.row) {
            case 0:
            {
                TSHealthStudSelectionViewController *healthSelectionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBHealthStudentSelection"];
                
                [self.navigationController pushViewController:healthSelectionVC animated:YES];
                
                break;
            }
                
            case 1:
            {
                TSTemperatureHomeTableViewController *temperatureHomeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBTemperatureHome"];
                
                [self.navigationController pushViewController:temperatureHomeVC animated:YES];
                
                break;
            }
                
            default:
                break;
        }
    }
    else{
        [TSSingleton showAlertWithMessage:TEXT_NOINTERNET];
    }
    
}

@end