//
//  TSSettingsTableViewController.h
//  Trackidon Staff
//
//  Created by ORGware on 18/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSSettingsTableViewController : UITableViewController


-(void)fetchSettingsByStaffTransIDWithURL:(NSString *)url params:(NSMutableDictionary *)param;

-(void) updateSettingsByStaffTransIDWithURL:(NSString *)url params:(NSMutableDictionary *)param;

@end

@interface SettingsModel : NSObject

@property (nonatomic, strong) NSString *pushNotificationAlert;

@property (nonatomic, strong) NSString *emailAlert;

@property (nonatomic, strong) NSString *proximityAlert;

@property (nonatomic, strong) NSString *smsAlert;

@property (nonatomic, strong) NSString *notificationTransID;

@end


