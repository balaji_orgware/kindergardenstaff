//
//  TSEmergencyDescViewController.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 21/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSEmergencyDescViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIView *descView;
@property (weak, nonatomic) IBOutlet UIView *dateView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *descTxtVw;

-(void) showDescViewController:(UIViewController *)viewController withModelObj:(EmergencyInfoModel *)modelObj;

@end
