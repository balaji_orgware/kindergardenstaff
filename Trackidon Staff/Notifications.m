//
//  Notifications.m
//  Trackidon Staff
//
//  Created by Elango on 22/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "Notifications.h"
#import "StaffDetails.h"

@implementation Notifications

+ (instancetype)instance{
    
    static Notifications *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (&onceToken, ^{
        
        kSharedInstance = [[Notifications alloc]init];
        
    });
    
    return kSharedInstance;
}

- (void)saveNotificationsWithResponseDict:(NSDictionary *)dict  andIsForApprovals:(BOOL)isForApprovals{
    
    NSLog(@"%@",dict);
    
    @try {
        
        NSManagedObjectContext *mainContext = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSArray *responseArray = [NSArray new];
        
        NSPredicate *predicate;
        
        if (isForApprovals) { // Notification Approvals
            
            responseArray = dict[@"FetchUnApprovedCommunicationResult"][@"UnApprovalClass"][@"SchoolNotificationClass"];
            
            predicate = [NSPredicate predicateWithFormat:@"isForApprovals == 1"];
            
        }
        else{ // Notifications
            
            responseArray = dict[@"FetchSchoolNotificationsByStaffResult"][@"SchoolNotificationClass"];
            
            predicate = [NSPredicate predicateWithFormat:@"isForApprovals == 0"];
        }
        
        [SINGLETON_INSTANCE deleteSelectedObjectsInEntityWithEntityName:@"Notifications" withPredicate:predicate withContext:mainContext];

        
        NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        
        [context setParentContext:mainContext];
        
        NSManagedObjectID *objectId = [SINGLETON_INSTANCE.staff objectID];
        
        StaffDetails *tmpStaff = [context objectWithID:objectId];
        
        for (NSDictionary *tmpNotificationDict in responseArray) {
            
            NSDictionary *notificationDict = [tmpNotificationDict dictionaryByReplacingNullsWithBlanks];
            
            Notifications *notification;
            
            //if (isForApprovals) {
                
                notification = [NSEntityDescription insertNewObjectForEntityForName:@"Notifications" inManagedObjectContext:context];
            /*}
            else{
                
                notification = [self checkAvailabilityWithNotificationTransId:notificationDict[@"TransID"] context:context andIsForApprovals:isForApprovals];
                
                if (notification == nil) {
                    
                    notification = [NSEntityDescription insertNewObjectForEntityForName:@"Notifications" inManagedObjectContext:context];
                }
            }*/
            
            notification.acadamicTransId = notificationDict[@"AcadamicTransID"];
            notification.approveByTransId = notificationDict[@"ApprovedBy"];
            notification.approvedByName = notificationDict[@"ApprovedByName"];
            notification.approvedDate = [SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:notificationDict[@"ApprovedDate"]];
            notification.approvedStatus = [NSNumber numberWithInteger:[notificationDict[@"ApprovedStatus"]integerValue]];
            notification.notificationDescription = notificationDict[@"NotificationDescription"];
            notification.notificationAttachmentUrl = notificationDict[@"Attachment"];
            notification.notificationDate = [SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:notificationDict[@"NotificationDate"]];
            notification.notificationGroupType = [NSNumber numberWithInteger:[notificationDict[@"GroupType"]integerValue]];
            notification.notificationIsHasAttachment = [NSNumber numberWithInteger:[notificationDict[@"IsAttachment"]integerValue]];
            notification.notificationPriority = [NSNumber numberWithInteger:[notificationDict[@"Priority"]integerValue]];
            notification.notificationTitle = notificationDict[@"NotificationTitle"];
            
            notification.schoolTransId = notificationDict[@"SchoolTransID"];
            notification.notificationTransId = notificationDict[@"TransID"];
            notification.userCreatedTransId = notificationDict[@"CreatedBy"];
            notification.userCreatedName = notificationDict[@"CreatedBy"];
            notification.userModifiedTransId = notificationDict[@"ModifiedBy"];
            notification.userModifiedName = notificationDict[@"ModifiedBy"];
            notification.staffTransId = SINGLETON_INSTANCE.staffTransId;
            notification.modifiedDate = [SINGLETON_INSTANCE dateFromString:notificationDict[@"ModifiedDate"] withFormate:DATE_FORMATE_MODIFIED_DATE];
            notification.staff = tmpStaff;
            
            notification.isForApprovals = [NSNumber numberWithBool:isForApprovals];
            notification.boardName = notificationDict[@"BoardName"];
            
            NSString *assigneeList = [[TSCommonAssignedDetails instance] getAssignedDetailsWithArray:notificationDict[@"SchoolNotificationDetails"] withGroupType:notificationDict[@"GroupType"]];
            
            notification.assignedList = assigneeList;
            
            //[[AssignedDetails instance] saveAssignedDetailsWithDict:notificationDict[@"SchoolNotificationDetails"] withGroupType:notificationDict[@"GroupType"] transId:notification.notificationTransId forObject:notification objectType:OBJECT_TYPE_NOTIFICATION context:context];
            
        }
        
        NSError *error;
        
        if (![context save:&error]) {
            
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            
            
        }
        
        [SINGLETON_INSTANCE storeContextInMainThread];
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    }
}

-(Notifications *)checkAvailabilityWithNotificationTransId:(NSString *)notificationTransId context:(NSManagedObjectContext *)context andIsForApprovals:(BOOL)isForApprovals
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Notifications" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"notificationTransId contains[cd] %@ AND isForApprovals == %@", notificationTransId,[NSNumber numberWithBool:isForApprovals]];

    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
    
}

- (void)savePushNotificationWithDict:(NSDictionary *)dictionary{

    @try {
        
        NSManagedObjectContext *context = [APPDELEGATE_INSTANCE managedObjectContext];
        
        NSError *error = nil;
        
        NSDictionary *notificationDict = [dictionary[@"FetchSchoolNotificationsByTransIDResult"][@"SchoolNotificationClass"] dictionaryByReplacingNullsWithBlanks];
        
        Notifications *notification = [self checkAvailabilityWithNotificationTransId:notificationDict[@"TransID"] context:context andIsForApprovals:NO];
        
        if (notification == nil) {
            
            notification = [NSEntityDescription insertNewObjectForEntityForName:@"Notifications" inManagedObjectContext:context];
        }
        
        notification.acadamicTransId = notificationDict[@"AcadamicTransID"];
        notification.approveByTransId = notificationDict[@"ApprovedBy"];
        notification.approvedByName = notificationDict[@"ApprovedByName"];
        notification.approvedDate = [SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:notificationDict[@"ApprovedDate"]];
        notification.approvedStatus = [NSNumber numberWithInteger:[notificationDict[@"ApprovedStatus"]integerValue]];
        notification.notificationDescription = notificationDict[@"NotificationDescription"];
        notification.notificationAttachmentUrl = notificationDict[@"Attachment"];
        notification.notificationDate = [SINGLETON_INSTANCE dateFromStringWithFormate:DATE_FORMATE_SERVICE_RESPONSE withDate:notificationDict[@"NotificationDate"]];
        notification.notificationGroupType = [NSNumber numberWithInteger:[notificationDict[@"GroupType"]integerValue]];
        notification.notificationIsHasAttachment = [NSNumber numberWithInteger:[notificationDict[@"IsAttachment"]integerValue]];
        notification.notificationPriority = [NSNumber numberWithInteger:[notificationDict[@"Priority"]integerValue]];
        notification.notificationTitle = notificationDict[@"NotificationTitle"];
        
        notification.schoolTransId = notificationDict[@"SchoolTransID"];
        notification.notificationTransId = notificationDict[@"TransID"];
        notification.userCreatedTransId = notificationDict[@"CreatedBy"];
        notification.userCreatedName = notificationDict[@"CreatedBy"];
        notification.userModifiedTransId = notificationDict[@"ModifiedBy"];
        notification.userModifiedName = notificationDict[@"ModifiedBy"];
        notification.staffTransId = SINGLETON_INSTANCE.staffTransId;
        notification.modifiedDate = [SINGLETON_INSTANCE dateFromString:notificationDict[@"ModifiedDate"] withFormate:DATE_FORMATE_MODIFIED_DATE];
        notification.staff = SINGLETON_INSTANCE.staff;
        
        notification.isForApprovals = [NSNumber numberWithBool:NO];
        notification.boardName = notificationDict[@"BoardName"];
        
        NSString *assigneeList = [[TSCommonAssignedDetails instance] getAssignedDetailsWithArray:notificationDict[@"SchoolNotificationDetails"] withGroupType:notificationDict[@"GroupType"]];
        
        notification.assignedList = assigneeList;
        
        //[[AssignedDetails instance] saveAssignedDetailsWithDict:notificationDict[@"SchoolNotificationDetails"] withGroupType:notificationDict[@"GroupType"] transId:notification.notificationTransId forObject:notification objectType:OBJECT_TYPE_NOTIFICATION context:context];

        if (![context save:&error]) {
            
            NSLog(@"whoops Could not save ");
        }
    }
    @catch (NSException *exception) {
        
        
    } @finally {
        
        
    }
}

@end
