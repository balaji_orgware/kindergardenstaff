//
//  TSNewPasswordViewController.h
//  Trackidon Staff
//
//  Created by Balaji on 29/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSHeaders.pch"

@interface TSNewPasswordViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UITextField *oldPwdTxtField;

@property (weak, nonatomic) IBOutlet UITextField *newpwdTxtField;

@property (weak, nonatomic) IBOutlet UITextField *confirmPwdTxtField;

@property (nonatomic,strong) NSString *oldPassword;

@end
