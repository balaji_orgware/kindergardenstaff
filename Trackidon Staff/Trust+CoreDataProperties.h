//
//  Trust+CoreDataProperties.h
//  Trackidon Staff
//
//  Created by Elango on 14/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Trust.h"

NS_ASSUME_NONNULL_BEGIN

@interface Trust (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *addressOne;
@property (nullable, nonatomic, retain) NSString *addressThree;
@property (nullable, nonatomic, retain) NSString *addressTwo;
@property (nullable, nonatomic, retain) NSString *baseUrl;
@property (nullable, nonatomic, retain) NSString *contactFaxNo;
@property (nullable, nonatomic, retain) NSString *contactMail;
@property (nullable, nonatomic, retain) NSString *contactPerson;
@property (nullable, nonatomic, retain) NSString *contactPhone;
@property (nullable, nonatomic, retain) NSString *latitude;
@property (nullable, nonatomic, retain) NSString *longtitude;
@property (nullable, nonatomic, retain) NSString *pincode;
@property (nullable, nonatomic, retain) NSString *schoolCode;
@property (nullable, nonatomic, retain) NSData *schoolLogoData;
@property (nullable, nonatomic, retain) NSString *schoolLogoUrl;
@property (nullable, nonatomic, retain) NSString *schoolName;
@property (nullable, nonatomic, retain) NSString *schoolTransId;
@property (nullable, nonatomic, retain) NSString *startDate;
@property (nullable, nonatomic, retain) NSString *trustName;
@property (nullable, nonatomic, retain) NSString *trustTransId;

@end

NS_ASSUME_NONNULL_END
