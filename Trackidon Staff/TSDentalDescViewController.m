//
//  TSDentalDescViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 22/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSDentalDescViewController.h"

@interface TSDentalDescViewController (){
    
    NSString *symptomsString;
}

@end

@implementation TSDentalDescViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);
    _backView.frame = self.view.frame;
    _descView.frame = CGRectMake(_descView.frame.origin.x, _descView.frame.origin.y, DeviceWidth, _descView.frame.size.height);
    
    UITapGestureRecognizer *tapOnBackViewGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOnBackView)];
    [_backView addGestureRecognizer:tapOnBackViewGesture];
    
    [TSSingleton layerDrawForView:_dateView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Helper methods

-(void) tapOnBackView{
    
    [self hideAnimationView];
}

-(void) loadViewWithContentView:(DentalInfoModel *)modelObj{
    
    _dateLabel.text = [SINGLETON_INSTANCE stringFromDate:modelObj.examinedDate withFormate:DATE_FORMATE_MNTH_DAY_YR];
    _examinedByName.text = modelObj.examinedBy;
    
    _intraOralDescLbl.text = modelObj.intraOral;
    _extraOralDescLbl.text = modelObj.extraOral;
    
    if (!modelObj.toothCavity)
        symptomsString = [NSString stringWithFormat:@"- Tooth cavity\n"];
    if (!modelObj.plaque)
        symptomsString = [symptomsString stringByAppendingString:@"- Plaque\n"];
    if (!modelObj.gumBleeding)
        symptomsString = [symptomsString stringByAppendingString:@"- Gum bleeding\n"];
    if (!modelObj.gumInflammation)
        symptomsString = [symptomsString stringByAppendingString:@"- Gum inflammation\n"];
    if (!modelObj.stains)
        symptomsString = [symptomsString stringByAppendingString:@"- Stains\n"];
    if (!modelObj.tarter)
        symptomsString = [symptomsString stringByAppendingString:@"- Tarter\n"];
    if (!modelObj.softTissue)
        symptomsString = [symptomsString stringByAppendingString:@"- Soft Tissue\n"];
    if (!modelObj.badBreath)
        symptomsString = [symptomsString stringByAppendingString:@"- Bad breath\n"];
    
    _alsoHasTxtVw.text = [symptomsString isEqualToString:@""] ? @"---":symptomsString;
    _remarksTxtVw.text = [modelObj.remarks isEqualToString:@""] ? @"---":modelObj.remarks;
    
    _alsoHasTxtVw.font = [UIFont fontWithName:APP_FONT size:15];
    _remarksTxtVw.font = [UIFont fontWithName:APP_FONT size:15];
    
    [_remarksTxtVw scrollRangeToVisible:NSMakeRange(0, 0)];
    [_alsoHasTxtVw scrollRangeToVisible:NSMakeRange(0, 0)];
}

-(void) showDescViewController:(UIViewController *)viewController withModelObj:(DentalInfoModel *)modelObj{
    
    [viewController.view addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    [viewController.view endEditing:YES];
    
    [self loadViewWithContentView:modelObj];
    
    if (symptomsString.length >0) {
        
        _descView.frame = CGRectMake(_descView.frame.origin.x, DeviceHeight, viewController.view.frame.size.width,viewController.view.frame.size.height *3/4);
    }
    else{
        _alsoHasLbl.hidden = YES;
        _alsoHasTxtVw.hidden = YES;
        _remarksLbl.frame = CGRectMake(_alsoHasLbl.frame.origin.x, _alsoHasLbl.frame.origin.y, _remarksLbl.frame.size.width, _remarksLbl.frame.size.height);
        _remarksTxtVw.frame = CGRectMake(_remarksTxtVw.frame.origin.x, _remarksLbl.frame.origin.y+_remarksLbl.frame.size.height+15, _remarksTxtVw.frame.size.width,70);
        
        _contentView.frame = CGRectMake(_contentView.frame.origin.x, _contentView.frame.origin.y, _contentView.frame.size.width, _remarksTxtVw.frame.origin.y+_remarksTxtVw.frame.size.height+10);
        _descView.frame = CGRectMake(_descView.frame.origin.x, DeviceHeight, viewController.view.frame.size.width, _contentView.frame.origin.y+_contentView.frame.size.height);
    }
    
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [_backView setAlpha:0.5];
        _descView.frame = CGRectMake(_descView.frame.origin.x, viewController.view.frame.size.height-_descView.frame.size.height, _descView.frame.size.width, _descView.frame.size.height);
    }];
}

-(void)hideAnimationView{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [_backView setAlpha:0.0];
        _descView.frame = CGRectMake(_descView.frame.origin.x, DeviceHeight,_descView.frame.size.width, _descView.frame.size.height);
        
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

@end
