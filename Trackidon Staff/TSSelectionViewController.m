//
//  TSSelectionViewController.m
//  Trackidon Staff
//
//  Created by ORGware on 22/02/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSSelectionViewController.h"

@interface TSSelectionViewController ()
{
    NSArray *sortedArray;
}
@end

@implementation TSSelectionViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
//    {
//        self.tableView.layer.masksToBounds = NO;
//        self.tableView.layer.shadowColor = [UIColor blackColor].CGColor;
//        self.tableView.layer.shadowOffset = CGSizeMake(2.0f, 0.2f);
//        self.tableView.layer.shadowOpacity = 0.5f;
//    }
    
    self.view.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);
    
    [self.tableView setFrame:CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, DeviceWidth, DeviceHeight/2)];
    
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    
    [self.tableView setDataSource:self];
    
    [self.tableView setDelegate:self];
    
    sortedArray = [[_contentDict allKeys]sortedArrayUsingSelector:@selector(compare:)];
    
    [_backView setFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    
    [_backView setAlpha:0.5];
    
    UITapGestureRecognizer *tapOnBackView = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOnBackViewAction)];
    
    [_backView addGestureRecognizer:tapOnBackView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Methods

-(void) tapOnBackViewAction{
        
    [self hideAnimatedSelectionView];
    
    [self.delegate backViewTappedOn:self];
}

#pragma mark - Table view Datasources & Delegates

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (sortedArray.count>0)
        return sortedArray.count;
    else
        return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    [cell.textLabel setFont:[UIFont fontWithName:APP_FONT size:15]];
    
    cell.textLabel.text = [sortedArray objectAtIndex:indexPath.row];
    
    UIImageView *radioButton = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 15, 15)];
    
    cell.accessoryView = radioButton;
    
    if ([self.selectedValue isEqualToString:[sortedArray objectAtIndex:indexPath.row]]) {
        
        [radioButton setImage:[UIImage imageNamed:@"radio_on"]];
    }
    else{
        
        [radioButton setImage:[UIImage imageNamed:@"radio_off"]];
    }
    
    [tableView setTableFooterView:[UIView new]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.selectedValue = [sortedArray objectAtIndex:indexPath.row];
    
    [tableView reloadData];
    
    _textField.text = self.selectedValue;

    [self.delegate selectedValueFromDict:self.contentDict withSelectedValue:self.selectedValue andTransID:[self.contentDict objectForKey:self.selectedValue] forTextField:_textField];
    
    _textField = nil;
    
    [self hideAnimatedSelectionView];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 30.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView;
    
    headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, tableView.tableHeaderView.frame.size.height)];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *selectLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, 100, 30)];
    selectLabel.text = @"Select";
    selectLabel.textColor = APPTHEME_COLOR;
    selectLabel.font = [UIFont fontWithName:APP_FONT size:14];
    
    [headerView addSubview: selectLabel];
    
    return headerView;
}

#pragma mark - Show Selection View Controller

// Showing Selection view controller above calling view controller
-(void)showSelectionViewController:(UIViewController *)viewController forTextField:(UITextField *)textField{
    
    _textField = textField;
    
    [viewController addChildViewController:self];
    
    [viewController.view addSubview:self.view];
    
    [self didMoveToParentViewController:viewController];
    
    [viewController.view endEditing:YES];
    
    self.tableView.frame = CGRectMake(viewController.view.frame.origin.x, DeviceHeight, viewController.view.frame.size.width, viewController.view.frame.size.height/2);
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [_backView setAlpha:0.5];
        
        if ([APPDELEGATE_INSTANCE tabBarController] && [[[APPDELEGATE_INSTANCE tabBarController] tabBar] isHidden]){
            
            self.tableView.frame = CGRectMake(viewController.view.frame.origin.x, viewController.view.frame.size.height - self.tableView.frame.size.height, viewController.view.frame.size.width, viewController.view.frame.size.height/2);
        }
        else{
            
            self.tableView.frame = CGRectMake(viewController.view.frame.origin.x, viewController.view.frame.size.height - 44 - self.tableView.frame.size.height, viewController.view.frame.size.width, viewController.view.frame.size.height/2);
        }
        
    } completion:nil];

}

-(void)removeSelectionViewController:(UIViewController *)viewController{
    
    [self hideAnimatedSelectionView];
    
    _textField = nil;
}

#pragma mark - Animated view

-(void) showAnimatedSelectionView{
    
    self.view.frame = CGRectMake(0, DeviceHeight, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:nil];
}

-(void) hideAnimatedSelectionView{
    
    [UIView animateWithDuration:0.3 animations:^{
        
//        self.view.frame = CGRectMake(self.view.frame.origin.x, DeviceHeight, self.view.frame.size.width, self.view.frame.size.height);
        
        [self.backView setAlpha:0.0];
        
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, DeviceHeight, self.tableView.frame.size.width, self.tableView.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        
        [self removeFromParentViewController];
    }];
}
@end