//
//  TSNotificationInboxDetailViewController.m
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/4/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSNotificationInboxDetailViewController.h"

@interface TSNotificationInboxDetailViewController ()
{
    NSURL *attachmentURL;

}
@end

@implementation TSNotificationInboxDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [TSSingleton layerDrawForView:_topView position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    
    [TSSingleton layerDrawForView:_dateView position:LAYER_TOP color:[UIColor lightGrayColor]];
    
    _attachmentButton.frame = CGRectMake(DEVICE_SIZE.width - _attachmentButton.frame.size.width - 10, _topView.frame.origin.y + _topView.frame.size.height - _attachmentButton.frame.size.height/2 + 18, _attachmentButton.frame.size.height, _attachmentButton.frame.size.height);
    
    _fromDateLbl.layer.cornerRadius = _fromDateLbl.frame.size.width/2;
    
    _fromDateLbl.layer.masksToBounds = YES;
    
    _notificationPriorityView.layer.cornerRadius = _notificationPriorityView.frame.size.width/2;
    
    _notificationPriorityView.layer.masksToBounds = YES;
    
    
    _titleLbl.text = [NSString stringWithFormat:@"Title : %@",_notificationInboxObj.notificationTitle];
    
    _createdByLbl.text = [NSString stringWithFormat:@"- by %@",_notificationInboxObj.createdBy];
    
    _descriptionTextView.text = _notificationInboxObj.notificationDescription;
    
    _fromDateLbl.attributedText = [self getDateAttributedStringForDateString:[SINGLETON_INSTANCE stringFromDate:_notificationInboxObj.notificationDate withFormate:DATE_FORMATE_SERVICE_RESPONSE] inLabel:_fromDateLbl];
    
    _fromDateLbl.layer.cornerRadius = _fromDateLbl.frame.size.width/2;
    
    _fromDateLbl.layer.masksToBounds = YES;
    
    switch ([_notificationInboxObj.priority integerValue]) {
            
        case 1:
            
            _notificationPriorityTextLbl.text = @"Priority High";
            
            _notificationPriorityView.backgroundColor = [UIColor colorWithRed:1.0 green:45.0/255.0 blue:85.0/255.0 alpha:1.0];
            
            break;
            
        case 2:
            
            _notificationPriorityTextLbl.text = @"Priority Medium";
            
            _notificationPriorityView.backgroundColor = [UIColor colorWithRed:1.0 green:149.0/255.0 blue:0.0/255.0 alpha:1.0];
            
            break;
            
        case 3:
            
            _notificationPriorityTextLbl.text = @"Priority Low";
            
            _notificationPriorityView.backgroundColor = [UIColor colorWithRed:15.0/255.0 green:170.0/255.0 blue:99.0/255.0 alpha:1.0];
            
            break;
            
        default:
            break;
    }
    if ([_notificationInboxObj.isHasAttachment intValue] == 1) {
        
        [_attachmentButton setHidden:NO];
    }
    else{
        
        [_attachmentButton setHidden:YES];
    }


    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.navigationItem.title = @"Notification Description";
}

-(NSMutableAttributedString *) getDateAttributedStringForDateString:(NSString *)dateString inLabel:(UILabel *)label{
    
    [label setNumberOfLines:2];
    
    NSString *dateNo = [SINGLETON_INSTANCE changeDateFormatFromService:dateString withFormat:@"dd"];
    
    NSString *monthName = [SINGLETON_INSTANCE changeDateFormatFromService:dateString withFormat:@"MMM"];
    
    UIFont *dateFont = [UIFont fontWithName:APP_FONT size:25.0];
    NSDictionary *dateTextAttr = [NSDictionary dictionaryWithObject: dateFont forKey:NSFontAttributeName];
    NSMutableAttributedString *countAttrStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@   ",dateNo] attributes: dateTextAttr];
    
    UIFont *monthFont = [UIFont fontWithName:APP_FONT size:12.0];
    NSDictionary *monthTextAttr = [NSDictionary dictionaryWithObject:monthFont forKey:NSFontAttributeName];
    NSMutableAttributedString *textAttrStr = [[NSMutableAttributedString alloc]initWithString:monthName attributes:monthTextAttr];
    
    [countAttrStr appendAttributedString:textAttrStr];
    
    return countAttrStr;
}

- (IBAction)attachmentBtnClk:(id)sender {
    
    [_attachmentButton setUserInteractionEnabled:NO];
    
    NSString *attachmentURLString;
    
    attachmentURL = [NSURL URLWithString:_notificationInboxObj.notificationAttachment];
    
    attachmentURLString = _notificationInboxObj.notificationAttachment;
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *attachmentFile = [documentsPath stringByAppendingPathComponent:[attachmentURL lastPathComponent]];
    
    NSLog(@"%@",attachmentFile);
    
    NSLog(@"%@",[attachmentURL lastPathComponent]);
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:attachmentFile];
    
    if (fileExists) {
        
        [self navigateToAttachmentViewControllerWithFileName:attachmentURL];
    }
    else{
        
        
        
        [[TSHandlerClass sharedHandler]downloadAttachmentWithURL:attachmentURLString currentView:self.view withHandler:^(BOOL success, NSURL *filePath) {
            
            if (success) {
                
                [self navigateToAttachmentViewControllerWithFileName:filePath];
            }
            else{
                
                _attachmentButton.userInteractionEnabled = YES;
            }
        }];
    }
}

-(void) navigateToAttachmentViewControllerWithFileName:(NSURL *)fileURL{
    
    TSAttachmentViewController *attachmentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SBAttachment"];
    
    [attachmentVC setFileName:[fileURL lastPathComponent]];
    
    UINavigationController *navCtrl = [[UINavigationController alloc]initWithRootViewController:attachmentVC];
    
    navCtrl.navigationBar.translucent = NO;
    
    navCtrl.navigationBar.barTintColor = APPTHEME_COLOR;
    
    navCtrl.navigationBar.tintColor = [UIColor whiteColor];
    
    [navCtrl.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:APP_FONT size:18]}];
    
    [self.navigationController presentViewController:navCtrl animated:YES completion:nil];
}


@end
