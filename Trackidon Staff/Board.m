//
//  Board.m
//  Trackidon Staff
//
//  Created by Elango on 18/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "Board.h"
#import "School.h"

@implementation Board

+ (instancetype)instance{
    
    static Board *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (&onceToken, ^{
        
        kSharedInstance = [[Board alloc]init];
        
    });
    
    return kSharedInstance;
    
}

- (void)saveBoardAndClassDetailsWithResponseDict:(NSDictionary *)dict schoolTransId:(NSString *)schoolTransId{
    
    NSLog(@"%@",dict);
    
    NSManagedObjectContext *context = [[TSAppDelegate instance] managedObjectContext];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"schoolTransId contains[cd] %@",schoolTransId];
    
    NSArray *schoolArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"School" withPredicate:predicate];
    
    School *school;
    
    if (schoolArr.count >0)
        school = [schoolArr objectAtIndex:0];

    
    [SINGLETON_INSTANCE deleteSelectedObjectsInEntityWithEntityName:@"Board" withPredicate:predicate withContext:[APPDELEGATE_INSTANCE managedObjectContext]];
    
    @try {
        
        for (NSDictionary *boardDict in dict[@"FetchMastersbySchoolResult"][@"MasterRefBoard"]) {
            
            //NSDictionary *boardDict = [tmpBoardDict dictionaryByReplacingNullsWithBlanks];
            
            //Board *board;
            
            //board = [self checkBoardAvailablibilityWithBoardTransId:boardDict[@"BoardTransID"] withContext:context];
            
            //if (board == nil) {
                Board *board = [NSEntityDescription insertNewObjectForEntityForName:@"Board" inManagedObjectContext:context];
            //}
            
            board.boardName = boardDict[@"BoardName"] != [NSNull null] ? boardDict[@"BoardName"] : @"";
            
            board.boardTransId = boardDict[@"BoardTransID"]!= [NSNull null] ? boardDict[@"BoardTransID"] : @"";
            
            board.schoolTransId = boardDict[@"SchoolTransID"] != [NSNull null] ? boardDict[@"SchoolTransID"] : @"";
            
            board.schoolName = boardDict[@"SchoolName"] != [NSNull null] ? boardDict[@"SchoolName"] : @"";
            
            board.school = school;
            
            board.isViewableByStaff = [NSNumber numberWithBool:[self checkIsBoardViewableByStaffWithBoardTransId:board.boardTransId schoolTransId:board.schoolTransId]];
            
            //NSMutableSet *classSet = [board valueForKey:@"classes"];
            
            if (boardDict[@"ClassList"] != [NSNull null]) {
            
            for (NSDictionary *classDict in boardDict[@"ClassList"]) {
                
                //NSDictionary *classDict = [tmpClassDict dictionaryByReplacingNullsWithBlanks];
                
                Classes *class;
                
                class = [self checkClassAvailablibilityWithClassTransId:classDict[@"ClassTransID"] withContext:context];
                
                if (class == nil) {
                    
                    class = [NSEntityDescription insertNewObjectForEntityForName:@"Classes" inManagedObjectContext:context];
                }
                
                class.boardTransId = classDict[@"BoardTransID"] != [NSNull null] ? classDict[@"BoardTransID"] : @"";
                
                class.classStandardName = classDict[@"ClassName"]!= [NSNull null] ? classDict[@"ClassName"] : @"";
                
                class.classTransId = classDict[@"ClassTransID"]!= [NSNull null] ? classDict[@"ClassTransID"] : @"";
                
                class.schoolName = classDict[@"SchoolName"]!= [NSNull null] ? classDict[@"SchoolName"] : @"";
                
                class.schoolTransId = classDict[@"SchoolTransID"]!= [NSNull null] ? classDict[@"SchoolTransID"] : @"";
                
                class.boardName = board.boardName;
                
                class.board = board;
                
                
                if (classDict[@"SectionList"] != [NSNull null]) {
                    
                    //NSMutableSet *sectionSet = [class valueForKey:@"section"];
                    
                    for (NSDictionary *tmpSectionDict in classDict[@"SectionList"]) {
                        
                        NSDictionary *sectionDict = [tmpSectionDict dictionaryByReplacingNullsWithBlanks];
                        
                        Section *section;
                        
                        section = [self checkSectionAvailablibilityWithSectionTransId:sectionDict[@"SectionTransID"] withContext:context];
                        
                        if (section == nil) {
                            section = [NSEntityDescription insertNewObjectForEntityForName:@"Section" inManagedObjectContext:context];
                        }
                        
                        section.boardTransId = sectionDict[@"BoardTransID"];
                        section.classTransId = sectionDict[@"ClassTransID"];
                        section.classStandardName = class.classStandardName;
                        section.schoolName = sectionDict[@"SchoolName"];
                        section.schoolTransId = sectionDict[@"SchoolTransID"];
                        section.sectionName = sectionDict[@"SectionName"];
                        section.sectionTransId = sectionDict[@"SectionTransID"];
                        
                        section.boardName = board.boardName;
                        
                        section.classes = class;
                        
                        //[sectionSet addObject:section];
                        
                        //Section Save
                    }
                }
                
                //[classSet addObject:sectionSet];
                
                //Class Save
            }
                
            }
            
            //[board addClasses:classSet];
            
            //Board Save
            
            //[school addBoard:boardSet];
        }
        
        
        
        NSError *error;
        
        if (![context save:&error]) {
            
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    
}

-(Board *)checkBoardAvailablibilityWithBoardTransId:(NSString *)boardTransId withContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Board" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"boardTransId contains[cd] %@", boardTransId];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
    
}

-(Classes *)checkClassAvailablibilityWithClassTransId:(NSString *)classTransId withContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Classes" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"classTransId contains[cd] %@", classTransId];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
}

-(Section *)checkSectionAvailablibilityWithSectionTransId:(NSString *)sectionTransId withContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Section" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"sectionTransId contains[cd] %@", sectionTransId];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
}

- (BOOL)checkIsBoardViewableByStaffWithBoardTransId:(NSString *)boardTransId schoolTransId:(NSString *)schoolTransId{
        
    NSPredicate *boardPredicate = [NSPredicate predicateWithFormat:@"schoolTransId contains[cd] %@ AND boardTransId contains[cd] %@",schoolTransId, boardTransId];
    
    NSArray *boardArr = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"BoardList" withPredicate:boardPredicate];
    
    if (boardArr.count > 0)
        return YES;
    else
        return NO;
    
}


@end
