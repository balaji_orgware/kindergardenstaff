//
//  TSRemoteNotificationHandler.h
//  Trackidon Staff
//
//  Created by Balaji on 12/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSRemoteNotificationHandler : NSObject <UIAlertViewDelegate>

// Class method to Get instance of Remote Notification Handler
+(instancetype) instance;

// Method to handle Remote Notification with given Payload
-(void) handleNotificationWithPayload:(NSDictionary *)payload;

@property(nonatomic,strong) NSDictionary *payloadDictionary;

@end
