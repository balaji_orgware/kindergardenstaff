//
//  StaffDetails.m
//  Trackidon Staff
//
//  Created by Elango on 16/03/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "StaffDetails.h"

@implementation StaffDetails

+ (instancetype)instance{
    
    static StaffDetails *kSharedInstance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once (&onceToken, ^{
        
        kSharedInstance = [[StaffDetails alloc]init];
        
    });
    
    return kSharedInstance;
}




- (void)saveStaffDetailsWithDict:(NSDictionary *)dict{
    
    NSManagedObjectContext *context = [[TSAppDelegate instance] managedObjectContext];
    
    StaffDetails *staff;
    
    @try {
        
    NSDictionary *staffDict = dict[@"FetchAccountLoginResult"][@"SelectAccountClass"][@"UserDetails"];
        
        staffDict = [staffDict dictionaryByReplacingNullsWithBlanks];
    
    NSDictionary *schoolListDict = dict[@"FetchAccountLoginResult"][@"SelectAccountClass"][@"SchoolList"];
    
    staff = [self checkAvailablibilityWithStaffTransId:staffDict[@"TransID"] withContext:context];
    
    if (staff == nil) {
        staff=[NSEntityDescription insertNewObjectForEntityForName:@"StaffDetails" inManagedObjectContext:[[TSAppDelegate instance] managedObjectContext]];
    }
    
    staff.addressOne = staffDict[@"Address1"];
    staff.addressTwo = staffDict[@"Address2"];
    staff.addressThree = staffDict[@"Address3"];
    staff.emailId = staffDict[@"EmailID"];
    staff.inChargeClassName = staffDict[@"InchargeClass"];
    staff.inChargeClassTransId = staffDict[@"InchargeClassTransID"];
    staff.inChargeSectionName = staffDict[@"InchargeSection"];
    staff.inChargeSectionTransId = staffDict[@"InchargeSecTransID"];
    staff.mobileNo = staffDict[@"MobileNo"];
    staff.name = staffDict[@"Name"];
    staff.pincode = staffDict[@"Pincode"];
    staff.profileImageUrl = staffDict[@"ProfileImage"];
    staff.schoolTransId = staffDict[@"SchoolTransID"];
    staff.staffClassName = staffDict[@"ClassName"];
    staff.staffRole = [NSString stringWithFormat:@"%@",staffDict[@"UserRole"]];
    staff.staffTransId = staffDict[@"TransID"];
    staff.acadamicTransId = staffDict[@"AcadamicTransID"];
    staff.acadamicYearTransId = staffDict[@"AcademicYearTransID"];
        
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (staffDict[@"ProfileImage"] != [NSNull null]) {
            
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:staffDict[@"ProfileImage"]]];
            
            UIImage *image = [UIImage imageWithData:imageData];
            
            if(image != nil)
                staff.profileImageData = imageData;
        }
        
    });
        
        if (dict[@"FetchAccountLoginResult"][@"SelectAccountClass"][@"UserDetails"][@"StaffSchoolConfigClass"][@"StaffSchoolConfigClass"] != [NSNull null]) {
            
            [SINGLETON_INSTANCE deleteAllObjectsInEntity:@"BoardList"];
            
            for (NSDictionary *boardDict in dict[@"FetchAccountLoginResult"][@"SelectAccountClass"][@"UserDetails"][@"StaffSchoolConfigClass"][@"StaffSchoolConfigClass"]) {
                
                BoardList *board = [NSEntityDescription insertNewObjectForEntityForName:@"BoardList" inManagedObjectContext:context];
                
                board.boardTransId = boardDict[@"BoardTransID"];
                
                board.schoolTransId = boardDict[@"SchoolTransID"];
                
                board.staff = staff;

            }
            
        }
        
    NSMutableSet *schoolSet = [staff valueForKey:@"school"];
    
    for (NSDictionary *dict in schoolListDict) {
        
        School *school;
                
        NSDictionary *schoolDict = [dict dictionaryByReplacingNullsWithBlanks];
        
        school = [self checkSchoolAvailablibilityWithSchoolTransId:schoolDict[@"SchoolTransID"] withContext:context];
        
        if (school == nil) {
            school=[NSEntityDescription insertNewObjectForEntityForName:@"School" inManagedObjectContext:context];
        }
        
        school.staffUserMannualUrl = schoolDict[@"ParentUserManual"];
        school.schoolCode = schoolDict[@"SchoolCode"];
        school.latitude = schoolDict[@"SchoolLat"];
        school.longtitude = schoolDict[@"SchoolLong"];
        school.schoolLogoUrl = schoolDict[@"SchoolLogo"];
        school.schoolName = schoolDict[@"SchoolName"];
        school.schoolTransId = schoolDict[@"SchoolTransID"];
        school.staffTransId = staffDict[@"TransID"];
        school.schoolAboutUs = schoolDict[@"AboutUs"];
        school.schoolContactNo = schoolDict[@"ContactNo"];
        school.schoolEmailID = schoolDict[@"EmailID"];

        school.staff = staff;
        
        if (schoolDict[@"SchoolLogo"] != nil) {
            
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:schoolDict[@"SchoolLogo"]]];
            
            school.schoolLogoData = imageData;
        }
        
        [staff addSchoolObject:school];
        
        NSDictionary *menuListDict = schoolDict[@"MobileMenusMClassResult"][@"MobileMenusClass"];
        
        NSMutableSet *menuSet = [school valueForKey:@"menu"];
        
        if([schoolDict[@"MobileMenusMClassResult"][@"ResponseCode"] integerValue] == 1)
        {
        
        for (NSDictionary *menuTmpDict in menuListDict) {
            
            NSDictionary *menuDict =[menuTmpDict dictionaryByReplacingNullsWithBlanks];
            
            Menu *menu;
            
            menu = [self checkMenuAvailablibilityWithMenuTransId:menuDict[@"TransID"] schoolTransId:schoolDict[@"SchoolTransID"] withContext:context];
            
            if (menu == nil) {
                
                menu=[NSEntityDescription insertNewObjectForEntityForName:@"Menu" inManagedObjectContext:context];
            }
            
            menu.schoolTransId = schoolDict[@"SchoolTransID"];
            
            menu.isShow = [NSNumber numberWithInteger:[menuDict[@"IsShow"] integerValue]];
            
            menu.isModule = [NSNumber numberWithInteger:[menuDict[@"IsModule"] integerValue]];
            
            menu.menuTransId = menuDict[@"TransID"];
            
            menu.moduleId = [NSString stringWithFormat:@"%@", menuDict[@"ModuleID"]];
            menu.moduleName = menuDict[@"ModuleName"];
            menu.modulePriority = [NSString stringWithFormat:@"%@",menuDict[@"ModulePriority"] ];
            menu.school = school;
            
            if ([menuDict[@"Submenus"] count] > 0)
            {
                menu.isHasSubMenu = [NSNumber numberWithBool:YES];
                
                NSDictionary *subMenuList = menuDict[@"Submenus"];
                
                for (NSDictionary *subMenuDict in subMenuList) {
                    
                    Menu *subMenu;
                    
                    subMenu = [self checkSubMenuAvailablibilityWithSchoolTransId:schoolDict[@"SchoolTransID"] menuTransId:subMenuDict[@"TransID"] withContext:context];
                    
                    if (subMenu == nil) {
                        
                        subMenu=[NSEntityDescription insertNewObjectForEntityForName:@"Menu" inManagedObjectContext:context];
                    }
                    
                    subMenu.schoolTransId = schoolDict[@"SchoolTransID"];
                    subMenu.isShow = [NSNumber numberWithInteger:[subMenuDict[@"IsShow"] integerValue]];
                    subMenu.isModule = [NSNumber numberWithInteger:[subMenuDict[@"IsModule"] integerValue]];
                    subMenu.menuTransId = subMenuDict[@"TransID"];
                    subMenu.moduleId = [NSString stringWithFormat:@"%@", subMenuDict[@"ModuleID"]];
                    subMenu.moduleName = subMenuDict[@"ModuleName"];

                    subMenu.modulePriority = [NSString stringWithFormat:@"%@",subMenuDict[@"ModulePriority"] ];
                    subMenu.school = school;
                    
                    if (![subMenuDict[@"IsModule"] boolValue]) {
                        
                        subMenu.menuId = [NSString stringWithFormat:@"%@", subMenuDict[@"MenuID"]];
                        subMenu.menuName = subMenuDict[@"MenuName"];
                        subMenu.menuPriority =[NSString stringWithFormat:@"%@",subMenuDict[@"MenuPriority"] ] ;
                        subMenu.moduleTransId = subMenuDict[@"ModuleTransID"];
                        subMenu.isHasSubMenu = [NSNumber numberWithBool:NO];
                    }
                    
                    [menuSet addObject:subMenu];
                    
                }
                
            }else {
                menu.isHasSubMenu = [NSNumber numberWithBool:NO];
            }
            
            [menuSet addObject:menu];
            
            [school addMenu:menuSet];
            
        }
            
        }
        
        [schoolSet addObject:school];
        
        [staff addSchool:schoolSet];
        
    }
    
    NSError *error;
    
    if (![context save:&error]) {
        
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
        
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception);
        
    }
}

-(StaffDetails *)checkAvailablibilityWithStaffTransId:(NSString *)staffTransId withContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSLog(@"Documents Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"StaffDetails" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"staffTransId contains[cd] %@", staffTransId];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
    
}

-(School *)checkSchoolAvailablibilityWithSchoolTransId:(NSString *)schoolTransId withContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"School" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"schoolTransId contains[cd] %@", schoolTransId];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
    
}

-(Menu *)checkMenuAvailablibilityWithMenuTransId:(NSString *)menuTransId schoolTransId:(NSString *)schoolTransId withContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Menu" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"schoolTransId contains[cd] %@ AND menuTransId contains[cd] %@", schoolTransId,menuTransId];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
    
}

-(Menu *)checkSubMenuAvailablibilityWithSchoolTransId:(NSString *)schoolTransId menuTransId:(NSString *)menuTransId withContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Menu" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"schoolTransId contains[cd] %@ AND menuTransId contains[cd] %@", schoolTransId,menuTransId];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
    
}

-(StaffDetails *)checkAvailablibilityWithParentTransId:(NSString *)staffTransId{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSLog(@"Documents Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"StaffDetails" inManagedObjectContext:[[TSAppDelegate instance] managedObjectContext]];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"staffTransId contains[cd] %@", staffTransId];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    [fetchRequest setPredicate:pred];
    
    NSError* error;
    
    NSArray *fetchedRecords = [[[TSAppDelegate instance] managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedRecords.count>0)
    {
        return [fetchedRecords objectAtIndex:0];
    }
    else
    {
        return nil;
    }
    
}


-(void)updateStaffImageWithData:(NSData *)imageData parentTransId:(NSString *)transID{
    
    NSManagedObjectContext *context = [[TSAppDelegate instance] managedObjectContext];
    
    StaffDetails *staff;
    
    staff = [self checkAvailablibilityWithParentTransId:transID];
    
    if (staff != nil) {
        
        staff.profileImageData = imageData;
    }
    
    NSError *error;
    
    if (![context save:&error]) {
        
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
}

@end
