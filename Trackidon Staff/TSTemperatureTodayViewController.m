//
//  TSTemperatureTodayViewController.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 11/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSTemperatureTodayViewController.h"

@interface TSTemperatureTodayViewController (){
    
    BOOL isRaisedTableView;
    
    float raisedHeight;
    
    NSArray *boardArray;
    
    NSMutableArray *studentsListArray;
    
    NSString *selectedBoardTransID,*selectedClassTransID,*selectedSectionTransID,*trustTransID;
    
    NSInteger chosenSession;
    
    TSSelectionViewController *selectionVC;
    
    CGRect viewRect, currentVisibleRect;
    
    UIView *currentResponder;
}

@end

@implementation TemperatureStudModel

@end

@implementation TSTemperatureTodayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    viewRect = self.view.frame;
    
    raisedHeight = 0.0;
    
    isRaisedTableView = NO;
    
    [_studentTableView setTableFooterView:[UIView new]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"schoolTransId MATCHES %@",SINGLETON_INSTANCE.selectedSchoolTransId];
    
    NSArray *trustArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Trust" withPredicate:predicate];
    
    if (trustArray.count>0) {
        Trust *trustObj = [trustArray objectAtIndex:0];
        trustTransID = trustObj.trustTransId;
    }
    else{
        trustTransID = @"";
    }
    
    chosenSession = Session_FORENOON;
    
    _sessionTxtFld.text = @"Forenoon";
    
    studentsListArray = [NSMutableArray new];
    
    [SINGLETON_INSTANCE addLeftInsetToTextField:_boardTxtFld];
    [SINGLETON_INSTANCE addLeftInsetToTextField:_classTxtFld];
    [SINGLETON_INSTANCE addLeftInsetToTextField:_sectionTxtFld];
    [SINGLETON_INSTANCE addLeftInsetToTextField:_sessionTxtFld];
    
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_boardTxtFld];
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_classTxtFld];
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_sectionTxtFld];
    [SINGLETON_INSTANCE addDropDownArrowToTextField:_sessionTxtFld];
    
    [TSSingleton layerDrawForView:_boardTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_classTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_sectionTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_sessionTxtFld position:LAYER_BOTTOM color:[UIColor lightGrayColor]];
    [TSSingleton layerDrawForView:_classTxtFld position:LAYER_RIGHT color:[UIColor lightGrayColor]];
    
    NSPredicate *boardPredicate = [NSPredicate predicateWithFormat:@"schoolTransId contains[cd] %@ AND isViewableByStaff == 1",SINGLETON_INSTANCE.selectedSchoolTransId];
    
    boardArray = [SINGLETON_INSTANCE retrieveSelectedObjectsInEntityWithEntityName:@"Board" withPredicate:boardPredicate];
    
    if (boardArray.count == 1) {
        Board *boardObj = [boardArray objectAtIndex:0];
        
        _boardTxtFld.text = boardObj.boardName;
        selectedBoardTransID = boardObj.boardTransId;
        
        _selectionView.frame = CGRectMake(_selectionView.frame.origin.x, _selectionView.frame.origin.y-_boardTxtFld.frame.size.height, _selectionView.frame.size.width, _selectionView.frame.size.height);
        
        _studentTableView.frame = CGRectMake(_studentTableView.frame.origin.x, _selectionView.frame.origin.y+_selectionView.frame.size.height, _studentTableView.frame.size.width, _studentTableView.frame.size.height+_boardTxtFld.frame.size.height);
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = @"";
    self.navigationItem.title = @"Temperature Today";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Helper methods

-(BOOL) checkIncompleteTempArray{
    
    BOOL incompleteCheck = NO;
    
    for (TemperatureStudModel *studentObj in studentsListArray) {
        
        if (chosenSession == Session_FORENOON) {
            
            if ([studentObj.morningTemperature isEqualToString:@""]) {
                incompleteCheck = YES;
                break;
            }
            else{
                incompleteCheck = NO;
            }
        }
        else{
            
            if ([studentObj.eveningTemperature isEqualToString:@""]) {
                incompleteCheck = YES;
                break;
            }
            else{
                incompleteCheck = NO;
            }
        }
    }
    
    return incompleteCheck;
}

-(void) keyboardWillShown:(NSNotification *)notification{
    
    NSDictionary* info = [notification userInfo];
    
    CGSize currentKeyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    CGRect textFieldFrame;
    
    textFieldFrame = CGRectMake(currentResponder.frame.origin.x, [_studentTableView rowHeight]*([currentResponder.accessibilityHint integerValue]+1) ,currentResponder.frame.size.width, currentResponder.frame.size.height);
    
    CGPoint textFieldOrigin = CGPointMake(currentResponder.frame.origin.x, currentResponder.frame.origin.y+textFieldFrame.origin.y);
    textFieldOrigin.y = textFieldOrigin.y + currentResponder.frame.size.height;
    CGFloat textFieldHeight = currentResponder.frame.size.height;
    
    CGRect visibleRect = viewRect;
    visibleRect.size.height -= currentKeyboardSize.height + textFieldHeight;
    currentVisibleRect = visibleRect;
    
    if (!CGRectContainsPoint(visibleRect, textFieldOrigin))
    {
        isRaisedTableView = YES;
        
        raisedHeight = textFieldOrigin.y -visibleRect.size.height +textFieldHeight;
        
        CGPoint scrollPoint = CGPointMake(0.0, raisedHeight);
        
        [_studentTableView setContentOffset:scrollPoint animated:YES];
    }
    else{
        isRaisedTableView = NO;
    }
}

-(void) keyboardWillHide:(NSNotification *)notification{
    
    if (isRaisedTableView) {
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[currentResponder.accessibilityHint integerValue] inSection:0];
        
        [_studentTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

#pragma mark - Button actions

- (IBAction)onDoneButtonTapped:(id)sender {

    if (!_studentTableView.isHidden) {
        
        if(![self checkIncompleteTempArray]) {
            
            NSMutableArray *temperatureArray = [NSMutableArray new];
            
            for (TemperatureStudModel *studentObj in studentsListArray) {
                
                NSMutableDictionary *studentDict = [NSMutableDictionary new];
                [studentDict setValue:[studentObj.temperatureTransId isEqualToString:@""] ? @"00000000-0000-0000-0000-000000000000" : studentObj.temperatureTransId forKey:@"TemperatureID"];
                [studentDict setValue:trustTransID forKey:@"TrustID"];
                [studentDict setValue:SINGLETON_INSTANCE.selectedSchoolTransId forKey:@"SchoolID"];
                [studentDict setValue:selectedBoardTransID forKey:@"BoardID"];
                [studentDict setValue:selectedClassTransID forKey:@"ClassID"];
                [studentDict setValue:selectedSectionTransID forKey:@"SectionID"];
                [studentDict setValue:studentObj.studentTransID forKey:@"StudentID"];
                [studentDict setValue:[SINGLETON_INSTANCE stringFromDate:[NSDate date] withFormate:@"dd-MMMM-yyyy"]  forKey:@"TakenDate"];
                [studentDict setValue:[studentObj.morningTemperature isEqualToString: @""] ? @"0 C":studentObj.morningTemperature forKey:@"MorningTemperature"];
                [studentDict setValue:[studentObj.eveningTemperature isEqualToString: @""] ? @"0 C":studentObj.eveningTemperature forKey:@"EveningTemperature"];
                [studentDict setValue:@"R" forKey:@"Smilies"];
                [studentDict setValue:SINGLETON_INSTANCE.staffTransId forKey:@"UserTransID"];
                
                [temperatureArray addObject:studentDict];
            }
            
            NSDictionary *param = @{@"ObjUpdateTemperature":temperatureArray};
            
            [self updateTemperatureServiceCall:param];
        }
        else{
            [TSSingleton showAlertWithMessage:@"Please fill all students temperature and try again."];
        }
    }
    else{
        [TSSingleton showAlertWithMessage:@"No Records to update"];
    }
}

#pragma mark - Selection View 

-(void)showSelectionViewControllerWithSelectedTextField:(UITextField *)textField withSelectedValue:(NSString *)selected{
    
    if (!selectionVC) {
        
        selectionVC = [[TSSelectionViewController alloc]initWithNibName:@"TSSelectionViewController" bundle:nil];
        
        selectionVC.delegate = self;
        
        NSMutableDictionary *contentDict = [NSMutableDictionary new];
        
        if (textField == _boardTxtFld) {
            
            for (Board *boardObj  in boardArray) {
                
                [contentDict setValue:boardObj.boardTransId forKey:boardObj.boardName];
            }
        }
        else if (textField == _classTxtFld){
            
            NSPredicate *classPredicate = [NSPredicate predicateWithFormat:@"boardTransId MATCHES %@",selectedBoardTransID];
            Board *boardObj = [boardArray filteredArrayUsingPredicate:classPredicate].count>0 ? [[boardArray filteredArrayUsingPredicate:classPredicate] objectAtIndex:0] : nil;
            
            for (Classes *classObj in boardObj.classes) {
                [contentDict setValue:classObj.classTransId forKey:classObj.classStandardName];
            }
        }
        else if (textField == _sectionTxtFld){
            
            NSPredicate *boardPredicate = [NSPredicate predicateWithFormat:@"boardTransId MATCHES %@",selectedBoardTransID];
            Board *boardObj = [boardArray filteredArrayUsingPredicate:boardPredicate].count>0 ? [[boardArray filteredArrayUsingPredicate:boardPredicate] objectAtIndex:0] : nil;
            
            NSPredicate *classPredicate = [NSPredicate predicateWithFormat:@"classTransId MATCHES %@",selectedClassTransID];
            
            Classes *classObj = [[boardObj.classes filteredSetUsingPredicate:classPredicate] allObjects].count >0 ? [[[boardObj.classes filteredSetUsingPredicate:classPredicate] allObjects] objectAtIndex:0] : nil;
            
            for (Section *secObj in classObj.section) {
                
                [contentDict setValue:secObj.sectionTransId forKey:secObj.sectionName];
            }
        }
        else if (textField == _sessionTxtFld){
         
            [contentDict setValue:@"" forKey:@"Forenoon"];
            [contentDict setValue:@"" forKey:@"Afternoon"];
        }
        
        if (contentDict.allKeys.count >0) {
            
            [selectionVC setContentDict:contentDict];
            [selectionVC setSelectedValue:selected];
            
            [selectionVC showSelectionViewController:self forTextField:textField];
        }
        else{
            if (textField == _boardTxtFld) {
                
                [TSSingleton showAlertWithMessage:@"No boards to Choose"];
            }
            else if (textField == _classTxtFld){
                
                [TSSingleton showAlertWithMessage:@"No Classes to Choose"];
            }
            else if (textField == _sectionTxtFld){
                
                [TSSingleton showAlertWithMessage:@"No Sections to Choose"];
            }
            selectionVC = nil;
        }
    }
}

#pragma mark - SelectionCallBack Delegate

-(void)selectedValueFromDict:(NSDictionary *)contentDict withSelectedValue:(NSString *)selectedValue andTransID:(NSString *)transID forTextField:(UITextField *)textField{
    
    if (textField == _boardTxtFld) {
        
        selectedBoardTransID = transID;
    }
    else if (textField  == _classTxtFld){
        
        selectedClassTransID = transID;
    }
    else if(textField == _sectionTxtFld){
        
        selectedSectionTransID = transID;
        
        [self fetchTempBySectionAPICall];
    }
    else if(textField == _sessionTxtFld){
     
        if ([selectedValue isEqualToString:@"Forenoon"])
            chosenSession = Session_FORENOON;
        else
            chosenSession = Session_AFTERNOON;
        
        if (studentsListArray.count >0) {
            
            [self.studentTableView setHidden:NO];
            [self.studentTableView reloadData];
        }
        else{
            [self.studentTableView setHidden:YES];
        }
    }
    selectionVC = nil;
}

-(void)backViewTappedOn:(UIViewController *)viewController{
    selectionVC = nil;
}

#pragma mark - TableView Datasource & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return studentsListArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:100];
    UITextField *tempTxtFld = (UITextField *)[cell viewWithTag:101];
    
    tempTxtFld.delegate = self;
    tempTxtFld.accessibilityHint = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    [TSSingleton layerDrawForView:tempTxtFld position:LAYER_LEFT color:[UIColor lightGrayColor]];
    
    [SINGLETON_INSTANCE addDoneButtonForTextField:tempTxtFld];
    
    TemperatureStudModel *studentObj = [studentsListArray objectAtIndex:indexPath.row];
    
    nameLabel.text = studentObj.studentName;
    
    if (chosenSession == Session_FORENOON) {
        
        tempTxtFld.text = studentObj.morningTemperature;
    }
    else{
        tempTxtFld.text = studentObj.eveningTemperature;
    }
    
    return cell;
}

#pragma mark - TextField Delegates

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField == _boardTxtFld || textField == _classTxtFld || textField == _sectionTxtFld) {
        if (textField == _boardTxtFld) {
            
            if (boardArray.count == 1) {
                
                return NO;
            }
            else{
                _classTxtFld.text = @"";
                _sectionTxtFld.text = @"";
                selectedClassTransID = nil;
                selectedSectionTransID = nil;
            }
        }
        else if (textField == _classTxtFld){
            
            if (_boardTxtFld.text.length == 0) {
                [TSSingleton showAlertWithMessage:@"Please Choose Board"];
                return  NO;
            }
            else{
                _sectionTxtFld.text = @"";
                selectedSectionTransID = nil;
            }
        }
        else if (textField == _sectionTxtFld){
            
            if (_boardTxtFld.text.length == 0) {
                [TSSingleton showAlertWithMessage:@"Please Choose Board"];
                return  NO;
            }
            else if (_classTxtFld.text.length == 0){
                [TSSingleton showAlertWithMessage:@"Please Choose Class"];
                return  NO;
            }
        }
        
        _sectionTxtFld.text = @"";
        
        [studentsListArray removeAllObjects];
        
        [self.studentTableView reloadData];
        
        [self showSelectionViewControllerWithSelectedTextField:textField withSelectedValue:textField.text];
        
        return NO;
    }
    else if (textField == _sessionTxtFld){
     
        [self showSelectionViewControllerWithSelectedTextField:textField withSelectedValue:textField.text];
        
        return NO;
    }
    else{
        
        currentResponder = textField;
        
        textField.text = @"";
        
        return YES;
    }
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    if (!(textField == _boardTxtFld || textField == _classTxtFld || textField == _sectionTxtFld || textField == _sessionTxtFld)) {
        
        if (!([textField.text rangeOfString:@"C"].location != NSNotFound) && textField.text.length>0) {
            
            textField.text = [NSString stringWithFormat:@"%@ C",textField.text];
            TemperatureStudModel *studentObj = [studentsListArray objectAtIndex:[textField.accessibilityHint integerValue]];
            
            if (chosenSession == Session_FORENOON) {
                studentObj.morningTemperature = textField.text;
            }
            else{
                studentObj.eveningTemperature = textField.text;
            }
        }
        else {
            TemperatureStudModel *studentObj = [studentsListArray objectAtIndex:[textField.accessibilityHint integerValue]];
            if (chosenSession == Session_FORENOON)
                textField.text = studentObj.morningTemperature;
            else
                textField.text = studentObj.eveningTemperature;
        }
    }
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Service Call

-(void) fetchTempBySectionAPICall{
    
    NSString *urlString = @"HealthDetails/Services/TemperatureService.svc/FetchTemperatureBySection";
    
    NSDictionary *params = @{@"ClassID":selectedClassTransID,@"SectionID":selectedSectionTransID,@"FromDate":[SINGLETON_INSTANCE stringFromDate:[NSDate date] withFormate:DATE_FORMATE_SERVICE_CALL]};
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:params currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"FetchTemperatureBySectionResult"][@"ResponseCode"] integerValue] == 1) {
                
                [studentsListArray removeAllObjects];
                
                for (NSDictionary *tmpDict in responseDictionary[@"FetchTemperatureBySectionResult"][@"TemperatureSection"]) {
                    
                    NSDictionary *dict = [tmpDict dictionaryByReplacingNullsWithBlanks];
                    
                    TemperatureStudModel *studentObj = [TemperatureStudModel new];
                    
                    studentObj.studentName = dict[@"StudentName"];
                    studentObj.studentTransID = dict[@"StudentID"];
                    studentObj.morningTemperature = dict[@"MorningTemperature"];
                    studentObj.eveningTemperature = dict[@"EveningTemperature"];
                    studentObj.temperatureTransId = dict[@"TemperatureID"];
                    studentObj.takenDate = [SINGLETON_INSTANCE dateFromString:dict[@"TakenDate"] withFormate:DATE_FORMATE_SERVICE_RESPONSE];
                    studentObj.createdDate = [SINGLETON_INSTANCE dateFromString:dict[@"DateTimeCreated"] withFormate:DATE_FORMATE_SERVICE_RESPONSE];
                    studentObj.modifiedDate = [SINGLETON_INSTANCE dateFromString:dict[@"DateTimeModified"] withFormate:DATE_FORMATE_SERVICE_RESPONSE];
                    
                    [studentsListArray addObject:studentObj];
                    
                    studentObj = nil;
                }

                if (studentsListArray.count >0) {
                    
                    [self.studentTableView setHidden:NO];
                    [self.studentTableView reloadData];
                }
                else{
                    [self.studentTableView setHidden:YES];
                }
            }
            else{
                
                [self.studentTableView setHidden:YES];
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"FetchTemperatureBySectionResult"][@"ResponseMessage"]];
            }
        }
        else{
            [self.studentTableView setHidden:YES];
            
            [TSSingleton showAlertWithMessage:TEXT_FAILURERESPONSE];
        }
    }];
}

-(void) updateTemperatureServiceCall:(NSDictionary *)param{
    
    NSString *urlString = @"HealthDetails/Services/TemperatureService.svc/UpdateTemperature";
    
    [[TSHandlerClass sharedHandler]postRequestWithURL:urlString baseURL:[USERDEFAULTS objectForKey:BASE_URL] param:param currentView:self.view showIndicator:YES onCompletion:^(BOOL success, NSDictionary *responseDictionary) {
        
        if (success) {
            
            if ([responseDictionary[@"UpdateTemperatureResult"][@"ResponseCode"] integerValue] == 1) {
                
                [TSSingleton showAlertWithMessage:responseDictionary[@"UpdateTemperatureResult"][@"ResponseMessage"]];
                
                NSArray *viewControllers = [self.navigationController viewControllers];
                
                [self.navigationController popToViewController:[viewControllers objectAtIndex:viewControllers.count-2] animated:YES];
            }
        }
    }];
}

@end