//
//  TSHeightAndWeightTableViewCell.m
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 20/07/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import "TSHeightAndWeightTableViewCell.h"

@implementation TSHeightAndWeightTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) loadContentForCell:(TSHeightAndWeightTableViewCell *)cell withModelArray:(NSArray *)modelArray withRow:(NSInteger)row{
    
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:200];
    UILabel *heightLabel = (UILabel *)[cell viewWithTag:201];
    UILabel *weightLabel = (UILabel *)[cell viewWithTag:202];
    
    BMIInfoModel *modelObj = [modelArray objectAtIndex:row];
    
    dateLabel.text = [SINGLETON_INSTANCE stringFromDate:modelObj.examinedDate withFormate:DATE_FORMATE_DAY_MNTH_YR];
    
    heightLabel.text = [NSString stringWithFormat:@"%@ cm",modelObj.height];
    
    weightLabel.text = [NSString stringWithFormat:@"%@ kg",modelObj.weight];
}

@end
