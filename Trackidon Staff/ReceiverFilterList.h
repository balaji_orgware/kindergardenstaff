//
//  ReceiverFilterList.h
//  Trackidon Staff
//
//  Created by Elango on 01/04/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StaffDetails;

NS_ASSUME_NONNULL_BEGIN

@interface ReceiverFilterList : NSManagedObject

+ (instancetype)instance;
- (void)saveReceiverFilterListWithResponseDict:(NSDictionary *)dict menuId:(NSString *)menuId;
@end

NS_ASSUME_NONNULL_END

#import "ReceiverFilterList+CoreDataProperties.h"
