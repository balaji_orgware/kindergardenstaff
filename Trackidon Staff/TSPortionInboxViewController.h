//
//  TSPortionInboxViewController.h
//  Trackidon Staff
//
//  Created by ORGMacMini2 on 4/15/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSPortionInboxViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *portionTableView;

@end
