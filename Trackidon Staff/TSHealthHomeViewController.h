//
//  TSHealthHomeViewController.h
//  Trackidon Staff KinderSchool
//
//  Created by Balaji on 25/06/16.
//  Copyright © 2016 ORGware. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,HealthType) {
    HealthType_GeneralInfo = 0,
    HealthType_Emergency,
    HealthType_Eyes,
    HealthType_Ears,
    HealthType_Dental,
    HealthType_NoseAndThroat,
    HealthType_GeneralCheckup,
    HealthType_HeightAndWeight
};

@interface TSHealthHomeViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIView *studentNameVw;
@property (weak, nonatomic) IBOutlet UILabel *studentNameLbl;
@property (weak, nonatomic) IBOutlet UICollectionView *menuCollectionView;

@property (nonatomic,strong) StudentModel *studentObj;

@end

@interface BMIInfoModel : NSObject

@property (nonatomic,strong) NSString *examinedBy;
@property (nonatomic,strong) NSString *examinerID;
@property (nonatomic,strong) NSString *place;
@property (nonatomic,strong) NSString *bmi;
@property (nonatomic,strong) NSString *hcTransID;
@property (nonatomic,strong) NSString *height;
@property (nonatomic,strong) NSString *transID;
@property (nonatomic,strong) NSString *weight;
@property (nonatomic,strong) NSString *remarks;

@property (nonatomic,strong) NSDate *examinedDate;

@end

@interface DentalInfoModel : NSObject

@property (nonatomic,strong) NSString *examinedBy;
@property (nonatomic,strong) NSString *examinerID;
@property (nonatomic,strong) NSString *place;
@property (nonatomic,strong) NSString *bmi;
@property (nonatomic,strong) NSString *hcTransID;
@property (nonatomic,strong) NSString *height;
@property (nonatomic,strong) NSString *transID;
@property (nonatomic,strong) NSString *weight;
@property (nonatomic,strong) NSString *extraOral;
@property (nonatomic,strong) NSString *intraOral;
@property (nonatomic,strong) NSString *remarks;

@property (nonatomic,assign) BOOL badBreath;
@property (nonatomic,assign) BOOL gumBleeding;
@property (nonatomic,assign) BOOL gumInflammation;
@property (nonatomic,assign) BOOL plaque;
@property (nonatomic,assign) BOOL softTissue;
@property (nonatomic,assign) BOOL stains;
@property (nonatomic,assign) BOOL tarter;
@property (nonatomic,assign) BOOL toothCavity;

@property (nonatomic,strong) NSDate *examinedDate;

@end

@interface EarInfoModel : NSObject

@property (nonatomic,strong) NSString *examinedBy;
@property (nonatomic,strong) NSString *examinerID;
@property (nonatomic,strong) NSString *place;
@property (nonatomic,strong) NSString *bmi;
@property (nonatomic,strong) NSString *hcTransID;
@property (nonatomic,strong) NSString *height;
@property (nonatomic,strong) NSString *transID;
@property (nonatomic,strong) NSString *leftEarRemarks;
@property (nonatomic,strong) NSString *leftEarTitle;
@property (nonatomic,strong) NSString *rightEarRemarks;
@property (nonatomic,strong) NSString *rightEarTitle;

@property (nonatomic,strong) NSDate *examinedDate;

@end

@interface EmergencyInfoModel : NSObject

@property (nonatomic,strong) NSString *examinedBy;
@property (nonatomic,strong) NSString *examinerID;
@property (nonatomic,strong) NSString *hcTransID;
@property (nonatomic,strong) NSString *transID;
@property (nonatomic,strong) NSString *emergencyTitle;
@property (nonatomic,strong) NSString *emergencyRemarks;

@property (nonatomic,strong) NSDate *examinedDate;

@end

@interface EyeInfoModel : NSObject

@property (nonatomic,strong) NSString *examinedBy;
@property (nonatomic,strong) NSString *examinerID;
@property (nonatomic,strong) NSString *hcTransID;
@property (nonatomic,strong) NSString *transID;
@property (nonatomic,strong) NSString *leftEyeRemarks;
@property (nonatomic,strong) NSString *leftEyeTitle;
@property (nonatomic,strong) NSString *rightEyeRemarks;
@property (nonatomic,strong) NSString *rightEyeTitle;

@property (nonatomic,strong) NSDate *examinedDate;

@end

@interface GeneralCheckupModel : NSObject

@property (nonatomic,strong) NSString *examinedBy;
@property (nonatomic,strong) NSString *examinerID;
@property (nonatomic,strong) NSString *hcTransID;
@property (nonatomic,strong) NSString *transID;
@property (nonatomic,strong) NSString *abdomen;
@property (nonatomic,strong) NSString *allergy;
@property (nonatomic,strong) NSString *anemia;
@property (nonatomic,strong) NSString *cardioVascular;
@property (nonatomic,strong) NSString *handsRemarks;
@property (nonatomic,strong) NSString *legsRemarks;
@property (nonatomic,strong) NSString *neckRemarks;
@property (nonatomic,strong) NSString *nerveSystem;
@property (nonatomic,strong) NSString *respiratary;

@property (nonatomic,strong) NSDate *examinedDate;

@end

@interface GeneralInfoModel : NSObject

@property (nonatomic,strong) NSString *examinedBy;
@property (nonatomic,strong) NSString *examinerID;
@property (nonatomic,strong) NSString *hcTransID;
@property (nonatomic,strong) NSString *transID;
@property (nonatomic,strong) NSString *bloodGroup;
@property (nonatomic,strong) NSString *foodAllergy;
@property (nonatomic,strong) NSString *hairColor;
@property (nonatomic,strong) NSString *identityMarkOne;
@property (nonatomic,strong) NSString *identityMarkTwo;
@property (nonatomic,strong) NSString *skinColor;
@property (nonatomic,strong) NSString *studentTransID;

@property (nonatomic,strong) NSDate *examinedDate;

@end

@interface NoseThroatInfoModel : NSObject

@property (nonatomic,strong) NSString *examinedBy;
@property (nonatomic,strong) NSString *examinerID;
@property (nonatomic,strong) NSString *hcTransID;
@property (nonatomic,strong) NSString *transID;
@property (nonatomic,strong) NSString *noseTitle;
@property (nonatomic,strong) NSString *noseDescription;
@property (nonatomic,strong) NSString *throatTitle;
@property (nonatomic,strong) NSString *throatDescription;

@property (nonatomic,strong) NSDate *examinedDate;

@end
